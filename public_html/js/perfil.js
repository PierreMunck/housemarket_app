$().ready(function(){
    
    var linkSend = $("#sendinquiry"),
    btnShare = $("#btnShare"),
    selectShareDestinyDialog = $("#select-share-destiny-dialog"),
    requestComments = $("#requestComments"),
    noRequestComments = $("#noRequestComments"),
    inquiryForm=$("#inquiry"),
    shareConf = {},    
    
    executeShare = function() {
        var shareAs = selectShareDestinyDialog.find("input:checked").val();

        selectShareDestinyDialog.dialog("close");

        if(shareAs !== 'me') {
            shareConf.uid = shareAs;
        }

        shareProperty(shareConf);
    },
        
    cancelShare = function() {
        selectShareDestinyDialog.dialog("close");
    },
        
    dialogOpts = {
        autoOpen: false,
        modal: true,
        position: [308,265],
        width: 300,
        minHeight:150,
        minWidth: 230,
        buttons: {
            "Share": executeShare,
            "Cancel": cancelShare
        }
    };
    
    /**
     * Aplica internacionalizacion
     */
    function __translate(s) {
        if (typeof(i18n)!='undefined' && i18n[s]) {
            return i18n[s];
        }
        return s;
    }

    function getParameterByName(name)
    {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if(results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }


    function shareProperty(conf) {
        // See <div id="share"> in enlistar/perfil.phtml
        
        var obj = {
            'name': conf.title,
            'caption': conf.caption,
            'description': conf.description,
            'properties': conf.properties,
            'link': btnShare.attr('urlFbProperty'),
            'picture': btnShare.attr('imageSrc')
        };
        
        if( typeof(conf.uid) !== 'undefined' ) {
            obj.to = conf.uid;
            obj.from = conf.uid;
        }
        
        postToFeed(obj);
    }
    
    if( selectShareDestinyDialog && selectShareDestinyDialog.length > 0 ) {
        selectShareDestinyDialog.dialog(dialogOpts);
        
        $(".ui-dialog-buttonset button:first").addClass("submit-share");
        $(".ui-dialog-buttonset button:last").addClass("cancel-share");
    }
    
    if( getParameterByName('valprop') !== '' && getParameterByName('permreq') === '1' && getParameterByName('error') === '' ) {
        shareConf = {
            'title': btnShare.attr('title'),
            'caption': btnShare.attr('description'),
            'description': btnShare.attr('location')
        };
        
        selectShareDestinyDialog.dialog("open");
    }
    
    if ( btnShare && btnShare.length > 0 ) {
        btnShare.click(function() {
            
            shareConf = {
                'title': btnShare.attr('title'),
                'caption': btnShare.attr('description'),
                'description': btnShare.attr('location')
            };
            
            if(selectShareDestinyDialog.length === 0) {
                shareProperty(shareConf);
            }
            else {                
                selectShareDestinyDialog.dialog("open");
            }
        });
    }
    
    if( requestComments && requestComments.length > 0) {
        requestComments.click(function() {
            parent.triggerShare = true;
            parent.$.colorbox.close();
            return false;
        });
    }
    
    if( noRequestComments && noRequestComments.length > 0) {
        noRequestComments.click(function() {
            parent.triggerShare = false;
            parent.$.colorbox.close();
            return false;
        });
    }

    if( linkSend && linkSend.length > 0) {
        linkSend.colorbox({
            iframe:true,
            innerWidth:520,
            innerHeight:260,
            fixed:true,
            scrolling:false,
            top:"10%",
            onOpen: function() {
                triggerShare = false;
            },
            onClosed:function(){
                if(triggerShare === true) {                    
                    shareConf = {
                        'title': linkSend.attr('requestcomments'),
                        'caption': btnShare.attr('title'),
                        'description': btnShare.attr('description')
                    };
                    shareProperty(shareConf);
                }
            }
        }); 
    }
    
    //Validando form inquiry si existe
    if( inquiryForm && inquiryForm.length > 0 ){
        var container = $("#mierror");
        inquiryForm.validate({
            rules:{
                name_c:{
                    required:true
                },
                email_c:{
                    required:true,
                    email:true
                }
            },
            messages:{
                name_c:{
                    required:"The name is required"
                },
                email_c:{
                    required:"The email is required",
                    email:"The email syntax is incorrect"
                }
            },
            errorElement: "li",
            errorContainer: container,
            errorPlacement: function(error, element) {            
                $("ul",container).append(error);
            },
            submitHandler: function(form) {
                var ownid=$("#ownid").val();
                var property=$("#property").val();
                var response=$("#response").val();
                var id=$("#id").val();
                var photo=$("#photo").val();
                var location=$("#location").val();
                var to=$("#email_c").val();
                var toname=$("#name_c").val();
                var tocel=$("#phone").val();
                var tocomment=$("#comment_c").val();
                var signed=$("#signed_request").val();
                var origin=$("#origin").val();
                $.ajax({
                    url:"/enlistar/sendaviso?signed_request="+signed,
                    data:{
                        response:response,
                        ownid:ownid,
                        id:id,
                        property: property,
                        photo: photo,
                        location: location,
                        to:to,
                        toname:toname,
                        tocel:tocel,
                        tocomment:tocomment,
                        origin:origin
                    },
                    type:"POST",
                    beforeSend:function(){
                        $("#sending").show('slow');
                        $("#info").hide();
                        $("#enviar").attr('disabled','disabled');
                    },
                    success:function(){
                        //$("#info").fadeIn('slow');
                        $("#requestFriendsComments").fadeIn('slow');
                        $("#fields").remove();                        
                    },
                    complete:function(){
                        $("#sending").hide('slow');
                        $("#enviar").removeAttr('disabled');
                        $("#inquiry").each(function(){
                            this.reset();
                        });
                    } 
                });  
            }
        });
    }
});