$().ready(function(){
    var categories = $("#cbCategoria");
    if(categories) { 
        categories.change(function() {            
            var name = $(this).find("option:selected").val()
            if(!name||name==11){
                $("#droom").fadeOut('fast');
            }else{
                $("#droom").fadeIn('fast');
            }            
        });
    }
    var atype = $("#type");
    if(atype) { 
        atype.change(function() {            
            var name = $(this).find("option:selected").val()
            if(!name){
                $("#dprice").fadeOut('fast');
            }else{
                $("#dprice").fadeIn('fast');
            }            
        });
    }
    
    var combo_price_min = $("#pricemin");
    var combo_price_max = $("#pricemax");
    var combo_area = $("#area");
    var combo_area_min = $("#minarea");
    var combo_area_max = $("#maxarea");
    var temp;
    
      
    var ComboAreaFt2=function(max_min,rang1){
        var label_c="";         
        var option_c = "<option value='' >"+max_min+"</option>";
        var MIN_C = 0;
        var  AUM;
        for (var j=1; j<=10; j++){
            if(j<=10)AUM=rang1;                                              
            MIN_C += AUM;  
            label_c = MIN_C;
            label_c = label_c.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
            label_c = label_c.split('').reverse().join('').replace(/^[\,]/,'');
            option_c += "<option value= '"+ MIN_C+"'>"+ label_c + " ft<sup>2</sup>"+"</option>";              
                    
        }
        option_c += "<option value= '"+ 43560 +"'>"+ "1 Acre" +"</option>"; 
        option_c += "<option value= '"+ 217800+"'>"+ "5 Acre" +"</option>"; 
        option_c += "<option value= '"+ 2178000+"'>"+ "50 Acre" +"</option>"; 
        option_c += "<option value= '"+ 4356000+"'>"+ "100 Acre" +"</option>"; 
        return option_c;
    }
        
    var ComboArea=function(max_min,rang1, rang2, rang3){
        var label_c="";         
        var option_c = "<option value='' >"+max_min+"</option>";
        var MIN_C = 0;
        var  AUM;
        for (var j=1; j<=12; j++){
            if(j<=10)AUM=rang1; 
            if(j>10 && j<=11){
                AUM=rang2;
            } 
            if(j>11 && j<=12){
                AUM=rang3;
            }                            
            MIN_C += AUM;  
            label_c = MIN_C;
            label_c = label_c.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
            label_c = label_c.split('').reverse().join('').replace(/^[\,]/,'');
            option_c += "<option value= '"+ MIN_C+"'>"+ label_c + " mt<sup>2</sup>" +"</option>";             
                    
        }
        option_c += "<option value= '"+ 10000 +"'>"+ "1 hect" +"</option>"; 
        option_c += "<option value= '"+ 50000+"'>"+ "5 hect" +"</option>"; 
        option_c += "<option value= '"+ 500000+"'>"+ "50 hect" +"</option>"; 
        option_c += "<option value= '"+ 1000000+"'>"+ "100 hect" +"</option>"; 
        return option_c;
    }
        
        
    var ComboRent=function(max_min,rang1, rang2, rang3, rang4){
        var label_c="";         
        var option_c = "<option value='' >"+max_min+"</option>";
        var MIN_C = 0;
        var  AUM;
        for (var j=1; j<22; j++){
            if(j<=10)AUM=rang1; 
            if(j>10 && j<=15){
                AUM=rang2;
            } 
            if(j>15 && j<=19){
                AUM=rang3;
            } 
            if(j>19 && j<=22){
                AUM=rang4;
            }                
            MIN_C += AUM;  
            label_c = MIN_C;
            label_c = label_c.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
            label_c = label_c.split('').reverse().join('').replace(/^[\,]/,'');
            option_c += "<option value= '"+ MIN_C+"'>"+ label_c +"</option>";             
                    
        }
        return option_c;
    }
        
    var ComboSale=function(max_min,rang1, rang2, rang3, rang4, rang5, rang6){
        var label_c="";
        var option_c = "<option value='' >"+max_min+"</option>";
        var MIN_C = 0;
        var  AUM;
        for (var j=1; j<24; j++){
            if(j<=10)AUM=rang1; 
            if(j>10 && j<=12){
                AUM=rang2;
            } 
            if(j>12 && j<=14){
                AUM=rang3;
            } 
            if(j>14 && j<=17){
                AUM=rang4;
            }
            if(j>17 && j<=19){
                AUM=rang5;
            } 
            if(j>19 && j<=23){
                AUM=rang6;
            }                 
            MIN_C += AUM;
            if(j>= 12){
                label_c = (MIN_C/1000000) + " " +__translate("SEARCH_LABEL_MILLION");                   
            }
            else{
                label_c= MIN_C;
                label_c = label_c.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
                label_c = label_c.split('').reverse().join('').replace(/^[\,]/,'');
            }                   
            option_c += "<option value= '"+ MIN_C+"'>"+ label_c +"</option>";                          
                    
        }
        return option_c;
    }
        
   
        
    if(atype) {
        atype.change(function(e){
            var valor = atype.val();                     
            if(valor=="R"){
                combo_price_min.html(ComboRent("Min",500,1000,5000,10000));
                combo_price_max.html(ComboRent("Max",500,1000,5000,10000));
            }else if(valor=="S"){
                combo_price_min.html(ComboSale("Min",50000,250000,500000,1000000,2500000,10000000));
                combo_price_max.html(ComboSale("Max",50000,250000,500000,1000000,2500000,10000000));
            }  
            else if(valor=="F"){
                combo_price_min.html(ComboSale("Min",50000,250000,500000,1000000,2500000,10000000));
                combo_price_max.html(ComboSale("Max",50000,250000,500000,1000000,2500000,10000000));
            } else {
                combo_price_min.html("<option value=''>Min</option>");
                combo_price_max.html("<option value=''>Max</option>");
            }  
        });
    }    
   
    if(combo_area) {             
        combo_area.change(function(e){   
            if(combo_area.val()=="mt2"){
                combo_area_min.html(ComboArea("Min",50,500,4000));   
                combo_area_max.html(ComboArea("Max",50,500,4000));
            }else{
                combo_area_min.html(ComboAreaFt2("Min",500)); 
                combo_area_max.html(ComboAreaFt2("Max",500));               
            }               
                  
        } )
    }
     
});
