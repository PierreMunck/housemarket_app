$().ready(function(){
    
    var currencyCode;
    
    function __translate(s) {
        if (typeof(i18n)!='undefined' && i18n[s]) {
            return i18n[s];
        }
        return s;
    }
    
    function setCurrencyCode() {
        var currencyHtml = '',
            currencyCode = mapCountryToCurreny[countryCode].CurrencyCode,
            currencyName = __translate("ISO_4217_CURRENCY_CODE_" + currencyCode),
            countryName = mapCountryToCurreny[countryCode].CountryName;
            
        if(mapCountryToCurreny !== undefined) {
            currencyHtml = '<abbr title="' + currencyName + '">' + currencyCode + '</abbr>';
            $(".currency-code").html(currencyHtml);
        }
        
        $(".currency-name").html(currencyName);
        $(".country-name").html(countryName);
    }
    
    //Configurando Google Maps 3.0
    var geocoder;
    var map;
    var markersArray = [];
    var centermap = {
        lat: "25.79",
        lng: "4.21"
    };
    var first=true;
    
    var mapCountryToCurreny;
    
    function initialize() {
        geocoder = new google.maps.Geocoder();
        //Capturando coordenadas, si existen
        var lat=$.trim($("#latitud").val());
        var lng=$.trim($("#longitud").val());        
        var zoom=12;
        if(lat!=""){
            centermap.lat=lat;
            zoom=14;
        }
        if(lng!=""){
            centermap.lng=lng;
            zoom=14;
        }
        
        // Obtener moneda por paises
        $.ajax({
            url:"/propiedades/getcurrencybycountry",
            type:'post',
            dataType:'json',
            success:function(data){
                if(data.success){
                    mapCountryToCurreny = data.rows;
                }
            }
        });
        
        
        var latlng = new google.maps.LatLng(centermap.lat, centermap.lng);
        var myOptions = {
            zoom: zoom,
            scrollwheel:false,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map"), myOptions);        
        addMarker(latlng);
    }
    function addMarker(location) {
        var image = new google.maps.MarkerImage('/img/house.png',
            new google.maps.Size(24, 33));
        marker = new google.maps.Marker({
            position: location,
            map: map,
            icon: image,
            draggable: true,
            clickable:false,
            cursor:'hand'       
        });
        markersArray.push(marker);
        //Ubicando direccion con agregar el marcador
        codeLatLng(location);
        //Ubicando latitud y longitud
        $("#latitud").val(location.lat());
        $("#longitud").val(location.lng());
        //Controlando el evento de arrastre    
        google.maps.event.addListener(marker, 'dragend', function() {           
            var position=marker.getPosition();            
            $("#latitud").val(position.lat());
            $("#longitud").val(position.lng());
            //Ubicando direccion con mover el marcador
            codeLatLng(position);
        });
    }
 
    // Removes the overlays from the map, but keeps them in the array
    function clearOverlays() {
        if (markersArray) {
            for (i in markersArray) {
                markersArray[i].setMap(null);
            }
        }
    }

    function codeAddress() {
        var address = $("#dir").val();
        geocoder.geocode( {
            'address': address,
            'region' : cc
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                map.setZoom(14);
                addMarker(results[0].geometry.location);
                //clearOverlays();
                
                              
            } else {
                //alert("Geocode was not successful for the following reason: " + status);
            }
        });
    }
    function codeLatLng(latlng) {
        // For request/response documentation refer to
        // DOC: http://bit.ly/he0iZK The Google Geocoding API
        if (geocoder) {
            geocoder.geocode({
                'latLng': latlng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results) {  
                        var rs,name;
                        //Capturando pais
                        rs=results[1];
                        if(rs&&rs.address_components){
                            if(!first)
                                $("#direccion").val(rs.formatted_address);
                            first=false;
                            $.each(rs.address_components,function(i,item){
                                switch(item.types[0]){
                                    case "country":{
                                        $("#pais").val(item.long_name);
                                        $("#zipcode").val(item.short_name);
                                        countryCode = item.short_name;
                                        setCurrencyCode();
                                        break;
                                    }
                                    case "administrative_area_level_1":{
                                        $("#estado").val(item.long_name);
                                        break;
                                    }
                                    case "locality":{
                                        $("#ciudad").val(item.long_name);
                                        break;
                                    }
                                    case "postal_code":
                                    {
                                        $("#codigoPostal").val(item.long_name);
                                        break;
                                    }
                                }
                            });                            
                        }
                    }
                } else {
                    //alert("Geocoder failed due to: " + status);
                }
            });
        }
    }
    //Lanzando funciones de google maps
    initialize();
    $("#btFind").click(function(){
        codeAddress();
    });

    $("#dir").keypress(function (e) {
        if (e.which == 13){
            codeAddress();
        }
    });
    //Poniendo eventos en el check
    var checkterms=$("#chkTerms");
    if(!checkterms||checkterms.length===0){
        $("#btnSaveDown,#btnSavePubDown").removeClass('button_disable').addClass('button').attr('href', 'javascript:;');            
        $("#btnSaveDown").click(function(){                
            $('#registro').val("S");
            $('#publicado').val("N");
            FB.Canvas.scrollTo(0,0);
            $('#frm').submit();                
        });   
        $("#btnSavePubDown").click(function(){
            $('#registro').val("S");
            $('#publicado').val("S");
            FB.Canvas.scrollTo(0,0);
            $('#frm').submit();
        }); 
    }else{
        checkterms.click(function(){
            if($(this).is(":checked")){   

                $("#btnSaveDown,#btnSavePubDown").removeClass('button_disable').addClass('button').attr('href', 'javascript:;');            
                $("#btnSaveDown").click(function(){                
                    $('#registro').val("S");
                    $('#publicado').val("N");
                    FB.Canvas.scrollTo(0,0);
                    $('#frm').submit();                
                });   
                $("#btnSavePubDown").click(function(){
                    $('#registro').val("S");
                    $('#publicado').val("S");
                    FB.Canvas.scrollTo(0,0);
                    $('#frm').submit();
                }); 
            }else{
                $("#btnSaveDown,#btnSavePubDown").removeClass('button').addClass('button_disable').attr('href', 'javascript:;');
            //$("#btnSaveDown,#btnSavePubDown").unbind('click');
            }
        });
    }
    //Validacion de formulario con jquery
    var container = $('#frm #errorbox');
    $('#frm').validate({
        rules:{
            cat:{
                required:true
            },            
            alquilerUS:{
                required:function(){
                    return ($.trim($("#alquilerUS").val())==''&&$.trim($("#ventaUS").val())==''&&$.trim($("#remateUS").val())=='');
                }                
            },
            titulo:{
                required:true
            },
            pais:{
                minlength:1,
                maxlength:128
            },
            estado:{
                minlength:1,
                maxlength:64
            },
            ciudad:{
                minlength:1,
                maxlength:64
            },
            direccion:{
                required:true,
                minlength:1,
                maxlength: 255
        
            },
            codigoPostal:{
                minlength:1,
                maxlength:64
            },
            descripcion:{
                maxlength:512
            },
            fprincNew:{
                required:true,
                accept:'jpg|gif|png|bmp|jpeg'                
            },
            fotraNew0:{
                accept:'jpg|gif|png|bmp|jpeg'
            },
            fotraNew1:{
                accept:'jpg|gif|png|bmp|jpeg'
            },
            fotraNew2:{
                accept:'jpg|gif|png|bmp|jpeg'
            },
            fotraNew3:{
                accept:'jpg|gif|png|bmp|jpeg'
            },
            area:"required"
        },
        messages:{
            cat:"Choose a valid property type",
            alquilerUS:"Choose for foreclosure or for rent and/or for sale",
            titulo:"Enter a valid title",
            pais:"Country name is too long. Country name must be between 1 and 128 characteres",
            estado:"State name is too long. State name must be between 1 and 64 characteres",
            ciudad:"City name is too long. City name must be between 1 and 64 characteres",
            dir:{
                required:"Enter a valid address"
            },
            direccion:{
                required:"Enter a valid address",
                maxlength:"Address is too long. Address must be between 1 and 255 characters"
            },
            codigoPostal:"Zip code is too long. Zip Code must be between 1 and 64 characters",
            descripcion:"Description is too long, please enter a title equal or less than 512 characters",
            fprincNew:{
                required:"The principal image is required",
                accept:"Select an image(png, jpg, jpeg, gif) "
            },
            area:"Enter a valid area"
            
        },        
        errorElement: "li",
        errorContainer: container,
        errorPlacement: function(error, element) {            
            $("ul",container).append(error);
        },
        submitHandler: function(form) {  
            FB.Canvas.scrollTo(0,0);
            $(form).ajaxSubmit({
                target: "#box",
                dataType:"xml",
                beforeSubmit:function(){
                    $("#loading").fadeIn('slow');
                },
                error:function(jqXHR, textStatus, errorThrown){
                    $('#errorbox ul').fadeIn('slow').append("<li>"+errorThrown+"</li>");
                },
                success: function(responseXML) {
                    var obj = {};
                    obj.location= $('location', responseXML).text();
                    obj.title= $('title', responseXML).text();
                    obj.message= $('message', responseXML).text(); 
                    obj.status=  $('status', responseXML).text(); 
                    obj.redirect=$('redirect', responseXML).text(); 
                    obj.publicar=$('publicar', responseXML).text(); 
                    obj.href=$('href', responseXML).text(); 
                    obj.caption=$('caption', responseXML).text(); 
                    obj.description=$('description', responseXML).text(); 
                    obj.image=$('image', responseXML).text(); 
                    if(obj){
                        if(obj.status=='success'){
                            if($('#publicado').val()=='S'){
                                var attachment={
                                    'name':obj.title,
                                    'href':obj.href,
                                    'caption':obj.caption,
                                    'description':obj.description,  
                                    'properties':{
                                        'Loc':obj.location
                                    },                              
                                    'media':[{
                                        'type':'image',
                                        'src':obj.image,
                                        'href':obj.href
                                    }]
                                };
        
                                var publish={
                                    method:'stream.publish',
                                    message:'',
                                    attachment:attachment
                                };
                                FB.ui(publish,function(response) {
                                    top.location.href=obj.redirect;
                                    return false;
                                });
                            }else{
                                top.location.href=obj.redirect;
                                return false;
                            }
                            
                           
                            
                        //console.log(obj.redirect);
                        }
                        $("#loading").fadeOut('slow');                    
                        $('#box').fadeIn('slow').html(obj.message); 
                        
                    }
                /*if($('#box div').hasClass('success')){
                        $('#frm').resetForm();
                        
                        if($("#chkTerms").is(":checked")){
                            $("#chkTerms").click();
                        }
                        initialize();
                        setInterval(function(){
                            $("div.success").fadeOut(3000);
                        },15000);
                    }*/
                },
                complete:function(){
                    $("#loading").fadeOut('slow');
                }            
            });//$(form).submit();
             
        }
    });
    //Validando largos de 512.
    var descripcion = $("#descripcion");
    if(descripcion) {
        descripcion.keyup(function(e){
            var valor=$(this).val();;           
            if(valor.length>512){
                valor =valor.substring(0,512);
            }            
            return true;
        });

        descripcion.blur(function(e){
            var valor=$(this).val();            
            if(valor.length>512){
                valor=valor.substring(0,512);
            }
            return true;
        });
    }


    //Mostrando los beneficios y atributos
    var LabelArea = $("#labarea");
    
    $("#cat").change(function() {
        value = $(this).val();       
        if(value == 11){
            LabelArea.html(__translate("SEARCH_RESULT_FOR_AREA_LOTE"));
            $("#labareaLote").hide();            
            $("#areaLote").hide();             
            $("#areaLoteUM").hide();
        }else{
            LabelArea.html(__translate("SEARCH_RESULT_FOR_AREA"));
            $("#labareaLote").show();            
            $("#areaLote").show();             
            $("#areaLoteUM").show();
        }
        //Obteniendo los atributos
        $.ajax({
            url:"/propiedades/getatributos",
            data:{
                'category':value,
                'signed_request':signed
            },
            type:'post',
            dataType:'json',
            beforeSubmit:function(){
                $("#beneficiosList").hide();
            },
            success:function(data){                
                if(data.success){
                    var body='';
                    $.each(data.rows, function(index,item){
                        var labelAppend = '';
                        if(item.nombre === 'HOA') {
                            labelAppend = ' <span class="currency-code"></span>';
                        }
                        
                        body+='<div class=\"detalle\"><label>'+item.nombre + labelAppend +': </label><br />';
                        if(item.tipo=="Range"){
                            body+='<input class=\"labelHighlight\" value=\"\" id=\"attr-'+item.codigo+'\" name=\"attr['+item.codigo+']\" type=\"text\" />';  
                        }else if(item.tipo=="Check"){
                            body+='<input type=\"hidden\" name=\"attr['+item.codigo+']\" value=\"N\"/><input class=\"labelHighlight\" value=\"S\" name=\"attr['+item.codigo+']\" id=\"attr-'+item.codigo+'\" type=\"checkbox\"/>';
                        }else{
                            body+='<input class=\"labelHighlight\" value=\"\" id=\"attr-'+item.codigo+'\" name=\"attr['+item.codigo+']\" type=\"text\" />';
                        }
                        body+='</div>';
                    });  
                    
                    $("#attributes").html(body);
                    setCurrencyCode();
                }                
            },
            complete:function(){
                $("#beneficiosList").fadeIn('slow');
            }
        });
        
        //Obteniendo los beneficios
        $.ajax({
            url:"/propiedades/getbeneficios",
            data:{
                'category':value,
                'signed_request':signed
            },
            type:'post',
            dataType:'json',
            beforeSubmit:function(){
                $("#beneficiosList").hide();
            },
            success:function(data){                
                if(data.success){
                    var body='';
                    $.each(data.rows, function(index,item){
                        body+='<label class=\"beneficios\"><input type=\"checkbox\" value=\"'+item.codigo+'\" name=\"benef[]\" id=\"benef[]\" />'+item.nombre+'</label>';                        
                        
                    });  
                    body+='<div style=\"clear:both\"></div>';
                    $("#beneficiosList").html(body);
                }             
                
            },
            complete:function(){
                $("#beneficiosList").fadeIn('slow');
            }
        });
    });

});