/*
 * Grid para el listado de aprobaci�n de cr�ditos del cliente
 *
 */
Ext.onReady(function(){
    Ext.BLANK_IMAGE_URL = '/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = "side";

    function _GetParametersListURL(){
        var _parameters={};
        var _6=window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,_index,_value){
            _parameters[_index]=unescape(_value);
        });
        return _parameters;
    };

    var _parametersList=_GetParametersListURL();
    var _urlParameters=Ext.urlEncode(_parametersList);

    var prueba = getUrlVars();
    var fb = Ext.urlEncode(prueba);
    var fb1 = Ext.urlDecode(Ext.urlEncode(prueba));

    Ext.namespace('Ext.userdata');
    Ext.userdata.status = [['0', 'inactiva'], ['1', 'activa']];
    var stStore = new Ext.data.SimpleStore({
        fields: ['status', 'label'],
        data: Ext.userdata.status
    });

    var mutual = Ext.get("mutual");
    mutual.on("click",function() {
        Ext.Ajax.request({
            url:"/fbml/mutual-friends" +"?"+ _urlParameters,
            params: {
                id: $(this).attr("uid")//id de facebook del usuario actual
            },
            success:function(_response,_options){
                var content = '<fb:fbml>' +
                _response.responseText +
                '</fb:fbml>';
                var req = {
                    method: 'fbml.dialog',
                    display: 'dialog',
                    fbml:  (content),
                    width: 900
                }

                FB.ui(req);
            },
            failure:function(_response,_options){
                alert(_response);
            }
        });
    });

    function getUrlVars() {
        var map = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            map[key] = unescape(value);
        });
        return map;
    }

    $("a.paginacion_img  > img").hover(function(){
            //$("img",this).attr("src").replace("/img/page_first_blue.gif", "/img/page_first_gray.gif");
            var id = this.id;
            this.src = "/img/page_"+id+"_green.gif";
        }, function(){
            var id = this.id;
            this.src = "/img/page_"+id+"_blue.gif";
        }
        );
});