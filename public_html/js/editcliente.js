$().ready(function(){
    var parseUrl=function(name,url)
    {
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec(url);
        if( results == null )
            return "";
        else
            return results[1];
    }
    //Preview de video
    $("#youpreview").hover(function(){
        var id=parseUrl('v',$("#youvideo").val());
        if(id){
            $("#youpreview").attr("href","https://www.youtube.com/embed/"+id);
            $("#youpreview").colorbox({
                iframe:true, 
                innerWidth:425, 
                innerHeight:344
            });
        }
    }, function(){
        $("#youpreview").attr("href","#")
    });
    setInterval(function(){
        $("div.success").fadeOut(3000);
    },5000);    
    var container = $('#errorbox');
    $("#form_show").validate({
        rules:{
            nombre:{
                required:true
            },
            email:{
                email:true,
                required:true
            },
            twitter:{
                semiurl:true
            },
            paginaweb:{
                semiurl:true
            },
            youvideo:{
                semiurl:true
            }
        },       
        errorElement: "li",
        errorContainer: container,
        errorPlacement: function(error, element) {
            container.attr('class','error');            
            $("ul",container).append(error);            
        }
    });
    //Cambios de perfil
    var changeProfile=function(id){
        $.ajax({
            url:"/mihousebook/previewprofile",
            dataType: 'json',
            async:false,
            data:{
                id:id
            },
            type:"POST",
            start:function(){
                $("#btnGuardar").attr("disabled","disabled");
            },
            success:function(data){
                $.each(data, function(index) {
                        
                    if(data[index]!==undefined){
                        if(data[index].pic_big!==undefined){
                            $("#fb_poster-info img").fadeOut(100, function(){
                                $(this).attr("src",data[index].pic_big)
                                .attr("title",data[index].name)
                                .attr("alt",data[index].name);
                            });
                            $("#fb_poster-info img").fadeIn('fast');
                                
                            $("#nombre").val(data[index].name);
                            $("#email").val(data[index].email);
                            $("#twitter").val(data[index].twitter);
                            $("#telefono").val(data[index].phone);
                            $("#paginaweb").val(data[index].web);
                            $("#empresa").val(data[index].bness);
                            $("#telempresa").val(data[index].phoneb);
                            $("#youvideo").val(data[index].yvideo);
                            $(".fbtitle .linkprofile").text(data[index].name);
                            var uid=(data[index].uid)?data[index].uid:data[index].page_id;
                            $(".linkprofile").attr("href","https://www.facebook.com/profile.php?id="+uid);
                        }                    
                    }
                });
                    
            },
            complete:function(){
                $("#btnGuardar").removeAttr("disabled");
            }
        });
    }
    $(".fanpage").change(function(){
        if($(this).attr("id")=="fanpage_1" && $(this).is(":checked")){
            $("#pageid").fadeIn('slow');
        }else if($(this).attr("id")=="fanpage_0" && $(this).is(":checked")){
            //$("#pageid option").attr("selected", "");
            $("#pageid").val(null).change();                
            $("#pageid").fadeOut('fast');
        //changeProfile(0);
                
        }
    });
    $(".fanpage").change();
    //Cambiando perfiles
    $("#pageid").change(function(){
        var id=$(this).find(" option:selected").val();
        changeProfile(id);            
    });
/*    
    $("#form_show").submit(function(){
        $("#btnGuardar").attr("disabled","disabled"); 
    });
    */
});
