/**
 * Recupera variables de entorno
 */


function __translate(s) {
    if (typeof(i18n)!='undefined' && i18n[s]) {
        return i18n[s];
    }
    return s;
}
function __environment(s) {
    if (typeof(env)!='undefined' && env[s]) {
        return env[s];
    }
    return s;
}

function ListPage(pag_show){
    var ulPage = "<ul class='paginacion_cont'>" +
                 "<li class='paginacion_cont_li'></li> ";
    for(var i=1;i<=pag_show;i++){
        
    }
    return ulPage;
}

$(document).ready(function(){ 
    var test = 0; 
    var init_range = (5*(pages - 1)) + 1;
   var final_range 
    var pages = 1;
  var page_show = 1;
  var sum_range;
   var total_result;
    function getownerUid(){       
        var vownerUid = owner_Uid;
        return vownerUid;
    }
    function habPaginatorNextLast(){
        $('#previous_li').html('<span class="disabled"><img src="/img/page_previous_gray.gif"/></span>');
        $('#first_li').html('<span class="disabled"><img src="/img/page_first_gray.gif"/></span>');    
        $('#last_li').html('<a id="last_a" href="javascript:;">  <img id="last" src="/img/page_last_blue.gif"/></a>');
       $('#next_li').html('<a id="next_a">  <img id="next" src="/img/page_next_blue.gif"/></a>');
   
      
    }
    
    function habPaginatorPreviousFirst(){
        $('#previous_li').html('<a id="previous_a" heref="javascript:;"><img id="previous" src="/img/page_previous_blue.gif"/></a>');
              $('#first_li').html(' <a id="first_a"class="paginacion_img" href="javascript:;"><img id="first" src="/img/page_first_blue.gif"/></a>');   
              $('#next_li').html('<span class="disabled"><img src="/img/page_next_gray.gif"/></span>'); 
              $('#last_li').html('<span class="disabled"><img src="/img/page_last_gray.gif"/></span>');
           
    }
    
    function TabFiltro(){
        $.ajax({
            url: "/fbml/propertiesByUid",
            type: 'post',
            dataType: "json", 
            data: {
                uid:getownerUid(),
                page:pages,
                signed_request:$("#signed_request").val(),
                cbCategoria:$("#cbCategoria").val()
            },
            success: function(response){
                 var property
                 var template ='';
                 var destacada = '';
                 var codigoP ;
                 var PROFotoID;
                 var limit_for;
              
                
              try{
                    if(response.rows.results >0){ 
                        total_result = response.rows.results;
                        page_show =Math.ceil(total_result/5);                        
                          $('#listado_propiedades').fadeIn('slow');
                          $('#TotalItemNumber').text(" "+ response.rows.results+" ");                          
                     sum_range = response.rows.rows.length;
                     console.log("first");
                     console.log(sum_range);
                     limit_for = sum_range - 1;
                    for (var i = 0; i <= limit_for; i++) { 
                        property = response.rows.rows[i];                     
                        if(property['PROFotoID'] == null || property['PROFotoID'] == '')
                          PROFotoID = __translate("AD_NO_IMAGE");  
                       else
                         PROFotoID = '/imagen.php?id='+property['PROFotoID']+'&tipo=2';                         
                        codigoP = 'ID'+property['CodigoPropiedad'];
                        if(property['CodigoBroker'] != null)
                            codigoP=property['CodigoBroker'];
                        var creditos = property['creditos'];
                        var area = property['Area'] ;
                        var rooms = property['Cuarto'] ;
                        var bano = property['bano'] ;
                        var detalle_atributos = '';
                       if(area > 0){
                           detalle_atributos += area  +" " + property['UnidadMedida'] + " | ";}
                       if( bano >0){
                           detalle_atributos += bano + ' '+  __translate('SEARCH_RESULT_FOR_BATH') +  " | ";}
                       if(rooms > 0){
                           detalle_atributos += rooms + ' '+ __translate('SEARCH_RESULT_FOR_ROOM') + " | ";}
                        if(creditos>0)
                        destacada = "<a id='fet' class='fet' target='_top'href='"+ __environment('facebook_url') + "enlistar/perfil?valprop=11188' name='fet' style='background-image: url("+__translate("FEATURE_IMAGE_BACKGROUND")+");'></a>";
                        else
                        destacada = '';
                        template += "<div id='propiedad_item_'"+property['CodigoPropiedad']+" class='propiedad_item'>" +
                                        "<div class='likedv'> </div>" +
                                        "<span class='detalle_propiedad'>" +
                                            "<span class='precio'>" + property['NombreCategoria'] +" "+ property['Transaccion'] +"</span>" +
                                        "</span>" +
                                        "<a class='titulo' href='"+ __environment('facebook_url') + "enlistar/perfil?valprop="+property['CodigoPropiedad'] + "' target='_top'>"+
                                            "<strong id='"+property['CodigoPropiedad'] + "'>"+codigoP+" - " +property['NombrePropiedad']+"</strong>" +
                                        "</a>"+
                                        "<div class='imagen_propiedad'>"+
                                            "<a href='"+ __environment('facebook_url') + "enlistar/perfil?valprop="+property['CodigoPropiedad'] + "' target='_top'>"+
                                                "<img id='' width='120' height='90' src='"+PROFotoID+ "'>"+
                                            "</a>"+ destacada +
                                        "</div>"+
                                        "<div class='detalle_propiedad'>"+
                                            "<span class='direccion'>"+ property['Direccion'] + "</span>" +
                                            "<span class='detalles'>"+detalle_atributos+"</span>"+
                                            "<span class='descripcion'>"+property['DescPropiedad']+"</span>"+
                                        "</div>"+
                                        "<div class='clear'></div>"+
                                    "</div>"
                     
                            } 
                         $("#listado_items").html(template);
                }
            else
                    $('#listado_propiedades').fadeOut('fast');}
                catch(err){
                    console.log(err)
                } 
            
            }
        });
    }
    // Identificando al enlace que le damos click
    $('#ul_categoria a').click(function(){      
        //Removiendo la clase chec en los enlaces encontrados                    
        $("#test a").removeClass('chec');   
        var id =$(this).attr('id'); 
        if(id=="property_none")id='';
        var texto_a=$(this).text();   
        //Agrego la clase chec al enlace seleccionado
        $(this).addClass("chec");       
        $("#mywrap").removeClass("openToggler");
        $("#div_property").slideUp(1);
        console.log('test');
        test=0;                  
        $("#titmy").text(texto_a);                      
        $("#cbCategoria").val(id);
        $("#nCategoria").val(texto_a);        //$("#formdi").submit();
      
        TabFiltro();			   
    });
    //PREVIOUS
     $("#previous_li").click(function(){//IR UNO POR UNO EN EL NEXT
         if(pages <= (page_show) && pages > 1){             
          if((pages -1) == 1) 
          habPaginatorNextLast(); 
          $("#pages_cont li").removeClass('current'); 
          pages = pages - 1;          
           $("#li_"+pages).addClass('current');
           TabFiltro();//HACEMOS EL FILTRO
         }
      }); 
      //FIRST
        $("#first_li").click(function(){
          if(pages != 1){
          pages = 1;
          $("#pages_cont li").removeClass('current');
          $("#li_"+pages).addClass('current');       
          TabFiltro();//HACEMOS EL FILTRO
          habPaginatorNextLast();
          }
      });
      //
    //NEXT && LAST
      $("#next_li").click(function(){//IR UNO POR UNO EN EL NEXT
         if(pages <= (page_show-1)){ 
          $("#pages_cont li").removeClass('current'); 
          pages = pages + 1;          
           $("#li_"+pages).addClass('current');
           TabFiltro();//HACEMOS EL FILTRO
       init_range = (5*(pages - 1)) + 1;
       console.log('second');
       console.log(sum_range)
       final_range = init_range + sum_range -1;
          if((pages) == page_show){ 
          habPaginatorPreviousFirst();
          final_range =final_range -1;
          }     
        $("#FirstItemNumber").html(" "+init_range+" ");
        $("#lastItemNumber").html(" "+final_range+" ");
         }
      });      
      $("#last_li").click(function(){
          if(pages != page_show){
          pages = page_show;         
          $("#pages_cont li").removeClass('current');
          TabFiltro();//HACEMOS EL FILTRO
           habPaginatorPreviousFirst()
       init_range = (5*(pages - 1)) + 1;       
        $("#FirstItemNumber").html(" "+init_range+" ");
        $("#lastItemNumber").html(" "+ total_result+" ");
          }
      });
      //
    
  $('#pages_cont li.paginacion_cont_li').click(function(){  
        if(page_show>1) { 
            var current_page = parseInt($(this).text());
         if(pages != current_page){ 
         if(($(this).text() < page_show && $(this).text()>0)){            
             pages = parseInt($(this).text());
             if(pages>1){//SIEMPRE Y CUANDO NO SE LA PRIMERA PAGINA
              $('#previous_li').html('<a id="previous_a" heref="javascript:;"><img id="previous" src="/img/page_previous_blue.gif"/></a>');
              $('#first_li').html(' <a id="first_a"class="paginacion_img" href="javascript:;"><img id="first" src="/img/page_first_blue.gif"/></a>');   
             }else{
               $('#previous_li').html('<span class="disabled"><img src="/img/page_previous_gray.gif"/></span>');
               $('#first_li').html('<span class="disabled"><img src="/img/page_first_gray.gif"/></span>');    
            }
             //SIEMPRE Y CUANDO NO SE LA ULTIMA PAGINA
             $('#last_li').html('<a id="last_a" href="javascript:;">  <img id="last" src="/img/page_last_blue.gif"/></a>');
             $('#next_li').html('<a id="next_a">  <img id="next" src="/img/page_next_blue.gif"/></a>');
         }   
         else{//SI ES LA ULTIMA PAGINA HABILITAMOS EL FIRST y EL PREVIOUS
             if($(this).text() == page_show){
             habPaginatorPreviousFirst()           
         }
         }
       $("#pages_cont li").removeClass('current');    
       $(this).addClass("current");      
       TabFiltro();//HACEMOS EL FILTRO
     init_range = (5*(pages - 1)) + 1;
     final_range = init_range + sum_range - 1;
        $("#FirstItemNumber").html(" "+init_range+" ");
        $("#lastItemNumber").html(" "+final_range+" ");
     }}    
       
  });
  $("#listing").click(function(){			
        if(test==0){
            $(this).parent().find("#div_listing").slideDown(1); 
            $("#mywrap2").addClass("openToggler");
            test=1;
        }
        else{
            $(this).parent().find("#div_listing").slideUp(1); 
            $("#mywrap2").removeClass("openToggler");
            test=0;
        }
    });  
  
    $("#test").click(function(){			
        if(test==0){
            $(this).parent().find("#div_property").slideDown(1); 
            $("#mywrap").addClass("openToggler");
            test=1;
        }
        else{
            $(this).parent().find("#div_property").slideUp(1); 
            $("#mywrap").removeClass("openToggler");
            test=0;
        }
    });              
});