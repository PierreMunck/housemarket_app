$(document).ready(function(){

    function __translate(s){
        if(typeof(i18n)!='undefined'&&i18n[s]){
            return i18n[s];
        }
        return s;
    }

    $('#upload-listing-toggle-errors-content').click(function() {

      $('#upload-listing-errors-content').toggle('slow', function() {
        // Animation complete.
      });

      $(this).text($(this).text() === __translate('LISTING_UPLOAD_CLICK_HERE_TO_SHOW_ERROR_DETAILS') ? __translate('LISTING_UPLOAD_CLICK_HERE_TO_HIDE_ERROR_DETAILS') : __translate('LISTING_UPLOAD_CLICK_HERE_TO_SHOW_ERROR_DETAILS'));
      return false;

    });

});