var i18n = { 
    // Pagina Index
    // Google Map Messages
    G_GEO_SUCCESS : 'Success',
    G_GEO_MISSING_ADDRESS : 'Missing address: the address provided was lost',
    G_GEO_UNKNOWN_ADDRESS : 'Unknown address: the geographical location could not be found with the address provided.',
    G_GEO_UNAVAILABLE_ADDRESS : 'Unavailable address: the geographical coordinates for the address provided can not be displayed because of legal or contractual reasons.',
    G_GEO_BAD_KEY : 'Bad Key: the API key is invalid or does not correspond to the one generated for this domain.',
    G_GEO_TOO_MANY_QUERIES : 'Too many queries: the number of Geocode queries has exceeded the quota for this site.',
    G_GEO_SERVER_ERROR : 'Server error: the request could not be processed successfully by the server.',
    G_GEO_ALERT_TITLE : 'Warning',
    G_GEO_ALERT_MESSAGE_FAIL : 'Geocode service could not be contacted.',
    G_GEO_ALERT_MESSAGE_AMBIGUOUS : 'The address provided is ambiguous. Please provide further reference to this address in order to perform the search successfully.',
    G_GEO_ALERT_MESSAGE_NOT_FOUND : ' not found',
    G_GEO_ALERT_GOOGLE_NOT_SUPPORTED : 'Your browser does not support Google Maps',
    G_GEO_ALERT_STATUS_CODE : 'Status code : ', 

    // Tooltip
    TOOLTIP_ENTER_SEARCH_TEXT : 'Enter any city, neighborhood, zip, country, ID number',
    TOOLTIP_ENTER_SEARCH_TISEARCH_RESULT_GOTO_PROFILETLE : 'Help',


    // SEARCH SAMPLE
    TEXT_SEARCH_SAMPLE : 'E.g. Miami, USA',

    //PROPERTIES
    DLG_TERMS_CONDITIONS_CONTENT : 'Please Accept Terms of Use',
    DLG_TERMS_CONDITIONS_TITLE : 'Terms and Conditions',

    // SEARCH RESULT
    ALERT_MESSAGE_NOT_FILTER: 'Please select a property type and listing type',
    SEARCH_RESULT_STATUS : 'Status',
    SEARCH_RESULT_DESCRIPTION : 'Description',
    SEARCH_RESULT_PRICE : 'Price',
    SEARCH_RESULT_COUNTRY : 'Country',
    SEARCH_RESULT_FOR_SALE : 'Sale',
    SEARCH_RESULT_FOR_RENT : 'Rent',
    SEARCH_RESULT_SERVICE_PRICE : 'Services',
    SEARCH_RESULT_FOR_FORECLOSURE : 'Foreclosure',
    SEARCH_RESULT_FOR_AREA : 'Area',
    SEARCH_RESULT_FOR_AREA_LOTE : 'Lot size',
    SEARCH_RESULT_FOR_ROOM : 'br',
    SEARCH_RESULT_FOR_BATH : 'ba',
    SEARCH_RESULT_BOTH : 'For sale / rent',
    SEARCH_RESULT_MORE_INFO : 'More information',
    SEARCH_RESULT_FOR : 'For',
    SEARCH_RESULT_GOTO_PROFILE: 'Go to Profile',
    SEARCH_RESULT_TO: 'to',
    SEARCH_RESULT_OF: 'of',
    SEARCH_RESULT_PROPERTY: 'property(ies)',
    SEARCH_LABEL_MILLION : 'Million',
    SEARCH_RESULT_LABEL: 'Results',
    SEARCH_PROPERTY_TYPE : 'Property type', 
    SEARCH_ROOM_TYPE : 'Room type',
    SEARCH_PROPERTY_LISTING : 'Listing type',
    SEARCH_PROPERTY_ALL : 'All',
    SEARCH_LABEL_ROOM : 'Room',
    LABEL_RENT : 'Rent',
    LABEL_SALE : 'Sale',
    LABEL_FORECLOSURE : 'Foreclosure',   

    // Filters
    FILTER_ONLY_NUMBERS: 'Enter only numbers',

    // IMAGENES
    AD_NO_IMAGE : '/img/en/ad_no_image.jpg',
    AD_NO_IMAGE_RESULT : '/img/en/ad_no_image_result.jpg',
    AD_NO_IMAGE_OUTSTANDING : '/img/en/no_photoDestacada.jpg',
    FEATURE_IMAGE_BACKGROUND : '/img/en/overlay_featured_green.png',

    // Compartir
    DLG_SHARE_PROPERTY_TITLE : 'Add your comment',

    // Cuadro de dialogo Invitar amigos
    DLG_INVITE_FRIENDS_TITLE : 'Invite your friends to Housemarket',
    DLG_INVITE_FRIENDS_CONTENT : 'Housemarket is the best application to find homes, apartments, vacation rentals, lots and rooms posted by Facebook users.',
    DLG_INVITE_FRIENDS_ACTION_TEXT : 'Invite your friends to join Housemarket.',
    DLG_INVITE_FRIENDS_ACCEPT_LABEL : 'Yes',

    // Cuadro de dialogo Reportar Imagenes Ofensivas
    DLG_REPORT_ABUSE_TITLE : 'Tell us what you think',
    DLG_REPORT_ABUSE_TYPE_LABEL : 'What do you think of this property?',
    DLG_REPORT_ABUSE_COMMENT_LABEL : 'Comment',
    DLG_REPORT_ABUSE_TYPE_VALUE_SELECT : 'Please select',
    DLG_REPORT_ABUSE_TYPE_VALUE_0 : 'Miscategorized',
    DLG_REPORT_ABUSE_TYPE_VALUE_1 : 'Illegal or inappropriate content',
    DLG_REPORT_ABUSE_TYPE_VALUE_2 : 'Spam or fraud',
    DLG_REPORT_ABUSE_TYPE_VALUE_3 : 'It seems suspicious',
    DLG_REPORT_ABUSE_TYPE_VALUE_4 : 'Unavalaible',
    DLG_REPORT_ABUSE_BUTTON_REPORT : 'Report',

    // Editar Cliente
    EDIT_CLIENT_WARNING : 'Warning',
    EDIT_CLIENT_NAME_REQUIRED : 'Name Required',
    EDIT_CLIENT_EMAIL_REQUIRED : 'Email Required',
    EDIT_CLIENT_EMAIL_INVALID : 'Your email is not valid',

     //Roommate
    SEARCH_RESULT_ROOMMATE: 'Roommate(s)',

    //Room
    SEARCH_RESULT_ROOM : 'Room(s)',
    INFO_ROOM_INCLUDED_SERVICES : "Services Included",
    
    // Varios
    MSG_EMAIL_SENT : "Email sent.",
    MSG_ERROR_ZIP : "Zip code is too long. Zip Code must be between 1 and 64 characters",
    
    // Código estándar de moneda
    ISO_4217_CURRENCY_CODE_USD : "USA Dollar",
    ISO_4217_CURRENCY_CODE_COP : "Colombian peso",
    
    // Carga masiva
    LISTING_UPLOAD_CLICK_HERE_TO_SHOW_ERROR_DETAILS : "Some errors were found. Click here to show the details.",
    LISTING_UPLOAD_CLICK_HERE_TO_HIDE_ERROR_DETAILS : "Some errors were found. Click here to hide the details.",
    
    //Roommates
    LBL_MIN_AGE : "Minimum Age",
    LBL_AGE : "Age",
    MSG_ERROR_HOSTING_RMT : "Enter valid hosting",
    MSG_ERROR_PROPERTY_TYPE_RMT : "Enter a valid type of property",
    MSG_ERROR_STATE_ROOM : "Enter valid state",
    MSG_ERROR_CAPACITY_RMT : "Enter maximum capacity",
    MSG_ERROR_AVAILABLE_SPACE_RMT : "Enter number of available space",
    MSG_ERROR_LENGTH_AVAILABLE_SPACE : "Available space is greater than maximum capacity",
    MSG_ERROR_TYPE_ROOM : "Enter a valid type for room",
    MSG_ERROR_BATH_RMT : "Enter a valid type of bath",
    MSG_ERROR_RENT_RMT : "Rent is required",
    MSG_ERROR_QUANTITY_MONTH_RMT : "Especify quantity of month",
    MSG_ERROR_AMOUNT_SERVICE_RMT : "Specify an amount for services",
    MSG_ERROR_DATE_RMT : "Enter a valid date",
    MSG_ERROR_TITLE_RMT : "Enter a valid title",
    MSG_ERROR_COUNTRY_RMT : "Country name is too long. Country name must be between 1 and 128 characteres",
    MSG_ERROR_STATE_RMT : "State name is too long. State name must be between 1 and 64 characteres",
    MSG_ERROR_CITY_RMT : "City name is too long. City name must be between 1 and 64 characteres",
    MSG_ERROR_ADDRESS_RMT : "Put the icon in are you area interested",
    MSG_ERROR_ADDRES_LENGTH_RMT : "Address is too long. Address must be between 1 and 255 characters",
    MSG_ERROR_EMAIL_RMT : "Enter a valid email address",
    MSG_ERROR_EMAIL_VALID_RMT : "You need to provide a valid email",
    MSG_ERROR_DESCRIPTION_RMT : "Description is too long, please enter a title equal or less than 512 characters",
    MSG_ERROR_MAXIMUM_AGE_RMT : "Enter valid Maximum Age you accept",
    MSG_ERROR_MINIMUM_AGE_RMT : "Enter valid Minimun Age you accept",
    MSG_ERROR_MIN_AGE_LESSER_MAX_RMT : "Verify range age you are looking for",
    MSG_ERROR_OCUPATION_RMT : "Enver a valid occupation for accept",
    MSG_ERROR_GENDER_RMT : "Enter a valid gender",
    MSG_ERROR_COUPLE_RMT : "Enter a valid couple selection",
    MSG_ERROR_CHILDREN_RMT : "Enter valid children you accept",
    MSG_ERROR_PET_RMT : "Enter a valid pet option you accept",
    MSG_ERROR_SMOKER_RMT : "Enter a valid smoker option you accept",
    MSG_ERROR_MINIMUM_AGE_HAVE_RMT : "Enter valid Minimun Age you have",
    MSG_ERROR_MIN_AGE_LESSER_MAX_HAVE_RMT : "Verify range age you have",
    MSG_ERROR_MAXIMUM_AGE_HAVE_RMT : "Enter valid maximum age you have",
    MSG_ERROR_SMOKER_HAVE_RMT : "Enter a valid smoker option you have",
    MSG_ERROR_LANGUAGE_HAVE_RMT : "Enter a valid language",
    MSG_ERROR_OCUPATION_HAVE_RMT : "Enter valid occupation you have",
    MSG_ERROR_GENDER_HAVE_RMT : "Enter a valid gender you have",
    MSG_ERROR_PET_HAVE_RMT : "Enter a valid pet option you have",
    MSG_ERROR_CHILDREN_HAVE_RMT : "Enter a valid children option you have",
    MSG_ERROR_STAY_MONTH_RMT : "Especify cantity of month",
    MSG_ERROR_TYPE_SEARCH_RMT : "Type of search is required",
    MSG_ERROR_FURNITURE_SEARCH_RMT : "Enter valid for furniture",
    MSG_QUANTITY_SEARCH_RMT : "Enter quantity search"
};