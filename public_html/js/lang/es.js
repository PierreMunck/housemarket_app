var i18n = {
    // Pagina Index
    // Google Map Messages
    G_GEO_SUCCESS : 'Éxito',
    G_GEO_MISSING_ADDRESS : 'Dirección Pérdida: la dirección proveída se perdió',
    G_GEO_UNKNOWN_ADDRESS : 'Dirección Desconocida: La localización geográfica no puede ser encontrada con la dirección proveída.',
    G_GEO_UNAVAILABLE_ADDRESS : 'Dirección no disponible: las coordenadas geográficas para la dirección proveída no pueden ser mostradas, por razones legales o contractuales.',
    G_GEO_BAD_KEY : 'LLave errónea: la llave API es inválida o no corresponde a la generada por este dominio.',
    G_GEO_TOO_MANY_QUERIES : 'Muchas consultas: el número de consultas al geocodificador han excedido la cuota para este sitio.',
    G_GEO_SERVER_ERROR : 'Error del Servidor: la petición no podría ser procesada exitosamente por el servidor.',
    G_GEO_ALERT_TITLE : 'Advertencia',
    G_GEO_ALERT_MESSAGE_FAIL : 'Servicio geocodificador no puede ser contactado.',
    G_GEO_ALERT_MESSAGE_AMBIGUOUS :  'La dirección dada es ambigua. Por favor indique una referencia más detallada de esta dirección para ejecutar la búsqueda exitosamente.',
    G_GEO_ALERT_MESSAGE_NOT_FOUND : ' no encontrada',
    G_GEO_ALERT_GOOGLE_NOT_SUPPORTED :  'Su navegador web no soporta Google Maps',
    G_GEO_ALERT_STATUS_CODE : 'Código de Estado : ',
   
    // Tooltip
    TOOLTIP_ENTER_SEARCH_TEXT : 'Introduzca una ciudad, vecindario, zip, país, número ID',
    TOOLTIP_ENTER_SEARCH_TITLE : 'Ayuda',

    // SEARCH SAMPLE
    TEXT_SEARCH_SAMPLE : 'Ejemplo: Miami, USA',

    //PROPERTIES
    DLG_TERMS_CONDITIONS_CONTENT : 'Por favor acepte Términos y condiciones de uso',
    DLG_TERMS_CONDITIONS_TITLE : 'Términos y condiciones',

    // SEARCH RESULT
    ALERT_MESSAGE_NOT_FILTER: 'Por favor seleccione un tipo de propiedad y de enlistamiento',
    SEARCH_RESULT_STATUS : 'Estado',
    SEARCH_RESULT_DESCRIPTION : 'Descripción',
    SEARCH_RESULT_PRICE : 'Precio',
    SEARCH_RESULT_COUNTRY : 'País',
    SEARCH_RESULT_FOR_SALE : 'Venta',
    SEARCH_RESULT_FOR_AREA : 'Area',
    SEARCH_RESULT_FOR_AREA_LOTE : 'Area de Lote',
    SEARCH_RESULT_FOR_ROOM : 'br',
    SEARCH_RESULT_FOR_BATH : 'ba',
    SEARCH_RESULT_FOR_RENT : 'Renta',
    SEARCH_RESULT_SERVICE_PRICE : 'Servicios',
    SEARCH_RESULT_FOR_FORECLOSURE : 'Remate',
    SEARCH_RESULT_BOTH : 'En venta / renta',
    SEARCH_RESULT_MORE_INFO : 'Más información',
    SEARCH_RESULT_FOR : 'En',
    SEARCH_RESULT_GOTO_PROFILE: 'Ir al perfil',
    SEARCH_RESULT_TO: 'al',
    SEARCH_RESULT_OF: 'de',
    SEARCH_RESULT_PROPERTY: 'proppiedad(es)',
    SEARCH_RESULT_LABEL: 'Resultados',
    
    
    LABEL_RENT : 'Renta',
    LABEL_SALE : 'Venta',
    LABEL_FORECLOSURE : 'Remate',
    
    SEARCH_LABEL_MILLION : 'Millón',
    SEARCH_PROPERTY_TYPE : 'Tipo de propiedad',
    SEARCH_ROOM_TYPE : 'Tipo de cuarto',
    SEARCH_PROPERTY_LISTING : 'Tipo de enlistamiento', 
    SEARCH_PROPERTY_ALL : 'Todos',
    SEARCH_LABEL_ROOM : 'Cuartos', 
    // Filters
    FILTER_ONLY_NUMBERS: 'Introduzca sólo números',

    // IMAGENES
    AD_NO_IMAGE : '/img/es/ad_no_image.jpg',
    AD_NO_IMAGE_RESULT : '/img/es/ad_no_image_result.jpg',
    AD_NO_IMAGE_OUTSTANDING : '/img/es/no_photoDestacada.jpg',
    FEATURE_IMAGE_BACKGROUND : '/img/es/overlay_featured_green.png',

    // Compartir
    DLG_SHARE_PROPERTY_TITLE : 'Comentarios',

    // Cuadro de dialogo Invitar amigos
    DLG_INVITE_FRIENDS_TITLE : 'Invita a tus amigos a Housemarket',
    DLG_INVITE_FRIENDS_CONTENT : 'Housemarket es la mejor aplicación para encontrar casas, apartamentos, rentas vacacionales, terrenos y cuartos publicados por usuarios de Facebook.',
    DLG_INVITE_FRIENDS_ACTION_TEXT : 'Invita a tus amigos a unirse a Housemarket.',
    DLG_INVITE_FRIENDS_ACCEPT_LABEL : 'Si',

    // Cuadro de dialogo Reportar Imagenes Ofensivas
    DLG_REPORT_ABUSE_TITLE : 'Danos tu opinión',
    DLG_REPORT_ABUSE_TYPE_LABEL : '¿Qué opinas de esta propiedad?',
    DLG_REPORT_ABUSE_COMMENT_LABEL : 'Comentarios',
    DLG_REPORT_ABUSE_TYPE_VALUE_SELECT : 'Por favor, seleccione',
    DLG_REPORT_ABUSE_TYPE_VALUE_0 : 'Mal categorizado',
    DLG_REPORT_ABUSE_TYPE_VALUE_1 : 'Contenido ilegal o inapropiado',
    DLG_REPORT_ABUSE_TYPE_VALUE_2 : 'Spam o fraude',
    DLG_REPORT_ABUSE_TYPE_VALUE_3 : 'Se ve sospechoso',
    DLG_REPORT_ABUSE_TYPE_VALUE_4 : 'No disponible',
    DLG_REPORT_ABUSE_BUTTON_REPORT : 'Reportar',

    // Editar Cliente
    EDIT_CLIENT_WARNING : 'Advertencia',
    EDIT_CLIENT_NAME_REQUIRED : 'Nombre requerido',
    EDIT_CLIENT_EMAIL_REQUIRED : 'Email requerido',
    EDIT_CLIENT_EMAIL_INVALID : 'Tu email no es valido',

    //Roommate
    SEARCH_RESULT_ROOMMATE: 'Compañero(s)',

    //Room
    SEARCH_RESULT_ROOM : 'Cuarto(s)',
    INFO_ROOM_INCLUDED_SERVICES : "Servicios Incluídos",
    
    // Varios
    MSG_EMAIL_SENT : "Correo enviado.",
    MSG_ERROR_ZIP : "Zip code es demasiado largo. Debe ser entre 1 y 64 caracteres",
    
    // Código estándar de moneda
    ISO_4217_CURRENCY_CODE_USD : "Dólar USA",
    ISO_4217_CURRENCY_CODE_COP : "Peso colombiano",
    
    // Carga masiva
    LISTING_UPLOAD_CLICK_HERE_TO_SHOW_ERROR_DETAILS : "Se encontraron algunos errores. Haz clic aquí para mostrarlos.",
    LISTING_UPLOAD_CLICK_HERE_TO_HIDE_ERROR_DETAILS : "Se encontraron algunos errores. Haz clic aquí para ocultarlos.",
    
    //Roommates
    LBL_MIN_AGE : "Edad Mínima",
    LBL_AGE : "Edad",
    MSG_ERROR_HOSTING_RMT : "Introducir un tipo valido de alojamiento",
    MSG_ERROR_PROPERTY_TYPE_RMT : "Introducir un tipo válido de propiedad",
    MSG_ERROR_STATE_ROOM : "Introducir un tipo válido para el estado del cuarto",
    MSG_ERROR_CAPACITY_RMT : "Introducir capacidad máxima",
    MSG_ERROR_AVAILABLE_SPACE_RMT : "Introducir número disponible de espacios",
    MSG_ERROR_LENGTH_AVAILABLE_SPACE : "Espacio disponible no debe ser mayor a capacidad máxima",
    MSG_ERROR_TYPE_ROOM : "Introducir un tipo válido de cuarto",
    MSG_ERROR_BATH_RMT : "Introducir el tipo de baño",
    MSG_ERROR_RENT_RMT : "Renta es requerida",
    MSG_ERROR_QUANTITY_MONTH_RMT : "Espicificar la cantidad de meses",
    MSG_ERROR_AMOUNT_SERVICE_RMT : "Especificar un monto para los servicios",
    MSG_ERROR_DATE_RMT : "Introducir una fecha válida",
    MSG_ERROR_TITLE_RMT : "Introduzca un título",
    MSG_ERROR_COUNTRY_RMT : "Nombre del país muy largo. Debe ser entre 1 y 128 caracteres",
    MSG_ERROR_STATE_RMT : "Nombre de Estado muy largo. Debe ser entre 1 y 64 caracteres",
    MSG_ERROR_CITY_RMT : "Nombre de ciudad muy largo. Debe de ser entre 1 y 64 caracteres",
    MSG_ERROR_ADDRESS_RMT : "Arrastre el ícono para establecer la ubicación de la propiedad",
    MSG_ERROR_ADDRES_LENGTH_RMT : "Dirección es muy larga. Debe de ser entre 1 y 255 caracters",
    MSG_ERROR_EMAIL_RMT : "Introduzca un correo",
    MSG_ERROR_EMAIL_VALID_RMT : "Introduzca un correo válido",
    MSG_ERROR_DESCRIPTION_RMT : "Descripción muy larga, por favor introduzca un titulo menor o igual a 512 caracteres",
    MSG_ERROR_MAXIMUM_AGE_RMT : "Introduzca máxima edad que usted acepta",
    MSG_ERROR_MINIMUM_AGE_RMT : "Introduzca la edad mínima que usted acepta",
    MSG_ERROR_MIN_AGE_LESSER_MAX_RMT : "Verifique rango de edad que busca",
    MSG_ERROR_OCUPATION_RMT : "Introduzca una ocupación válida que usted acepta",
    MSG_ERROR_GENDER_RMT : "Introduzca el género a aceptar",
    MSG_ERROR_COUPLE_RMT : "Por favor seleccione un valor para pareja",
    MSG_ERROR_CHILDREN_RMT : "Introduzca un valor para opción hijos que acepta",
    MSG_ERROR_PET_RMT : "Introduzca un valor para opción mascota que acepta",
    MSG_ERROR_SMOKER_RMT : "Introduzca un valor para opción fumador que acepta",
    MSG_ERROR_MINIMUM_AGE_HAVE_RMT : "Introduzca la edad mínima que usted tiene",
    MSG_ERROR_MIN_AGE_LESSER_MAX_HAVE_RMT : "Verífique su rango de edad en su información",
    MSG_ERROR_MAXIMUM_AGE_HAVE_RMT : "Introduzca la edad máxima que usted tiene",
    MSG_ERROR_SMOKER_HAVE_RMT : "Introduzca el tipo de fumador que tiene",
    MSG_ERROR_LANGUAGE_HAVE_RMT : "Introduzca un idioma",
    MSG_ERROR_OCUPATION_HAVE_RMT : "Introduzca la ocupación que tiene",
    MSG_ERROR_GENDER_HAVE_RMT : "Introduzca el género al que pertenece",
    MSG_ERROR_PET_HAVE_RMT : "Por favor seleccione un valor para mascota que tiene",
    MSG_ERROR_CHILDREN_HAVE_RMT : "Por favor seleccione un valor para hijos que habitan",
    MSG_ERROR_STAY_MONTH_RMT : "Específique la cantidad de meses de estadía",
    MSG_ERROR_TYPE_SEARCH_RMT : "Tipo de búsquedad es requerido",
    MSG_ERROR_FURNITURE_SEARCH_RMT : "Seleccione un valor para amueblado",
    MSG_QUANTITY_SEARCH_RMT : "Seleccione la cantidad que busca"
};