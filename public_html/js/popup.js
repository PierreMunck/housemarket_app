function __translate(s) {
    if (typeof(i18n)!='undefined' && i18n[s]) {
        return i18n[s];
    }
    return s;
}
function fillTextArea(){
    var myCategory = $("#cbCategoria option:selected").text();
    var myType = $("#type option:selected").text();
    var myRoom = $("#room option:selected").text();
    var myPriceMin = $("#minprice option:selected").text();
    var myPriceMax = $("#maxprice option:selected").text();
    var myAreaMin = $("#minarea option:selected").text();
    var myAreaMax = $("#maxarea option:selected").text();    
    var texto = __translate("SEARCH_PROPERTY_TYPE") + " : " + myCategory + "\r";
    if($("#cbCategoria option:selected").val() != 11)
        texto += __translate("SEARCH_LABEL_ROOM")+" : " + myRoom + "\r";
    texto += __translate("SEARCH_PROPERTY_LISTING")+" : " + myType + "\r";
    if($("#type option:selected").val() != ""){
        if(myPriceMin != "Min")
            myPriceMin = "U$ "+myPriceMin;
        if(myPriceMax != "Max")
            myPriceMax = "U$ "+myPriceMax;
    
        texto += __translate("SEARCH_RESULT_PRICE") + " : " + myPriceMin + " - " + myPriceMax  + "\r";
    }
    texto += __translate("SEARCH_RESULT_FOR_AREA") + " : " + myAreaMin + " - "+ myAreaMax+ "\r";
    $("#inquiry").text(texto);   
}

//CONTROLLING EVENTS IN jQuery
$(document).ready(function(){
    
    fillTextArea();
    
    $("#isli,#cancelar").click(function(){      
        //$("#contact").slideDown('slow');
        $("#contact").animate({
                opacity: 1,
                height: 'toggle',
                marginBottom:'10'
            },'slow');
    });
    /*    
    $("#cancelar").click(function(event){    

        $("#mierror").slideUp(100);
        $("#contact").slideUp(500);

       
    } 
    );
    */
  
    $("#enviar").click(function(event) {  
        var name = $.trim($("#name_c").val());
        var email = $("#email_c").val();    
        var inquire = $.trim($("#inquiry").val());  
        
        if (name==""){
            $("#mierror").text("Name is empty");
            $("#mierror").slideDown(200);
            event.preventDefault(); 
        }else if (email==""){
            event.preventDefault(); 
            document.getElementById("mierror").innerHTML = "Email is empty";
            $("#mierror").slideDown(200);
            event.preventDefault(); 
        }else if (inquire && inquire == ""){
            document.getElementById("mierror").innerHTML = "Inquire is empty";
            $("#mierror").slideDown(200);
            event.preventDefault(); 
        }else
        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){       
            document.getElementById("mierror").innerHTML = "Email is not valid";
            $("#mierror").slideDown(200);        
            event.preventDefault();  
        }else
          
            $("#mierror").slideUp(100);
        
  
    });
  

});