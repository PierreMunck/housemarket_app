function __translate(s){
    if(typeof(i18n)!='undefined'&&i18n[s]){
        return i18n[s];
    }
    return s;
}
function __environment(s){
    if(typeof(env)!='undefined'&&env[s]){
        return env[s];
    }
    return s;
}
function getDialog(title,fbmlContent,opts){
    var dlg={
        method:'fbml.dialog',
        fbml:fbmlContent
    }
    return dlg;
}
function getInviteFriendDialog(title,content,responseUrl,requestOpts,selectorOpts){
    var selector='<fb:multi-friend-selector target="_top" style="width:80%;" ';
    if(selectorOpts){
        if(selectorOpts.bypass){
            selector+=' bypass="'+selectorOpts.bypass+'"';
        }
        if(selectorOpts.target){
            selector+=' target="'+selectorOpts.target+'"';
        }
        if(selectorOpts.emailInvite){
            selector+=' email_invite="'+selectorOpts.emailInvite+'"';
        }
        if(selectorOpts.rows){
            selector+=' rows="'+selectorOpts.rows+'"';
        }
        if(selectorOpts.cols){
            selector+=' cols="'+selectorOpts.cols+'"';
        }
        if(selectorOpts.showborder){
            selector+=' showborder="'+selectorOpts.showborder+'"';
        }
        if(selectorOpts.actiontext){
            selector+=' actiontext="'+selectorOpts.actiontext+'"';
        }
    }
    selector+='></fb:multi-friend-selector>'
    var requestForm='<fb:request-form target="_top" style="width:80%;" ';
    if(requestOpts){
        if(requestOpts.invite){
            requestForm+=' invite="'+requestOpts.invite+'"';
        }
        if(requestOpts.method){
            requestForm+=' method="'+requestOpts.method+'"';
        }
        if(requestOpts.action){
            requestForm+=' action="'+requestOpts.action+'"';
        }
        if(requestOpts.type){
            requestForm+=' type="'+requestOpts.type+'"';
        }
    }
    requestForm+=' content="'+content+'<fb:req-choice url=\''+responseUrl+'\' label=\''+__translate('DLG_INVITE_FRIENDS_ACCEPT_LABEL')+'\' />">'+'<input id=\'first_overloaded_id\' type=\'hidden\' fb_protected=\'true\' value=\'first_overloaded_value\' name=\'first_overloaded_name\'/>'+'<input id=\'second_overloaded_id\' type=\'hidden\' fb_protected=\'true\' value=\'second_overloaded_value\' name=\'second_overloaded_name\'/>'+
    selector+'</fb:request-form>';
    var fbmlContent=requestForm;
    var dlg={
        method:'fbml.dialog',
        display:'dialog',
        fbml:"<div style='width:300px; border: solid #000 1px'>"+fbmlContent+"</div>"
    }
    return dlg;
}
function getReportAbuse(url){
    var title=__translate('DLG_REPORT_ABUSE_TITLE');
    var header=__translate('DLG_REPORT_ABUSE_TYPE_LABEL');
    function _GetParametersListURL(){
        var _parameters={};
        
        var _6=facebook_perms.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,_index,_value){
            _parameters[_index]=unescape(_value);
        });
        return _parameters;
    };
    
    var _parametersList=_GetParametersListURL();
    var fb=Ext.urlDecode(Ext.urlEncode(_parametersList));
    var _urlParameters=unescape(Ext.urlEncode(_parametersList));
    var content='<fb:header icon="false" decoration="add_border">'+title+'</fb:header>'+'<fb:fbml>'+'<fb:editor action="'+__environment('canvas_url')+'documentos/report?'+_urlParameters+'" labelwidth="230">'+'<fb:editor-custom label="'+header+'">'+'<select name="state">'+'<option value="" selected>'+__translate('DLG_REPORT_ABUSE_TYPE_VALUE_SELECT')+'</option>'+'<option value="1">'+__translate('DLG_REPORT_ABUSE_TYPE_VALUE_0')+'</option>'+'<option value="2">'+__translate('DLG_REPORT_ABUSE_TYPE_VALUE_1')+'</option>'+'<option value="3">'+__translate('DLG_REPORT_ABUSE_TYPE_VALUE_2')+'</option>'+'<option value="4">'+__translate('DLG_REPORT_ABUSE_TYPE_VALUE_3')+'</option>'+'</select>'+'<input type="hidden" name="url" value="'+url+'" />'+'</fb:editor-custom>'+'<fb:editor-textarea label="'+__translate('DLG_REPORT_ABUSE_COMMENT_LABEL')+'" name="comment"/>'+'<fb:editor-buttonset>'+'<fb:editor-button value="'+__translate('DLG_REPORT_ABUSE_BUTTON_REPORT')+'"/>'+'</fb:editor-buttonset>'+'</fb:editor>'+'</fb:fbml>';
    var req={
        method:'fbml.dialog',
        display:'dialog',
        fbml:(content),
        width:'300px',
        height:'100px'
    }
    FB.ui(req);
}
function ShowInviteFriendDialog(){
    var title=__translate('DLG_INVITE_FRIENDS_TITLE');
    var content=__translate('DLG_INVITE_FRIENDS_CONTENT');
    var responseUrl=__environment('facebook_url');
    var requestOpts={
        invite:'false',
        method:'POST',
        action:__environment('facebook_url'),
        type:'Housemarket'
    };
    
    var selectorOpts={
        bypass:'cancel',
        target:'_top',
        emailInvite:'false',
        rows:5,
        cols:3,
        showborder:false,
        actiontext:__translate('DLG_INVITE_FRIENDS_ACTION_TEXT')
    };
        
    FB.ui({
        method:'apprequests',
        message:__translate('DLG_INVITE_FRIENDS_TITLE'),
        data:'tracking information for the user'
    });
}
function ShowBookmarkDialog(){
    FB.ui({
        method:'bookmark.add'
    });
}

// Share button functionality: call from perfil.js
function postToFeed(obj, callback) {
    obj.method = 'feed';
    FB.ui(obj, callback);
}
      
function StreamPublish(data){
    var attachment={};
    attachment.name=(data.name)?data.name:'Housemarket';
    if(data.url){
        attachment.href=data.url;
    }
    if(data.caption){
        attachment.caption=data.caption;
    }
    if(data.description){
        attachment.description=data.description;
    }
    if(data.properties){
        attachment.properties=data.properties;
    }
    if(data.media){
        attachment.media=data.media;
    }
        
    var publish={
        method:'stream.publish',
        message:(data.message)? data.message : '',
        attachment:attachment
    };
    if(data.action_links){
        publish.action_links=data.action_links;
    }
    FB.ui(publish);
}


Ext.onReady(function(){
	var fb_like_bt=$("#facebook-like-factice-button");
	var html = communityButtonTemplates('facebook', $(this).attr('rel'), $(this).attr('title'));
	fb_like_bt.html(html);
});

/**
 * 
 * Préparation des boutons facebook et twitter en asynchrone.
 */
var loadRealCommunityButton = function(){
  
  // Chargement des boutons Facebook.
  $('span.facebook-like-factice-button').each(function(key, item){
    
    
    $(item).hide();
    $(item).before(html);
    $(item).remove();
  });
  
  // Chargement des boutons Twitter.
  $('span.twitter-like-factice-button').each(function(key, item){
    var html = communityButtonTemplates('twitter', $(this).attr('rel'), $(this).attr('title'));
    
    $(item).hide();
    $(item).before(html);
    $(item).remove();
  });
  
}

/**
 * Templates pour les boutons facebook et twitter.
 * @param string button
 * @param string rel
 * @returns string html;
 */
var communityButtonTemplates = function (button, rel, title){
  var  html = '';
  switch(button) {
    case 'facebook':
      html += '<iframe src="https://www.facebook.com/plugins/like.php?href='+ rel +'&amp;layout=button_count&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=80"';
      html += ' frameborder="no"';
      html += ' scrolling="0"';
      html += ' allowTransparency="true"';
      html += ' style="border:none; overflow:hidden; width:450px; height:20px;margin-bottom: -3px;margin-top: 3px;"';
      html += ' ></iframe>';
    break;
    case 'twitter':
      html += '<iframe allowtransparency="true" frameborder="0" scrolling="no"';
      html += ' src="https://platform.twitter.com/widgets/tweet_button.html?url='+ rel +'&count=horizontal&via=le_figaro&lang=fr&text='+ title +'"';
      html += ' style="width:130px; height:20px;"></iframe>';
    break;
  }
  return html;
}