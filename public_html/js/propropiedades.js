/* 
 * Grid para el listado de aprobación de créditos del cliente
 *
 */


Ext.onReady(function(){
    Ext.BLANK_IMAGE_URL = '/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = "side";


    

    function isEmpty(inputStr) {
        if (null == inputStr || "" == inputStr) {
            return true;
        }
        return false;
    }

    /*var bus = Ext.get('_filtro');
    bus.on('click', function(){
        propbuscar();
    })
   */var cbEstado = Ext.get("cbEstado");
   var cbCategoria  = Ext.get("cbCategoria");
    
    cbEstado.on('change',function(){
         propbuscar();
    })
     cbCategoria.on('change',function(){
         propbuscar();
    })
    function propbuscar() {
        var dataurl = Ext.Ajax.serializeForm('pp');
        var data = Ext.urlDecode(dataurl);
        var Estado=data.cbEstado;
        var Categoria=data.cbCategoria;

        getProPropiedad(Estado,Categoria);
    }

    function getProPropiedad(Estado,Categoria){
        clienteDataStore.load({
            params: {
                start:0,
                'Estado': Estado,
                'Categoria': Categoria
            }
        });
    }

    var urlsitio=document.getElementById('_urlsite').value;
    var urlpropiedades= urlsitio + "mihousebook/publicacion";

    function verperfilPropiedad () {
        var sm = dtgridCliente.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            window.top.location=urlsitio + "enlistar/perfil?valprop=" + sel.data.CodigoPropiedad;
        } else {
            var msj=document.getElementById('select_property').value;
            var encab=document.getElementById('_alert').value;
            Ext.Msg.alert(encab, msj);
        }
    }

    function editarPropiedad () {
        var sm = dtgridCliente.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            window.top.location=urlsitio + "propiedades/show?id=" + sel.data.CodigoPropiedad;
        } else {
            var msj=document.getElementById('select_property').value;
            var encab=document.getElementById('_alert').value;
            Ext.Msg.alert(encab, msj);
        }
    }

    function eliminarPropiedad () {
       
        var sm = dtgridCliente.getSelectionModel();
        if(sm.hasSelection()){
            if(confirm(document.getElementById('_confirm').value)){
                //var sel = sm.getSelected();
                var codes=new Array();
                dtgridCliente.getSelectionModel().each(function(record){
                    codes.push(record.get('CodigoPropiedad'));                    
                });
                
                Ext.Ajax.request({
                    url: '/mihousebook/eliminarpropropiedad',
                    method: 'POST',
                    params: {
                        'CodigoPropiedad': codes.join(',')
                        
                    },
                    success: function (result) {
                        var data;
                        data = Ext.decode(result.responseText);
                        if (data.success === true) {
                            dtgridCliente.getStore().reload();
                        } else {
                            Ext.MessageBox.alert($('_alert').val(), $('_error').val());
                        }
                    //alert("Test Correct");
                    //
                    },
                    failure: function (result, response) {
                        var msj=document.getElementById('_error').value;
                        var encab=document.getElementById('_alert').value;
                        Ext.Msg.alert(encab, msj);
                    }
                });
                
            }
        } else {
            var msj=document.getElementById('select_property').value;
            var encab=document.getElementById('_alert').value;
            Ext.Msg.alert(encab, msj);
        }
    }

    function publicarPropiedad () {
        var sm = dtgridCliente.getSelectionModel();

        if(sm.hasSelection()){
            //var  arrayRecords = dtgridCliente.getSelectionModel().getSelections();
            var  CodigoPropiedad = '';
            dtgridCliente.getSelectionModel().each(function(record){
                var tmp = record.get('CodigoPropiedad');
                CodigoPropiedad+=tmp+',';
            });
            Ext.Ajax.request({
                url: '/mihousebook/publicarpropropiedad',
                params: {
                    'CodigoPropiedad':CodigoPropiedad
                },
                success: function (result, response) {
                    dtgridCliente.getStore().reload();
                //window.top.location=urlpropiedades;
                },
                failure: function (result, response) {
                    var msj=document.getElementById('_error').value;
                    var encab=document.getElementById('_alert').value;
                    Ext.Msg.alert(encab, msj);
                }
            });
           
        } else {
            var msj=document.getElementById('select_property').value;
            var encab=document.getElementById('_alert').value;
            Ext.Msg.alert(encab, msj);
        }
    }

    function nopublicarPropiedad () {
        var sm = dtgridCliente.getSelectionModel();
        if(sm.hasSelection()){
            //var sel = sm.getSelected();
            var CodigoPropiedad = '';//sel.data.CodigoPropiedad;
            dtgridCliente.getSelectionModel().each(function(record){
                var tmp = record.get('CodigoPropiedad');
                CodigoPropiedad+=tmp+',';
            });
            Ext.Ajax.request({
                url: '/mihousebook/nopublicarpropropiedad',
                params: {
                    'CodigoPropiedad': CodigoPropiedad
                },
                success: function (result, response) {
                    dtgridCliente.getStore().reload();
                },
                failure: function (result, response) {
                    var msj=document.getElementById('_error').value;
                    var encab=document.getElementById('_alert').value;
                    Ext.Msg.alert(encab, msj);
                }
            });
        } else {
            var msj=document.getElementById('select_property').value;
            var encab=document.getElementById('_alert').value;
            Ext.Msg.alert(encab, msj);
        }
    }

    var addcreditform = new Ext.FormPanel({
        bodyStyle : "padding: 10px 4px 5px 4px;",
        url: '/mihousebook/addcredit',
        buttonAlign: "center",
        id: 'status-form',
        renderTo: Ext.getBody(),
        width: 200,
        frame:false,
        height: 100,
        items: [new Ext.Panel({ 
            bodyStyle : "margin: 5px auto;",
            border:false,
            defaultType: 'textfield',
            items: [
            {
                xtype:'label',
                text:$("#qty").val(),
                selectOnFocus: true
            },
            {
                xtype:'hidden',
                name: 'propiedad'
            },            
            {               
                name:'quantity',
                width:70
            },{
                xtype:'label',
                text:' x 10 '
            }
            ]
        })],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                Ext.MessageBox.confirm('Confirm', 'Are you sure you want to do that?', function(btn){
                    if(btn=='yes'){
                        addcreditform.getForm().submit({
                            waitMsg: 'Processing...',                    
                            success: function(f,a){
                        
                                $("#creditDisponible").html(a.result.credito);
                                addcreditform.getForm().reset();
                                win.hide();
                        
                                clienteDataStore.reload();
                            
                       
                            },
                            failure: function(f,a){
                                //console.log(a.result.data);
                                Ext.MessageBox.show({
                                    title: 'Credits',
                                    msg: a.result.msg,
                                    buttons: Ext.MessageBox.OK,                           
                                    icon: Ext.MessageBox.ERROR
                                });
                            }
                        });
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                win.hide();
            }
        }]

    });
    var win = new Ext.Window({
        title: $("#addcredits").val(),
        applyTo: 'win',
        layout: 'fit',
        height: 'auto',
        modal: true,
        closeAction: 'hide',
        items:addcreditform
    });
    function addCredits(){
        var sm = dtgridCliente.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            var propiedad =sel.data.CodigoPropiedad;    
            addcreditform.getForm().findField('propiedad').setValue(propiedad);
            addcreditform.getForm().findField('quantity').setValue(1);
            win.show();
            
        } else {
            var msj=$('#select_property').val();
            var encab=$('_alert').val();
            Ext.Msg.alert(encab, msj);
        }   
    }

    var proptoolbar = new Ext.Toolbar({
        autoWidth:true,
        items: [
        {
            xtype: 'tbbutton',
            text: document.getElementById('_edit').value,
            cls: 'x-btn-text-icon',
            icon: '/img/editar_propiedad.png',
            handler: editarPropiedad
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'tbbutton',
            text: document.getElementById('_publish').value,
            cls: 'x-btn-text-icon',
            icon: '/img/activo.png',
            handler: publicarPropiedad
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'tbbutton',
            text: document.getElementById('no_publish').value,
            cls: 'x-btn-text-icon',
            icon: '/img/inactivo.png',
            handler: nopublicarPropiedad
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'tbbutton',
            text: document.getElementById('_delete').value,
            cls: 'x-btn-text-icon',
            icon: '/img/delete.png',
            handler: eliminarPropiedad
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'tbbutton',
            text: document.getElementById('_profile').value,
            cls: 'x-btn-text-icon',
            icon: '/img/status.png',
            handler: verperfilPropiedad
        },
        {
            xtype: 'tbseparator'
        },{
            xtype: 'tbbutton',
            text: $("#addcredits").val(),
            cls: 'x-btn-text-icon',
            icon: '/img/addcredits.png',
            handler: addCredits,
            width:100
        },
        {
            xtype: 'tbfill'
        }
        ]
    });

    Ext.namespace('Ext.userdata');
    Ext.userdata.status = [['0', 'inactiva'], ['1', 'activa']];
    var stStore = new Ext.data.SimpleStore({
        fields: ['status', 'label'],
        data: Ext.userdata.status
    });

  
    function viewIcon(val){
        if(val==2){
            val='/img/activo.png';
        }else{
            val='/img/inactivo.png';
        }
        return '<img src="'+ val +'" />';
    }
    var clienteDataStore = new Ext.data.GroupingStore({
        url: '/mihousebook/getpropropiedades',
        groupField:'Pais',
        reader: new Ext.data.JsonReader({
            root: 'rows',
            totalProperty: 'results',
            id: 'CodigoPropiedad'
        }, [
        {
            name: 'CodigoPropiedad',
            type: 'int'
        },
        {
            name: 'Status',
            type: 'int'
            
        },
        {
            name: 'CodigoBroker',
            type: 'string'
        },
        {
            name: 'TipoPropiedad',
            type: 'string'
        },
        {
            name: 'TipoEnlistamiento',
            type: 'string'
        },
        {
            name: 'Pais',
            type: 'string'
        },
        {
            name: 'Estado',
            type: 'string'
        },
        {
            name: 'Ciudad',
            type: 'string'
        },
        {
            name: 'Creditos',
            type: 'int'
        },
        {
            name: 'FechaRegistro',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            name: 'Latitud'
        },
        {
            name: 'Longitud'
        }
        ])
        ,
        remoteSort:true
        ,
        sortInfo: {
            field: 'CodigoPropiedad',
            direction: 'DESC'
        }
    });

    var proppagingBar = new Ext.PagingToolbar({
        pageSize:50,
        store: clienteDataStore,
        displayInfo: true,
        beforePageText : document.getElementById('_page').value,
        afterPageText  : document.getElementById('_of').value +" {0}",
        firstText      : document.getElementById('_firtspage').value,
        prevText       : document.getElementById('_previouspage').value,
        nextText       : document.getElementById('_nextpage').value,
        lastText       : document.getElementById('_lastpage').value,
        refreshText    : document.getElementById('_refresh').value,
        emptyMsg: document.getElementById('no_property').value,
        displayMsg: document.getElementById('_showin').value + ' {0} - {1} '+ document.getElementById('_of').value +' {2}'

    });

    var clienteCM = [
    new Ext.grid.CheckboxSelectionModel(),    
    {        
        dataIndex: 'Status',
        sortable: true,
        renderer: viewIcon,
        width:40
    },
    {
        header: 'ID',
        dataIndex: 'CodigoPropiedad',
        sortable: true,
        width:70,
        align:'center'
    },
    {
        header: document.getElementById('source_id').value,
        dataIndex: 'CodigoBroker',
        sortable: true,
        width:60
    },
    {
        header: document.getElementById('property_type').value,
        dataIndex: 'TipoPropiedad',
        sortable: true,
        width:100
    },
    {
        header: document.getElementById('listing_type').value,
        dataIndex: 'TipoEnlistamiento',
        sortable: true
    },
    {
        header: document.getElementById('_country').value,
        dataIndex: 'Pais',
        sortable: true,
        hidden:true
    },
    {
        header: document.getElementById('_state').value,
        dataIndex: 'Estado',
        sortable: true
    },
    {
        header: document.getElementById('_city').value,
        dataIndex: 'Ciudad',
        sortable: true
    },
    {
        header: document.getElementById('_credits').value,
        dataIndex: 'Creditos',
        sortable: true
    },
    {
        header: document.getElementById('upload_date').value,
        dataIndex: 'FechaRegistro',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    }
    ];

    var mySelectionModel = new Ext.grid.CheckboxSelectionModel({
        singleSelect: false
    });
    var dtgridCliente = new Ext.grid.GridPanel({
        store: clienteDataStore,
        columns: clienteCM,
        view: new Ext.grid.GroupingView({
            forceFit:true,
            groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
        }),
        frame: true,
        loadMask: {
            msg: document.getElementById('load_grid').value
        },
        viewConfig: {
            //forceFit:true,
            emptyText: document.getElementById('no_result').value
        },
        renderTo: 'pro_propiedades',
        stripeRows: true,
        /*selModel: new Ext.grid.RowSelectionModel({
            singleSelect: true
        }),*/
        height: 300,
        // autoWidth: true,
        width: 750,
        collapsed: false,
        title: document.getElementById('property_listing').value,
        sm: mySelectionModel,
        tbar: proptoolbar,
        bbar: proppagingBar,
        iconCls: 'creditos_panel'
    });

    clienteDataStore.load({
        params:{
            start:0,
            limit:50
        }
    });
});