// Crear la validacion de los campos de agregar propiedad
/*
$(document).ready(function() {
	$("#frm").validate({
        errorClass: "er",
        errorContainer: "#errorbox",
        errorLabelContainer: "#errorbox > ul",
        wrapper: 'li',
        event: "submit",
	rules: {
              //name=> del elemento
                cat : {
                        required : true
                    },
                titulo : {
                        required : true
                    },
                pais : {
                        required : true
                    },
                estado : {
                        required : true
                    },
                ciudad : {
                        required : true
                    },
                direccion : {
                        required : true
                    },
                codigoPostal : {
                        required : false
                    },
                descripcion : {
                        required : true
                    },
                area : {
                        required : true
                    },
                areaUM : {
                        required : true
                    }
		},
        messages : {
                cat : {
                    required : "Please, choose a property type"
                },
                titulo : {
                    required : "Please, enter a property's title"
                },
                pais : {
                    required : "Please, enter a country code"
                },
                estado : {
                    required : "Please, enter a state name"
                },
                ciudad : {
                    required : "Please, enter a city name"
                },
                direccion : {
                    required : "Please, enter an address"
                },
                codigoPostal : {
                    required : "Please, enter a zip code"
                },
                descripcion : {
                    required : "Please, enter a description"
                },
                area : {
                    required : "Please, enter an area size"
                },
                areaUM : {
                    required : "Please, choose a area's measurement unit"
                }
        }
    });
    
  });
*/
function validate() {
    var estateType = $("cat");
    var alquiler = $("alquiler");
    var alquilerUS = $("alquilerUS");
    var venta = $("venta");
    var ventaUS = $("ventaUS");
    var titulo = $("titulo");
    var pais = $("pais");
    var estado = $("estado");
    var ciudad = $("ciudad");
    var direccion = $("direccion");
    var codigoPostal = $("codigoPostal");
    var descripcion = $("descripcion");
    var area = $("area");
    var areaUM = $("areaUM");
    var areaLote = $("areaLote");
    var areaLoteUM = $("areaLoteUM");

    // Tipo de categoria debe ser valido.
    if(estateType) {
        value = estateType.val();
        if(value.length == 0) {
            alert("Invalido");
            return false;
        } else { // Revisar que sea numerico.
        }
    } else {
        alert("Invalido");
        return false;
    }

    // Si es alquiler, debe ser valido el valor del alquiler.
    if(alquiler) {
        value = alquiler.val();
        // Verificar que este chequeado para verificar que este presente el valor de alquiler
        if(alquilerUS) {
            value = alquilerUS.val();
            if(value.length == 0) {
                alert("Invalido");
                return false;
            } else {

            }
        } else {
            alert("Invalido");
            return false;
        }
    } else {
        alert("Invalido");
        return false;
    }

    // Si es venta, debe ser valido el valor de la venta.
    if(venta) {
        value = venta.val();
        // Verificar que este chequeado para verificar que este presente el valor de la venta
        if(ventaUS) {
            value = ventaUS.val();
            if(value.length == 0) {
                alert("Invalido");
                return false;
            } else {

            }
        } else {
            alert("Invalido");
            return false;
        }
    } else {
        alert("Invalido");
        return false;
    }

    // El titulo de la propiedad debe ser valido.
    if(titulo) {
        value = titulo.val();
        if(value.length == 0) {
            alert("Invalido");
            return false;
        } else { // Revisar que no sea mayor a ciertas cantidades de caracteres.

        }
    } else {
        alert("Invalido");
        return false;
    }

    // El pais debe ser valido
    if(pais) {
        value = pais.val();
        if(value.length == 0) {
            alert("Invalido");
            return false;
        } else { // Revisar que no sea mayor a ciertas cantidades de caracteres

        }
    } else {
        alert("Invalido");
        return false;
    }

    // Revisar que el estado del pais sea valido
    if(estado) {
        value = estado.val();
        if(value.length == 0) {
            alert("Invalido");
            return false;
        } else { // Revisar que no sea mayor a ciertas cantidades de caracteres

        }
    } else {
        alert("Invalido");
        return false;
    }

    // Revisar que la ciudad del estado del pais sea valido
    if(ciudad) {
        value = ciudad.val();
        if(value.length == 0) {
            alert("Invalido");
            return false;
        } else { // Revisar que no sea mayor a ciertas cantidades de caracteres

        }
    } else {
        alert("Invalido");
        return false;
    }

    // Revisar que la direccion de la propiedad sea valida
    if(direccion) {
        value = direccion.val();
        if(value.length == 0) {
            alert("Invalido");
            return false;
        } else { // Revisar que no sea mayor a ciertas cantidades de caracteres

        }
    } else {
        alert("Invalido");
        return false;
    }

    // Revisar que el codigo postal de la propiedad sea valido
    if(codigoPostal) {
        value = codigoPostal.val();
        if(value.length == 0) {
            alert("Invalido");
            return false;
        } else { // Revisar que no sea mayor a ciertas cantidades de caracteres

        }
    } else {
        alert("Invalido");
        return false;
    }

    // Revisar que la descripcion de la propiedad sea valida
    if(descripcion) {
        value = descripcion.val();
        if(value.length == 0) {
            alert("Invalido");
            return false;
        } else { // Revisar que no sea mayor a ciertas cantidades de caracteres

        }
    } else {
        alert("Invalido");
        return false;
    }

    // Revisar que la direccion de la propiedad sea valida
    if(area) {
        value = area.val();
        // Verificar que este chequeado para verificar que este presente el valor de la venta
        if(areaUM) {
            value = areaUM.val();
            if(value.length == 0) {
                alert("Invalido");
                return false;
            } else {

            }
        } else {
            alert("Invalido");
            return false;
        }
    } else {
        alert("Invalido");
        return false;
    }

    // Revisar que la direccion de la propiedad sea valida
    if(areaLote) {
        value = areaLote.val();
        // Verificar que este chequeado para verificar que este presente el valor de la venta
        if(areaLoteUM) {
            value = areaLoteUM.val();
            if(value.length == 0) {
                alert("Invalido");
                return false;
            } else {

            }
        } else {
            alert("Invalido");
            return false;
        }
    } else {
        alert("Invalido");
        return false;
    }

    // Ok. Los campos requeridos se proporcionaron.
    return true;
}