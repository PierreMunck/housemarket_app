// Custom validate rules 
// Adding new rules to jquery-validate plugin
// work but two field are required
$(document).ready(function() {
    var container = $('div.container');
    $("#form_enlistar").validate({
        errorClass: "er",
        errorContainer: container,
        errorLabelContainer: $("ol", container),
        wrapper: 'li',
        meta: "validate",
        event: "keyup",
        rules: {
            //name=> del elemento
            'precio': "required",
            'area': "required",
            'cuarto': "required",
            'bano': "required",
            'tamano_lote': "required"
		
        }
    //         ,submitHandler: function(form) {
    //                   form.submit();
    //         }

    });
    container.corner();
});
