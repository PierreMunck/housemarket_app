/**
 * Almacena variables de entorno a utilizar en la aplicacion
 */
var env = {
    canvas_url : 'https://app.hmapp.com/',
    facebook_url : 'https://apps.facebook.com/housemarket/'
}
