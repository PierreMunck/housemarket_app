// Map Search Control and Stylesheet
window._uds_msw_donotrepair = true;

/**
 * Aplica internacionalizacion
 */
function __translate(s) {
    if (typeof(i18n)!='undefined' && i18n[s]) {
        return i18n[s];
    }
    return s;
}

/**
 * Recupera variables de entorno
 */
function __environment(s) {
    if (typeof(env)!='undefined' && env[s]) {
        return env[s];
    }
    return s;
}

var map;
var geocoder;
var address;
var place;
var lat2;
var long2;
var cn;
var fb;
var marker;
var countrycode;

 function _GetParametersListURL(){
        var _parameters={};
        var _6=facebook_perms.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,_index,_value){
            _parameters[_index]=unescape(_value);
        });
        return _parameters;
    };

 var _parametersList=_GetParametersListURL();
var _urlParameters=Ext.urlEncode(_parametersList);


//Guarda los brokers segun el area seleccionada
    var _brokers = new Ext.data.Store({
        url: "/index/brokersByArea"+"?"+_urlParameters,
        baseParams:fb,
        reader: new Ext.data.JsonReader({
            root:"rows",
            id:"Uid",
            coordinate:"coordinates"
        },
        [{
            name:"NombreCliente"
        },{
            name:"Uid"
        },{
           name: "pic_square"
        }
        ])
    });



//var mini=new GOverviewMapControl();
function getUrlVars() {
    var map = {};
    var parts = facebook_perms.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        map[key] = unescape(value);
    });
    return map;
}
cn = getUrlVars();
fb = Ext.urlEncode(cn);
var fbparams = Ext.urlDecode(Ext.urlEncode(cn));

function initialize() {
    map = new GMap2(document.getElementById("mapsearch"));
    geocoder = new GClientGeocoder();

    // Here we set the cache to use the UsCitiesCache custom cache.
    // Se Puede desactivar la cache pasando null al metodo setCache() del objeto GClientGeocoder
    geocoder.setCache(new CapitalCitiesCache());

    //map.addControl(new GSmallMapControl());
    //map.addControl(new GMapTypeControl());
    //map.addControl(new GScaleControl()) ;


    function GetGMCoordinates(){
            var _gmBounds=map.getBounds();
            var _gmBoundsSW = _gmBounds.getSouthWest();
            var _gmBoundsNE = _gmBounds.getNorthEast();
            var _gmSWLng=_gmBoundsSW.lng();
            var _gmSWLat=_gmBoundsSW.lat();
            var _gmNELng=_gmBoundsNE.lng();
            var _gmNELat=_gmBoundsNE.lat();

            return {
                minX:_gmSWLng,
                maxX:_gmNELng,
                minY:_gmSWLat,
                maxY:_gmNELat
            };
        };

    var centermap = {
        lat: "11.3",
        lng: "-85.75"
    };

    Ext.Ajax.request({
        url: '/enlistar/propiedadenmapa'+ '?valprop=9372&'+fb,
        success:function (result, response) {
            try {
                var rs = Ext.util.JSON.decode(result.responseText);
                centermap = rs.data[0];

                point = new GLatLng(centermap.lat,centermap.lng);
                marker = new GMarker(point,markerOptions);
                map.setCenter(point, 14);
                map.addOverlay(marker);

                countrycode=centermap.cc;

                GEvent.addListener(marker, "dragend", function() {
                    validar(0);
                    lat2 = marker.getPoint().lat();
                    long2 = marker.getPoint().lng();
                    asignarCoordenadas(lat2,long2);
                    geocoder.getLocations(marker.getPoint(), addMarkerAddressToMap);
                });
                asignarCoordenadas(centermap.lat,centermap.lng);
                asignarcountrycode(countrycode);

                var _coordinates = GetGMCoordinates();
                _brokers.load({
                            params:{
                                "coordinates":Ext.encode(_coordinates)
                            }
                        });

            } catch (err) {
                map.setCenter(new GLatLng(centermap.lat, centermap.lng), 1);
                validar(0);
            }

        },
        failure: function(result,response) {
            map.setCenter(new GLatLng(centermap.lat, centermap.lng), 1);
            validar(0);
        }
    });

      _brokers.on("load", function(s,r){
        var _obj_coordinates = GetGMCoordinates();

        var _showBrokers = new Ext.XTemplate(
            "<input type=\"hidden\" name=\"maxX\" value=\""+_obj_coordinates.maxX+"\" />",
            "<input type=\"hidden\" name=\"maxY\" value=\""+_obj_coordinates.maxY+"\" />",
            "<input type=\"hidden\" name=\"minX\" value=\""+_obj_coordinates.minX+"\" />",
            "<input type=\"hidden\" name=\"minY\" value=\""+_obj_coordinates.minY+"\" />",
            "<tpl for=\".\">",
              "<tpl for=\"data\">",
                "<div class=\"person\">",
                   "<tpl if=\"this.noSrc(pic_square) == false\">",
                     "<a href=\""+ __environment('facebook_url') + "enlistar/propiedadesautor?id={Uid}\" target=\"_top\"><img src=\"{pic_square}\" alt=\"{NombreCliente}\" onerror=\"this.src='/img/q_silhouette.gif'\" /><span>{NombreCliente}</span></a>",
                   "</tpl>",
                   "<tpl if=\"this.noSrc(pic_square) == true\">",
                      "<a href=\""+ __environment('facebook_url') + "enlistar/propiedadesautor?id={Uid}\" target=\"_top\"><img src=\"/img/q_silhouette.gif\" alt=\"{NombreCliente}\"/>{NombreCliente}</a>",
                   "</tpl>",
                "</div>",
              "</tpl>",
            "</tpl>",{
            compiled:true,
            noSrc:function(src){
                return src === "";
            }
            });
            _showBrokers.overwrite("allBrokers",r);
            $("#see_all_count").html(_brokers.getTotalCount());

      });

      var allBroker = $("#link_all_brokers");
    if(allBroker) {
        allBroker.click(function(){
            document.getElementById("frmAllBrokers").submit();
            return false;
        });
    }

    map.setUIToDefault();

    map.disableScrollWheelZoom();
    map.disableDoubleClickZoom();
    map.disableContinuousZoom();
}

// Create our "tiny" marker icon
tinyIcon = new GIcon();
tinyIcon.image = "https://labs.google.com/ridefinder/images/mm_20_red.png";
tinyIcon.shadow = "https://labs.google.com/ridefinder/images/mm_20_shadow.png";
tinyIcon.shadowSize = new GSize(22, 20);
tinyIcon.iconAnchor = new GPoint(6, 20);
tinyIcon.infoWindowAnchor = new GPoint(5, 1);

// iconos para marcador default.
var hb_icon = new GIcon(G_DEFAULT_ICON);
hb_icon.image = '/img/house.png';
hb_icon.iconSize = new GSize(27, 32);
var markerOptions ={
    icon:hb_icon,
    draggable: false
};

function trim (myString){
    return myString.replace(/^\s+/g,'').replace(/\s+$/g,'');
}

function getAddress(overlay, latlng) {
    if (latlng != null) {
        // Set up our GMarkerOptions object
        validar(0);
        address = latlng;
        lat2= address['y'];
        long2= address['x'];
        asignarCoordenadas(lat2,long2);
        geocoder.getLocations(latlng, addAddressToMap);
    }
}

function addMarkerAddressToMap(response) {
    if (!response || response.Status.code != 200) {
        Ext.MessageBox.alert('Alerta', "Status Code:" + response.Status.code);
        validar(0);
    }
    else
    {
        place = response.Placemark[0];
        code_country=place.AddressDetails.Country.CountryNameCode;
        asignarcountrycode(code_country);
        procesarPrecision(place,code_country);

    }
}

// addAddressToMap() is called when the geocoder returns an
// answer.  It adds a marker to the map with an open info window
// showing the nicely formatted version of the address and the country code.
function addAddressToMap(response) {
    map.clearOverlays();

    if (!response || response.Status.code != 200) {
        Ext.MessageBox.alert('Alerta', "Status Code:" + response.Status.code);
        validar(0);
    }
    else
    {
        place = response.Placemark[0];
        marker = new GMarker(address,markerOptions);
        map.addOverlay(marker);
        map.setCenter(address, 13);

        GEvent.addListener(marker, "dragend", function() {
            validar(0);
            lat2 = marker.getPoint().lat();
            long2 = marker.getPoint().lng();
            console.log(marker.getPoint());
            asignarCoordenadas(lat2,long2);
            geocoder.getLocations(marker.getPoint(), addMarkerAddressToMap);
        });

        lat2 = marker.getPoint().lat();
        long2 = marker.getPoint().lng();

        code_country=place.AddressDetails.Country.CountryNameCode;

        asignarcountrycode(code_country);

        procesarPrecision(place,code_country);

    }
}

function procesarPrecision(place,codigopais){

    if(place.AddressDetails.Country!=null){
        precision=place.AddressDetails.Accuracy;
        var direcc=place.address;
        var data = direcc.split(',');
        var nparams=data.length;

        for(i=0; i<(data.length); i++){
        }
        var nom_pais;
        var ciudad;
        switch(precision){
            case 1:
                // a nivel de pais
                break;
            case 2:
                switch(nparams){
                    case 2:
                        ciudad= data[0];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(codigopais);
                        break;


                }
                if(place.AddressDetails.Country.AdministrativeArea!=null){
                    ciudad = place.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName;
                    $("#ciudad").val(ciudad);
                }
                break;
            case 3:
                switch(nparams){
                    case 2:
                        ciudad= data[0];
                        $("#ciudad").val(ciudad);
                        break;
                    case 3:
                        ciudad= data[0];
                        nom_estado= data[1];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        //console.log("Pais:"+nom_pais);
                        break;

                }
                if(place.AddressDetails.Country.AdministrativeArea!=null){
                    ciudad = place.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName;
                    $("#ciudad").val(ciudad);
                }
                break;
            case 4:
                switch(nparams){
                    case 2:
                        ciudad= data[0];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(code_country);
                        break;
                    case 3:
                        if(code_country=="NI"){
                            ciudad1= data[0];
                            ciudad2= data[1];
                            ciudad= ciudad1+","+ciudad2;
                            nom_estado=code_country;
                        }else{
                            ciudad= data[0];
                            nom_estado= data[1];
                        }

                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        //console.log("Pais:"+nom_pais);
                        break;
                    case 4:
                        ciudad1= data[0];
                        ciudad2= data[1];
                        ciudad= ciudad1+","+ciudad2;
                        nom_estado= data[2];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        //console.log("Pais:"+nom_pais);
                        break;
                    case 5:
                        ciudad1= data[0];
                        ciudad2= data[1];
                        ciudad3= data[2];
                        ciudad= ciudad1+","+ciudad2+","+ciudad3;
                        nom_estado= data[3];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        //console.log("Pais:"+nom_pais);
                        break;

                }
                if(place.AddressDetails.Country.AdministrativeArea!=null){
                    ciudad = place.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName;
                    $("#ciudad").val(ciudad);
                //   console.log("ciudad area administrativa:"+ciudad);
                }
                break;
            case 5:
                switch(nparams){
                    case 2:
                        ciudad= data[0];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(code_country);
                        break;
                    case 3:
                        ciudad= data[0];
                        nom_estado= data[1];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        break;

                }

                if(place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea!=null){
                    ciudad =place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
                    $("#ciudad").val(ciudad);
                }
                break;
            case 6:
                switch(nparams){

                    case 2:
                        ciudad= data[0];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(code_country);
                        break;
                    case 3:
                        if(code_country=="NI"){
                            ciudad1= data[0];
                            ciudad2= data[1];
                            ciudad= ciudad1+","+ciudad2;
                        }else{
                            ciudad= data[1];
                        }
                        $("#ciudad").val(ciudad);
                        $("#estado").val(code_country);
                        break;
                    case 4:
                        if(code_country=="NI"){
                            ciudad1= data[0];
                            ciudad2= data[1];
                            ciudad= ciudad1+","+ciudad2;
                            nom_estado= code_country;
                        }else{
                            ciudad1= data[0];
                            ciudad2= data[1];
                            ciudad= ciudad1+","+ciudad2;
                            nom_estado= data[2];
                        }
                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        break;

                }
                if(place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea!=null){
                    ciudad =place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
                    $("#ciudad").val(ciudad);

                }
                break;
            case 7:
                if(place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea!=null){
                    ciudad =place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
                    $("#ciudad").val(ciudad);
                }
                break;
            case 8:

                switch(nparams){
                    case 2:
                        nom_pais= data[(1-1)];
                        $("#ciudad").val(nom_pais);
                        $("#estado").val(code_country);
                        //console.log("Pais:"+nom_pais);
                        break;
                    case 3:
                        ciudad1= data[0];
                        ciudad2= data[1];
                        ciudad=ciudad1+","+ciudad2;
                        nom_estado= code_country;
                        $("#ciudad").val(nom_pais);
                        $("#estado").val(nom_estado);
                        break;
                    case 4:
                        ciudad1= data[0];
                        ciudad2= data[1];
                        ciudad=ciudad1+","+ciudad2;
                        nom_estado= data[2];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        break;

                }
                if(place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea!=null){
                    ciudad =place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
                    $("#ciudad").val(ciudad);
                }
                break;
            default:

                break;
        }
    //console.log("Ciudad:"+ciudad);
    //validar(1);

    }else{

};

}

function validar(flag){
    if (flag=="1"){
    //$("#direction").val(place.address);
    //  $("#latitud").val(lat2);
    //  $("#longitud").val(long2);

    }else{
        //$("#direction").val("");
        $("#ciudad").val("");
        $("#estado").val("");
        $("#latitud").val("");
        $("#longitud").val("");
        $("#zip_code").val("");
        $("#country").val("");
    }
}

// showLocation() is called when you click on the Search button
// in the form.  It geocodes the address entered into the form
// and adds a marker to the map at that location.
function showLocation(address) {
    geocoder.getLocations(address, addAddressToMap);
}

// findLocation() is used to enter the sample addresses into the form.
function findLocation(address) {
    showLocation(address);
}

// Geocoding Cache
// CapitalCitiesCache is a custom cache that extends the standard GeocodeCache.
// We call apply(this) to invoke the parent's class constructor.
function CapitalCitiesCache() {
    GGeocodeCache.apply(this);
}

// Assigns an instance of the parent class as a prototype of the
// child class, to make sure that all methods defined on the parent
// class can be directly invoked on the child class.
CapitalCitiesCache.prototype = new GGeocodeCache();

function asignarCoordenadas(latitud,longitud){
    $("#latitud").val(latitud);
    $("#longitud").val(longitud);
}

function asignarcountrycode(countrycode){
    $("#zip_code").val(countrycode);
    $("#country").val(countrycode);
    $("#country").change();
}

// Ext.onReady(function(){
$().ready(function(){
    if (GBrowserIsCompatible()) {
        initialize();
    }else {
        Ext.Msg.alert('Alerta', 'Su browser no esta soportado por Google Maps');
    }

    var share = Ext.get('btnShare');
    if(share) {
        share.on('click', function() {
            StreamPublishProperty(
                'Housemarket',
                this.dom.attributes.getNamedItem('caption').value,
                this.dom.attributes.getNamedItem('description').value,
                this.dom.attributes.getNamedItem('location').value,
                this.dom.attributes.getNamedItem('urlFbProperty').value,
                this.dom.attributes.getNamedItem('imageSrc').value,
                this.dom.attributes.getNamedItem('urlSrvProperty').value)
        });
    }

    $("#btnResponde").click(function() {
        Ext.Ajax.request({
            url:"/fbml/responde-request" +"?"+ _urlParameters,
            params: {
                    id: $(this).attr("code")
                },
            success:function(_response,_options){
                var content = '<fb:fbml>' +
                              _response.responseText +
                              '</fb:fbml>';
                var req = {
                    method: 'fbml.dialog',
                    display: 'dialog',
                    fbml:  (content),
                    width: 900

                }

                FB.ui(req);
            },
            failure:function(_response,_options){
                alert(_response);
            }
        });
    });

 $("#mutual").click(function() {
        Ext.Ajax.request({
            url:"/fbml/mutual-friends" +"?"+"id="+$(this).attr("uid")+"&"+ _urlParameters,
            params: {
                    id: $(this).attr("uid")//id de facebook del usuario actual
                },
            success:function(_response,_options){
                var content = '<fb:fbml>' +
                              _response.responseText +
                              '</fb:fbml>';
                var req = {
                    method: 'fbml.dialog',
                    display: 'dialog',
                    fbml:  (content),
                    width: 900
                }

                FB.ui(req);
            },
            failure:function(_response,_options){
                alert(_response);
            }
        });
    });

// We only want these styles applied when javascript is enabled
    $('div.navigation').css({'width' : '90px', 'float' : 'right'});
    $('div.content').css('display', 'block');

    // Initially set opacity on thumbs and add
    // additional styling for hover effect on thumbs
    

    // Initialize Advanced Galleriffic Gallery

    var gallery = $('#thumbs');
    if(gallery.length > 0){

        var onMouseOutOpacity = 0.67;
        $('#thumbs ul.thumbs li').opacityrollover({
            mouseOutOpacity:   onMouseOutOpacity,
            mouseOverOpacity:  1.0,
            fadeSpeed:         'fast',
            exemptionSelector: '.selected'
        });

        gallery.galleriffic({
            delay:                     2500,
            numThumbs:                 5,
            preloadAhead:              10,
            enableTopPager:            false,
            enableBottomPager:         false,
            maxPagesToShow:            7,
            imageContainerSel:         '#slideshow',
            controlsContainerSel:      '#controls',
            captionContainerSel:       '#caption',
            loadingContainerSel:       '#loading',
            renderSSControls:          false,
            renderNavControls:         false,
            playLinkText:              'Play Slideshow',
            pauseLinkText:             'Pause Slideshow',
            prevLinkText:              '&lsaquo; Previous Photo',
            nextLinkText:              'Next Photo &rsaquo;',
            nextPageLinkText:          'Next &rsaquo;',
            prevPageLinkText:          '&lsaquo; Prev',
            enableHistory:             false,
            autoStart:                 false,
            syncTransitions:           true,
            defaultTransitionDuration: 900,
            onSlideChange:             function(prevIndex, nextIndex) {
                // 'this' refers to the gallery, which is an extension of $('#thumbs')
                this.find('ul.thumbs').children()
                .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
                .eq(nextIndex).fadeTo('fast', 1.0);
            },
            onPageTransitionOut:       function(callback) {
                this.fadeTo('fast', 0.0, callback);
            },
            onPageTransitionIn:        function() {
                this.fadeTo('fast', 1.0);
            }
        });
    }

});

