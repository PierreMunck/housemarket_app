/* 
 * Grid para el listado de usuarios
 *
 */
Ext.onReady(function(){
    // @todo cambiar url en produccion
    Ext.BLANK_IMAGE_URL = '/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = "side";

    function getUrlVars() {
	var map = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		map[key] = value;
	});
	return map;
	}

    var prueba = getUrlVars();
    var fb = Ext.urlEncode(prueba);
    var fb1 = Ext.urlDecode(Ext.urlEncode(prueba));





    var tipoStore = new Ext.data.SimpleStore({
        fields: ['tipousuario'],
        data: [['Administrador'],
        ['operador']]
    });

    Ext.apply(Ext.form.VTypes, {
        password : function(val, field) {
            if (field.initialPassField) {
                var pwd = Ext.getCmp(field.initialPassField);
                return (val == pwd.getValue());
            }
            return true;
        },
        passwordText : 'Passwords no coinciden'
    });

    Ext.namespace('Ext.property');
    Ext.property.status = [
    ['1', 'Vendida'], ['2', 'Alquilada'], ['3', 'Activa'], ['4', 'Inactiva'] ];
    var stStore = new Ext.data.SimpleStore({
        fields: ['status', 'label'],
        data: Ext.property.status
    });
    //stStore.load();


 
//   edit_form
    

    function addProp () {
       window.top.location="https://apps.facebook.com/hmproject/enlistar/form";
    }

    function cargaProp () {
       window.top.location="https://apps.facebook.com/hmproject/mihousebook/showload";
    }
   
    function editProp() {
         var sm = gridProp.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            //console.dir(sel.data);
            //console.log(sel.data.idpropiedad);
            //var p2=Ext.encode(sel.data.idpropiedad);
	    var p2 = sel.data.idpropiedad;
            window.top.location="https://apps.facebook.com/hmproject/Mihousebook/edit?propiedad="+p2;

        } else {
            Ext.Msg.alert('Alerta', 'Debe Seleccionar una propiedad para poder realizar esta operacion')
        }
    }

    function statusProp(){
        var sm = gridProp.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            //console.dir(sel.data);
            var id_prop = sel.data.idpropiedad;
            Ext.Msg.show({
                title: 'Status de la Propiedad',
                msg: 'Esta seguro que desea cambiar el status de <br /> la propiedad #' + id_prop + ' ?',
                //buttons: Ext.MessageBox.OKCANCEL,
                buttons: {
                    ok: true,
                    cancel: true
                },
                fn: function(btn) {
                    switch(btn){
                         case 'ok':
                            Ext.Ajax.request({
                                url: '/Mihousebook/changestatus'+'?'+fb,
                                params: {
                                    'id': id_prop
                                },
                                success: function () {
                                    propDataStore.load();
                                },
                                failure: function () {
                                    Ext.msg.alert('Error', 'Ha ocurrido un error el status <br />de la propiedad no pudo ser cambiada');
                                }
                            });
                            //Ext.Msg.alert('kill this dude', btn);
                            break;
                        case 'cancel':
                            //Ext.Msg.alert('wtf, no elimination', btn);
                            break;
                    }

                },
                icon: Ext.MessageBox.WARNING
            });

        } else {
            Ext.Msg.alert('Error', 'Debe Seleccionar una propiedad para poder realizar esta operacion')
        }
    }
    function statusProp2(){
        var sm = gridProp.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            //console.dir(sel.data);
            var id_prop = sel.data.idpropiedad;
            Ext.Msg.show({
                title: 'Status de la Propiedad',
                msg: 'Esta seguro que desea cambiar el status de <br /> la propiedad #' + id_prop + ' ?',
                //buttons: Ext.MessageBox.OKCANCEL,
                buttons: {
                    ok: true,
                    cancel: true
                },
                fn: function(btn) {
                    switch(btn){
                         case 'ok':
                            Ext.Ajax.request({
                                url: '/Mihousebook/changestatus2'+'?'+fb,
                                params: {
                                    'id': id_prop
                                },
                                success: function () {
                                    propDataStore.load();
                                },
                                failure: function () {
                                    Ext.msg.alert('Error', 'Ha ocurrido un error el status <br />de la propiedad no pudo ser cambiada');
                                }
                            });
                            //Ext.Msg.alert('kill this dude', btn);
                            break;
                        case 'cancel':
                            //Ext.Msg.alert('wtf, no elimination', btn);
                            break;
                    }

                },
                icon: Ext.MessageBox.WARNING
            });

        } else {
            Ext.Msg.alert('Error', 'Debe Seleccionar una propiedad para poder realizar esta operacion')
        }
    }
    function status_image(val){
       var caption;
       switch(val)
          {
            case "1":
               caption = "Vendida";
            break;
            case "2":
               caption = "Alquilada";
            break;
            case "3":
               caption = "Activa";
            break;
            case "4":
               caption = "Inactiva";
            break;
          }
        return '<div class="imagen-' + val +'">' + caption +'</div>'
    }

    var toolbar = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Agregar Propiedad',
            cls: 'x-btn-text-icon',
            icon: '/img/status.png',
            handler: addProp
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'tbbutton',
            text: 'Cargar Datos',
            cls: 'x-btn-text-icon',
            icon: '/img/status.png',
            handler: cargaProp
        },
        {
            xtype: 'tbseparator'
        },
	  {
	      xtype: 'tbbutton',
            text: 'Editar Propiedad',
            cls: 'x-btn-text-icon',
            icon: '/img/status.png',
            handler: editProp
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'tbbutton',
            text: 'Activar',
            cls: 'x-btn-text-icon',
            icon: '/img/status2.png',
            handler: statusProp
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'tbbutton',
            text: 'Vendida/Alquilada',
            cls: 'x-btn-text-icon',
            icon: '/img/status2.png',
            handler: statusProp2
        }
        ]
    });

 
    var propDataStore = new Ext.data.Store({
        //url: '/Mihousebook/getregisters',
        url: '/Mihousebook/getregisters'+'?'+ fb,
	baseParams: fb1,
	autoLoad: false,
        reader: new Ext.data.JsonReader({
            root: 'rows',
            totalProperty: 'results',
            id: 'idpropiedad'
        }, [
        {
            name: 'idpropiedad'
        },

        {
            name: 'descripcion'
        },

        {
            name: 'pais'
        },

        {
            name: 'precio_venta'
        },
        {
            name: 'precio_alquiler'
        },
        
        {
            name: 'tipo_enlistamiento'
        },
        {
            name: 'tipo_propiedad'
        },
        {
            name: 'fecha_ingreso',
            type: 'date', dateFormat: 'Y-m-d H:i:s'
        },

        {
            name: 'ultima_modificacion'
        },
        {
            name: 'area'
        },

        {
            name: 'cuartos'
        },

        {
            name: 'banos'
        },

        {
            name: 'lat'
        },

        {
            name: 'lng'
        },
        {
            name: 'estatus'
        },

        {
            name: 'uid'
        }
        ]),
        sortInfo: {
            field: 'idpropiedad',
            direction: 'ASC'
        }
        ,remoteSort:true
    });
 //propDataStore.load();

     var pagingBar = new Ext.PagingToolbar({
        pageSize:12,
        store: propDataStore,
        displayInfo: true,
        emptyMsg: "Sin Registros a desplegar",
        displayMsg: 'Registros {0} - {1} de {2}'

    });

    var propCM = [
    {
        header: 'ID',
        dataIndex: 'idpropiedad',
        width: 50,
        sortable: true
    },
    {
        header: 'Pais',
        dataIndex: 'pais',
        sortable: true
    },
    {
        header: 'Enlistamiento',
        dataIndex: 'tipo_enlistamiento',
        width: 74,
        sortable: true
    },
    {
        header: 'Propiedad',
        dataIndex: 'tipo_propiedad',
        width: 65,
        sortable: true
    },
  
    {
        header: 'Venta',
        dataIndex: 'precio_venta',
        width: 40,
        sortable: true
    },
    {
        header: 'Alquiler',
        dataIndex: 'precio_alquiler',
        width: 46,
        sortable: true
    },
     {
        header: 'Estatus',
        dataIndex: 'estatus',
        width:78,
        sortable: true,
        renderer: status_image
    },
     {
        header: 'F. Ingreso',
        dataIndex: 'fecha_ingreso',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        width:75,
        sortable: true
    }//,
    //{
   //     header: 'Ultima Modificacion',
   //     dataIndex: 'ultima_modificacion',
   //     sortable: true
   // }
    ];

    var gridProp = new Ext.grid.GridPanel({
       store: propDataStore,
       columns: propCM,
        frame: false,
        loadMask: {
            msg: 'Cargando datos, por favor espere ....'
        },
       
        collapsible: true,
        animCollapse:true,
        stripeRows: true,
        height: 320,
        autoWidth: true,
        //autoHeight:true,
        collapsed: false,
        tbar: toolbar,
        bbar: pagingBar,
        iconCls: 'useradmin_panel'
        //,title: 'Administracion de Propiedades'
        //renderTo: 'property_grid',
        

    });
    //store: userDataStore,
   //  columns: userColumnModel,
      gridProp.render('property_grid');
      gridProp.on('rowdblclick',editProp);

      propDataStore.load({params:{start:0, limit:12}});

      var tabs_property = new Ext.TabPanel({
                        renderTo: 'tabs_property',
                        //width:530,
                        autoWidth: true,
                //        height:550,
                        plain:true,
                        activeTab: 0,
                        frame:true,
                        defaults:{autoHeight: true,autoScroll: true},
                        items:[
                            {contentEl:'detalle', title: 'Administracion de Propiedades'}
                              ]
                    });

});