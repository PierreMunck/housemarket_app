$(document).ready(function(){
  // hide all the content panes when the page loads
  $( '#optional_filter div' ).hide();

  // uncomment the next line if you'd like the first pane to be visible by default
  // $('#bio > div:first').show();

  $('#advanced').click(function() {
	if($('#img_search_advanced').attr("src") == 'img/down_search.png')
	{$('#img_search_advanced').attr('src', 'img/up_search.png');}
	else{$('#img_search_advanced').attr('src', 'img/down_search.png');}

    $('#optional_filter div').next().animate(
	    {'height':'toggle'}, 'slow'
    );
  });
});