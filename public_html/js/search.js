Ext.onReady(function(){
    Ext.BLANK_IMAGE_URL="/js/extjs/resources/images/gray/s.gif";
    Ext.QuickTips.init();
    if(Ext.isGecko){
        Ext.get("destacados").applyStyles("margin: 13px 0;");
    }
    
    var _1=[];
    _1[G_GEO_SUCCESS]="Success";
    _1[G_GEO_MISSING_ADDRESS]="Falta direccion: La direccion ingresada se perdio o no se ingrese valor.";
    _1[G_GEO_UNKNOWN_ADDRESS]="Direccion desconocida: No se pudo encontrar la correspondiente locacion geografica para la direccion ingresada.";
    _1[G_GEO_UNAVAILABLE_ADDRESS]="Direccion no disponible: Las coordenadas geograficas de la direccion no pueden ser retornadas debido a razones legales o contractuales.";
    _1[G_GEO_BAD_KEY]="Clave API erronea: La clave API es erronea o invalida no coincide con la generada para este dominio.";
    _1[G_GEO_TOO_MANY_QUERIES]="Demasiadas consultas: La cuota diaria de peticiones al servicio Geocode ha sido excedida para este sitio";
    _1[G_GEO_SERVER_ERROR]="Error del servidor: La peticion al servidor no pudo ser procesada con exito.";
    var _2=new GIcon(G_DEFAULT_ICON);
    _2.image="/img/house.png";
    _2.iconSize=new GSize(27,32);
    var _3={
        icon:_2
    };
    function _4(){
        var _5={};
        var _6=window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,_7,_8){
            _5[_7]=_8;
        });
        return _5;
    };
    var _9=_4();
    var fb=Ext.urlDecode(Ext.urlEncode(_9));
    var _a=Ext.urlEncode(_9);

    // Extractor de datos para las consultas de Ubicar en Mapa
    var _toGM=new Ext.data.Store({
        url:"/index/estateToGM"+"?"+_a,
        baseParams:fb,
        reader:new Ext.data.JsonReader({
            root:"rows",
            id:"CodigoPropiedad"
        },[{
            name:"CodigoPropiedad"
        },{
            name:"NombrePropiedad"
        },{
            name:"DescPropiedad"
        },{
            name:"Direccion"
        },{
            name:"Estado"
        },{
            name:"Ciudad"
        },{
            name:"ZipCode"
        },{
            name:"PrecioVenta"
        },{
            name:"PrecioAlquiler"
        },{
            name:"FechaRegistro"
        },{
            name:"FechaModifica"
        },{
            name:"CodigoCategoria"
        },{
            name:"Latitud"
        },{
            name:"Longitud"
        },{
            name:"Uid"
        },{
            name:"src"
        },{
            name:"width"
        },{
            name:"height"
        },{
            name:"src_normal"
        },{
            name:"src_small"
        }]),
        sortInfo:{
            field:"CodigoPropiedad",
            direction:"ASC"
        }
    });

    var _b=new Ext.data.Store({
        url:"/index/getmarks"+"?"+_a,
        baseParams:fb,
        reader:new Ext.data.JsonReader({
            root:"rows",
            id:"idpropiedad"
        },[{
            name:"idpropiedad"
        },{
            name:"nombre"
        },{
            name:"descripcion"
        },{
            name:"direccion"
        },{
            name:"pais"
        },{
            name:"estado"
        },{
            name:"ciudad"
        },{
            name:"zip_code"
        },{
            name:"precio_venta"
        },{
            name:"precio_alquiler"
        },{
            name:"tipo_enlistamiento"
        },{
            name:"fecha_ingreso"
        },{
            name:"ultima_modificacion"
        },{
            name:"tags"
        },{
            name:"area"
        },{
            name:"cuartos"
        },{
            name:"banos"
        },{
            name:"tamano_lote"
        },{
            name:"destacada"
        },{
            name:"roomate"
        },{
            name:"estatus"
        },{
            name:"tipo_propiedad"
        },{
            name:"lat"
        },{
            name:"lng"
        },{
            name:"foto"
        },{
            name:"uid"
        },{
            name:"src"
        },{
            name:"width"
        },{
            name:"height"
        },{
            name:"src_normal"
        },{
            name:"src_small"
        }]),
        sortInfo:{
            field:"idpropiedad",
            direction:"ASC"
        }
    });
var _c=[{
    header:"ID",
    dataIndex:"idpropiedad",
    sortable:false
},{
    header:"Area",
    dataIndex:"area",
    sortable:false
},{
    header:"Precio",
    dataIndex:"precio_venta",
    sortable:false
},{
    header:"Habitaciones",
    dataIndex:"cuartos",
    sortable:false
},{
    header:"Banos",
    dataIndex:"banos",
    sortable:false
}];
var _d=new Ext.grid.GridPanel({
    store:_b,
    columns:_c,
    frame:false,
    loadMask:{
        msg:"Loading ..."
    },
    viewConfig:{
        forceFit:true,
        emptyText:"Datos no disponibles <br /> No hay propiedades registradas en esta locacion."
    },
    stripeRows:true,
    height:200,
    autoWidth:true,
    collapsible:true,
    collapsed:false,
    title:"Resultados de la b&uacute;squeda",
    bbar:new Ext.PagingToolbar({
        pageSize:50,
        store:_b
    })
    });
_d.render("data");
    var _e;
    var cc;
    var _f;
    var _10=[];
    var _11=[];
    var x="";
    Ext.apply(Ext.form.VTypes,{
        numbersonly:function(val,_12){
            var n=parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        },
        numbersonlyText:"Ingresa solo numeros aqui"
    });
    new Ext.ToolTip({
        target:"dir",
        html:"Ingresa una locacion, id de propiedad o direccion, incluyendo el pais, Ej. Villa Fontana, Nicaragua",
        title:"Ayuda",
        width:240,
        frame:true,
        autoHide:false,
        closable:true,
        draggable:true
    });
    $(":text").labelify({
        labelledClass:"labelHighlight"
    });
    var _13=new Ext.Panel({
        contentEl:"destacados",
        autoWidth:true,
        height:155,
        title:"Resultados de la b&uacute;squeda en fotos",
        titleCollapse:false,
        renderTo:"panel_destacados"
    });
    /*
    var _14=new Ext.Panel({
        contentEl:"contenedor",
        autoWidth:true,
        autoHeight:true,
        title:"Filtro b&uacute;squeda",
        titleCollapse:false,
        renderTo:"filter_panel"
    });
    */
    if(GBrowserIsCompatible()){
        function _15(_16,_17,_18,_19){
            x=_17;
            var _1a=new GMarker(_16,_19);
            GEvent.addListener(_1a,"click",function(){
                _1a.openInfoWindowHtml(_18);
            });
            _10[x]=_1a;
            _11[x]=_18;
            x="";
            return _1a;
        };
        _f=new GMap2(document.getElementById("map"));
        _f.setUIToDefault();
        _f.removeMapType(G_SATELLITE_MAP);
        _f.removeMapType(G_HYBRID_MAP);
        _f.removeMapType(G_PHYSICAL_MAP);
        var _1b;
        function _1c(){
            _1b=true;
        };
        _1b=false;
        function _1d(){
            if(!_1b){
                _1e();
            }
            _1b=false;
        };
        function _1e(){
            var b=_1f();
            var _20=Ext.Ajax.serializeForm("hb");
            var _21=Ext.urlDecode(_20);
            var _22=_21.dir;
            var _23=_21.tipo;
            var _24=_21.min_price;
            var _25=_21.max_price;
            var _26=_21.min_beds;
            var _27=_21.min_baths;
            var _28=_21.min_area;
            var _29=_21.max_beds;
            var _2a=_21.max_baths;
            var _2b=_21.max_area;
            var _2c=_21.cat;
            _2d(cc,_e,_22,_23,_24,_25,_26,_27,_28,_29,_2a,_2b,_2c,b.minX,b.minY,b.maxX,b.maxY);
        };
        GEvent.addListener(_f,"zoomend",function(){
            });
        GEvent.addListener(_f,"dragend",_1e);
        GEvent.addListener(_f,"moveend",_1d);
        GEvent.addListener(_f,"infowindowopen",_1c);
        function _2e(){
            var _2f=_1f();
            var _30=_2f.maxX-_2f.minX;
            var _31=_2f.maxY-_2f.minY;
            _f.queryCenter=_f.getCenter();
            _f.maxY=_2f.maxY+_31*0.2;
            _f.maxX=_2f.maxX+_30*0.2;
            _f.minY=_2f.minY-_31*0.2;
            _f.minX=_2f.minX-_30*0.2;
        };
        var _32={
            lat:"25.79",
            lng:"4.21"
        };
        Ext.Ajax.request({
            url:"/index/centermap"+"?"+_a,
            params:fb,
            success:function(_33,_34){
                try{
                    var rs=Ext.util.JSON.decode(_33.responseText);
                    _32=rs.data[0];
                    _f.setCenter(new GLatLng(_32.lat,_32.lng),12);
                    var b=_1f();
                    _2d(_32.cc,_32.city,"","","","","","","","","","","",b.minX,b.minY,b.maxX,b.maxY);
                }
                catch(err){
                    _f.setCenter(new GLatLng(_32.lat,_32.lng),1);
                }
            },
            failure:function(_35,_36){
                _f.setCenter(new GLatLng(_32.lat,_32.lng),1);
            }
        });
    function _1f(){
        var _37=_f.getBounds();
        var _38=_37.getSouthWest();
        _38=_38.lng();
        var _39=_37.getSouthWest();
        _39=_39.lat();
        var _3a=_37.getNorthEast();
        _3a=_3a.lng();
        var _3b=_37.getNorthEast();
        _3b=_3b.lat();
        return {
            minX:_38,
            maxX:_3a,
            minY:_39,
            maxY:_3b
        };
    };
    var dir=Ext.get("dir");
    var cat=Ext.get("cat");
    var _3c=Ext.get("rent");
    var buy=Ext.get("buy");
    var _3d=Ext.get("min_price");
    var _3e=Ext.get("max_price");
    var _3f=Ext.get("min_beds");
    var _40=Ext.get("min_baths");
    var _41=Ext.get("min_area");
    var _42=Ext.get("max_beds");
    var _43=Ext.get("max_baths");
    var _44=Ext.get("max_area");
    function _45(m,min,max){
        var _46=parseInt(min,10);
        var _47=parseInt(max,10);
        var _48=m.substring(4);
        var _49=m.substring(3,0);
        if(_49=="min"){
            if((_46>_47)&&(_47>=0)){
                Ext.getDom("max_"+_48).value=0;
            }
        }else{
            if(_49=="max"){
                if((_47<_46)&&(_46>=0)){
                    Ext.getDom("min_"+_48).value=0;
                }
            }
        }
    };
    function _4a(num){
        var _4b=false;
        if(Ext.isEmpty(num)){
            _4b=true;
        }else{
            _4b=Ext.num(num,false);
            if(_4b<0){
                _4b=false;
            }
        }
        return _4b;
    };
    _3d.on("blur",function(){
        if(_4a(this.getValue())){
        }else{
    }
    });
    _3e.on("blur",function(){
        if(_4a(this.getValue())){
        }else{
    }
    });

    if(_3f) {
        _3f.on("blur",function(){
            if(_4a(this.getValue())){
            }else{
        }
        });
    }
    if(_42) {
        _42.on("blur",function(){
            if(_4a(this.getValue())){
            }else{
        }
        });
    }
    if(_40) {
        _40.on("blur",function(){
            if(_4a(this.getValue())){
            }else{
        }
        });
    }
    if(_43) {
        _43.on("blur",function(){
            if(_4a(this.getValue())){
            }else{
        }
        });
    }
    if(_41) {
        _41.on("blur",function(){
            if(_4a(this.getValue())){
            }else{
        }
        });
    }
    if(_44) {
        _44.on("blur",function(){
            if(_4a(this.getValue())){
            }else{
        }
        });
    }

    cat.on("change",function(){
        var _4c=this.getValue();
        var _4d=Ext.getDom("min_beds");
        var _4e=Ext.getDom("min_baths");
        var _4f=Ext.getDom("max_beds");
        var _50=Ext.getDom("max_baths");
        switch(_4c){
            case "CB":
            case "EB":
            case "EO":
                _4d.setAttribute("disabled","disabled");
                _4e.removeAttribute("disabled");
                _4f.setAttribute("disabled","disabled");
                _50.removeAttribute("disabled");
                break;
            case "TE":
                _4d.setAttribute("disabled","disabled");
                _4e.setAttribute("disabled","disabled");
                _4f.setAttribute("disabled","disabled");
                _50.setAttribute("disabled","disabled");
                break;
            default:
                _4d.removeAttribute("disabled");
                _4e.removeAttribute("disabled");
                _4f.removeAttribute("disabled");
                _50.removeAttribute("disabled");
        }
    });

    function _2d(cc,_51,_52,_53,_54,_55,_56,_57,_58,_59,_5a,_5b,_5c,_5d,_5e,_5f,_60){
        _b.load({
            params:{
                "pais":cc,
                "ciudad":_51,
                "dir":_52,
                "tipo":_53,
                "min_price":_54,
                "max_price":_55,
                "min_beds":_56,
                "min_baths":_57,
                "min_area":_58,
                "max_beds":_59,
                "max_baths":_5a,
                "max_area":_5b,
                "cat":_5c,
                "minX":_5d,
                "minY":_5e,
                "maxX":_5f,
                "maxY":_60,
                "start":0,
                "limit":50
            }
        });
};

/**
 * Para ubicar en el mapa.
 */
var findGM=Ext.get("btnFgm");
findGM.on("click",function(){
    var b=_1f(); // Recuperar las coordenadas del mapa
alert("click");
    _toGM.baseParams={
        "dir":_dir,
        "minX":b.minX,
        "minY":b.minY,
        "maxX":b.maxX,
        "maxY":b.maxY
        };

    var city;
    var _frmSerialized=Ext.Ajax.serializeForm("frmFgm"); // Serializar el formulario
    var _frmDecoded=Ext.urlDecode(_frmSerialized); // Decodificar los valores de los elementos del formulario
    var _dir=_frmDecoded.dir; // Recuperar la direccion proporcionada por el usuario
    if(_frmDecoded.dir=="Ej. Villa Fontana, Nicaragua"){
        _dir="";
    }

    var firstPlaceMark;
    var firstPlaceMarkAccuracy;
    var statusCode;
    var country;
    if(!_63(_dir)){
        if(_65(_dir)){
            loadToGM(country,city,_dir);
        }else{
            _62.getLocations(_dir,function(response){
                if(!response){
                    Ext.MessageBox.alert("Alerta","No fue posible contactar al servicio Geocode");
                }else{
                    statusCode=response.Status.code;
                    if(statusCode==200){
                        if(response.Placemark.length==1){
                            firstPlaceMark=response.Placemark[0];
                            firstPlaceMarkAccuracy=firstPlaceMark.AddressDetails.Accuracy;
                            if(!response.Placemark[0].address){
                            }else{
                                dir=response.Placemark[0].address;
                            }
                            switch(firstPlaceMarkAccuracy){
                                case 0:
                                    Ext.MessageBox.alert("Alerta","La direccion es ambigua, agregar el pais u otra referencia a la direccion, <br />mejorara los resultados de la busqueda");
                                    break;
                                default:
                                    country=!firstPlaceMark.AddressDetails.Country.CountryNameCode?"":firstPlaceMark.AddressDetails.Country.CountryNameCode;
                                    var firstPlaceMarkAddressParts=firstPlaceMark.address.split(",");
                                    if(!firstPlaceMarkAddressParts){
                                        city="";
                                    }else{
                                        city=!firstPlaceMarkAddressParts[firstPlaceMarkAddressParts.length-2]?"":firstPlaceMarkAddressParts[firstPlaceMarkAddressParts.length-2];
                                        if(country=="US"){
                                            if((city.length==3)||(city.length==2)){
                                                city=firstPlaceMarkAddressParts[firstPlaceMarkAddressParts.length-3];
                                            }
                                        }
                                    }
                                    var p=response.Placemark[0].Point.coordinates;
                                    _f.setCenter(new GLatLng(p[1],p[0]),12);
                                    var b=_1f();
                                    loadToGM(country,city,_dir,b.minX,b.minY,b.maxX,b.maxY);
                                    break;
                            }
                        }else{
                            Ext.MessageBox.alert("Alerta","La direccion es ambigua, agregar el pais u otra referencia a la direccion, <br />mejorara los resultados de la busqueda");
                        }
                    }else{
                        if(_1[statusCode]){
                            var _7c=_1[statusCode];
                        }
                        Ext.MessageBox.alert("Alerta","No se pudo encontrar:  \""+city+"\" "+_7c);
                    }
                }
            });
        }
    }else{
        loadToGM(country,city,_dir,b.minX,b.minY,b.maxX,b.maxY);
    }
});

var b=Ext.get("bt");
    b.on("click",function(){
        /*
        _45("min_price",_3d.getValue(),_3e.getValue());
        _45("max_price",_3d.getValue(),_3e.getValue());
        _45("min_beds",_3f.getValue(),_42.getValue());
        _45("max_beds",_3f.getValue(),_42.getValue());
        _45("min_baths",_40.getValue(),_43.getValue());
        _45("max_baths",_40.getValue(),_43.getValue());
        _45("min_area",_41.getValue(),_44.getValue());
        _45("max_area",_41.getValue(),_44.getValue());
        */
        _61();
    });
    var rst=Ext.get("rs");
    rst.on("click",function(){
        Ext.getDom("hb").reset();
    });
    var _62=new GClientGeocoder();
    function _63(_64){
        if(null==_64||""==_64){
            return true;
        }
        return false;
    };
    function _65(val){
        var n=parseInt(val);
        return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
    };
    function _61(){
        var _66;
        var _67=Ext.Ajax.serializeForm("hb");
        var _68=Ext.urlDecode(_67);
        var _69=_68.dir;
        if(_68.dir=="Ej. Villa Fontana, Nicaragua"){
            _69="";
        }else{
        }
        var _6a=_68.tipo;
        var _6b=_68.min_price;
        var _6c=_68.max_price;
        var _6d=_68.min_beds;
        var _6e=_68.min_baths;
        var _6f=_68.min_area;
        var _70=_68.max_beds;
        var _71=_68.max_baths;
        var _72=_68.max_area;
        var _73=_68.cat;
        var _74;
        var _75;
        var _76;
        var _77;
        var _78;
        var _79;
        var _7a;
        var cc;
        if(!_63(_69)){
            if(_65(_69)){
                _2d(cc,_66,_69,_6a,_6b,_6c,_6d,_6e,_6f,_70,_71,_72,_73);
            }else{
                _62.getLocations(_69,function(_7b){
                    if(!_7b){
                        Ext.MessageBox.alert("Alerta","No fue posible contactar al servicio Geocode");
                    }else{
                        _7a=_7b.Status.code;
                        if(_7a==200){
                            if(_7b.Placemark.length==1){
                                _78=_7b.Placemark[0];
                                _79=_78.AddressDetails.Accuracy;
                                if(!_7b.Placemark[0].address){
                                }else{
                                    dir=_7b.Placemark[0].address;
                                }
                                switch(_79){
                                    case 0:
                                        Ext.MessageBox.alert("Alerta","La direccion es ambigua, agregar el pais u otra referencia a la direccion, <br />mejorara los resultados de la busqueda");
                                        break;
                                    default:
                                        cc=!_78.AddressDetails.Country.CountryNameCode?"":_78.AddressDetails.Country.CountryNameCode;
                                        var l=_78.address.split(",");
                                        if(!l){
                                            _66="";
                                        }else{
                                            _66=!l[l.length-2]?"":l[l.length-2];
                                            if(cc=="US"){
                                                if((_66.length==3)||(_66.length==2)){
                                                    _66=l[l.length-3];
                                                }
                                            }
                                        }
                                        var p=_7b.Placemark[0].Point.coordinates;
                                        _f.setCenter(new GLatLng(p[1],p[0]),12);
                                        var b=_1f();
                                        _2d(cc,_66,_69,_6a,_6b,_6c,_6d,_6e,_6f,_70,_71,_72,_73,b.minX,b.minY,b.maxX,b.maxY);
                                        break;
                                }
                            }else{
                                Ext.MessageBox.alert("Alerta","La direccion es ambigua, agregar el pais u otra referencia a la direccion, <br />mejorara los resultados de la busqueda");
                            }
                        }else{
                            if(_1[_7a]){
                                var _7c=_1[_7a];
                            }
                            Ext.MessageBox.alert("Alerta","No se pudo encontrar:  \""+_69+"\" "+_7c);
                        }
                    }
                });
            }
        }else{
            var b=_1f();
            _2d(cc,_66,_69,_6a,_6b,_6c,_6d,_6e,_6f,_70,_71,_72,_73,b.minX,b.minY,b.maxX,b.maxY);
        }
    };
    var nav=new Ext.KeyNav(Ext.getDom("dir"),{
        "enter":_61,
        "scope":this
    });
    var _7d;
    var _7e;
    var _7f;
    var _80;
    var _81;
    var _82;
    var id;
    var _83;
    var _84;
    var _85;
    var _86;
    var _87;
    var _88;
    var _89;
    _b.on("load",function(s,r){
        _f.clearOverlays();
        Ext.each(r,function(rec){
            _7d=rec.data.descripcion;
            _7e=rec.data.precio_venta;
            _80=rec.data.banos;
            _81=rec.data.cuartos;
            _7f=rec.data.pais;
            id=rec.data.idpropiedad;
            _83=rec.data.area;
            _87=rec.data.foto;
            switch(rec.data.estatus){
                case "1":
                    _82="Vendida";
                    break;
                case "2":
                    _82="Alquilada";
                    break;
                case "3":
                    _82="Activa";
                    break;
                case "4":
                    _82="Inactiva";
                    break;
            }
            _84=rec.data.lat;
            _85=rec.data.lng;
            var _8a="<table cellpadding=\"10\" cellspacing=\"5\" class=\"details\">"+"<tr>"+"<td valign=\"top\">"+"<div class=\"photo_wrapper\">"+"<img alt=\"\" src=\""+_87+"\" height=\"87\" width=\"116\" />"+"</div>"+"</td>"+"<td valign=\"top\" nowrap=\"nowrap\">"+"<ul>"+"<li><strong>Estatus:</strong>"+_82+"</li>"+"<li><strong>Cuartos:</strong>"+_81+"</li>"+"<li><strong>Banos:</strong>"+_80+"</li>"+"<li><strong>Area:</strong>"+_83+"</li>"+"</ul>"+"</td>"+"</tr>"+"<tr>"+"<td colspan=\"2\"><a href=\"https://apps.facebook.com/hmproject/mihousebook/edit?propiedad="+id+"\" target=\"_top\">Mas informacion</a></td>"+"</tr>"+"</table>";
            _86=new GLatLng(_84,_85);
            _89=_15(_86,id,_8a,_3);
            _f.addOverlay(_89);
        });
        var _8b=new Ext.XTemplate("<tpl for=\".\">","<tpl for=\"data\">","<tpl if=\"this.noSrc(src) == true\">","<img src=\"/img/no_photoCercanas.jpg\" width=\"123\" height=\"55\" id=\"{idpropiedad}\" />","</tpl>","<tpl if=\"this.noSrc(src) == false\">","<img src=\"{src}\" width=\"{width}\" height=\"{height}\" id=\"{idpropiedad}\" />","</tpl>","</tpl>","</tpl>",{
            compiled:true,
            noSrc:function(src){
                return src==null;
            }
        });
    _8b.overwrite("gallery",r);
        $("div.scrollable").scrollable({
            size:4
        });
        $("#gallery > img").click(function(){
            var z=$(this).attr("id");
            if(z==null||z==""){
            }else{
                _10[z].openInfoWindowHtml(_11[z]);
            }
        });
        var _8c=new Ext.XTemplate("<div id=\"destacadas\">","<div id=\"b1\" class=\"block_left\">","<div class=\"fleft\">","<tpl for=\".\">","<tpl for=\"data\">","<tpl if=\"this.noSrc(src_normal) == false\">","<a href=\"https://apps.facebook.com/housebooksite/hmproject/edit?propiedad={idpropiedad}\" target=\"_top\"><img src=\"{src_normal}\"  width=\"120\" height=\"90\"/></a>","</tpl>","<tpl if=\"this.noSrc(src_normal) == true\">","<a href=\"https://apps.facebook.com/hmproject/mihousebook/edit?propiedad={idpropiedad}\" target=\"_top\"><img src=\"/img/no_photoDestacada.jpg\" width=\"120\" height=\"90\"/></a>","</tpl>","</tpl>","</tpl>","</div>","</div>",{
            compiled:true,
            noSrc:function(src){
                return src==null;
            }
        });
    _8c.overwrite("destacadas",r);
});
var j;
_d.on("rowclick",function(s){
    var sm=this.getSelectionModel();
    if(sm.hasSelection()){
        var sel=sm.getSelected();
        j=sel.data.idpropiedad;
        _10[j].openInfoWindowHtml(_11[j]);
    }
});
}else{
    Ext.MessageBox.alert("Alerta","Su browser no esta soportado por Google Maps");
}
_toGM.on("beforeload",function(s){
    var b=_1f(); // Recuperar las coordenadas del mapa
    var _frmSerialized=Ext.Ajax.serializeForm("frmFgm"); // Serializar el formulario
    var _frmDecoded=Ext.urlDecode(_frmSerialized); // Decodificar los valores de los elementos del formulario
    var _dir=_frmDecoded.dir; // Recuperar la direccion proporcionada por el usuario
    _toGM.baseParams={
        "dir":_dir,
        "minX":b.minX,
        "minY":b.minY,
        "maxX":b.maxX,
        "maxY":b.maxY
        };
});
_b.on("beforeload",function(s){
    var b=_1f();
    var _8d=Ext.Ajax.serializeForm("hb");
    var _8e=Ext.urlDecode(_8d);
    var _8f=_8e.dir;
    var _90=_8e.tipo;
    var _91=_8e.min_price;
    var _92=_8e.max_price;
    var _93=_8e.min_beds;
    var _94=_8e.min_baths;
    var _95=_8e.min_area;
    var _96=_8e.max_beds;
    var _97=_8e.max_baths;
    var _98=_8e.max_area;
    var _99=_8e.cat;
    _b.baseParams={
        "dir":_8f,
        "tipo":_90,
        "min_price":_91,
        "max_price":_92,
        "min_beds":_93,
        "min_baths":_94,
        "min_area":_95,
        "max_beds":_96,
        "max_baths":_97,
        "max_area":_98,
        "cat":_99,
        "minX":b.minX,
        "minY":b.minY,
        "maxX":b.maxX,
        "maxY":b.maxY
        };
});
});

