Ext.onReady(function() {
    //var mini= new GOverviewMapControl();
    Ext.BLANK_IMAGE_URL = '/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();

    if ( Ext.isGecko ) { Ext.get('destacados').applyStyles("margin: 13px 0;"); }

    var reasons = [];
    reasons[G_GEO_SUCCESS] = "Success";
    reasons[G_GEO_MISSING_ADDRESS] = "Falta direccion: La direccion ingresada se perdio o no se ingrese valor.";
    reasons[G_GEO_UNKNOWN_ADDRESS] = "Direccion desconocida: No se pudo encontrar la correspondiente locacion geografica para la direccion ingresada.";
    reasons[G_GEO_UNAVAILABLE_ADDRESS] = "Direccion no disponible: Las coordenadas geograficas de la direccion no pueden ser retornadas debido a razones legales o contractuales.";
    reasons[G_GEO_BAD_KEY] = "Clave API erronea: La clave API es erronea o invalida no coincide con la generada para este dominio.";
    reasons[G_GEO_TOO_MANY_QUERIES] = "Demasiadas consultas: La cuota diaria de peticiones al servicio Geocode ha sido excedida para este sitio";
    reasons[G_GEO_SERVER_ERROR] = "Error del servidor: La peticion al servidor no pudo ser procesada con exito.";

    // iconos para marcador default.
    var hb_icon = new GIcon(G_DEFAULT_ICON);
	
    hb_icon.image = '/img/house.png';
    //hb_icon.shadow = '/img/shadow.png';
    hb_icon.iconSize = new GSize(27, 32);
    var markerOptions = {
        icon: hb_icon
    };

    function getUrlVars() {
        var map = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
            function(m, key, value) {
                map[key] = value;
            });
        return map;
    }
    var prueba = getUrlVars();
    var fb = Ext.urlDecode(Ext.urlEncode(prueba));
    var fb1 = Ext.urlEncode(prueba);

    var propDataStore = new Ext.data.Store({
        //url: '/index/getmarks',
        url: '/index/getmarks' + '?' + fb1,
        baseParams: fb,
        //autoLoad: true,
        reader: new Ext.data.JsonReader({
            root: 'rows',
            id: 'idpropiedad'
        },
        [{
            name: 'idpropiedad'
        },

        {
            name: 'nombre'
        },

        {
            name: 'descripcion'
        },

        {
            name: 'direccion'
        },

        {
            name: 'pais'
        },

        {
            name: 'estado'
        },

        {
            name: 'ciudad'
        },

        {
            name: 'zip_code'
        },

        {
            name: 'precio_venta'
        },

        {
            name: 'precio_alquiler'
        },

        {
            name: 'tipo_enlistamiento'
        },

        {
            name: 'fecha_ingreso'
        },

        {
            name: 'ultima_modificacion'
        },

        {
            name: 'tags'
        },

        {
            name: 'area'
        },

        {
            name: 'cuartos'
        },

        {
            name: 'banos'
        },

        {
            name: 'tamano_lote'
        },

        {
            name: 'destacada'
        },

        {
            name: 'roomate'
        },

        {
            name: 'estatus'
        },

        {
            name: 'tipo_propiedad'
        },

        {
            name: 'lat'
        },

        {
            name: 'lng'
        },

        {
            name: 'foto'
        },

        {
            name: 'uid'
        },
        {
            name: 'src'
        },
        {
            name: 'width'
        },
        {
            name: 'height'
        },
        {
            name: 'src_normal'
        },
        {
            name: 'src_small'
        }]),
        sortInfo: {
            field: 'idpropiedad',
            direction: 'ASC'
        }
    });

    var propCM = [{
        header: 'ID',
        dataIndex: 'idpropiedad',
        sortable: false
    },
    {
        header: 'Area',
        dataIndex: 'area',
        sortable: false
    },
    {
        header: 'Precio',
        dataIndex: 'precio_venta',
        sortable: false
    },
    {
        header: 'Habitaciones',
        dataIndex: 'cuartos',
        sortable: false
    },
    {
        header: 'Banos',
        dataIndex: 'banos',
        sortable: false
    }];

    var gridProp = new Ext.grid.GridPanel({
        store: propDataStore,
        columns: propCM,
        frame: false,
        loadMask: {
            msg: 'Loading ...'
        },
        viewConfig: {
            forceFit: true,
            emptyText: 'Datos no disponibles <br /> No hay propiedades registradas en esta locacion.'
        },
        stripeRows: true,
        height: 200,
        autoWidth: true,
        collapsible: true,
        collapsed: false,
        title: 'Resultados de la b&uacute;squeda',
        bbar: new Ext.PagingToolbar({
            pageSize: 50,
            store: propDataStore
        })

    });
    gridProp.render('data');

    var city;
    var cc;

    var map;
    var gmarkers = [];
    var htmls = [];
    var x = '';

    /*  Formulario de Busqueda   */

    Ext.apply(Ext.form.VTypes, {
        numbersonly: function(val, field) {
            var n = parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        },
        numbersonlyText: 'Ingresa solo numeros aqui'
    //minmax: function(val, field) {}
    });

    new Ext.ToolTip({
        target: 'dir',
        html: 'Ingresa una locacion, id de proiedad o direccion, incluyendo el pais, Ej. Villa Fontana, Nicaragua',
        title: 'Ayuda',
        width: 240,
        frame: true,
        autoHide: false,
        closable: true,
        draggable: true
    });

    //$('#dir').labelify({labelledClass: "labelHighlight"});
    $(":text").labelify({
        labelledClass: "labelHighlight"
    });
    
    var dest_panel = new Ext.Panel({
        contentEl: 'destacados',
        //autoScroll: true,
        autoWidth: true,
        height: 155,
        //iconCls: 'panelIcon',
        title: 'Resultados de la b&uacute;squeda en fotos',
        titleCollapse: false,
        renderTo: 'panel_destacados'        
    });

    var filter_panel = new Ext.Panel({
        contentEl: 'contenedor',
        //autoScroll: true,
        autoWidth: true,
        autoHeight: true,
        //iconCls: 'panelIcon',
        title: 'Filtro b&uacute;squeda',
        titleCollapse: false,
        renderTo: 'filter_panel'
    });  


    /* fin formulario de busqueda */

    if (GBrowserIsCompatible()) {
        function createMarker(point, name, html, opts) {
            x = name;
            var marker = new GMarker(point, opts);
            GEvent.addListener(marker, "click",
                function() {
                    marker.openInfoWindowHtml(html);
                });
            // save the info we need to use later for the grid
            gmarkers[x] = marker;
            htmls[x] = html;
            x = '';
            ////console.dir(gmarkers);
            return marker;
        }

        map = new GMap2(document.getElementById("map"));
        map.setUIToDefault();
        map.removeMapType(G_SATELLITE_MAP);
        map.removeMapType(G_HYBRID_MAP);
        map.removeMapType(G_PHYSICAL_MAP);

        //map.addControl(new GOverviewMapControl({hide: true}));
        //map.addControl(mini);
        //mini.hide(true);
       
        
       
       	
        var opening_window;
        function infoWindowOpen() {
            opening_window = true;
        }
        opening_window = false;
        function moveEnd() {
            if (!opening_window) {
                dragEnd();
            }
            opening_window = false;
        }
        function dragEnd() {
            var b = mapBounds();
            var dataurl = Ext.Ajax.serializeForm('hb');
            var data = Ext.urlDecode(dataurl);
            var dir1 = data.dir;
            var tipo1 = data.tipo;
            var min_price1 = data.min_price;
            var max_price1 = data.max_price;

            var beds1 = data.min_beds;
            var baths1 = data.min_baths;
            var area1 = data.min_area;

            var beds2 = data.max_beds;
            var baths2 = data.max_baths;
            var area2 = data.max_area;

            var cat1 = data.cat;
            //getProps('', '', '', '', '', '', '', '', '', '', b.minX, b.minY, b.maxX, b.maxY);
            getProps(cc, city, dir1, tipo1, min_price1, max_price1, beds1, baths1, area1, beds2, baths2, area2, cat1, b.minX, b.minY, b.maxX, b.maxY);
        }

        GEvent.addListener(map, "zoomend",
            function() {
            //update_map();
            });
        GEvent.addListener(map, "dragend", dragEnd);
        GEvent.addListener(map, "moveend", moveEnd);
        GEvent.addListener(map, "infowindowopen", infoWindowOpen);

        function updateMapCenter() {
            var bounds = mapBounds();
            var width = bounds.maxX - bounds.minX;
            var height = bounds.maxY - bounds.minY;
            map.queryCenter = map.getCenter();
            map.maxY = bounds.maxY + height * 0.2;
            map.maxX = bounds.maxX + width * 0.2;
            map.minY = bounds.minY - height * 0.2;
            map.minX = bounds.minX - width * 0.2;
        }

        var centermap = {
            lat: "25.79",
            lng: "4.21"
        };
        //  Centramos el Mapa, segun el IP del Cliente.
        Ext.Ajax.request({
            //url: '/index/centermap',
            url: '/index/centermap' + '?' + fb1,
            params: fb,
            success: function(result, response) {
                try {
                    var rs = Ext.util.JSON.decode(result.responseText);
                    centermap = rs.data[0];
                    // fijamos el centro del mapa.
                    map.setCenter(new GLatLng(centermap.lat, centermap.lng), 12);

                    var b = mapBounds();
                    //console.dir(b);
                    //getProps(centermap.city, '');
                    getProps(centermap.cc, centermap.city, '', '', '', '', '', '', '', '', '', '', '', b.minX, b.minY, b.maxX, b.maxY);

                } catch(err) {
                    //console.log('ocurrio un error al centrar el mapa');
                    //console.dir(result.responseText);
                    map.setCenter(new GLatLng(centermap.lat, centermap.lng), 1);
                }

            },
            failure: function(result, response) {
                map.setCenter(new GLatLng(centermap.lat, centermap.lng), 1);
            }
        });

        function mapBounds() {
            var bounds = map.getBounds();
            var min_x = bounds.getSouthWest();
            min_x = min_x.lng();
            var min_y = bounds.getSouthWest();
            min_y = min_y.lat();
            var max_x = bounds.getNorthEast();
            max_x = max_x.lng();
            var max_y = bounds.getNorthEast();
            max_y = max_y.lat();
            return {
                minX: min_x,
                maxX: max_x,
                minY: min_y,
                maxY: max_y
            };
        }

        var dir = Ext.get('dir');
        var cat = Ext.get('cat');
        var rent = Ext.get('rent');
        var buy = Ext.get('buy');

        var min_price = Ext.get('min_price');
        var max_price = Ext.get('max_price');

        var beds1 = Ext.get('min_beds');
        var baths1 = Ext.get('min_baths');
        var area1 = Ext.get('min_area');

        var beds2 = Ext.get('max_beds');
        var baths2 = Ext.get('max_baths');
        var area2 = Ext.get('max_area');

        function min_max(m, min, max) {
            var min_value = parseInt(min, 10);
            var max_value = parseInt(max, 10);
            var vals = m.substring(4);
            var tipo = m.substring(3,0);
            if (tipo == 'min') {
                if ((min_value > max_value) && (max_value >= 0)) {
                    Ext.getDom('max_' + vals).value = 0;
                }
            } else if (tipo == 'max') {
                if ((max_value < min_value) && (min_value >= 0)) {
                    Ext.getDom('min_' + vals).value = 0;
                }
            }
        }

        function isNum(num) {
            var test = false;
            if (Ext.isEmpty(num)) {
                test = true;
            } else {
                test = Ext.num(num, false);
                //var tx = (test >= 0) ? true : false;
                //console.info('dato es ' + typeof(test));
                //console.info('test = '+  tx);
                if (test < 0) {
                    test = false;
                }

            }
            return test;
        }

        min_price.on('blur', function() {
            if (isNum(this.getValue())) {
            //console.info('ok: min_price = ' +this.getValue());
            //if (this.hasClass('error')) {
            //    this.removeClass('error');
            //}
            } else {
        //console.info('fail: min_price = ' +this.getValue());
        //this.addClass('error');
        }
        });

        max_price.on('blur', function() {
            if (isNum(this.getValue())) {
            //console.info('ok: min_price = ' +this.getValue());
            //if (this.hasClass('error')) {
            //    this.removeClass('error');
            //}
            } else {
        //console.info('fail: min_price = ' +this.getValue());
        //this.addClass('error');
        }
        });

        beds1.on('blur',function() {
            if (isNum(this.getValue())) {
            //console.info('ok: min_price = ' +this.getValue());
            //if (this.hasClass('error')) {
            //    this.removeClass('error');
            //}
            } else {
        //console.info('fail: min_price = ' +this.getValue());
        //this.addClass('error');
        }
        });

        beds2.on('blur', function() {
            if (isNum(this.getValue())) {
            //console.info('ok: min_price = ' +this.getValue());
            //if (this.hasClass('error')) {
            //    this.removeClass('error');
            // }
            } else {
        //console.info('fail: min_price = ' +this.getValue());
        //this.addClass('error');
        }
        });

        baths1.on('blur', function() {
            if (isNum(this.getValue())) {
            //console.info('ok: min_price = ' +this.getValue());
            //if (this.hasClass('error')) {
            //    this.removeClass('error');
            //}
            } else {
        //console.info('fail: min_price = ' +this.getValue());
        //this.addClass('error');
        }
        });

        baths2.on('blur', function() {
            if (isNum(this.getValue())) {
            //console.info('ok: min_price = ' +this.getValue());
            //if (this.hasClass('error')) {
            //    this.removeClass('error');
            // }
            } else {
        //console.info('fail: min_price = ' +this.getValue());
        //this.addClass('error');
        }
        });

        area1.on('blur', function() {
            if (isNum(this.getValue())) {
            //console.info('ok: min_price = ' +this.getValue());
            //if (this.hasClass('error')) {
            //    this.removeClass('error');
            // }
            } else {
        //console.info('fail: min_price = ' +this.getValue());
        //this.addClass('error');
        }
        });

        area2.on('blur', function() {
            if (isNum(this.getValue())) {
            //console.info('ok: min_price = ' +this.getValue());
            //if (this.hasClass('error')) {
            //    this.removeClass('error');
            //}
            } else {
        //console.info('fail: min_price = ' +this.getValue());
        //this.addClass('error');
        }
        });



        /*
        dir.on('blur',
        function() {
            if (Ext.isEmpty(this.getValue())) {
                this.addClass('error');

                //console.info('fail: sin direccion ');
            } else {
                if (this.hasClass('error')) {
                    this.removeClass('error');
                }
                //console.info('ok: - direccion ingresada: ' + this.getValue());
            }
        });
*/
        cat.on('change', function() {
            var tipo_prop = this.getValue();
            var rooms1 = Ext.getDom('min_beds');
            var banos1 = Ext.getDom('min_baths');
            var rooms2 = Ext.getDom('max_beds');
            var banos2 = Ext.getDom('max_baths');
            switch (tipo_prop) {
                case 'CB':
                case 'EB':
                case 'EO':
                    rooms1.setAttribute("disabled", "disabled");
                    banos1.removeAttribute('disabled');
                    rooms2.setAttribute("disabled", "disabled");
                    banos2.removeAttribute('disabled');
                    break;
                case 'TE':
                    rooms1.setAttribute("disabled", "disabled");
                    banos1.setAttribute("disabled", "disabled");
                    rooms2.setAttribute("disabled", "disabled");
                    banos2.setAttribute("disabled", "disabled");
                    break;
                default:
                    rooms1.removeAttribute('disabled');
                    banos1.removeAttribute('disabled');
                    rooms2.removeAttribute('disabled');
                    banos2.removeAttribute('disabled');
            }
        });

        /***  fin formulario busqued ***/
        // funcion para recarga dinamica del grid.
        //function getProps(city, cc) {
        function getProps(cc, city, dir1, tipo1, min_price1, max_price1, beds1, baths1, area1, beds2, baths2, area2, cat1, minX, minY, maxX, maxY) {
            propDataStore.load({
                params: {
                    'pais': cc,
                    'ciudad': city,
                    'dir': dir1,
                    //'buy': buy1,
                    //'rent': rent1,
                    'tipo': tipo1,
                    'min_price': min_price1,
                    'max_price': max_price1,
                    'min_beds': beds1,
                    'min_baths': baths1,
                    'min_area': area1,
                    'max_beds': beds2,
                    'max_baths': baths2,
                    'max_area': area2,
                    'cat': cat1,
                    'minX': minX,
                    'minY': minY,
                    'maxX': maxX,
                    'maxY': maxY,
                    'start': 0,
                    'limit': 50
                }
            });
        }

        var b = Ext.get('bt');
        b.on('click', function() {
            min_max('min_price', min_price.getValue(), max_price.getValue());
            min_max('max_price', min_price.getValue(), max_price.getValue());
            min_max('min_beds', beds1.getValue(), beds2.getValue());
            min_max('max_beds', beds1.getValue(), beds2.getValue());
            min_max('min_baths', baths1.getValue(), baths2.getValue());
            min_max('max_baths', baths1.getValue(), baths2.getValue());
            min_max('min_area', area1.getValue(), area2.getValue());
            min_max('max_area', area1.getValue(), area2.getValue());
            buscar();
        });

        /* resetear formulario busqueda */
        var rst = Ext.get('rs');
        rst.on('click', function(){
            Ext.getDom('hb').reset();
        });


        var geocoder = new GClientGeocoder();
        //var search_button = Ext.get('search_button');

        function isEmpty(inputStr) {
            if (null == inputStr || "" == inputStr) {
                return true;
            }
            return false;
        }

        function check_id(val) {
            var n = parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        }

        //var search_button1 = Ext.get('search_button1');
        function buscar() {
            // var query = Ext.get('buscar').getValue();
            // console.log('consulta: ' + query);

            var city;
            //var data = form_busqueda.getForm().getValues();
            var dataurl = Ext.Ajax.serializeForm('hb');
            var data = Ext.urlDecode(dataurl);
            //console.dir(data);
            var dir1 = data.dir;
            if (data.dir =='Ej. Villa Fontana, Nicaragua') {
                dir1 = ''
            } else {
	    	
            }
            //var buy1 = data.buy;
            //var rent1 = data.rent;
            var tipo1 = data.tipo;
            var min_price1 = data.min_price;
            var max_price1 = data.max_price;
            var beds1 = data.min_beds;
            var baths1 = data.min_baths;
            var area1 = data.min_area;

            var beds2 = data.max_beds;
            var baths2 = data.max_baths;
            var area2 = data.max_area;

            var cat1 = data.cat;
            var minX;
            var minY;
            var maxX;
            var maxY;

            //var query;
            var locacion;
            var precision;
            var code;
            //var cn;
            var cc;

            if (!isEmpty(dir1)) {

                if (check_id(dir1)) {
                    //console.log('query es un id de propiedad ' + dir);
                    getProps(cc, city, dir1, tipo1, min_price1, max_price1, beds1, baths1, area1, beds2, baths2, area2, cat1);

                } else {
                    //console.log('locacion: ' +dir);
                    geocoder.getLocations(dir1,
                        function(response) {
                            if (!response) {
                                // //console.log('no hubo respuesta de geocode');
                                Ext.MessageBox.alert('Alerta', 'No fue posible contactar al servicio Geocode');
                            } else {
                                code = response.Status.code;
                                if (code == 200) {
                                    // verificamos resultados:
                                    // 1 -- resultado unico,
                                    // > 1 -- multiples resultados.
                                    if (response.Placemark.length == 1) {
                                        locacion = response.Placemark[0];
                                        //console.dir(locacion);
                                        precision = locacion.AddressDetails.Accuracy;
                                        //cn = !locacion.AddressDetails.Country.CountryName ? '': locacion.AddressDetails.Country.CountryName;

                                        //console.dir(locacion.address.split(','));

                                        if (!response.Placemark[0].address) {
                                        //dir;
                                        } else {
                                            dir = response.Placemark[0].address;

                                        }

                                        switch (precision) {
                                            case 0:
                                                Ext.MessageBox.alert('Alerta', 'La direccion es ambigua, agregar el pais u otra referencia a la direccion, <br />mejorara los resultados de la busqueda');
                                                break;

                                            default:
                                                cc = !locacion.AddressDetails.Country.CountryNameCode ? '': locacion.AddressDetails.Country.CountryNameCode;
                                                // city opciones
                                                // AddressDetails.Country.AdministrativeArea.AdministrativeAreaName
                                                // AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName

                                                var l = locacion.address.split(',');
                                                if (!l) {
                                                    city = '';
                                                } else {
                                                    city = !l[l.length - 2] ? '': l[l.length - 2];
                                                    if (cc == 'US') {
                                                        if ((city.length == 3) || (city.length == 2)) {
                                                            city = l[l.length - 3];
                                                        }
                                                    //city= l[l.length-3];
                                                    }
                                                }
                                                var p = response.Placemark[0].Point.coordinates;
                                                map.setCenter(new GLatLng(p[1], p[0]), 12);
                                                var b = mapBounds();
                                                getProps(cc, city, dir1, tipo1, min_price1, max_price1, beds1, baths1, area1, beds2, baths2, area2, cat1, b.minX, b.minY, b.maxX, b.maxY);
                                                //console.dir(b);
                                                break;
                                        }
                                    } else {
                                        // tenemos multiples resultados, presentamos lista
                                        // console.log( "resultados geocode: " + response.Placemark.length);
                                        // console.dir(response.Placemark);
                                        Ext.MessageBox.alert('Alerta', 'La direccion es ambigua, agregar el pais u otra referencia a la direccion, <br />mejorara los resultados de la busqueda');
                                    }
                                } else {
                                    if (reasons[code]) {
                                        var reason = reasons[code]
                                    }
                                    Ext.MessageBox.alert('Alerta', 'No se pudo encontrar:  "' + dir1 + '" ' + reason);
                                }
                            }

                        });
                }
            } else {
                //Ext.MessageBox.alert('Alerta', 'Direccion esta vacia');
                // cambio, usar vista actual para filtrar

                var b = mapBounds();
                getProps(cc, city, dir1, tipo1, min_price1, max_price1, beds1, baths1, area1, beds2, baths2, area2, cat1, b.minX, b.minY, b.maxX, b.maxY);

            }
        }

        // Centramos el Mapa segun los resultados de la busqueda del Usuario.

        //search_button.on('click', buscar);
        var nav = new Ext.KeyNav(Ext.getDom('dir'), {
            'enter': buscar,
            'scope': this
        });

        // google info window
        var descripcion;
        var precio;
        var pais;
        var banos;
        var cuartos;
        var estatus;
        var id;
        var area;
        var latitud;
        var longitud;
        var point;
        var foto;
        var tmpl;
        var marker;
        //map.clearOverlays(); // limpiar mapa, antes de cargar marcadores.
        // s -> store, r -> record
        propDataStore.on('load', function(s, r) {
            map.clearOverlays();
            Ext.each(r, function(rec) {
                descripcion = rec.data.descripcion;
                precio = rec.data.precio_venta;
                banos = rec.data.banos;
                cuartos = rec.data.cuartos;
                pais = rec.data.pais;
                id = rec.data.idpropiedad;
                area = rec.data.area;
                foto = rec.data.foto;

                switch (rec.data.estatus) {
                    case '1':
                        estatus = 'Vendida';
                        break;
                    case '2':
                        estatus = 'Alquilada';
                        break;
                    case '3':
                        estatus = 'Activa';
                        break;
                    case '4':
                        estatus = 'Inactiva';
                        break;
                }

                latitud = rec.data.lat;
                longitud = rec.data.lng;

                var tmpl = '<table cellpadding="10" cellspacing="5" class="details">' + '<tr>' + '<td valign="top">' + '<div class="photo_wrapper">' + '<img alt="" src="' + foto + '" height="87" width="116" />' + '</div>' + '</td>' + '<td valign="top" nowrap="nowrap">' + '<ul>' + '<li><strong>Estatus:</strong>' + estatus + '</li>' + '<li><strong>Cuartos:</strong>' + cuartos + '</li>' + '<li><strong>Banos:</strong>' + banos + '</li>' + '<li><strong>Area:</strong>' + area + '</li>' + '</ul>' + '</td>' + '</tr>' + '<tr>' + '<td colspan="2"><a href="https://apps.facebook.com/housebooksite/mihousebook/edit?propiedad=' + id + '" target="_top">Mas informacion</a></td>' + '</tr>' + '</table>';

                point = new GLatLng(latitud, longitud);
                marker = createMarker(point, id, tmpl, markerOptions);
                map.addOverlay(marker);

            });

            var tmpl_img = new Ext.XTemplate('<tpl for=".">', '<tpl for="data">', '<tpl if="this.noSrc(src) == true">', '<img src="/img/no_photoCercanas.jpg" width="123" height="55" id="{idpropiedad}" />', '</tpl>', '<tpl if="this.noSrc(src) == false">', '<img src="{src}" width="{width}" height="{height}" id="{idpropiedad}" />', '</tpl>', '</tpl>',
                //'<script type="javascript">$(function() { $("div.scrollable").scrollable(); });</script>',
                '</tpl>', {
                    compiled: true,
                    noSrc: function(src) {
                        return src == null
                    }
                });
            tmpl_img.overwrite('gallery', r);
            $("div.scrollable").scrollable({
                size: 4
            });
            ////console.log(rec.data.lat + ", " + rec.data.lng);
            $('#gallery > img').click(function() {
                var z = $(this).attr('id');
                // console.log(typeof(z));
                // console.log(z);
                if (z == null || z == '') {
                // no id
                } else {
                    gmarkers[z].openInfoWindowHtml(htmls[z]);
                }

            });
            /*
            var tpl3 = new Ext.XTemplate(
                '<table  border="0" align="left" cellpadding="5" cellspacing="5">',
                '<tr>',
                '<tpl for=".">',
                '<tpl for="data">',
                '<tpl if="[xindex] == 1">',
                '<tpl if="this.noSrc(src_normal) == false">',
                '<td valign="top"><a href="/mihousebook/edit?propiedad={idpropiedad}"><img src="{src_normal}"  width="182" height="81"/></a></td>',
                '</tpl>',
                '<tpl if="this.noSrc(src_normal) == true">',
                '<td valign="top"><a href="/mihousebook/edit?propiedad={idpropiedad}"><img src="/img/no_photoDestacada.jpg"  width="182" height="81"/></a></td>',
                '</tpl>',
                '</tpl>',
                '<tpl if="[xindex] == 2">',
                '<tpl if="this.noSrc(src_normal) == false">',
                '<td valign="top"><a href="/mihousebook/edit?propiedad={idpropiedad}"><img src="{src_normal}"  width="182" height="81"/></a></td>',
                '</tpl>',
                '<tpl if="this.noSrc(src_normal) == true">',
                '<td valign="top"><a href="/mihousebook/edit?propiedad={idpropiedad}"><img src="/img/no_photoDestacada.jpg"  width="182" height="81"/></a></td>',
                '</tpl>',
                '<td valign="top"><table  border="0" cellpadding="5" cellspacing="5">',
                '<tr>',
                '</tpl>',
                '<tpl if="[xindex] == 3">',
                '<tpl if="this.noSrc(src_small) == false">',
                '<td valign="top"><a href="/mihousebook/edit?propiedad={idpropiedad}"><img src="{src_small}"  width="75" height="40"/></a></td>',
                '</tpl>',
                '<tpl if="this.noSrc(src_small) == true">',
                '<td valign="top"><a href="/mihousebook/edit?propiedad={idpropiedad}"><img src="/img/no_photoDestacadaSmall.jpg" width="75" height="40" /></a></td>',
                '</tpl>',
                '</tpl>',
                '<tpl if="[xindex] == 4">',
                '<tpl if="this.noSrc(src_small) == false">',
                '<td valign="top"><a href="/mihousebook/edit?propiedad={idpropiedad}"><img src="{src_small}"  width="75" height="40"/></a></td>',
                '</tpl>',
                '<tpl if="this.noSrc(src_small) == true">',
                '<td valign="top"><a href="/mihousebook/edit?propiedad={idpropiedad}"><img src="/img/no_photoDestacadaSmall.jpg" width="75" height="40" /></a></td>',
                '</tpl>',
                '</tr>',
                '<tr>',
                '</tpl>',
                '<tpl if="[xindex] == 5">',
                '<tpl if="this.noSrc(src_small) == false">',
                '<td valign="top"><a href="/mihousebook/edit?propiedad={idpropiedad}"><img src="{src_small}"  width="75" height="40"/></a></td>',
                '</tpl>',
                '<tpl if="this.noSrc(src_small) == true">',
                '<td valign="top"><a href="/mihousebook/edit?propiedad={idpropiedad}"><img src="/img/no_photoDestacadaSmall.jpg" width="75" height="40" /></a></td>',
                '</tpl>',
                '</tpl>',
                '<tpl if="[xindex] == 6">',
                '<tpl if="this.noSrc(src_small) == false">',
                '<td valign="top"><a href="/mihousebook/edit?propiedad={idpropiedad}"><img src="{src_small}"  width="75" height="40"/></a></td>',
                '</tpl>',
                '<tpl if="this.noSrc(src_small) == true">',
                '<td valign="top"><a href="/mihousebook/edit?propiedad={idpropiedad}"><img src="/img/no_photoDestacadaSmall.jpg" width="75" height="40" /></a></td>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</tr>',
                '</table></td>',
                '</tr>',
                '</table>',
                {
                    compiled: true,
                    noSrc: function(src) {
                        return src == null
                    }
                });
            tpl3.overwrite('destacadas', r);
*/

            var tpl3 = new Ext.XTemplate(
                '<div id="destacados">',
                '<div id="b1" class="block_left">',
                '<div class="fleft">',
                '<tpl for=".">',
                '<tpl for="data">',
                '<tpl if="[xindex] == 1">',
                '<tpl if="this.noSrc(src_normal) == false">',
                '<a href="https://apps.facebook.com/housebooksite/mihousebook/edit?propiedad={idpropiedad}" target="_top"><img src="{src_normal}"  width="182" height="81"/></a>',
                '</tpl>',
                '<tpl if="this.noSrc(src_normal) == true">',
                '<a href="https://apps.facebook.com/housebooksite/mihousebook/edit?propiedad={idpropiedad}" target="_top"><img src="/img/no_photoDestacada.jpg" width="182" height="81"/></a>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</div>',
                '<div class="rg">',
                '<tpl for=".">',
                '<tpl for="data">',
                '<tpl if="[xindex] == 2">',
                '<tpl if="this.noSrc(src_normal) == false">',
                '<a href="https://apps.facebook.com/housebooksite/mihousebook/edit?propiedad={idpropiedad}" target="_top"><img src="{src_normal}"  width="182" height="81"/></a>',
                '</tpl>',
                '<tpl if="this.noSrc(src_normal) == true">',
                '<a href="https://apps.facebook.com/housebooksite/mihousebook/edit?propiedad={idpropiedad}" target="_top"><img src="/img/no_photoDestacada.jpg" width="182" height="81"/></a>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</div>',
                '  </div>',
                '  <div id="b2" class="block_right" >',
                '<div id="sb1">',
                '<div class="img1">',
                '<tpl for=".">',
                '<tpl for="data">',
                '<tpl if="[xindex] == 3">',
                '<tpl if="this.noSrc(src_small) == false">',
                '<a href="https://apps.facebook.com/housebooksite/mihousebook/edit?propiedad={idpropiedad}" target="_top"><img src="{src_small}"  width="75" height="40"/></a>',
                '</tpl>',
                '<tpl if="this.noSrc(src_small) == true">',
                '<a href="https://apps.facebook.com/housebooksite/mihousebook/edit?propiedad={idpropiedad}" target="_top"><img src="/img/no_photoDestacadaSmall.jpg" width="75" height="40" /></a>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</div>',
                '<div class="img2">',
                '<tpl for=".">',
                '<tpl for="data">',
                '<tpl if="[xindex] == 4">',
                '<tpl if="this.noSrc(src_small) == false">',
                '<a href="https://apps.facebook.com/housebooksite/mihousebook/edit?propiedad={idpropiedad}" target="_top"><img src="{src_small}"  width="75" height="40"/></a>',
                '</tpl>',
                '<tpl if="this.noSrc(src_small) == true">',
                '<a href="https://apps.facebook.com/housebooksite/mihousebook/edit?propiedad={idpropiedad}" target="_top"><img src="/img/no_photoDestacadaSmall.jpg" width="75" height="40" /></a>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</div>',
                '</div>',
                '<div id="sb2">',
                '<div class="img3">',
                '<tpl for=".">',
                '<tpl for="data">',
                '<tpl if="[xindex] == 5">',
                '<tpl if="this.noSrc(src_small) == false">',
                '<a href="https://apps.facebook.com/housebooksite/mihousebook/edit?propiedad={idpropiedad}" target="_top"><img src="{src_small}"  width="75" height="40"/></a>',
                '</tpl>',
                '<tpl if="this.noSrc(src_small) == true">',
                '<a href="https://apps.facebook.com/housebooksite/mihousebook/edit?propiedad={idpropiedad}" target="_top"><img src="/img/no_photoDestacadaSmall.jpg" width="75" height="40" /></a>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</div>',
                '<div class="img4">',
                '<tpl for=".">',
                '<tpl for="data">',
                '<tpl if="[xindex] == 6">',
                '<tpl if="this.noSrc(src_small) == false">',
                '<a href="https://apps.facebook.com/housebooksite/mihousebook/edit?propiedad={idpropiedad}" target="_top"><img src="{src_small}"  width="75" height="40"/></a>',
                '</tpl>',
                '<tpl if="this.noSrc(src_small) == true">',
                '<a href="https://apps.facebook.com/housebooksite/mihousebook/edit?propiedad={idpropiedad}" target="_top"><img src="/img/no_photoDestacadaSmall.jpg" width="75" height="40" /></a>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</tpl>',
                '</div>',
                '</div>',
                '</div>',
                '</div>',
                {
                    compiled: true,
                    noSrc: function(src) {
                        return src == null
                    }
                });
            tpl3.overwrite('destacadas', r);




        });


        var j;
        gridProp.on('rowclick', function(s) {
            var sm = this.getSelectionModel();
            if (sm.hasSelection()) {
                var sel = sm.getSelected();
                j = sel.data.idpropiedad;
                gmarkers[j].openInfoWindowHtml(htmls[j]);
            }
        });

    } else {
        Ext.MessageBox.alert('Alerta', 'Su browser no esta soportado por Google Maps');
    }

    propDataStore.on('beforeload', function(s) {
        //console.info('ejecutando evento beforeload ');
        var b = mapBounds();
        var dataurl = Ext.Ajax.serializeForm('hb');
        var data = Ext.urlDecode(dataurl);
        var dir1 = data.dir;
        var tipo1 = data.tipo;
        var min_price1 = data.min_price;
        var max_price1 = data.max_price;
        var beds1 = data.min_beds;
        var baths1 = data.min_baths;
        var area1 = data.min_area;

        var beds2 = data.max_beds;
        var baths2 = data.max_baths;
        var area2 = data.max_area;

        var cat1 = data.cat;

        propDataStore.baseParams = {
            'dir': dir1,
            //'buy': buy1,
            //'rent': rent1,
            'tipo': tipo1,
            'min_price': min_price1,
            'max_price': max_price1,
            'min_beds': beds1,
            'min_baths': baths1,
            'min_area': area1,
            'max_beds': beds2,
            'max_baths': baths2,
            'max_area': area2,
            'cat': cat1,
            'minX': b.minX,
            'minY': b.minY,
            'maxX': b.maxX,
            'maxY': b.maxY

        }
    });

});
