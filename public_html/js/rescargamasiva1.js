/*
 * Grid para el listado de aprobaci�n de cr�ditos del cliente
 *
 */
Ext.onReady(function(){
    Ext.BLANK_IMAGE_URL = '/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = "side";

    Ext.apply(Ext.form.VTypes, {
        daterange : function(val, field) {
            var date = field.parseDate(val);

            if(!date){
                return;
            }
            if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                var start = Ext.getCmp(field.startDateField);
                start.setMaxValue(date);
                start.validate();
                this.dateRangeMax = date;
            }
            else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                var end = Ext.getCmp(field.endDateField);
                end.setMinValue(date);
                end.validate();
                this.dateRangeMin = date;
            }
            /*
         * Always return true since we're only using this vtype to set the
         * min/max allowed values (these are tested for after the vtype test)
         */
            return true;
        },
        numbersonly: function(val, field) {
            var n = parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        },
        numbersonlyText: 'Ingresa solo numeros aqui'
    });

    function isEmpty(inputStr) {
        if (null == inputStr || "" == inputStr) {
            return true;
        }
        return false;
    }

    Ext.namespace('Ext.userdata');
    Ext.userdata.status = [['0', 'inactiva'], ['1', 'activa']];
    var stStore = new Ext.data.SimpleStore({
        fields: ['status', 'label'],
        data: Ext.userdata.status
    });

    function getUrlVars() {
        var map = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            map[key] = unescape(value);
        });
        return map;
    }

    var prueba = getUrlVars();
    var fb = Ext.urlEncode(prueba);
    var fb1 = Ext.urlDecode(Ext.urlEncode(prueba));

    var clienteDataStore = new Ext.data.Store({
        url: '/mihousebook/getresultcarga' + '?' + fb,
        baseParams:fb1,
        reader: new Ext.data.JsonReader({
            root: 'rows',
            totalProperty: 'results',
            id: 'CodigoPropiedad'
        }, [
        {
            name: 'CodigoPropiedad',
            type: 'int'
        },
        {
            name: 'NombrePropiedad',
            type: 'string'
        },
        {
            name: 'NombreCategoria',
            type: 'string'
        },
        {
            name: 'Transaccion',
            type: 'string'
        }
        ])
        ,
        remoteSort:true
        ,
        sortInfo: {
            field: 'CodigoPropiedad',
            direction: 'ASC'
        }
    });

    var crepagingBar = new Ext.PagingToolbar({
        pageSize:50,
        store: clienteDataStore,
        displayInfo: true,
        beforePageText : document.getElementById('_page').value,
        afterPageText  : document.getElementById('_of').value +" {0}",
        firstText      : document.getElementById('_firtspage').value,
        prevText       : document.getElementById('_previouspage').value,
        nextText       : document.getElementById('_nextpage').value,
        lastText       : document.getElementById('_lastpage').value,
        refreshText    : document.getElementById('_refresh').value,
        emptyMsg: document.getElementById('no_property').value,
        displayMsg: document.getElementById('_showin').value + ' {0} - {1} '+ document.getElementById('_of').value +' {2}'

    });

    var clienteCM = [
    {
        header: 'ID',
        dataIndex: 'CodigoPropiedad',
        sortable: true
    },
    {
        header: document.getElementById('_title').value,
        dataIndex: 'NombrePropiedad',
        sortable: true
    },
    {
        header: document.getElementById('property_type').value,
        dataIndex: 'NombreCategoria',
        sortable: true
    },
    {
        header: document.getElementById('listing_type').value,
        dataIndex: 'Transaccion',
        sortable: true
    }
    ];

 var proptoolbar = new Ext.Toolbar({
        items: [
        ]
    });

    var dtgridCliente = new Ext.grid.GridPanel({
        store: clienteDataStore,
        columns: clienteCM,
        frame: true,
        loadMask: {
            msg: document.getElementById('load_grid').value
        },
        viewConfig: {
            forceFit:true,
            emptyText: document.getElementById('no_result').value
        },
        renderTo: 'carga_masiva',
        stripeRows: true,
        height: 500,
        autoWidth: true,
        collapsed: false,
        title: document.getElementById('property_listing').value,
        tbar: proptoolbar,
        bbar: crepagingBar,
        iconCls: 'creditos_panel'
    });

    clienteDataStore.load({
        params:{
            start:0,
            limit:50
        }
    });
    
});