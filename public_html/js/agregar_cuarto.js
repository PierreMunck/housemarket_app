$(function() {
    $("#date").datepicker({minDate: new Date(), changeYear : true, changeMonth : true,
                            showOn: "button",
                            buttonImage: "/img/calendar.gif",
                            buttonImageOnly: true,
                            dateFormat : "yy-mm-dd"
                           });

    $('#rentaUS, #serviceUS').blur(function(){
        $('#rentaUS, #serviceUS').formatCurrency({symbol: ''});
    });

});

// Map Search Control and Stylesheet
window._uds_msw_donotrepair = true;

var map;
var geocoder;
var address;
var place;
var lat2;
var long2;
var cn;
var fb;
var marker;
var countrycode;

function __translate(s) {
    if (typeof(i18n)!='undefined' && i18n[s]) {
        return i18n[s];
    }
    return s;
}

/**
 * Recupera variables de entorno
 */
function __environment(s) {
    if (typeof(env)!='undefined' && env[s]) {
        return env[s];
    }
    return s;
}

function getUrlVars() {
    var map = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        map[key] = unescape(value);
    });
    return map;
}
cn = getUrlVars();
fb = Ext.urlEncode(cn);
var fbparams = Ext.urlDecode(Ext.urlEncode(cn));

var fbParameters=Ext.urlDecode(Ext.urlEncode(cn));

//	console.log(fb);
function initialize() {

    map = new GMap2(document.getElementById("map"));
    map.setUIToDefault();

    geocoder = new GClientGeocoder();

    // Here we set the cache to use the UsCitiesCache custom cache.
    // Se Puede desactivar la cache pasando null al metodo setCache() del objeto GClientGeocoder
    geocoder.setCache(new CapitalCitiesCache());

    var centermap = {
        lat: "25.79",
        lng: "4.21"
    };

    var isNew = $("#isNew");

    if(isNew && isNew.val() == "N") {
        centermap.lat = $("#latitud").val();
        centermap.lng=$("#longitud").val();
    }

    Ext.Ajax.request({
        url: '/roommates/centermap',
        success:function (result, response) {
            //console.log('success: ' + result.responseText);
            try {
                var rs = Ext.util.JSON.decode(result.responseText);
                centermap = rs.data[0];
                if($("#isNew") && $("#isNew").val() == "N") { // Usar las coordenadas ya indicadas
                    centermap.lat = $("#latitud").val();
                    centermap.lng=$("#longitud").val();
                }
                //console.log(centermap.lat + ' ' + centermap.lng );

                point = new GLatLng(centermap.lat,centermap.lng);
                marker = new GMarker(point,markerOptions);
                map.setCenter(point, 11);
                map.addOverlay(marker);

                countrycode=centermap.cc;

                GEvent.addListener(marker, "dragend", function() {
                    lat2 = marker.getPoint().lat();
                    long2 = marker.getPoint().lng();
                    asignarCoordenadas(lat2,long2);
                    geocoder.getLocations(marker.getPoint(), addMarkerAddressToMap);
                });
                asignarCoordenadas(centermap.lat,centermap.lng);
            } catch (err) {
                map.setCenter(new GLatLng(centermap.lat, centermap.lng), 1);
            }

        },
        failure: function(result,response) {
            alert(result.responseText);
            map.setCenter(new GLatLng(centermap.lat, centermap.lng), 1);
        }
    });

    map.disableContinuousZoom();
    map.disableDoubleClickZoom();
    map.disableScrollWheelZoom();

    // GEvent.addListener(map, "click", getAddress);

}

// Create our "tiny" marker icon
tinyIcon = new GIcon();
tinyIcon.image = "https://labs.google.com/ridefinder/images/mm_20_red.png";
tinyIcon.shadow = "https://labs.google.com/ridefinder/images/mm_20_shadow.png";
tinyIcon.shadowSize = new GSize(22, 20);
tinyIcon.iconAnchor = new GPoint(6, 20);
tinyIcon.infoWindowAnchor = new GPoint(5, 1);

// iconos para marcador default.
var hb_icon = new GIcon(G_DEFAULT_ICON);
hb_icon.image = '/img/house_blue.png';
hb_icon.iconSize = new GSize(24, 33);
var markerOptions ={
    icon:hb_icon,
    draggable: true
};

function trim (myString){
    return myString.replace(/^\s+/g,'').replace(/\s+$/g,'');
}

function getAddress(overlay, latlng) {
    if (latlng != null) {
        // Set up our GMarkerOptions object
        address = latlng;
        lat2= address['y'];
        long2= address['x'];
        asignarCoordenadas(lat2,long2);
        geocoder.getLocations(latlng, addAddressToMap);
    }
}

function addMarkerAddressToMap(response) {
    if (!response || response.Status.code != 200) {
        Ext.MessageBox.alert(__translate("G_GEO_ALERT_TITLE"), __translate("G_GEO_ALERT_STATUS_CODE") + response.Status.code);
    }
    else
    {
        place = response.Placemark[0];
        code_country=place.AddressDetails.Country.CountryNameCode;
        ProcessPlaceMark(place);
    }
}

// addAddressToMap() is called when the geocoder returns an
// answer.  It adds a marker to the map with an open info window
// showing the nicely formatted version of the address and the country code.
function addAddressToMap(response) {
    map.clearOverlays();

    if (!response || response.Status.code != 200) {
        Ext.MessageBox.alert(__translate("G_GEO_ALERT_TITLE"), __translate("G_GEO_ALERT_STATUS_CODE") + response.Status.code);
    }
    else
    {
        place = response.Placemark[0];
        marker = new GMarker(address,markerOptions);
        map.addOverlay(marker);

        map.setCenter(address, 13);

        GEvent.addListener(marker, "dragend", function() {
            lat2 = marker.getPoint().lat();
            long2 = marker.getPoint().lng();

            asignarCoordenadas(lat2,long2);
            geocoder.getLocations(marker.getPoint(), addMarkerAddressToMap);
        });

        lat2 = marker.getPoint().lat();
        long2 = marker.getPoint().lng();

        code_country=place.AddressDetails.Country.CountryNameCode;
        ProcessPlaceMark(place);

    }
}

function ProcessPlaceMark(place){
    // Limpiar las variables
    var country = "";
    var countryName = "";
    var town = "";
    var region = "";
    var subregion = "";
    var postcode = "";
    var street = "";
    var intersection = "";
    var address = "";

    if(place != null) { // Se ha proporcionado un placemark correcto
        if(place.AddressDetails != null) { // Hay detalle de direccion
            accuracy = place.AddressDetails.Accuracy;
            var placeAddress = place.address;
            var addressData = placeAddress.split(',');

            address = placeAddress;
            // Intersecciones
            if(place.AddressDetails.Country != null &&
               place.AddressDetails.Country.AdministrativeArea != null &&
               place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea != null &&
               place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality != null &&
               place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare != null &&
               place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.ThoroughfareName != null) {
                 intersection = place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.ThoroughfareName;
            } else if(place.AddressDetails.Country != null &&
               place.AddressDetails.Country.AdministrativeArea != null &&
               place.AddressDetails.Country.AdministrativeArea.Locality != null &&
               place.AddressDetails.Country.AdministrativeArea.Locality.DependentLocality != null &&
               place.AddressDetails.Country.AdministrativeArea.Locality.DependentLocality.Thoroughfare != null &&
               place.AddressDetails.Country.AdministrativeArea.Locality.DependentLocality.Thoroughfare.ThoroughfareName != null){
                intersection = place.AddressDetails.Country.AdministrativeArea.Locality.DependentLocality.Thoroughfare.ThoroughfareName;
            }
            if(place.AddressDetails.Country != null &&
               place.AddressDetails.Country.AdministrativeArea != null &&
               place.AddressDetails.Country.AdministrativeArea.Locality != null &&
               place.AddressDetails.Country.AdministrativeArea.Locality.DependentLocality != null &&
               place.AddressDetails.Country.AdministrativeArea.Locality.DependentLocality.DependentLocalityName != null) {
                 street = place.AddressDetails.Country.AdministrativeArea.Locality.DependentLocality.DependentLocalityName;
            }
            // Codigo Postal
            if(place.AddressDetails.Country != null &&
               place.AddressDetails.Country.AdministrativeArea != null &&
               place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea != null &&
               place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality != null &&
               place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.PostalCode != null &&
               place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.PostalCode.PostalCodeNumber != null) {
                 postcode = place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.PostalCode.PostalCodeNumber;
            }
            // Ciudad
            if(place.AddressDetails.Country != null &&
               place.AddressDetails.Country.AdministrativeArea != null) {
                   if( place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea != null &&
                       place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality != null &&
                       place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName != null) {
                            town = place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
                    } else if( place.AddressDetails.Country.AdministrativeArea.Locality != null &&
                               place.AddressDetails.Country.AdministrativeArea.Locality.LocalityName != null) {
                            town = place.AddressDetails.Country.AdministrativeArea.Locality.LocalityName;
                    }
               }
            // SubRegion
            if(place.AddressDetails.Country != null &&
               place.AddressDetails.Country.AdministrativeArea != null &&
               place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea != null &&
               place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.SubAdministrativeAreaName != null ) {
                subregion = place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.SubAdministrativeAreaName;
            }
            // Region (Departamento)
            if(place.AddressDetails.Country != null &&
               place.AddressDetails.Country.AdministrativeArea != null &&
               place.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName != null) {
                region = place.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName;
            }
            // Pais
            if(place.AddressDetails.Country != null) {
                country = place.AddressDetails.Country.CountryNameCode;
                countryName = place.AddressDetails.Country.CountryName;
            }

        }
    }
    // Actualizar los campos.
    $("#pais").val(countryName);
    $("#zipCode").val(country);
    $("#estado").val(region);
    $("#ciudad").val(town);
    $("#direccion").val(address);
    $("#codigoPostal").val(postcode);

}

// Geocoding Cache
// CapitalCitiesCache is a custom cache that extends the standard GeocodeCache.
// We call apply(this) to invoke the parent's class constructor.
function CapitalCitiesCache() {
    GGeocodeCache.apply(this);
}

    function validate_date(date){
        var pattern= new RegExp(/\b\d{4}[-]\d{1,2}[-]\d{1,2}\b/);
        return pattern.test(date);
    }

   function validar_Fecha(fecha){
     var Fecha= new String(fecha)
    //Cadena Año
     var Dia= new String(Fecha.substring(Fecha.lastIndexOf("-")+1,Fecha.length))
    //Cadena Mes
     var Mes= new String(Fecha.substring(Fecha.indexOf("-")+1,Fecha.lastIndexOf("-")))
     //Cadena Dia
     var Ano= new String(Fecha.substring(0,Fecha.indexOf("-")))

    //Valido el año
     if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){
         return false;
     }
    //Valido el Mes
     if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){
         return false;
     }
    //Valido el Dia
     if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){
         alert('Dia inválido')
         return false
     }
     if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {
         if (Mes==2 && Dia > 28 || Dia>30) {
             alert('Día inválido')
             return false
         }
     }
     //para que envie los datos, quitar las  2 lineas siguientes
   return true
 }

// Assigns an instance of the parent class as a prototype of the
// child class, to make sure that all methods defined on the parent
// class can be directly invoked on the child class.
CapitalCitiesCache.prototype = new GGeocodeCache();

function asignarCoordenadas(latitud,longitud){
    $("#latitud").val(latitud);
    $("#longitud").val(longitud);
}

Ext.onReady(function(){
    Ext.QuickTips.init();
    
    // Inicializar el mapa si es nuevo.
    if (GBrowserIsCompatible()) {
        initialize();
    }else {
        Ext.Msg.alert(__translate("G_GEO_ALERT_TITLE"), __translate("G_GEO_ALERT_GOOGLE_NOT_SUPPORTED"));
    }

    /*Tratamiento a longitud de caracteres en descripcion*/
    var saveDown = Ext.get('btSaveDown');
    var savePubDown = Ext.get('btSavePubDown');
    var unPubDown = Ext.get('btSaveUnPubDown');
    var chkTerms = $("#chkTerms");
    var publicada = $("#publicado");
    
    var noRequestComments = $("#noRequestComments");
    var requestComments = $("#requestComments");
    var shareInfo = new Object();
    shareInfo.active = false; // No esta activa la ventana del share
    
    if(publicada) {
        isPublicada = publicada.val();
    }

    function assignSaveEvents(){
        saveDown.on('click', function(){
            $('#registro').val("S");
            $('#publicado').val("N");
            if($('#isNew').val() == "S"){
                $('#estadoCuarto').val(63);
            }
            FB.Canvas.scrollTo(0,0);
            $('#frm').submit();
        });
        if(savePubDown){
            savePubDown.on('click', function(){
                $('#registro').val("S");
                $('#publicado').val("S");
                $('#estadoCuarto').val(64);
                FB.Canvas.scrollTo(0,0);
                $('#frm').submit();
            });
        }
        if(unPubDown){
            unPubDown.on('click', function(){
                $('#registro').val("S");
                $('#publicado').val("N");
                $('#estadoCuarto').val(65);
                FB.Canvas.scrollTo(0,0);
                $('#frm').submit();
            });
        }
    }

    if(chkTerms.length > 0){//Si existe el checkbox chkTerms
        saveDown.removeAllListeners();
        saveDown.set({href:'#',
                      'class':'button_disable'});
        savePubDown.set({href:'#',
                      'class':'button_disable'});
        chkTerms.click(function(){
            if(chkTerms.get(0).checked == false){
                saveDown.removeAllListeners();
                saveDown.set({href:'#',
                              'class':'button_disable'});
                if(savePubDown){
                    savePubDown.removeAllListeners();
                    savePubDown.set({href:'#',
                                'class':'button_disable'});
                }
            }else{
                assignSaveEvents();
                saveDown.set({href:'#top',
                              'class': 'button'});
                savePubDown.set({href:'#top', 'class': 'button'})
            }
        });
    }else{
        assignSaveEvents();
    }

    var servicio = $("#servicio");
    var servicioUS=$("#serviceUS");
    servicio.click(function(){
        if (servicio.get(0).checked) {
            servicioUS.attr("disabled", true);
            servicioUS.get(0).value = '';
        } else {
             servicioUS.removeAttr("disabled", true);
        }
    })

    function updateGoogleMap() {
        map.clearOverlays();
        dir = document.getElementById('dir');
        var query = dir.value;
        geocoder.getLatLng(query, function(response){
            if(!response){

            } else {
                point = new GLatLng(response.lat(),response.lng());
                marker = new GMarker(point,markerOptions);
                map.setCenter(point, 11);
                map.addOverlay(marker);
                asignarCoordenadas(response.lat(),response.lng());
                GEvent.addListener(marker, "dragend", function() {
                    lat2 = marker.getPoint().lat();
                    long2 = marker.getPoint().lng();
                    asignarCoordenadas(lat2,long2);
                    geocoder.getLocations(marker.getPoint(), addMarkerAddressToMap);
                });

                geocoder.getLocations(marker.getPoint(), addMarkerAddressToMap);
            }
        });
    }

    $("#btFind").click(function(){
        updateGoogleMap();
        return false;
    });

    $("#dir").keypress(function (e) {
        if (e.which == 13){
            updateGoogleMap();
            return false;
        }
        return true;
    });
   
    
    //Atandolo al evento clic de boton para eliminar anuncio
    var deleteAd = $("#del_ad");
    if(deleteAd){
        deleteAd.click(function(){
            $.colorbox({
                iframe:false,
                innerWidth:420,
                innerHeight:240,
                fixed:true,
                scrolling:false,
                top:"10%",
                overlayClose: false,
                escKey: false,
                open: false,
                href: '/dialog/delete?noframe=true&signed_request=' + signed
            });
            return false;
        })
    }
    
 //Pertenecientes a los botones del renderizado en colorbox   
 if(requestComments){
     requestComments.live('click',function(){
     shareInfo.active = true;
     $.colorbox.close();
     if(shareInfo && shareInfo.status=='success'){
          FB.ui(shareInfo.publish,function(response) {
              top.location.href=shareInfo.redirect;
              return false;
          });
     }
     })
 }

$("#confirmbtn").live('click', function(){
    FB.Canvas.scrollTo(0,0);
    var id = $("#del_ad").attr("b");
    $.ajax({
        type:"post",
        data:{signed_request: signed},
        url:"/roommates/deleteroom?id=" + id,
        beforeSend: function(){
            $("#loading").fadeIn('slow');  
        },
        error:function(jqXHR, textStatus, errorThrown){
            $('#errorbox').show();
            $('#errorbox ul').fadeIn('slow').append("<li>"+errorThrown+"</li>");
            $("#loading").fadeOut('slow'); 
        },
        dataType:"json",
        success:function(data, textStatus, jqXHR){
            var obj = {};
            obj.message= data.objeto.message;
            $('#box').fadeIn('slow').html(obj.message);
            $("#loading").fadeOut('slow');
            if(data.objeto.redirect){
                top.location.href=data.objeto.redirect;
                return false;
            }
        }
    })
      
})

$("#noRequestComments, #cancelbtn").live('click', function(event) {
    $.colorbox.close();
    if(shareInfo && shareInfo.status == 'success')
        top.location.href=shareInfo.redirect;
    return false;
});
 
var container = $('#frm #errorbox');
 $('#frm').validate({
        rules:{
            tipoAlojamiento:"required",
            estadoCuarto:"required",
            tipoPropiedad:"required",
            espacioDisponible:{
                required:true,
                maxlength: function(){
                   return $("#espacioDisponible option:selected").val() <= $("#maxCapacidad option:selected").val()
                }
            },
            maxCapacidad:"required",
            tipoCuarto:"required",
            tipoBano:"required",
            rentaUS:"required",
            stayMonth:"required",
            serviceUS:{
             required:"#servicio:unchecked"
            },
            date:"required",
            titulo:"required",
            pais:{
                minlength:1,
                maxlength:128
            },
            estado:{
                minlength:1,
                maxlength:64
            },
            ciudad:{
                minlength:1,
                maxlength:64
            },
            direccion:{
                required:true,
                minlength:1,
                maxlength: 255

            },
            correo:{
              required:true,
              email:true
            },
            codigoPostal:{
                minlength:1,
                maxlength:64
            },
            descripcion:{
                maxlength:512
            },
            area:"required",
            fprincNew:{
                required:true,
                accept:'jpg|gif|png|bmp|jpeg'                
            },
            edadMaximaAcepta : {
                required : true
            },
            edadMinimaAcepta: {
                required: true,
                maxlength : function(){
                   return $("#edadMinimaAcepta option:selected").val() <= $("#edadMaximaAcepta option:selected").val()
                }
            },
            ocupacionAcepta: "required",
            generoAcepta : "required",
            parejaAcepta : "required",
            ninosAcepta : "required",
            mascotaAcepta : "required",
            fumadorAcepta :"required",
            // Caracteristica de habitante(s)
            edadMinimaHabitan : {
                 required: true,
                maxlength : function(){
                   return $("#edadMinimaHabitan option:selected").val() <= $("#edadMaximaHabitan option:selected").val()
                }
            },
            edadMaximaHabitan:"required",
            fumadorTengo:"required",
            idiomaTengo:"required",
            ocupacionTengo:"required",
            generoTengo:"required",
            mascotaTengo:"required",
            hijosTengo:"required"
        },
        messages:{
            tipoAlojamiento: __translate("MSG_ERROR_HOSTING_RMT"),
            estadoCuarto:__translate("MSG_ERROR_STATE_ROOM"),
            tipoPropiedad:__translate("MSG_ERROR_PROPERTY_TYPE_RMT"),
            maxCapacidad:__translate("MSG_ERROR_CAPACITY_RMT"),
            espacioDisponible:{
                required:__translate("MSG_ERROR_AVAILABLE_SPACE_RMT"),
                maxlength: __translate("MSG_ERROR_LENGTH_AVAILABLE_SPACE")
            },
            tipoCuarto: __translate("MSG_ERROR_TYPE_ROOM"),
            tipoBano:__translate("MSG_ERROR_BATH_RMT"),
            rentaUS:__translate("MSG_ERROR_RENT_RMT"),
            stayMonth:__translate("MSG_ERROR_QUANTITY_MONTH_RMT"),
            serviceUS:__translate("MSG_ERROR_AMOUNT_SERVICE_RMT"),
            date:__translate("MSG_ERROR_DATE_RMT"),
            titulo:__translate("MSG_ERROR_TITLE_RMT"),
            pais:__translate("MSG_ERROR_COUNTRY_RMT"),
            estado:__translate("MSG_ERROR_STATE_RMT"),
            ciudad:__translate("MSG_ERROR_CITY_RMT"),
            direccion:{
                required:__translate("MSG_ERROR_ADDRESS_RMT"),
                maxlength:__translate("MSG_ERROR_ADDRES_LENGTH_RMT")
            },
            correo:{
                required:__translate("MSG_ERROR_EMAIL_RMT"),
                email:__translate("MSG_ERROR_EMAIL_VALID_RMT")
            },
            codigoPostal:__translate("MSG_ERROR_ZIP"),
            descripcion:__translate("MSG_ERROR_DESCRIPTION_RMT"),
            fprincNew:{
                required:"The principal image is required",
                accept:"Select an image(png, jpg, jpeg, gif) "
            },
            edadMaximaAcepta: __translate("MSG_ERROR_MAXIMUM_AGE_RMT"),
            edadMinimaAcepta: {
                required: __translate("MSG_ERROR_MINIMUM_AGE_RMT"),
                maxlength:__translate("MSG_ERROR_MIN_AGE_LESSER_MAX_RMT")
            },
            ocupacionAcepta: __translate("MSG_ERROR_OCUPATION_RMT"),
            generoAcepta: __translate("MSG_ERROR_GENDER_RMT"),
            parejaAcepta: __translate("MSG_ERROR_COUPLE_RMT"),
            ninosAcepta: __translate("MSG_ERROR_CHILDREN_RMT"),
            mascotaAcepta : __translate("MSG_ERROR_PET_RMT"),
            fumadorAcepta : __translate("MSG_ERROR_SMOKER_RMT"),
            edadMinimaHabitan : {
                required: __translate("MSG_ERROR_MINIMUM_AGE_HAVE_RMT"),
                maxlength:__translate("MSG_ERROR_MIN_AGE_LESSER_MAX_HAVE_RMT")
            },
            edadMaximaHabitan:__translate("MSG_ERROR_MAXIMUM_AGE_HAVE_RMT"),
            fumadorTengo:__translate("MSG_ERROR_SMOKER_HAVE_RMT"),
            idiomaTengo:__translate("MSG_ERROR_LANGUAGE_HAVE_RMT"),
            ocupacionTengo:__translate("MSG_ERROR_OCUPATION_HAVE_RMT"),
            generoTengo:__translate("MSG_ERROR_GENDER_HAVE_RMT"),
            mascotaTengo:__translate("MSG_ERROR_PET_HAVE_RMT"),
            hijosTengo:__translate("MSG_ERROR_CHILDREN_HAVE_RMT")
        },
        errorElement: "li",
        errorContainer: container,
        errorPlacement: function(error, element) {
            $("ul",container).append(error);
        },
        submitHandler: function(form) {
            FB.Canvas.scrollTo(0,0);
            $(form).ajaxSubmit({
                target: "#box",
                dataType:"xml",
                beforeSubmit:function(){
                    saveDown.removeAllListeners();
                    if(savePubDown)
                        savePubDown.removeAllListeners();
                    if(unPubDown)
                        unPubDown.removeAllListeners();
                    $("#loading").fadeIn('slow');
                },
                error:function(jqXHR, textStatus, errorThrown){
                    $('#errorbox ul').fadeIn('slow').append("<li>"+errorThrown+"</li>");
                },
                success: function(responseXML) {
                    shareInfo.message= $('message', responseXML).text();
                    shareInfo.status=  $('status', responseXML).text();
                    shareInfo.redirect=$('redirect', responseXML).text();
                    shareInfo.publicar=$('publicar', responseXML).text();
                    var email = $('correo', responseXML).text();
                    
                    //Construimos la informacion para el share de facebook
                    var hrefShare = $('href', responseXML).text();
                    var propertiesShare = {
                        " " : $('conditions', responseXML).text(),
                        "Loc" : $('location', responseXML).text()
                    }
                    var attachmentShare={
                       'name': $('title', responseXML).text(),
                       'href': hrefShare,
                       'caption': $('caption', responseXML).text(),
                       'description': $('description', responseXML).text(),
                       'media':[{
                           'type': 'image',
                           'src': $('image', responseXML).text(),
                           'href': hrefShare
                       }],
                       'properties' : propertiesShare
                    };
                    shareInfo.publish = {
                        method:'stream.publish',
                        message:'',
                        attachment: attachmentShare
                    };
                    if(shareInfo){
                        if(shareInfo.status=='success'){
                            if($('#publicado').val()=='S'){
                                $.colorbox({
                                    iframe:false,
                                    innerWidth:520,
                                    innerHeight:270,
                                    fixed:true,
                                    scrolling:false,
                                    top:"10%",
                                    overlayClose: false,
                                    escKey: false,
                                    open: false,
                                    href: '/dialog/confirmshare?noframe=true&signed_request=' + signed + '&correo=' +  email,
                                    onClosed: function(){
                                        if(!shareInfo.active)//Esta definido una redireccion al cerrar el colorbox
                                           top.location.href=shareInfo.redirect; //Si no se invoca ventana del share se redirige
                                    }
                                });
                            }else{
                                top.location.href=shareInfo.redirect;
                                return false;
                            }
                        }
                            $("#loading").fadeOut('slow');
                            $('#box').fadeIn('slow').html(shareInfo.message);
                    }
                },
                complete:function(){
                    assignSaveEvents();
                    $("#loading").fadeOut('slow');
                }
            });//form.submit();

        }
    });
    
    var descripcion = $("#descripcion");
    if(descripcion) {
        descripcion.keyup(function(e){
            var valor=document.getElementById('descripcion');
            var cadena=valor.value;
            if(cadena.length>512){
                valor.value=cadena.substring(0,512);
            }
            return true;
        });

        descripcion.blur(function(e){
            var valor=document.getElementById('descripcion');
            var cadena=valor.value;
            if(cadena.length>512)
                valor.value=cadena.substring(0,512);
            return true;
        });
    }

});
