// Map Search Control and Stylesheet
window._uds_msw_donotrepair = true;

var map;
var geocoder;
var address;
var place;
var lat2;
var long2;
var cn;
var fb;

	

// iconos para marcador default.
var hb_icon = new GIcon(G_DEFAULT_ICON);
hb_icon.image = '/img/house.png';
//hb_icon.shadow = '/img/shadow.png';
hb_icon.iconSize = new GSize(27, 32);
var markerOptions ={
    icon:hb_icon,
    draggable: false
};
var markerOptionseditable ={
    icon:hb_icon,
    draggable: true
};
var options;
var searchControl;
var point;
var mapclickhandler;
var zoom;
var opening_window;
var mini=new GOverviewMapControl();
	
function getUrlVars() {
    var map = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        map[key] = value;
    });
    return map;
}
cn = getUrlVars();
fb = Ext.urlEncode(cn);
var fbparams = Ext.urlDecode(Ext.urlEncode(cn));

// obtenemos el ID de propiedad de las variables en el url dentro del iframe.
var p = fbparams.propiedad;
//console.dir(fbparams);
var idp = ((p != null) && (p.length > 0) ) ? p : "";

//	console.log(fb);

function initialize() {
    map = new GMap2(document.getElementById("mapsearch"));
        
    //map.setCenter(new GLatLng(12.144061,-86.270142), 12);
    geocoder = new GClientGeocoder();
	
    // Here we set the cache to use the UsCitiesCache custom cache.
    // Se Puede desactivar la cache pasando null al metodo setCache() del objeto GClientGeocoder
    // geocoder.setCache(new CapitalCitiesCache());
		  
     	

    var centermap = {
        lat: "25.79",
        lng: "4.21"
    };
        
    point = new GLatLng(latitud,longitud);
    marker = new GMarker(point,markerOptions);
    map.setCenter(point, 14);
		  
      

    map.setUIToDefault();
    map.removeMapType(G_SATELLITE_MAP);
    map.removeMapType(G_HYBRID_MAP);
    map.removeMapType(G_PHYSICAL_MAP);
    map.removeMapType(G_NORMAL_MAP);
	 
    map.addControl(mini);
    mini.hide(true);

    map.disableDoubleClickZoom();
    map.disableContinuousZoom();
    //map.setCenter(new GLatLng(latitud,longitud), 11);
    map.addOverlay(marker);

    options = {
        resultList : document.getElementById("results"),
        //onSearchCompleteCallback : function(searcher){alert(searcher.results.length + " results");},
        suppressZoomToBounds : true
    };


    searchControl = new google.maps.LocalSearch(options);

     
    map.addControl(searchControl);





        
		
}
var i=1;
function infoWindowOpen() {
   
    opening_window = true;
    if(i==1){
        map.setCenter(point, 14);
    }
    i++;
            
}
opening_window = false;
function moveEnd() {
           
    if (!opening_window) {
        dragEnd();
                
    }
    opening_window = false;
            
}
function dragEnd() {
           
}


function Encadenarcheckboxs(){
    var optchecks= $("#editar_propiedad").find("input[type=checkbox]");
    $.each(optchecks, function(index, element){
        var nameradio=element.name;//element.name.substring(1);
        var optchk = $("."+nameradio+(element.value));
        var optradio = $("input[name="+nameradio+ "]");
        optchk.click(function() {
            if(!this.checked){
                optradio.attr("checked", this.checked);
                optradio.attr("disabled",this.checked);
                $(".mensaje").css("display","none");
            //$(".mensaje").html("Elija una opci&oacute;n de enlistamiento");
            }else{
                optradio.attr("disabled",this.checked);
                //optradio.attr("checked",!this.checked);
                optchk.attr("checked",this.checked);
                optchk.attr("disabled",!this.checked);
                $(".mensaje").css("display","block");
                $(".mensaje").html("Deschequee su opci&oacute;n para poder elegir otra opci&oacute;n de enlistamiento ");
            }
	
        });
    });
}

        

function EncadenarcheckboxsName(name){
        
    var optchecks= $("#editar_propiedad").find("input[name="+name+"]");
	
	
        
    $.each(optchecks, function(index, element){
        var nameradio=element.name;//element.name.substring(1);
                  
        var optchk = $("."+nameradio+(index+1));
                   
        var optradio = $("input[name="+nameradio+ "]");
        optchk.click(function() {
            if(!this.checked){
                optradio.attr("checked", this.checked);
                optradio.attr("disabled",this.checked);
                searchControl.execute("''");
                i=1;
                map.setCenter(point, 14);
                $(".mensaje").html("Elija una opci&oacute;n a la vez");
            }else{
                                  
                optradio.attr("disabled",this.checked);
                //optradio.attr("checked",!this.checked);
                optchk.attr("checked",this.checked);
                optchk.attr("disabled",!this.checked);
                $(".mensaje").html("Deschequee su opci&oacute;n para poder elegir otra");
                                        
                //console.log($(this).val());
                searchControl.execute($(this).val());
                GEvent.addListener(map, "dragend", dragEnd);
                GEvent.addListener(map, "moveend", moveEnd);
                GEvent.addListener(map, "infowindowopen", infoWindowOpen);
                                        
            }
	 
        });
    });
}

function cancelar(){
    map.clearOverlays();
    var coord_latitud=$('#latitud').val();
    var coord_longitud=$('#longitud').val();

    point = new GLatLng(coord_latitud,coord_longitud);
    marker = new GMarker(point,markerOptions);
    map.setCenter(point, 14);
    map.addOverlay(marker);

    // Remove any previous maxclick handler
    if (mapclickhandler) {
        GEvent.removeListener(mapclickhandler);
    }
    var elementxname=$("#editar_propiedad").find(":input");
    $.each(elementxname, function(index, element){

        
        if(element.type=="checkbox"){
                   
            if(element.name!="hotspot"){
                $("#"+element.id).attr("disabled",true);
            }
            if($("#"+element.id).is(':checked')){
                $("#"+element.id).attr("checked",false);
            }
            if(element.name=="hotspot"){
                $("#"+element.id).attr("disabled",false);
                              
            }
        }
        if(element.type=="select-one"){
            $("#"+element.id).attr("disabled",true);
            $("#"+element.id).attr("background-color","#FFFFFF");

        }
	
        if(element.type=="textarea"){
            $("#"+element.id).attr("disabled",true);
        
        }

        if(element.type=="text"){
            if(element.name!="search"){
                $("#"+element.id).attr("disabled",true);
                $("#"+element.id).attr("background-color","#FFFFFF");
            }
                
                         
        }

        $("#descripcion").attr("disabled",true);
    });

    $("#tipo_enlistamiento"+tipo_enlistamiento).attr("checked",true);
    //$("#hotspot1").attr("disabled",false);
    $("#estatus"+tipo_status).attr("checked",true);
    $('#guardar').css("display",'none');
    $('#cancelar').css("display",'none');
    $('#editar').css("display",'inline');
    $("#cerrar_edicio_descripcion").css("display","none");
    $('.gmls').css("display",'none');


}
function asignarCoordenadas(latitud,longitud){
    $("#latitud").val(latitud);
    $("#longitud").val(longitud);
}

function addMarkerAddressToMap(response) {
    if (!response || response.Status.code != 200) {
        Ext.MessageBox.alert('Alerta', "Status Code:" + response.Status.code);
        validar(0);

    }
    else
    {
        // console.log("response de addMarkerAddressToMap");
        place = response.Placemark[0];
        code_country=place.AddressDetails.Country.CountryNameCode;
        asignarcountrycode(code_country);
     
        procesarPrecision(place,code_country);

    }
}
function asignarcountrycode(countrycode){
    $("#zip_code").val(countrycode);
    $("#country").val(countrycode);
    $("#country").change();
}

function procesarPrecision(place,codigopais){

    //	 Ext.Msg.alert('Alerta', "procesando niveles de precision....");

    if(place.AddressDetails.Country!=null){
        precision=place.AddressDetails.Accuracy;
        //console.log('Nivel de precision:'+precision);
        var direcc=place.address;
        //   console.log('Direccion:'+direcc);
        var data = direcc.split(',');
        var nparams=data.length;
        //console.log('Num. de Parametros:'+nparams);
        //console.log('Direccion:'+direcc);

        for(i=0; i<(data.length); i++){
        // console.log(data[i]);
        }
        var nom_pais;
        var ciudad;
        switch(precision){
            case 1:
                // a nivel de pais
                break;
            case 2:
                switch(nparams){
                    case 2:
                        ciudad= data[0];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(codigopais);
                        break;


                }
                if(place.AddressDetails.Country.AdministrativeArea!=null){
                    ciudad = place.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName;
                    $("#ciudad").val(ciudad);
                }
                break;
            case 3:
                switch(nparams){
                    case 2:
                        ciudad= data[0];
                        $("#ciudad").val(ciudad);
                        //console.log("Pais:"+nom_pais);
                        break;
                    case 3:
                        ciudad= data[0];
                        nom_estado= data[1];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        //console.log("Pais:"+nom_pais);
                        break;

                }
                if(place.AddressDetails.Country.AdministrativeArea!=null){
                    ciudad = place.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName;
                    $("#ciudad").val(ciudad);
                }
                break;
            case 4:
                // console.log("numero de parametros:"+nparams);
                switch(nparams){
                    case 2:
                        ciudad= data[0];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(code_country);
                        //console.log("Pais:"+nom_pais);
                        break;
                    case 3:
                        if(code_country=="NI"){
                            ciudad1= data[0];
                            ciudad2= data[1];
                            ciudad= ciudad1+","+ciudad2;
                            nom_estado=code_country;
                        }else{
                            ciudad= data[0];
                            nom_estado= data[1];
                        }
                                                            
                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        //console.log("Pais:"+nom_pais);
                        break;
                    case 4:
                        ciudad1= data[0];
                        ciudad2= data[1];
                        ciudad= ciudad1+","+ciudad2;
                        nom_estado= data[2];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        //console.log("Pais:"+nom_pais);
                        break;
                    case 5:
                        ciudad1= data[0];
                        ciudad2= data[1];
                        ciudad3= data[2];
                        ciudad= ciudad1+","+ciudad2+","+ciudad3;
                        nom_estado= data[3];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        //console.log("Pais:"+nom_pais);
                        break;

                }
                if(place.AddressDetails.Country.AdministrativeArea!=null){
                    ciudad = place.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName;
                    $("#ciudad").val(ciudad);
                //   console.log("ciudad area administrativa:"+ciudad);
                }
                break;
            case 5:
                switch(nparams){
                    case 2:
                        ciudad= data[0];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(code_country);
                        break;
                    case 3:
                        ciudad= data[0];
                        nom_estado= data[1];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        break;

                }

                if(place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea!=null){
                    ciudad =place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
                    $("#ciudad").val(ciudad);
                }
                break;
            case 6:
                switch(nparams){

                    case 2:
                        ciudad= data[0];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(code_country);
                        break;
                    case 3:
                        if(code_country=="NI"){
                            ciudad1= data[0];
                            ciudad2= data[1];
                            ciudad= ciudad1+","+ciudad2;
                        }else{
                            ciudad= data[1];
                        }
                        $("#ciudad").val(ciudad);
                        $("#estado").val(code_country);
                        break;
                    case 4:
                        if(code_country=="NI"){
                            ciudad1= data[0];
                            ciudad2= data[1];
                            ciudad= ciudad1+","+ciudad2;
                            nom_estado= code_country;
                        }else{
                            ciudad1= data[0];
                            ciudad2= data[1];
                            ciudad= ciudad1+","+ciudad2;
                            nom_estado= data[2];
                        }
                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        break;

                }
                if(place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea!=null){
                    ciudad =place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
                    $("#ciudad").val(ciudad);
                                                     
                }
                break;
            case 7:
                if(place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea!=null){
                    ciudad =place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
                    $("#ciudad").val(ciudad);
                }
                break;
            case 8:

                switch(nparams){
                    case 2:
                        nom_pais= data[(1-1)];
                        $("#ciudad").val(nom_pais);
                        $("#estado").val(code_country);
                        //console.log("Pais:"+nom_pais);
                        break;
                    case 3:
                        ciudad1= data[0];
                        ciudad2= data[1];
                        ciudad=ciudad1+","+ciudad2;
                        nom_estado= code_country;
                        $("#ciudad").val(nom_pais);
                        $("#estado").val(nom_estado);
                        break;
                    case 4:
                        ciudad1= data[0];
                        ciudad2= data[1];
                        ciudad=ciudad1+","+ciudad2;
                        nom_estado= data[2];
                        $("#ciudad").val(ciudad);
                        $("#estado").val(nom_estado);
                        break;

                }
                if(place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea!=null){
                    ciudad =place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
                    $("#ciudad").val(ciudad);
                }
                break;
            default:

                break;
        }
    //console.log("Ciudad:"+ciudad);
    //validar(1);

    }else{
        
};

}
function getAddress(overlay, latlng) {
    if (latlng != null) {
        // Set up our GMarkerOptions object
        address = latlng;
        lat2= address['y'];
        long2= address['x'];
        asignarCoordenadas(lat2,long2);
        geocoder.getLocations(latlng, addAddressToMap);
    }
}

function addAddressToMap(response) {
    map.clearOverlays();

    if (!response || response.Status.code != 200) {
        Ext.MessageBox.alert('Alerta', "Status Code:" + response.Status.code);
    //validar(0);
    }
    else
    {
        // console.log("response de addAddresToMap");
        // console.log(response);
        place = response.Placemark[0];
        marker = new GMarker(address,markerOptionseditable);
        map.addOverlay(marker);
        zoom=map.getZoom();
        map.setCenter(address, zoom);
        GEvent.addListener(marker, "dragend", function() {
            lat2 = marker.getPoint().lat();
            long2 = marker.getPoint().lng();
            asignarCoordenadas(lat2,long2);
            geocoder.getLocations(marker.getPoint(), addMarkerAddressToMap);
        });

        lat2 = marker.getPoint().lat();
        long2 = marker.getPoint().lng();
        if(place.AddressDetails.Country!=null){
            code_country=place.AddressDetails.Country.CountryNameCode;
            //console.log("Codigo de pais :"+code_country);
            asignarcountrycode(code_country);
        }
                
        procesarPrecision(place,code_country);

    }
}

function editar(){
    searchControl.execute("''");
    map.clearOverlays();
    var coord_latitud=$('#latitud').val();
    var coord_longitud=$('#longitud').val();

    point = new GLatLng(coord_latitud,coord_longitud);
    marker = new GMarker(point,markerOptionseditable);
    zoom=map.getZoom();
    map.setCenter(point, zoom);
    map.addOverlay(marker);

    GEvent.addListener(marker, "dragend", function() {
        lat2 = marker.getPoint().lat();
        long2 = marker.getPoint().lng();
        asignarCoordenadas(lat2,long2);
        geocoder.getLocations(marker.getPoint(), addMarkerAddressToMap);
    });

    mapclickhandler=GEvent.addListener(map, "click", getAddress);

    var elementxname=$("#editar_propiedad").find(":input");
    $.each(elementxname, function(index, element){
        //console.log(element.type);
        if(element.type=="text"){
            if(element.name!="search"){
                $("#"+element.id).attr("disabled",false);
            }
        }

        if(element.type=="checkbox"){
            if($("#"+element.id).is(':checked')){
                $("#"+element.id).attr("disabled",false);
            }else{
                $("#"+element.id).attr("disabled",true);
            }

            if(element.name=="hotspot"){
                       
                $("#"+element.id).attr("disabled",true);
                $("#"+element.id).attr("checked",false);
            }
        }
        if(element.type=="select-one"){
            $("#"+element.id).attr("disabled",false);
        }

        if(element.type=="textarea"){
            $("#"+element.id).attr("disabled",false);
        }
	

    });

    $("#idpropiedad_c").attr("disabled",true);
    $("#fecha_ingreso").attr("disabled",true);
    $("#ultima_modificacion").attr("disabled",true);
    $('#guardar').css("display",'inline');
    $('#cancelar').css("display",'inline');
    $('#editar').css("display",'none');

}



function editar_descripcion(){
          
    $("#descripcion").attr("disabled",false);
            
    $("#cambiar_descripcion").css("display","none");
    $("#cerrar_edicio_descripcion").css("display","inline");
          
           
}

function cerrar_editar_descripcion(){
    var desc=$("#descripcion").val();
    var idprop=$("#idpropiedad_c").val();
    Ext.Ajax.request({
        //url: '/Mihousebook/savedescripcion',
        url: '/Mihousebook/savedescripcion' + '?' + fb,
        method: 'POST',
        params: {
            descripcion:desc,
            idpropiedad:idp
            //fb:fb
        },
        success: function(result, response) {
        // console.log("Registro Exitoso");
        // console.log(result);
        },
        failure: function(result, response) {
        //console.log(result);
        //console.log(response);
        }
    });


    $("#descripcion").attr("disabled",true);
    $('#descripcion2').val($('#descripcion').val());
    $(".descrip_propiedad").css("border","1px solid #CCCCCC");
    $("#descripcion").attr("class","sbg_editar");
    $("#cambiar_descripcion").css("display","inline");
    $("#cerrar_edicio_descripcion").css("display","none");



}


Ext.onReady(function(){

    if(administrador==1){
        // basic tabs 1, built from existing content
        var tabs_property = new Ext.TabPanel({
            renderTo: 'tabs_property',
            width:520,
            height:1050, // height agregado por resize del iframe
            plain:true,
            activeTab: 0,
            frame:true,
            defaults:{
                autoHeight: true,
                autoScroll: true
            },
            items:[
            {
                contentEl:'detalle',
                title: 'Perfil de la Propiedad'
            },

            {
                contentEl:'admin_foto',
                title: 'Administrar Fotos'
            }
            // , {
            //     title: 'Video',
            //     disabled:true,
            //     html: "Video"
            // }

            ]
        });
    }else{
        var tabs_property = new Ext.TabPanel({
            renderTo: 'tabs_property',
            width:520,
            height:1050,
            plain:true,
            activeTab: 0,
            frame:true,
            defaults:{
                autoHeight: true,
                autoScroll: true
            },
            items:[
            {
                contentEl:'detalle',
                title: 'Perfil de la Propiedad'
            }
            ]
        });
    }

    //Full Caption Sliding (Hidden to Visible)
    $('.boxgrid.captionfull').hover(function(){
        $(".cover", this).stop().animate({
            top:'42px'
        },{
            queue:false,
            duration:160
        });
    }, function() {
        $(".cover", this).stop().animate({
            top:'142px'
        },{
            queue:false,
            duration:160
        });
    });

    Encadenarcheckboxs();

  
    $("#tipo_enlistamiento"+tipo_enlistamiento).click();

    $("#tipo_enlistamiento"+tipo_enlistamiento).attr("checked",true);
    $("#estatus"+tipo_status).attr("checked",true);


    $("#precio").val(precio_propiedad);


    if (GBrowserIsCompatible()) {

        initialize();

    }else {
        Ext.Msg.alert('Alerta', 'Su browser no esta soportado por Google Maps');
    }

    cancelar();

    $('#guardar').css("display",'none');

    $('#editar').click(function(){
        editar();
    })

    $('#cambiar_descripcion').click(function(){
        editar_descripcion();
    })
    $('#cerrar_edicio_descripcion').click(function(){
        cerrar_editar_descripcion();
    })


    $('#cancelar').click(function(){
        cancelar();
    })

    $("#input_up").MultiFile({
        STRING: {
            remove: '<img src="/img/delete.png" height="16" border="0" width="16" alt="x"/>'
        }
    });
	 
    $('#enviar').click(function(){
        $('#upload_fotos').submit();
    });


    EncadenarcheckboxsName("hotspot");
    $("#descripcion").attr("class","sbg_editar");

    $("#country").change(function () {
        
        var str = "";
        $("#country option:selected").each(function () {
            str += $(this).text() + " ";
        });
        $("#pais").val(str);
    }).trigger('change');
   
    var chooser, btn;

    function insertImage(data){
        //console.log(data);
        //console.log(imagen_default.val());

        Ext.Ajax.request({
            url: '/Mihousebook/establecerdeterminada' + '?' + fb,
            //url: '/Mihousebook/establecerdeterminada',
            method: 'POST',
            params: {
                nameimage: data.name,
                //fb:fb
		propiedad: idp
            },

            success: function(result, response) {
                try {
                    
                    //result tiene ademas de responseText un status y statusText
                    //var rs = Ext.util.JSON.decode(result.responseText);
                    Ext.DomHelper.overwrite('info_save', {
                        tag: 'p', 
                        html: "Imagen "+data.name+" establecida por defecto exitosamente",
                        style:'margin:10px;visibility:hidden;'
                    }, true).show(true).frame();

                    Ext.DomHelper.overwrite('images', {
                        tag: 'img', 
                        src: data.url,
                        style:'margin:10px;visibility:hidden;height:150px;'
                    }, true).show(true).frame();
                } catch(err) {
              
                    Ext.DomHelper.overwrite('info_save', {
                        tag: 'p', 
                        html: "La imagen "+data.name+" no se ha establecido por defecto",
                        style:'margin:10px;visibility:hidden;'
                    }, true).show(true).frame();
                }

            },
            failure: function(result, response) {
                Ext.DomHelper.overwrite('info_save', {
                    tag: 'p',
                    html: "Error, la imagen "+data.name+" no se ha establecido por defecto",
                    style:'margin:10px;visibility:hidden;'
                }, true).show(true).frame();

            }
        });
        btn.focus();
    };

    function eliminarfotoBD(data){
         
        Ext.Ajax.request({
            //url: '/Mihousebook/eliminarfoto',
            url: '/Mihousebook/eliminarfoto' + '?' + fb,
            method: 'POST',
            params: {
                nameimage: data.name,
                //fb:fb
		propiedad: idprop
            },

            success: function(result, response) {
                try {

                    //result tiene ademas de responseText un status y statusText
                    //var rs = Ext.util.JSON.decode(result.responseText);
                    Ext.DomHelper.overwrite('info_save', {
                        tag: 'p', 
                        html: "Imagen "+data.name+" ha sido eliminada exitosamente",
                        style:'margin:10px;visibility:hidden;'
                    }, true).show(true).frame();

                    Ext.DomHelper.overwrite('images', {
                        tag: 'img', 
                        src: "",
                        style:'margin:10px;visibility:hidden;height:20px;'
                    }, true).show(true).frame();
                } catch(err) {

                    Ext.DomHelper.overwrite('info_save', {
                        tag: 'p', 
                        html: "La imagen "+data.name+" no ha sido eliminada",
                        style:'margin:10px;visibility:hidden;'
                    }, true).show(true).frame();
                }

            },
            failure: function(result, response) {
                Ext.DomHelper.overwrite('info_save', {
                    tag: 'p',
                    html: "Error, la imagen "+data.name+" no ha sido eliminada",
                    style:'margin:10px;visibility:hidden;'
                }, true).show(true).frame();

            }
        });
        btn.focus();

    };


    function choose(btn){
        if(!chooser){
            var metaID=55;
            chooser = new ImageChooser({
                url:'/Mihousebook/getimages?'+Math.random()+'&'+fb,
                width:515,
                height:350
            });
        }

        chooser.show(btn.getEl(), insertImage,eliminarfotoBD);
    };

    btn = new Ext.Button({
        text: "Galeria de propiedades",
        handler: choose,
        renderTo: 'buttons'
    });



});