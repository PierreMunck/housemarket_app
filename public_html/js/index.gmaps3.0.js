/**
 * Aplica internacionalizacion
 */
var destacadaload=false;
var codcat=""; 
var textcod1="";
var textcod2="";
var textcod3="";
var selcod1;
var selcod2;
var selcod3;
var once = 0;

function __translate(s) {
    if (typeof(i18n)!='undefined' && i18n[s]) {
        return i18n[s];
    }
    return s;
}
/**
 * Recupera variables de entorno
 */
function __environment(s) {
    if (typeof(env)!='undefined' && env[s]) {
        return env[s];
    }
    return s;
}

/**
 * Oculta muestra los filtros adicionales
 */
function toggleOption() {
    $('#optional_filter').slideToggle('slow');
    if($('#img_search_advanced').attr("src") == '/img/down_search.png') {
        $('#img_search_advanced').attr('src', '/img/up_search.png');
    } else{
        $('#img_search_advanced').attr('src', '/img/down_search.png');
    }
}

Ext.onReady(function(){
    var myInfoWindow = null; 
    if(Ext.state.Manager.getProvider() == null) {
        Ext.state.Manager.setProvider(new Ext.state.CookieProvider({
            path:"/",
            expires: new Date(new Date().getTime()+(1000*60*60*24*365))
        }));
    }

    Ext.BLANK_IMAGE_URL="/js/extjs/resources/images/gray/s.gif";
    Ext.QuickTips.init();

    var gmResponseCode=[];
    /* gmResponseCode[G_GEO_SUCCESS]= __translate("G_GEO_SUCCESS");
    gmResponseCode[G_GEO_MISSING_ADDRESS]=__translate("G_GEO_MISSING_ADDRESS");
    gmResponseCode[G_GEO_UNKNOWN_ADDRESS]=__translate("G_GEO_UNKNOWN_ADDRESS");
    gmResponseCode[G_GEO_UNAVAILABLE_ADDRESS]=__translate("G_GEO_UNAVAILABLE_ADDRESS");
    gmResponseCode[G_GEO_BAD_KEY]=__translate("G_GEO_BAD_KEY");
    gmResponseCode[G_GEO_TOO_MANY_QUERIES]=__translate("G_GEO_TOO_MANY_QUERIES");
    gmResponseCode[G_GEO_SERVER_ERROR]=__translate("G_GEO_SERVER_ERROR");*/

    var _toGM=new Ext.data.Store({
        url:"/index/estatetogm",
        reader:new Ext.data.JsonReader({
            root:"rows",
            id:"CodigoPropiedad"
        },[{
            name:"CodigoPropiedad"
        },{
            name:"nombre"
        },
        {
            name:"cuarto"
        },
        {
            name:"bano"
        }
        ,
        {
            name:"Area"
        },
        {
            name:"UnidadMedida"
        },
        {
            name:"Accion"
        }         
        ,{
            name:"Estado"
        },{
            name:"Ciudad"
        },{
            name:"ZipCode"
        },{
            name:"PrecioVenta"
        },{
            name:"PrecioAlquiler"
        },{
            name:"moneda"
        },{
            name:"Latitud"
        },{
            name:"Longitud"
        },{
            name:"PROFotoID"
        },{
            name:"credito"
        }]),
        sortInfo:{
            field:"credito",
            direction:"DESC"
        }
    });

    var _filtradas=new Ext.data.Store({
        url:"/index/fotosfiltradas",
        reader:new Ext.data.JsonReader({
            root:"rows",
            id:"id"
        },[{
            name:"cuarto" 
        },{
            name:"bano"
        },{
            name:"Area" 
        },{
            name:"UnidadMedida"
        }
        ,{
            name:"nombre"
        },{
            name:"estado"
        },{
            name:"ciudad"
        },{ 
            name:"foreclosure"
        },{
            name:"sale"
        },{
            name:"rent"
        },{
            name:"bothPrice"
        },{
            name:"PROFotoID"
        },{
            name:"id"
        }])
    });

    //Guarda los brokers segun el area seleccionada
    var _brokers = new Ext.data.Store({
        url: "/index/brokersByArea",
        reader: new Ext.data.JsonReader({
            root:"rows",
            id:"Uid"
        },
        [{
            name:"pageid"
        },            {
            name:"WebPage"
        },            {
            name:"creditos"
        },           {
            name:"NombreCliente"
        },{
            name:"Uid"
        },{
            name: "pic_square"
        }
        ])
    });


    var country;
    var city;
    var googleMap;
    var _gMarkerList=[];
    var _gMarkerContentList=[];  
    var x="";
    Ext.apply(Ext.form.VTypes,{
        numbersonly:function(val,_12){
            var n=parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        },
        numbersonlyText:__translate("FILTER_ONLY_NUMBERS")
    });

    //if(GBrowserIsCompatible()){
    function _putGMMarker(_gLatLng,_id,_content,_icon){        
        x=_id;
        var opts = new Object();
        opts.zIndexProcess = function(marker) {
            return marker.credits;
        };
        var _gMarker= new google.maps.Marker({
            position: _gLatLng,
            map: googleMap,
            icon: _icon
        });     
        google.maps.event.addListener(_gMarker,"click",function(){
            myInfoWindow.close();
            myInfoWindow = new google.maps.InfoWindow({
                content: _content
            });

            myInfoWindow.open(googleMap, _gMarker);
        });
        _gMarkerList[x]=_gMarker;
        _gMarkerContentList[x]=_content;
        x="";
            
        /* var marcador = new google.maps.Marker({
	    position: _gLatLng,
	    map: mapa,
	    icon: 'Images/MiImagen.png',
	    title: 'Este es mi marcador'
	});
            */
        return _gMarker;
    }

    googleMap= new google.maps.Map(document.getElementById('map'));
        
   
    /*var customUI = googleMap.getDefaultUI();
        customUI.zoom.doubleclick = false;
        customUI.zoom.scrollwheel = false;
        googleMap.setUI(customUI);
*/
    var _isGMInfoWindowOpen;
    _isGMInfoWindowOpen=false;

    function _onGMInfoWindowOpen(){
            
        _isGMInfoWindowOpen=true;
    }

    function _onGMMoveEnd(){            
        destacadaload = false;
        if(!_isGMInfoWindowOpen){
            _onGMDragEnd();
        }
        _isGMInfoWindowOpen=false;
    }

    function _onGMDragEnd(){           
        destacadaload = false;       
        loadToGM(country,city);
    }

    function _onGMZoomEnd() {
        destacadaload = false;
    }
    //Mis nuevas funciones
    function _onCenterChange() {
        loadToGM(country,city);  
        destacadaload = false;
    }
    // Agregar eventos al Google Map
  
    
    // google.maps.event.addListener(googleMap,"moveend",_onGMMoveEnd);
    // google.maps.event.addListener(googleMap,"infowindowopen",_onGMInfoWindowOpen);

    function _2e(){
        var _gmCoordinates=GetGMCoordinates();
        var _30=_gmCoordinates.maxX-_gmCoordinates.minX;
        var _31=_gmCoordinates.maxY-_gmCoordinates.minY;
        googleMap.queryCenter=googleMap.getCenter();
        googleMap.maxY=_gmCoordinates.maxY+_31*0.2;
        googleMap.maxX=_gmCoordinates.maxX+_30*0.2;
        googleMap.minY=_gmCoordinates.minY-_31*0.2;
        googleMap.minX=_gmCoordinates.minX-_30*0.2;
    }

    // Coordenadas a pasarle al Google Map cuando se realice el centrado
    // Por defecto, se ubica de acuerdo la IP del cliente
    var _gmResultCoordinates={
        lat:"25.79",
        lng:"4.21"
    };

    // Condicionar, porque si ya existe en memoria, no mandar a centrar.
    var cachedState = Ext.state.Manager.get("cached");
    if(cachedState) {
        var frmU = Ext.state.Manager.get("ub");
        var frmF = Ext.state.Manager.get("fi");
        var frmUbi = Ext.decode(frmU);
        var frmFilter = Ext.decode(frmF);
        $("#dir").val(frmUbi.dir);
        $("#cat").val(frmFilter.category);
        $("#type").val(frmFilter.type);
        $("#min_price").val(frmFilter.min_price);
        $("#max_price").val(frmFilter.max_price);
        $("#min_area").val(frmFilter.min_area);
        $("#max_area").val(frmFilter.max_area);
        $("#min_area_lote").val(frmFilter.min_area_lote);
        $("#max_area_lote").val(frmFilter.max_area_lote);
        $("#area_lote").val(frmFilter.area_lote_uni);
        $("#area").val(frmFilter.area_uni);
    }        
        
    Ext.Ajax.request({
        url:"/index/centermap",            
        success:function(_response,_options){
            try{
                var rs=Ext.util.JSON.decode(_response.responseText);
                _gmResultCoordinates=rs.data[0];                 
                var latlng = new google.maps.LatLng(_gmResultCoordinates.lat, _gmResultCoordinates.lng);
                var myOptions = {
                    zoom: 12,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                googleMap = new google.maps.Map(document.getElementById("map"), myOptions); 
                google.maps.event.addListener(googleMap,'dragend',function(){
                    destacadaload = false; 
                    loadToGM(country,city);
                });
                google.maps.event.addListener(googleMap,'zoom_changed',function(){
                    destacadaload = false; 
                    loadToGM(country,city);
                });
                google.maps.event.addListener(googleMap,'click',function(){
                    myInfoWindow.close();
                });
                loadToGM(_gmResultCoordinates.cc,_gmResultCoordinates.city);
            }
            catch(err){                
            /*CONVERTIR EL CODIGO PARA UN CATCH
                 *
                 *var poserror = new google.maps.LatLng(25.79,4.21);
                googleMap.setCenter(poserror);
                googleMap.setZoom(12);*/
            }
        },
        failure:function(_response,_options){            
        /*
             *CONVERTIR EL CODIGO PARA UN FAILURE
             *var poserror = new google.maps.LatLng(25.79,4.21);
            googleMap.setCenter(poserror);
            googleMap.setZoom(12);*/
        }
    });
    function GetGMCoordinates(){
        var _gmBounds=googleMap.getBounds();
            
            
        var _gmBoundsSW = _gmBounds.getSouthWest();
        var _gmBoundsNE = _gmBounds.getNorthEast();
        var _gmSWLng=_gmBoundsSW.lng();
        var _gmSWLat=_gmBoundsSW.lat();
        var _gmNELng=_gmBoundsNE.lng();
        var _gmNELat=_gmBoundsNE.lat();
           
        return {
            minX:_gmSWLng,
            maxX:_gmNELng,
            minY:_gmSWLat,
            maxY:_gmNELat
        }
    }
    function _45(m,min,max){
        var _46=parseInt(min,10);
        var _47=parseInt(max,10);
        var _48=m.substring(4);
        var _49=m.substring(3,0);
        if(_49=="min"){
            if((_46>_47)&&(_47>=0)){
                Ext.getDom("max_"+_48).value=0;
            }
        }else{
            if(_49=="max"){
                if((_47<_46)&&(_46>=0)){
                    Ext.getDom("min_"+_48).value=0;
                }
            }
        }
    }

    function _4a(num){
        var _4b=false;
        if(Ext.isEmpty(num)){
            _4b=true;
        }else{
            _4b=Ext.num(num,false);
            if(_4b<0){
                _4b=false;
            }
        }
        return _4b;
    }

    /**
         * Cargar los datos en el google map a partir del pais y la ciudad
         */
    function loadToGM(_country, _city){
        if(_country) {
            country = _country;
            city = _city;            
            var _coordinates = GetGMCoordinates(); 
            FiltroDinamico();
            _brokers.load({
                params:{
                    "coordinates":Ext.encode(_coordinates)
                }
            });

            

        }
    }    
	
    var findGM=Ext.get("btnFgm");
    findGM.on("click",function(){
        SearchByDir();
    });

    /**
         * Expandir / Colapsar filtros adicionales del mapa
         **/
    //        var advanced = $("#advanced");
    //        if(advanced) {
    //            advanced.click(function() {
    //                toggleOption();
    //                return false;
    //            });
    //        }
    var asktofriend = $("#asktofriend");
    if(asktofriend){
        asktofriend.click(function(){
           if(jQuery.trim($("#cat:selected").text())!=""&&jQuery.trim($("#type:selected").text())!=""){
               StreamPublish(data)
           }else{
               Ext.MessageBox.alert(__translate("SEARCH_RESULT_STATUS"),__translate("ALERT_MESSAGE_NOT_FILTER"));
           }
        });
    }
    
    /**
         * Mostrar / Ocultar filtros adicionales por categorias
         **/
    var categories = $("#cat");
    if(categories) { 
        categories.change(function() {                
            name = $("#cat :selected").text()
            name = name.replace(/ /gi,"");                             
            name = name.split(' ').join('') + "_";                
            $("#optional_filter div").each(function(){
                div_name = $(this).attr("id");
                if(div_name.indexOf(name) == 0) {
                    $(this).attr("class", "show");
                } else if(div_name.indexOf("fixed") == -1 && div_name.indexOf("buttons") == -1){
                    $(this).attr("class", "hide");
                }
            });
        });
    }
    var atype = $("#type");
    var combo_price_min = $("#pricemin");
    var combo_area = $("#area");
    var combo_area_min = $("#area-min");
    var combo_area_max = $("#area-max");
    var combo_price_max = $("#pricemax");
      
    function ComboAreaFt2(max_min,rang1){
        var label_c="";         
        var option_c = "<option value='' >"+max_min+"</option>";
        var MIN_C = 0;
        var  AUM;
        for (var j=1; j<=10; j++){
            if(j<=10)AUM=rang1;                                              
            MIN_C += AUM;  
            label_c = MIN_C;
            label_c = label_c.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
            label_c = label_c.split('').reverse().join('').replace(/^[\,]/,'');
            option_c += "<option value= '"+ MIN_C+"'>"+ label_c + " ft<sup>2</sup>"+"</option>";              
                    
        }
        option_c += "<option value= '"+ 43560 +"'>"+ "1 Acre" +"</option>"; 
        option_c += "<option value= '"+ 217800+"'>"+ "5 Acre" +"</option>"; 
        option_c += "<option value= '"+ 2178000+"'>"+ "50 Acre" +"</option>"; 
        option_c += "<option value= '"+ 4356000+"'>"+ "100 Acre" +"</option>"; 
        return option_c;
    }
        
    function ComboArea(max_min,rang1, rang2, rang3){
        var label_c="";         
        var option_c = "<option value='' >"+max_min+"</option>";
        var MIN_C = 0;
        var  AUM;
        for (var j=1; j<=12; j++){
            if(j<=10)AUM=rang1; 
            if(j>10 && j<=11){
                AUM=rang2;
            } 
            if(j>11 && j<=12){
                AUM=rang3;
            }                            
            MIN_C += AUM;  
            label_c = MIN_C;
            label_c = label_c.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
            label_c = label_c.split('').reverse().join('').replace(/^[\,]/,'');
            option_c += "<option value= '"+ MIN_C+"'>"+ label_c + " mt<sup>2</sup>" +"</option>";             
                    
        }
        option_c += "<option value= '"+ 10000 +"'>"+ "1 hect" +"</option>"; 
        option_c += "<option value= '"+ 50000+"'>"+ "5 hect" +"</option>"; 
        option_c += "<option value= '"+ 500000+"'>"+ "50 hect" +"</option>"; 
        option_c += "<option value= '"+ 1000000+"'>"+ "100 hect" +"</option>"; 
        return option_c;
    }
        
        
    function ComboRent(max_min,rang1, rang2, rang3, rang4){
        var label_c="";         
        var option_c = "<option value='' >"+max_min+"</option>";
        var MIN_C = 0;
        var  AUM;
        for (var j=1; j<22; j++){
            if(j<=10)AUM=rang1; 
            if(j>10 && j<=15){
                AUM=rang2;
            } 
            if(j>15 && j<=19){
                AUM=rang3;
            } 
            if(j>19 && j<=22){
                AUM=rang4;
            }                
            MIN_C += AUM;  
            label_c = MIN_C;
            label_c = label_c.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
            label_c = label_c.split('').reverse().join('').replace(/^[\,]/,'');
            option_c += "<option value= '"+ MIN_C+"'>"+ label_c +"</option>";             
                    
        }
        return option_c;
    }
        
    function ComboSale(max_min,rang1, rang2, rang3, rang4, rang5, rang6){
        var label_c="";
        var option_c = "<option value='' >"+max_min+"</option>";
        var MIN_C = 0;
        var  AUM;
        for (var j=1; j<24; j++){
            if(j<=10)AUM=rang1; 
            if(j>10 && j<=12){
                AUM=rang2;
            } 
            if(j>12 && j<=14){
                AUM=rang3;
            } 
            if(j>14 && j<=17){
                AUM=rang4;
            }
            if(j>17 && j<=19){
                AUM=rang5;
            } 
            if(j>19 && j<=23){
                AUM=rang6;
            }                 
            MIN_C += AUM;
            if(j>= 12){
                label_c = (MIN_C/1000000) + " " +__translate("SEARCH_LABEL_MILLION");                   
            }
            else{
                label_c= MIN_C;
                label_c = label_c.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
                label_c = label_c.split('').reverse().join('').replace(/^[\,]/,'');
            }                   
            option_c += "<option value= '"+ MIN_C+"'>"+ label_c +"</option>";                          
                    
        }
        return option_c;
    }
        
    function FiltroDinamico(){
        dest = 1;
        var frmFiltro = GetObjectFrmFilter();
        var _coordinates = GetGMCoordinates();
       
        _toGM.load({
            params:{
                "country":country,
                "city":city,
                "category":frmFiltro.category,
                "type":frmFiltro.type,
                "min_price":frmFiltro.min_price,
                "max_price":frmFiltro.max_price,
                "max_area":frmFiltro.max_area,
                "min_area":frmFiltro.min_area,
                "uni_area":frmFiltro.area_uni,
                "max_area_lote":frmFiltro.max_area_lote,
                "min_area_lote":frmFiltro.min_area_lote,
                "uni_area_lote":frmFiltro.area_lote_uni,
                "parameters": Ext.encode(frmFiltro.parameters),
                "coordinates":Ext.encode(_coordinates)
            }
        });
			
        _filtradas.load({
            params:{
                "category":frmFiltro.category,
                "type":frmFiltro.type,
                "min_price":frmFiltro.min_price,
                "max_price":frmFiltro.max_price,
                "max_area":frmFiltro.max_area,
                "min_area":frmFiltro.min_area,
                "uni_area":frmFiltro.area_uni,
                "max_area_lote":frmFiltro.max_area_lote,
                "min_area_lote":frmFiltro.min_area_lote,
                "uni_area_lote":frmFiltro.area_lote_uni,
                "parameters": Ext.encode(frmFiltro.parameters),
                "coordinates":Ext.encode(_coordinates),
                "cPage" : 1,
                "sPage" : 1,
                "total" : 1
            }
        });
    }
        
    if(atype) {
        atype.change(function(e){
            var valor = atype.val();                     
            if(valor=="R"){
                combo_price_min.html(ComboRent("Min",500,1000,5000,10000));
                combo_price_max.html(ComboRent("Max",500,1000,5000,10000));
            }else if(valor=="S"){
                combo_price_min.html(ComboSale("Min",50000,250000,500000,1000000,2500000,10000000));
                combo_price_max.html(ComboSale("Max",50000,250000,500000,1000000,2500000,10000000));
            }  
            else if(valor=="F"){
                combo_price_min.html(ComboSale("Min",50000,250000,500000,1000000,2500000,10000000));
                combo_price_max.html(ComboSale("Max",50000,250000,500000,1000000,2500000,10000000));
            } else {
                combo_price_min.html("<option value=''>Min</option>");
                combo_price_max.html("<option value=''>Max</option>");
            }  
            FiltroDinamico();
        });
    }
    
    if(combo_price_min) {
        combo_price_min.change(function(e){
            FiltroDinamico();           
        } )
    }
    if(combo_price_max) {
        combo_price_max.change(function(e){
            FiltroDinamico();           
        } )
    }
    if(combo_area_min) {
        combo_area_min.change(function(e){
            FiltroDinamico();           
        } )
    }
    if(combo_area_max) {
        combo_area_max.change(function(e){
            FiltroDinamico();           
        } )
    }
    if(combo_area) {             
        combo_area.change(function(e){   
            FiltroDinamico();
            if(combo_area.val()=="mt2"){
                combo_area_min.html(ComboArea("Min",50,500,4000));   
                combo_area_max.html(ComboArea("Max",50,500,4000));
                FiltroDinamico();
            }else{
                combo_area_min.html(ComboAreaFt2("Min",500)); 
                combo_area_max.html(ComboAreaFt2("Max",500));  
                FiltroDinamico();
              
            }               
                  
        } )
    }
         
    var acat = $("#cat");
       
    if(acat) {
        acat.change(function(e){           
            FiltroDinamico();
                
            if($("#cat option:selected").val()==6){
                textcod1 = "ApartamentoCondo";
                textcod1 = textcod1 + "-min_1";
                selcod1 = $("#"+textcod1);
                textcod2 = "ApartamentoCondo";
                textcod2 = textcod2 + "-min_2";
                selcod2 = $("#"+textcod2);
                textcod3 = "ApartamentoCondo";
                textcod3 = textcod3 + "-min_3";                 
                selcod3 = $("#"+textcod3);
                    
            }else
            {
                textcod1 = $("#cat option:selected").text();
                textcod1 = textcod1.replace(/ /gi,"");
                textcod1 = textcod1 + "-min_1";
                selcod1 = $("#"+textcod1);
                textcod2 = $("#cat option:selected").text();
                textcod2 = textcod2.replace(/ /gi,"");
                textcod2 = textcod2 + "-min_2";
                selcod2 = $("#"+textcod2);
                textcod3 = $("#cat option:selected").text();
                textcod3 = textcod3.replace(/ /gi,"");
                textcod3 = textcod3 + "-min_3";                 
                selcod3 = $("#"+textcod3);
            }
            if(selcod1){
                selcod1.change(function(e){
                    FiltroDinamico();             
                }
                )
            }
            if(selcod2){
                selcod2.change(function(e){
                    FiltroDinamico();
                }
                )
            }
            if(selcod3){
                selcod3.change(function(e){
                    FitroDinamico();
                }
                )
            }
               
               
        });
              
    }
    // Recupera un objeto que represente al formulario de Ubicar en el Mapa
    function GetObjectFrmUbicar() {
        var _frmSerUbicar=Ext.Ajax.serializeForm("frmFgm"); // Serializar el formulario
        var _frmDecUbicar=Ext.urlDecode(_frmSerUbicar); // Decodificar los valores de los elementos del formulario
        var _dir=_frmDecUbicar.dir; // Recuperar la direccion proporcionada por el usuario
        if(_frmDecUbicar.dir==__translate("TEXT_SEARCH_SAMPLE")){
            _dir="";
        }

        return {
            dir:_dir
        };
    }
        
        

    // Recuperar un objeto que represente al formulario de Filtros
    function GetObjectFrmFilter() {

        var _cat = $("#cat").val();
        var _type =/* $("ul#listing_tabs li.active").attr("valor");*/$("#type").val();
        var _price_min = $("#pricemin :selected").val();
        var _price_max = $("#pricemax :selected").val();

        if(_type == "0") {
            _type = "";
        }

        if(_price_max == "MAX") {
            _price_max = "";
        }
        if(_price_min == "MIN") {
            _price_min = "";
        }

        var _area_min = $("#area-min :selected").val();
        var _area_max = $("#area-max :selected").val();
        if(_area_max == "MAX") {
            _area_max = "";
        }
        if(_area_min == "MIN") {
            _area_min = "";
        }
        var _uni_area = $("#area").val();

        var _area_lote_min = $("#area-min").val();
        var _area_lote_max = $("#area-max").val();
        if(_area_lote_max == "MAX") {
            _area_lote_max = "";
        }
        if(_area_lote_min == "MIN") {
            _area_lote_min = "";
        }
        var _uni_area_lote = $("#area_lote").val();

        var parameters = [];
        var name = $("#cat :selected").text();
        name = name.replace(/ /gi,"");            
        var nombreCat = 0;
           
        $("#optional_filter div[id^='" + name + "_'].show").each(function() {
            var select = $(this).find("select");
                
            var id = $(this).attr("id");
            if(id.indexOf('_') > 0) {
                var sections = id.split("_");
                    
                    
                var max = select.val();               
                    
                var min = select.val();
                   
                    
                var value = $("#" + sections[nombreCat] +"-value_"+ 4)
                    
                var val = value.val();
              
                   
                if(value.attr("type")=="checkbox") {
                    if(value.get(0).checked) {
                        val = "S";
                    } else {
                        val = "N";
                    }
                }
                 
                parameters.push({
                    codigo : sections[1], 
                    min : min, 
                    max : max, 
                    val : val
                });
            }
        });

        return {
            category:_cat,
            type:_type,
            min_price:_price_min,
            max_price:_price_max,
            min_area:_area_min,
            max_area:_area_max,
            area_uni:_uni_area,
            min_area_lote:_area_lote_min,
            max_area_lote:_area_lote_max,
            area_lote_uni:_uni_area_lote,
            parameters:parameters
        };
    }
    var findGMList= $("#dir");
    findGMList.keyup(function(response){
        var code = response.originalEvent.keyCode;
        if(code!=16){
            var texto = findGMList.val();
            _gClientGeoDecoder.geocode({
                'address': texto
            },function(response, status){ 
                
                });
        }
            
    });
        
    function SearchByDir() { 
        destacadaload = false;
        
        var frmUbicar = GetObjectFrmUbicar();
        var _dir = frmUbicar.dir;
        var firstPlace;           
        var statusCode;
        if(!isEmpty(_dir)){
            _gClientGeoDecoder.geocode({
                'address': _dir
            },function(response, status){ 
                statusCode=status;
                if (response) {
                    firstPlace = response[0]; 
                    //Utilizamos el primer resultado por defecto
                    for(var i=0;i< firstPlace.address_components.length;i++){                      
                        if(firstPlace.address_components[i].types[0]=='country')
                            country = firstPlace.address_components[i].short_name;
                    }
                    if(firstPlace.types[0] == 'locality')
                        city = firstPlace.address_components[0].long_name;  
                    
                    //Centramos el MAPA por el momento con el primer Resultado
                    googleMap.setOptions({ 
                        center: firstPlace.geometry.location,
                        zoom: 12
                    });
                    loadToGM(country,city);
                } else {
                    console.log(statusCode);
                //Ext.MessageBox.alert(__translate("G_GEO_ALERT_TITLE"),__translate("G_GEO_ALERT_MESSAGE_NOT_FOUND"));
                }
                    
                
            });
        }
    }

    var _gClientGeoDecoder = new google.maps.Geocoder();
    function isEmpty(value){
        if(null==value||""==value){
            return true;
        }
        return false;
    }
    function isNumeric(val){
        var n=parseInt(val);
        return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
    }

    var nav=new Ext.KeyNav(Ext.getDom("dir"),{
        "enter":SearchByDir,
        "scope":this
    });
    var _nameEstate;
    var _titleEstate;
    var _areaEstate;       
    var _zipCode;
    var _city;
    var _status;
    var _id;
    var _lat;
    var _lng;
    var _gLatLng;
    var _idPhoto;       
    var _result_photo;
    var googleMarker;       
    var atribEstate;
    var _roomEstate;
    var _bathEstate;
    var markerCluster = null;
        
        
    var _showDestacadas=new Ext.XTemplate(
        "<tpl for=\".\">",
        "<tpl for=\"data\">",
        "<tpl if=\"this.advertise(id) == false\">",
        "<tpl if=\"this.noSrc(PROFotoID) == false\">",
        "<div id=\"dest_item_{[xindex]}\" class=\"",
        "<tpl if=\"this.lastOne(xindex, xcount)==true\" >",
        "last_one",
        "</tpl>",
        "<tpl if=\"this.isMiddleOne(xindex, xcount)==true\" >",
        "destacadas_item",
        "</tpl>",
        "<tpl if=\"this.firstOne(xindex)==true\" >",
        "first_one",
        "</tpl>",
        "\"><a  href='javascript:;'><img src=\"/pic{PROFotoID}_2.jpg\" width=\"120\" height=\"90\" onerror=\"this.src='" + __translate("AD_NO_IMAGE") +"'\" /><b id=\"{CodigoPropiedad}\"></b></a>\n\
<a class=\"fpOver\" style=\"background-image: url('"+__translate("FEATURE_IMAGE_BACKGROUND")+"');\" href=\"javascript:;\"><b id=\"{CodigoPropiedad}\"></b></a></div>",
        //+ __environment('facebook_url') + "enlistar/perfil?valprop={id}\" target=\"_top\"><img src=\"/pic{PROFotoID}_2.jpg\" width=\"120\" height=\"90\" onerror=\"this.src='" + __translate("AD_NO_IMAGE") +"'\" /></a><a class=\"fpOver\" style=\"background-image: url('"+__translate("FEATURE_IMAGE_BACKGROUND")+"');\" href=\""+ __environment('facebook_url') + "enlistar/perfil?valprop={id}\" target=\"_top\"></a></div>",
        "</tpl>",
        "<tpl if=\"this.noSrc(PROFotoID) == true\">",
        "<div id=\"dest_item_{[xindex]}\" class=\"",
        "<tpl if=\"this.lastOne(xindex, xcount)==true\" >",
        "last_one",
        "</tpl>",
        "<tpl if=\"this.isMiddleOne(xindex, xcount)==true\" >",
        "destacadas_item",
        "</tpl>",
        "<tpl if=\"this.firstOne(xindex)==true\" >",
        "first_one",
        "</tpl>",
        "\"><a  href=\"javascript:;\"><img src=\"" + __translate("AD_NO_IMAGE") +"\" width=\"120\" height=\"90\"/><b id=\"{CodigoPropiedad}\"></b></a><a class=\"fpOver\" style=\"background-image: url('"+__translate("FEATURE_IMAGE_BACKGROUND")+"');\" href=\"javascript:;\"><b id=\"{CodigoPropiedad}\"></b></a></div>",
        //"+ __environment('facebook_url') + "enlistar/perfil?valprop={id}\" target=\"_top\"><img src=\"" + __translate("AD_NO_IMAGE") +"\" width=\"120\" height=\"90\"/></a><a class=\"fpOver\" style=\"background-image: url('"+__translate("FEATURE_IMAGE_BACKGROUND")+"');\" href=\""+ __environment('facebook_url') + "enlistar/perfil?valprop={id}\" target=\"_top\"></a></div>",
        "</tpl>",
        "</tpl>",
        "<tpl if=\"this.advertise(id) == true\">",
        "<div id=\"{id}\" class=\"",
        "<tpl if=\"this.lastOne(xindex, xcount)==true\" >",
        "last_one",
        "</tpl>",
        "<tpl if=\"this.isMiddleOne(xindex, xcount)==true\" >",
        "destacadas_item",
        "</tpl>",
        "<tpl if=\"this.firstOne(xindex)==true\" >",
        "first_one",
        "</tpl>",
        "\"><a href=\"javascript:;\" ><img src=\"" + __translate("AD_NO_IMAGE_OUTSTANDING") +"\" width=\"120\" height=\"90\"/></a><a class=\"fpOver\" style=\"background-image: url('"+__translate("FEATURE_IMAGE_BACKGROUND")+"');\" href=\"javascript:void(0);\"></a></div>",
        "</tpl>",
        "</tpl>",
        "</tpl>",{
            compiled:true,
            noSrc:function(src){
                return src==null;
            },
            advertise:function(id) {
                return id==-1;
            },
            firstOne:function(index) {
                return index == 1;
            } ,
            lastOne:function(index, count) {
                return index >= count;
            } ,
            isMiddleOne:function(index, count){
                return index > 1 && index < count;
            }
        });

    _toGM.on("load",function(s,r){            
        var arr_markers = [];
        var arr_destacados = new Array();
        //googleMap.clearOverlays(); 
            
        var test = new Array(); 
        for(i=0;i<=5;i++){
            test[i]={
                "id":"-1",
                "data":{
                    "id":"-1",
                    "NombrePropiedad":null,
                    "NombreCategoria":"null",
                    "ZipCode":"null",
                    "Ciudad":"null",
                    "Area":"null", 
                    "UnidadMedida":"null",
                    "cuarto":"null",
                    "bano":"null",
                    "CodigoPropiedad":"null",
                    "PROFotoID":null,
                    "Accion":"null",
                    "Latitud":"null",
                    "Longitud":"null",
                    "PrecioAlquiler":"null",
                    "PrecioVenta":"null",
                    "nombre":"eliazer",
                    "credito":"null"
                }
            };
        }
           
        Ext.each(r,function(rec,i){
            if(i<6){                    
                test[i] = r[i];                   
            }
                
            _nameEstate = rec.data.NombrePropiedad;
            if(!_nameEstate) {
                _nameEstate = "";
            }
            _titleEstate = rec.data.NombreCategoria;
            if(!_titleEstate) {
                _titleEstate = "";
            }          
         
            _zipCode=rec.data.ZipCode;
            if(!_zipCode) {
                _zipCode = "";
            }
            _city = rec.data.Ciudad;
            if(!_city) {
                _city = "";
            }
            _areaEstate = rec.data.Area;
                
            if(!_areaEstate) {
                _areaEstate = "";
            }else{
                _areaEstate = _areaEstate + " " + rec.data.UnidadMedida  +" | ";
            }
                
            _roomEstate = rec.data.cuarto;
            if(!_roomEstate) {
                _roomEstate = "";
            }else {
                _roomEstate = _roomEstate + " " +__translate("SEARCH_RESULT_FOR_ROOM") + " | ";
            }
                
            _bathEstate = Math.round(rec.data.bano);
            if(!_bathEstate) {
                _bathEstate = "";
            }else {
                _bathEstate = _bathEstate + " " +__translate("SEARCH_RESULT_FOR_BATH") + "  ";
            }
                
            _id=rec.data.CodigoPropiedad;
            _idPhoto=rec.data.PROFotoID;
            if(!_idPhoto) {
                _result_photo = __translate("AD_NO_IMAGE_RESULT");
            }else {
                _result_photo = "/pic"+_idPhoto+"_3.jpg"
            }
            _status = "";
            atribEstate = _areaEstate + _roomEstate + _bathEstate ;
                
            switch(rec.data.Accion){
                case "B":
                    _status="<li>" + __translate("SEARCH_RESULT_FOR_SALE") + " " + rec.data.moneda + "$ " + rec.data.PrecioVenta + " | </li>" +
                    "<li>" + __translate("SEARCH_RESULT_FOR_RENT") + " " + rec.data.moneda + "$ " + rec.data.PrecioAlquiler + "  </li>";
                    break;
                case "S":
                    _status="<li>"+ __translate("SEARCH_RESULT_FOR_SALE") + " " + rec.data.moneda + "$ " + rec.data.PrecioVenta + "  </li>";
                    break;
                case "R":
                    _status= "<li>"+ __translate("SEARCH_RESULT_FOR_RENT") + " " + rec.data.moneda + "$ " + rec.data.PrecioAlquiler + "  </li>";
                    break;
                default:
                    _status = "";
                    break;
            }

            _lat=rec.data.Latitud;
            _lng=rec.data.Longitud;
            var _windowInfoContent="<table cellpadding=\"10\" cellspacing=\"5\" border=\"0\" width=\"252px\" class=\"details\">"+
            "<tr>"+
            "<td width=\"0px\" valign=\"top\">"+
            "<div class=\"photo_wrapper\">"+
            "<img alt=\"\" src=\""+_result_photo+"\" height=\"75\" width=\"100\" onerror=\"this.src='" + __translate("AD_NO_IMAGE_RESULT") + "'\" />"+
            "</div>"+
            "</td>"+
            "<td width=\"252px\" valign=\"top\" >"+
            "<ul>"+
            "<li style='font-weight:bold;'>"+_titleEstate+"</li>" +
            "<li>"+  _status  +"</li>" +
            "<li>"+ atribEstate +"</li>"+
            "<li>"+_city+ ", "+_zipCode+"</li>"+
            "<li><a target='_new' href=\""+ __environment('facebook_url') + "enlistar/perfil?valprop="+_id+"\" target=\"_top\">" + __translate("SEARCH_RESULT_GOTO_PROFILE") + "</a></li>"+										
            "</ul>"+
            "</td>"+
            "</tr>"+
            "<tr>" +
            "</tr>"+
            "</table>";
            //var mgrOptions = { borderPadding: 50, maxZoom: 15, trackMarkers: true };               
                               
            _gLatLng= new google.maps.LatLng(_lat, _lng);
            myInfoWindow = new google.maps.InfoWindow();
            
            if(rec.data.credito>0) {
                googleMarker=_putGMMarker(_gLatLng,_id,_windowInfoContent,'img/house.png');            
                googleMarker.credits = rec.data.credito;
              
                arr_destacados.push(googleMarker);
              
              
                myInfoWindow.setContent(_gMarkerContentList[_id]);               
                if(once == 0)
                    myInfoWindow.open(googleMap, googleMarker);
                once++;
            
            } else {   
                googleMarker=_putGMMarker(_gLatLng,_id,_windowInfoContent,'img/house_normal_blue.png'); 
                googleMarker.credits = rec.data.credito;
                arr_markers.push(googleMarker);                      
            }
            
        });
        if (markerCluster != null) {
           markerCluster.clearMarkers();
        }
        markerCluster = new MarkerClusterer(googleMap,arr_markers);      
        if(destacadaload==false){    
            _showDestacadas.overwrite("destacadas",test);
            destacadaload=true;
        }
    });

    var styles = [
    [{
        url: '../img/house_clust.png',
        height: 16,
        width: 16,
        anchor: [7, 0],
        textColor: '#FF00FF'
    },
    {
        url: '../img/house_clust.png',
        height: 16,
        width: 16,
        opt_anchor: [7, 0],
        opt_textColor: '#FF0000'
    },
    {
        url: '../img/house_clust.png',
        width: 16,
        height: 16,
        opt_anchor: [7, 0]
    }]];


    function refreshMap(arrMarkers) {        
        var ZOOM_MAX = 15;
        var GRID_SIZE = 8; //Tamaño en pixeles del cuadrado que contendra los marcadores
        if (markerClusterer != null) {
            markerClusterer.clearMarkers();			
        }
        markerClusterer = new MarkerClusterer(googleMap, arrMarkers, {
            maxZoom: ZOOM_MAX, 
            gridSize: GRID_SIZE, 
            styles: styles[0]
        });

    }


    _brokers.on("load", function(s,r){
        var _obj_coordinates = GetGMCoordinates();
        var _showBrokers = new Ext.XTemplate(
            "<input type=\"hidden\" name=\"maxX\" value=\""+_obj_coordinates.maxX+"\" />",
            "<input type=\"hidden\" name=\"maxY\" value=\""+_obj_coordinates.maxY+"\" />",
            "<input type=\"hidden\" name=\"minX\" value=\""+_obj_coordinates.minX+"\" />",
            "<input type=\"hidden\" name=\"minY\" value=\""+_obj_coordinates.minY+"\" />",
            "<tpl for=\".\">",
            "<tpl for=\"data\">",
            "<tpl if=\"this.noLimit(xindex) == true\">",
            "<div>",
            "<tpl if=\"this.noSrc(pic_square) == false\">",
            "<div style='width:142px;float:top;height:13px;font-size:11px;font-weight:bold;'>",
            "<a href=\""+ __environment('facebook_url') + "enlistar/propiedadesautor?id={Uid}\" target=\"_top\"><span>{NombreCliente}</span></a>",
            "</div>",
            "<div style='float:left;width:60px;'>",           
            "<a target='_blank' href=\""+ __environment('facebook_url') + "enlistar/propiedadesautor?id={Uid}\" target=\"_top\"><img src=\"{pic_square}\" alt=\"{NombreCliente}\" onerror=\"this.src='/img/q_silhouette.gif'\" /></a>",
            "</tpl>",
            "<tpl if=\"this.noSrc(pic_square) == true\">",
            "<a target='_blank' href=\""+ __environment('facebook_url') + "enlistar/propiedadesautor?id={Uid}\" target=\"_top\"><img src=\"/img/q_silhouette.gif\" alt=\"{NombreCliente}\" width=\"120\" height=\"90\"/></a>",
            "</tpl>",
            "</div>",  
            "<div class='button_r' id='button_r'>", 
            "<div class='name_top'  id='name_top'>",
            "<a target='_blank'  style='text-decoration:none;color:#444;' href=\""+ __environment('facebook_url') + "enlistar/propiedadesautor?id={Uid}\" target=\"_top\" >Properties</a>",
            "</div>",
            "<tpl if=\"this.fanpage(pageid) == false\">",
            "<div class='name_top'  id='name_top' ><a target='_blank' href='https://www.facebook.com/pages/housemarket/{pageid}' style='text-decoration:none;color:#444;' >Fanpage</a></div>",
            "</tpl>",
            "<tpl if=\"this.webpage(WebPage) == false\">",
            "<div class='name_top' id='name_top'><a target='_blank' href='{WebPage}' style='text-decoration:none;color:#444;' >Website</a></div>",        
            "</tpl>",
            "</div>",             
            "</div>", 
            "</tpl>",
            "</tpl>",
            "</tpl>",{
                compiled:true,
                noSrc:function(src){
                    return src === "";
                },
                fanpage : function(pageid){
                    return (pageid == null || pageid == "0");
                },
                webpage : function(WebPage){
                    return (WebPage == "" || WebPage == null || WebPage.length <= 8); 
                },
                noLimit:function(index){
                    return 9 > index;
                },
                credits: function(creditos){
                    return creditos > 0;
                }
            });
        if(_brokers.getTotalCount() > 0) {
            $("#see_all_brokers").show();
            _showBrokers.overwrite("allBrokers",r);
            $("#see_all_count").html(_brokers.getTotalCount());
            $("#allBrokers").show();
        } else {
            $("#see_all_brokers").hide();
            $("#allBrokers").hide();
        }
    });

    _filtradas.on("load",function(s,r){
       
        var itemsPerPage = 5;// offset
        var linksToDraw = 4; // Total of links to print in pagination
        
        function Total (totalRecords, itemsPerPage){
            var totalPages = Math.ceil(totalRecords/itemsPerPage);
            return totalPages;
        }

        /**
         * Calcula el intervalo de enlaces a dibujar a partir de la pagina seleccionada
         * el numero de enlaces y su cota superior.
         * @return {Array}
         */
        function getInterval(num_display_entries, numPages, selected_page)  {
            var upper_limit = numPages - num_display_entries + 1;
            upper_limit = (upper_limit > 1)? upper_limit : 1;

            var start = selected_page >= upper_limit? upper_limit :  selected_page;
            var end = selected_page < upper_limit? selected_page + num_display_entries -1 : numPages ;

            return [start,end];
        }

        function DrawConsecutivesLinks(beginingLink, totalPages, selectedPage, curPage, linksToDraw){
            var legend = "";
            var classCurrentPagination = "paginacion_cont_li current";
            var classPagination = "paginacion_cont_li";
            var interval = getInterval(linksToDraw, totalPages, selectedPage);

            for(var i=interval[0]; i<=interval[1]; i++){
                var classLink = (selectedPage == i)? classCurrentPagination : classPagination;
                legend += "<li class=\""+classLink+"\"><a class=\"paginacion_cont_a\" href=\"javascript:;\">"+i+"</a></li>";
            }

            return legend;
        }

        function lastAction(beginingLink, totalPages, selectedPage,firstLink, prevLink, curPage, linksToDraw){
            var links  = firstLink;
            links = links + prevLink;
            links = links + DrawConsecutivesLinks(beginingLink, totalPages, selectedPage, curPage, linksToDraw);
            links = links + "<li class=\"paginacion_cont_li\"><a href=\"javascript:;\"><img src='/img/page_next_gray.gif'/></a></li>";
            links = links + "<li class=\"paginacion_cont_li\"><a href=\"javascript:;\"><img src='/img/page_last_gray.gif'/></a></li>";
            return links;
        }

        function firstAction(beginingLink, totalPages, selectedPage, nextLink, lastLink, curPage, linksToDraw){
            var links = "";
            if(totalPages > 1){
                links = "<li class=\"paginacion_cont_li\"><a href=\"javascript:;\"><img src='/img/page_first_gray.gif'/></a></li>";
                links = links + "<li class=\"paginacion_cont_li\"><a href=\"javascript:;\"><img src='/img/page_previous_gray.gif'/></a></li>";
                links = links + DrawConsecutivesLinks(beginingLink, totalPages, selectedPage, curPage, linksToDraw);
                links = links + nextLink;
                links = links + lastLink;
            }else {
                links = DrawConsecutivesLinks(beginingLink, totalPages, selectedPage, curPage, linksToDraw);
            }
            return links;
        }

        function printAllLinks(beginingLink, totalPages, selectedPage, firstLink, nextLink, prevLink, lastLink, curPage, linksToDraw){
            var links = firstLink;
            links = links + prevLink;
            links = links + DrawConsecutivesLinks(beginingLink, totalPages, selectedPage, curPage, linksToDraw);
            links = links + nextLink;
            links = links + lastLink;
            return links;
        }

        function printPagination(currentPage, selectedPage, totalPages, totalLinks){
            var beginingLink = 1;
            var links = "<ul class=\"paginacion_cont\">";
            var prevLink = "<li class=\"paginacion_cont_li\" name=\"plpp\" id=\"plpp\"><a class=\"paginacion_img\" href=\"javascript:;\"><img id=\"previous\" src='/img/page_previous_blue.gif'/></a></li>";
            var nextLink = "<li class=\"paginacion_cont_li\" name=\"nlpp\" id=\"nlpp\"><a class=\"paginacion_img\" href=\"javascript:;\"><img id=\"next\" src='/img/page_next_blue.gif'/></a></li>";
            var firstLink = "<li class=\"paginacion_cont_li\" name=\"flpp\" id=\"flpp\"><a class=\"paginacion_img\" href=\"javascript:;\"><img id=\"first\" src='/img/page_first_blue.gif'/></a></li>";
            var lastLink = "<li class=\"paginacion_cont_li\" name=\"llpp\" id=\"llpp\"><a class=\"paginacion_img\" href=\"javascript:;\"><img id=\"last\" src='/img/page_last_blue.gif'/></a></li>";

            switch (selectedPage) {
                case 1:
                    //implementar last
                    links += firstAction(beginingLink, totalPages, selectedPage, nextLink, lastLink, currentPage, totalLinks);
                    break;
                case totalPages:
                    //implementar first
                    links += lastAction(beginingLink, totalPages, selectedPage, firstLink, prevLink, currentPage, totalLinks);
                    break;
                default:
                    //imprimir todo
                    links += printAllLinks(beginingLink, totalPages, selectedPage, firstLink, nextLink, prevLink, lastLink, currentPage, totalLinks);

                    break;
            }

            return links + "</ul>";
        }

        function getInfoResults(selectedPage, offset, resulsetNumber, totalRecords){
            var firstOcurrence = (selectedPage - 1) * offset + 1;
            var lastOcurrence = firstOcurrence + resulsetNumber -1;

            return firstOcurrence + " " + __translate("SEARCH_RESULT_TO") + " " + lastOcurrence + " " + __translate("SEARCH_RESULT_OF") + " " + totalRecords + " " + __translate("SEARCH_RESULT_PROPERTY");
        }

        var page = s.reader.jsonData.cPage; //Page in use
        var selPage = s.reader.jsonData.sPage; //selected page
        var totalRecords = s.reader.jsonData.total; //total de registros
        totalPages = Total(totalRecords, itemsPerPage);

        var paginacion = printPagination(page, selPage, totalPages, linksToDraw);
        var infoPagination = getInfoResults(selPage, itemsPerPage, r.length, totalRecords);
       
        var _galery=new Ext.XTemplate(
            "<div id=\"paginacion_superior\">",
            paginacion,
            "</div>", 
            "<div id=\"listado_items\">",
            "<tpl for=\".\">",
            "<tpl for=\"data\">",
            "<div id=\"propiedad_item_{[xindex]}\" class=\"propiedad_item\">",
            "<div style=\"height: 55px;\">",
            "<div class=\"imagen_propiedad\">",
            "<tpl if=\"this.noSrc(PROFotoID) == true\">",
            "<img src=\"" + __translate("AD_NO_IMAGE_RESULT") +"\" width=\"75\" height=\"56\" id=\"{id}\" />",
            "</tpl>",
            "<tpl if=\"this.noSrc(PROFotoID) == false\">",
            "<img src=\"/pic{PROFotoID}_4.jpg\" width=\"80\" height=\"60\" id=\"{id}\" onerror=\"this.src='" + __translate("AD_NO_IMAGE_RESULT") +"'\"/>",
            "</tpl>",
            "</div>",
            "<div class=\"detalle_propiedad\">",
            "<a href=\"#\"><b id=\"{id}\">{nombre}</b></a><br/>",
            "{sale} {rent} {bothPrice} {foreclosure}<br/>",                                 
            "<tpl if=\"(Area)!=null\">",
            "{Area} " + "{UnidadMedida}"  + " | ",
            "</tpl>",
            "<tpl if=\"(cuarto)!=null\">",
            "{cuarto} " +__translate("SEARCH_RESULT_FOR_ROOM")+ " | ",
            "</tpl>",
            "<tpl if=\"(bano)!=null\">",
            "{bano} " + __translate("SEARCH_RESULT_FOR_BATH"),
            "</tpl>",
            // "<p>{Area}"+ " " +"{UnidadMedida}" + " | "+"{cuarto} "+__translate("SEARCH_RESULT_FOR_ROOM")+ " | "+"{bano} " + __translate("SEARCH_RESULT_FOR_BATH")+"</p>",
            "</div>",
            "</div>",
            "<div class=\"clear\"></div>",
            "</div>",
            "</tpl>",
            "</tpl>",
            "</div>",
            "<div id=\"paginacion_inferior\">",
            paginacion,
            "</div>",
            "<div id=\"listado_footer\">",
            __translate("SEARCH_RESULT_LABEL") + " "+ infoPagination,
            "</div>",
            {
                compiled:true,
                noSrc:function(src){
                    return src==null;
                }
            });
        _galery.overwrite("galery",r);

        if(totalRecords == 0) {
            $("#paginacion_superior, #paginacion_inferior, #listado_footer").hide();
        } else {
            $("#paginacion_superior, #paginacion_inferior, #listado_footer").show();
        }

        function LoadPropertiesGrid(curPage, selectedPage, start, offset){
            var _coordinates = GetGMCoordinates();
            var frmFiltro = GetObjectFrmFilter();
            _filtradas.load({
                params:{
                    "country":country,
                    "city":city,
                    "category":frmFiltro.category,
                    "type":frmFiltro.type,
                    "min_price":frmFiltro.min_price,//frmFiltro no esta definido
                    "max_price":frmFiltro.max_price,
                    "max_area":frmFiltro.max_area,
                    "min_area":frmFiltro.min_area,
                    "uni_area":frmFiltro.area_uni,
                    "max_area_lote":frmFiltro.max_area_lote,
                    "min_area_lote":frmFiltro.min_area_lote,
                    "uni_area_lote":frmFiltro.area_lote_uni,
                    "parameters": Ext.encode(frmFiltro.parameters),
                    "coordinates":Ext.encode(_coordinates),
                    "sPage" : selectedPage,
                    "cPage" : curPage,
                    "start" : start,
                    "limit" : offset,
                    "total" : totalRecords
                }
            });
        }
        //Pagination Link Action
        $("a.paginacion_cont_a").click(function(){
            var selectedPage = $(this).text();
            var curPage = $("div#paginacion_superior > ul > li.current").text() || 1;
            if(curPage != selectedPage){
                var start = itemsPerPage * (selectedPage - 1);
                LoadPropertiesGrid(curPage, selectedPage, start, itemsPerPage);
            }
        });
        //Last Action
        $("li#llpp > a").click(function(){
            var selectedPage = totalPages;
            var curPage = $("div#paginacion_superior > ul > li.current").text() || 1;
            if(curPage != selectedPage){
                var start = itemsPerPage * (selectedPage - 1);
                LoadPropertiesGrid(curPage, selectedPage, start, itemsPerPage);
            }
        });
        //First Action
        $("li#flpp > a").click(function(){
            var selectedPage = 1;
            var curPage = $("div#paginacion_superior > ul > li.current").text() || 1;
            if(curPage != selectedPage){
                var start = itemsPerPage * (selectedPage - 1);
                LoadPropertiesGrid(curPage, selectedPage, start, itemsPerPage);
            }
        });
        //Next Action
        $("li#nlpp > a").click(function(){
            var curPage = $("div#paginacion_superior > ul > li.current").text() || 0;
            curPage = parseInt(curPage);
            var selectedPage = curPage + 1;
            if(curPage != selectedPage){
                var start = itemsPerPage * (selectedPage - 1);
                LoadPropertiesGrid(curPage, selectedPage, start, itemsPerPage);
            }
        });
        //Previous Action
        $("li#plpp > a").click(function(){
            var curPage = $("div#paginacion_superior > ul > li.current").text() || 1;
            curPage = parseInt(curPage);
            var selectedPage = curPage -1;
            if(curPage != selectedPage){
                var start = itemsPerPage * (selectedPage - 1);
                LoadPropertiesGrid(curPage, selectedPage, start, itemsPerPage);
            }
        });		
       

        $("div.propiedad_item").click(function(){
            var z=$("b",this).attr("id");
            if(z==null||z==""){
            }else{   
                myInfoWindow.setContent(_gMarkerContentList[z] ); 
                
                myInfoWindow.open(googleMap,_gMarkerList[z]);

            // googleMap.openInfoWindowHtml(new google.maps.LatLng(_gMarkerList[z].Ca.y, _gMarkerList[z].Ca.x ),_gMarkerContentList[z]);
            //_gMarkerList[z].openInfoWindowHtml(_gMarkerContentList[z]);
            }
        });
       
        $("div.last_one").click(function(){            								   
            var z=$("b",this).attr("id");
            if(z==null||z==""){
				
            }else{
                
                myInfoWindow.setContent(_gMarkerContentList[z]); 
               
                myInfoWindow.open(googleMap,_gMarkerList[z]);
            }
        }); 
        $("div.destacadas_item").click(function(){            								   
            var z=$("b",this).attr("id");            
            if(z==null||z==""){			
            }else{
                
                MyInfoWindow.setContent(_gMarkerContentList[z]);            
                MyInfoWindow.open(googleMap,_gMarkerList[z]);
            }
        }); 
        $("div.first_one").click(function(){										   
            var z=$("b",this).attr("id");
            if(z==null||z==""){
				
            }else{                
                myInfoWindow.setContent(_gMarkerContentList[z]); 
                myInfoWindow.open(googleMap,_gMarkerList[z]);
            }
        });  


        $("a.paginacion_img  > img").hover(function(){
            var id = this.id;
            this.src = "/img/page_"+id+"_green.gif";
        }, function(){
            var id = this.id;
            this.src = "/img/page_"+id+"_blue.gif";
        }
        );

        // Procesar si esta cacheada.
        var cachedState = Ext.state.Manager.get("cached");
        if(cachedState) {
            Ext.state.Manager.set("cached", false);
            SearchByDir();
        }
    });

   

    var allBroker = $("#link_all_brokers");
    if(allBroker) {
        allBroker.click(function(){
            document.getElementById("frmAllBrokers").submit();
            return false;
        });
    }

    
    var body =  Ext.getBody();
    body.addListener("unload", function() {
        var frmU = GetObjectFrmUbicar();
        var frmF = GetObjectFrmFilter();
        Ext.state.Manager.set("cached", true);
        Ext.state.Manager.set("ub", Ext.encode(frmU));
        Ext.state.Manager.set("fi", Ext.encode(frmF));
    });
}); // Fin Ext.onReady