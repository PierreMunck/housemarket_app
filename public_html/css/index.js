/**
 * Aplica internacionalizacion
 */
function __translate(s) {
    if (typeof(i18n)!='undefined' && i18n[s]) {
        return i18n[s];
    }
    return s;
}

/**
 * Recupera variables de entorno
 */
function __environment(s) {
    if (typeof(env)!='undefined' && env[s]) {
        return env[s];
    }
    return s;
}

/**
 * Oculta muestra los filtros adicionales
 */
function toggleOption() {
    $('#optional_filter').slideToggle('slow');
    if($('#img_search_advanced').attr("src") == '/img/down_search.png') {
        $('#img_search_advanced').attr('src', '/img/up_search.png');
    } else{
        $('#img_search_advanced').attr('src', '/img/down_search.png');
    }
};

Ext.onReady(function(){
    if(Ext.state.Manager.getProvider() == null) {
        Ext.state.Manager.setProvider(new Ext.state.CookieProvider({
            path:"/",
            expires: new Date(new Date().getTime()+(1000*60*60*24*365))
        }));
    }

    Ext.BLANK_IMAGE_URL="/js/extjs/resources/images/gray/s.gif";
    Ext.QuickTips.init();

    var gmResponseCode=[];
    gmResponseCode[G_GEO_SUCCESS]= __translate("G_GEO_SUCCESS");
    gmResponseCode[G_GEO_MISSING_ADDRESS]=__translate("G_GEO_MISSING_ADDRESS");
    gmResponseCode[G_GEO_UNKNOWN_ADDRESS]=__translate("G_GEO_UNKNOWN_ADDRESS");
    gmResponseCode[G_GEO_UNAVAILABLE_ADDRESS]=__translate("G_GEO_UNAVAILABLE_ADDRESS");
    gmResponseCode[G_GEO_BAD_KEY]=__translate("G_GEO_BAD_KEY");
    gmResponseCode[G_GEO_TOO_MANY_QUERIES]=__translate("G_GEO_TOO_MANY_QUERIES");
    gmResponseCode[G_GEO_SERVER_ERROR]=__translate("G_GEO_SERVER_ERROR");
    var gmStarIcon=new GIcon(G_DEFAULT_ICON);
    gmStarIcon.image="/img/house.png";
    gmStarIcon.iconSize=new GSize(24,33);
    gmStarIcon.shadowSize =new GSize(0,0);
    var _gmStarIcon={
        icon:gmStarIcon
    };
    var gmIcon=new GIcon(G_DEFAULT_ICON);
    gmIcon.image="/img/house_normal.png";
    gmIcon.shadow="/img/blank.gif";
    gmIcon.iconSize=new GSize(9,9);
    gmIcon.shadowSize =new GSize(0,0);
    gmIcon.iconAnchor = new GPoint(5, 5);
    gmIcon.infoWindowAnchor = new GPoint(5, 5);
    var _gmIcon={
        icon:gmIcon
    };

    function _GetParametersListURL(){
        var _parameters={};
        var _6=facebook_perms.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,_index,_value){
            _parameters[_index]=unescape(_value);
        });
        return _parameters;
    };
    
    var _parametersList=_GetParametersListURL();
    var fb=Ext.urlDecode(Ext.urlEncode(_parametersList));
    var _urlParameters=unescape(Ext.urlEncode(_parametersList));

    // Extractor de datos para las consultas de Ubicar en Mapa
       var _toGM=new Ext.data.Store({
        url:"/index/estatetogm"+"?"+_urlParameters,
        baseParams:fb,
        reader:new Ext.data.JsonReader({
            root:"rows",
            id:"CodigoPropiedad"
        },[{
            name:"CodigoPropiedad"
        },{
            name:"NombrePropiedad"
        },{
            name:"DescPropiedad"
        },{
            name:"Direccion"
        },{
            name:"Estado"
        },{
            name:"Ciudad"
        },{
            name:"ZipCode"
        },{
            name:"moneda"
        },{
            name:"PrecioVenta"
        },{
            name:"PrecioAlquiler"
        },{
            name:"FechaRegistro"
        },{
            name:"FechaModifica"
        },{
            name:"CodigoCategoria"
        },{
            name:"Latitud"
        },{
            name:"Longitud"
        },{
            name:"Uid"
        },{
            name:"Accion"
        },{
            name:"PROFotoID"
        },{
            name:"credito"
        }]),
        sortInfo:{
            field:"credito",
            direction:"DESC"
        }
    });


    // Extractor de datos para las consultas de Ubicar en Mapa
    // Muestra las imagenes del carrusel superior

    var _destacadas=new Ext.data.Store({
        url:"/index/fotosdestacada"+"?"+_urlParameters,
        baseParams:fb,
        reader:new Ext.data.JsonReader({
            root:"rows",
            id:"id"
        },[{
            name:"id"
        },{
            name:"PROFotoID"
        },{
            name:"creditos"
        },{
            name:"FKCodigoPropiedad"
        }]),
        sortInfo:{
            field:"creditos",
            direction:"DESC"
        }	
    });

/*
 _destacadas.on("beforeload",function(s){
         var _coordinates = GetGMCoordinates();
		        _destacadas.baseParams={
            "country":country,
            "city":city,
            "coordinates": Ext.encode(_coordinates)
        };
    });

    _destacadas.on("loadexception", function(p, o, r, e) {
        alert(r.responseText);
       alert(e);
    });
*/

    // Muestra las imagenes del carrusel inferior
    var _filtradas=new Ext.data.Store({
        url:"/index/fotosfiltradas"+"?"+_urlParameters,
        baseParams:fb,
        reader:new Ext.data.JsonReader({
            root:"rows",
            id:"id"
        },[{
            name:"nombre"
        },{
            name:"descripcion"
        },{
            name:"direccion"
        },{
            name:"estado"
        },{
            name:"ciudad"
        },{
            name:"zipcode"
        },{
            name:"categoria"
        },{
            name:"foreclosure"
        },{
            name:"fechareg"
        },{
            name:"sale"
        },{
            name:"rent"
        },{
            name:"bothPrice"
        },{
            name:"Uid"
        },{
            name:"PROFotoID"
        },{
            name: "id"
        }])
    });

//Guarda los brokers segun el area seleccionada
    var _brokers = new Ext.data.Store({
        url: "/index/brokersByArea"+"?"+_urlParameters,
        baseParams:fb,
        reader: new Ext.data.JsonReader({
            root:"rows",
            id:"Uid"
        },
        [{
            name:"NombreCliente"
        },{
            name:"Uid"
        },{
           name: "pic_square"
        }
        ])
    });

    var _e;
    var country;
    var city;
    var googleMap;
    var _gMarkerList=[];
    var _gMarkerContentList=[];
    var _attributeList=[];
    var x="";
    Ext.apply(Ext.form.VTypes,{
        numbersonly:function(val,_12){
            var n=parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        },
        numbersonlyText:__translate("FILTER_ONLY_NUMBERS")
    });

    if(GBrowserIsCompatible()){
        function _putGMMarker(_gLatLng,_id,_content,_icon){
            x=_id;
            var opts = new Object();
            opts.icon = _icon.icon;
            opts.zIndexProcess = function(marker) {
                return marker.credits;
            };
            var _gMarker=new GMarker(_gLatLng, opts);
            GEvent.addListener(_gMarker,"click",function(){
                _gMarker.openInfoWindowHtml(_content);
            });
            _gMarkerList[x]=_gMarker;
            _gMarkerContentList[x]=_content;
            x="";
            return _gMarker;
        };

        googleMap=new GMap2(document.getElementById("map"));
        var customUI = googleMap.getDefaultUI();
        customUI.zoom.doubleclick = false;
        customUI.zoom.scrollwheel = false;
        googleMap.setUI(customUI);

        var _isGMInfoWindowOpen;
        _isGMInfoWindowOpen=false;

        function _onGMInfoWindowOpen(){
            _isGMInfoWindowOpen=true;
        };

        function _onGMMoveEnd(){
            if(!_isGMInfoWindowOpen){
                _onGMDragEnd();
            }
            _isGMInfoWindowOpen=false;
        };

        function _onGMDragEnd(){
            loadToGM(country,city);
        };

        function _onGMZoomEnd() {
        };

        // Agregar eventos al Google Map
        GEvent.addListener(googleMap,"zoomend",_onGMZoomEnd);
        GEvent.addListener(googleMap,"dragend",_onGMDragEnd);
        GEvent.addListener(googleMap,"moveend",_onGMMoveEnd);
        GEvent.addListener(googleMap,"infowindowopen",_onGMInfoWindowOpen);

        function _2e(){
            var _gmCoordinates=GetGMCoordinates();
            var _30=_gmCoordinates.maxX-_gmCoordinates.minX;
            var _31=_gmCoordinates.maxY-_gmCoordinates.minY;
            googleMap.queryCenter=googleMap.getCenter();
            googleMap.maxY=_gmCoordinates.maxY+_31*0.2;
            googleMap.maxX=_gmCoordinates.maxX+_30*0.2;
            googleMap.minY=_gmCoordinates.minY-_31*0.2;
            googleMap.minX=_gmCoordinates.minX-_30*0.2;
        };

        // Coordenadas a pasarle al Google Map cuando se realice el centrado
        // Por defecto, se ubica de acuerdo la IP del cliente
        var _gmResultCoordinates={
            lat:"25.79",
            lng:"4.21"
        };

        // Condicionar, porque si ya existe en memoria, no mandar a centrar.
        var cachedState = Ext.state.Manager.get("cached");
        if(cachedState) {
            var frmU = Ext.state.Manager.get("ub");
            var frmF = Ext.state.Manager.get("fi");
            var frmUbi = Ext.decode(frmU);
            var frmFilter = Ext.decode(frmF);
            $("#dir").val(frmUbi.dir);
            $("#cat").val(frmFilter.category);
            $("#type").val(frmFilter.type);
            $("#min_price").val(frmFilter.min_price);
            $("#max_price").val(frmFilter.max_price);
            $("#min_area").val(frmFilter.min_area);
            $("#max_area").val(frmFilter.max_area);
            $("#min_area_lote").val(frmFilter.min_area_lote);
            $("#max_area_lote").val(frmFilter.max_area_lote);
            $("#area_lote").val(frmFilter.area_lote_uni);
            $("#area").val(frmFilter.area_uni);
        }

        Ext.Ajax.request({
            url:"/index/centermap"+"?"+_urlParameters,
            params:fb,
            success:function(_response,_options){
                try{
                    var rs=Ext.util.JSON.decode(_response.responseText);
                    _gmResultCoordinates=rs.data[0];
                    googleMap.setCenter(new GLatLng(_gmResultCoordinates.lat,_gmResultCoordinates.lng),12);
                    loadToGM(_gmResultCoordinates.cc,_gmResultCoordinates.city);
                }
                catch(err){
                    googleMap.setCenter(new GLatLng(25.79,4.21),1);
                }
            },
            failure:function(_response,_options){
                googleMap.setCenter(new GLatLng(_gmResultCoordinates.lat,_gmResultCoordinates.lng),1);
            }
        });
        function GetGMCoordinates(){
            var _gmBounds=googleMap.getBounds();
            var _gmBoundsSW = _gmBounds.getSouthWest();
            var _gmBoundsNE = _gmBounds.getNorthEast();
            var _gmSWLng=_gmBoundsSW.lng();
            var _gmSWLat=_gmBoundsSW.lat();
            var _gmNELng=_gmBoundsNE.lng();
            var _gmNELat=_gmBoundsNE.lat();

            return {
                minX:_gmSWLng,
                maxX:_gmNELng,
                minY:_gmSWLat,
                maxY:_gmNELat
            };
        };

        var dir=Ext.get("dir");
        function _45(m,min,max){
            var _46=parseInt(min,10);
            var _47=parseInt(max,10);
            var _48=m.substring(4);
            var _49=m.substring(3,0);
            if(_49=="min"){
                if((_46>_47)&&(_47>=0)){
                    Ext.getDom("max_"+_48).value=0;
                }
            }else{
                if(_49=="max"){
                    if((_47<_46)&&(_46>=0)){
                        Ext.getDom("min_"+_48).value=0;
                    }
                }
            }
        };

        function _4a(num){
            var _4b=false;
            if(Ext.isEmpty(num)){
                _4b=true;
            }else{
                _4b=Ext.num(num,false);
                if(_4b<0){
                    _4b=false;
                }
            }
            return _4b;
        };

        /**
         * Cargar los datos en el google map a partir del pais y la ciudad
         */
        function loadToGM(_country, _city){
            if(_country) {
                country = _country;
                city = _city;
                var _coordinates = GetGMCoordinates();
                var frmFiltro = GetObjectFrmFilter();

                _toGM.load({
                    params:{
                        "country":country,
                        "city":city,
                        "category":frmFiltro.category,
                        "type":frmFiltro.type,
                        "min_price":frmFiltro.min_price,
                        "max_price":frmFiltro.max_price,
                        "max_area":frmFiltro.max_area,
                        "min_area":frmFiltro.min_area,
                        "uni_area":frmFiltro.area_uni,
                        "max_area_lote":frmFiltro.max_area_lote,
                        "min_area_lote":frmFiltro.min_area_lote,
                        "uni_area_lote":frmFiltro.area_lote_uni,
                        "parameters": Ext.encode(frmFiltro.parameters),
                        "coordinates":Ext.encode(_coordinates)
                    }
                });

                _destacadas.load({
                    params:{
                        "country":country,
                        "city":city,
                        "coordinates":Ext.encode(_coordinates)
                    }
                });

                _brokers.load({
                    params:{
                        "coordinates":Ext.encode(_coordinates)
                    }
                });

                _filtradas.load({
                    params:{
                        "country":country,
                        "city":city,
                        "category":frmFiltro.category,
                        "type":frmFiltro.type,
                        "min_price":frmFiltro.min_price,
                        "max_price":frmFiltro.max_price,
                        "max_area":frmFiltro.max_area,
                        "min_area":frmFiltro.min_area,
                        "uni_area":frmFiltro.area_uni,
                        "max_area_lote":frmFiltro.max_area_lote,
                        "min_area_lote":frmFiltro.min_area_lote,
                        "uni_area_lote":frmFiltro.area_lote_uni,
                        "parameters": Ext.encode(frmFiltro.parameters),
                        "coordinates":Ext.encode(_coordinates),
                        "cPage" : 1,
                        "sPage" : 1,
                        "total" : 1
                    }
                });

            }
        };

        /**
         * Resetear el formulario de b�squeda
         **/
//        var reset = $("#btnReset");
//        if(reset) {
//            reset.click(function(e) {
//                $("#frmFgm").get(0).reset();
//                toggleOption();
//            });
//        }

        /**
         * Para ubicar en el mapa.
         */
        
		/*var findBB=Ext.get("btnFB");
        findBB.on("click",function(){
            //SearchByDir();
			alert("Test FB");
        });*/
		
		var findGM=Ext.get("btnFgm");
        findGM.on("click",function(){
            SearchByDir();
        });

	

        /**
         * Expandir / Colapsar filtros adicionales del mapa
         **/
//        var advanced = $("#advanced");
//        if(advanced) {
//            advanced.click(function() {
//                toggleOption();
//                return false;
//            });
//        }

        /**
         * Mostrar / Ocultar filtros adicionales por categorias
         **/
        var categories = $("#cat");
        if(categories) {
            categories.change(function() {
                name = $("#cat :selected").text().split(' ').join('') + "_";
                $("#optional_filter div").each(function(){
                    div_name = $(this).attr("id");
                    if(div_name.indexOf(name) == 0) {
                        $(this).attr("class", "show");
                    } else if(div_name.indexOf("fixed") == -1 && div_name.indexOf("buttons") == -1){
                        $(this).attr("class", "hide");
                    }
                });
            });
        }

        /**
         * Aplicar filtros adicionales a la busqueda
         **/
        var apply = $("#btnApply");
        if(apply) {
            apply.click(function(e){
                var frmFiltro = GetObjectFrmFilter();
                var _coordinates = GetGMCoordinates();

                _toGM.load({
                    params:{
                        "country":country,
                        "city":city,
                        "category":frmFiltro.category,
                        "type":frmFiltro.type,
                        "min_price":frmFiltro.min_price,
                        "max_price":frmFiltro.max_price,
                        "max_area":frmFiltro.max_area,
                        "min_area":frmFiltro.min_area,
                        "uni_area":frmFiltro.area_uni,
                        "max_area_lote":frmFiltro.max_area_lote,
                        "min_area_lote":frmFiltro.min_area_lote,
                        "uni_area_lote":frmFiltro.area_lote_uni,
                        "parameters": Ext.encode(frmFiltro.parameters),
                        "coordinates":Ext.encode(_coordinates)
                    }
                });
			







                _filtradas.load({
                        params:{
                            "category":frmFiltro.category,
                            "type":frmFiltro.type,
                            "min_price":frmFiltro.min_price,
                            "max_price":frmFiltro.max_price,
                            "max_area":frmFiltro.max_area,
                            "min_area":frmFiltro.min_area,
                            "uni_area":frmFiltro.area_uni,
                            "max_area_lote":frmFiltro.max_area_lote,
                            "min_area_lote":frmFiltro.min_area_lote,
                            "uni_area_lote":frmFiltro.area_lote_uni,
                            "parameters": Ext.encode(frmFiltro.parameters),
                            "coordinates":Ext.encode(_coordinates),
                            "cPage" : 1,
                            "sPage" : 1,
                            "total" : 1
                        }
                });
            });
        }

        // Recupera un objeto que represente al formulario de Ubicar en el Mapa
        function GetObjectFrmUbicar() {
            var _frmSerUbicar=Ext.Ajax.serializeForm("frmFgm"); // Serializar el formulario
            var _frmDecUbicar=Ext.urlDecode(_frmSerUbicar); // Decodificar los valores de los elementos del formulario
            var _dir=_frmDecUbicar.dir; // Recuperar la direccion proporcionada por el usuario
            if(_frmDecUbicar.dir==__translate("TEXT_SEARCH_SAMPLE")){
                _dir="";
            }

            return {
                dir:_dir
            };
        }

        // Recuperar un objeto que represente al formulario de Filtros
        function GetObjectFrmFilter() {

            var _cat = $("#cat").val();
            var _type = $("ul#listing_tabs li.active").attr("valor");//$("#type").val();
            var _price_min = $("#price-min").val();
            var _price_max = $("#price-max").val();

            if(_price_max == "MAX") {
                _price_max = "";
            }
            if(_price_min == "MIN") {
                _price_min = "";
            }

            var _area_min = $("#area-min").val();
            var _area_max = $("#area-max").val();
            if(_area_max == "MAX") {
                _area_max = "";
            }
            if(_area_min == "MIN") {
                _area_min = "";
            }
            var _uni_area = $("#area").val();

            var _area_lote_min = $("#area_lote-min").val();
            var _area_lote_max = $("#area_lote-max").val();
            if(_area_lote_max == "MAX") {
                _area_lote_max = "";
            }
            if(_area_lote_min == "MIN") {
                _area_lote_min = "";
            }
            var _uni_area_lote = $("#area_lote").val();

            var parameters = [];
            var name = $("#cat :selected").text();

            var nombreCat = 0;
            var codigoAtr = 1;
            $("#optional_filter div[id^='" + name + "_'].show").each(function() {
                var id = $(this).attr("id");
                if(id.indexOf('_') > 0) {
                    var sections = id.split("_");
                    // Recuperar el correspondiente a max
                    var maximo = $("#" + sections[nombreCat] + "-max_" + sections[codigoAtr]);
                    var max = maximo.val();
                    if(max=="MAX") max = "";

                    // Recuperar el correspondiente a min
                    var minimo = $("#" + sections[nombreCat] + "-min_" + sections[codigoAtr]);
                    var min = minimo.val();
                    if(min=="MIN") min = "";

                    // Recuperar el correspondiente a valor
                    var value = $("#" + sections[nombreCat] + "-value_" + sections[codigoAtr])
                    var val = value.val();

                    // Recuperar el correspondiente a checked
                    if(value.attr("type")=="checkbox") {
                        if(value.get(0).checked) {
                            val = "S";
                        } else {
                            val = "N";
                        }
                    }

                    parameters.push({codigo : sections[codigoAtr], min : min, max : max, val : val});
                }
            });

            return {
                category:_cat,
                type:_type,
                min_price:_price_min,
                max_price:_price_max,
                min_area:_area_min,
                max_area:_area_max,
                area_uni:_uni_area,
                min_area_lote:_area_lote_min,
                max_area_lote:_area_lote_max,
                area_lote_uni:_uni_area_lote,
                parameters:parameters
            };
        }

        function SearchByDir() {
            var frmUbicar = GetObjectFrmUbicar();
            var _dir = frmUbicar.dir;

            var firstPlaceMark;
            var firstPlaceMarkAccuracy;
            var statusCode;
            if(!isEmpty(_dir)){
                _gClientGeoDecoder.getLocations(_dir,function(response){
                    if(!response){
                        Ext.MessageBox.alert(__translate("G_GEO_ALERT_TITLE"),__translate("G_GEO_ALERT_MESSAGE_FAIL"));
                    }else{
                        statusCode=response.Status.code;
                        if(statusCode==200){
                            if(response.Placemark.length==1){
                                firstPlaceMark=response.Placemark[0];
                                firstPlaceMarkAccuracy=firstPlaceMark.AddressDetails.Accuracy;
                                if(!response.Placemark[0].address){
                                }else{
                                    dir=response.Placemark[0].address;
                                }
                                switch(firstPlaceMarkAccuracy){
                                    case 0:
                                        Ext.MessageBox.alert(__translate("G_GEO_ALERT_TITLE"),__translate("G_GEO_ALERT_MESSAGE_AMBIGUOUS"));
                                        break;
                                    default:
                                        country=!firstPlaceMark.AddressDetails.Country.CountryNameCode?"":firstPlaceMark.AddressDetails.Country.CountryNameCode;
                                        var firstPlaceMarkAddressParts=firstPlaceMark.address.split(",");
                                        if(!firstPlaceMarkAddressParts){
                                            city="";
                                        }else{
                                            city=!firstPlaceMarkAddressParts[firstPlaceMarkAddressParts.length-2]?"":firstPlaceMarkAddressParts[firstPlaceMarkAddressParts.length-2];
                                            if(country=="US"){
                                                if((city.length==3)||(city.length==2)){
                                                    city=firstPlaceMarkAddressParts[firstPlaceMarkAddressParts.length-3];
                                                }
                                            }
                                        }
                                        var p=response.Placemark[0].Point.coordinates;
                                        googleMap.setCenter(new GLatLng(p[1],p[0]),12);
                                        loadToGM(country,city);
                                        break;
                                }
                            }else{
                                Ext.MessageBox.alert(__translate("G_GEO_ALERT_TITLE"),__translate("G_GEO_ALERT_MESSAGE_AMBIGUOUS"));
                            }
                        }else{
                            if(gmResponseCode[statusCode]){
                                var _7c=gmResponseCode[statusCode];
                            }
                            Ext.MessageBox.alert(__translate("G_GEO_ALERT_TITLE"),"\""+frmUbicar.dir+"\" "+_7c + __translate("G_GEO_ALERT_MESSAGE_NOT_FOUND") );
                        }
                    }
                });
            }else{
                loadToGM(country,city);
            }
        }

        var _gClientGeoDecoder=new GClientGeocoder();

        function isEmpty(value){
            if(null==value||""==value){
                return true;
            }
            return false;
        };
        function isNumeric(val){
            var n=parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        };

        var nav=new Ext.KeyNav(Ext.getDom("dir"),{
            "enter":SearchByDir,
            "scope":this
        });
        var _nameEstate;
        var _descEstate;
        var _salesEstate;
        var _zipCode;
        var _city;
        var _status;
        var _id;
        var _lat;
        var _lng;
        var _gLatLng;
        var _idPhoto;
        var _featured_photo;
        var _result_photo;
        var googleMarker;
        var markerClusterer = null;

        _toGM.on("load",function(s,r){
            var arr_markers = [];
            googleMap.clearOverlays();
            Ext.each(r,function(rec){
                
                console.log(rec);

                _nameEstate = rec.data.NombrePropiedad;
                if(!_nameEstate) {
                    _nameEstate = "";
                }
                _descEstate='';
                if(!_descEstate) {
                    _descEstate = "";
                }
                _zipCode=rec.data.ZipCode;
                if(!_zipCode) {
                    _zipCode = "";
                }
                _city = rec.data.Ciudad;
                if(!_city) {
                    _city = "";
                }

                _id=rec.data.CodigoPropiedad;
                _idPhoto=rec.data.PROFotoID;
                if(!_idPhoto) {
                    _result_photo = __translate("AD_NO_IMAGE_RESULT");
                }else {
                    _result_photo = "/imagen.php?id="+_idPhoto+"&tipo=3"
                }
                _status = "";
                switch(rec.data.Accion){
                    case "B":
                        _status=__translate("SEARCH_RESULT_FOR_SALE") + " " + rec.data.moneda + "$ " + rec.data.PrecioVenta + " / " +
                                __translate("SEARCH_RESULT_FOR_RENT") + " " + rec.data.moneda + "$ " + rec.data.PrecioAlquiler;
                        break;
                    case "S":
                        _status=__translate("SEARCH_RESULT_FOR_SALE") + " " + rec.data.moneda + "$ " + rec.data.PrecioVenta;
                        break;
                    case "R":
                        _status=__translate("SEARCH_RESULT_FOR_RENT") + " " + rec.data.moneda + "$ " + rec.data.PrecioAlquiler;
                        break;
                    default:
                        _status = "";
                        break;
                }

                _lat=rec.data.Latitud;
                _lng=rec.data.Longitud;
                var _windowInfoContent="<table cellpadding=\"10\" cellspacing=\"5\" border=\"0\" width=\"252px\" class=\"details\">"+
                                        "<tr>"+
                                        "<td width=\"0px\" valign=\"top\">"+
                                        "<div class=\"photo_wrapper\">"+
                                        "<img alt=\"\" src=\""+_result_photo+"\" height=\"75\" width=\"100\" onerror=\"this.src='" + __translate("AD_NO_IMAGE_RESULT") + "'\" />"+
                                        "</div>"+
                                        "</td>"+
                                        "<td width=\"252px\" valign=\"top\" >"+
                                        "<ul>"+
                                        "<li>"+ _status + "</li>" +
                                        "<li>"+ _nameEstate + "</li>" +
                                        "<li>"+ _descEstate + "</li>" +
                                        "<li>"+ _city + "," + _zipCode + "</li>"+
                                        "</ul>"+
                                        "</td>"+
                                        "</tr>"+
                                        "<tr>"+
                                        "<td colspan=\"2\"><a target='_new' href=\""+ __environment('facebook_url') + "enlistar/perfil?valprop="+_id+"\" target=\"_top\">" + __translate("SEARCH_RESULT_GOTO_PROFILE") + "</a></td>"+
                                        "</tr>"+
                                        "</table>";
                _gLatLng= new GLatLng(_lat,_lng);
                if(rec.data.credito>0) {
                    googleMarker=_putGMMarker(_gLatLng,_id,_windowInfoContent,_gmStarIcon);
                } else {
                    googleMarker=_putGMMarker(_gLatLng,_id,_windowInfoContent,_gmIcon);
                }
                googleMarker.credits = rec.data.credito;
                arr_markers.push(googleMarker);
               // googleMap.addOverlay(googleMarker);
            });
            refreshMap(arr_markers);
        });


    }// Fin Si google maps compatible
    else{
        Ext.MessageBox.alert(__translate("G_GEO_ALERT_TITLE"),__translate("G_GEO_ALERT_GOOGLE_NOT_SUPPORTED"));
    }

     var styles = [
      [{
        url: '../img/maps_clust/conv30.png',
        height: 27,
        width: 30,
        anchor: [3, 0],
        textColor: '#FF00FF'
      },
      {
        url: '../img/maps_clust/conv40.png',
        height: 36,
        width: 40,
        opt_anchor: [6, 0],
        opt_textColor: '#FF0000'
      },
      {
        url: '../img/maps_clust/conv50.png',
        width: 50,
        height: 45,
        opt_anchor: [8, 0]
      }]];


    function refreshMap(arrMarkers) {        
        var ZOOM_MAX = 15;
        var GRID_SIZE = 50; //Tama�o en pixeles del cuadrado que contendra los marcadores
        if (markerClusterer != null) {
            markerClusterer.clearMarkers();
			
        }
	    markerClusterer = new MarkerClusterer(googleMap, arrMarkers, {maxZoom: ZOOM_MAX, gridSize: GRID_SIZE, styles: styles[0]});
		var totalClusters = markerClusterer.getTotalClusters();
		var mark = markerClusterer.getTotalMarkers();
		//alert(mark);
		//alert(totalClusters);
		//if(markerClusterer.getTotalMarkers() > 20){
		//markerClusterer.visible=false;	//}
	//end clustermarker 
     }

    _destacadas.on("load",function(s,r){
        if(r.length < 6) {
            var destacadasRecordType = Ext.data.Record.create(
                {name: 'id'},
                {name: 'PROFotoID'},
                {name: 'creditos'},
                {name: 'FKCodigoPropiedad'}
            );

            var destacadasAds = new destacadasRecordType({
                id: '-1',
                PROFotoID: '',
                creditos: 0,
                FKCodigoPropiedad: null
            });
            var style = "";
            while(r.length < 6) {
                r.push(destacadasAds);
            }
        }


        //Construye el carrussell para visualizar imagenes de propiedades destacadas
        var _showDestacadas=new Ext.XTemplate(
                        "<tpl for=\".\">",
                            "<tpl for=\"data\">",
                                "<tpl if=\"this.advertise(id) == false\">",
								"<div id='t_{[xindex]}' class='t' ><a href='#'>test</a></div>",
                                    "<tpl if=\"this.noSrc(PROFotoID) == false\">",
                                        "<div id=\"propiedad_item_{[xindex]}\" class=\"",
                                        "<tpl if=\"this.lastOne(xindex, xcount)==true\" >",
                                        "last_one",
                                        "</tpl>",
                                        "<tpl if=\"this.isMiddleOne(xindex, xcount)==true\" >",
                                        "destacadas_item",
                                        "</tpl>",
                                        "<tpl if=\"this.firstOne(xindex)==true\" >",
                                        "first_one",
                                        "</tpl>",
                                        "\"><a href='#'><img src=\"/imagen.php?id={PROFotoID}&tipo=2\" width=\"120\" height=\"90\" onerror=\"this.src='" + __translate("AD_NO_IMAGE") +"'\" /></a><a id='propiedad_item_{[xindex]}' class=\"propiedad_item_{[xindex]}\" style=\"background-image: url('"+__translate("FEATURE_IMAGE_BACKGROUND")+"');\" href=\"#\"></a></div>",
                                        //+ __environment('facebook_url') + "enlistar/perfil?valprop={id}\" target=\"_top\"><img src=\"/imagen.php?id={PROFotoID}&tipo=2\" width=\"120\" height=\"90\" onerror=\"this.src='" + __translate("AD_NO_IMAGE") +"'\" /></a><a class=\"fpOver\" style=\"background-image: url('"+__translate("FEATURE_IMAGE_BACKGROUND")+"');\" href=\""+ __environment('facebook_url') + "enlistar/perfil?valprop={id}\" target=\"_top\"></a></div>",
                                    "</tpl>",
                                    "<tpl if=\"this.noSrc(PROFotoID) == true\">",
                                        "<div id=\"{id}\" class=\"",
                                        "<tpl if=\"this.lastOne(xindex, xcount)==true\" >",
                                        "last_one",
                                        "</tpl>",
                                        "<tpl if=\"this.isMiddleOne(xindex, xcount)==true\" >",
                                        "destacadas_item",
                                        "</tpl>",
                                        "<tpl if=\"this.firstOne(xindex)==true\" >",
                                        "first_one",
                                        "</tpl>",
                                         "\"><a href=\"#\"target=\"_top\"><img src=\"" + __translate("AD_NO_IMAGE") +"\" width=\"120\" height=\"90\"/></a><a class=\"fpOver\" style=\"background-image: url('"+__translate("FEATURE_IMAGE_BACKGROUND")+"');\" href=\"#\" target=\"_top\"></a></div>",
                                 //"+ __environment('facebook_url') + "enlistar/perfil?valprop={id}\" target=\"_top\"><img src=\"" + __translate("AD_NO_IMAGE") +"\" width=\"120\" height=\"90\"/></a><a class=\"fpOver\" style=\"background-image: url('"+__translate("FEATURE_IMAGE_BACKGROUND")+"');\" href=\""+ __environment('facebook_url') + "enlistar/perfil?valprop={id}\" target=\"_top\"></a></div>",
                                    "</tpl>",
                                "</tpl>",
                                "<tpl if=\"this.advertise(id) == true\">",
                                    "<div id=\"{id}\" class=\"",
                                    "<tpl if=\"this.lastOne(xindex, xcount)==true\" >",
                                    "last_one",
                                    "</tpl>",
                                    "<tpl if=\"this.isMiddleOne(xindex, xcount)==true\" >",
                                    "destacadas_item",
                                    "</tpl>",
                                    "<tpl if=\"this.firstOne(xindex)==true\" >",
                                    "first_one",
                                    "</tpl>",
                                    "\"><a href=\"#\" target=\"_top\"><img src=\"" + __translate("AD_NO_IMAGE_OUTSTANDING") +"\" width=\"120\" height=\"90\"/></a><a class=\"fpOver\" style=\"background-image: url('"+__translate("FEATURE_IMAGE_BACKGROUND")+"');\" href=\"#\" target=\"_top\"></a></div>",
                            //"+ __environment('facebook_url') + "index/advertise\" target=\"_top\"><img src=\"" + __translate("AD_NO_IMAGE_OUTSTANDING") +"\" width=\"120\" height=\"90\"/></a><a class=\"fpOver\" style=\"background-image: url('"+__translate("FEATURE_IMAGE_BACKGROUND")+"');\" href=\""+ __environment('facebook_url') + "enlistar/perfil?valprop={id}\" target=\"_top\"></a></div>",
                                "</tpl>",
                            "</tpl>",
                        "</tpl>",{
            compiled:true,
            noSrc:function(src){
                return src==null;
            },
            advertise:function(id) {
                return id==-1;
            },
            firstOne:function(index) {
                return index == 1;
            } ,
            lastOne:function(index, count) {
                return index >= count;
            } ,
            isMiddleOne:function(index, count){
                return index > 1 && index < count;
            }
        });
        _showDestacadas.overwrite("destacadas",r);
    });

    _brokers.on("load", function(s,r){
        var _obj_coordinates = GetGMCoordinates();
        var _showBrokers = new Ext.XTemplate(
            "<input type=\"hidden\" name=\"maxX\" value=\""+_obj_coordinates.maxX+"\" />",
            "<input type=\"hidden\" name=\"maxY\" value=\""+_obj_coordinates.maxY+"\" />",
            "<input type=\"hidden\" name=\"minX\" value=\""+_obj_coordinates.minX+"\" />",
            "<input type=\"hidden\" name=\"minY\" value=\""+_obj_coordinates.minY+"\" />",
            "<tpl for=\".\">",
              "<tpl for=\"data\">",
                  "<tpl if=\"this.noLimit(xindex) == true\">",
                    "<div>",
                       "<tpl if=\"this.noSrc(pic_square) == false\">",
                         "<a href=\""+ __environment('facebook_url') + "enlistar/propiedadesautor?id={Uid}\" target=\"_top\"><img src=\"{pic_square}\" alt=\"{NombreCliente}\" onerror=\"this.src='/img/q_silhouette.gif'\" /><span>{NombreCliente}</span></a>",
                       "</tpl>",
                       "<tpl if=\"this.noSrc(pic_square) == true\">",
                          "<a href=\""+ __environment('facebook_url') + "enlistar/propiedadesautor?id={Uid}\" target=\"_top\"><img src=\"/img/q_silhouette.gif\" alt=\"{NombreCliente}\" width=\"120\" height=\"90\"/>{NombreCliente}</a>",
                       "</tpl>",
                    "</div>",
                  "</tpl>",
                "</tpl>",
            "</tpl>",{
            compiled:true,
            noSrc:function(src){
                return src === "";
            },noLimit:function(index){
                    return 9 > index;
            }
                });
        if(_brokers.getTotalCount() > 0) {
            $("#see_all_brokers").show();
            _showBrokers.overwrite("allBrokers",r);
            $("#see_all_count").html(_brokers.getTotalCount());
            $("#allBrokers").show();
        } else {
            $("#see_all_brokers").hide();
            $("#allBrokers").hide();
        }
    });

    _filtradas.on("load",function(s,r){
        var itemsPerPage = 5;// offset
        var linksToDraw = 4; // Total of links to print in pagination

         function Total (totalRecords, itemsPerPage){
            var totalPages = Math.ceil(totalRecords/itemsPerPage);
            return totalPages;
         }

         /**
         * Calcula el intervalo de enlaces a dibujar a partir de la pagina seleccionada
         * el numero de enlaces y su cota superior.
         * @return {Array}
         */
        function getInterval(num_display_entries, numPages, selected_page)  {
            var upper_limit = numPages - num_display_entries + 1;
            upper_limit = (upper_limit > 1)? upper_limit : 1;

            var start = selected_page >= upper_limit? upper_limit :  selected_page;
            var end = selected_page < upper_limit? selected_page + num_display_entries -1 : numPages ;

            return [start,end];
        }

         function DrawConsecutivesLinks(beginingLink, totalPages, selectedPage, curPage, linksToDraw){
            var legend = "";
            var classCurrentPagination = "paginacion_cont_li current";
            var classPagination = "paginacion_cont_li";
            var interval = getInterval(linksToDraw, totalPages, selectedPage);

            for(var i=interval[0]; i<=interval[1]; i++){
                var classLink = (selectedPage == i)? classCurrentPagination : classPagination;
                legend += "<li class=\""+classLink+"\"><a class=\"paginacion_cont_a\" href=\"#\">"+i+"</a></li>";
            }

            return legend;
         }

         function lastAction(beginingLink, totalPages, selectedPage,firstLink, prevLink, curPage, linksToDraw){
             var links  = firstLink;
                 links = links + prevLink;
                 links = links + DrawConsecutivesLinks(beginingLink, totalPages, selectedPage, curPage, linksToDraw);
                 links = links + "<li class=\"paginacion_cont_li\"><a href=\"#\"><img src='/img/page_next_gray.gif'/></a></li>";
                 links = links + "<li class=\"paginacion_cont_li\"><a href=\"#\"><img src='/img/page_last_gray.gif'/></a></li>";
             return links;
         }

         function firstAction(beginingLink, totalPages, selectedPage, nextLink, lastLink, curPage, linksToDraw){
             var links = "";
             if(totalPages > 1){
                links = "<li class=\"paginacion_cont_li\"><a href=\"#\"><img src='/img/page_first_gray.gif'/></a></li>";
                links = links + "<li class=\"paginacion_cont_li\"><a href=\"#\"><img src='/img/page_previous_gray.gif'/></a></li>";
                links = links + DrawConsecutivesLinks(beginingLink, totalPages, selectedPage, curPage, linksToDraw);
                links = links + nextLink;
                links = links + lastLink;
             }else {
                 links = DrawConsecutivesLinks(beginingLink, totalPages, selectedPage, curPage, linksToDraw);
             }
             return links;
         }

         function printAllLinks(beginingLink, totalPages, selectedPage, firstLink, nextLink, prevLink, lastLink, curPage, linksToDraw){
              var links = firstLink;
              links = links + prevLink;
              links = links + DrawConsecutivesLinks(beginingLink, totalPages, selectedPage, curPage, linksToDraw);
              links = links + nextLink;
              links = links + lastLink;
              return links;
         }

         function printPagination(currentPage, selectedPage, totalPages, totalLinks){
            var beginingLink = 1;
            var links = "<ul class=\"paginacion_cont\">";
            var prevLink = "<li class=\"paginacion_cont_li\" name=\"plpp\" id=\"plpp\"><a class=\"paginacion_img\" href=\"#\"><img id=\"previous\" src='/img/page_previous_blue.gif'/></a></li>";
            var nextLink = "<li class=\"paginacion_cont_li\" name=\"nlpp\" id=\"nlpp\"><a class=\"paginacion_img\" href=\"#\"><img id=\"next\" src='/img/page_next_blue.gif'/></a></li>";
            var firstLink = "<li class=\"paginacion_cont_li\" name=\"flpp\" id=\"flpp\"><a class=\"paginacion_img\" href=\"#\"><img id=\"first\" src='/img/page_first_blue.gif'/></a></li>";
            var lastLink = "<li class=\"paginacion_cont_li\" name=\"llpp\" id=\"llpp\"><a class=\"paginacion_img\" href=\"#\"><img id=\"last\" src='/img/page_last_blue.gif'/></a></li>";

            switch (selectedPage) {
                 case 1:
                      //implementar last
                      links += firstAction(beginingLink, totalPages, selectedPage, nextLink, lastLink, currentPage, totalLinks);
                    break;
                 case totalPages:
                     //implementar first
                     links += lastAction(beginingLink, totalPages, selectedPage, firstLink, prevLink, currentPage, totalLinks);
                    break;
                default:
                    //imprimir todo
                    links += printAllLinks(beginingLink, totalPages, selectedPage, firstLink, nextLink, prevLink, lastLink, currentPage, totalLinks);

                    break;
            }

            return links + "</ul>";
        }

        function getInfoResults(selectedPage, offset, resulsetNumber, totalRecords){
            var firstOcurrence = (selectedPage - 1) * offset + 1;
            var lastOcurrence = firstOcurrence + resulsetNumber -1;

            return firstOcurrence + " " + __translate("SEARCH_RESULT_TO") + " " + lastOcurrence + " " + __translate("SEARCH_RESULT_OF") + " " + totalRecords + " " + __translate("SEARCH_RESULT_PROPERTY");
        }

        var page = s.reader.jsonData.cPage; //Page in use
        var selPage = s.reader.jsonData.sPage; //selected page
        var totalRecords = s.reader.jsonData.total; //total de registros
        totalPages = Total(totalRecords, itemsPerPage);

        var paginacion = printPagination(page, selPage, totalPages, linksToDraw);
        var infoPagination = getInfoResults(selPage, itemsPerPage, r.length, totalRecords);
        
        var _galery=new Ext.XTemplate(
            "<div id=\"paginacion_superior\">",
                paginacion,
            "</div>",
            "<div id=\"listado_items\">",
                "<tpl for=\".\">",
                    "<tpl for=\"data\">",
                        "<div id=\"propiedad_item_{[xindex]}\" class=\"propiedad_item\">",
                            "<div style=\"height: 55px;\">",
                                "<div class=\"imagen_propiedad\">",
                                    "<tpl if=\"this.noSrc(PROFotoID) == true\">",
                                        "<img src=\"" + __translate("AD_NO_IMAGE_RESULT") +"\" width=\"75\" height=\"56\" id=\"{id}\" />",
                                    "</tpl>",
                                    "<tpl if=\"this.noSrc(PROFotoID) == false\">",
                                        "<img src=\"/imagen.php?id={PROFotoID}&tipo=3\" width=\"75\" height=\"56\" id=\"{id}\" onerror=\"this.src='" + __translate("AD_NO_IMAGE_RESULT") +"'\"/>",
                                    "</tpl>",
                                 "</div>",
                                "<div class=\"detalle_propiedad\">",
                                    "<a href=\"#\"><b id=\"{id}\">{nombre}</b></a><br/>",
                                    "{sale} {rent} {bothPrice} {foreclosure}",
                                "</div>",
                             "</div>",
                             "<div class=\"clear\"></div>",
                        "</div>",
                    "</tpl>",
                "</tpl>",
         "</div>",
         "<div id=\"paginacion_inferior\">",
                paginacion,
         "</div>",
         "<div id=\"listado_footer\">",
         __translate("SEARCH_RESULT_LABEL") + " "+ infoPagination,
         "</div>",
            {
            compiled:true,
            noSrc:function(src){
                return src==null;
            }
        });
        _galery.overwrite("galery",r);

        if(totalRecords == 0) {
                $("#paginacion_superior, #paginacion_inferior, #listado_footer").hide();
        } else {
                $("#paginacion_superior, #paginacion_inferior, #listado_footer").show();
        }

        function LoadPropertiesGrid(curPage, selectedPage, start, offset){
            var _coordinates = GetGMCoordinates();
            var frmFiltro = GetObjectFrmFilter();
             _filtradas.load({
                params:{
                    "country":country,
                    "city":city,
                    "category":frmFiltro.category,
                    "type":frmFiltro.type,
                    "min_price":frmFiltro.min_price,//frmFiltro no esta definido
                    "max_price":frmFiltro.max_price,
                    "max_area":frmFiltro.max_area,
                    "min_area":frmFiltro.min_area,
                    "uni_area":frmFiltro.area_uni,
                    "max_area_lote":frmFiltro.max_area_lote,
                    "min_area_lote":frmFiltro.min_area_lote,
                    "uni_area_lote":frmFiltro.area_lote_uni,
                    "parameters": Ext.encode(frmFiltro.parameters),
                    "coordinates":Ext.encode(_coordinates),
                    "sPage" : selectedPage,
                    "cPage" : curPage,
                    "start" : start,
                    "limit" : offset,
                    "total" : totalRecords
                }
            });
        }
        //Pagination Link Action
        $("a.paginacion_cont_a").click(function(){
            var selectedPage = $(this).text();
            var curPage = $("div#paginacion_superior > ul > li.current").text() || 1;
            if(curPage != selectedPage){
                var start = itemsPerPage * (selectedPage - 1);
                LoadPropertiesGrid(curPage, selectedPage, start, itemsPerPage);
            }
        });
        //Last Action
        $("li#llpp > a").click(function(){
            var selectedPage = totalPages;
            var curPage = $("div#paginacion_superior > ul > li.current").text() || 1;
            if(curPage != selectedPage){
                var start = itemsPerPage * (selectedPage - 1);
                LoadPropertiesGrid(curPage, selectedPage, start, itemsPerPage);
            }
        });
        //First Action
        $("li#flpp > a").click(function(){
            var selectedPage = 1;
            var curPage = $("div#paginacion_superior > ul > li.current").text() || 1;
            if(curPage != selectedPage){
                var start = itemsPerPage * (selectedPage - 1);
                LoadPropertiesGrid(curPage, selectedPage, start, itemsPerPage);
            }
        });
        //Next Action
        $("li#nlpp > a").click(function(){
            var curPage = $("div#paginacion_superior > ul > li.current").text() || 0;
            curPage = parseInt(curPage);
            var selectedPage = curPage + 1;
            if(curPage != selectedPage){
                var start = itemsPerPage * (selectedPage - 1);
                LoadPropertiesGrid(curPage, selectedPage, start, itemsPerPage);
            }
        });
        //Previous Action
        $("li#plpp > a").click(function(){
            var curPage = $("div#paginacion_superior > ul > li.current").text() || 1;
            curPage = parseInt(curPage);
            var selectedPage = curPage -1;
            if(curPage != selectedPage){
                var start = itemsPerPage * (selectedPage - 1);
                LoadPropertiesGrid(curPage, selectedPage, start, itemsPerPage);
            }
        });

        var property = $("div#propiedad_item_1");
		var pro=$("div#dest_1");
		
        function ClickProperty(){
                    alert("Hago algo");
                };

        var clickProperty = ClickProperty;

       // GEvent.addDomListener(property, "click", clickProperty);


        $("div.propiedad_item").click(function(){
											   alert("test");
          /*  var z=$("b",this).attr("id");
            if(z==null||z==""){
            }else{
                googleMap.openInfoWindowHtml(new GLatLng(_gMarkerList[z].Ca.y, _gMarkerList[z].Ca.x ),_gMarkerContentList[z]);
                //_gMarkerList[z].openInfoWindowHtml(_gMarkerContentList[z]);
            }*/
        });
		
		
		 $("div.t").click(function(){
            alert("H");
			//var z=$("b",this).attr("id");
            //if(z==null||z==""){
           // }else{
               // googleMap.openInfoWindowHtml(new GLatLng(_gMarkerList[z].Ca.y, _gMarkerList[z].Ca.x ),_gMarkerContentList[z]);
                //_gMarkerList[z].openInfoWindowHtml(_gMarkerContentList[z]);
           // }
        });
		

	

        $("div.destacadas_item", "div.last_one", "div.first_one").click(function(){
            var z = this.id;
            if(z==null||z==""){
            }else{
                _gMarkerList[z].openInfoWindowHtml(_gMarkerContentList[z]);
            }
        });


         $("a.paginacion_img  > img").hover(function(){
            var id = this.id;
            this.src = "/img/page_"+id+"_green.gif";
        }, function(){
            var id = this.id;
            this.src = "/img/page_"+id+"_blue.gif";
        }
        );

        // Procesar si esta cacheada.
        var cachedState = Ext.state.Manager.get("cached");
        if(cachedState) {
            Ext.state.Manager.set("cached", false);
            SearchByDir();
        }
    });

   

    var allBroker = $("#link_all_brokers");
    if(allBroker) {
        allBroker.click(function(){
            document.getElementById("frmAllBrokers").submit();
            return false;
        });
    }

    //On Click Event
	$("ul.tabs_search li").click(function() {
		$("ul.tabs_search li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
                apply.click();
		return false;
	});

    var body =  Ext.getBody();
    body.addListener("unload", function() {
        var frmU = GetObjectFrmUbicar();
        var frmF = GetObjectFrmFilter();
        Ext.state.Manager.set("cached", true);
        Ext.state.Manager.set("ub", Ext.encode(frmU));
        Ext.state.Manager.set("fi", Ext.encode(frmF));
    });
}); // Fin Ext.onReady