<?php

/*
 * Actualizacion a la clase facebook 3.0.1
 * Mejoras en rendimiento del php y redireccion de las paginas
 * Ambiente de desarrollo bajo lighttpd
 * Para la instalacion: 
 * -app.log tiene como propietario a apache
 * -Se debe activar las directivas de reescritura en el .htaccess
 * -Se requiere de autenticacion oAuth en el panel de la aplicacion en facebook
 * -Para optimiazar se comprime con mod_deflate
 */

// Define path to application directory
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../housemarket/application'));
/* Si no esta definida esta variable de entorno en los archivos de configuracion del servidor web, no se cargara la configuracion de desarrollo
 * y se cargará por defecto la configuracion de produccion, generando un redireccionamiento a hmapp.com
 */
// Define application environment. 
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

//Mostramos los errores en desarrollo
if (APPLICATION_ENV == "development") {
    error_reporting(E_ALL);
}else{
	error_reporting(0);
}

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
            realpath(APPLICATION_PATH . '/../housemarket/library/'),
            realpath(APPLICATION_PATH . '/models/'),
            realpath(APPLICATION_PATH . '/views/helpers/'),
			      realpath(APPLICATION_PATH . '/controllers/'),
            get_include_path(),
        )));

/** Zend_Application * */
require_once 'Zend/Loader/Autoloader.php';
require_once 'Zend/Config/Ini.php';
require_once 'Zend/Session/Namespace.php';
require_once 'Zend/Session.php';
require_once 'Dgt/Fb.php';

/*
 * Clases para el manejo de arreglos
 * Clase de facebook 3.0.1 para manejo de la informacion de usuarios facebook
 */

require_once 'Hm/Table.class.php';
require_once 'Hm/Cache.php';
require_once 'facebook/base_facebook.php';
require_once 'facebook/facebook.php';
require_once 'functions/function.php';
require_once 'functions/common.php';
//Cargando el archivo de configuracion

$cfg = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
	
//@todo try catch for the session
Zend_Session::start();

$UserSession = new Zend_Session_Namespace('HmUserSession');
$AppSession = new Zend_Session_Namespace('HmAppSession');

$UserSession->setExpirationSeconds(60);
$AppSession->setExpirationSeconds(60);

// Create our Application instance
//if(!isset($AppSession->appfb)){
	$appfb = new Facebook(array('appId' => $cfg->facebook_appid, 'secret' => $cfg->facebook_secret, 'cookie' => true));
	$AppSession->appfb = $appfb;
/*}else{
	$appfb = $AppSession->appfb;
}*/

// Get User ID

// Create our Application instance
//if(!isset($UserSession->uid)){
	$UserUid = $appfb->getUser();
	$UserSession->uid = $UserUid;
/*}else{
	$UserUid = $UserSession->uid;
}*/

// Get User Signed
$signed = $appfb->getSignedRequest();

if($UserUid == 0 && isset($signed['user']) && isset($signed['user']['user_id'])){
	$UserUid = $signed['user']['user_id'];
}
// Create application
require_once 'Zend/Application.php';

$application = new Zend_Application(
		APPLICATION_ENV,
		APPLICATION_PATH . '/configs/application.ini'
);

Zend_Registry::set('fb', $appfb);
Zend_Registry::set('signed', $signed);
Zend_Registry::set('uid', $UserUid);

// application bootstrap
try{
	$application->bootstrap();
}catch(Exception $e){
	
}

if(!isset($AppSession->info)){
	$AppSession->info = $appfb->api("/" . $cfg->facebook_appid);
}
Zend_Registry::set('app', $AppSession->info);

// get info user
if ($UserUid) {
	$cli = new Hm_Cli_Cliente();
	if($cli->NuevoCliente($UserUid)){
		$user_info['is_new'] = true;
	}
	if(!isset($UserSession->info)){
		$fbDgt = Dgt_Fb::getInstance();
		$user_info = $fbDgt->getUserInformation();
		//$user_data = $fbDgt->getDataUser($uid);
		$db = Zend_Registry::get('db');
		// recuperaci�n de informaci�n si el user tiene el derecho de broker (see all)
		$NbPropriedad = $db->fetchAll("SELECT count(*) as nbpropriedad FROM propropiedad p INNER JOIN cliente c on p.Uid = c.Uid WHERE c.Uid =" . $UserUid );
		$user_info['nbPropriedad'] = $NbPropriedad[0]->nbpropriedad;
		$user_info['type'] = 'comun';
		//$user_info['extra_data'] = $user_data;
		if($user_info['nbPropriedad'] > 0){
			$user_info['type'] = 'broker';
		}
		$UserSession->info = $user_info;
	}
	Zend_Registry::set('user_info', $UserSession->info);
}

if(Zend_Registry::isRegistered('uid') && Zend_Registry::isRegistered('user_info')){
	$cache = new HmCache();
	$cache->start();	
}

// redireccion para pagina de housemarket app page id == 112757475509748 es el tab de housemarket
if(isset($signed['page']) && isset($signed['page']['id']) && $signed['page']['id'] == '112757475509748' ){
	echo "<script>top.location.href='https://apps.facebook.com/housemarket/';</script>";
	exit;
}

// the anonymous user can use the tab
if (!$UserUid) {
	$user_info['type'] = 'none';
	$UserSession->info = $user_info;
	
    if (isset($signed['page']['id'])) {
    	
        Zend_Registry::set('fbparams', 'x=tabs');
         
        $application->run();
        exit();
    }
}

if ($UserUid ) {

    try {

        /* Esto es necesario si aun no tenemos configurado la autenticacion oAuth desde el panel de configuracion de facebook
         * Si tenemos sesion valida, y existe el parametro code, redirigir a la aplicacion de facebook. */
        if (isset($_GET['code'])) {
            header("Location: " . $cfg->fb_app_url);
            $path = parse_url($_SERVER['REQUEST_URI']);
            unset($_GET['code']);
            unset($_GET['state']);
            header("Location: " . $cfg->fb_app_url . ltrim($path['path'], "/") . "?" . http_build_query($_GET));
            exit;
        }

        
        /* A partir de este punto la sesion de facebook existe y esta validada para la app, verificada en:
         * - Aplicacion desde facebook
         * - Aplicacion desde el dominio base
         * - Aplicacion desde fanpages
         */
        // Extraemos la informacion del usuario

        $fbDgt = Dgt_Fb::getInstance();

        /** Zend_Application */
        
        
        
        Zend_Registry::set('fbparams', 'x=standalone');
		
        if(!isset($UserSession->granted_permissions)){
        	$granted = $fbDgt->getUserGrantedPermissions($UserUid, array('manage_pages', 'publish_stream'));
        	$UserSession->granted_permissions = reset($granted);
        }
        Zend_Registry::set('user_granted_permissions', $UserSession->granted_permissions);
        
        // manage_pages permision now requires
        if(!isset($UserSession->pages)){
        	$UserSession->pages = $fbDgt->getPagesUser($UserUid);
        }
        
        Zend_Registry::set('pages', $UserSession->pages);
        
        if (isset($_GET['installed']) && isset($_GET['fb_page_id']) && $_GET['installed'] == 1) {
            $pageid = $_GET['fb_page_id'];
            $namePage = $fbDgt->execFql("select name from page where page_id= " . $pageid);
            $prev = sprintf("https://www.facebook.com/pages/%s/%s?sk=app_%s", $namePage[0]["name"], $pageid, $cfg->facebook_appid);
            echo "<script>top.location.href=\"" . $prev . "\";</script>";
            exit();
        }

        if (isset($_GET['faninstalled']) && $_GET['faninstalled'] == 1) {
            if (isset($_GET['xprom_r'])) {
                $db = Zend_Db::factory('Pdo_Mysql', array(
                            'host' => 'localhost',
                            'username' => 'hmapp',
                            'password' => 'H1M2A3p4p_',
                            'dbname' => 'hmapp'
                        ));

                //Test de conexion con la base de datos
                $db->getConnection();
                $xprom_r = base64_decode($_REQUEST['xprom_r']);
                $part_r = explode("/", $xprom_r);
                $email_r = $part_r[0];
                $page_r = $part_r[1];
                $sql_r = "SELECT * FROM prompage WHERE Uid =" . $UserUid;
                $register = $db->fetchAll($sql_r);
                if (!$register) {
                    $data = array(
                        'CodigoReference' => null,
                        'Uid' => $UserUid,
                        'PageReference' => $page_r,
                        'EmailReference' => $email_r,
                        'FechaRegistro' => new Zend_Db_Expr('CURDATE()'),
                    );
                    try {
                        $db->insert('prompage', $data);
                    } catch (Exception $e) {
                        echo "Error message: " . $e->getMessage() . "\n";
                    }
                } else {
                    
                }
            }
            
            if($pages->obtenerCantidad()>=0){
            	$redirectUri = 'https://tab.hmapp.com/register';
            	$scope = array(
            			'manage_pages',
            			'user_about_me',
            			'publish_actions',
            			'email',
            			'user_birthday',
            	);
            	$scope_string = implode(',',$scope);
            	
            	$installurl = 'https://www.facebook.com/dialog/oauth/';
            	$installurl .= '?client_id=151516144978488';
            	$installurl .= '&redirect_uri='. urlencode($redirectUri);
            	$installurl .= '&scope='. $scope_string;
            	echo "<script>top.location.href=\"" . $installurl . "\";</script>";
            	exit();
            }
        }
        $application->run();
        exit();
    } catch (FacebookApiException $e) {
        error_log($e->getMessage());
        $user = null;
        exit();
    }
}


Zend_Session::destroy();
//Si la sesion no existe, o no tenemos un codigo oAuth entonces obligamos el inicio de sesion y la generacion de codigos
//Si no estamos sobre el servedor Facebook redirigimos por haya
if(isset($_REQUEST['state']) && isset($_REQUEST['code'])){
	echo "<script>top.location.href='" . $cfg->fb_app_url . "';</script>";
}else{
	$loginUrl = $appfb->getLoginUrl(array('scope' => ''));
	echo "<script>top.location.href='" . $loginUrl . "';</script>";
}
exit();

?>