<?php
define("app_PATH", realpath(dirname(__FILE__))."/../../housemarket_admin/app");

defined('APPLICATION_ENVIRONMENT') || define('APPLICATION_ENVIRONMENT', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

$paths = array (
    app_PATH . '/../lib',
    app_PATH .'/models',
    get_include_path());


set_include_path(implode(PATH_SEPARATOR, $paths));

require_once "Zend/Loader/Autoloader.php";
require_once 'functions/common.php';
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace('Dgt_');


try {
    require app_PATH .'/bootstrap.php';
} catch (Exception $exception) {
    echo '<html><body><center>'
        . 'An exception occured while bootstrapping the app.';
    echo '<br /><br />' . $exception->getMessage() . '<br />'
        . '<div align="left">Stack Trace:'
        . '<pre>' . $exception->getTraceAsString() . '</pre></div>';

    if (defined('app_ENVIRONMENT') && app_ENVIRONMENT != 'production') {
        echo '<br /><br />' . $exception->getMessage() . '<br />'
            . '<div align="left">Stack Trace:'
            . '<pre>' . $exception->getTraceAsString() . '</pre></div>';
    }
    echo '</center></body></html>';
    exit(1);
}

Zend_Controller_Front::getInstance()->dispatch();
