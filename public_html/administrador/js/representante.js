/*
 * Grid para el listado de aprobaci�n de cr�ditos del cliente
 *
 */
Ext.onReady(function(){
	
    Ext.BLANK_IMAGE_URL = '/administrador/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = "side";

    Ext.apply(Ext.form.VTypes, {
        daterange : function(val, field) {
            var date = field.parseDate(val);

            if(!date){
                return;
            }
            if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                var start = Ext.getCmp(field.startDateField);
                start.setMaxValue(date);
                start.validate();
                this.dateRangeMax = date;
            }
            else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                var end = Ext.getCmp(field.endDateField);
                end.setMinValue(date);
                end.validate();
                this.dateRangeMin = date;
            }
            /*
         * Always return true since we're only using this vtype to set the
         * min/max allowed values (these are tested for after the vtype test)
         */
            return true;
        },
        numbersonly: function(val, field) {
            var n = parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        },
        numbersonlyText: 'Ingresa solo numeros aqui'
    });

    function isEmpty(inputStr) {
        if (null == inputStr || "" == inputStr) {
            return true;
        }
        return false;
    }

    Ext.namespace('Ext.userdata');
    Ext.userdata.status = [['0', 'inactiva'], ['1', 'activa']];
    var stStore = new Ext.data.SimpleStore({
        fields: ['status', 'label'],
        data: Ext.userdata.status
    });

    function agregar () {
            window.top.location="/administrador/index.php/representante/representante";
    }

    function actualizar () {
        var sm = dtgridCliente.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            window.top.location="/administrador/index.php/representante/edit?id="+sel.data.CodigoRepresenta;
        } else {
            Ext.Msg.alert('Alerta', 'Seleccione el representante que desea editar');
        }
    }

    function eliminar () {
        var sm = dtgridCliente.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            if(confirm('Seguro que desea eliminar el registro')){
                Ext.Ajax.request({
                    url: '/administrador/index.php/representante/eliminarepresentante',
                    params: {
                        'CodigoRepresenta': sel.data.CodigoRepresenta
                    },
                    success: function (result, response) {
                        clienteDataStore.reload();
                    },
                    failure: function (result, response) {
                        Ext.Msg.alert('Alerta','Error al intentar eliminar el registro');
                    }
                });
            }
        } else {
            Ext.Msg.alert('Alerta', 'Seleccione el representante que desea eliminar');
        }
    }

    

    var cretoolbar = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Agregar Representante',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/add.png',
            handler: agregar
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'tbbutton',
            text: 'Editar Representante',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/saved.png',
            handler: actualizar
        },
        {
            xtype: 'tbseparator'
        },{
            xtype: 'tbbutton',
            text: 'Eliminar Representante',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/erase.png',
            handler: eliminar
        },
        {
            xtype: 'tbseparator'
        }
        ]
    });

    var clienteDataStore = new Ext.data.Store({
        url: '/administrador/index.php/representante/getrepresentantes',
        reader: new Ext.data.JsonReader({
            root: 'rows',
            totalProperty: 'results',
            id: 'CodigoRepresenta'
        }, [
        {
            name: 'CodigoRepresenta',
            type: 'int'
        },
        {
            name: 'Representante',
            type: 'string'
        },
        {
            name: 'Empresa',
            type: 'string'
        },
        {
            name: 'Direccion',
            type: 'string'
        },
        {
            name: 'Telefono',
            type: 'string'
        },
        {
            name: 'EMail',
            type: 'string'
        }
        
        ])
        ,
        remoteSort:true
        ,
        sortInfo: {
            field: 'CodigoRepresenta',
            direction: 'DESC'
        }
    });

   var crepagingBar = new Ext.PagingToolbar({
        pageSize:30,
        store: clienteDataStore,
        displayInfo: true,
        emptyMsg: "Sin Registros a desplegar",
        displayMsg: 'Registros {0} - {1} de {2}'

    });

   var clienteCM = [
    {
        header: 'ID',
        dataIndex: 'CodigoRepresenta',
        sortable: true
    },
     {
        header: 'Representante',
        dataIndex: 'Representante',
        sortable: true
    },
     {
        header: 'Empresa',
        dataIndex: 'Empresa',
        sortable: true
    },
    {
        header: 'Direcci�n',
        dataIndex: 'Direccion',
        sortable: true
    },
    {
        header: 'Tel�fono',
        dataIndex: 'Telefono',
        sortable: true
    },
    {
        header: 'e-mail',
        dataIndex: 'EMail',
        sortable: true
    }
    ];

    var dtgridCliente = new Ext.grid.GridPanel({
        store: clienteDataStore,
        columns: clienteCM,
        frame: true,
        loadMask: {
            msg: 'Cargando datos, por favor espere ....'
        },
        viewConfig: {
            forceFit:true,
            emptyText: 'No hay resultados.'
        },
        renderTo: 'creclientes_grid',
        stripeRows: true,
        height: 400,
        selModel: new Ext.grid.RowSelectionModel({
            singleSelect: true
        }),
        autoWidth: true,
        collapsed: false,
        title: 'Listado de Representantes',
        tbar: cretoolbar,
        bbar: crepagingBar/*,
        iconCls: 'creditos_panel'*/
    });

    clienteDataStore.load({
        params:{
            start:0,
            limit:30
        }
    });

});