Ext.onReady(function(){
    // @todo cambiar url en produccion
    Ext.BLANK_IMAGE_URL = '/administrador/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    //Ext.form.Field.prototype.msgTarget = 'side';

    var usersDataStore = new Ext.data.Store({
        url: '/administrador/index.php/useradmin/getusers',
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            root: 'rows',
            totalProperty: 'results',
            id: 'idusuario'
        },[{
            name: 'idusuario'
        },

        {
            name: 'usuario'
        }])
    });
    //usersDataStore.load();

    var reportDataStore = new Ext.data.SimpleStore({
        fields: ['id', 'reportName'],
        data: [[1, 'Consultas realizadas'],
        [2, 'Numeros dentro de la Promocion']]
    });
    
    var tabpanel = new Ext.TabPanel({
        renderTo: 'reportpanel',
        minTabWidth: 115,
        //tabWidth:135,
        enableTabScroll:true,
        width:'auto',
        //height:450
        autoHeight: true,
        frame: true,
        iconCls: 'rIcon'
    });
    
    tabpanel.hide();

    Ext.apply(Ext.form.VTypes, {
        daterange : function(val, field) {
            var date = field.parseDate(val);

            if(!date){
                return;
            }
            if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                var start = Ext.getCmp(field.startDateField);
                start.setMaxValue(date);
                start.validate();
                this.dateRangeMax = date;
            }
            else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                var end = Ext.getCmp(field.endDateField);
                end.setMinValue(date);
                end.validate();
                this.dateRangeMin = date;
            }
            /*
         * Always return true since we're only using this vtype to set the
         * min/max allowed values (these are tested for after the vtype test)
         */
            return true;
        }
    });

    var searchF =   new Ext.form.FormPanel({
        title: 'Reportes',
        url: '/administrador/index.php/reportes/doreport',
        hideCollapseTool: false,
        titleCollapse: true,
        collapsible: true,
        collapsed: false,
        iconCls: 'rIcon',
        frame: true,
        bodyStyle:"padding:5px 5px;",
        buttonAlign: "left",
        standarSubmit: true,
        labelWidth: 60,
        labelAlign: 'top',
        width: 650,
        renderTo: 'report_select',
        items: [{
            border: false,
            layout: 'column',
            items: [{
                columnWidth:.5,
                layout: 'form',
                items: [{
                    xtype: 'combo',
                    fieldLabel: 'Reporte',
                    store: reportDataStore,
                    emptyText: 'Seleccione un reporte a generar',
                    displayField: 'reportName',
                    valueField: 'id',
                    name: 'report',
                    hiddenName: 'reportid',
                    forceSelection: true,
                    typeAhead: true,
                    mode: 'local',
                    triggerAction: 'all',
                    //listWidth: 200,
                    anchor:'95%',
                    allowBlank:false,
                    width: 140
                },
                {
                    xtype: 'datefield',
                    fieldLabel: 'Desde',
                    name: 'startdt',
                    id: 'startdt',
                    vtype: 'daterange',
                    endDateField: 'enddt',
                    format: 'Y-m-d',
                    allowBlank:false
                }
                ]
            }, {
                columnWidth:.5,
                layout: 'form',
                items: [{
                    xtype: 'combo',
                    fieldLabel: 'Usuario',
                    store: usersDataStore,
                    emptyText: 'Seleccione un usuario',
                    displayField: 'usuario',
                    valueField: 'idusuario',
                    name: 'idusuario',
                    hiddenName: 'usrId',
                    forceSelection: true,
                    typeAhead: true,
                    triggerAction: 'all',
                    mode: 'local',
                    width: 140,
                    anchor:'95%'
                //listWidth: 140
                },
                {
                    xtype: 'datefield',
                    fieldLabel: 'Hasta',
                    name: 'enddt',
                    id: 'enddt',
                    vtype: 'daterange',
                    format: 'Y-m-d',
                    startDateField: 'startdt',
                    allowBlank:false

                }]

            }]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Generar Reporte',
            iconCls: 'okIcon',
            formBind: true,
            handler: function() {
                var reportid = searchF.getForm().findField('reportid').getValue();
                var parametros = searchF.getForm().getValues();
                
                if(reportid == 1) {
                    report1Store.load({
                        params: parametros
                    });
                    tabpanel.show()
                    //tabpanel.removeAll();
                    tabpanel.add(report1Grid)
                    tabpanel.setActiveTab(report1Grid);
                   

                //report1Grid.render('report1');

                }
                
                if (reportid == 2) {
                    report2Store.load({
                        params: parametros
                    });
                    //report2Grid.render('report2');
                    tabpanel.show()
                    //tabpanel.removeAll();
                    tabpanel.add(report2Grid)
                    tabpanel.setActiveTab(report2Grid);
                   
                }

            }
        }]
    });

    function getReport() {
       var param =  searchF.getForm().getValues();
       var base = "/administrador/index.php/reportes/tocsv?";
       var query =  Ext.urlEncode(param);
       window.location = base + query;
       //console.log( base+query );
    }


    var toolbar1 = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Exportar a Excel',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/csv_file.png',
            handler: getReport
        }]
    });

    var toolbar2 = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Exportar a Excel',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/csv_file.png',
            handler: getReport
        }]
    });

    var report1Store = new Ext.data.Store({
        url: '/administrador/index.php/reportes/doreport',
        reader: new Ext.data.JsonReader({
            root: 'rows',
            //totalProperty: 'results',
            id: 'id '
        },[
        {
            name: 'id'
        },
        {
            name: 'operador'
        },
        {
            name: 'numero_consultas'
        }
        ])
    });

    var report1CM = [
    {
        header: 'Id',
        dataIndex: 'id',
        sortable: true
    },
    {
        header: 'Operador',
        dataIndex: 'operador',
        sortable: true
    },
    {
        header: 'Numero de Consultas',
        dataIndex: 'numero_consultas',
        sortable: true
    }

    ];

    var report1Grid = new Ext.grid.GridPanel({
        store: report1Store,
        columns: report1CM,
        frame: true,
        loadMask: {
            msg: 'Cargando datos, por favor espere...'
        },
        viewConfig: {
            forceFit: true
        },
        title: 'Reporte de consultas realizadas',
        stripeRows: true,
        height: 400,
        width: 600,
        tbar: toolbar1,
        iconCls: 'rIcon'
    });


    var report2Store = new Ext.data.Store({
        url: '/administrador/index.php/reportes/doreport',
        reader: new Ext.data.JsonReader({
            root: 'rows',
            //totalProperty: 'results',
            id: 'id '
        },[
        {
            name: 'id'
        },
        {
            name: 'operador'
        },
        {
            name: 'movil_registrado'
        },
        {
            name: 'fecha'
        }
        ])
    });

    var report2CM = [
    {
        header: 'Id',
        dataIndex: 'id',
        sortable: true
    },
    {
        header: 'Operador',
        dataIndex: 'operador',
        sortable: true
    },
    {
        header: 'Movil Registrado',
        dataIndex: 'movil_registrado'
    },
    {
        header: 'Fecha de Registro',
        dataIndex: 'fecha',
        sortable: true
    }
    ];
 

    var report2Grid = new Ext.grid.GridPanel({
        store: report2Store,
        columns: report2CM,
        frame: true,
        loadMask: {
            msg: 'Cargando datos, por favor espere...'
        },
        viewConfig: {
            forceFit: true
        },
        title: 'Reporte de numeros dentro de la promocion',
        stripeRows: true,
        height: 400,
        width: 600,
        tbar: toolbar2,
        iconCls: 'rIcon'
    });

    

//tabpanel.hide();
//tabpanel.add(report1Grid);
//tabpanel.add(report2Grid);
    

/*
    tabpanel.on('tabchange', function(){
        console.dir(this.getActiveTab());
    })
*/
});