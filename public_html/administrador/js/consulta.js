Ext.onReady(function(){
    // @todo cambiar url en produccion
    Ext.BLANK_IMAGE_URL = '/test_code/claropromocion/www/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    //Ext.form.Field.prototype.msgTarget = "side";

    

    Ext.apply(Ext.form.VTypes, {
        movil: function(val, field) {
            var re = new RegExp( "\^8+\\d{7}" );
            var b = false;
            if(val.match(re)){
                b = true;
            } else {
                b = false;
            }
            return b;
        },
        movilText: "Ingrese solo numeros moviles validos"
    });

    function registerPhone() {
        var num = Ext.getDom('movil').value;
        Ext.Ajax.request({
            url: '/test_code/claropromocion/www/index.php/consultas/register',
            params: {
                'numero': num
            },
            success: function (result, response) {
                //console.log(result.responseText);
                try {
                    var rs = Ext.util.JSON.decode(result.responseText);
                    tpl_success.overwrite(dataPanel.body, rs.data[0]);
                    dataPanel.body.highlight('#eeeeee', {
                        block:true
                    });
                    dataPanel.getBottomToolbar().disable();
                } catch(err) {
                //Ext.Msg.alert('Alerta', 'wtf fails ? ');
                }

            //console.dir(rs);
            },
            failure: function (result, response) {
                try {
                    var rs = Ext.util.JSON.decode(result.responseText);
                    var data = {
                        'mensaje': rs.mensaje
                    }
                    tpl_failure.overwrite(dataPanel.body, data);
                    dataPanel.body.highlight('#eeeeee', {
                        block:true
                    });
                } catch(err){
                //Ext.Msg.alert('Alerta', 'El numero telefonico no fue encontrado1');
                }
            //Ext.Msg.alert('Error', 'Ha ocurrido un error con la peticion al servidor');
            }
        });

    }

    var mybb = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Registrar movil en Promocion',
            cls: 'x-btn-text-icon',
            icon: '/test_code/claropromocion/www/img/add.png',
            disabled: true,
            handler: registerPhone
        }]
    });

    var dataPanel = new Ext.Panel({
        title: 'Resultado',
        width: 450,
        height: 200,
        bbar: mybb,
        iconCls: 'reportIcon'
    });    

    dataPanel.render('response');


    var tpl_success = new Ext.XTemplate(
        '<div style="padding: 10px 10px;">',
        '<p><b>Telefono: </b>{telefono}</p>',
        '<br />',
        '<p><b>Operador: </b>{operador}</p>',
        '<br />',
        '<p><b>Fecha activacion: </b>{fecha}</p>',
        '<br />',
        '<p><b>Promocion: </b>{promocion}</p>',
        '<br />',
        '<p style="color: blue;">El movil ya fue registrado en la promocion</p>',
        '</div>');
    var tpl_failure = new Ext.XTemplate(
        '<div style="padding: 10px 10px;">',
        '<p style="color: blue;">{mensaje}</p>',
        '</div>');

    var tpl_failure1 = new Ext.XTemplate(
        '<div style="padding: 10px 10px;">',
        '<p style="color: red;">{mensaje}</p>',
        '</div>');

    

    var consultaForm =  new Ext.form.FormPanel({
        bodyStyle:"padding:10px 0px 10px 10px; margin: 0",
        url: '/test_code/claropromocion/www/index.php/consultas/check',
        buttonAlign: "right",
        width: 390,
        height: 110,
        labelWidth: 175,
        title: "Consultar telefono",
        iconCls: 'search_button',
        frame: true,
        items: [
        //{
        //border: false,
        //layout: 'form',
        //items: [
        {
            xtype: 'textfield',
            fieldLabel: 'Ingrese el numero a consultar',
            name: 'movil',
            allowBlank: false,
            id: 'movil',
            vtype: 'movil',
            width: 180,
            maxLength: 8

        }
        //]
        //}
        ],
        monitorValid: true,
        buttons: [{
            text: 'Consultar',
            formBind: true,
            type: 'submit',
            iconCls: 'search_button',
            handler: function() {
                consultaForm.getForm().submit({
                    success: function(result, response){
                        //console.log(result.responseText);
                        //Ext.Msg.alert("success");

                        try {
                            var rs = Ext.util.JSON.decode(response.response.responseText);
                            //console.dir(rs);
                            //dataPanel.render('response');
                            tpl_success.overwrite(dataPanel.body, rs.data[0]);
                            dataPanel.body.highlight('#eeeeee', {
                                block:true
                            });

                        } catch(err) {
                            Ext.Msg.alert('Alerta', 'El numero telefonico no fue encontrado');
                        }

                    },
                    failure: function(result, response){
                        //Ext.Msg.alert("failure")
                        //var rs = Ext.util.JSON.decode(response.response.responseText);
                        //console.log(rs.mensaje);
                        //console.log(response.response.responseText);

                        try {
                            var rs = Ext.util.JSON.decode(response.response.responseText);
                            //console.dir(rs);
                            if(rs.code == 'err1') {
                                dataPanel.getBottomToolbar().enable();
                            //dataPanel.add(register_button);
                            }
                            var data = {
                                'mensaje': rs.mensaje
                            }
                            //dataPanel.render('response');
                            //tpl_failure.overwrite(dataPanel.body, data);
                            if (rs.code == 'err1') {
                                tpl_failure.overwrite(dataPanel.body, data);
                            } else {
                                tpl_failure1.overwrite(dataPanel.body, data);
                            }
                            dataPanel.body.highlight('#eeeeee', {
                                block:true
                            });
                        } catch(err){
                        //Ext.Msg.alert('Alerta', 'El numero telefonico no fue encontrado1');
                        }

                    }

                });

            }
        }]
    });
    consultaForm.render('form1');
    var nav = new Ext.KeyNav(consultaForm.getForm().getEl(), {
        'enter': function(e) {
            this.getForm().submit({
                success: function(result, response){
                    //console.log(result.responseText);
                    //Ext.Msg.alert("success");

                    try {
                        var rs = Ext.util.JSON.decode(response.response.responseText);
                        //console.dir(rs);
                        //dataPanel.render('response');
                        tpl_success.overwrite(dataPanel.body, rs.data[0]);
                        dataPanel.body.highlight('#eeeeee', {
                            block:true
                        });

                    } catch(err) {
                        Ext.Msg.alert('Alerta', 'El numero telefonico no fue encontrado');
                    }

                },
                failure: function(result, response){
                    //Ext.Msg.alert("failure")
                    //var rs = Ext.util.JSON.decode(response.response.responseText);
                    //console.log(rs.mensaje);
                    //console.log(response.response.responseText);

                    try {
                        var rs = Ext.util.JSON.decode(response.response.responseText);
                        //console.dir(rs);
                        if(rs.code == 'err1') {
                            dataPanel.getBottomToolbar().enable();
                        //dataPanel.add(register_button);
                        }
                        var data = {
                            'mensaje': rs.mensaje
                        }
                        //dataPanel.render('response');
                        //tpl_failure.overwrite(dataPanel.body, data);
                        if (rs.code == 'err1') {
                            tpl_failure.overwrite(dataPanel.body, data);
                        } else {
                            tpl_failure1.overwrite(dataPanel.body, data);
                        }
                        dataPanel.body.highlight('#eeeeee', {
                            block:true
                        });
                    } catch(err){
                    //Ext.Msg.alert('Alerta', 'El numero telefonico no fue encontrado1');
                    }

                }

            });
        },
        'scope': consultaForm
    });

});
