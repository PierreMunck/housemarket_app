/*
 * Grid para el listado de creditos del clientes
 *
 */
Ext.onReady(function(){
    Ext.BLANK_IMAGE_URL = '/administrador/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = "side";

    var tipoStore = new Ext.data.SimpleStore({
        fields: ['tipousuario'],
        data: [['Administrador']]
    });

    Ext.apply(Ext.form.VTypes, {
        daterange : function(val, field) {
            var date = field.parseDate(val);

            if(!date){
                return;
            }
            if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                var start = Ext.getCmp(field.startDateField);
                start.setMaxValue(date);
                start.validate();
                this.dateRangeMax = date;
            }
            else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                var end = Ext.getCmp(field.endDateField);
                end.setMinValue(date);
                end.validate();
                this.dateRangeMin = date;
            }
            /*
         * Always return true since we're only using this vtype to set the
         * min/max allowed values (these are tested for after the vtype test)
         */
            return true;
        },
        numbersonly: function(val, field) {
            var n = parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        },
        numbersonlyText: 'Ingresa solo numeros aqui',
        valcantidad: function(val, field) {
            var result= /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\d+)?$/.test(val);
            var CrediMax=document.getElementById('CrediMax');
            if(result){
                var valint=parseInt(val);
                if(valint<=0)
                    return false;
                else if(valint <= parseInt(CrediMax.value))
                    return result;
                else
                    return false;
            }
            return result;
        },
        valcantidadText: 'La cantidad debe ser menor � igual que cr�dito maximo.',
        montocantidad: function(val, field) {
            var Precio_Online=document.getElementById('Precio_Paypal');
            var Mini_Credit=document.getElementById('Mini_Credit');
            addcliente_form.getForm().findField('Monto').setValue(0);
            var result= /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\d+)?$/.test(val);
            if(result){
                if(parseInt(val)<=0)
                    return false;
                
                var porcentaje=parseInt(val) % parseInt(Mini_Credit.value);
                if(porcentaje==0){
                    var precio=parseFloat(val) * parseFloat(Precio_Online.value);
                    precio=precio.toFixed(2);
                    addcliente_form.getForm().findField('Monto').setValue(precio);
                    return result;
                }
                else
                    return false;
            }
            return result;
        },
        montocantidadText: 'La cantidad no es valida'
    });

    function isNum(num) {
        var test = false;
        if(Ext.isEmpty(num)){
            test = true;
        } else {
            test = Ext.num(num, false);
            if(test < 0){
                test = false;
            }

        }
        return test;
    }

    // funcion para recarga dinamica del grid.
    function getCliente(CodigoCliente) {
        clienteDataStore.load({
            params: {
                start:0,
                'CodigoCliente': CodigoCliente
            }
        });
    }

    //funcion para recargar el grid con las propiedades
    function getProPropiedad(CodigoCliente,Categoria,Pais,Transaccion,Ciudad,Broker){
        totalDataStore.load({
            params: {
                start:0,
                'CodigoCliente': CodigoCliente,
                'Categoria': Categoria,
                'Pais': Pais,
                'Transaccion':Transaccion,
                'Ciudad':Ciudad,
                'Broker':Broker
            }
        });
    }

    function getDetalle(){
        var credito = dtgridCliente.getSelectionModel();
        var propiedad = dtgridTotal.getSelectionModel();

        if(credito.hasSelection()){
            if(propiedad.hasSelection()){
                var selcre = credito.getSelected();
                var selprop = propiedad.getSelected();
                detalleDataStore.load({
                    params: {
                        start:0,
                        'CodigoCredito': selcre.data.CodigoCredito,
                        'CodigoPropiedad': selprop.data.CodigoPropiedad
                    }
                });
            }
            else
                Ext.Msg.alert('Alerta','Seleccione una propiedad');
        }
        else
            Ext.Msg.alert('Alerta','Seleccione un cr�dito');
    }


    var b = Ext.get('crebt');
    b.on('click', function(){
        crebuscar();
    })

    var busP =Ext.get('BuscarPropiedad');
    busP.on('click', function(){
        var CodCliente=document.getElementById('IdCliente');
        if(CodCliente.value!=""){
            propbuscar();
            document.getElementById('hmk').focus();
        }
        else
            Ext.Msg.alert('Alerta','Seleccione un cliente y presione el boton buscar');
    })

    var c = Ext.get('crers');
    c.on('click', function(){
        Ext.getDom('hb').reset();
        clienteDataStore.load();
        totalDataStore.load();
        detalleDataStore.load();
        return false;
    })

    //var search_button = Ext.get('search_button');
    function isEmpty(inputStr) {
        if (null == inputStr || "" == inputStr) {
            return true;
        }
        return false;
    }

    function check_id(val) {
        var n = parseInt(val);
        return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
    }

    //var search_button1 = Ext.get('search_button1');
    function crebuscar() {
        var dataurl = Ext.Ajax.serializeForm('hb');
        var data = Ext.urlDecode(dataurl);
        var idCliente=data.cbCliente;
        var Categoria=data.cbCategoria;
        var Pais=data.cbPais;
        var Transaccion=data.cbTansaccion;
        var Ciudad=data.val_ciudad;
        var Broker=data.val_broker;
        var CodCliente=document.getElementById('IdCliente');
        if(idCliente!="")
            CodCliente.value=idCliente;
        else
            CodCliente.value="";

        getCliente(idCliente);
        getProPropiedad(idCliente,Categoria,Pais,Transaccion,Ciudad,Broker);
        detalleDataStore.load();
    }

    function propbuscar() {
        var dataurl = Ext.Ajax.serializeForm('hb');
        var data = Ext.urlDecode(dataurl);
        var Categoria=data.cbCategoria;
        var Pais=data.cbPais;
        var Transaccion=data.cbTansaccion;
        var Ciudad=data.val_ciudad;
        var Broker=data.val_broker;
        var CodCliente=document.getElementById('IdCliente');
        getProPropiedad(CodCliente.value,Categoria,Pais,Transaccion,Ciudad,Broker);
        detalleDataStore.load();
    }

    Ext.namespace('Ext.userdata');
    Ext.userdata.status = [['0', 'inactiva'], ['1', 'activa']];
    var stStore = new Ext.data.SimpleStore({
        fields: ['status', 'label'],
        data: Ext.userdata.status
    });

    var addcliente_form = new Ext.form.FormPanel({
        url: '/administrador/index.php/creditos/crecliente',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 350,
        height: 150,
        labelWidth: 130,
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'ID Cliente',
                name: 'CodigoCliente',
                allowBlank: false,
                readOnly : true,
                vtype: 'numbersonly',
                maxLength: 11
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Cantidad('+document.getElementById('Mini_Credit').value + ' pack)',
                name:'Cantidad',
                allowBlank: false,
                vtype: 'montocantidad',
                maxLength: 4
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Monto U$',
                name:'Monto',
                allowBlank: false,
                readOnly : true,
                maxLength: 8
            }
            ]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                addcliente_form.getForm().submit({
                    success: function(f,a){
                        addcliente_form.getForm().reset();
                        win.hide();
                        clienteDataStore.reload();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Warning','Ha ocurrido un error y <br /> no es posible guardar los datos');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                addcliente_form.getForm().reset();
                win.hide();

            }
        }]

    });

    var addprop_form = new Ext.form.FormPanel({
        url: '/administrador/index.php/creditos/actpropiedad',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 350,
        height: 150,
        labelWidth: 130,
        //labelAlign: 'right',
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'ID Cr�dito',
                name: 'CodigoCredito',
                allowBlank: false,
                readOnly : true,
                vtype: 'numbersonly',
                maxLength: 11
            },
            {
                xtype: 'textfield',
                fieldLabel: 'ID Propiedad',
                name: 'CodigoPropiedad',
                allowBlank: false,
                readOnly : true,
                vtype: 'numbersonly',
                maxLength: 11
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Cr�dito Maximo',
                name: 'CreditoMaximo',
                allowBlank: false,
                readOnly : true,
                vtype: 'numbersonly',
                maxLength: 11
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Cantidad',
                name:'Cantidad',
                allowBlank: false,
                vtype: 'valcantidad',
                maxLength: 4
            },
            {
                xtype: 'hidden',
                name:'Estado'
            }
            ]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                addprop_form.getForm().submit({
                    success: function(f,a){
                        addprop_form.getForm().reset();
                        winProp.hide();
                        getDetalle();
                        clienteDataStore.reload();
                        totalDataStore.reload();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Warning','Ha ocurrido un error y <br /> no es posible guardar los datos');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                addprop_form.getForm().reset();
                winProp.hide();

            }
        }]

    });


    var editcliente_form = new Ext.form.FormPanel({
        url: '/administrador/index.php/creditos/creeditcliente',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 350,
        height: 150,
        labelWidth: 130,
        //labelAlign: 'right',
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Codigo Cliente',
                name: 'CodigoCliente',
                allowBlank: false,
                readOnly : true,
                vtype: 'numbersonly',
                maxLength: 20
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Cantidad',
                name:'Cantidad',
                vtype: 'numbersonly',
                maxLength: 20
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Fecha Vence',
                name: 'FechaVence',
                id: 'FechaVence',
                format: 'Y-m-d',
                allowBlank:false
            }
            ]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                editcliente_form.getForm().submit({
                    success: function(f,a){
                        editcliente_form.getForm().reset();
                        win_edit.hide();
                        clienteDataStore.reload();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Warning', 'Ha ocurrido un error y <br /> no es posible guardar los datos');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                editcliente_form.getForm().reset();
                win_edit.hide();

            }
        }]

    });


    var win = new Ext.Window({
        title: "Comprar Cr�dito",
        applyTo: 'ventana',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: addcliente_form
    });

    var winProp = new Ext.Window({
        title: "Asignar Cr�dito",
        applyTo: 'ventana_propiedad',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: addprop_form
    });

    var win_edit = new Ext.Window({
        title: 'Modificar Cr�dito',
        applyTo: 'ventana1',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: editcliente_form
    });

    function addCliente () {
        //var dataurl = Ext.Ajax.serializeForm('hb');
        //var data = Ext.urlDecode(dataurl);
        //var idCliente=data.cbCliente;
        var CodCliente=document.getElementById('IdCliente');

        if(CodCliente.value!=""){
            addcliente_form.getForm().findField('CodigoCliente').setValue(CodCliente.value);
            win.show();
        } else {
            Ext.Msg.alert('Alerta', 'Seleccione un cliente y presione buscar para poder comprar cr�dito')
        }
    }

    function addProp () {
        var CrediMax=document.getElementById('CrediMax');
        var credito = dtgridCliente.getSelectionModel();
        var propiedad = dtgridTotal.getSelectionModel();

        if(credito.hasSelection()){
            if(propiedad.hasSelection()){
                var selcre = credito.getSelected();
                var selprop = propiedad.getSelected();
                if(selcre.data.Estado=='Vencido')
                    Ext.Msg.alert('Alerta', 'El saldo del cr�dito est� vencido');
                else if(selcre.data.Saldo==0)
                    Ext.Msg.alert('Alerta', 'El cr�dito no tiene saldo');
                else{
                    addprop_form.getForm().findField('CodigoCredito').setValue(selcre.data.CodigoCredito);
                    addprop_form.getForm().findField('CodigoPropiedad').setValue(selprop.data.CodigoPropiedad);
                    addprop_form.getForm().findField('CreditoMaximo').setValue(selcre.data.Saldo);
                    if(selcre.data.Estado=='Pendiente')
                        addprop_form.getForm().findField('Estado').setValue('P');
                    else
                        addprop_form.getForm().findField('Estado').setValue('A');

                    CrediMax.value=selcre.data.Saldo;
                    winProp.show();
                }
            }else {
                Ext.Msg.alert('Alerta', 'Seleccione la propiedad para signar credito');
            }

        } else {
            Ext.Msg.alert('Alerta', 'Debe seleccionar un cr�dito para realizar la operaci�n');
        }
    }

    function editCliente() {
        var sm = dtgridCliente.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            editcliente_form.getForm().findField('CodigoCredito').setValue(sel.data.CodigoCredito);
            editcliente_form.getForm().findField('Cantidad').setValue(sel.data.Cantidad);
            win_edit.show();
        } else {
            Ext.Msg.alert('Alerta', 'Debe seleccionar un cr�dito para poder realizar operacion')
        }
    }

    function eliminaCredito(){
        var sm = dtgridDetalle.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            //sel.data.CodigoCreditoProp;
            if(sel.data.EstadoAsigna.toUpperCase()=='P'){
                if(confirm('Seguro que desea eliminar el registro?')){
                    Ext.Ajax.request({
                        url: '/administrador/index.php/creditos/eliminarcreditos',
                        params: {
                            'CodigoCreditoProp': sel.data.CodigoCreditoProp,
                            'CodigoCredito': sel.data.CodigoCredito,
                            'Cantidad': sel.data.Cantidad
                        },
                        success: function (result, response) {
                            clienteDataStore.reload();
                            totalDataStore.reload();
                            detalleDataStore.reload();
                        },
                        failure: function (result, response) {
                            Ext.Msg.alert('Alerta','Error al intentar eliminar el registro');
                        }
                    });
                }
            }
            else{
                Ext.Msg.alert('alerta','Solo se permite eliminar en estado pendiente')
            }
        } else {
            Ext.Msg.alert('Alerta', 'Debe seleccionar un registro')
        }
    }

    function status_image(val){
        var caption;
        switch(val)
        {
            case 1:
                caption = "Vendida";
                break;
            case 2:
                caption = "Alquilada";
                break;
            case 3:
                caption = "Activa";
                break;
            case 4:
                caption = "Inactiva";
                break;
        }
        return '<div class="imagen-' + val +'">' + caption +'</div>'
    }

    var cretoolbar = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Comprar cr�dito',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/add_credito.png',
            handler: addCliente
        },
        {
            xtype: 'tbseparator'
        }
        ]
    });

    var protoolbar = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Asignar cr�dito',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/add_credito.png',
            handler: addProp
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'tbbutton',
            text: 'Detalle cr�dito',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/add.png',
            handler: getDetalle
        },
        {
            xtype: 'tbseparator'
        }
        ]
    });


    var detalletoolbar = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Eliminar cr�dito',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/erase.png',
            handler: eliminaCredito
        },
        {
            xtype: 'tbseparator'
        }
        ]
    });

    /* Datepicker */
    /*   var datefield = new Ext.FormPanel({
        labelWidth: 72, // label settings here cascade unless overridden
        frame:false,
        title: '',
        formId:"fechas_creditos",
        bodyStyle:'padding:5px 5px 0;background-color:#F7F7F7;border:none;',
        width: 198,
        defaults: {width: 95},
        defaultType: 'datefield',

        items: [{
                fieldLabel: 'F.Asignaci�n',
                name: 'date_ac',
                id: 'date_ac',
                vtype: 'daterange',
                endDateField: 'date_cc',
                //format: 'Y-m-d',
                allowBlank:true
            },
            {
                fieldLabel: 'F.Caducidad',
                name: 'date_cc',
                id: 'date_cc',
                vtype: 'daterange',
                startDateField: 'date_ac',
                //format: 'Y-m-d',
                allowBlank:true
            }
        ]
    });


    datefield.render('datefield_ac');*/


    var clienteDataStore = new Ext.data.Store({
        url: '/administrador/index.php/creditos/getcrecliente',
        reader: new Ext.data.JsonReader({
            root: 'rows',
            totalProperty: 'results',
            id: 'CodigoCredito'
        }, [
        {
            name: 'CodigoCredito',
            type: 'int'
        },
        {
            name: 'FechaCompra',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            name: 'FechaVence',
            type: 'date',
            dateFormat: 'Y-m-d'
        },
        {
            name: 'Cantidad',
            type: 'int'
        },
        {
            name: 'Saldo',
            type: 'int'
        },
        {
            name: 'Estado',
            type: 'string'
        }
        ])
        ,
        remoteSort:true
        ,
        sortInfo: {
            field: 'CodigoCredito',
            direction: 'DESC'
        }
    });

    var totalDataStore = new Ext.data.Store({
        url: '/administrador/index.php/creditos/totalcredit',
        reader: new Ext.data.JsonReader({
            root: 'rows',
            totalProperty: 'results',
            id: 'CodigoPropiedad'
        }, [
        {
            name: 'CodigoPropiedad',
            type: 'int'
        },
        {
            name: 'CodigoBroker',
            type: 'string'
        },
        {
            name: 'Creditos',
            type: 'string'
        },
        {
            name: 'NombrePropiedad',
            type: 'string'
        },
        {
            name: 'Pais',
            type: 'string'
        },
        {
            name: 'Ciudad',
            type: 'string'
        },
        {
            name: 'Estatus',
            type: 'string'
        },
        {
            name: 'FechaRegistro',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        }
        ])
        ,
        remoteSort:true
        ,
        sortInfo: {
            field: 'CodigoPropiedad',
            direction: 'DESC'
        }
    });

    var detalleDataStore = new Ext.data.Store({
        url: '/administrador/index.php/creditos/detallecredit',
        reader: new Ext.data.JsonReader({
            root: 'rows',
            totalProperty: 'results',
            id: 'CodigoCreditoProp'
        }, [
        {
            name: 'CodigoCreditoProp',
            type: 'int'
        },
        {
            name: 'CodigoCredito',
            type: 'int'
        },
        {
            name: 'CodigoPropiedad',
            type: 'int'
        },
        {
            name: 'Cantidad',
            type: 'int'
        },
        {
            name: 'EstadoAsigna',
            type: 'string'
        }
        ])
        ,
        remoteSort:true
        ,
        sortInfo: {
            field: 'CodigoCreditoProp',
            direction: 'DESC'
        }
    });

    var crepagingBar = new Ext.PagingToolbar({
        pageSize:30,
        store: clienteDataStore,
        displayInfo: true,
        emptyMsg: "Sin Registros a desplegar",
        displayMsg: 'Registros {0} - {1} de {2}'

    });

    var proppagingBar = new Ext.PagingToolbar({
        pageSize:30,
        store: totalDataStore,
        displayInfo: true,
        emptyMsg: "Sin Registros a desplegar",
        displayMsg: 'Registros {0} - {1} de {2}'

    });

    var detallepagingBar = new Ext.PagingToolbar({
        pageSize:30,
        store: detalleDataStore,
        displayInfo: true,
        emptyMsg: "Sin Registros a desplegar",
        displayMsg: 'Registros {0} - {1} de {2}'

    });


    var clienteCM = [
    {
        header: 'ID',
        dataIndex: 'CodigoCredito',
        sortable: true
    },
    {
        header: 'Fecha Compra',
        dataIndex: 'FechaCompra',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    },
    {
        header: 'Fecha Vence',
        dataIndex: 'FechaVence',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    },
    {
        header: 'Cantidad',
        dataIndex: 'Cantidad',
        sortable: true
    },
    {
        header: 'Saldo',
        dataIndex: 'Saldo',
        sortable: true
    },
    {
        header: 'Estado',
        dataIndex: 'Estado',
        sortable: true
    }
    ];

    var totalCM = [
    {
        header: 'ID',
        dataIndex: 'CodigoPropiedad',
        sortable: true
    },
    {
        header: 'C�digo Broker',
        dataIndex: 'CodigoBroker',
        sortable: true
    },
    {
        header: 'Cr�ditos',
        dataIndex: 'Creditos',
        sortable: true
    },
    {
        header: 'Propiedad',
        dataIndex: 'NombrePropiedad',
        sortable: true
    },
    {
        header: 'Pa�s',
        dataIndex: 'Pais',
        sortable: true
    },
    {
        header: 'Ciudad',
        dataIndex: 'Ciudad',
        sortable: true
    },
    {
        header: 'Estatus',
        dataIndex: 'Estatus',
        sortable: true
    },
    {
        header: 'Fecha de Registro',
        dataIndex: 'FechaRegistro',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    }
    ];


    var detalleCM = [
    {
        header: 'ID',
        dataIndex: 'CodigoCreditoProp',
        sortable: true
    },
    {
        header: 'C�digo Cr�dito',
        dataIndex: 'CodigoCredito',
        sortable: true
    },
    {
        header: 'C�digo Propiedad',
        dataIndex: 'CodigoPropiedad',
        sortable: true
    },
    {
        header: 'Cantidad',
        dataIndex: 'Cantidad',
        sortable: true
    }
    ];


    var dtgridCliente = new Ext.grid.GridPanel({
        store: clienteDataStore,
        columns: clienteCM,
        frame: true,
        loadMask: {
            msg: 'Cargando datos, por favor espere ....'
        },
        viewConfig: {
            forceFit:true,
            emptyText: 'No hay resultados.'
        },
        renderTo: 'creclientes_grid',
        stripeRows: true,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel({
            singleSelect: true
        }),
        autoWidth: true,
        collapsed: false,
        title: 'Cr�ditos',
        tbar: cretoolbar,
        bbar: crepagingBar,
        iconCls: 'creditos_panel'
    });

    clienteDataStore.load({
        params:{
            start:0,
            limit:30
        }
    });


    var dtgridTotal = new Ext.grid.GridPanel({
        store: totalDataStore,
        columns: totalCM,
        frame: true,
        loadMask: {
            msg: 'Cargando datos, por favor espere ....'
        },
        viewConfig: {
            forceFit:true,
            emptyText: 'No hay resultados.'
        },
        renderTo: 'total_grid',
        stripeRows: true,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel({
            singleSelect: true
        }),
        autoWidth: true,
        collapsed: false,
        title: 'Propiedades',
        tbar: protoolbar,
        bbar: proppagingBar,
        iconCls: 'creditos_panel'
    });

    totalDataStore.load({
        params:{
            start:0,
            limit:30
        }
    });

    var dtgridDetalle = new Ext.grid.GridPanel({
        store: detalleDataStore,
        columns: detalleCM,
        frame: true,
        loadMask: {
            msg: 'Cargando datos, por favor espere ....'
        },
        viewConfig: {
            forceFit:true,
            emptyText: 'No hay resultados.'
        },
        renderTo: 'detalle_grid',
        stripeRows: true,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel({
            singleSelect: true
        }),
        autoWidth: true,
        collapsed: false,
        title: 'Detalle',
        tbar: detalletoolbar,
        bbar: detallepagingBar,
        iconCls: 'creditos_panel'
    });

    detalleDataStore.load({
        params:{
            start:0,
            limit:30
        }
    });

});