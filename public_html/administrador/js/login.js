Ext.onReady(function(){
    // @todo cambiar url en produccion
    Ext.BLANK_IMAGE_URL = '/administrador/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();

    var loginbutton = new Ext.Button({
        type: 'submit',
        text: 'Login',
        iconCls: 'loginIcon',
        applyTo: 'login_button'
    });

    var lpanel = new Ext.Panel({
        contentEl: 'login_form',
        autoScroll: true,
        width: '350',
        iconCls: 'panelIcon',
        title: 'Login Form',
        titleCollapse: false,
        renderTo: 'login_panel'
        
    });

    loginbutton.on('click', function(){
        var f = Ext.getDom('login');
        f.submit();
    });
});
