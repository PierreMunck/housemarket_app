/* 
 * Grid para el listado de usuarios
 *
 */
Ext.onReady(function(){
    // @todo cambiar url en produccion
    Ext.BLANK_IMAGE_URL = '/administrador/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = "side";

    var tipoStore = new Ext.data.SimpleStore({
        fields: ['tipousuario'],
        data: [['Administrador']]
    });

 Ext.apply(Ext.form.VTypes, {
        daterange : function(val, field) {
            var date = field.parseDate(val);

            if(!date){
                return;
            }
            if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                var start = Ext.getCmp(field.startDateField);
                start.setMaxValue(date);
                start.validate();
                this.dateRangeMax = date;
            }
            else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                var end = Ext.getCmp(field.endDateField);
                end.setMinValue(date);
                end.validate();
                this.dateRangeMin = date;
            }
            /*
         * Always return true since we're only using this vtype to set the
         * min/max allowed values (these are tested for after the vtype test)
         */
            return true;
        },
        numbersonly: function(val, field) {
            var n = parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        },
        numbersonlyText: 'Ingresa solo numeros aqui'
    });

     function isNum(num) {
            var test = false;
            if(Ext.isEmpty(num)){
                test = true;
            } else {
                test = Ext.num(num, false);
                //var tx = (test >= 0) ? true : false;
                //console.info('dato es ' + typeof(test));
                //console.info('test = '+  tx);
                if(test < 0){
                    test = false;
                }

            }
            return test;
        }

     new Ext.ToolTip({
        target: direccion,
        bodyStyle: 'padding: 5px 10px;',
        trackMouse:true,
        html: 'Ingresa una locacion o direccion, incluyendo el pais, Ej. Villa Fontana, Nicaragua'
    });



    var direccion = Ext.get('direccion');
    var cat = Ext.get('cat');
    var rent = Ext.get('rent');
    var buy = Ext.get('buy');
    var min_price = Ext.get('min_price');
    var max_price = Ext.get('max_price');
    var beds = Ext.get('beds');
    var baths = Ext.get('baths');
    var area = Ext.get('area');
    var city= '';
    var cc= '';


         min_price.on('blur', function(){
            if(isNum(this.getValue())){
                //console.info('ok: min_price = ' +this.getValue());
                if(this.hasClass('error')) {
                    this.removeClass('error');
                }
            } else {
                //console.info('fail: min_price = ' +this.getValue());
                this.addClass('error');
            }
        });

        max_price.on('blur', function(){
            if(isNum(this.getValue())){
                //console.info('ok: max_price = ' +this.getValue());
                if(this.hasClass('error')) {
                    this.removeClass('error');
                }
            } else {
                //console.info('fail: max_price = ' +this.getValue());
                this.addClass('error');
            }
        });

        direccion.on('blur', function(){
            if( Ext.isEmpty( this.getValue()) ) {
                this.addClass('error');

            //console.info('fail: sin direccion ');
            } else {
                if(this.hasClass('error')) {
                    this.removeClass('error');
                }
            //console.info('ok: - direccion ingresada: ' + this.getValue());
            }
        });

        cat.on('change', function(){
            var tipo_prop = this.getValue();
            var rooms = Ext.getDom('beds');
            var banos = Ext.getDom('baths');
            switch (tipo_prop) {
                case 'CB':
                case 'EB':
                case 'EO':
                    rooms.setAttribute("disabled","disabled");
                    banos.removeAttribute('disabled');
                    break;
                case 'TE':
                    rooms.setAttribute("disabled","disabled");
                    banos.setAttribute("disabled","disabled");
                    break;
                default:
                    rooms.removeAttribute('disabled');
                    banos.removeAttribute('disabled');
            }
        });

         // funcion para recarga dinamica del grid.
        function getProps(cc, city, dir1, tipo1,min_creditos1,max_creditos1, min_price1, max_price1, min_beds1, max_beds1,min_baths1, max_baths1, area1, cat1,date_ac1,date_cc1) {
            propDataStore.load({
                params: {
                     start:0,
                    'pais': cc,
                    'ciudad': city,
                    'direccion': dir1,
                    'tipo': tipo1,
                    'min_creditos': min_creditos1,
                    'max_creditos': max_creditos1,
                    'min_price': min_price1,
                    'max_price': max_price1,
                    'min_beds': min_beds1,
                    'max_beds': max_beds1,
                    'min_baths': min_baths1,
                    'max_baths': max_baths1,
                    'area': area1,
                    'cat': cat1,
                    'fecha_ac': date_ac1,
                    'fecha_cc': date_cc1
                }
                     
            });
        }

    var b = Ext.get('bt');
    b.on('click', function(){
          buscar();
    })

    var c = Ext.get('rs');
    c.on('click', function(){
         Ext.getDom('hb').reset();
         return false;
    })

    //var search_button = Ext.get('search_button');
    function isEmpty(inputStr) {
            if (null == inputStr || "" == inputStr) {
                return true;
            }
            return false;
        }

     function check_id(val) {
            var n = parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
     }

            //var search_button1 = Ext.get('search_button1');
     function buscar() {
            // var query = Ext.get('buscar').getValue();
            // console.log('consulta: ' + query);
            //var data = form_busqueda.getForm().getValues();
            var dataurl = Ext.Ajax.serializeForm('hb');
            var data = Ext.urlDecode(dataurl);
            var dataurlcred = Ext.Ajax.serializeForm('fechas_creditos');
            var datacred = Ext.urlDecode(dataurlcred);

            // console.dir(data);
            // console.dir(datacred);
            var dir1 = data.direccion;
            var tipo1 = data.tipo;
            var min_creditos = data.min_creditos;
            var max_creditos = data.max_creditos;
            var min_price1 = data.min_price;
            var max_price1 = data.max_price;
            var min_beds1 = data.min_beds;
            var max_beds1 = data.max_beds;
            var min_baths1 = data.min_baths;
            var max_baths1 = data.max_baths;
          
            var area1 = data.area;
            var cat1 = data.cat;
            var date_ac1 = datacred.date_ac;
            var date_cc1 = datacred.date_cc;

            //var query;
            var cc=data.country;
            var city=data.city;
            if (!isEmpty(dir1)) {
                    //console.log('query es un id de propiedad ' + dir);
                    getProps(cc, city, dir1, tipo1,min_creditos,max_creditos,min_price1, max_price1,min_beds1, max_beds1,min_baths1, max_baths1, area1, cat1,date_ac1,date_cc1);
             
            } else {
                    getProps(cc, city, null, tipo1,min_creditos,max_creditos, min_price1, max_price1, min_beds1, max_beds1,min_baths1, max_baths1, area1, cat1,date_ac1,date_cc1);
                
            }
     }

    //search_button.on('click', buscar);
    var nav = new Ext.KeyNav(Ext.getDom('direccion'), {
            'enter': buscar,
            'scope': this
    });
    var creditform = new Ext.form.FormPanel({
        url: '/administrador/index.php/creditos/add',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 350,
        height: 150,
        labelWidth: 130,
        //labelAlign: 'right',
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Id Propiedad',
                name: 'idpropiedad',
                allowBlank: false,
                readOnly : true,
                vtype: 'alphanum',
                maxLength: 20
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Creditos',
                name:'creditos',
                vtype: 'numbersonly',
                maxLength: 20
            }
           ]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                creditform.getForm().submit({
                    success: function(f,a){
                        creditform.getForm().reset();
                        win.hide();
                        propDataStore.reload();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Warning', 'Ha ocurrido un error <br /> no es posible guardar los datos');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                creditform.getForm().reset();
                win.hide();

            }
        }]

    });
    




    





    
    Ext.namespace('Ext.userdata');
    Ext.userdata.status = [['0', 'inactiva'], ['1', 'activa']];
    var stStore = new Ext.data.SimpleStore({
        fields: ['status', 'label'],
        data: Ext.userdata.status
    });
    //stStore.load();

    var edit_form = new Ext.form.FormPanel({
        url: '/administrador/index.php/creditos/edit',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 350,
        height: 150,
        labelWidth: 130,
        //labelAlign: 'right',
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'ID propiedad',
                name: 'idpropiedad',
                allowBlank: false,
                readOnly : true,
                vtype: 'alphanum',
                maxLength: 20
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Creditos',
                name:'creditos',
                vtype: 'numbersonly',
                maxLength: 20
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Fecha de caducidad',
                name: 'fecha_expiracion_credito',
                id: 'fecha_expiracion_credito',
                format: 'Y-m-d',
                allowBlank:false
            }
//            ,
//            {
//                xtype: 'hidden',
//                name: 'idpropiedad'
//            }
            ]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                edit_form.getForm().submit({
                    success: function(f,a){
                        edit_form.getForm().reset();
                        win_edit.hide();
                        propDataStore.reload();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Warning', 'Ha ocurrido un error y <br /> no es posible guardar los datos');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                edit_form.getForm().reset();
                win_edit.hide();

            }
        }]

    });

                       
    var win = new Ext.Window({
        title: "Asignar Credito",
        applyTo: 'ventana',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: creditform
    });

    var win_edit = new Ext.Window({
        title: 'Modificar Credito',
        applyTo: 'ventana1',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: edit_form
    });

    function addCredit () {
      
        var sm = gridCreditos.getSelectionModel();

        if(sm.hasSelection()){
            var sel = sm.getSelected();
//            console.dir(sel.data);
//             console.log(sel.data.fecha_asignacion_credito);
           if((sel.data.fecha_asignacion_credito==null)||(sel.data.fecha_asignacion_credito=="")){
            creditform.getForm().findField('idpropiedad').setValue(sel.data.idpropiedad);
            creditform.getForm().findField('creditos').setValue(sel.data.creditos);
            win.show();
           }else {
              Ext.Msg.alert('Alerta', 'Haga clic en el boton Modificar Credito')
           }

        } else {
            Ext.Msg.alert('Alerta', 'Debe Seleccionar una propiedad para poder realizar esta operacion')
        }
    }

    function editCredit() {
        var sm = gridCreditos.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            //console.dir(sel.data);
          if((sel.data.fecha_asignacion_credito!=null)||(sel.data.fecha_asignacion_credito!="")){
                edit_form.getForm().findField('idpropiedad').setValue(sel.data.idpropiedad);
                edit_form.getForm().findField('creditos').setValue(sel.data.creditos);
                edit_form.getForm().findField('fecha_expiracion_credito').setValue(sel.data.fecha_expiracion_credito);
                edit_form.getForm().findField('fecha_expiracion_credito').setMinValue(sel.data.fecha_expiracion_credito);
                win_edit.show();
           }else {
                Ext.Msg.alert('Alerta', 'Haga clic en el boton Asignar Credito')
           }


        } else {
            Ext.Msg.alert('Alerta', 'Debe Seleccionar una propiedad para poder realizar esta operacion')
        }
    }

    function delUsr(){
        var sm = gridCreditos.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            //console.dir(sel.data);
            var id_user = sel.data.idusuario;
            Ext.Msg.show({
                title: 'Eliminar Usuario',
                msg: 'Esta seguro que desea eliminar <br /> la cuenta del usuario ' + sel.data.usuario + ' ?',
                //buttons: Ext.MessageBox.OKCANCEL,
                buttons: {
                    ok: true,
                    cancel: true
                },
                fn: function(btn) {
                    switch(btn){
                        case 'ok':
                            Ext.Ajax.request({
                                url: '/administrador/index.php/creditos/delete',
                                params: {
                                    'id': id_user
                                },
                                success: function () {
                                    propDataStore.load();
                                },
                                failure: function () {
                                    Ext.msg.alert('Error', 'Ha ocurrido un error <br /> el usuario no pudo ser eliminado');
                                }
                            });
                            //Ext.Msg.alert('kill this dude', btn);
                            break;
                        case 'cancel':
                            //Ext.Msg.alert('wtf, no elimination', btn);
                            break;
                    }

                },
                icon: Ext.MessageBox.WARNING
            });

        } else {
            Ext.Msg.alert('Error', 'Debe Seleccionar un usuario para poder realizar esta operacion')
        }
    }

    function status_image(val){
       var caption;
       switch(val)
          {
            case 1:
               caption = "Vendida";
            break;
            case 2:
               caption = "Alquilada";
            break;
            case 3:
               caption = "Activa";
            break;
            case 4:
               caption = "Inactiva";
            break;
          }
        return '<div class="imagen-' + val +'">' + caption +'</div>'
    }

    var toolbar = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Asignar credito',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/add_credito.png',
            handler: addCredit
        },
        {
            xtype: 'tbseparator'
        },

        {
            xtype: 'tbbutton',
            text: 'Modificar Credito',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/edit_credito.png',
            handler: editCredit
        },
        {
            xtype: 'tbseparator'
        }
        /*,
        {
            xtype: 'tbbutton',
            text: 'Eliminar usuario',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/delete_user.png',
            handler: delUsr
        }
        ,
        { xtype: 'tbseparator'},
        { xtype: 'tbfill'},
            {  xtype: 'tbtext',
               text:  'Buscar usuario: &nbsp;'
            },
            {
                xtype: 'textfield',
                //text: 'buscar'
                listeners: {
                    specialkey: function() { console.log('checking'); }
                }

           }
           */
        ]
    });

   /* Datepicker */
    var datefield = new Ext.FormPanel({
        labelWidth: 72, // label settings here cascade unless overridden
        frame:false,
        title: '',
        formId:"fechas_creditos",
        bodyStyle:'padding:5px 5px 0;background-color:#F7F7F7;border:none;',
        width: 198,
        defaults: {width: 95},
        defaultType: 'datefield',

        items: [{
                fieldLabel: 'F.Asignaci&oacute;n',
                name: 'date_ac',
                id: 'date_ac',
                vtype: 'daterange',
                endDateField: 'date_cc',
                //format: 'Y-m-d',
                allowBlank:true
            },
            {
                fieldLabel: 'F.Caducaci&oacute;n',
                name: 'date_cc',
                id: 'date_cc',
                vtype: 'daterange',
                startDateField: 'date_ac',
                //format: 'Y-m-d',
                allowBlank:true
            }
        ]
    });


    datefield.render('datefield_ac');
    //datefield2.render('datefield_cc');


    var propDataStore = new Ext.data.Store({
        url: '/administrador/index.php/creditos/getregisters',
	//baseParams: fb,
	//autoLoad: false,
        reader: new Ext.data.JsonReader({
            root: 'rows',
            totalProperty: 'results',
            id: 'idpropiedad'
        }, [
        {
            name: 'idpropiedad',
            type: 'int'
        },

        {
            name: 'descripcion',
            type: 'string'
        },

        {
            name: 'creditos',
            type: 'float'
        },
        {
            name: 'fecha_asignacion_credito',
            type: 'date', dateFormat: 'Y-m-d'
        },
        {
            name: 'fecha_expiracion_credito',
            type: 'date', dateFormat: 'Y-m-d'
        },
        {
            name: 'pais',
            type: 'string'
        },
        {
            name: 'ciudad',
            type: 'string'
        },
        {
            name: 'precio_venta',
            type: 'float'
        },
        {
            name: 'precio_alquiler',
            type: 'float'
        },

        {
            name: 'tipo_enlistamiento',
            type: 'int'
        },
        {
            name: 'tipo_propiedad',
            type: 'int'

        },
        {
            name: 'fecha_ingreso',
            type: 'date', dateFormat: 'Y-m-d H:i:s'
        },

        {
            name: 'ultima_modificacion',
            type: 'date', dateFormat: 'Y-m-d H:i:s'
        },
        {
            name: 'area',
            type: 'string'
        },

        {
            name: 'cuartos',
            type: 'int'
        },

        {
            name: 'banos',
            type: 'int'
        },

        {
            name: 'lat',
            type: 'float'
        },

        {
            name: 'lng',
            type: 'float'
        },
        {
            name: 'estatus',
            type: 'int'
        },

        {
            name: 'uid',
            type: 'int'
        }
        ])
        ,remoteSort:true
        ,sortInfo: {
            field: 'idpropiedad',
            direction: 'ASC'
        }
    });
    
   // propDataStore.load();
    var pagingBar = new Ext.PagingToolbar({
        pageSize:30,
        store: propDataStore,
        displayInfo: true,
        emptyMsg: "Sin Registros a desplegar",
        displayMsg: 'Registros {0} - {1} de {2}'

    });

    var propCM = [
    {
        header: 'ID',
        dataIndex: 'idpropiedad',
        sortable: true
    },
    {
        header: 'Creditos',
        dataIndex: 'creditos',
        sortable: true
    },
    {
        header: 'Asignacion',
        dataIndex: 'fecha_asignacion_credito',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    },
    {
        header: 'Caducacion',
        dataIndex: 'fecha_expiracion_credito',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    },
    {
        header: 'Pais',
        dataIndex: 'pais',
        sortable: true
    },
    {
        header: 'Ciudad',
        dataIndex: 'ciudad',
        sortable: true
    },
    {
        header: 'Estatus de la Propiedad',
        dataIndex: 'estatus',
        width:120,
        sortable: true,
        renderer: status_image
    },
     {
        header: 'Fecha Ingreso',
        dataIndex: 'fecha_ingreso',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    },
    {
        header: 'Ultima Modificacion',
        dataIndex: 'ultima_modificacion',
         renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    }
    ];


    var gridCreditos = new Ext.grid.GridPanel({
        store: propDataStore,
        columns: propCM,
        frame: true,
        loadMask: {
            msg: 'Cargando datos, por favor espere ....'
        },
        viewConfig: {
            forceFit:true,
            emptyText: 'No hay resultados, intente otros filtros de busquedas.'
        },
        renderTo: 'creditos_grid',
        stripeRows: true,
        height: 600,
        autoWidth: true,
        collapsed: false,
        title: 'Gestión de creditos',
        tbar: toolbar,
        bbar: pagingBar,
        iconCls: 'creditos_panel'
    });

     propDataStore.load({params:{start:0, limit:30}});

});