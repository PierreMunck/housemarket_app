/*
 * Grid para el listado de aprobaci�n de cr�ditos del cliente
 *
 */
Ext.onReady(function(){
    Ext.BLANK_IMAGE_URL = '/administrador/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = "side";

    Ext.apply(Ext.form.VTypes, {
        daterange : function(val, field) {
            var date = field.parseDate(val);

            if(!date){
                return;
            }
            if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                var start = Ext.getCmp(field.startDateField);
                start.setMaxValue(date);
                start.validate();
                this.dateRangeMax = date;
            }
            else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                var end = Ext.getCmp(field.endDateField);
                end.setMinValue(date);
                end.validate();
                this.dateRangeMin = date;
            }
            /*
         * Always return true since we're only using this vtype to set the
         * min/max allowed values (these are tested for after the vtype test)
         */
            return true;
        },
        numbersonly: function(val, field) {
            var n = parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        },
        numbersonlyText: 'Ingresa solo numeros aqui'
    });

    // funcion para recarga dinamica del grid.
    function getpropiedad(CodigoCredito) {
        propiedadDataStore.load({
            params: {
                start:0,
                'CodigoCredito':CodigoCredito
            }
        });
    }

   /* var b = Ext.get('crebt');
    b.on('click', function(){
        crebuscar();
    })

    var c = Ext.get('crers');
    c.on('click', function(){
        Ext.getDom('hb').reset();
        return false;
    })*/

    //var search_button = Ext.get('search_button');
    function isEmpty(inputStr) {
        if (null == inputStr || "" == inputStr) {
            return true;
        }
        return false;
    }

    Ext.namespace('Ext.userdata');
    Ext.userdata.status = [['0', 'inactiva'], ['1', 'activa']];
    var stStore = new Ext.data.SimpleStore({
        fields: ['status', 'label'],
        data: Ext.userdata.status
    });

    var updatecredito_form = new Ext.form.FormPanel({
        url: '/administrador/index.php/autorizar/actualizacreditos',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 400,
        height: 150,
        labelWidth: 130,
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'ID Pedido',
                name: 'CodigoCredito',
                allowBlank: false,
                readOnly : true,
                vtype: 'numbersonly',
                maxLength: 11
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Cliente',
                name:'Cliente',
                allowBlank: false,
                readOnly : true
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Representante',
                name:'Representante',
                allowBlank: false,
                readOnly : true
            }
            ]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Aceptar',
            formBind: true,
            handler: function() {
                updatecredito_form.getForm().submit({
                    success: function(f,a){
                        updatecredito_form.getForm().reset();
                        win.hide();
                        clienteDataStore.reload();
                        propiedadDataStore.reload();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Warning','Ha ocurrido un error y <br /> no es posible autorizar los datos');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                win.hide();
            }
        }]

    });

    var deletecredito_form = new Ext.form.FormPanel({
        url: '/administrador/index.php/autorizar/eliminacreditos',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 350,
        height: 150,
        labelWidth: 130,
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'ID Pedido',
                name: 'CodigoCredito',
                allowBlank: false,
                readOnly : true,
                vtype: 'numbersonly',
                maxLength: 11
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Cliente',
                name:'Cliente',
                allowBlank: false,
                readOnly : true
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Representante',
                name:'Representante',
                allowBlank: false,
                readOnly : true
            }
            ]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Aceptar',
            formBind: true,
            handler: function() {
                deletecredito_form.getForm().submit({
                    success: function(f,a){
                        deletecredito_form.getForm().reset();
                        wineliminar.hide();
                        clienteDataStore.reload();
                        propiedadDataStore.reload();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Warning','Ha ocurrido un error y <br /> no es posible eliminar los datos');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                wineliminar.hide();
            }
        }]

    });

    var win = new Ext.Window({
        title: "Autorizar Pedido",
        applyTo: 'autorizaP',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: updatecredito_form
    });

    var wineliminar = new Ext.Window({
        title: "Eliminar Pedido",
        applyTo: 'eliminarP',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: deletecredito_form
    });

    function updateCredito () {
        var sm = dtgridCliente.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            updatecredito_form.getForm().findField('CodigoCredito').setValue(sel.data.CodigoCredito);
            updatecredito_form.getForm().findField('Cliente').setValue(sel.data.NombreCliente);
            updatecredito_form.getForm().findField('Representante').setValue(sel.data.NombreRepresenta);
            win.show();
        } else {
            Ext.Msg.alert('Alerta', 'Seleccione un pedido para autorizarlo');
        }
    }

    function deleteCredito () {
        var sm = dtgridCliente.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            deletecredito_form.getForm().findField('CodigoCredito').setValue(sel.data.CodigoCredito);
            deletecredito_form.getForm().findField('Cliente').setValue(sel.data.NombreCliente);
            deletecredito_form.getForm().findField('Representante').setValue(sel.data.NombreRepresenta);
            wineliminar.show();
        } else {
            Ext.Msg.alert('Alerta', 'Seleccione un pedido para eliminarlo');
        }
    }

    function verDetalle () {
        var sm = dtgridCliente.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            getpropiedad(sel.data.CodigoCredito);
        } else {
            Ext.Msg.alert('Alerta', 'Seleccione un pedido para ver su detalle');
        }
    }

    var cretoolbar = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Detalle del Pedido',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/add.png',
            handler: verDetalle
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'tbbutton',
            text: 'Autorizar Pedido',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/saved.png',
            handler: updateCredito
        },
        {
            xtype: 'tbseparator'
        },{
            xtype: 'tbbutton',
            text: 'Eliminar Pedido',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/erase.png',
            handler: deleteCredito
        },
        {
            xtype: 'tbseparator'
        }
        ]
    });

    var clienteDataStore = new Ext.data.Store({
        url: '/administrador/index.php/autorizar/getcreditos',
        reader: new Ext.data.JsonReader({
            root: 'rows',
            totalProperty: 'results',
            id: 'CodigoCredito'
        }, [
        {
            name: 'CodigoCredito',
            type: 'int'
        },
        {
            name: 'NombreRepresenta',
            type: 'string'
        },
        {
            name: 'NombreCliente',
            type: 'string'
        },
        {
            name: 'FechaPedido',
            type: 'date',
            dateFormat: 'Y-m-d'
        },
        {
            name: 'Cantidad',
            type: 'int'
        },
        {
            name: 'MontoCompra'
        },
        {
            name: 'CreditoAsignado',
            type: 'int'
        }
        ])
        ,
        remoteSort:true
        ,
        sortInfo: {
            field: 'CodigoCredito',
            direction: 'DESC'
        }
    });

    var propiedadDataStore = new Ext.data.Store({
        url: '/administrador/index.php/autorizar/getpropiedades',
        reader: new Ext.data.JsonReader({
            root: 'rows',
            totalProperty: 'results',
            id: 'CodigoPropiedad'
        }, [
        {
            name: 'CodigoPropiedad',
            type: 'int'
        },
        {
            name: 'NombrePropiedad',
            type: 'string'
        },
        {
            name: 'Pais',
            type: 'string'
        },
        {
            name: 'Ciudad',
            type: 'string'
        },
        {
            name: 'Cantidad',
            type: 'int'
        }
        ])
        ,
        remoteSort:true
        ,
        sortInfo: {
            field: 'CodigoPropiedad',
            direction: 'DESC'
        }
    });

    var crepagingBar = new Ext.PagingToolbar({
        pageSize:30,
        store: clienteDataStore,
        displayInfo: true,
        emptyMsg: "Sin Registros a desplegar",
        displayMsg: 'Registros {0} - {1} de {2}'

    });

     var propiedadpagingBar = new Ext.PagingToolbar({
        pageSize:30,
        store: propiedadDataStore,
        displayInfo: true,
        emptyMsg: "Sin Registros a desplegar",
        displayMsg: 'Registros {0} - {1} de {2}'

    });

    var clienteCM = [
    {
        header: 'ID',
        dataIndex: 'CodigoCredito',
        sortable: true
    },
     {
        header: 'Representante',
        dataIndex: 'NombreRepresenta',
        sortable: true
    },
     {
        header: 'Cliente',
        dataIndex: 'NombreCliente',
        sortable: true
    },
    {
        header: 'Fecha Pedido',
        dataIndex: 'FechaPedido',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    },
    {
        header: 'Cr�ditos',
        dataIndex: 'Cantidad',
        sortable: true
    },
    {
        header: 'Monto U$',
        dataIndex: 'MontoCompra',
        sortable: true
    },
    {
        header: 'Credito Asignado',
        dataIndex: 'CreditoAsignado',
        sortable: true
    }
    ];

    var propiedadCM = [
    {
        header: 'ID',
        dataIndex: 'CodigoPropiedad',
        sortable: true
    },
    {
        header: 'Propiedad',
        dataIndex: 'NombrePropiedad',
        sortable: true
    },
    {
        header: 'Pa�s',
        dataIndex: 'Pais',
        sortable: true
    },
    {
        header: 'Ciudad',
        dataIndex: 'Ciudad',
        sortable: true
    },
    {
        header: 'Cr�ditos',
        dataIndex: 'Cantidad',
        sortable: true
    }
    ];

    var dtgridCliente = new Ext.grid.GridPanel({
        store: clienteDataStore,
        columns: clienteCM,
        frame: true,
        loadMask: {
            msg: 'Cargando datos, por favor espere ....'
        },
        viewConfig: {
            forceFit:true,
            emptyText: 'No hay resultados.'
        },
        renderTo: 'creclientes_grid',
        stripeRows: true,
        height: 300,
        selModel: new Ext.grid.RowSelectionModel({
            singleSelect: true
        }),
        autoWidth: true,
        collapsed: false,
        title: 'Pedidos Pendientes',
        tbar: cretoolbar,
        bbar: crepagingBar,
        iconCls: 'creditos_panel'
    });

    var dtgridPropiedad = new Ext.grid.GridPanel({
        store: propiedadDataStore,
        columns: propiedadCM,
        frame: true,
        loadMask: {
            msg: 'Cargando datos, por favor espere ....'
        },
        viewConfig: {
            forceFit:true,
            emptyText: 'No hay resultados.'
        },
        renderTo: 'propiedades_grid',
        stripeRows: true,
        height: 300,
        selModel: new Ext.grid.RowSelectionModel({
            singleSelect: true
        }),
        autoWidth: true,
        collapsed: false,
        title: 'Detalle del Pedido',
        bbar: propiedadpagingBar,
        iconCls: 'creditos_panel'
    });

    clienteDataStore.load({
        params:{
            start:0,
            limit:30
        }
    });

     propiedadDataStore.load({
        params:{
            start:0,
            limit:30
        }
    });
});