/*
 * Grid para el listado de aprobaci�n de cr�ditos del cliente
 *
 */
Ext.onReady(function(){
    Ext.BLANK_IMAGE_URL = '/administrador/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = "side";

    Ext.apply(Ext.form.VTypes, {
        daterange : function(val, field) {
            var date = field.parseDate(val);

            if(!date){
                return;
            }
            if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                var start = Ext.getCmp(field.startDateField);
                start.setMaxValue(date);
                start.validate();
                this.dateRangeMax = date;
            }
            else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                var end = Ext.getCmp(field.endDateField);
                end.setMinValue(date);
                end.validate();
                this.dateRangeMin = date;
            }
            /*
         * Always return true since we're only using this vtype to set the
         * min/max allowed values (these are tested for after the vtype test)
         */
            return true;
        },
        numbersonly: function(val, field) {
            var n = parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        },
        numbersonlyText: 'Ingresa solo numeros aqui'
    });

    /* var b = Ext.get('crebt');
    b.on('click', function(){
        crebuscar();
    })

    var c = Ext.get('crers');
    c.on('click', function(){
        Ext.getDom('hb').reset();
        return false;
    })*/

    //var search_button = Ext.get('search_button');
    function isEmpty(inputStr) {
        if (null == inputStr || "" == inputStr) {
            return true;
        }
        return false;
    }

    Ext.namespace('Ext.userdata');
    Ext.userdata.status = [['0', 'inactiva'], ['1', 'activa']];
    var stStore = new Ext.data.SimpleStore({
        fields: ['status', 'label'],
        data: Ext.userdata.status
    });

    var autorizarsolicitud_form = new Ext.form.FormPanel({
        url: '/administrador/index.php/carga/autorizasolicitud',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 400,
        height: 150,
        labelWidth: 130,
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'ID Cliente',
                name: 'CodigoCliente',
                allowBlank: false,
                readOnly : true,
                vtype: 'numbersonly',
                maxLength: 11
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Cliente',
                name:'Cliente',
                allowBlank: false,
                readOnly : true,
                maxLength: 100
            },{
                xtype: 'textfield',
                fieldLabel: 'Email',
                name:'Email',
                allowBlank: false,
                readOnly : true,
                maxLength: 100
            }
            ]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                autorizarsolicitud_form.getForm().submit({
                    success: function(f,a){
                        autorizarsolicitud_form.getForm().reset();
                        winautorizar.hide();
                        clienteDataStore.reload();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Warning','Ha ocurrido un error y <br /> no es posible autorizar la solicitud');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                winautorizar.hide();
            }
        }]

    });

    var revocarsolicitud_form = new Ext.form.FormPanel({
        url: '/administrador/index.php/carga/revocasolicitud',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 350,
        height: 150,
        labelWidth: 130,
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'ID Cliente',
                name: 'CodigoCliente',
                allowBlank: false,
                readOnly : true,
                vtype: 'numbersonly',
                maxLength: 11
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Cliente',
                name:'Cliente',
                allowBlank: false,
                readOnly : true
            }
            ]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                revocarsolicitud_form.getForm().submit({
                    success: function(f,a){
                        revocarsolicitud_form.getForm().reset();
                        winrevocar.hide();
                        clienteDataStore.reload();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Warning','Ha ocurrido un error y <br /> no es posible revocar la solicitud');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                winrevocar.hide();
            }
        }]

    });

    var winautorizar = new Ext.Window({
        title: "Autorizar Solicitud",
        applyTo: 'autorizaS',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: autorizarsolicitud_form
    });

    var winrevocar = new Ext.Window({
        title: "Revocar Solicitud",
        applyTo: 'revocarS',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: revocarsolicitud_form
    });

    function autorizarSolicitud () {
        var sm = dtgridCliente.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            if(sel.data.Estado=='Autorizado'){
                Ext.Msg.alert('La solicitud est� autorizada');
            }
            else{
                autorizarsolicitud_form.getForm().findField('CodigoCliente').setValue(sel.data.CodigoCliente);
                autorizarsolicitud_form.getForm().findField('Cliente').setValue(sel.data.NombreCliente);
                autorizarsolicitud_form.getForm().findField('Email').setValue(sel.data.EMail);
                winautorizar.show();
            }
        } else {
            Ext.Msg.alert('Alerta', 'Seleccione una solicitud para autorizarla');
        }
    }

    function revocarSolicitud() {
        var sm = dtgridCliente.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            if(sel.data.Estado=='Revocado'){
                Ext.Msg.alert('La solicitud est� revocada');
            }
            else{
                revocarsolicitud_form.getForm().findField('CodigoCliente').setValue(sel.data.CodigoCliente);
                revocarsolicitud_form.getForm().findField('Cliente').setValue(sel.data.NombreCliente);
                winrevocar.show();
            }
        } else {
            Ext.Msg.alert('Alerta', 'Seleccione una solicitud para revocarla');
        }
    }

    var cretoolbar = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Autorizar',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/activa.png',
            handler: autorizarSolicitud
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'tbbutton',
            text: 'Revocar',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/inactiva.png',
            handler: revocarSolicitud
        },
        {
            xtype: 'tbseparator'
        }
        ]
    });

    var clienteDataStore = new Ext.data.Store({
        url: '/administrador/index.php/carga/getcarga',
        reader: new Ext.data.JsonReader({
            root: 'rows',
            totalProperty: 'results',
            id: 'CodigoCliente'
        }, [
        {
            name: 'CodigoCliente',
            type: 'int'
        },
        {
            name: 'NombreCliente',
            type: 'string'
        },
        {
            name: 'TelefonoCliente',
            type: 'string'
        },
        {
            name: 'EMail',
            type: 'string'
        },
        {
            name: 'Estado',
            type: 'string'
        },
        {
            name: 'FechaSolicitaCarga',
            type: 'date',
            dateFormat: 'Y-m-d'
        }
        ,
        {
            name: 'FechaOtorgaCarga',
            type: 'date',
            dateFormat: 'Y-m-d'
        },
        {
            name: 'FechaRevocaCarga',
            type: 'date',
            dateFormat: 'Y-m-d'
        }
        ])
        ,
        remoteSort:true
        ,
        sortInfo: {
            field: 'FechaSolicitaCarga',
            direction: 'ASC'
        }
    });

    var crepagingBar = new Ext.PagingToolbar({
        pageSize:30,
        store: clienteDataStore,
        displayInfo: true,
        emptyMsg: "Sin Registros a desplegar",
        displayMsg: 'Registros {0} - {1} de {2}'

    });

    var clienteCM = [
    {
        header: 'ID',
        dataIndex: 'CodigoCliente',
        sortable: true
    },
    {
        header: 'Cliente',
        dataIndex: 'NombreCliente',
        sortable: true
    },
    {
        header: 'Tel�fono',
        dataIndex: 'TelefonoCliente',
        sortable: true
    },
    {
        header: 'Email',
        dataIndex: 'EMail',
        sortable: true
    },
    {
        header: 'Estado',
        dataIndex: 'Estado',
        sortable: true
    },
    {
        header: 'F. Solicita',
        dataIndex: 'FechaSolicitaCarga',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    },
    {
        header: 'F. Autoriza',
        dataIndex: 'FechaOtorgaCarga',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    },
    {
        header: 'F. Revoca',
        dataIndex: 'FechaRevocaCarga',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    }
    ];

    var dtgridCliente = new Ext.grid.GridPanel({
        store: clienteDataStore,
        columns: clienteCM,
        frame: true,
        loadMask: {
            msg: 'Cargando datos, por favor espere ....'
        },
        viewConfig: {
            forceFit:true,
            emptyText: 'No hay resultados.'
        },
        renderTo: 'carga_grid',
        stripeRows: true,
        height: 500,
        selModel: new Ext.grid.RowSelectionModel({
            singleSelect: true
        }),
        autoWidth: true,
        collapsed: false,
        title: 'Autorizar/Revocar Carga Masiva ',
        tbar: cretoolbar,
        bbar: crepagingBar,
        iconCls: 'creditos_panel'
    });

    clienteDataStore.load({
        params:{
            start:0,
            limit:50
        }
    });
    
});