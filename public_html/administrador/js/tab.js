/*
 * Grid para el listado de aprobaci�n del Tab en Fan Pages
 * 
 */
Ext.onReady(function(){
     
    Ext.BLANK_IMAGE_URL = '/administrador/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = "side";

    Ext.apply(Ext.form.VTypes, {
        daterange : function(val, field) {
            var date = field.parseDate(val);

            if(!date){
                return;
            }
            if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                var start = Ext.getCmp(field.startDateField);
                start.setMaxValue(date);
                start.validate();
                this.dateRangeMax = date;
            }
            else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                var end = Ext.getCmp(field.endDateField);
                end.setMinValue(date);
                end.validate();
                this.dateRangeMin = date;
            }
            /*
         * Always return true since we're only using this vtype to set the
         * min/max allowed values (these are tested for after the vtype test)
         */
            return true;
        },
        numbersonly: function(val, field) {
            var n = parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        },
        numbersonlyText: 'Ingresa s�lo n�meros aqu�'
    }); // End of Ext.apply(Ext.form.VTypes, function {})
    
    function isEmpty(inputStr) {
    	if (null == inputStr || "" == inputStr) {
            return true;
        }
        return false;
    }

    Ext.namespace('Ext.userdata');
    Ext.userdata.status = [['0', 'inactiva'], ['1', 'activa']];    
    var stStore = new Ext.data.SimpleStore({
        fields: ['status', 'label'],
        data: Ext.userdata.status
    });

    var autorizarsolicitud_form = new Ext.form.FormPanel({
        url: '/administrador/index.php/tab/autorizarsolicitudtab',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 400,
        height: 150,
        labelWidth: 130,
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'P&aacute;gina ID',
                name: 'pageId',
                allowBlank: false,
                readOnly : true,
                vtype: 'numbersonly',
                maxLength: 18
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Nombre',
                name:'name',
                allowBlank: false,
                readOnly : true,
                maxLength: 100
            }
            ]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                autorizarsolicitud_form.getForm().submit({
                    success: function(f,a){
                        autorizarsolicitud_form.getForm().reset();
                        autorizarWindow.hide();
                        clienteDataStore.reload();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Error','Ha ocurrido un error y <br /> no es posible autorizar la solicitud.');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                autorizarWindow.hide();
            }
        }]

    });

    var revocarsolicitud_form = new Ext.form.FormPanel({
        url: '/administrador/index.php/tab/revocarsolicitudtab',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 350,
        height: 150,
        labelWidth: 130,
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'P&aacute;gina ID',
                name: 'pageId',
                allowBlank: false,
                readOnly : true,
                vtype: 'numbersonly',
                maxLength: 18
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Nombre',
                name:'name',
                allowBlank: false,
                readOnly : true,
                maxLength: 100
            }
            ]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                revocarsolicitud_form.getForm().submit({
                    success: function(f,a){
                        revocarsolicitud_form.getForm().reset();
                        revocarWindow.hide();
                        clienteDataStore.reload();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Error','Ha ocurrido un error y <br /> no es posible revocar la solicitud.');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                revocarWindow.hide();
            }
        }]

    });

    var autorizarWindow = new Ext.Window({
        title: "Autorizar Solicitud",
        applyTo: 'autorizaS',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: autorizarsolicitud_form
    });

    var revocarWindow = new Ext.Window({
        title: "Revocar Solicitud",
        applyTo: 'revocarS',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: revocarsolicitud_form
    });

    function autorizarSolicitud () {
        var sm = dtgridTab.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            if(sel.data.serviceStatus === 1){
                Ext.Msg.alert('Alerta', 'El tab est&aacute; activo.');
            }
            else{
                autorizarsolicitud_form.getForm().findField('pageId').setValue(sel.data.pageId);
                autorizarsolicitud_form.getForm().findField('name').setValue(sel.data.name);
                autorizarWindow.show();
            }
        } else {
            Ext.Msg.alert('Alerta', 'Seleccione una solicitud para autorizarla.');
        }
    }

    function revocarSolicitud() {
        var sm = dtgridTab.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            if(sel.data.serviceStatus === 0){
                Ext.Msg.alert('Alerta', 'El tab est&aacute; inactivo.');
            }
            else{
                revocarsolicitud_form.getForm().findField('pageId').setValue(sel.data.pageId);
                revocarsolicitud_form.getForm().findField('name').setValue(sel.data.name);
                revocarWindow.show();
            }
        } else {
            Ext.Msg.alert('Alerta', 'Seleccione una solicitud para revocarla.');
        }
    }

    var cretoolbar = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Autorizar',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/activa.png',
            handler: autorizarSolicitud
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'tbbutton',
            text: 'Revocar',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/inactiva.png',
            handler: revocarSolicitud
        },
        {
            xtype: 'tbseparator'
        }
        ]
    });

    var clienteDataStore = new Ext.data.Store({
        url: '/administrador/index.php/tab/gettab',
        reader: new Ext.data.JsonReader({
            root: 'rows',
            totalProperty: 'results',
            id: 'id'
        }, [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'pageId',
            type: 'int'
        },
        {
            name: 'name',
            type: 'string'
        },
        {
            name: 'serviceStatus',
            type: 'int'
        },
        {
            name: 'serviceStartDate',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            name: 'serviceExpirationDate',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            name: 'requestDate',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        }
        ])
        ,
        remoteSort:true
        ,
        sortInfo: { 
            field: 'requestDate',
            direction: 'DESC'
        }
    });
    
    var crepagingBar = new Ext.PagingToolbar({
        pageSize:50,
        store: clienteDataStore,
        displayInfo: true,
        emptyMsg: "Sin registros a desplegar",
        displayMsg: 'Registros {0} - {1} de {2}'
    });

    var tabFields = [
    {
        header: 'P&aacute;gina ID',
        dataIndex: 'pageId',
        sortable: true
    },{
        header: 'Nombre',
        dataIndex: 'name',
        sortable: true
    },
    {
        header: 'Estado',
        dataIndex: 'serviceStatus',
        renderer: serviveStatusString,
        sortable: true
    },
    {
        header: 'Fecha Inicio',
        dataIndex: 'serviceStartDate',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    },
    {
        header: 'Fecha Vence',
        dataIndex: 'serviceExpirationDate',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    },
    {
        header: 'Fecha Solicitud',
        dataIndex: 'requestDate',
        renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        sortable: true
    }
    ];
    
    function serviveStatusString(val) {
        switch(val) {
            case 0:
                return 'Inactiva';
                break;
            case 1:
                return 'Activa';
                break;
        }            
    }
    
    var dtgridTab = new Ext.grid.GridPanel({
        store: clienteDataStore,
        columns: tabFields,
        frame: true,
        loadMask: {
            msg: 'Cargando datos, por favor espere...'
        },
        viewConfig: {
            forceFit:true,
            emptyText: 'No hay resultados.'
        },
        renderTo: 'tab_grid',
        stripeRows: true,
        height: 500,
        selModel: new Ext.grid.RowSelectionModel({
            singleSelect: true
        }),
        autoWidth: true,
        collapsed: false,
        title: 'Autorizar/Revocar Tab en Fan Page',
        tbar: cretoolbar,
        bbar: crepagingBar,
        iconCls: 'creditos_panel'
    });

    clienteDataStore.load({
        params:{
            start:0,
            limit:50
        }
    });
    
});