Ext.onReady(function() {

    Ext.BLANK_IMAGE_URL = '/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();

//
//    var propDataStore = new Ext.data.Store({
//        url: '/creditos/getregistersfilter',
//        //url: '/index/getmarks' + '?' + fb,
//      //  baseParams: fb,
//        //autoLoad: true,
//        reader: new Ext.data.JsonReader({
//            root: 'rows',
//            id: 'idpropiedad'
//        },
//        [
//        {
//            name: 'idpropiedad'
//        },
//
//        {
//            name: 'nombre'
//        },
//
//        {
//            name: 'descripcion'
//        },
//
//        {
//            name: 'direccion'
//        },
//
//        {
//            name: 'pais'
//        },
//
//        {
//            name: 'estado'
//        },
//
//        {
//            name: 'ciudad'
//        },
//
//        {
//            name: 'zip_code'
//        },
//
//        {
//            name: 'precio_venta'
//        },
//
//        {
//            name: 'precio_alquiler'
//        },
//
//        {
//            name: 'tipo_enlistamiento'
//        },
//
//        {
//            name: 'fecha_ingreso'
//        },
//
//        {
//            name: 'ultima_modificacion'
//        },
//
//        {
//            name: 'tags'
//        },
//
//        {
//            name: 'area'
//        },
//
//        {
//            name: 'cuartos'
//        },
//
//        {
//            name: 'banos'
//        },
//
//        {
//            name: 'tamano_lote'
//        },
//
//        {
//            name: 'destacada'
//        },
//
//        {
//            name: 'roomate'
//        },
//
//        {
//            name: 'estatus'
//        },
//
//        {
//            name: 'tipo_propiedad'
//        },
//
//        {
//            name: 'lat'
//        },
//
//        {
//            name: 'lng'
//        },
//
//        {
//            name: 'foto'
//        },
//        {
//            name: 'creditos'
//        },
//
//        {
//            name: 'fecha_asignacion_credito'
//        },
//
//        {
//            name: 'fecha_expiracion_credito'
//        },
//
//
//        {
//            name: 'uid'
//        }]),
//        sortInfo: {
//            field: 'idpropiedad',
//            direction: 'ASC'
//        }
//    });

//    var propCM = [{
//        header: 'ID',
//        dataIndex: 'idpropiedad',
//        sortable: true
//    },
//    {
//        header: 'Area',
//        dataIndex: 'area',
//        sortable: true
//    },
//    {
//        header: 'Precio',
//        dataIndex: 'precio_venta',
//        sortable: true
//    },
//    {
//        header: 'Habitaciones',
//        dataIndex: 'cuartos',
//        sortable: true
//    },
//    {
//        header: 'Banos',
//        dataIndex: 'banos',
//        sortable: true
//    }];
//
//    var gridProp = new Ext.grid.GridPanel({
//        store: propDataStore,
//        columns: propCM,
//        frame: false,
//        loadMask: {
//            msg: 'Loading ...'
//        },
//        viewConfig: {
//            forceFit: true,
//            emptyText: 'Datos no disponibles <br /> No hay propiedades registradas en esta locacion.'
//        },
//        stripeRows: true,
//        height: 200,
//        autoWidth: true,
//        collapsible: true,
//        collapsed: false,
//        title: 'Resultados'
//
//    });
//    gridProp.render('data');

    var city;
    var cc;
    var x = '';

    /*  Formulario de Busqueda   */

    Ext.apply(Ext.form.VTypes, {
        numbersonly: function(val, field) {
            var n = parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        },
        numbersonlyText: 'Ingresa solo numeros aqui'
    //minmax: function(val, field) {}
    });

    /*
    Ext.QuickTips.register({
 target: 'buscar',
 title: 'Ayuda',
 text: 'Ingresa una locacion o direccion, incluyendo el pais, Ej. Villa Fontana, Nicaragua',
 width: 100,
 dismissDelay: 20
    });

    */

    new Ext.ToolTip({
        target: dir,
        bodyStyle: 'padding: 5px 10px;',
        trackMouse:true,
        html: 'Ingresa una locacion o direccion, incluyendo el pais, Ej. Villa Fontana, Nicaragua'
    });

    /* fin formulario de busqueda */
 
        var dir = Ext.get('dir');
        var cat = Ext.get('cat');
        var rent = Ext.get('rent');
        var buy = Ext.get('buy');
        var min_price = Ext.get('min_price');
        var max_price = Ext.get('max_price');
        var beds = Ext.get('beds');
        var baths = Ext.get('baths');
        var area = Ext.get('area');

        function isNum(num) {
            var test = false;
            if(Ext.isEmpty(num)){
                test = true;
            } else {
                test = Ext.num(num, false);
                //var tx = (test >= 0) ? true : false;
                //console.info('dato es ' + typeof(test));
                //console.info('test = '+  tx);
                if(test < 0){
                    test = false;
                }

            }
            return test;
        }

        min_price.on('blur', function(){
            if(isNum(this.getValue())){
                //console.info('ok: min_price = ' +this.getValue());
                if(this.hasClass('error')) {
                    this.removeClass('error');
                }
            } else {
                //console.info('fail: min_price = ' +this.getValue());
                this.addClass('error');
            }
        });

        max_price.on('blur', function(){
            if(isNum(this.getValue())){
                //console.info('ok: max_price = ' +this.getValue());
                if(this.hasClass('error')) {
                    this.removeClass('error');
                }
            } else {
                //console.info('fail: max_price = ' +this.getValue());
                this.addClass('error');
            }
        });

        dir.on('blur', function(){
            if( Ext.isEmpty( this.getValue()) ) {
                this.addClass('error');

            //console.info('fail: sin direccion ');
            } else {
                if(this.hasClass('error')) {
                    this.removeClass('error');
                }
            //console.info('ok: - direccion ingresada: ' + this.getValue());
            }
        });

        cat.on('change', function(){
            var tipo_prop = this.getValue();
            var rooms = Ext.getDom('beds');
            var banos = Ext.getDom('baths');
            switch (tipo_prop) {
                case 'CB':
                case 'EB':
                case 'EO':
                    rooms.setAttribute("disabled","disabled");
                    banos.removeAttribute('disabled');
                    break;
                case 'TE':
                    rooms.setAttribute("disabled","disabled");
                    banos.setAttribute("disabled","disabled");
                    break;
                default:
                    rooms.removeAttribute('disabled');
                    banos.removeAttribute('disabled');
            }
        });


        /***  fin formulario busqued ***/
        // funcion para recarga dinamica del grid.
        //function getProps(city, cc) {
        function getProps(cc, city, dir1, tipo1, min_price1, max_price1, beds1, baths1, area1, cat1) {
            propDataStore.load({
                params: {
                    'pais': cc,
                    'ciudad': city,
                    'dir': dir1,
                    'tipo': tipo1,
                    'min_price': min_price1,
                    'max_price': max_price1,
                    'beds': beds1,
                    'baths': baths1,
                    'area': area1,
                    'cat': cat1
                }
            });
        }

        var b = Ext.get('bt');
        b.on('click', function(){
            buscar();
        })


        //var search_button = Ext.get('search_button');

        function isEmpty(inputStr) {
            if (null == inputStr || "" == inputStr) {
                return true;
            }
            return false;
        }

        function check_id(val) {
            var n = parseInt(val);
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(n);
        }

        //var search_button1 = Ext.get('search_button1');
        function buscar() {
            // var query = Ext.get('buscar').getValue();
            // console.log('consulta: ' + query);
 
            //var data = form_busqueda.getForm().getValues();
            var dataurl = Ext.Ajax.serializeForm('hb');
            var data = Ext.urlDecode(dataurl);
            //console.dir(data);
            var dir1 = data.dir;
            var tipo1 = data.tipo;
            var min_price1 = data.min_price;
            var max_price1 = data.max_price;
            var beds1 = data.beds;
            var baths1 = data.baths;
            var area1 = data.area;
            var cat1 = data.cat;

            //var query;
            var cc="";
            var city="";
            if (!isEmpty(dir1)) {
  
                if(check_id(dir1)){
                    //console.log('query es un id de propiedad ' + dir);
                    getProps(cc, city, dir1, tipo1, min_price1, max_price1, beds1, baths1, area1, cat1);
                } else {
                  Ext.MessageBox.alert('Alerta', 'Dirreccion no existe');
                }
            } else {
                Ext.MessageBox.alert('Alerta', 'Ingrese un Id de propiedad');
            }
        }


     
 
        //search_button.on('click', buscar);
        var nav = new Ext.KeyNav(Ext.getDom('dir'), {
            'enter': buscar,
            'scope': this
        });

        // google info window
//        var descripcion;
//        var precio;
//        var pais;
//        var banos;
//        var cuartos;
//        var estatus;
//        var id;
//        var area;
//        var latitud;
//        var longitud;
//        var point;
//        var foto;
//        var tmpl;
//        var marker;
        //map.clearOverlays(); // limpiar mapa, antes de cargar marcadores.
        // s -> store, r -> record
//        propDataStore.on('load',
//            function(s, r) {
//                map.clearOverlays();
//                Ext.each(r,
//                    function(rec) {
//                        descripcion = rec.data.descripcion;
//                        precio = rec.data.precio_venta;
//                        banos = rec.data.banos;
//                        cuartos = rec.data.cuartos;
//                        pais = rec.data.pais;
//                        id = rec.data.idpropiedad;
//                        area = rec.data.area;
//                        foto = rec.data.foto;
//
//                        switch (rec.data.estatus) {
//                            case '1':
//                                estatus = 'Vendida';
//                                break;
//                            case '2':
//                                estatus = 'Alquilada';
//                                break;
//                            case '3':
//                                estatus = 'Activa';
//                                break;
//                            case '4':
//                                estatus = 'Inactiva';
//                                break;
//                        }
//
//                    });
//            });
});