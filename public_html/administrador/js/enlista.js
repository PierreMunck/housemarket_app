/*
 * Grid para el listado de categorias
 *
 */
Ext.onReady(function(){
    // @todo cambiar url en produccion
    Ext.BLANK_IMAGE_URL = '/administrador/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = "side";

    var lystingForm = new Ext.form.FormPanel({
        url: '/administrador/index.php/enlistamientos/add',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 350,
        height: 300,
        labelWidth: 130,
        //labelAlign: 'right',
        items: [{
            border: false,
            layout: 'form',
            items: [{
                xtype: 'textfield',
                fieldLabel: 'Enlistamiento',
                name: 'enlistamiento',
                allowBlank: false,
                maxLength: 64
            }]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                lystingForm.getForm().submit({
                    success: function(f,a){
                        lystingForm.getForm().reset();
                        win.hide();
                        lystingDataStore.load();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Warning', 'Ha ocurrido un error <br /> no es posible guardar los datos');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                lystingForm.getForm().reset();
                win.hide();

            }
        }]
    });

    var edit_form = new Ext.form.FormPanel({
        url: '/administrador/index.php/enlistamientos/edit',
        reader : new Ext.data.JsonReader(
        [{
            name:"NombreEnlista"
        }
        ]),
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 350,
        height: 300,
        labelWidth: 130,
        //labelAlign: 'right',
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Enlistamiento',
                name: 'enlistamiento',
                allowBlank: false,
                maxLength: 20,
                vtype:'Alpha'
            },
            {
                xtype: 'hidden',
                name: 'id'
            }
            ]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                edit_form.getForm().submit({
                    success: function(f,a){
                        edit_form.getForm().reset();
                        win_edit.hide();
                        lystingDataStore.load();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Warning', 'Ha ocurrido un error y <br /> no es posible guardar los datos');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                edit_form.getForm().reset();
                win_edit.hide();

            }
        }]

    });

    var win = new Ext.Window({
        title: "Agregar Nuevo Enlistamiento",
        applyTo: 'ventana',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: lystingForm
    });

    var win_edit = new Ext.Window({
        title: 'Editar Enlistamiento',
        applyTo: 'ventana1',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: edit_form
    });

    function addLysting() {
        win.show();
    }

    function editLysting() {
        var sm = gridLystings.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            edit_form.getForm().findField('enlistamiento').setValue(sel.data.enlistamiento);
            edit_form.getForm().findField('id').setValue(sel.data.codigoenlista);
            win_edit.show();
        } else {
            Ext.Msg.alert('Alerta', 'Debe Seleccionar un mensaje para poder realizar esta operacion')
        }
    }

    function delCategory(){
        var sm = gridLystings.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            //console.dir(sel.data);
            var id_lysting = sel.data.codigoenlista;
            Ext.Msg.show({
                title: 'Eliminar Enlistamiento',
                msg: 'Esta seguro que desea eliminar <br /> esta categoria ' + sel.data.enlistamiento + ' ?',
                //buttons: Ext.MessageBox.OKCANCEL,
                buttons: {
                    ok: true,
                    cancel: true
                },
                fn: function(btn) {
                    switch(btn){
                        case 'ok':
                            Ext.Ajax.request({
                                url: '/administrador/index.php/enlistamientos/delete',
                                params: {
                                    'id': id_lysting
                                },
                                success: function () {
                                    lystingDataStore.load();
                                },
                                failure: function () {
                                    Ext.msg.alert('Error', 'Ha ocurrido un error <br /> la categoria no pudo ser eliminada');
                                }
                            });
                            //Ext.Msg.alert('kill this dude', btn);
                            break;
                        case 'cancel':
                            //Ext.Msg.alert('wtf, no elimination', btn);
                            break;
                    }

                },
                icon: Ext.MessageBox.WARNING
            });

        } else {
            Ext.Msg.alert('Error', 'Debe Seleccionar una categoria para poder realizar esta operacion')
        }
    }

    function status_image(val){
        return '<div class="' + val +'">' + val +'</div>'
    }

    var toolbar = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Agregar enlistamiento',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/add.png',
            handler: addLysting
        },
        {
            xtype: 'tbseparator'
        },

        {
            xtype: 'tbbutton',
            text: 'Editar enlistamiento',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/edit_profile.png',
            handler: editLysting
        },
        {
            xtype: 'tbseparator'
        },

        {
            xtype: 'tbbutton',
            text: 'Eliminar enlistamiento',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/erase.png',
            handler: delLysting
        }
        ]
    });

    var lystingDataStore = new Ext.data.Store({
        // @todo cambiar url en produccion
        url: '/administrador/index.php/enlistamientos/getLysting',
        reader: new Ext.data.JsonReader({
            root: 'rows',
            id: 'codigoenlista'
        }, [{
            name: 'codigoenlista'
        },{
            name: 'enlistamiento'
        }
        ]),
        sortInfo: {
            field: 'codigoenlista',
            direction: 'ASC'
        }
    });

    lystingDataStore.load();

    var lystingColumnModel = [
    {
        header: 'Id',
        dataIndex: 'codigoenlista',
        sortable: true
    },
    {
        header: 'Enlistamiento',
        dataIndex: 'enlistamiento',
        sortable: true
    }
    ];

    var gridLystings = new Ext.grid.GridPanel({
        store: lystingDataStore,
        columns: lystingColumnModel,
        frame: true,
        loadMask: {
            msg: 'Cargando datos, por favor espere ....'
        },
        viewConfig: {
            forceFit:true
        },
        renderTo: 'lysting_grid',
        stripeRows: true,
        height: 350,
        autoWidth: 'true',
        title: 'Administracion de Enlistamientos',
        tbar: toolbar,
        iconCls: 'useradmin_panel'
    });

});