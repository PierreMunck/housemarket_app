/* 
 * Grid para el listado de usuarios
 *
 */
Ext.onReady(function(){

    Ext.BLANK_IMAGE_URL = '/administrador/js/extjs/resources/images/gray/s.gif';
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = "side";

    var tipoStore = new Ext.data.SimpleStore({
        fields: ['tipousuario'],
        data: [['Administrador'],['Representante']]
    });

    Ext.apply(Ext.form.VTypes, {
        password : function(val, field) {
            if (field.initialPassField) {
                var pwd = Ext.getCmp(field.initialPassField);
                return (val == pwd.getValue());
            }
            return true;
        },
        passwordText : 'Passwords no coinciden'
    });

    var userform = new Ext.form.FormPanel({
        url: '/administrador/index.php/useradmin/add',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 350,
        height: 300,
        labelWidth: 130,
        //labelAlign: 'right',
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Username',
                name: 'username',
                allowBlank: false,
                vtype: 'alphanum',
                maxLength: 20
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Password',
                inputType: 'password',
                name:'password',
                allowBlank: false,
                maxLength: 20,
                id: 'password'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Confirmar Password',
                inputType: 'password',
                name: 'password_confirm',
                allowBlank: false,
                maxLength: 20,
                vtype: 'password',
                initialPassField: 'password'
            },
            {
                xtype: 'combo',
                fieldLabel: 'Tipo de Usuario',
                width: 170,
                store: tipoStore,
                triggerAction: 'all',
                mode: 'local',
                displayField: 'tipousuario',
                name: 'tipo',
                id: 'tipo',
                forceSelection: true
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Nombre',
                name: 'nombre',
                allowBlank: false,
                vtype: 'alpha'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Apellido',
                name: 'apellido',
                allowBlank: false,
                vtype: 'alpha'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Email',
                name: 'email',
                allowBlank: false,
                vtype: 'email'
            }]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                userform.getForm().submit({
                    success: function(f,a){
                        userform.getForm().reset();
                        win.hide();
                        userDataStore.load();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Warning', 'Ha ocurrido un error <br /> no es posible guardar los datos');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                userform.getForm().reset();
                win.hide();

            }
        }]

    });
    Ext.namespace('Ext.userdata');
    Ext.userdata.status = [
    ['0', 'inactiva'], ['1', 'activa']
    ];
    var stStore = new Ext.data.SimpleStore({
        fields: ['status', 'label'],
        data: Ext.userdata.status
    });
    //stStore.load();

    var edit_form = new Ext.form.FormPanel({
        url: '/administrador/index.php/useradmin/edit',
        bodyStyle:"padding:15px 0px 10px 10px; margin: 0",
        buttonAlign: "right",
        width: 350,
        height: 300,
        labelWidth: 130,
        //labelAlign: 'right',
        items: [{
            border: false,
            layout: 'form',
            items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Username',
                name: 'username',
                allowBlank: false,
                vtype: 'alphanum',
                maxLength: 20
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Password',
                inputType: 'password',
                name:'password',
                allowBlank: true,
                maxLength: 20,
                id: 'password0'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Confirmar Password',
                inputType: 'password',
                name: 'password_confirm',
                allowBlank: true,
                maxLength: 20,
                vtype: 'password',
                initialPassField: 'password0'
            },
            {
                xtype: 'combo',
                fieldLabel: 'Tipo de Usuario',
                width: 170,
                store: tipoStore,
                triggerAction: 'all',
                mode: 'local',
                displayField: 'tipousuario',
                name: 'tipoe',
                id: 'tipoe',
                forceSelection: true
            },
            {
                xtype: 'combo',
                fieldLabel: 'Estatus de la Cuenta',
                store: stStore,
                displayField: 'label',
                valueField: 'status',
                emptyText: 'selecionar estatus',
                hiddenName: 'active',
                mode: 'local',
                width: 170,
                name: 'status',
                forceSelection: true,
                selectOnFocus: true
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Nombre',
                name: 'nombre',
                allowBlank: false,
                vtype: 'alpha'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Apellido',
                name: 'apellido',
                allowBlank: false,
                vtype: 'alpha'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Email',
                name: 'email',
                allowBlank: false,
                vtype: 'email'
            },
            {
                xtype: 'hidden',
                name: 'id'
            }
            ]
        }],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            formBind: true,
            handler: function() {
                edit_form.getForm().submit({
                    success: function(f,a){
                        edit_form.getForm().reset();
                        win_edit.hide();
                        userDataStore.load();
                    },
                    failure: function(f,a){
                        Ext.Msg.alert('Warning', 'Ha ocurrido un error y <br /> no es posible guardar los datos');
                    }
                });
            }
        },{
            text: 'Cancelar',
            handler: function(){
                edit_form.getForm().reset();
                win_edit.hide();

            }
        }]

    });

    var win = new Ext.Window({
        title: "Agregar Nuevo Usuario",
        applyTo: 'ventana',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: userform
    });

    var win_edit = new Ext.Window({
        title: 'Editar Usuario',
        applyTo: 'ventana1',
        layout: 'fit',
        width: 370,
        height: 'auto',
        plain: true,
        closeAction: 'hide',
        items: edit_form
    });

    function addUser () {
        win.show();
    }

    function editUser() {
        var sm = gridUsers.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            //console.dir(sel.data);
            edit_form.getForm().findField('username').setValue(sel.data.usuario);
            edit_form.getForm().findField('nombre').setValue(sel.data.nombre);
            edit_form.getForm().findField('apellido').setValue(sel.data.apellido);
            edit_form.getForm().findField('email').setValue(sel.data.email);
            edit_form.getForm().findField('id').setValue(sel.data.idusuario);
            //var status = sel.data.active == 1 ? 'activa' : 'inactiva';
            edit_form.getForm().findField('active').setValue(sel.data.active);
            edit_form.getForm().findField('tipoe').setValue(sel.data.tipo);
            //edit_form.getForm().findField('status').selectByValue(status);
            win_edit.show();
        } else {
            Ext.Msg.alert('Alerta', 'Debe Seleccionar un mensaje para poder realizar esta operacion')
        }
    }

    function delUsr(){
        var sm = gridUsers.getSelectionModel();
        if(sm.hasSelection()){
            var sel = sm.getSelected();
            //console.dir(sel.data);
            var id_user = sel.data.idusuario;
            Ext.Msg.show({
                title: 'Eliminar Usuario',
                msg: 'Esta seguro que desea eliminar <br /> la cuenta del usuario ' + sel.data.usuario + ' ?',
                //buttons: Ext.MessageBox.OKCANCEL,
                buttons: {
                    ok: true,
                    cancel: true
                },
                fn: function(btn) {
                    switch(btn){
                        case 'ok':
                            Ext.Ajax.request({
                                url: '/administrador/index.php/useradmin/delete',
                                params: {
                                    'id': id_user
                                },
                                success: function () {
                                    userDataStore.load();
                                },
                                failure: function () {
                                    Ext.msg.alert('Error', 'Ha ocurrido un error <br /> el usuario no pudo ser eliminado');
                                }
                            });
                            //Ext.Msg.alert('kill this dude', btn);
                            break;
                        case 'cancel':
                            //Ext.Msg.alert('wtf, no elimination', btn);
                            break;
                    }

                },
                icon: Ext.MessageBox.WARNING
            });

        } else {
            Ext.Msg.alert('Error', 'Debe Seleccionar un usuario para poder realizar esta operacion')
        }
    }

    function status_image(val){
        return '<div class="' + val +'">' + val +'</div>'
    }

    var toolbar = new Ext.Toolbar({
        items: [
        {
            xtype: 'tbbutton',
            text: 'Agregar usuario',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/add_user.png',
            handler: addUser
        },
        {
            xtype: 'tbseparator'
        },

        {
            xtype: 'tbbutton',
            text: 'Editar usuario',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/edit_profile.png',
            handler: editUser
        },
        {
            xtype: 'tbseparator'
        },

        {
            xtype: 'tbbutton',
            text: 'Eliminar usuario',
            cls: 'x-btn-text-icon',
            icon: '/administrador/img/delete_user.png',
            handler: delUsr
        }
        /*,
        { xtype: 'tbseparator'},
        { xtype: 'tbfill'},
            {  xtype: 'tbtext',
               text:  'Buscar usuario: &nbsp;'
            },
            {
                xtype: 'textfield',
                //text: 'buscar'
                listeners: {
                    specialkey: function() { console.log('checking'); }
                }

           }
           */
        ]
    });

    var userDataStore = new Ext.data.Store({
        // @todo cambiar url en produccion
        url: '/administrador/index.php/useradmin/getusers',
        reader: new Ext.data.JsonReader({
            root: 'rows',
            id: 'idusuario'
        }, [{
            name: 'idusuario'
        },

        {
            name: 'usuario'
        },
        {
            name: 'tipo'
        },
        {
            name: 'active'
        },
        {
            name: 'estatus'
        },
        {
            name: 'nombre'
        },

        {
            name: 'apellido'
        },

        {
            name: 'email'
        }]),
        sortInfo: {
            field: 'idusuario',
            direction: 'ASC'
        }
    });
    userDataStore.load();

    var userColumnModel = [
    {
        header: 'Id',
        dataIndex: 'idusuario',
        sortable: true
    },
    {
        header: 'Usuario',
        dataIndex: 'usuario',
        sortable: true
    },
    {
        header: 'Tipo',
        dataIndex: 'tipo',
        sortable: true
    },
    {
        header: 'Estatus de la Cuenta',
        dataIndex: 'estatus',
        sortable: true,
        renderer: status_image
    },
    {
        header: 'Nombre',
        dataIndex: 'nombre',
        sortable: true
    },
    {
        header: 'Apellido',
        dataIndex: 'apellido',
        sortable: true
    },
    {
        header: 'Email',
        dataIndex: 'email',
        sortable: true
    }
    ];

    var gridUsers = new Ext.grid.GridPanel({
        store: userDataStore,
        columns: userColumnModel,
        frame: true,
        loadMask: {
            msg: 'Cargando datos, por favor espere ....'
        },
        viewConfig: {
            forceFit:true
        },
        renderTo: 'user_grid',
        stripeRows: true,
        height: 350,
        autoWidth: 'true',
        title: 'Administracion de Usuarios',
        tbar: toolbar,
        iconCls: 'useradmin_panel'
    });

});