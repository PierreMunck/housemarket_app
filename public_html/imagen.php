<?php

require_once 'Zend/Cache.php';
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../housemarket/application'));

$idPhoto = (isset($_GET["id"])) ? $_GET["id"] : 0;
$typePhoto = (isset($_GET["tipo"])) ? $_GET["tipo"] : 4;

if (!is_numeric($idPhoto))
    exit();


$cache = Zend_Cache::factory(
		'Page',
		'File',
		array(
				'lifetime'=>86400, //24h
				'ignore_user_abort' => true,
		),
		array(
				'cache_dir' => APPLICATION_PATH . '/data/imagecache',
				'hashed_directory_level'=>3,
		)
);
$id = $idPhoto ."|". $typePhoto;

$cache->start(md5($id));

// Los distintos tama�os de Fotos
switch ($typePhoto) {
    case 1:
        $srcPhoto = "FotoGrande";
        break;
    case 2:
        $srcPhoto = "FotoDestacada";
        break;
    case 3:
        $srcPhoto = "FotoResultado";
        break;
    default:
        $srcPhoto = "FotoGaleria";
}

$localhost = "localhost";
$username = "hmapp";
$password = "H1M2A3p4p_";
$database_name = "hmapp";

$link = mysql_connect($localhost, $username, $password) or die("No se pudo conectar");
$success = mysql_select_db($database_name, $link);
$where = "WHERE PROFotoID =" . mysql_escape_string($idPhoto);
$consultaSQL = "SELECT $srcPhoto,Mime FROM profoto " . $where;


$result = mysql_query($consultaSQL) or die("No se logro consulta sql");
$dataPhoto = mysql_fetch_assoc($result);

if ($dataPhoto[$srcPhoto] === null) {
    echo $defImageRoute[$srcPhoto];
} else {


    //iconv("UTF-8", "ISO-8859-1", $text)
    try {
        $sourceImage = $dataPhoto[$srcPhoto];
        if (mb_detect_encoding($dataPhoto[$srcPhoto], "auto") == 'ASCII') {
            $sourceImage = base64_decode($dataPhoto[$srcPhoto]);
        }
        require_once '../housemarket/application/models/Hm/Pro/ThumbLib.inc.php';

        $size = strlen($sourceImage) / 1024;
        if (($size > 40 && $typePhoto == 1) || ($size > 5 && $typePhoto != 1)) {
            $options = array('jpegQuality' => 60);
            $thumb = Hm_Pro_PhpThumbFactory::create($sourceImage, $options, true);
            $thumb->setFormat("JPG");
            $sourceImage = $thumb->getImageAsString();
            $dataPhoto['Mime'] = $thumb->getMime();
            $sql = sprintf("UPDATE profoto SET %s='%s',Mime='%s' %s", $srcPhoto, base64_encode($sourceImage), $dataPhoto['Mime'], $where);

            mysql_query($sql) or die("No se actualizó la imagen");
        }


        header("Content-type: " . $result["Mime"]);
        echo $sourceImage;
    } catch (Exception $e) {
        debug($e->getMessage());
    }


    //echo "<br>Coding: ".mb_detect_encoding(base64_decode($dataPhoto[$srcPhoto]));
    //echo mb_detect_encoding($dataPhoto[$srcPhoto], "auto");
}

function debug($var, $exit=true) {
    echo "<pre>";
    print_r($var);
    echo "</pre>";
    if ($exit)
        exit();
}

?>
