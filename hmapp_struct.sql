
DROP TABLE IF EXISTS `catatributocategoria`;
DROP TABLE IF EXISTS `catbeneficiocategoria`;
DROP TABLE IF EXISTS `usuarios`;
DROP TABLE IF EXISTS `usuario_app`;
DROP TABLE IF EXISTS `user_pref`;
DROP TABLE IF EXISTS `sessiondb`;
DROP TABLE IF EXISTS `prop_foto`;
DROP TABLE IF EXISTS `propfotoalbum`;
DROP TABLE IF EXISTS `prop_video`;
DROP TABLE IF EXISTS `prop_foto_album`;
DROP TABLE IF EXISTS `prop_favoritas`;
DROP TABLE IF EXISTS `prompage`;
DROP TABLE IF EXISTS `promocion`;
DROP TABLE IF EXISTS `profoto`;
DROP TABLE IF EXISTS `probeneficiopropiedad`;
DROP TABLE IF EXISTS `proatributopropiedad`;
DROP TABLE IF EXISTS `phfiletypes`;
DROP TABLE IF EXISTS `parametros`;
DROP TABLE IF EXISTS `pais`;
DROP TABLE IF EXISTS `page_service_status`;
DROP TABLE IF EXISTS `page`;
DROP TABLE IF EXISTS `geolitecity_location`;
DROP TABLE IF EXISTS `geolitecity_blocks`;
DROP TABLE IF EXISTS `geolite_country`;
DROP TABLE IF EXISTS `geoipinfodb_ipcities`;
DROP TABLE IF EXISTS `fb_page`;
DROP TABLE IF EXISTS `crecreditopropiedad`;
DROP TABLE IF EXISTS `crecredito`;
DROP TABLE IF EXISTS `representante`;
DROP TABLE IF EXISTS `propropiedad`;
DROP TABLE IF EXISTS `cliente`;
DROP TABLE IF EXISTS `cattipoenlista`;
DROP TABLE IF EXISTS `catpais`;
DROP TABLE IF EXISTS `catmoneda`;
DROP TABLE IF EXISTS `catidiomaidioma`;
DROP TABLE IF EXISTS `catidioma`;
DROP TABLE IF EXISTS `catenlistaidioma`;
DROP TABLE IF EXISTS `catedopropiedad`;
DROP TABLE IF EXISTS `catedoproidioma`;
DROP TABLE IF EXISTS `catcategoriaidioma`;
DROP TABLE IF EXISTS `catcategoria`;
DROP TABLE IF EXISTS `catcamposcarga`;
DROP TABLE IF EXISTS `catbeneficioidioma`;
DROP TABLE IF EXISTS `catbeneficio`;
DROP TABLE IF EXISTS `catatributoidioma`;
DROP TABLE IF EXISTS `catatributo`;
DROP TABLE IF EXISTS `RecuentoPropiedad`;
DROP TABLE IF EXISTS `RMTRoommate`;
DROP TABLE IF EXISTS `RMTFoto`;
DROP TABLE IF EXISTS `RMTCuarto`;
DROP TABLE IF EXISTS `RMTBeneficio`;
DROP TABLE IF EXISTS `CATValorDominioIdioma`;
DROP TABLE IF EXISTS `CATValorDominio`;
DROP TABLE IF EXISTS `CATDominio`;

--
-- Table structure for table `CATDominio`
--


CREATE TABLE `CATDominio` (
  `CodigoDominio` tinyint(4) NOT NULL COMMENT 'Identificador de cada grupo de datos. Unico.',
  `NombreDominio` varchar(128) NOT NULL COMMENT 'Nombre del grupo de datos. Unico.',
  `Estado` varchar(1) NOT NULL COMMENT 'Valores A= Activo  B= Borrado I=Inactivo',
  PRIMARY KEY (`CodigoDominio`),
  UNIQUE KEY `NombreDominio` (`NombreDominio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Nombres de los grupos de datos cuyos valores estan creados en el detalle de Dominios.';


--
-- Table structure for table `CATValorDominio`
--


CREATE TABLE `CATValorDominio` (
  `CodigoValor` tinyint(4) NOT NULL COMMENT 'Identificador único de cada valor. ',
  `CodigoDominio` tinyint(4) NOT NULL COMMENT 'Identifica el grupo de datos al cual pertenece el valor. ',
  `NombreValor` varchar(128) NOT NULL COMMENT 'Nombre de cada valor dentro del dominio. ',
  `Estado` varchar(1) NOT NULL COMMENT 'A=Activo, B=Borrado, I=Inactivo',
  PRIMARY KEY (`CodigoValor`),
  KEY `CodigoDominio` (`CodigoDominio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Valores que puede tomar el grupo de datos.  El código de cada valor es lo que se almacena en las tablas operativas. ';

--
-- Table structure for table `CATValorDominioIdioma`
--

CREATE TABLE `CATValorDominioIdioma` (
  `CATValorDominioIdiomaID` int(11) NOT NULL AUTO_INCREMENT,
  `CodigoValor` tinyint(4) NOT NULL COMMENT 'Identificador del valor dominio. ',
  `CodigoIdioma` varchar(8) NOT NULL COMMENT 'Identificador del idioma al cual se está traduciendo el valor del dominio.',
  `ValorIdioma` varchar(128) NOT NULL COMMENT 'Traducción del vlaor dominio al idioma indicado.',
  PRIMARY KEY (`CATValorDominioIdiomaID`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=latin1 COMMENT='Traducción de los dominios al idioma indicado';

--
-- Table structure for table `RMTBeneficio`
--

CREATE TABLE `RMTBeneficio` (
  `RMTBeneficioID` smallint(6) NOT NULL AUTO_INCREMENT,
  `BeneficioCuarto` tinyint(4) NOT NULL COMMENT 'Identificador del codigo valor que corresponde al dominio Beneficios Cuarto. ',
  `FechaCreacion` datetime NOT NULL COMMENT 'Tomar de la base de datos',
  `FechaModificacion` datetime DEFAULT NULL COMMENT 'Ultima fecha de actualización del registro del beneficio',
  `CodigoCuarto` int(11) NOT NULL COMMENT 'Identificador del Cuarto',
  PRIMARY KEY (`RMTBeneficioID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Guarda beneficios de cuarto';


CREATE TABLE `RMTCuarto` (
  `CodigoCuarto` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador del cuarto',
  `Titulo` varchar(128) NOT NULL COMMENT 'Título del anuncio del cuarto.',
  `Descripcion` varchar(255) DEFAULT NULL COMMENT 'Dato adicional sobre el cuarto. Opcional',
  `TipoAlojamiento` tinyint(4) NOT NULL COMMENT 'Tomar los valores del dominio Tipo Alojamiento.',
  `TipoPropiedad` tinyint(4) NOT NULL COMMENT 'Tomar del dominio Tipo Propiedad RMT Tengo',
  `MaxCapacidad` tinyint(4) NOT NULL COMMENT 'Número que indica la máxima capacidad de un cuarto. Mínomo 1 máximo 10.',
  `EspacioDisponible` tinyint(4) NOT NULL COMMENT 'Número que indica la cantidad de espacios disponibles en el cuarto. Número del 1 al 10',
  `TipoCuarto` tinyint(4) NOT NULL COMMENT 'Indica si el cuarto es individual o múltiple. Tomar los valores del dominio Tipo Cuarto.',
  `TipoBano` tinyint(4) NOT NULL COMMENT 'Indica si el baño es privado o compartido. Tomar del dominio Tipo Baño.',
  `MontoRenta` decimal(10,2) NOT NULL COMMENT 'Monto de alquiler del cuarto por mes.',
  `IncluyeServicio` varchar(1) NOT NULL COMMENT 'Indica si en el precio de renta está incluido el costo por servicios. Valores S o N.',
  `MontoServicios` decimal(10,2) DEFAULT NULL COMMENT 'Si la renta no cubre los servicios se debe indicar cual es el monto por servicios.',
  `FechaDisponible` date NOT NULL COMMENT 'Indica la fecha en que se puede ocupar el apartamento',
  `CortoPlazo` varchar(1) NOT NULL COMMENT 'Indica si en el cuarto se acepta estancias mínimas.  Valores S o N.',
  `Direccion` varchar(255) NOT NULL COMMENT 'Detalle de la dirección del cuarto',
  `Estado` varchar(64) NOT NULL,
  `Ciudad` varchar(64) DEFAULT NULL,
  `ZipCode` varchar(64) NOT NULL COMMENT 'Código del país. ',
  `Calle` varchar(32) DEFAULT NULL,
  `CodigoPostal` varchar(16) DEFAULT NULL,
  `Latitud` double NOT NULL,
  `Longitud` double NOT NULL,
  `Uid` varchar(16) NOT NULL COMMENT 'Identificador de facebook del cliente que ingresa el cuarto',
  `FechaRegistro` datetime NOT NULL COMMENT 'Tomar de la base de datos. Indica cuando se crea el registro del cuarto.',
  `FechaInactiva` datetime DEFAULT NULL COMMENT 'Fecha en que el cuarto se marca como inactivo para que no se siga mostrando en las consultas.  Se obtiene por un proceso automático que calcula la antiguedad de un cuarto y la compara con el parametro Vigencia Cuarto.',
  `EdadMinimaHabitan` tinyint(4) NOT NULL COMMENT 'Edad inicial del rango de edades de las personas que habitan en la propiedad. Mayor que 16 y menor que 100',
  `EdadMaximaHabitan` tinyint(4) NOT NULL COMMENT 'Edad máxima de las personas que habitan en la propiedad. Mayor o igual que la edad mínima.  de 16 a 100.',
  `FumadorHabitan` tinyint(4) NOT NULL COMMENT 'Tomar del dominio Fumador Tengo',
  `IdiomaHabitan` varchar(4) DEFAULT NULL,
  `OcupacionHabitan` tinyint(4) NOT NULL COMMENT 'Tomar del dominio Ocupación Tengo',
  `GeneroHabitan` tinyint(4) DEFAULT NULL COMMENT 'Género de las personas que habitan.  Tomar del dominio Género Habitan.',
  `MascotaHabitan` tinyint(4) NOT NULL COMMENT 'Dominio Mascotas Tengo',
  `OrientacionHabitan` tinyint(4) NOT NULL COMMENT 'Indica la orientación sexual de los que habitan en la propiedad tomar de dominio Orientación Tengo.',
  `HijosHabitan` tinyint(4) NOT NULL COMMENT 'Tomar del dominio Hijos Tengo',
  `GeneroAcepta` tinyint(4) DEFAULT NULL COMMENT 'Género del roommate ideal. Tomar del dominio Género Acepta.',
  `ParejaAcepta` tinyint(4) NOT NULL COMMENT 'Indica si aceptan o no parejas para el  roommate ideal. Tomar del dominio Pareja Acepta',
  `NinosAcepta` tinyint(4) NOT NULL COMMENT 'Indica si para roommate ideal le aceptan o no niños. Tomar del dominio Acepta Niños',
  `MascotaAcepta` tinyint(4) NOT NULL COMMENT 'Indica si le aceptan mascotas al roommates ideal. Tomar del dominio Mascotas Acepta.',
  `FumadorAcepta` tinyint(4) NOT NULL COMMENT 'Indica si el roommate ideal puede ser fumador o no. Tomar del dominio Fumador Acepta.',
  `OrientacionAcepta` tinyint(4) NOT NULL COMMENT 'Indica la orientación sexual del roommates ideal. Tomar del dominio Orientación Acepta.',
  `OcupacionAcepta` tinyint(4) NOT NULL COMMENT 'Indica la  ocupación del roommates ideal. Tomar del dominio Ocupación Acepta.',
  `EdadMinimaAcepta` tinyint(4) NOT NULL COMMENT 'Valor mínimo para el rango de edad del roommate ideal. Valores entre 16 y 100. ',
  `EdadMaximaAcepta` tinyint(4) NOT NULL COMMENT 'Edad máxima del rango de edad aceptado para el roommate ideal. Valores entre 16 y 100. Tiene que ser mayor o igual que la edad mínima.',
  `EstanciaMinima` tinyint(4) NOT NULL COMMENT 'Cantidad de meses mínimo que debe estar el inquilino en el cuarto. valores del 1 al 12.',
  `EstadoRoommate` tinyint(4) NOT NULL COMMENT 'Indica el estado del registro de cuarto o de la persona que busca un cuarto.  Tomar del dominio Estado Roommate.',
  `Correo` varchar(250) NOT NULL DEFAULT 'NULL',
  PRIMARY KEY (`CodigoCuarto`),
  UNIQUE KEY `Uid` (`Uid`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1 COMMENT='Datos del cuarto';

--
-- Table structure for table `RMTFoto`
--

CREATE TABLE `RMTFoto` (
  `RMTFotoID` int(11) NOT NULL AUTO_INCREMENT,
  `CodigoCuarto` int(11) NOT NULL,
  `Principal` bit(1) NOT NULL,
  `FotoGrande` blob NOT NULL,
  `Comentario` varchar(255) DEFAULT NULL,
  `AltoFotoGrande` int(11) NOT NULL,
  `AnchoFotoGrande` int(11) NOT NULL,
  `FotoDestacada` blob,
  `FotoResultado` blob,
  `FotoGaleria` blob NOT NULL,
  `Mime` varchar(15) DEFAULT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`RMTFotoID`),
  KEY `FX_Cuarto` (`CodigoCuarto`),
  CONSTRAINT `FX_Cuarto` FOREIGN KEY (`CodigoCuarto`) REFERENCES `RMTCuarto` (`CodigoCuarto`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

--
-- Table structure for table `RMTRoommate`
--

CREATE TABLE `RMTRoommate` (
  `CodigoRoommate` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la persona que busca cuartos',
  `Titulo` varchar(128) NOT NULL COMMENT 'Título del anuncio del que busca un cuarto',
  `Descripcion` varchar(255) DEFAULT NULL COMMENT 'Dato adicional sobre la persona que busca roommate. Opcional',
  `TipoBusqueda` tinyint(4) NOT NULL COMMENT 'Tomar los valores del dominio Tipo Búsqueda.',
  `TipoPropiedad` tinyint(4) NOT NULL COMMENT 'Tomar del dominio Tipo Propiedad RMT Busco, con valores No importa, Casa, Apartamento, Residencia Universitaria',
  `MaxCapacidad` tinyint(4) NOT NULL COMMENT 'Número que indica la máxima capacidad de un cuarto. Mínomo 1 máximo 10.',
  `TipoCuarto` tinyint(4) NOT NULL COMMENT 'Indica si el cuarto es individual o múltiple. Tomar los valores del dominio Tipo Cuarto Busco.',
  `TipoBano` tinyint(4) NOT NULL COMMENT 'Indica si el baño es privado o compartido. Tomar del dominio Tipo Baño Busco.',
  `MontoRenta` decimal(10,2) NOT NULL COMMENT 'Monto máximo a pagar en concepto de alquiler del cuarto por mes.',
  `FechaMudanza` date NOT NULL COMMENT 'Indica la fecha en que se desea mudarse al cuarto',
  `Uid` varchar(16) NOT NULL COMMENT 'Identificador de facebook del cliente que está buscando un cuarto',
  `FechaRegistro` datetime NOT NULL COMMENT 'Tomar de la base de datos. Indica cuando se crea el registro del cuarto.',
  `FechaInactiva` datetime DEFAULT NULL COMMENT 'Fecha en que el cuarto se marca como inactivo para que no se siga mostrando en las consultas.  Se obtiene por un proceso automático que calcula la antiguedad de un cuarto y la compara con el parametro Vigencia Cuarto.',
  `CantBusca` tinyint(4) NOT NULL COMMENT 'Cantidad de personas que se desea como máximo en la propiedad. mayor que cero.',
  `EdadMinimaBusca` tinyint(4) NOT NULL COMMENT 'Edad inicial del rango al que pertenece la persona que está buscando un cuarto. Mayor que 16 y menor que 100',
  `EdadMaximaBusca` tinyint(4) DEFAULT NULL COMMENT 'Edad máxima del rango al que pertenece la persona que está buscando un cuarto. Mayor o igual que la edad mínima.  de 16 a 100.',
  `FumadorBusca` tinyint(4) NOT NULL COMMENT 'Indica si la persona que está buscando el cuarto es fumador. Tomar del dominio Fumador Tengo',
  `IdiomaBusca` varchar(4) DEFAULT NULL,
  `OcupacionBusca` tinyint(4) NOT NULL COMMENT 'Ocupación de la persona que está buscando el cuarto. Tomar del dominio Ocupación Tengo',
  `GeneroBusca` tinyint(4) NOT NULL COMMENT 'Género de la persona que está buscando el cuarto.  Tomar del dominio Género Tengo.',
  `MascotaBusca` tinyint(4) NOT NULL COMMENT 'Indica si la persona que busca cuarto tiene o no mascota. Dominio Mascotas Tengo',
  `OrientacionBusca` tinyint(4) DEFAULT NULL COMMENT 'Indica la orientación sexual de la persona que está buscando el cuarto. Tomar de dominio Orientación Tengo.',
  `HijosBusco` tinyint(4) NOT NULL COMMENT 'Indica si la persona que está buscando el cuarto tiene o no niños. Tomar del dominio Hijos Tengo',
  `GeneroAcepta` tinyint(4) NOT NULL COMMENT 'Género del roommate ideal. Tomar del dominio Género Acepta.',
  `ParejaAcepta` tinyint(4) NOT NULL COMMENT 'Indica si aceptan o no parejas para el  roommate ideal. Tomar del dominio Pareja Acepta',
  `NinosAcepta` tinyint(4) NOT NULL COMMENT 'Indica si para roommate ideal le aceptan o no niños. Tomar del dominio Acepta Niños',
  `MascotaAcepta` tinyint(4) NOT NULL COMMENT 'Indica si le aceptan mascotas al roommates ideal. Tomar del dominio Mascotas Acepta.',
  `FumadorAcepta` tinyint(4) NOT NULL COMMENT 'Indica si el roommate ideal puede ser fumador o no. Tomar del dominio Fumador Acepta.',
  `OrientacionAcepta` tinyint(4) NOT NULL COMMENT 'Indica la orientación sexual del roommates ideal. Tomar del dominio Orientación Acepta.',
  `OcupacionAcepta` tinyint(4) NOT NULL COMMENT 'Indica la  ocupación del roommates ideal. Tomar del dominio Ocupación Acepta.',
  `EdadMinimaAcepta` tinyint(4) NOT NULL COMMENT 'Valor mínimo para el rango de edad del roommate ideal. Valores entre 16 y 100. ',
  `EdadMaximaAcepta` tinyint(4) NOT NULL COMMENT 'Edad máxima del rango de edad aceptado para el roommate ideal. Valores entre 16 y 100. Tiene que ser mayor o igual que la edad mínima.',
  `EstanciaMinima` tinyint(4) NOT NULL COMMENT 'Cantidad de meses mínimo que puede estar la persona que busca cuarto como roommate. Valores del 1 al 12.',
  `Amueblado` tinyint(4) NOT NULL COMMENT 'Indica si la persona está buscando un cuerto que esté amueblado o no. Tomar los valores del dominio Amueblado Busco.',
  `EstadoRoommate` tinyint(4) NOT NULL,
  `Direccion` varchar(512) NOT NULL,
  `Estado` varchar(64) DEFAULT NULL,
  `Ciudad` varchar(64) DEFAULT NULL,
  `Calle` varchar(32) DEFAULT NULL,
  `CodigoPostal` varchar(16) DEFAULT NULL,
  `Latitud` double NOT NULL,
  `Longitud` double NOT NULL,
  `ZipCode` varchar(64) DEFAULT NULL,
  `Correo` varchar(250) NOT NULL,
  PRIMARY KEY (`CodigoRoommate`),
  UNIQUE KEY `Uid` (`Uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Datos de personas que buscan cuarto';

--
-- Table structure for table `RecuentoPropiedad`
--

CREATE TABLE `RecuentoPropiedad` (
  `RecuentoID` int(11) NOT NULL AUTO_INCREMENT,
  `FechaHora` datetime NOT NULL COMMENT 'Fecha y hora de registro de la cantidad de propiedades.',
  `Cantidad` int(11) NOT NULL COMMENT 'Cantidad de propiedades',
  PRIMARY KEY (`RecuentoID`),
  UNIQUE KEY `RecuentoID` (`RecuentoID`),
  UNIQUE KEY `FechaHora` (`FechaHora`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Almacena el registro por fecha y hora de la cantidad de prop';

--
-- Table structure for table `catatributo`
--

CREATE TABLE `catatributo` (
  `CodigoAtributo` smallint(6) NOT NULL COMMENT 'Identificador de cada atributo.',
  `NombreAtributo` varchar(128) CHARACTER SET latin1 NOT NULL COMMENT 'Nombre del atributo de las propiedades.',
  `TipoAtributo` varchar(32) CHARACTER SET latin1 NOT NULL COMMENT 'Indica el tipo (MySql) del atributo. ',
  `TamanioAtributo` smallint(6) NOT NULL COMMENT 'Indica el tamaño máximo que puede tener el valor para el atributo.  Se usa para campos de tipo caracter, numérico entero.  Para los decimales se indica en este campo el tamaño del campo en su totalidad y en el campo LongDecimal la cantidad de posiciones d',
  `LongDecimal` smallint(6) DEFAULT NULL,
  `NombreEN` varchar(64) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Nombre del atributo en inglés',
  PRIMARY KEY (`CodigoAtributo`),
  UNIQUE KEY `NombreAtributo` (`NombreAtributo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la lista de atributos que pueden ser asociados a la';



--
-- Table structure for table `catatributoidioma`
--

CREATE TABLE `catatributoidioma` (
  `CATAtributoIdiomaID` int(11) NOT NULL AUTO_INCREMENT,
  `CodigoAtributo` smallint(6) NOT NULL,
  `CodigoIdioma` varchar(8) CHARACTER SET latin1 NOT NULL,
  `ValorIdioma` varchar(64) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`CATAtributoIdiomaID`),
  UNIQUE KEY `CATAtributoIdiomaID` (`CATAtributoIdiomaID`),
  KEY `fk_catatributo_catatributoidioma` (`CodigoAtributo`),
  KEY `fk_catidioma_catatributoidioma_2` (`CodigoIdioma`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Table structure for table `catbeneficio`
--

CREATE TABLE `catbeneficio` (
  `CodigoBeneficio` smallint(6) NOT NULL AUTO_INCREMENT,
  `NombreBeneficio` varchar(128) NOT NULL COMMENT 'Nombre del beneficio',
  `NombreEN` varchar(64) DEFAULT NULL COMMENT 'Nombre del beneficio en inglés',
  PRIMARY KEY (`CodigoBeneficio`),
  UNIQUE KEY `NombreBeneficio` (`NombreBeneficio`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1 COMMENT='Almacena los valores que pueden dar valor agregado a las pro';


--
-- Table structure for table `catbeneficioidioma`
--

CREATE TABLE `catbeneficioidioma` (
  `CATBeneficioIdiomaID` int(11) NOT NULL AUTO_INCREMENT,
  `CodigoBeneficio` smallint(6) NOT NULL,
  `CodigoIdioma` varchar(8) NOT NULL,
  `ValorIdioma` varchar(128) NOT NULL,
  PRIMARY KEY (`CATBeneficioIdiomaID`),
  UNIQUE KEY `CATBeneficioIdiomaID` (`CATBeneficioIdiomaID`),
  KEY `fk_catidioma_catbeneficioidioma` (`CodigoIdioma`),
  KEY `fk_catbeneficio_catbeneficioidi` (`CodigoBeneficio`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

--
-- Table structure for table `catcamposcarga`
--

CREATE TABLE `catcamposcarga` (
  `IDCamposCarga` tinyint(4) NOT NULL AUTO_INCREMENT,
  `CampoArchivo` varchar(16) NOT NULL COMMENT 'Nombre de la columna en el archivo de carga masiva de propiedades.',
  `CampoTabla` varchar(16) NOT NULL COMMENT 'Nombre del campo en la tabla PROPropiedad donde corresponde se almacene el valor de la columna correspondiente en el archivo de carga',
  PRIMARY KEY (`IDCamposCarga`),
  UNIQUE KEY `IDCamposCarga` (`IDCamposCarga`),
  UNIQUE KEY `CampoArchivo` (`CampoArchivo`),
  UNIQUE KEY `CampoTabla` (`CampoTabla`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1 COMMENT='Usada para almacenar los nombres de las columnas obligatoria';

--
-- Table structure for table `catcategoria`
--

CREATE TABLE `catcategoria` (
  `CodigoCategoria` smallint(6) NOT NULL COMMENT 'Identificador de cada registro',
  `NombreCategoria` varchar(64) NOT NULL,
  `NombreEN` varchar(128) DEFAULT NULL COMMENT 'Nombre en inglés',
  `filtro` smallint(6) DEFAULT '1',
  `orden` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`CodigoCategoria`),
  UNIQUE KEY `NombreCategoria` (`NombreCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `catcategoriaidioma`
--

CREATE TABLE `catcategoriaidioma` (
  `CATCategoriaIdiomaID` int(11) NOT NULL AUTO_INCREMENT,
  `CodigoCategoria` smallint(6) NOT NULL,
  `CodigoIdioma` varchar(8) NOT NULL,
  `ValorIdioma` varchar(64) NOT NULL,
  PRIMARY KEY (`CATCategoriaIdiomaID`),
  UNIQUE KEY `CATCategoriaIdiomaID` (`CATCategoriaIdiomaID`),
  KEY `fk_catcategoria_catcategoriaidi` (`CodigoCategoria`),
  KEY `fk_catidioma_catcategoriaidioma` (`CodigoIdioma`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Table structure for table `catedoproidioma`
--

CREATE TABLE `catedoproidioma` (
  `CATEdoProIdiomaID` int(11) NOT NULL AUTO_INCREMENT,
  `CodigoEdoPropiedad` tinyint(4) NOT NULL,
  `CodigoIdioma` varchar(8) NOT NULL,
  `ValorIdioma` varchar(64) NOT NULL,
  PRIMARY KEY (`CATEdoProIdiomaID`),
  UNIQUE KEY `CATEdoProIdiomaID` (`CATEdoProIdiomaID`),
  KEY `FK_EdoPropiedad_EdoProIdioma` (`CodigoEdoPropiedad`),
  KEY `FK_IdiomaEdoPropiedad` (`CodigoIdioma`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Table structure for table `catedopropiedad`
--

CREATE TABLE `catedopropiedad` (
  `CodigoEdoPropiedad` tinyint(4) NOT NULL,
  `NombreEdoPropiedad` varchar(32) NOT NULL,
  `NombreEN` varchar(32) DEFAULT NULL COMMENT 'Nombre del estado en Inglés',
  PRIMARY KEY (`CodigoEdoPropiedad`),
  UNIQUE KEY `NombreEdoPropiedad` (`NombreEdoPropiedad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Almacena los estados en que se puede encontrar una propiedad';

--
-- Table structure for table `catenlistaidioma`
--

CREATE TABLE `catenlistaidioma` (
  `CATEnlistaIdiomaID` int(11) NOT NULL AUTO_INCREMENT,
  `CodigoEnlista` varchar(4) NOT NULL,
  `CodigoIdioma` varchar(8) NOT NULL,
  `ValorIdioma` varchar(64) NOT NULL,
  PRIMARY KEY (`CATEnlistaIdiomaID`),
  UNIQUE KEY `CATEnlistaIdiomaID` (`CATEnlistaIdiomaID`),
  UNIQUE KEY `ValorIdioma` (`ValorIdioma`),
  KEY `fk_cattipoenlista_catenlistaidi` (`CodigoEnlista`),
  KEY `fk_catidioma_catenlistaidioma` (`CodigoIdioma`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Table structure for table `catidioma`
--

CREATE TABLE `catidioma` (
  `CodigoIdioma` varchar(8) NOT NULL,
  `NombreIdioma` varchar(64) DEFAULT NULL,
  `Activo` varchar(1) NOT NULL,
  PRIMARY KEY (`CodigoIdioma`),
  UNIQUE KEY `CodigoIdioma` (`CodigoIdioma`),
  UNIQUE KEY `NombreIdioma` (`NombreIdioma`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `catidiomaidioma`
--

CREATE TABLE `catidiomaidioma` (
  `CATIdiomaIdiomaID` int(11) NOT NULL AUTO_INCREMENT,
  `CodigoIdiomaOrigen` varchar(8) NOT NULL,
  `CodigoIdiomaDestino` varchar(8) NOT NULL,
  `ValorIdioma` varchar(128) NOT NULL,
  PRIMARY KEY (`CATIdiomaIdiomaID`),
  KEY `CodigoIdiomaOrigen` (`CodigoIdiomaOrigen`),
  KEY `FK_IdiomaIdiomaDestino` (`CodigoIdiomaDestino`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Table structure for table `catmoneda`
--

CREATE TABLE `catmoneda` (
  `CodigoMoneda` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `SimboloMoneda` varchar(6) NOT NULL COMMENT 'Símbolo',
  `NombreMoneda` varchar(64) NOT NULL,
  `NombreEN` varchar(64) DEFAULT NULL COMMENT 'Nombre moneda en inglés',
  `CodigoEstandar` char(3) NOT NULL COMMENT 'Códido para moneda según estandar ISO 4217.',
  PRIMARY KEY (`CodigoMoneda`),
  UNIQUE KEY `CodigoEstandar` (`CodigoEstandar`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Almacena los diferentes monedas. ';

--
-- Table structure for table `catpais`
--

CREATE TABLE `catpais` (
  `ZipCode` varchar(10) NOT NULL,
  `Pais` varchar(255) DEFAULT NULL COMMENT 'Nombre del país',
  `CodigoMoneda` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Llave foránea a tabla catmoneda.',
  PRIMARY KEY (`ZipCode`),
  UNIQUE KEY `ZipCode` (`ZipCode`),
  KEY `CodigoMoneda` (`CodigoMoneda`),
  CONSTRAINT `catpais_ibfk_1` FOREIGN KEY (`CodigoMoneda`) REFERENCES `catmoneda` (`CodigoMoneda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Codigo de paises con su zipcode';

--
-- Table structure for table `cattipoenlista`
--

CREATE TABLE `cattipoenlista` (
  `CodigoEnlista` varchar(4) NOT NULL,
  `NombreEnlista` varchar(16) DEFAULT NULL,
  `MostrarBusqueda` varchar(1) DEFAULT NULL,
  `MostrarAgregar` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`CodigoEnlista`),
  UNIQUE KEY `CodigoEnlista` (`CodigoEnlista`),
  UNIQUE KEY `NombreEnlista` (`NombreEnlista`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `fb_page`
--

CREATE TABLE `fb_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) unsigned DEFAULT NULL,
  `pageid` bigint(20) unsigned DEFAULT NULL,
  `dateIngreso` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_page_idx` (`uid`,`pageid`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

--
-- Table structure for table `geoipinfodb_ipcities`
--

CREATE TABLE `geoipinfodb_ipcities` (
  `ipfrom` int(11) unsigned zerofill NOT NULL DEFAULT '00000000000',
  `ipto` int(11) unsigned zerofill NOT NULL DEFAULT '00000000000',
  `cc` char(2) CHARACTER SET latin1 DEFAULT NULL,
  `country` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `region` varchar(128) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(128) CHARACTER SET latin1 DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ipfrom`,`ipto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `geolite_country`
--

CREATE TABLE `geolite_country` (
  `start_ip` char(15) NOT NULL,
  `end_ip` char(15) NOT NULL,
  `start_num` int(10) unsigned NOT NULL,
  `end_num` int(10) unsigned NOT NULL,
  `cc` char(2) NOT NULL,
  `cn` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `geolitecity_blocks`
--

CREATE TABLE `geolitecity_blocks` (
  `startIPNum` int(10) unsigned NOT NULL,
  `endIPNum` int(10) unsigned NOT NULL,
  `locID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`startIPNum`,`endIPNum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=1 DELAY_KEY_WRITE=1;

--
-- Table structure for table `geolitecity_location`
--

CREATE TABLE `geolitecity_location` (
  `locID` int(10) unsigned NOT NULL,
  `country` char(2) DEFAULT NULL,
  `region` char(2) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `postalCode` char(7) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `dmaCode` char(3) DEFAULT NULL,
  `areaCode` char(3) DEFAULT NULL,
  PRIMARY KEY (`locID`),
  KEY `Index_Country` (`country`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` bigint(20) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `youvideo` varchar(200) DEFAULT NULL,
  `twitter` varchar(50) DEFAULT NULL,
  `telefono` varchar(16) DEFAULT NULL,
  `webpage` varchar(100) DEFAULT NULL,
  `telefonoempresa` varchar(16) DEFAULT NULL,
  `nombreempresa` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pageid_idx` (`pageid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Table structure for table `page_service_status`
--

CREATE TABLE `page_service_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador.',
  `pageId` bigint(20) NOT NULL COMMENT 'Identidicado del Fan Page.',
  `pageName` varchar(256) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT 'Fanpage name.',
  `serviceType` int(10) unsigned NOT NULL COMMENT 'Tipo de servicio brindado. Ej. 1 = tab en fan page.',
  `serviceStatus` int(10) unsigned NOT NULL COMMENT 'Estado del servicio. Ej. 1 = habilitado.',
  `serviceStartDate` datetime DEFAULT NULL COMMENT 'Fecha en que el servicio se comienza a brindar.',
  `serviceExpirationDate` datetime DEFAULT NULL COMMENT 'Fecha en que el servicio se termina de brindar.',
  `hmNotification` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Código de notificación a Housemarket. Ej. 1 = notificación inicial enviada.',
  `clientNotification` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Código de notificación al cliente. Ej. 1 = notificación inicial enviada.',
  `requestDate` datetime DEFAULT NULL COMMENT 'Fecha en que solicitó el tab.',
  PRIMARY KEY (`id`),
  KEY `pageId` (`pageId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Almacena información sobre los servicios ofrecidos a las fan pages.';

--
-- Table structure for table `pais`
--

CREATE TABLE `pais` (
  `zip_code` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `pais` varchar(200) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='InnoDB free: 8192 kB';

--
-- Table structure for table `parametros`
--

CREATE TABLE `parametros` (
  `CodigoParametro` smallint(6) NOT NULL AUTO_INCREMENT,
  `NombreParametro` varchar(128) NOT NULL COMMENT 'Nombre del parámetro. Usar Máyúsculas.',
  `ValorParametro` varchar(64) NOT NULL,
  PRIMARY KEY (`CodigoParametro`),
  UNIQUE KEY `CodigoParametro` (`CodigoParametro`),
  UNIQUE KEY `NombreParametro` (`NombreParametro`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='Almacena valores para los parámetros que deben controlar asp';

--
-- Table structure for table `phfiletypes`
--

CREATE TABLE `phfiletypes` (
  `extension` char(7) NOT NULL DEFAULT '',
  `mime` char(30) DEFAULT NULL,
  `content` char(15) DEFAULT NULL,
  `player` varchar(5) DEFAULT NULL,
  `disabled` bigint(20) DEFAULT '1',
  PRIMARY KEY (`extension`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Used to store the file extensions';

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `CodigoCliente` int(11) NOT NULL AUTO_INCREMENT,
  `NombreCliente` varchar(64) NOT NULL,
  `TelefonoCliente` varchar(16) DEFAULT NULL,
  `WebPage` varchar(255) DEFAULT NULL,
  `EMail` varchar(255) DEFAULT NULL,
  `NombreEmpresa` varchar(255) DEFAULT NULL,
  `Uid` bigint(20) NOT NULL COMMENT 'Identificador Facebook',
  `FechaRegistro` datetime NOT NULL,
  `FechaCambio` datetime DEFAULT NULL COMMENT 'Fecha de último cambio',
  `TelefonoEmpresa` varchar(16) DEFAULT NULL,
  `Origen` varchar(8) DEFAULT NULL,
  `CargaMasiva` bit(1) DEFAULT b'0' COMMENT 'Indica si el cliente tiene permiso para acceder a la opción de carga masiva. Por default el valor es 0-> sin permiso. ',
  `FechaOtorgaCarga` date DEFAULT NULL COMMENT 'Fecha en que el administrador otorga el permiso de acceder a la funcionalidad de carga masiva',
  `FechaRevocaCarga` date DEFAULT NULL COMMENT 'Fecha en que el administrador le revoca el permiso para acceder a la funcionalidad de carga masiva.',
  `Broker` bit(1) DEFAULT b'0' COMMENT 'Indica si el cliente tiene permiso de Broker, lo cual le permite registrar más de 3 propiedades. Valores 0=No tiene permisos;  1=permisos de Broker.',
  `FechaSolicitaCarga` date DEFAULT NULL COMMENT 'Fecha en que el cliente solicita carga masiva',
  `profilePageID` bigint(20) DEFAULT NULL,
  `suscribe` bit(1) NOT NULL DEFAULT b'1',
  `youVideo` varchar(100) DEFAULT NULL,
  `twitter` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`CodigoCliente`),
  UNIQUE KEY `CodigoCliente` (`CodigoCliente`)
) ENGINE=InnoDB AUTO_INCREMENT=470 DEFAULT CHARSET=latin1 COMMENT='Datos de los usuarios que publican propiedades';

--
-- Table structure for table `representante`
--

CREATE TABLE `representante` (
  `CodigoRepresenta` smallint(6) NOT NULL AUTO_INCREMENT,
  `NombreRepresenta` varchar(64) NOT NULL,
  `NombreEmpresa` varchar(255) DEFAULT NULL,
  `Direccion` varchar(255) NOT NULL,
  `ZipCode` varchar(10) NOT NULL,
  `Telefono` varchar(16) NOT NULL,
  `EMail` varchar(255) DEFAULT NULL,
  `Fax` varchar(16) DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL COMMENT 'Indica el usuario que está asociado al representante para ingresar a la app de administración de pedidos',
  `Uid` bigint(20) NOT NULL COMMENT 'Identificador de Facebook. Unico.',
  PRIMARY KEY (`CodigoRepresenta`),
  UNIQUE KEY `Uid` (`Uid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='Datos de los representantes para HApp';

--
-- Table structure for table `crecredito`
--

CREATE TABLE `crecredito` (
  `CodigoCredito` int(11) NOT NULL AUTO_INCREMENT,
  `FechaCompra` datetime DEFAULT NULL,
  `FechaVence` date DEFAULT NULL,
  `Cantidad` smallint(6) NOT NULL COMMENT 'Cantidad de créditos comprados',
  `Saldo` smallint(6) DEFAULT NULL COMMENT 'Cantidad de créditos pendientes de asignar a las propiedades',
  `EstadoCredito` varchar(1) NOT NULL COMMENT 'Indica el estado de compra de Créditos.  P: pendiente de autorizar, se asigna al momento de registrar el Pedido. A: autorizada, se asigna una vez que el administrador autoriza la compra V: vencido se asigna cuando fecha actual=fecha compra+30dias',
  `CodigoRepresentante` smallint(6) DEFAULT NULL,
  `CodigoCliente` int(11) NOT NULL,
  `FechaPedido` date DEFAULT NULL COMMENT 'Fecha en que se registra la compra-pedido. ',
  `MontoCompra` decimal(8,2) DEFAULT NULL COMMENT 'Monto en USA que corresponde a la cantidad de créditos adquiridos.',
  `TipoCompra` varchar(16) DEFAULT NULL COMMENT 'Valores: ONLINE y Representante',
  `CodigoPromocion` int(11) DEFAULT '0',
  PRIMARY KEY (`CodigoCredito`),
  KEY `CodigoCliente` (`CodigoCliente`),
  KEY `CodigoRepresentante` (`CodigoRepresentante`),
  CONSTRAINT `fk_cliente_credito` FOREIGN KEY (`CodigoCliente`) REFERENCES `cliente` (`CodigoCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_representante_credito` FOREIGN KEY (`CodigoRepresentante`) REFERENCES `representante` (`CodigoRepresenta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1 COMMENT='Compra de creditos por un cliente';

--
-- Table structure for table `propropiedad`
--

CREATE TABLE `propropiedad` (
  `CodigoBroker` varchar(16) DEFAULT NULL COMMENT 'Código asignado y manejado por los clientes tipo Broker que les permite identificar sus propiedades (carga masiva)',
  `CodigoPropiedad` int(11) NOT NULL AUTO_INCREMENT,
  `NombrePropiedad` varchar(128) NOT NULL COMMENT 'Nombre de la propiedad',
  `DescPropiedad` varchar(512) DEFAULT NULL COMMENT 'Descripción adicional al nombre, que detalle las características de las propiedad',
  `Direccion` varchar(255) DEFAULT NULL COMMENT 'Detalle de la ubicación de la propiedad',
  `Estado` varchar(64) DEFAULT NULL COMMENT 'Nombre del estado (región) donde se ubica la propiedad',
  `Ciudad` varchar(64) DEFAULT NULL COMMENT 'Nombre de la ciudad donde se ubica la propiedad',
  `ZipCode` varchar(64) DEFAULT NULL,
  `Latitud` double DEFAULT NULL,
  `Longitud` double DEFAULT NULL,
  `CodigoCategoria` smallint(6) NOT NULL COMMENT 'Indica la categoría de la propiedad.',
  `FechaRegistro` datetime NOT NULL COMMENT 'Tomar de la fecha de la base de datos.',
  `FechaPublica` datetime DEFAULT NULL COMMENT 'Fecha en que se publica el anuncio de la propiedad.',
  `FechaModifica` datetime DEFAULT NULL COMMENT 'Fecha en que se realiza la última modificación al registro de la propiedad.',
  `Accion` varchar(1) NOT NULL COMMENT 'Indica si la propiedad está para Venta (S) o  Alquiler (R) o Ambas (B)',
  `CodigoMoneda` smallint(6) DEFAULT '1' COMMENT 'Identificador de la moneda en la cual se está especificando el precio de venta/alquiler',
  `PrecioVenta` float DEFAULT NULL,
  `PrecioAlquiler` float DEFAULT NULL,
  `Uid` bigint(20) NOT NULL COMMENT 'Identificador del usuario de facebook.',
  `CodigoEdoPropiedad` tinyint(4) NOT NULL COMMENT 'Identificador del estado en que se encuentra la propiedad.',
  `CargaMasiva` bit(1) NOT NULL DEFAULT b'0',
  `Calle` varchar(32) DEFAULT NULL,
  `CodigoPostal` varchar(16) DEFAULT NULL,
  `AreaLote` decimal(8,2) DEFAULT NULL COMMENT 'Tamaño del terreno o lote. Es opcional.',
  `Area` decimal(8,2) DEFAULT NULL COMMENT 'Tamaño de la propiedad.  Area construida. Es opcional.',
  `UnidadMedida` varchar(3) DEFAULT NULL COMMENT 'Valores: mt2, vr2, ft2. Unidad de medida usada para el area de la propiedad.',
  `AreaLoteMt2` decimal(8,2) DEFAULT NULL COMMENT 'Equivalencia en mt2 del area del lote almacenada en el campo AreaLote.',
  `AreaMt2` decimal(8,2) DEFAULT NULL COMMENT 'Equivalencia en mt2 del area de la propiedad almacenada en el campo Area.',
  `UnidadMedidaLote` varchar(3) DEFAULT NULL COMMENT 'Unidad de medida en que se indica el tamaño del terreno/lote. Valores vr2, mt2, ft2',
  `FechaInactiva` datetime DEFAULT NULL COMMENT 'Fecha y hora en que la propiedad se deja en estado Inactiva, porque ya se venció el plazo de 3 meses desde su publicación',
  PRIMARY KEY (`CodigoPropiedad`),
  UNIQUE KEY `CodigoPropiedad` (`CodigoPropiedad`),
  KEY `CodigoCategoria` (`CodigoCategoria`),
  KEY `CodigoEdoPropiedad` (`CodigoEdoPropiedad`),
  CONSTRAINT `propropiedad_ibfk_1` FOREIGN KEY (`CodigoEdoPropiedad`) REFERENCES `catedopropiedad` (`CodigoEdoPropiedad`)
) ENGINE=InnoDB AUTO_INCREMENT=15228 DEFAULT CHARSET=latin1 COMMENT='Datos generales de las propiedades que son publicadas en el ';

--
-- Table structure for table `crecreditopropiedad`
--

CREATE TABLE `crecreditopropiedad` (
  `CodigoCreditoProp` int(11) NOT NULL AUTO_INCREMENT,
  `CodigoPropiedad` int(11) NOT NULL,
  `CodigoCredito` int(11) NOT NULL,
  `FechaAsigna` datetime DEFAULT NULL,
  `FechaRetira` datetime DEFAULT NULL,
  `Cantidad` smallint(6) NOT NULL,
  `EstadoAsigna` varchar(1) NOT NULL COMMENT 'P: Pendiente de aprobar\r\nA: Activa, cuando lo aprueban\r\nV: Vencido, cuando se cumple el plazo',
  PRIMARY KEY (`CodigoCreditoProp`),
  KEY `CodigoCredito` (`CodigoCredito`),
  KEY `CodigoPropiedad` (`CodigoPropiedad`),
  CONSTRAINT `fk_credito_creditopropiedad` FOREIGN KEY (`CodigoCredito`) REFERENCES `crecredito` (`CodigoCredito`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_propiedad_creditopropiedad` FOREIGN KEY (`CodigoPropiedad`) REFERENCES `propropiedad` (`CodigoPropiedad`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1 COMMENT='Distribución de cada crédito en las diferentes propiedades d';



--
-- Table structure for table `proatributopropiedad`
--

CREATE TABLE `proatributopropiedad` (
  `CodigoPropiedad` int(11) NOT NULL,
  `CodigoAtributo` smallint(6) NOT NULL,
  `ValorEntero` int(11) DEFAULT NULL,
  `ValorDecimal` decimal(10,2) DEFAULT NULL,
  `ValorCaracter` varchar(255) DEFAULT NULL,
  `ValorS_N` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`CodigoPropiedad`,`CodigoAtributo`),
  KEY `CodigoAtributo` (`CodigoAtributo`),
  KEY `CodigoPropiedad` (`CodigoPropiedad`),
  CONSTRAINT `fk_atributo_atributopropiedad` FOREIGN KEY (`CodigoAtributo`) REFERENCES `catatributo` (`CodigoAtributo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_propiedad_atributopropiedad` FOREIGN KEY (`CodigoPropiedad`) REFERENCES `propropiedad` (`CodigoPropiedad`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Almacena el detalle de las características de la propiedad e';

--
-- Table structure for table `probeneficiopropiedad`
--

CREATE TABLE `probeneficiopropiedad` (
  `CodigoPropiedad` int(11) NOT NULL,
  `CodigoBeneficio` smallint(6) NOT NULL,
  PRIMARY KEY (`CodigoPropiedad`,`CodigoBeneficio`),
  KEY `CodigoBeneficio` (`CodigoBeneficio`),
  KEY `CodigoPropiedad` (`CodigoPropiedad`),
  CONSTRAINT `fk_beneficio_beneficiopropiedad` FOREIGN KEY (`CodigoBeneficio`) REFERENCES `catbeneficio` (`CodigoBeneficio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_propiedad_beneficiopropiedad` FOREIGN KEY (`CodigoPropiedad`) REFERENCES `propropiedad` (`CodigoPropiedad`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Almacena el detalle de los beneficios que tiene la propiedad';

--
-- Table structure for table `profoto`
--

CREATE TABLE `profoto` (
  `PROFotoID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CodigoPropiedad` int(10) unsigned DEFAULT NULL,
  `Principal` bit(1) NOT NULL,
  `FotoGrande` mediumblob NOT NULL,
  `Comentario` varchar(256) DEFAULT NULL,
  `AltoFotoGrande` int(10) unsigned NOT NULL,
  `AnchoFotoGrande` int(10) unsigned NOT NULL,
  `FotoDestacada` blob,
  `FotoResultado` blob,
  `FotoGaleria` blob NOT NULL,
  `Mime` varchar(15) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModifica` datetime DEFAULT NULL,
  PRIMARY KEY (`PROFotoID`),
  KEY `IDX_PROFOTO_CODIGOPROPIEDAD` (`CodigoPropiedad`)
) ENGINE=InnoDB AUTO_INCREMENT=34632 DEFAULT CHARSET=latin1;

--
-- Table structure for table `promocion`
--

CREATE TABLE `promocion` (
  `CodigoPromocion` int(11) NOT NULL AUTO_INCREMENT,
  `NombrePromocion` varchar(255) DEFAULT NULL,
  `Descripcion` varchar(50) DEFAULT NULL,
  `CantidadCredito` int(11) DEFAULT NULL,
  `FehaPromo` date DEFAULT NULL,
  `FechaVence` date DEFAULT NULL,
  `CodigoEdoPromocion` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`CodigoPromocion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Table structure for table `prompage`
--

CREATE TABLE `prompage` (
  `CodigoReference` int(11) NOT NULL AUTO_INCREMENT,
  `Uid` bigint(20) DEFAULT NULL,
  `PageReference` bigint(20) DEFAULT NULL,
  `EmailReference` varchar(30) DEFAULT NULL,
  `FechaRegistro` date DEFAULT NULL,
  PRIMARY KEY (`CodigoReference`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Table structure for table `prop_favoritas`
--

CREATE TABLE `prop_favoritas` (
  `idprop_favoritas` int(11) NOT NULL,
  `idpropiedad` int(11) NOT NULL,
  `uid` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`idprop_favoritas`,`idpropiedad`,`uid`),
  KEY `fk_prop_favoritas_propiedad` (`idpropiedad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `prop_foto_album`
--

CREATE TABLE `prop_foto_album` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `locacion` varchar(255) DEFAULT NULL COMMENT 'El link hacia el directorio del album',
  `idpropiedad` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT '0',
  `creado` datetime DEFAULT NULL,
  `modificado` datetime DEFAULT NULL,
  `fbUser_id` bigint(20) unsigned DEFAULT NULL,
  `img_default` varchar(200) DEFAULT '/img/no_img_full.gif',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid` (`aid`),
  KEY `fk_prop_foto_propiedad` (`idpropiedad`),
  CONSTRAINT `fk_album_propiedad` FOREIGN KEY (`idpropiedad`) REFERENCES `propropiedad` (`CodigoPropiedad`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8;

--
-- Table structure for table `prop_foto`
--

CREATE TABLE `prop_foto` (
  `fid` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(100) DEFAULT NULL,
  `creada` datetime DEFAULT NULL,
  `modificada` datetime DEFAULT NULL,
  `src` varchar(255) DEFAULT '/img/no_img_full.gif' COMMENT 'Fotos Normal sin thumb de la propiedad',
  `src_height` int(11) DEFAULT '200',
  `src_width` int(11) DEFAULT '400',
  `src_destacadas_small` varchar(255) DEFAULT '/img/no_img_full.gif' COMMENT 'Propiedades destacadas small',
  `src_destacadas_smal_height` int(11) DEFAULT '40',
  `src_destacadas_small_width` int(11) DEFAULT '75',
  `src_destacadas_normal` varchar(255) DEFAULT '/img/no_img_full.gif' COMMENT 'Propiedades destacadas tamaño normal',
  `src_destacadas_normal_height` int(11) DEFAULT '81',
  `src_destacadas_normal_width` int(11) DEFAULT '182',
  `src_medium` varchar(255) DEFAULT '/img/no_img_full.gif' COMMENT 'Propiedades cercanas a la propiedad',
  `src_medium_height` int(11) DEFAULT '82',
  `src_medium_width` int(11) DEFAULT '123',
  `prop_foto_album_aid` int(11) DEFAULT NULL,
  `habilitado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fid`),
  KEY `fk_prop_foto_prop_foto_album` (`prop_foto_album_aid`),
  CONSTRAINT `fk_prop_foto_prop_foto_album` FOREIGN KEY (`prop_foto_album_aid`) REFERENCES `prop_foto_album` (`aid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=274 DEFAULT CHARSET=utf8;



--
-- Table structure for table `prop_video`
--

CREATE TABLE `prop_video` (
  `idvideo` int(11) NOT NULL,
  `video_file` varchar(255) DEFAULT NULL,
  `idpropiedad` int(11) DEFAULT NULL,
  PRIMARY KEY (`idvideo`),
  KEY `fk_prop_video_propiedad` (`idpropiedad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `propfotoalbum`
--

CREATE TABLE `propfotoalbum` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `locacion` varchar(255) DEFAULT NULL COMMENT 'El link hacia el directorio del album',
  `idpropiedad` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT '0',
  `creado` datetime DEFAULT NULL,
  `modificado` datetime DEFAULT NULL,
  `fbUser_id` bigint(20) unsigned DEFAULT NULL,
  `img_default` varchar(200) DEFAULT '/img/no_img_full.gif',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `aid` (`aid`),
  KEY `fk_prop_foto_propiedad` (`idpropiedad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `sessiondb`
--

CREATE TABLE `sessiondb` (
  `session_id` char(32) NOT NULL,
  `save_path` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `modified` int(11) DEFAULT NULL,
  `lifetime` int(11) DEFAULT NULL,
  `session_data` text,
  PRIMARY KEY (`session_id`,`save_path`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `user_pref`
--

CREATE TABLE `user_pref` (
  `uid` bigint(20) unsigned NOT NULL,
  `pais` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `usuario_app`
--

CREATE TABLE `usuario_app` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `rol` tinyint(4) DEFAULT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `clave` varchar(255) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `estatus` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='usuarios realbook.';

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) DEFAULT NULL COMMENT 'Se usa este campo con los valores "Administrador" que tiene permisos a todas las opciones de la Aplicación de Administración o bien "Representante" con permisos unicamente a opción de hacer pedidos',
  `usuario` varchar(100) DEFAULT NULL,
  `clave` varchar(255) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellido` varchar(100) DEFAULT NULL,
  `estatus` tinyint(4) DEFAULT '1',
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Table structure for table `catatributocategoria`
--

CREATE TABLE `catatributocategoria` (
  `CodigoCategoria` smallint(6) NOT NULL,
  `CodigoAtributo` smallint(6) NOT NULL,
  `OrdenPresenta` smallint(6) NOT NULL COMMENT 'Numeral que indica el orden en el que se van a presentar los atributos en el registro o consulta de propiedades que pertenezcan a esta categoría. ',
  `CampoDestino` varchar(64) NOT NULL COMMENT 'Indica el nombre del campo genérico de la tabla Propiedad donde se almacenara el detalle del atributo para una propiedad en particular. ',
  `Filtro` bit(1) DEFAULT b'0' COMMENT 'Indica si el campo será utilizado como filtro en las consultas de propiedades. Valor por omisión 0 que indica que no es filtro.',
  PRIMARY KEY (`CodigoCategoria`,`CodigoAtributo`),
  KEY `CodigoAtributo` (`CodigoAtributo`),
  KEY `CodigoCategoria` (`CodigoCategoria`),
  CONSTRAINT `fk_atributo_atribcategoria` FOREIGN KEY (`CodigoAtributo`) REFERENCES `catatributo` (`CodigoAtributo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_categoria_atribcategoria` FOREIGN KEY (`CodigoCategoria`) REFERENCES `catcategoria` (`CodigoCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Almacena los atributos que corresponden o definen una determ';

--
-- Table structure for table `catbeneficiocategoria`
--

CREATE TABLE `catbeneficiocategoria` (
  `CodigoCategoria` smallint(6) NOT NULL,
  `CodigoBeneficio` smallint(6) NOT NULL,
  PRIMARY KEY (`CodigoCategoria`,`CodigoBeneficio`),
  KEY `CodigoCategoria` (`CodigoCategoria`),
  KEY `CodigoBeneficio` (`CodigoBeneficio`),
  CONSTRAINT `fk_beneficio_beneficiocategoria` FOREIGN KEY (`CodigoBeneficio`) REFERENCES `catbeneficio` (`CodigoBeneficio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_categoria_beneficiocategoria` FOREIGN KEY (`CodigoCategoria`) REFERENCES `catcategoria` (`CodigoCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Almacena la asignación de los diferentes beneficios que pued';

