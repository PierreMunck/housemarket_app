<?php

/*require_once 'krumo/class.krumo.php';
require_once 'functions/debug.php';*/
require_once 'MainController.php';

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected $_logger;
    public $dbAdapter;
    
    protected function _initLogging() {
        $logger = new Zend_Log();

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/data/logs/app.log');
        //new Zend_Log_Writer_Firebug();
        $logger->addWriter($writer);

        if ('production' == $this->getEnvironment()) {
            $filter = new Zend_Log_Filter_Priority(Zend_Log::CRIT);
            $logger->addFilter($filter);
        }

        $this->_logger = $logger;
        Zend_Registry::set('log', $logger);
    }

    protected function _initConfig() {
        Zend_Registry::set('config', $this->getOptions());
    }

    protected function _initDB() {
        $resource = $this->getPluginResource('db');
        try {
            $this->dbAdapter = $resource->getDbAdapter();
            
            //$this->dbAdapter->query("SET NAMES UTF8");
            //$this->dbAdapter->query("SET CHARACTER SET UTF8");
            $this->dbAdapter->setFetchMode(Zend_Db::FETCH_OBJ);
            Zend_Registry::set('db', $this->dbAdapter);
            Zend_Db_Table_Abstract::setDefaultAdapter('db');
        } catch (Zend_Db_Adapter_Exception $e) {
            echo $e->getMessage();
            echo $e->getTraceAsString();
            die("Fallo de conexion, revisar host/username/password");
        } catch (Zend_Exception $e) {
            die('Fallo en la carga de la clase Adapter');
        }
        return;
    }

    protected function _initLanguage() {
        $info_locale="es";
        if (Zend_Registry::isRegistered('signed')) {
            $info = Zend_Registry::get('signed');
			if(isset($info['user']) && isset($info['user']['locale'])){
				$info_locale = $info['user']['locale'];
			}
        }
        $locale = new Zend_Locale($info_locale);

        $language = substr($locale, 0, 2);
        $appLanguages = array("en", "es");
        if (!in_array($language, $appLanguages)) {
            $language = "es";
        }

        $localeValue = $language;

        $locale = new Zend_Locale($localeValue);
        Zend_Registry::set('Zend_Locale', $locale);
        Zend_Registry::set('language', $language);

        $translationFile = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . $localeValue . '.inc.php';
        $translate = new Zend_Translate('array', $translationFile, $localeValue);
        Zend_Registry::set('Zend_Translate', $translate);
    }

    protected function _initViewSettings() {

        $this->bootstrap('view');

        $lang = Zend_Registry::get('language');

        $this->_view = $this->getResource('view');
        $this->view->addFilterPath('Hm/View/Filter', 'Hm_View_Filter');
        $this->view->setFilter('Translate');
        
        // add global helpers
        $this->_view->addHelperPath(APPLICATION_PATH . '/views/helpers', 'Zend_View_Helper');
        $this->_view->addHelperPath(APPLICATION_PATH . '/views/helpers', 'Dgt');

        // set encoding and doctype
        $this->_view->setEncoding('utf-8'); //iso-8859-1
        
        //$this->_view->doctype('XHTML1_RDFA');
        $this->_view->doctype('XHTML1_TRANSITIONAL');

        // set the content type and language
        $this->_view->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset=utf-8'); //iso-8859-1
        $this->_view->headMeta()->appendHttpEquiv('Content-Language', 'es-NI');

        // set css links and a special import for the accessibility styles
        $this->_view->headLink()->appendStylesheet('/css/facebook-styles.css');
		$this->_view->headScript()->appendFile("/js/lang/" . $lang . ".js?v=6");
		$this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
		$this->_view->headScript()->appendFile("/js/facebook.js?v=2");

        // setting the site in the title
        $this->_view->headTitle('Housemarket');

        // setting a separator string for segments:
        $this->_view->headTitle()->setSeparator(' - ');

        $d = 'Housebook es una aplicación sencilla y amigable que te ayuda a intercambiar información de bienes races, conectate con las personas que buscan comprar, vender, rentar y/o alquilar propiedades alrededor del mundo.';
        $this->_view->descripcion = $d;
        if(isset($_GET['noframe'])&&$_GET['noframe']==true){
            $this->bootstrap('layout');
            $layout=$this->getResource('layout');
            $layout->setLayout('noframe');
            $layout->enableLayout();
        }        
    }

}