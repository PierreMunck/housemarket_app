<?php
class Forms_Decorator_Attributes extends Zend_Form_Decorator_Abstract {

    /**
     * Construye el html de un elemento y lo adjunta en la parte correcta.
     * @param String $content Contenido al cual adherir el html del elemento
     * @param Zend_Form_Element $element Elemento al cual generar html
     * @return String El html a ensamblar
     */
    private function BuildElement($element) {
        $elementContent = "";
        if($element instanceof Zend_Form_SubForm) {
            $nameSubForm = $element->getLegend();
            $innerGroups = $element->getDisplayGroups();
            if(count($innerGroups)>0) {
                foreach($innerGroups as $name => $group) {
                    $named = str_replace(" ", "", $nameSubForm);
                    $elementContent .= "<div id='" . $named ."_" . $name ."' class=\"hide\">" . $name . "<br />";
                    $elements = $group->getElements();
                    if(count($elements) > 0) {
                        foreach($elements as $subelement) {
                            $elementContent .= $this->BuildElement($subelement);
                        }
                    }
                    $elementContent .= "</div>";
                }
            }
        } else if($element instanceof Zend_Form_Element){
            $elementContent = $element->render();
        }
        return $elementContent;
    }

    public function render($content) {
        $html = "";
        $element = $this->getElement();

        $html = $this->BuildElement($element);
        $separator = $this->getSeparator();
        $placement = $this->getPlacement();

        switch ($placement) {
            case 'APPEND':
                return $content . $separator . $html;
            case 'PREPEND':
                return $html . $separator . $content;
            case null:
            default:
                return $html;
        }
    }
}
?>