<?php

/**
 * Recupera parametros de la aplicacion.
 * Estos parametros estan almacenados en la base de datos
 */
class Hm_ApplicationParameter extends Zend_View_Helper_FormElement {

    public function __get($name){

        try {
            $db = Zend_Registry::get('db');
            $st = $db->query("SELECT CodigoParametro, NombreParametro, ValorParametro FROM parametros p where NombreParametro='$name'");
			$result = $st->fetchAll();
			foreach ($result as $key => $r){
				$result[$key] = get_object_vars($r);
			}
            return $result;
        }catch(Exception $ex) {

        }
        return null;
    }

}

?>
