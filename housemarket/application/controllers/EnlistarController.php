<?php

class EnlistarController extends MainController {

    protected $_flashMessenger = null;
    protected $_redirector = null;

    public function init() {
    	parent::init();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
        $this->view->web_path = $this->cfg['fb_app_url'];
    }

    public function preDispatch() {
        
    }

    /**
     *
     * fonction de retour de label de navigation courant
     */
    public function getCurrentLabelnav() {
    	return 'enlistar';
    }
    
    public function formAction() {

        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessión.");
            die;
        }

        $this->view->uid = $fb->get_uid();
        $this->view->appusers = $fb->getFriendsApp();

        $this->view->headTitle('Enlistar Propiedades');
        $this->view->loadmap = 'onunload="GUnload();"';

        $google_api_key = $this->cfg['google_api_key'];
        $this->view->headScript()->appendFile("https://maps.google.com/maps?file=api&v=2&sensor=false&key=$google_api_key", 'text/javascript');

        // minified and concatenated: ext-all.css, xtheme-gray.css
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/extjs-all.gray-theme.min.css?v=1');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');


        try {
            //Agregar primer combo del tipo pais
            $this->view->CboCountry = $this->getCboCountry("");
            $this->view->Cbotype_property = $this->getCbotype_property("");
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function checkLimit($uid) {
        $db = Zend_Registry::get('db');
        $params = array($uid);
        $count_sql = "SELECT COUNT(*) num_prop FROM propiedad WHERE uid= ?";
        $rs = $db->query($count_sql, $params);
        $data = $rs->fetchAll();
        $count = $data[0]->num_prop;

        if ($count >= 40) {
            return false;
        } else {
            return true;
        }
    }

    public function addAction() {

        $db = Zend_Registry::get('db');
        $f = new Zend_Filter_StripTags();

        if ($this->getRequest()->isGet()) {

            try {

                $Precio = $f->filter($this->_request->getParam('precio'));
                $insert_data_propiedad = array(
                    'tipo_enlistamiento' => (int) trim($f->filter($this->_request->getParam('tipo_enlistamiento'))),
                    'tipo_propiedad' => trim($f->filter($this->_request->getParam('type_property'))),
                    'direccion' => trim($f->filter($this->_request->getParam('direction_propiedad'))),
                    'pais' => trim($f->filter($this->_request->getParam('country'))),
                    'estado' => trim($f->filter($this->_request->getParam('estado'))),
                    'ciudad' => trim($f->filter($this->_request->getParam('ciudad'))),
                    'area' => (int) trim($f->filter($this->_request->getParam('area'))),
                    'cuartos' => (int) trim($f->filter($this->_request->getParam('cuarto'))),
                    'banos' => (int) trim($f->filter($this->_request->getParam('bano'))),
                    'tamano_lote' => trim($f->filter($this->_request->getParam('tamano_lote'))),
                    'zip_code' => trim($f->filter($this->_request->getParam('zip_code'))),
                    'lat' => trim($f->filter($this->_request->getParam('latitud'))),
                    'lng' => trim($f->filter($this->_request->getParam('longitud'))),
                    'uid' => trim($f->filter($this->_request->getParam('userid'))),
                    'fecha_ingreso' => new Zend_Db_Expr('CURDATE()'),
                    'estatus' => 3
                );


                $tipoPrecio = $f->filter($this->_request->getParam('tipo_enlistamiento'));


                if ($tipoPrecio == 1)
                    $insert_data_propiedad['precio_venta'] = $Precio;
                if ($tipoPrecio == 2)
                    $insert_data_propiedad['precio_alquiler'] = $Precio;
                if ($tipoPrecio == 3) {
                    $insert_data_propiedad['precio_venta'] = $Precio;
                    $insert_data_propiedad['precio_alquiler'] = $Precio;
                }

                $PropiedadEnlistar = new Propiedad();

                // @todo: agregar limit de propiedades a enlistar.
                // maximo = 25.
                // 1- verificar si el uid tienes propiedades enlistadas y si son <= 30
                // 2- si es igual a 30 - enviar mensaje de error y no continuar
                // 3- si es menor a 30 - continuar el registro.

                $uid = Zend_Registry::get('uid');
                if ($this->checkLimit($uid)) {
                    $PropiedadEnlistar->insert($insert_data_propiedad);
                    $idpropiedad = $db->lastInsertId();
                    Zend_Registry::set('message', "Su propiedad #$idpropiedad ha sido enlistada exitosamente");
                    $this->view->message = "Su propiedad #$idpropiedad ha sido enlistada exitosamente";
                    $this->_redirector->gotoUrl('https://apps.facebook.com/hmproject/Mihousebook/edit?propiedad=' . $idpropiedad);
                } else {
                    Zend_Registry::set('message', "No esta permitido enlistar mas de 30 propiedades");
                    $this->view->message = "No esta permitido enlistar mas de 30 propiedades";
                }

                exit();
            } catch (Exception $ex) {
                
            }
        } else {
            
        }
    }

    public function getCboCountry($selected) {
        //Agregando campo vacio
        $empty = new stdClass();
        $empty->id = '';
        $empty->label = 'Seleccione una opción';

        $option = Dgt_Enlistar::getPais();
        array_unshift($option, $empty);

        $cbo = array();
        foreach ($option as $index => $value) {
            if ($selected == $value->id)
                $cbo[] = sprintf("<option value='%s' selected='selected'>%s</option>", $value->id, $value->label);
            else
                $cbo[] = sprintf("<option value='%s'>%s</option>", $value->id, $value->label);
        }
        return join(" ", $cbo);
    }

    public function getCbotype_property($selected) {
        //Agregando campo vacio
        $empty = new stdClass();
        $empty->id = '';
        $empty->label = 'Seleccione una opción';

        $option = Dgt_Enlistar::gettype_property();
        array_unshift($option, $empty);

        $cbo = array();
        foreach ($option as $index => $value) {
            if ($selected == $value->id)
                $cbo[] = sprintf("<option value='%s' selected='selected'>%s</option>", $value->id, $value->label);
            else
                $cbo[] = sprintf("<option value='%s'>%s</option>", $value->id, $value->label);
        }
        return join(" ", $cbo);
    }

    public function centermapAction() {
        $ip = $this->_request->getClientIP();
        $location = Dgt_GeoData::getLocation($ip);

        //Zend_Debug::dump($location);
        if (!empty($location)) {
            echo '{"success":"true","data": ' . Zend_Json::encode($location) . '}';
        } else {
            echo '{"success":"false"}';
        }
        exit();
    }

    public function propiedadenmapaAction() {

        $this->_helper->viewRenderer->setNoRender();

        try {

            $f = new Zend_Filter_StripTags();
            $property = new Hm_Pro_Propiedad();
            $propId = $f->filter($this->getRequest()->getParam("valprop", null));
            $recordProperty = $property->GetPropiedad($propId); //Hm_Pro_Propiedad::GetDetailsLocationProperty($_GET("valprop"));
            $curProperty = $recordProperty->current();
            $locationProperty = Array('cc' => $curProperty->ZipCode, 'city' => $curProperty->Ciudad, 'zip' => '', 'lat' => $curProperty->Latitud, 'lng' => $curProperty->Longitud);
            $locationProperty = (object) $locationProperty;
            $locationProperty = array($locationProperty);

            if (!empty($locationProperty)) {
                echo '{"success":"true","data": ' . Zend_Json::encode($locationProperty) . '}';
            } else {
                $ip = $this->_request->getClientIP();
                $location = Dgt_GeoData::getLocation($ip);
                if (!empty($location)) {
                    echo '{"success":"true","data": ' . Zend_Json::encode($location) . '}';
                } else {
                    echo '{"success":"false"}';
                }
                //    exit();
            }
            exit();
        } catch (Exception $e) {
            echo "Error";
        }
    }

    public function updatepropAction() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessión.");
            die;
        }
        $urlretorno = '';
        if (!empty($_GET['id'])) {
            $IdProp = intval($_GET['id']);
            if (is_numeric($IdProp)) {
                $urlretorno = "/enlistar/perfil?valprop=$IdProp" . "&" . $_SERVER['QUERY_STRING'];
                $db = Zend_Registry::get('db');
                $actualizar = array('CodigoEdoPropiedad' => 2);
                $fb = Dgt_Fb::getInstance();
                $uid = $fb->get_uid();
                try {
                    $db->update('propropiedad', $actualizar, 'Uid=' . $uid . ' and Latitud is not null and Longitud is not null and CodigoPropiedad=' . $IdProp);
                    $this->_flashMessenger->addSuccess("<i18n>MSG_PROPERTY_PUBLISH</i18n>");
                } catch (Exception $e) {
                    $this->_flashMessenger->addError("<i18n>LABEL_ERROR_PROPERTY</i18n>.");
                }
            }
            else
                $urlretorno="/mihousebook/publicacion?" . $_SERVER['QUERY_STRING'];
        }
        else {
            $urlretorno = "/mihousebook/publicacion?" . $_SERVER['QUERY_STRING'];
        }
        $this->_redirector->gotoUrl($urlretorno);
    }

    // send email
    public function sendavisoAction() {

        // Variables de acceso
        $cfg = Zend_Registry::get('config');
        $uid = Zend_Registry::get('uid');
        $db = Zend_Registry::get('db');
        $signed = Zend_Registry::get('signed');
        //Propietario
        $ownid = $this->_request->getPost('ownid', '');

        //Datos del visitante
        $profile = new stdClass();
        $profile->to = $this->_request->getPost('to', '');
        $profile->toname = $this->_request->getPost('toname', '');
        $profile->tocel = $this->_request->getPost('tocel', '');
        $profile->tocomment = $this->_request->getPost('tocomment', '');

        //Datos de la propiedad
        $url = $this->_request->getPost('response', "");
        $id = $this->_request->getPost('id', 0);
        $photo = $this->_request->getPost('photo');
        $property = base64_decode($this->_request->getPost('property', 0));
        $location = base64_decode($this->_request->getPost('location', 0));
        $origin = $this->_request->getPost('origin', 0);

        //Si existe el propietario
        if ($ownid) {

            //Extrayendo datos de los propietario, contamos con permiso de email

            $sql = sprintf("SELECT nombrecliente as name,email,profilepageid FROM cliente WHERE Uid =%s", $ownid);
            $stmt = $db->query($sql);
            $objOwner = $stmt->fetch();

            if (!empty($objOwner->profilepageid)) {
                $sql = sprintf("SELECT nombre as name,email FROM page WHERE pageid =%s", $objOwner->profilepageid);
                $stmt = $db->query($sql);
                $objOwner = $stmt->fetch();
            }
            $objOwner->email = trim($objOwner->email);
            $ownprofile = $this->fbDgt->execFql('SELECT first_name, name,email FROM user WHERE uid=' . $ownid);
            $ownprofile = $ownprofile[0];
            $objOwner->fname = (isset($ownprofile['first_name']) && !empty($ownprofile['first_name'])) ? $ownprofile['first_name'] : $objOwner->name;
            if (!$objOwner || empty($objOwner->email)) {
                $objOwner->email = trim($ownprofile['email']);
            }

            //Buscamos la informacion del visitante, si no viene completa
            if (!$profile->to || empty($profile->to)) {
                $sql = sprintf("SELECT email FROM cliente WHERE Uid =%s", $uid);
                $stmt = $db->query($sql);
                $objCliente = $stmt->fetch();
                if ($objCliente) {
                    $profile->to = trim($objCliente->email);
                } else {
                    $profilefb = $this->fbDgt->execFql('SELECT first_name, name,email FROM user WHERE uid=' . $uid);
                    $profilefb = $profilefb[0];
                    $profile->to = trim($profilefb['email']);
                }
            }
            
            if($origin === "roommate"){
                $subjectEmail = " likes your profile";
                $renderBody = "likesroommate.phtml";
            }else{
                $subjectEmail = " likes your property";
                $renderBody = "likes.phtml";
            }

            try {

                //Armando correo con plantilla likes.phtml
                $html = new Zend_View();
                $html->setScriptPath(APPLICATION_PATH . '/views/scripts/emails/');
                // assign valeues
                $html->broker = $objOwner->fname;
                $html->name = $profile->toname;
                $html->email = $profile->to;
                $html->phone = $profile->tocel;
                $html->message = $profile->tocomment;
                $html->link = $url;
                $html->id = $id;
                $html->uid = $uid;
                $html->property = $property;
                $html->photo = $photo;
                $html->location = $location;
                $html->fburl = $cfg['fb_app_url'];
                $body = $html->render($renderBody);

                $textbody = preg_replace("'<style[^>]*>.*</style>'siU", '', $body);
                $textbody = trim(strip_tags($textbody));


                //Configuracion del servidor smtp
                $smtpServer = $cfg['gmail_smtpserver'];
                $username = $cfg['gmail_username'];
                $password = $cfg['gmail_password'];

                $config = array('ssl' => 'tls',
                    'port' => 587,
                    'auth' => 'login',
                    'username' => $username,
                    'password' => $password);


                if (!$objOwner || empty($objOwner->email)) {
                    throw new Exception("The property email is empty");
                }
                if (!$profile->to || empty($profile->to)) {
                    throw new Exception("The visitor email is empty");
                }
                $transport = new Zend_Mail_Transport_Smtp($smtpServer, $config);

                $mail = new Zend_Mail('UTF-8');
                $mail->setFrom($username, "Housemarket Notification");
                $mail->setReplyTo($username, "No-Reply");
                $mail->setReturnPath($username, "No-Reply");
                $mail->addTo($objOwner->email, $objOwner->name);
                //$mail->addBcc($cfg['info_email']);
                $mail->addBcc('hmappfb@gmail.com');
                $mail->setSubject($profile->toname . $subjectEmail);
                $mail->setBodyHtml($body);
                $mail->setBodyText($textbody);

                $status = $mail->send($transport);
                $this->_flashMessenger->addSuccess('');
            } catch (Exception $e) {
                $this->_flashMessenger->addError($e->getMessage());
            }
        }
        echo $this->view->flashMessenger();
        $this->_flashMessenger->clearMessages();
        exit();
    }

    // prompts the user
    public function sendinquiryAction() {
        $this->view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js', 'text/javascript');
        $this->view->headScript()->appendFile("/js/validate/jquery.validate1.8.1.min.js", "text/javascript");
        $this->view->headScript()->appendFile("/js/perfil.js", "text/javascript");
        // minified and concatenated: send-inquiry.css
        $this->view->headLink()->appendStylesheet("/css/sendinquiry.min.css?v=1");
        
        
        $userInformation = $this->fbDgt->getUserInformation();
        $this->view->userInformation = $userInformation;

        $publisherName = $_GET['name'];

        if (isset($publisherName) && !empty($publisherName)) {
            $this->view->message = '<i18n>MSG_WE_WILL_NOTIFY</i18n> <strong>' . $publisherName . '</strong> <i18n>MSG_YOUR_INTEREST_IN_THIS_AD</i18n>';
        } else {
            $this->view->message = '<i18n>MSG_WE_WILL_NOTIFY_THE_PUBLISHER_YOUR_INTEREST_IN_THIS_AD</i18n>';
        }

        $this->view->signed = $this->_request->getParam('signed_request', '');
        $params = base64_decode($this->_request->getParam('params', ''));
        $this->view->params = json_decode($params);
    }

    public function perfilAction() {
    	
    	$this->currentActionNav = 'perfil';
    	$this->navAction();
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessión.");
            die;
        }

        $this->_flashMessenger->clearMessages();
        $this->view->appusers = $fb->getFriendsApp();
        $this->view->showResponde = false;

        $this->view->loadmap = 'onunload="GUnload();"';

        // API Key no longer required for Google Maps v3
        $google_api_key = $this->cfg['google_api_key'];
        $this->view->headScript()->appendFile("https://maps.google.com/maps?file=api&v=2&sensor=false&key=$google_api_key", 'text/javascript');
        $this->view->headScript()->appendFile("https://www.google.com/uds/api?file=uds.js&v=1.0&source=uds-msw&key=ABQIAAAAvCeUucOYY74-kYy_PW657BTXiVpCZbJoaksdLXfYkWMngqmDrxS6-TiTbctgJsKVCf909V4KNSXSaA");
        $this->view->headScript()->appendFile("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.js?mode=new");

        $this->view->headLink()->appendStylesheet("https://www.google.com/uds/css/gsearch.css");
        $this->view->headLink()->appendStylesheet("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.css");
        $this->view->headLink()->appendStylesheet("/css/static.ak.fbcdn.net.I0YvHLrgaAw.css");
        // minified and concatenated: ext-all.css, xtheme-gray.css        
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/extjs-all.gray-theme.min.css?v=1');
        // minified and concatenated: galleriffic.css
        $this->view->headLink()->appendStylesheet('/js/galleriffic/css/galleriffic.min.css');
        // minified and concatenated: jquery-ui-1.7.2.custom.css
        $this->view->headLink()->appendStylesheet('/css/custom-theme/jquery-ui-custom-theme.min.css');
        // minified and concatenated: colorbox.css
        $this->view->headLink()->appendStylesheet('/css/colorbox/colorbox.min.css');
        // minified and concatenated: style_form.css, list_autor_properties.css, errores.css, style_menu.css, messages.css, style_perfil.css
        $this->view->headLink()->appendStylesheet('/css/perfil.min.css?v=1');
        

        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        $this->view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery.colorbox-min.js?v=1.3.17.2', 'text/javascript');
        $this->view->headScript()->appendFile("/js/galleriffic/js/jquery.galleriffic.js");
        $this->view->headScript()->appendFile("/js/galleriffic/js/jquery.opacityrollover.js");
        //$this->view->headScript()->appendFile("/js/facebook.js");
        $this->view->headScript()->appendFile("/js/mapa_en_perfil.js?v=5");
        $this->view->headScript()->appendFile("/js/perfil.js?v=16");
        
        $this->view->headerMeta->setProperty('og:type','housemarket:casa');

        $db = Zend_Registry::get('db');
        $language = Zend_Registry::get('language');

        $urlhome = "/index?" . $_SERVER['QUERY_STRING'];

        if (!empty($_GET['valprop'])) {
            $IdProp = $_GET['valprop'];
            $userId = Zend_Registry::get('uid');
            if (!is_numeric($IdProp)) {
                $this->_redirector->gotoUrl($urlhome);
            }
        	
            // gerar los favoritos del usiario
            
            $favorito = new Hm_Pro_Favorito($userId,$IdProp);
            $this->view->isfavorit = $favorito->isfavorit();
            
            /* *************************** */
            $logged_in_user = Zend_Registry::get('user_info');
            $this->view->logged_in_user = $logged_in_user;

            $user_granted_permissions = Zend_Registry::get('user_granted_permissions');
            if ($user_granted_permissions['manage_pages'] === '0' || $user_granted_permissions['publish_stream'] === '0') {
                $this->view->request_perm_url = 'https://graph.facebook.com/oauth/authorize?client_id=' . $this->cfg['facebook_appid'] . ' &redirect_uri=' . urlencode($this->cfg['fb_app_url'] . 'enlistar/perfil?valprop= ' . $IdProp . '&permreq=1') . '&scope=manage_pages,publish_stream&type=user_agent&display=page';
            }
            else {
            $pages = Zend_Registry::get('pages');
                $pages = $pages->tabla;
                if (!empty($pages)) {
                    $this->view->pages = $pages;
                }
            }
            
            /* *************************** */
            
            $this->cfg = Zend_Registry::get('config');
            $propResult = Hm_Pro_Propiedad::ObtenerDatosProPropiedad($IdProp);

            if (count($propResult) <= 0) {
                $this->_redirector->gotoUrl($urlhome);
            }

            $this->view->fb_path = $this->cfg['fb_app_url'];
            $this->view->urlLocal = $this->cfg['local_app_url'];

            $curProperty = $propResult[0];
            $this->view->property = $curProperty;
            $this->view->headerMeta->setProperty('og:title', $curProperty->NombrePropiedad);
            $this->view->headerMeta->setProperty('og:image', $this->view->urlLocal .'/pic'. $curProperty->IdFoto .'_2.jpg');
            $this->view->headerMeta->setProperty('og:url', $this->cfg['fb_app_url'] .'enlistar/perfil?valprop='. $curProperty->CodigoPropiedad);
            $this->view->headerMeta->setProperty('og:description',$curProperty->DescPropiedad);
			// format valor de venta
			
			$this->view->property->PrecioVenta = number_format($this->view->property->PrecioVenta);
			$this->view->property->PrecioAlquiler = number_format($this->view->property->PrecioAlquiler);
			
            $this->view->IdProp = $IdProp;
            $addressProperty = array();
            if (trim($curProperty->Direccion) != "")
                $addressProperty[] = $curProperty->Direccion;

            $addressProperty = implode(",", $addressProperty);
            $this->view->addressProperty = (trim($addressProperty) != "") ? "<span>" . $addressProperty . "</span>" : "";

            $this->view->uid = $curProperty->Uid;
            //$this->view->showResponde = $fb->facebook->api(array('method' => 'users.hasapppermission', 'ext_perm' => 'email'));
            //"email", $this->view->uid);
            $this->view->totalMutualFriends = $fb->GetTotalMutualFriends($this->view->uid);


            $arrMutualFriends = $fb->GetMutualFriends($this->view->uid);

            if ($this->view->totalMutualFriends > 0) {
                foreach ($arrMutualFriends as &$friend) {
                    $friend['pic_square'] = (!empty($friend['pic_square'])) ? $friend['pic_square'] : "/img/q_silhouette.gif";
                    $friend['first_name'] = $friend['first_name'];
                    $friend['last_name'] = $friend['last_name'];
                }
            }

            $this->view->mutualFriends = $arrMutualFriends;


            //@TODO verificar
            //Uid para consultas
            $ListValue = $this->fbDgt->getDataUser($curProperty->Uid, array('first_name', 'name', 'pic_big', 'current_location', 'pic_small'));
            $location = '';
            $rutaimagen = $this->cfg['local_app_url'] . "img/q_silhouette.gif";
            $pic = $rutaimagen;
            if (is_array($ListValue)) {
            	//@TODO verificar esa error
            	if(isset($ListValue[0])){
            		$ListValue = $ListValue[0];
            	}
                $this->view->username = $ListValue['first_name'];
                $this->view->usernames = $ListValue['name'];
                if (strlen($ListValue['pic_big']) > 0) {
                    $pic = $ListValue['pic_big'];
                }

                $location = $ListValue['current_location'];
            }
            $this->view->profile = $pic;

            //Inicializando ids del usuario actual
            $verifUid = $fb->get_uid();
            $this->view->isadmin = false;

            //Si tiene activo el perfil de fanpages cambiar los datos de uid,nombre y pic_big
            $sql = "SELECT NombreCliente,TelefonoCliente,twitter,WebPage,EMail,NombreEmpresa,TelefonoEmpresa,profilepageid,youVideo 
                FROM cliente where Uid=" . $curProperty->Uid;
            $stmt = $db->query($sql);

            $resCliente = $stmt->fetch();
            $this->view->isActivePage = false;
            if ($resCliente && count($resCliente) > 0) {
                $this->view->nombre = $resCliente->NombreCliente;
                $this->view->email = $resCliente->EMail;
                $this->view->twitter = $resCliente->twitter;
                $this->view->youvideo = $resCliente->youVideo;
                $this->view->telefono = $resCliente->TelefonoCliente;
                $this->view->paginaweb = ltrim($resCliente->WebPage, "https://");
                $this->view->paginaweb = (!empty($this->view->paginaweb)) ? "https://" . $this->view->paginaweb : $this->view->paginaweb;
                $this->view->empresa = $resCliente->NombreEmpresa;
                $this->view->telempresa = $resCliente->TelefonoEmpresa;
                //Son iguales el id del usuario actual con el del cliente?
                $this->view->isadmin = ($verifUid == $curProperty->Uid);

                if (!empty($resCliente->profilepageid)) {
                    //Verificando si es administrador de la pagina
                    $sql = sprintf('SELECT uid FROM fb_page WHERE pageid = %s', $resCliente->profilepageid);
                    $rs = $db->fetchAll($sql);
                    $owners = array();
                    foreach ($rs as $id) {
                        $owners[] = $id->uid;
                    }
                    $this->view->isadmin = (in_array($verifUid, $owners));

                    $this->view->isActivePage = true;
                    $fql = "SELECT page_id,name,has_added_app,pic_big,fan_count FROM page WHERE page_id=" . $resCliente->profilepageid;

                    $fbpages = $this->fbDgt->execFql($fql);

                    if ($fbpages && count($fbpages) > 0) {
                        $pages = new Table($fbpages);
                        $filter = sprintf("{page_id} = %s", $resCliente->profilepageid);
                        $pages->filtrar($filter);
                        $page = array_values((array) $pages->obtener());
                        $page = $page[0];
                        //Filtrando datos con la tabla page
                        $sql = "SELECT nombre as NombreCliente,EMail,twitter,telefono AS TelefonoCliente,WebPage,NombreEmpresa,TelefonoEmpresa,youVideo FROM page WHERE pageid=" . $resCliente->profilepageid;
                        $stmt = $db->query($sql);
                        $resCliente = $stmt->fetch();
                        if ($resCliente && count($resCliente) > 0) {
                            $this->view->nombre = $resCliente->NombreCliente;
                            $this->view->email = $resCliente->EMail;
                            $this->view->twitter = $resCliente->twitter;
                            $this->view->telefono = $resCliente->TelefonoCliente;
                            $this->view->paginaweb = $resCliente->WebPage;
                            $this->view->empresa = $resCliente->NombreEmpresa;
                            $this->view->telempresa = $resCliente->TelefonoEmpresa;
                            $this->view->youvideo = $resCliente->youVideo;
                            $this->view->username = $page->name;
                            $this->view->usernames = $page->name;
                            $this->view->previd = $this->view->uid;
                            $this->view->uid = $page['page_id'];
                            $this->view->profile = $page['pic_big'];
                        }
                    }
                }
            }
            //Si es el propietario presenta botones

            if ($this->view->isadmin) {
                //$this->view->isadmin = true;
                $url_en_fbc = $this->cfg['fb_app_url'];
                $editar = $this->view->fb_path . "propiedades/show?id=" . $curProperty->CodigoPropiedad;
                $publicar = $this->view->fb_path . "enlistar/updateprop?id=" . $curProperty->CodigoPropiedad;
                $botonesprop = "<div class='button_group'>
                        <a href='$editar' class='button' id='bt2' name='bt' target='_top'><i18n>LABEL_EDIT</i18n></a> ";
                if ($curProperty->CodigoEdoPropiedad == 1)
                    $botonesprop.="<a href='$publicar' class='button' id='_publica' name='_publica' target='_top'><i18n>LABEL_PUBLISH</i18n></a>";

                $botonesprop.="</div>";
                $this->view->botonesprop = $botonesprop;
            }

            $objFoto = new Hm_Pro_Foto();
            $photosRS = $objFoto->fetchAll(
                            $objFoto->select()
                                    ->from(array('f' => 'profoto'), array('PROFotoID', 'Principal', 'Comentario'))
                                    ->where('CodigoPropiedad = ?', $curProperty->CodigoPropiedad)
                                    ->order('Principal DESC')
            );

            if ($photosRS != null && $photosRS->count() > 0) {
                $this->view->photos = $photosRS;
            }

            $sql = "SELECT count(CodigoPropiedad) as total from propropiedad where CodigoEdoPropiedad=2 and Uid=" . $curProperty->Uid;
            $TotProp = $db->fetchAll($sql);
            //url datos de lectura de las propiedades del autor
            $propiedadesautor = "enlistar/propiedadesautor?id=" . $curProperty->Uid;
            $this->view->cantprop = "<a href='" . $this->view->fb_path . $propiedadesautor . "' target='_top'> " . $TotProp[0]->total . " <i18n>LABEL_PUBLISHED_PROPERTIES</i18n> </a>";

            $caracteristicasprop = '';

            $atributos = $this->ObtenerDatosAtributos($curProperty->CodigoPropiedad);
            $attributeCodes = array();
            $attributeCodes[] = 0;

            $area = ($curProperty->Area > 0) ? $curProperty->Area . ' ' . $curProperty->UnidadMedida : "-";
            $lote = ($curProperty->AreaLote > 0) ? $curProperty->AreaLote . ' ' . $curProperty->UnidadMedidaLote : "-";
            $caracteristicasprop .='<tr><th><i18n>ADD_ESTATE_ADDITIONAL_INFO_AREA</i18n></th><td>' . $area . '</td></tr><tr><th><i18n>LABEL_SEARCH_FILTER_LOT_SIZE</i18n></th><td>' . $lote . '</td></tr>';
            $att = "br";
            $testprop = null;
            foreach ($atributos as $val) {
                $SiNo = '';
                if (strtoupper($val->ValorS_N) == 'S')
                    $SiNo = "<i18n>LABEL_YES</i18n>";
                else if (strtoupper($val->ValorS_N) == 'N')
                    $SiNo = "<i18n>LABEL_NO</i18n>";
                $attributeCodes[] = $val->CodigoAtributo;
                if ($val->CodigoAtributo < 3) {
                    if ($val->CodigoAtributo == 2) {
                        $att = "ba";
                    }
                    $float_redondeado = round($val->ValorDecimal);
                    if ($float_redondeado == 0) {
                        $testprop.= $val->ValorEntero . " " . $att . " | ";
                    } else {
                        $testprop.= $float_redondeado . " " . $att . " | ";
                    }
                }

                $labelAppend = '';
                if ($val->nombre === 'HOA') {
                    $labelAppend = ' <abbr title="<i18n>ISO_4217_CURRENCY_CODE_' . $curProperty->MonedaCodidoEstandar . '</i18n>">'
                            . $curProperty->MonedaCodidoEstandar . '</abbr>';
                }

                $caracteristicasprop.="<tr><th>" . $val->nombre . $labelAppend . '</th><td>' . $val->ValorEntero . $val->ValorDecimal . $SiNo . $val->ValorCaracter . "</td></tr> ";
            }

            $this->view->test = $testprop;
            //Obtiene los atributos que no aparecen en la consulta.
            $atributosRestantes = $this->ObtenerAtributosRestantes($attributeCodes);
            $caracteristicasprop .= $atributosRestantes;
            $this->view->caracteristicas = '<table>' . $caracteristicasprop . '</table>';

            $Beneficios = $this->ObtenerDatosBeneficios($IdProp);
            $beneficiosprop = '';
            foreach ($Beneficios as $val) {
                $beneficiosprop.=$val->Nombre . ", ";
            }

            if ($beneficiosprop != "")
                $beneficiosprop = substr($beneficiosprop, 0, -2) . ".";

            $this->view->beneficiosprop = $beneficiosprop;
            //  }
            //catch (Exception $e) {
            //     $this->_redirector->gotoUrl($urlhome);
            // }
        }
    }

    public function ObtenerDatosAtributos($IdProp) {
        $db = Zend_Registry::get('db');
        $language = Zend_Registry::get('language');
        $DetaAtri = $db->fetchAll("select CA.CodigoAtributo, ai.ValorIdioma as nombre, AP.ValorEntero,AP.ValorDecimal,AP.ValorS_N,
                                AP.ValorCaracter from proatributopropiedad AP
                                inner join catatributo CA on AP.CodigoAtributo=CA.CodigoAtributo
                                INNER JOIN catatributoidioma ai ON ai.CodigoAtributo = CA.CodigoAtributo
                                inner join propropiedad P on AP.CodigoPropiedad=P.CodigoPropiedad
                                where P.CodigoPropiedad=$IdProp and P.CodigoEdoPropiedad<>4 AND ai.CodigoIdioma = '" . $language . "'");

        return $DetaAtri;
    }

    public function ObtenerAtributosRestantes($attributeCodes) {
    	$caracteristicasprop = '';
        $db = Zend_Registry::get('db');
        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $attribute = new Hm_Cat_AtributoIdioma();
        $select = $attribute->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART);
        $select->setIntegrityCheck(false)
                ->where($attribute->getAdapter()->quoteInto("CodigoIdioma = ?", $language))
                ->join('catatributo', 'catatributo.CodigoAtributo=catatributoidioma.CodigoAtributo')
                ->where($attribute->getAdapter()->quoteInto("catatributo.CodigoAtributo NOT IN(?)", $attributeCodes)
        );

        $restAttributes = $db->fetchAll($select);

        foreach ($restAttributes as $val) {
            $caracteristicasprop .= "<tr><th>" . $val->ValorIdioma . "</th><td>-</td></tr>";
        }

        return $caracteristicasprop;
    }

    public function ObtenerDatosBeneficios($IdProp) {
        $db = Zend_Registry::get('db');
        $language = Zend_Registry::get('language');
        $DetaBene = $db->fetchAll("SELECT BI.ValorIdioma as Nombre
                                FROM probeneficiopropiedad B
                                INNER JOIN catbeneficio CB ON B.CodigoBeneficio=CB.CodigoBeneficio
                                INNER JOIN  propropiedad P ON B.CodigoPropiedad=P.CodigoPropiedad
                                INNER JOIN catbeneficioidioma BI ON BI.CodigoBeneficio = CB.CodigoBeneficio
                                INNER JOIN catidioma i ON i.CodigoIdioma = BI.CodigoIdioma
                                where P.CodigoPropiedad= $IdProp AND P.CodigoEdoPropiedad<>4 AND BI.CodigoIdioma = '" . strtoupper($language) . "'");

        return $DetaBene;
    }

    public function ObtenerFotos($IdProp) {
        $db = Zend_Registry::get('db');
        $FotoPrincipal = $db->fetchAll("select FA.aid,FA.locacion,FA.img_default from prop_foto_album FA
                                inner join propropiedad P on FA.idpropiedad=P.CodigoPropiedad
                                where FA.idpropiedad=$IdProp and P.CodigoEdoPropiedad<>4");

        return $FotoPrincipal;
    }

    public function ObtenerAlbumFoto($aid) {
        $db = Zend_Registry::get('db');
        $AlbumFoto = $db->fetchAll("select caption,src from prop_foto
                                where prop_foto_album_aid=$aid order by src DESC");

        return $AlbumFoto;
    }

    private function DatosClienteHM($uid) {
        $db = Zend_Registry::get('db');

        $email = "";
        $tel = "";
        $nombempresa = "";
        $telempresa = "";
        $sitio = "";
        $datacliente = $db->fetchAll("select EMail,TelefonoCliente,WebPage,NombreEmpresa,TelefonoEmpresa from cliente where Uid=" . $uid);

        if (count($datacliente) > 0) {
            $email = $datacliente[0]->EMail;
            $tel = $datacliente[0]->TelefonoCliente;
            $nombempresa = $datacliente[0]->NombreEmpresa;
            $telempresa = $datacliente[0]->TelefonoEmpresa;
            $sitio = $datacliente[0]->WebPage;
        }

        $this->view->email = $email;
        $this->view->telefono = $tel;
        $this->view->nomEmpresa = $nombempresa;
        $this->view->telEmpresa = $telempresa;
        if (Zend_Uri::check($sitio)) {
            $this->view->website = $sitio;
        }
    }

    public function propiedadesautorAction() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessión.");
            die;
        }
        $this->currentActionNav = 'enlistar';
        $this->navAction();
        
        $this->view->appusers = $fb->getFriendsApp();
        $this->view->urlsitio = $this->cfg['fb_app_url'];
        $language = Zend_Registry::get('language');
        $language = ($language == "es") ? $language : "en";
        $_ITEMS_PER_PAGE = 5;
        $currentPage = $this->_getParam('page', 1);
        $_PAGE_RANGE = 10;

        // minified and concatenated: jquery-ui-1.7.2.custom.css
        $this->view->headLink()->appendStylesheet('/css/custom-theme/jquery-ui-custom-theme.min.css');
        // minified and concatenated: errores.css, style_menu.css, style_perfil.css, list_autor_properties.css, messages.css
        $this->view->headLink()->appendStylesheet("/css/propiedadesautor.min.css");
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile("/js/facebook.js");
        
        
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery-1.3.2.min.js');
        $this->view->headScript()->appendFile("/js/lecturapropiedades.js?v=1");

        if (!empty($_GET['id'])) {
            $urlLocal = $this->cfg['local_app_url'];
            $db = Zend_Registry::get('db');

            $this->view->uid = $_GET['id'];
            $ListValue = Hm_Usr_UserFb::ObtenerDatosUsuario($this->view->uid);
            $location = '';
            $rutaimagen = $urllocal . "img/q_silhouette.gif";
            $pic = $rutaimagen;

            if (is_array($ListValue)) {
                $this->view->username = $ListValue[0]['first_name'];
                $this->view->usernames = $ListValue[0]['name'];
                if (strlen($ListValue[0]['pic_big']) > 0) {
                    $pic = $ListValue[0]['pic_big'];
                }

                $location = $ListValue[0]['current_location'];
            }

            $this->view->profile = $pic;
            $uid = $this->view->uid;

            $TotProp = $db->fetchAll("SELECT count(CodigoPropiedad) as total from propropiedad where CodigoEdoPropiedad=2 and Uid=" . $this->view->uid);
            //url datos de lectura de las propiedades del autor
            $propiedadesautor = "/enlistar/propiedadesautor?id=" . $this->view->uid;
            $this->view->cantprop = "<a href='$propiedadesautor' target='_self'> " . $TotProp[0]->total . " <i18n>LABEL_PUBLISHED_PROPERTIES</i18n> </a>";

            $property = new Hm_Pro_Propiedad();
            $properties = array();

            $rsProperties = Hm_Pro_Propiedad::GetPropertyDataByUid($uid);

            if ($rsProperties != null && count($rsProperties) > 0) { //Tiene propiedades
                $var_index = 1;
                foreach ($rsProperties as $proproperty) {
                    $properties[$var_index]["property"] = $proproperty;

                    $objFoto = new Hm_Pro_Foto();

                    $idMainPhoto = $objFoto->fetchAll(
                                    $objFoto->select()
                                            ->from(array('f' => 'profoto'), array('PROFotoID'))
                                            ->where('CodigoPropiedad = ?', $proproperty->CodigoPropiedad)
                                            ->where('Principal = ?', 1)
                    );


                    //Asignacion id foto principal
                    if ($idMainPhoto->current()->PROFotoID) {
                        $properties[$var_index]["mainPhoto"] = $urlLocal . "pic" . $idMainPhoto->current()->PROFotoID . "_" . Hm_Pro_Foto::$_PHOTO_TYPES["Destacada"] . ".jpg";
                    } else {
                        $properties[$var_index]["mainPhoto"] = "/img/" . $language . "/ad_no_image.jpg";
                    }

                    //Recuperar todos los atributos
                    $rsAttributes = $this->ObtenerDatosAtributos($proproperty->CodigoPropiedad);
                    $attributes = array();

                    if ($property->Area > 0) {
                        $attributes[] = $proproperty->AreaLote . ' ' . $proproperty->UnidadMedidaLote . ' ' . strtolower("<i18n>ADD_ESTATE_ADDITIONAL_INFO_AREA</i18n>");
                    }
                    if ($property->AreaLote > 0) {
                        $attributes[] = $proproperty->AreaLote . ' ' . $proproperty->UnidadMedidaLote . ' ' . strtolower("<i18n>LABEL_SEARCH_FILTER_LOT_SIZE</i18n>");
                    }

                    foreach ($rsAttributes as $val) {
                        $SiNo = '';
                        if (strtoupper($val->ValorS_N) == 'S')
                            $SiNo = "<i18n>LABEL_YES</i18n>";
                        else if (strtoupper($val->ValorS_N) == 'N')
                            $SiNo = "<i18n>LABEL_NO</i18n>";
                        $attributes[] = $val->ValorEntero . $val->ValorDecimal . $SiNo . $val->ValorCaracter . ' ' . $val->nombre;
                    }

                    $properties[$var_index]["attributes"] = implode(" | ", $attributes);

                    $var_index++;
                }
            }


            $paginator = Zend_Paginator::factory($properties);
            $paginator->setCurrentPageNumber($currentPage);
            $paginator->setItemCountPerPage($_ITEMS_PER_PAGE);
            $paginator->setPageRange($_PAGE_RANGE);

            $this->view->properties = $paginator;

            $this->DatosClienteHM($this->view->uid);

            //$pages = Zend_Registry::get('pages');
            //Si tiene activo el perfil de fanpages cambiar los datos de uid,nombre y pic_big
            $sql = "SELECT NombreCliente,TelefonoCliente,WebPage,EMail,NombreEmpresa,TelefonoEmpresa,profilepageid,twitter,youVideo 
                FROM cliente where Uid=" . $uid;
            $stmt = $db->query($sql);

            $resCliente = $stmt->fetch();
            $this->view->isActivePage = false;
            if ($resCliente && count($resCliente) > 0) {
                $this->view->nombre = $resCliente->NombreCliente;
                $this->view->email = $resCliente->EMail;
                $this->view->twitter = $resCliente->twitter;
                $this->view->telefono = $resCliente->TelefonoCliente;

                $this->view->paginaweb = formatUrl($resCliente->WebPage);
                $this->view->empresa = $resCliente->NombreEmpresa;
                $this->view->telempresa = $resCliente->TelefonoEmpresa;
                $this->view->youvideo = $resCliente->youVideo;
                if (!empty($resCliente->profilepageid)) {
                    $this->view->isActivePage = true;

                    $fbpages = $this->fbDgt->execFql("SELECT page_id,name,has_added_app,pic_big,fan_count FROM page WHERE page_id=" . $resCliente->profilepageid);
                    $pages = new Table($fbpages);
                    $filter = sprintf("{page_id} = %s", $resCliente->profilepageid);
                    $pages->filtrar($filter);

                    $page = array_values((array) $pages->obtener());
                    $page = $page[0];

                    //Filtrando datos con la tabla page
                    $sql = "SELECT nombre as NombreCliente,telefono as TelefonoCliente,EMail,twitter,WebPage,NombreEmpresa,TelefonoEmpresa,youVideo FROM page WHERE pageid=" . $resCliente->profilepageid;
                    $stmt = $db->query($sql);
                    $resCliente = $stmt->fetch();

                    $this->view->nombre = $resCliente->NombreCliente;
                    $this->view->email = $resCliente->EMail;
                    $this->view->twitter = $resCliente->twitter;
                    $this->view->telefono = $resCliente->TelefonoCliente;
                    $this->view->paginaweb = formatUrl($resCliente->WebPage);
                    $this->view->empresa = $resCliente->NombreEmpresa;
                    $this->view->telempresa = $resCliente->TelefonoEmpresa;
                    $this->view->youvideo = $resCliente->youVideo;
                    $this->view->username = $page->name;
                    $this->view->usernames = $page->name;
                    $this->view->profile = $page->pic_big;
                    $this->view->previd = $this->view->uid;
                    $this->view->uid = $page['page_id'];
                }
            }
        } else {
            $this->view->uid = -1;
            $this->view->usernames = $this->view->usernames;
        }
    }

    public function getlecturapropiedadesAction() {
        if ($this->getRequest()->isPost()) {
            try {

                $db = Zend_Registry::get('db');

                $fb = Dgt_Fb::getInstance();
                $uid = $fb->get_uid();

                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 50);
                $coduid_raw = $this->_request->getParam('coduid', -1);
                $sort = $this->_request->getParam('sort', "CodigoPropiedad");
                $dir = $this->_request->getParam('dir', "DESC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);
                $coduid = $f->filter($coduid_raw);


                $rs = Dgt_Enlistar::getLecturaPropiedad($sort, $dir, $start, $limit, $coduid);
                $rs_count = Dgt_Enlistar::getCountLecturaPropiedad($coduid);

                $results = $rs_count[0]->total;

                echo '{"success":true, "results":' . $results . ', "rows":' . Zend_Json::encode($rs) . '}';
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la sessión.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
    }

}
