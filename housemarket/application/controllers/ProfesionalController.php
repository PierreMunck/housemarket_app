<?php

require_once 'Hm/Pro/PropertyHandling.php';

class ProfesionalController extends Zend_Controller_Action {
    
    protected $_flashMessenger = null;
    protected $_redirector = null;
    protected $_propertyHandler = null;

    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
        $this->_propertyHandler = new Hm_Pro_ProtertyHandling();
    }

    public function preDispatch() {
        
    }

    public function loadresultAction() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la session.");
            die;
        }
        $this->view->uid = $fb->get_uid();
        $this->view->appusers = $fb->getFriendsApp();

        $this->view->headTitle('Cargar Propiedades desde Archivo');
        $this->view->loadmap = 'onunload="GUnload();"';
        $config = Zend_Registry::get('config');

        // minified and concatenated: ext-all.css, xtheme-gray.css
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/extjs-all.gray-theme.min.css?v=1');
        // minified and concatenated: carga-masiva.css
        $this->view->headLink()->appendStylesheet('/css/loadresult.min.css');
        $this->view->headScript()->appendFile('/js/jquery-1.3.2.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/carga-masiva.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');

        if (!session_id()) {
            session_start();
        }

        if (!empty($_SESSION['simple_message'])) {
            $this->view->mensaje = $_SESSION["simple_message"];
        }

        if (!empty($_SESSION['error_messages'])) {
            $this->view->errores = $this->_propertyHandler->convertirArregloDeErrorAHTML($_SESSION['error_messages']);
        }
    }

    public function indexAction() {

        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessión.");
            die;
        }
        
        $config = Zend_Registry::get('config');
        $uid = $fb->get_uid();
        $db = Zend_Registry::get('db');

        $this->view->uid;

        $cli = new Hm_Cli_Cliente();
        $rsClients = $cli->fetchAll(
                        $cli->select(array("CargaMasiva"))
                                ->where('Uid = ?', $uid)
        );
        $dirUrlfb = $config['fb_app_url'];
        $this->view->docDirectory = $config['webhost'] . "doc/";
        $this->view->manualcarga = $dirUrlfb . "documentos/uploadlisting?" . $_SERVER['QUERY_STRING'];
        $CargeM = $db->fetchAll("SELECT CodigoCliente FROM cliente WHERE Uid=" . $uid . " AND CargaMasiva=1");
        
        if (Count($CargeM) > 0) { //Tiene permiso para carga masiva  
            $this->view->masive = true;
            $this->view->headScript()->appendFile("https://www.google.com/uds/api?file=uds.js&v=1.0&source=uds-msw&key=ABQIAAAAvCeUucOYY74-kYy_PW657BTXiVpCZbJoaksdLXfYkWMngqmDrxS6-TiTbctgJsKVCf909V4KNSXSaA");
            $this->view->headScript()->appendFile("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.js?mode=new");
            $this->view->headScript()->appendFile('/js/jquery-1.3.2.min.js');
            $this->view->headScript()->appendFile("/js/jquery-ui-1.7.2.custom.min.js");
            $this->view->headScript()->appendFile("/js/validate/jquery.form.js");
            $this->view->headScript()->appendFile("/js/validate/jquery.metadata.js");
            $this->view->headScript()->appendFile("/js/validate/jquery.delegate.js");
            $this->view->headScript()->appendFile("/js/validate/jquery.validate.js");
            $this->view->headScript()->appendFile("/js/validate/jquery.corner.js");
            $this->view->headScript()->appendFile("/js/validate/messages_es.js");
            $this->view->headScript()->appendFile("/js/validate/validate_property.js");
            $this->view->headLink()->appendStylesheet("https://www.google.com/uds/css/gsearch.css");
            $this->view->headLink()->appendStylesheet("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.css");
            // minified and concatenated: ext-all.css, xtheme-gray.css
            $this->view->headLink()->appendStylesheet("/css/custom-theme/jquery-ui-custom-theme.min.css");
            // minified and concatenated: layout.css, style_form.css, errores.css, tabs.css, style_menu.css
            $this->view->headLink()->appendStylesheet("/css/profesional-allowed.min.css?v=1");

            $config = Zend_Registry::get('config');
            $dirUrlfb = $config['fb_app_url'];
            $dirUrl = $dirUrlfb . "profesional/loadresult";

            if (isset($_FILES["file"])) {

                if (!$_FILES["file"]["tmp_name"]) {
                    if (!session_id()) {
                        session_start();
                    }

                    $_SESSION["simple_message"] = "<i18n>MSG_NO_SELECT_FILE</i18n>";
                    $this->_redirector->gotoUrl($dirUrl);
                } elseif ($_FILES["file"]["size"] > 1024000) {
                    if (!session_id()) {
                        session_start();
                    }
                    $_SESSION["simple_message"] = "<i18n>MSG_FILE_FILE_SIZE_LIMIT_EXCEEDED</i18n>";
                    $this->_redirector->gotoUrl($dirUrl);
                } else {
                    if ($_FILES["file"]["type"] == "text/plain") {

                        $db = Zend_Registry::get('db');

                        // Atributos para cada columna de la tabla 'propropiedad',
                        // incluyendo el nombre y tipo de dato del campo.
                        $columnasEnTablaPropropiedad = $db->fetchAll('show columns from propropiedad');
                        $columnasEnTablaPropropiedad = HMUtils::build_simple_indexed_array_from_object($columnasEnTablaPropropiedad, 'Field', false);

                        // Indica los atributos que puede tener una propiedad según su tipo.
                        // Por ejemplo, a una Casa se le permita tener el atributo Cuartos 
                        // para indicar el número de cuartos que tiene,
                        // pero un Terreno no puede tener tal atributo.
                        // Este se utiliza para asignar los valores a la variable
                        // $atributosPermitidosPorTipoDePropiedad y se desecha luego.
                        $resAtriCat = $db->fetchAll("SELECT CodigoAtributo, CodigoCategoria, CampoDestino FROM catatributocategoria");

                        // Arreglo bidimensional que almacena información sobre los atributos que se le pueden asignar
                        // a una propiedad dependiendo de su tipo. Las llaves del arreglo corresponden al código de
                        // la categoría y el valor es un sí mismo otro arreglo asociativo con dos valores: en la llave
                        // 'CodigoAtributo' se almacena el código del atributo permitido a este tipo de propiedad, y en
                        // la llave 'CampoDestino' el campo donde se almacenará (en la tabla 'proatributopropiedad')
                        // el valor que especifique el usuario para este atributo.
                        $atributosPermitidosPorTipoDePropiedad = array();
                        foreach ($resAtriCat as $value) {
                            $atributosPermitidosPorTipoDePropiedad[$value->CodigoCategoria][] = array(
                                'CodigoAtributo' => $value->CodigoAtributo,
                                'CampoDestino' => $value->CampoDestino,
                            );
                        }

                        unset($resAtriCat);

                        // Sirve para mapear los encabezados del archivo a los nombres
                        // de las columnas donde se almacena la información.
                        // Por ejemplo, el archivo debe contener un encabezado llamado 'LISTINGTYPE'
                        // cuyo valor se almacena en el campo 'Accion' de la tabla 'propropiedad'
                        // ('Accion' almacena un caracter que indica si la propiedad está en venta, alquiler, ambos, o remate).
                        $mapNombreColArchivoANombreColTablaPropiedad = $db->fetchAll("SELECT CampoArchivo, CampoTabla FROM catcamposcarga");
                        $mapNombreColArchivoANombreColTablaPropiedad = HMUtils::build_simple_map_array_from_object($mapNombreColArchivoANombreColTablaPropiedad, 'CampoArchivo', 'CampoTabla', true);

                        // Almacena el nombre de las columnas requeridas (según el esquema) de la tabla 'propropiedades'
                        $columnasRequeridasEnTablaPropiedad = array();

                        $property = new Hm_Pro_Propiedad();
                        $infoProperty = $property->info(); //Extrayendo metadata de la tabla del modelo Propiedad
                        foreach ($infoProperty["metadata"] as $rFields => $value) {
                            // Que sean no nulos y que no sean llave primaria.
                            if ($infoProperty["metadata"][$rFields]["NULLABLE"] == false and !$infoProperty["metadata"][$rFields]["PRIMARY"]) {
                                $columnasRequeridasEnTablaPropiedad[] = $rFields;
                            }
                        }
                        unset($property);
                        unset($infoProperty);

                        $totalColumnasRequeridasEnTablaPropiedad = count($columnasRequeridasEnTablaPropiedad); // total de columnas requeridas en la tabla de propiedades ('propropiedad')

                        $encabezadosPermitidosAttributos = array(
                            'ROOMS', 'BATHROOMS', 'PARKINGLOTS', 'PETSALLOWED', 'STORIES', 'MODEL', 'HOA', 'YEAR'
                        );

                        $encabezadosPermitosFotos = array(
                            'PHOTO1', 'PHOTO2', 'PHOTO3', 'PHOTO4', 'PHOTO5'
                        );

                        // Encabezados permitidos en el archivo subido
                        $encabezadosPermitos = array_keys($mapNombreColArchivoANombreColTablaPropiedad);
                        $encabezadosPermitos = array_merge(
                                $encabezadosPermitos, $encabezadosPermitosFotos, $encabezadosPermitidosAttributos
                        );

                        // Encabezados requeridos en el archivo subido
                        $encabezadosRequeridos = array('TITLE', 'DESCRIPTION',
                            'PROPERTYTYPE', 'LISTINGTYPE', 'SALEPRICE', 'RENTALPRICE');

                        $filePointer = fopen($_FILES["file"]["tmp_name"], "r");
                        $esPrimeraLinea = true; // utilizada para leer encabezado
                        $encabezadosEnArchivo = array(); // almacena los encabezados en el archivo
                        $totalEncabezadosEnArchivo = 0; // total de encabezados en el archivo
                        $indice_propiedad = 0; // contador de filas que representan propiedades en el archivo (todas menos el encabezado)
                        $valoresFilaPropiedad = array(); // almacena valores para los diferentes campos de una propiedad
                        $idsPropiedadesGuardadas; // almacena los IDs (campo 'CodigoPropiedad') de las propiedades registradas
                        $errorMessages = array(); // almacena los mensajes de error producidos en el archivo
                        // se eliminarán cada uno de los encabezados requeridos presentes de modo que en
                        // el arregle queden sólo los encabezado requeridos no presentes
                        //$errorMessages['HEADERS']['REQUIRED_HEADERS'] = $encabezadosRequeridos;

                        while ($linea = fgets($filePointer, 5000)) {

                            // fila de encabezado tiene procesamiento especial
                            if ($esPrimeraLinea === true) {

                                $esPrimeraLinea = false;
                                $encabezadosEnArchivo = explode("\t", $linea);

                                HMUtils::trim_array_values($encabezadosEnArchivo);

                                // Verificar que todos los encabezados requeridos estén presentes
                                foreach ($encabezadosRequeridos as $encabezado) {
                                    if (!in_array($encabezado, $encabezadosEnArchivo)) {
                                        $errorMessages['HEADERS']['MISSING_REQUIRED_HEADERS'][] = $encabezado;
                                    }
                                }

                                // Verificar que los encabezados usados son los permitidos
                                foreach ($encabezadosEnArchivo as $encabezado) {
                                    if (!in_array($encabezado, $encabezadosPermitos)) {
                                        $errorMessages['HEADERS']['INVALID_HEADERS'][] = $encabezado;
                                    }
                                }

                                if (!empty($errorMessages['HEADERS']['MISSING_REQUIRED_HEADERS'])) {
                                    $_SESSION['simple_message'] = "<i18n>MSG_HEADERS_NO_VALID</i18n>";
                                    $_SESSION['error_messages'] = $errorMessages;
                                    $this->_redirector->gotoUrl($dirUrl);
                                }

                                $totalEncabezadosEnArchivo = count($encabezadosEnArchivo);
                            } // Fin de if $esPrimeraLinea === true
                            else {

                                $indice_propiedad += 1;

                                if ($indice_propiedad > 2500) {
                                    $errorMessages['GENERAL'][] = '<i18n>LISTING_UPLOAD_ERROR_MAX_NUMBER_OF_PROPERTIES_EXCEDEED</i18n>';
                                    break;
                                }

                                $contenedor_datos_propiedad = array(); // almacena la información de la propiedad para posteriormento guardarla a la BD
                                //$errorMessages = array();
                                $urlPhotos = array(); // almacena los URL de las fotos de una propiedad
                                // almacena la información incluída en una fila del archivo
                                $valoresFilaPropiedad = array();
                                $valoresFilaPropiedad = explode("\t", $linea);

                                if (count($valoresFilaPropiedad) !== $totalEncabezadosEnArchivo) {
                                    // TODO: print error message indicating that number of fields in row does not match header's total
                                    $errorMessages['ROWS'][$indice_propiedad]['HEADER_COUNT_MISMATCH'] = $indice_propiedad;
                                    continue;
                                }

                                // eliminar espacios al inicio o final del nombre de la columna
                                HMUtils::trim_array_values($valoresFilaPropiedad);

                                $this->_propertyHandler->asignarValoresEnFilaAContenedorDePropiedad($contenedor_datos_propiedad, $indice_propiedad, $valoresFilaPropiedad, $encabezadosEnArchivo, $totalEncabezadosEnArchivo, $encabezadosPermitos, $atributosPermitidosPorTipoDePropiedad, $encabezadosPermitidosAttributos, $encabezadosPermitosFotos, $urlPhotos, $errorMessages);

                                $this->_propertyHandler->validarAtributosEspecialesSegunTipoDePropiedad($contenedor_datos_propiedad, $indice_propiedad, $errorMessages);

                                $this->_propertyHandler->validarValoresEnContenedorDePropiedad($contenedor_datos_propiedad, $indice_propiedad, $encabezadosRequeridos, $errorMessages);

                                $this->_propertyHandler->tratarDeEncontrarPais($contenedor_datos_propiedad, $indice_propiedad, $errorMessages);

                                $this->_propertyHandler->convertirLlavesEnContenedorDePropiedad($contenedor_datos_propiedad, $mapNombreColArchivoANombreColTablaPropiedad);

                                $contenedor_datos_propiedad['FechaRegistro'] = new Zend_Db_Expr('CURDATE()');
                                $contenedor_datos_propiedad['Uid'] = $uid;
                                $contenedor_datos_propiedad['CodigoEdoPropiedad'] = 1; // Estado de propiedad: 1 - Registrada (pero pediente de publicar)
                                $contenedor_datos_propiedad['CargaMasiva'] = true; // Propiedad subida a través de carga masiva

                                $contenedor_datos_propiedad['Area'] *= 1; // convertir cadena a número
                                if (($contenedor_datos_propiedad['Area'] > 0) && isset($contenedor_datos_propiedad['UnidadMedida'])) {
                                    $contenedor_datos_propiedad['AreaMt2'] = $this->_propertyHandler->obtenerEquivalenciaAreaEnMT2($contenedor_datos_propiedad['Area'], $contenedor_datos_propiedad['UnidadMedida']);
                                }

                                $contenedor_datos_propiedad['AreaLote'] *= 1; // convertir cadena a número
                                if (($contenedor_datos_propiedad['AreaLote'] > 0) && isset($contenedor_datos_propiedad['UnidadMedidaLote'])) {
                                    $contenedor_datos_propiedad['AreaLoteMt2'] = $this->_propertyHandler->obtenerEquivalenciaAreaEnMT2($contenedor_datos_propiedad['AreaLote'], $contenedor_datos_propiedad['UnidadMedidaLote']);
                                }

                                $ingreso = new Hm_Pro_Propiedad();

                                $idPropiedad = 0; // ID de la propiedad almacenada
                                // Validación: Campos requeridos en tabla 'propropiedad' están presentes
                                if ($this->_propertyHandler->validarCamposRequeridosEnTablaPropiedadEstanPresentes($contenedor_datos_propiedad, $indice_propiedad, $columnasRequeridasEnTablaPropiedad, $totalColumnasRequeridasEnTablaPropiedad)) {

                                    // Intento de registrar propiedad y atributos asociados
                                    try {
                                        // Almacena la informacion de la propiedad a agregar
                                        $insert_propiedad = array();

                                        // Almacena los atributos (válidos) asignados a un propiedad
                                        $atributosDePropiedad = array();

                                        foreach ($columnasEnTablaPropropiedad as $columnaEnTablaPropiedad) {
                                            if (isset($contenedor_datos_propiedad[$columnaEnTablaPropiedad])) {
                                                $insert_propiedad[$columnaEnTablaPropiedad] = $contenedor_datos_propiedad[$columnaEnTablaPropiedad];
                                            }
                                        }

                                        $ingreso->insert($insert_propiedad);
                                        $idPropiedad = $db->lastInsertId();
                                        $idsPropiedadesGuardadas[] = $idPropiedad;

                                        $atributosDePropiedad = $this->_propertyHandler->asignarAtributosAPropiedad($idPropiedad, $contenedor_datos_propiedad, $atributosPermitidosPorTipoDePropiedad);

                                        // Guardar atributos
                                        foreach ($atributosDePropiedad as $atributo) {
                                            $IngresoProAtributo = new Hm_Pro_AtributoPropiedad();
                                            try {
                                                $IngresoProAtributo->insert($atributo);
                                            } catch (Exception $e) {
                                                
                                            }
                                        }
                                    } catch (Exception $e) {
                                        $errorMessages['ROWS'][$indice_propiedad]['COULD_NOT_BE_INSERTED'][] = $e->getMessage();
                                    }
                                    // fin de: Intento de registrar propiedad y atributos asociados

                                    if (!empty($idPropiedad) && (count($urlPhotos) > 0)) {
                                        $isDataStream = true;

                                        $tiposPermitidos = array("image/jpeg", "image/gif", "image/png");
                                        $methodsLarge = array("methodname" => "resize", "params" => array(429, 321));
                                        $methodsGallery = array("methodname" => "adaptiveResize", "params" => array(80, 60));
                                        $methodsFeature = array("methodname" => "adaptiveResize", "params" => array(120, 90));
                                        $methodsResults = array("methodname" => "adaptiveResize", "params" => array(100, 75));

                                        try {
                                            $dbPhoto = new Hm_Pro_Foto();

                                            foreach ($urlPhotos as $photo => $value) {
                                                $image = $this->_propertyHandler->get_data($value);
                                                $sourceImage = $image["data"];
                                                $info = $image["info"];

                                                if (!empty($sourceImage) && in_array($info["content_type"], $tiposPermitidos)) {
                                                    $data = array();

                                                    $data["FechaCreacion"] = new Zend_Db_Expr('CURDATE()'); // Fecha de creacion
                                                    $data["CodigoPropiedad"] = $idPropiedad;
                                                    $data["Mime"] = $info["content_type"];
                                                    $data["Principal"] = ($photo === "PHOTO1") ? true : false;

                                                    $arrData = Hm_Pro_Foto::ProcessThumbnail($methodsLarge, $sourceImage, $isDataStream);
                                                    $data["FotoGrande"] = $arrData["ImageString"];
                                                    $dimensions = $arrData["Dimensions"];
                                                    $data["AnchoFotoGrande"] = $dimensions["width"];
                                                    $data["AltoFotoGrande"] = $dimensions["height"];

                                                    $arrData = Hm_Pro_Foto::ProcessThumbnail($methodsGallery, $sourceImage, $isDataStream);
                                                    $data["FotoGaleria"] = $arrData["ImageString"];

                                                    $arrData = Hm_Pro_Foto::ProcessThumbnail($methodsFeature, $sourceImage, $isDataStream);
                                                    $data["FotoDestacada"] = $arrData["ImageString"];

                                                    $arrData = Hm_Pro_Foto::ProcessThumbnail($methodsResults, $sourceImage, $isDataStream);
                                                    $data["FotoResultado"] = $arrData["ImageString"];


                                                    $dbPhoto->insert($data);
                                                }
                                            } // Fin de foreach
                                        } catch (Exception $e) {
                                            
                                        }
                                    } // Fin de count($urlPhotos) > 0
                                } // Fin de Validación: Campos requeridos en tabla 'propropiedad' están presentes
                                else {
                                    $errorMessages['ROWS'][$indice_propiedad]['COULD_NOT_BE_INSERTED'][] = ''; // lorem
                                }
                            } // Fin de $esPrimeraLinea !== true
                        } // Fin de while que lee las líneas del archivo

                        fclose($filePointer);
                        $mensaje = "";

                        if (!session_id()) {
                            session_start();
                        }

                        $_SESSION["ids_propiedades_guardadas"] = $idsPropiedadesGuardadas;
                        $_SESSION["simple_message"] = '<h4>' . count($idsPropiedadesGuardadas) . ' <i18n>MSG_TOTAL_RECORDS</i18n>' . '</h4>';
                        $_SESSION["error_messages"] = $errorMessages;
                        $this->_redirector->gotoUrl($dirUrl);
                    } else {// Si tipo no válido
                        if (!session_id()) {
                            session_start();
                        }
                        $_SESSION["simple_message"] = "<i18n>MSG_FILE_NO_VALID</i18n>";
                        $this->_redirector->gotoUrl($dirUrl);
                    }
                }
            }
        } else {//Si no tiene permiso
            $this->view->masive = false;
            $NoCliente = $db->fetchAll("SELECT Uid FROM cliente where Uid=" . $uid);
            if (Count($NoCliente) > 0) {
                $this->view->hdnuevo = 2; //Es un usuario registrado. La Accion sera Actualizar Sus Datos
                $ExCliente = $db->fetchAll("SELECT Uid FROM cliente where Uid=" . $uid . " AND FechaSolicitaCarga IS NOT NULL");
                if (Count($ExCliente) > 0) {
                    $this->view->hdestado = 0; //Esta registrado y ya ha solicitado anteriormente Permiso
                } else {
                    $this->view->hdestado = 2; //Esta registrado pero no ha solicitado Permiso de Carga Masiva;
                }
            } else {
                $this->view->hdnuevo = 1; //Todavia No esta Registrado en Nuestra Base de Datos
                $this->view->hdestado = 1; //Es un  usuario que no esta registrado en Nuestra Base de datos;				
            }

            if (!session_id()) {
                session_start();
            }
            
            // minified and concatenated: ext-all.css, xtheme-gray.css
            $this->view->headLink()->appendStylesheet("/css/profesional-not-allowed.min.css");
            
            $ListValue = $this->_propertyHandler->ObtenerDatosUsuario($uid);
            $this->view->username = iconv("utf-8", "latin1", $ListValue[0]['first_name']);
            $this->view->usernames = iconv("utf-8", "latin1", $ListValue[0]['name']);
            $urllocal = $config['local_app_url'];

            if (strlen($ListValue[0]['pic_small']) == 0) {
                $rutaimagen = $urllocal . "img/q_silhouette.gif";
                $pic = $rutaimagen;
            } else {
                $pic = $ListValue[0]['pic_small'];
            }
            $this->view->profile = $pic;

            if (strlen($ListValue[0]['pic_small']) == 0) {
                $rutaimgsmall = $urllocal . "img/q_silhouette.gif";
                $pic_small = $rutaimgsmall;
            } else {
                $pic_small = $ListValue[0]['pic_small'];
            }

            $imgprofile = $pic_small;
            $hmpp = '100000972827845';
            //$UserValue = new FacebookRestClient($config['facebook_api_key'],$config['facebook_secret']);

            $attachment = array('name' => $this->view->usernames . " " . $this->view->translate('LABEL_REQUEST_LISTING_UPLOAD'),
                'href' => 'https://www.facebook.com/profile.php?id=' . $uid,
                'media' => array(array('type' => 'image',
                        'src' => $imgprofile,
                        'href' => 'https://www.facebook.com/profile.php?id=' . $uid)),
                'latitude' => '41.4',
                'longitude' => '2.19');
            
            $attachment = json_encode($attachment);

            if ($this->getRequest()->isPost()) {
                $esNuevo = 1;
                try {
                    // porque ententamos de redirigir sobre la misma pagina?
                    /*$dirUrl = $config['fb_app_url'];
                    $dirUrl = $dirUrl . "/profesional";*/
                    $movCliente = new Hm_Cli_Cliente();
                    $datos = array('FechaSolicitaCarga' => new Zend_Db_Expr('CURDATE()'));
                    $ListValue = $this->_propertyHandler->ObtenerDatosUsuario($uid);
                    if ($_POST['hdnuevo'] == 1) {//insertamos
                        $cfg = Zend_Registry::get('config');
                        $name = $ListValue[0]['name'];
                        $email = $ListValue[0]['email'];
                        $datos['FechaRegistro'] = new Zend_Db_Expr('CURDATE()');
                        $datos['Origen'] = $config['org_fbk'];
                        $datos['Uid'] = $uid;
                        $datos['NombreCliente'] = $name;
                        $datos['Email'] = $email;
                        $movCliente->insert($datos);
                        $this->_flashMessenger->addSuccess("<i18n>MSG_LOAD_REQUEST</i18n>.");
                        $this->view->hdestado = 0;
                        //Envio del 
                        //Armando correo con plantilla likes.phtml
                        $html = new Zend_View();
                        $html->setScriptPath(APPLICATION_PATH . '/views/scripts/emails/');
                        // assign valeues

                        $email_destiny = "info@hmapp.com";
                        $html->UD = $uid;
                        $html->name = $name;
                        $html->email = $email; // Email destinatarioa de La Carga Masiva          

                        $body = $html->render('cargamasiva.phtml');
                        $textbody = preg_replace("'<style[^>]*>.*</style>'siU", '', $body);
                        $textbody = trim(strip_tags($textbody));

                        //Configuracion del servidor smtp
                        $smtpServer = $cfg['gmail_smtpserver'];
                        $username = $cfg['gmail_username'];
                        $password = $cfg['gmail_password'];

                        $config = array('ssl' => 'tls',
                            'port' => 587,
                            'auth' => 'login',
                            'username' => $username,
                            'password' => $password);
                        try {
                            $transport = new Zend_Mail_Transport_Smtp($smtpServer, $config);

                            $mail = new Zend_Mail('UTF-8');
                            $mail->setFrom($username, "Housemarket Notificacion");
                            $mail->setReplyTo($username, "No-Reply");
                            $mail->setReturnPath($username, "No-Reply");
                            $mail->addTo($email_destiny, "Housemarket");
                            $mail->setSubject('Solicitud de carga masiva');
                            $mail->setBodyHtml($body);
                            $mail->setBodyText($textbody);
                            $status = $mail->send($transport);
                        } catch (Exception $e) {
                            echo "Error message: " . $e->getMessage() . "\n";
                        }
                    }
                    if ($_POST['hdnuevo'] == 2) {//actualizamos                    
                        $esNuevo = 2;
                        if ($datos['NombreCliente'] == '')
                            $datos['NombreCliente'] = $ListValue[0]['name'];
                        if ($datos['Email'] == '')
                            $datos['Email'] = $ListValue[0]['email'];
                        $datos['FechaCambio'] = new Zend_Db_Expr('CURDATE()');
                        $where = $movCliente->getAdapter()->quoteInto('Uid =?', $uid);
                        $movCliente->update($datos, $where);
                        $this->_flashMessenger->addSuccess("<i18n>MSG_LOAD_REQUEST</i18n>.");
                        $this->view->hdestado = 2;
                    }
                    //$this->_flashMessenger->addSuccess($uid);
                    try {
                        //$UserValue->stream_publish('', $attachment, null,$hmpp,$hmpp);
                    } catch (Exception $e) {
                        //TODO: return exception
                    }
                } catch (Exception $e) {
                    $this->_flashMessenger->addError("<i18n>LABEL_ERROR_PROPERTY</i18n>.");
                }
                $datos['hdnuevo'] = $esNuevo;
                $_SESSION["ArrayCliente"] = $datos;
                //no redirect to the same page
                //$this->_redirector->gotoUrl($dirUrl);
            }
            //TODO: aqui no pasa nada pagina blanca
        }
    }

    public function currenciesbycountryAction() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessión.");
            die;
        }
        $this->view->uid = $fb->get_uid();
        $this->view->appusers = $fb->getFriendsApp();

        $this->view->headTitle('Currencies per country');
        $config = Zend_Registry::get('config');

        $this->view->headScript()->appendFile('/js/jquery-1.3.2.min.js', 'text/javascript');
        // minified and concatenated: general.css, style_menu.css, tabs.css
        $this->view->headLink()->appendStylesheet("/css/currenciesbycountry.min.css");

        if (!session_id()) {
            session_start();
        }

        $c = new Hm_Cat_Pais();
        $currenciesByCountry = $c->GetCurrenciesByCountry();

        $tableHeaders = array('<i18n>COUNTRY_NAME</i18n>', '<i18n>CURRENCY_NAME</i18n>', '<i18n>CURRENCY_CODE</i18n>');

        $tableRecords = array();
        foreach ($currenciesByCountry as $value) {
            $tableRecords[] = array($value->CountryName, '<i18n>ISO_4217_CURRENCY_CODE_' . $value->CurrencyCode . '</i18n>', $value->CurrencyCode);
        }

        $this->view->currenciesByCountryTable = HMUtils::build_simple_html_table($tableHeaders, $tableRecords, 'horizontal', 'currencies-by-country');
    }

}

?>