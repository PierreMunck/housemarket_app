<?php
class DestacadaController extends MainController {
     public function init() {
     	parent::init();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
    }

    public function preDispatch() {
    }

    /**
    *
    * fonction de retour de label de navigation courant
    */
    public function getCurrentLabelnav() {
    	return 'destacada';
    }
   /**
     * Recupera los brokers disponibles en el sistema para publicidad
     */
    public function indexAction(){
    	$this->currentActionNav = 'index';
    	$this->navAction();
    	
        // minified and concatenated: search.css
        $this->view->headLink()->appendStylesheet('/css/destacada.min.css');
    }
}
?>
