<?php

class MihousebookController extends MainController {

    protected $_application;
    protected $_flashMessenger = null;
    protected $_redirector = null;
    public $_rol;
    public $_messagefotos = "";

    public function init() {
    	parent::init();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
    }

    public function preDispatch() {
        
    }
    /**
    *
    * fonction de retour de label de navigation courant
    */
    public function getCurrentLabelnav() {
    	return 'mihousebook';
    }
    
    public function indexAction() {
        $this->currentActionNav = 'index';
        $this->navAction();

        // minified and concatenated: ext-all.css, xtheme-gray.css
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/extjs-all.gray-theme.min.css?v=1');
        // minified and concatenated: search.css, scrollable-horizontal.css, scrollable-buttons.css
        $this->view->headLink()->appendStylesheet('/css/mihousebook.min.css?v=1');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        //$this->view->appusers = $this->fbDgt->getFriendsApp();
        die();
    }

    public function ObtieneCategoria($nombre, $valores) {
        for ($i = 0; $i < count($valores); $i++) {
            if (strtoupper($valores[$i]->NombreCategoria) == strtoupper(trim($nombre)) || strtoupper($valores[$i]->NombreEN) == strtoupper(trim($nombre)))
                return $valores[$i]->CodigoCategoria;
        }
        return NULL;
    }

    public function ObtieneCamposTabla($valores, $campos) {
        for ($i = 0; $i < count($valores); $i++) {
            for ($j = 0; $j < count($campos); $j++) {
                if (strtoupper(trim($valores[$i])) == strtoupper(trim($campos[$j]->CampoArchivo))) {
                    $valores[$i] = $campos[$j]->CampoTabla;
                    break;
                }
            }
        }
        return $valores;
    }

    public function ColumnPropiedad($tabla, $NameColumn) {
        //buscando el nombre de la columna
        foreach ($tabla as $row) {
            if (strtoupper($row->Field) == strtoupper(trim($NameColumn)))
                return true;
        }
        return false;
    }

    public function ProcesaAtributo($nombre, $valores, $Encabezado, $Arreglo, $resCategoria, $resAtriCat) {
        $CodAtributo = NULL;
        $CodCategoria = NULL;
        foreach ($valores as $row) {
            if (strtoupper(trim($row->NombreAtributo)) == strtoupper(trim($nombre)) || strtoupper(trim($row->NombreEN)) == strtoupper(trim($nombre))) {
                $CodAtributo = $row->CodigoAtributo;
                break;
            }
        }
        if ($CodAtributo == NULL)
            return NULL;

        for ($i = 0; $i < count($Encabezado); $i++) {
            if (trim(strtoupper($Encabezado[$i])) == 'CODIGOCATEGORIA') {
                $CodCategoria = $this->ObtieneCategoria(str_replace('"', '', $Arreglo[$i]), $resCategoria);
                break;
            }
        }

        if ($CodCategoria == NULL)
            return NULL;

        foreach ($resAtriCat as $row) {
            if ($row->CodigoAtributo == $CodAtributo && $row->CodigoCategoria == $CodCategoria) {
                $resultado = trim($row->CampoDestino) . "|" . $CodAtributo;
                return $resultado;
            }
        }

        return NULL;
    }

    public function showloadAction() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la session.");
            die;
        }
        $config = Zend_Registry::get('config');
        $this->view->uid = $fb->get_uid();

        $this->view->appusers = $fb->getFriendsApp();

        $this->view->headTitle('Cargar Propiedades desde Archivo');
        $this->view->loadmap = 'onunload="GUnload();"';
        //$fb->catchInvalidSession();
        $dirUrlfb = $config['fb_app_url'];
        $dirUrl = $dirUrlfb . "/mihousebook/loadresult";
        // minified and concatenated: ext-all.css, xtheme-gray.css
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/extjs-all.gray-theme.min.css?v=1');
        // minified and concatenated: style_perfil.css
        $this->view->headLink()->appendStylesheet('/css/showload.min.css?v=1');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');


        $localurl = $config['local_app_url'];
        $this->view->manualcarga = "/documentos/uploadlisting?" . $_SERVER['QUERY_STRING'];

        if (!$this->TienePermisoCargaMasiva($this->view->uid)) {
            $paginacarga = "/mihousebook/solcarga?" . $_SERVER['QUERY_STRING'];
            $this->_redirector->gotoUrl($paginacarga);
        }

        if ($this->getRequest()->isPost()) {
            if (!$_FILES["file"]["tmp_name"]) {
                if (!session_id()) {
                    session_start();
                }
                $_SESSION["valor"] = false;
                $_SESSION["mensaje"] = "<i18n>MSG_NO_SELECT_FILE</i18n>";
                $this->_redirector->gotoUrl($dirUrl);
            } else {
                if (($_FILES["file"]["type"] == "text/plain") && ($_FILES["file"]["size"] < 900000)) {
                    $db = Zend_Registry::get('db');

                    $resCategoria = $db->fetchAll("SELECT CodigoCategoria, NombreCategoria, NombreEN FROM catcategoria");
                    $columsPropiedad = $db->fetchAll('show columns from propropiedad');
                    $resAtributo = $db->fetchAll("SELECT CodigoAtributo, NombreAtributo, NombreEN FROM catatributo");
                    $resAtriCat = $db->fetchAll("SELECT CodigoAtributo, CodigoCategoria, CampoDestino FROM catatributocategoria");
                    $resCamposTabla = $db->fetchAll("SELECT CampoArchivo,CampoTabla FROM catcamposcarga");

                    $fp = fopen($_FILES["file"]["tmp_name"], "r");
                    $fallas = 0;
                    $total = 0;
                    $posiciones;
                    $enDB;
                    $posGrid = 0;
                    $Encabezado;
                    $PrimeraPos = true;
                    $idGuardado;
                    while ($linea = fgets($fp, 5000)) {
                        if ($PrimeraPos) {
                            $PrimeraPos = false;
                            $CamposTambla = explode("\t", $linea);
                            $Encabezado = $this->ObtieneCamposTabla($CamposTambla, $resCamposTabla);
                        } else {
                            $total++;
                            $Arreglo = "";
                            $Arreglo = explode("\t", $linea);

                            $insert_data_propropiedad = "";
                            $contDetalle = 0;
                            $ValorDetalle = "";
                            $CampoDetalle = "";
                            $CodAtributos = "";
                            for ($i = 0; $i < count($Encabezado); $i++) {
                                if (trim(strtoupper($Encabezado[$i])) == 'CODIGOCATEGORIA') {
                                    $insert_data_propropiedad[trim($Encabezado[$i])] = $this->ObtieneCategoria(str_replace('"', '', $Arreglo[$i]), $resCategoria);
                                } else if (trim(strtoupper($Encabezado[$i])) == 'UNIDADMEDIDA') {
                                    $valUM = strtoupper(trim(str_replace('"', '', $Arreglo[$i])));
                                    if ($valUM == 'FT2' || $valUM == 'VR2' || $valUM == 'MT2') {
                                        $insert_data_propropiedad['UnidadMedida'] = trim(str_replace('"', '', $Arreglo[$i]));
                                    }
                                } else if (trim(strtoupper($Encabezado[$i])) == 'UNIDADMEDIDALOTE') {
                                    $valUM = strtoupper(trim(str_replace('"', '', $Arreglo[$i])));
                                    if ($valUM == 'FT2' || $valUM == 'VR2' || $valUM == 'MT2') {
                                        $insert_data_propropiedad['UnidadMedidaLote'] = trim(str_replace('"', '', $Arreglo[$i]));
                                    }
                                } else if (!$this->ColumnPropiedad($columsPropiedad, $Encabezado[$i])) {
                                    if (trim($Arreglo[$i]) != "") {
                                        $valCampDetalle = $this->ProcesaAtributo($Encabezado[$i], $resAtributo, $Encabezado, $Arreglo, $resCategoria, $resAtriCat);
                                        if ($valCampDetalle != NULL) {
                                            $respCamp = explode("|", $valCampDetalle);
                                            $CampoDetalle[$contDetalle] = trim($respCamp[0]);
                                            $ValorDetalle[$contDetalle] = trim(str_replace('"', '', $Arreglo[$i]));
                                            $CodAtributos[$contDetalle] = trim($respCamp[1]);
                                            $contDetalle++;
                                        }
                                    }
                                } else {
                                    if (trim(str_replace('"', '', $Arreglo[$i])) != "")
                                        $insert_data_propropiedad[trim($Encabezado[$i])] = trim(str_replace('"', '', $Arreglo[$i]));
                                }
                            }
                            $insert_data_propropiedad['FechaRegistro'] = new Zend_Db_Expr('CURDATE()');
                            $insert_data_propropiedad['Uid'] = $this->view->uid;
                            $insert_data_propropiedad['CodigoEdoPropiedad'] = 1;
                            $insert_data_propropiedad['CargaMasiva'] = true;
                            $insert_data_propropiedad['CodigoMoneda'] = 1;

                            if (strlen(trim($insert_data_propropiedad['UnidadMedida'])) > 0 && strlen(trim($insert_data_propropiedad['Area'])) > 0) {
                                if (strtoupper($insert_data_propropiedad['UnidadMedida']) == 'FT2')
                                    $insert_data_propropiedad['AreaMt2'] = $insert_data_propropiedad['Area'] * '0.092903';
                                else if (strtoupper($insert_data_propropiedad['UnidadMedida']) == 'VR2')
                                    $insert_data_propropiedad['AreaMt2'] = $insert_data_propropiedad['Area'] * '0.702579';
                                else
                                    $insert_data_propropiedad['AreaMt2'] = $insert_data_propropiedad['Area'];
                            }

                            if (strlen(trim($insert_data_propropiedad['UnidadMedidaLote'])) > 0 && strlen(trim($insert_data_propropiedad['AreaLote'])) > 0) {
                                if (strtoupper($insert_data_propropiedad['UnidadMedidaLote']) == 'FT2')
                                    $insert_data_propropiedad['AreaLoteMt2'] = $insert_data_propropiedad['AreaLote'] * '0.092903';
                                else if (strtoupper($insert_data_propropiedad['UnidadMedidaLote']) == 'VR2')
                                    $insert_data_propropiedad['AreaLoteMt2'] = $insert_data_propropiedad['AreaLote'] * '0.702579';
                                else
                                    $insert_data_propropiedad['AreaLoteMt2'] = $insert_data_propropiedad['AreaLote'];
                            }

                            $ingreso = new Hm_Pro_Propiedad();
                            try {
                                $ingreso->insert($insert_data_propropiedad);
                                $IdPropiedad = $db->lastInsertId();
                                $idGuardado[$posGrid] = $IdPropiedad;
                                for ($i = 0; $i < $contDetalle; $i++) {
                                    $IngresoProAtributo = new Hm_Pro_AtributoPropiedad();
                                    try {
                                        $data_proatributo = array('CodigoPropiedad' => $IdPropiedad, 'CodigoAtributo' => $CodAtributos[$i], trim($CampoDetalle[$i]) => $ValorDetalle[$i]);
                                        $IngresoProAtributo->insert($data_proatributo);
                                    } catch (Exception $e) {
                                        
                                    }
                                }
                                $posGrid++;
                            } catch (Exception $e) {
                                $fallas++;
                                $posiciones = $posiciones . $total . ", ";
                            }
                        }
                    }//en while

                    if ($posGrid > 0) {
                        $nuevoCliente = new Hm_Cli_Cliente();
                        $resultCliente = $nuevoCliente->NuevoCliente($this->view->uid);
                    }
                    fclose($fp);
                    $mensaje = "";
                    if ($fallas > 0) {
                        $posiciones = substr($posiciones, 0, strlen($posiciones) - 2);
                        $Tfallas = $total - $fallas;
                        $mensaje = "<i18n>MSG_WHERE_INSERTED</i18n> " . $Tfallas . " <i18n>LABEL_OF</i18n> " . $total . " <i18n>MSG_RECORDS</i18n>. <i18n>MSG_POSITIONS_FAIL</i18n> (" . $posiciones . ")";
                    } else {
                        $mensaje = "<i18n>MSG_TOTAL_RECORDS</i18n>: " . $total . " <i18n>LABEL_OF</i18n> " . $total . "";
                    }
                    if (!session_id()) {
                        session_start();
                    }
                    $_SESSION["contenido"] = $idGuardado;
                    $_SESSION["valor"] = true;
                    $_SESSION["mensaje"] = $mensaje;
                    $this->_redirector->gotoUrl($dirUrl);
                } else {
                    if (!session_id()) {
                        session_start();
                    }
                    $_SESSION["valor"] = false;
                    $_SESSION["mensaje"] = "<i18n>MSG_FILE_NO_VALID</i18n>";
                    $this->_redirector->gotoUrl($dirUrl);
                }
            }
        }
    }

    public function getresultcargaAction() {
        if ($this->getRequest()->isPost()) {
            try {
                $db = Zend_Registry::get('db');

                $fb = Dgt_Fb::getInstance();
                $uid = $fb->get_uid();

                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 50);
                $sort = $this->_request->getParam('sort', "CodigoPropiedad");
                $dir = $this->_request->getParam('dir', "ASC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);
                if (!session_id()) {
                    session_start();
                }
                //if ($_SESSION["valor"])
                    $valores = $_SESSION["ids_propiedades_guardadas"];
                //else
                    //$valores[0] = -1;

                $rs = Dgt_Enlistar::getCargaMasiva($sort, $dir, $start, $limit, $valores, $uid, true);
                $rs_count = Dgt_Enlistar::getCountCargaMasiva($valores, $uid, true);

                $results = $rs_count[0]->total;
                echo '{"success":true, "results":' . $results . ', "rows":' . Zend_Json::encode($rs) . '}';
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la session.");
                exit();
            }
        } else {
            echo '{"success":true, "results":0, "rows":0}';
            exit();
        }
    }

    public function loadresultAction() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la session.");
            die;
        }
        $this->view->uid = $fb->get_uid();
        $this->view->appusers = $fb->getFriendsApp();

        $this->view->headTitle('Cargar Propiedades desde Archivo');
        $this->view->loadmap = 'onunload="GUnload();"';
        $config = Zend_Registry::get('config');

        // minified and concatenated: ext-all.css, xtheme-gray.css
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/extjs-all.gray-theme.min.css?v=1');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');

        if (!session_id()) {
            session_start();
        }
        if ($_SESSION["valor"])
            $this->view->mensaje = "<i18n>LABEL_RESULT</i18n>: " . $_SESSION["mensaje"];
        elseif ($_SESSION["mensaje"]) {
            $this->view->mensaje = "<i18n>LABEL_WARNING</i18n>, " . $_SESSION["mensaje"];
        } else {
            $this->view->mensaje = "<i18n>LABEL_NO_RESULTS</i18n>";
        }
    }

    public function getresultloadviewAction() {
        if ($this->getRequest()->isPost()) {
            try {

                $db = Zend_Registry::get('db');

                $fb = Dgt_Fb::getInstance();
                $uid = $fb->get_uid();

                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 50);
                $sort = $this->_request->getParam('sort', "CodigoPropiedad");
                $dir = $this->_request->getParam('dir', "ASC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);

                $rs = Dgt_Enlistar::getCargaMasiva($sort, $dir, $start, $limit, $valores, $uid);
                $rs_count = Dgt_Enlistar::getCountCargaMasiva($valores, $uid);

                $results = $rs_count[0]->total;
                echo '{"success":true, "results":' . $results . ', "rows":' . Zend_Json::encode($rs) . '}';
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la session.");
                exit();
            }
        } else {
            echo '{"success":true, "results":0, "rows":0}';
            exit();
        }
    }

    public function loadviewAction() {
        //probamos de obtener los datos de la sesion
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la session.");
            die;
        }

        $this->view->headTitle('<< MyHouseBook >>');
        // minified and concatenated: ext-all.css, xtheme-gray.css
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/extjs-all.gray-theme.min.css?v=1');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        $this->view->appusers = $fb->getFriendsApp();
    }

    public function ObtenerTotalesPropropiedad($uid) {
        $db = Zend_Registry::get('db');

        $Totales = $db->fetchAll(
                "select (select count(CodigoPropiedad) from propropiedad where Uid=$uid and CodigoEdoPropiedad<>4) as TotalPropiedad,
        (select count(CodigoPropiedad) from propropiedad where CodigoEdoPropiedad=2 and Uid=$uid) as TotalPublicadas,
        (select sum(CC.cantidad) from crecredito CC
        inner join cliente CL on CC.CodigoCliente=CL.CodigoCliente where CC.EstadoCredito='A' and CL.Uid=$uid) as TotalCreditos,
        (select sum(CC.Saldo) from crecredito CC
        inner join cliente CL on CC.CodigoCliente=CL.CodigoCliente where CC.EstadoCredito='A' and CL.Uid=$uid) as TotalDisponible");

        return $Totales;
    }

    public function ObtenerDatosUsuario($uid) {
        return $this->fbDgt->getDataUser($uid, array('first_name', 'name' , 'pic_big', 'current_location', 'pic_small'));
    }

    public function TienePermisoCargaMasiva($uid) {
        $db = Zend_Registry::get('db');

        $permiso = $db->fetchAll("select CodigoCliente,CargaMasiva from cliente where CargaMasiva=true and Uid=" . $uid);
        if (count($permiso) > 0)
            return true;
        else
            return false;
    }

    public function publicarpropropiedadAction() {
        if ($this->getRequest()->isPost()) {
            try {
                $db = Zend_Registry::get('db');
                $CodigoPropiedad = $this->_request->getParam('CodigoPropiedad', "CodigoPropiedad");
                $actualizar = array('CodigoEdoPropiedad' => 2,
                    'FechaPublica' => new Zend_Db_Expr('CURDATE()'));
                $fb = Dgt_Fb::getInstance();
                $uid = $fb->get_uid();
                $idsprop = explode(',', substr($CodigoPropiedad, 0, -1));
                $db->update('propropiedad', $actualizar, 'Uid=' . $uid . ' and Latitud is not null and Longitud is not null and CodigoPropiedad in(' . implode(',', $idsprop) . ')');
                echo '{"success": true}';
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la session.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
    }

    public function nopublicarpropropiedadAction() {
        if ($this->getRequest()->isPost()) {
            try {
                $db = Zend_Registry::get('db');
                $CodigoPropiedad = $this->_request->getParam('CodigoPropiedad', "CodigoPropiedad");
                $actualizar = array('CodigoEdoPropiedad' => 3);
                $fb = Dgt_Fb::getInstance();
                $uid = $fb->get_uid();
                $idsprop = explode(',', substr($CodigoPropiedad, 0, -1));
                $db->update('propropiedad', $actualizar, 'Uid=' . $uid . ' and CodigoPropiedad in (' . implode(',', $idsprop) . ')');
                echo '{"success": true}';
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la session.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
    }

    public function eliminarpropropiedadAction() {
        $CodigoPropiedad = $this->_request->getParam('CodigoPropiedad', "CodigoPropiedad");

        if ($this->getRequest()->isPost()) {
            try {
                $db = Zend_Registry::get('db');
                $CodigoPropiedad = $this->_request->getParam('CodigoPropiedad', "CodigoPropiedad");
                $actualizar = array('CodigoEdoPropiedad' => 4);
                $fb = Dgt_Fb::getInstance();
                $uid = $fb->get_uid();
                $db->update('propropiedad', $actualizar, 'Uid=' . $uid . ' and CodigoPropiedad in (' . $CodigoPropiedad . ')');

                try {
                    $fotos = $db->fetchAll('select aid,locacion from prop_foto_album where idpropiedad in (' . $CodigoPropiedad . ')');
                    foreach ($fotos as $valfotos) {
                        $rutafisica = Hm_Pro_FotoAlbum::GetPhysicalPath($valfotos->locacion); //$fotos[0]->locacion);
                        $rutafisica = str_replace("//", "/", $rutafisica);
                        try {
                            if (file_exists($rutafisica))
                                unlink($rutafisica);
                        } catch (Exception $e) {
                            
                        }
                    }
                } catch (Exception $e) {
                    
                }
                echo '{"success": true}';
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la session.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
    }

    public function DatosClienteHM($uid) {
        $db = Zend_Registry::get('db');

        $email;
        $tel;
        $nombempresa;
        $telempresa;
        $sitio;
        $datacliente = $db->fetchAll("select EMail,TelefonoCliente,WebPage,NombreEmpresa,TelefonoEmpresa from cliente where Uid=" . $uid);

        if (count($datacliente) > 0) {
            $email = $datacliente[0]->EMail;
            $tel = $datacliente[0]->TelefonoCliente;
            $nombempresa = $datacliente[0]->NombreEmpresa;
            $telempresa = $datacliente[0]->TelefonoEmpresa;
            $sitio = $datacliente[0]->WebPage;
        }

        $hrefsito = formatUrl($sitio);


        $this->view->infoclientehm = "<table width='100%'>
                <tr>
                    <td style='wstyleidth:50%'> <span><i18n>LABEL_EMAIL</i18n>: <strong>$email</strong> </span></td>
                    <td style='wstyleidth:50%'><span><i18n>LABEL_PHONE</i18n>: <strong>$tel</strong></span></td>
                </tr>
                <tr>
                    <td style='wstyleidth:50%'><span><i18n>LABEL_COMPANY</i18n>: <strong>$nombempresa</strong></span></td>
                    <td style='wstyleidth:50%'><span><i18n>LABEL_TELEPHONE_COMPANY</i18n>: <strong>$telempresa</strong></span></td>
                </tr>
                <tr><td colspan='2' style='wstyleidth:100%'><span><i18n>LABEL_WEBSITE</i18n>: <strong><a href='$hrefsito' target='_blank' alt=''>$sitio</a></strong> </span></td></tr>
            </table>";
    }

    public function getDataClientHM($datacliente) {

        $email = $datacliente[0]->EMail;
        $tel = $datacliente[0]->TelefonoCliente;
        $nombempresa = $datacliente[0]->NombreEmpresa;
        $telempresa = $datacliente[0]->TelefonoEmpresa;
        $sitio = $datacliente[0]->WebPage;
        $hrefsito = formatUrl($sitio);
        $this->view->infoclientehm = "<table width='100%'>
                <tr>
                    <td style='wstyleidth:50%'> <span><i18n>LABEL_EMAIL</i18n>: <strong>$email</strong> </span></td>
                    <td style='wstyleidth:50%'><span><i18n>LABEL_PHONE</i18n>: <strong>$tel</strong></span></td>
                </tr>
                <tr>
                    <td style='wstyleidth:50%'><span><i18n>LABEL_COMPANY</i18n>: <strong>$nombempresa</strong></span></td>
                    <td style='wstyleidth:50%'><span><i18n>LABEL_TELEPHONE_COMPANY</i18n>: <strong>$telempresa</strong></span></td>
                </tr>
                <tr><td colspan='2' style='wstyleidth:100%'><span><i18n>LABEL_WEBSITE</i18n>: <strong><a href='$hrefsito' target='_blank' alt=''>$sitio</a></strong> </span></td></tr>
            </table>";
    }

    public function publicacionAction() {
    	$this->currentActionNav = 'publication';
    	$this->navAction();
        
        $db = Zend_Registry::get('db');
        // minified and concatenated: ext-all.css, xtheme-gray.css
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/extjs-all.gray-theme.min.css?v=1');
        // minified and concatenated: style_menu.css, search.css, errores.css, tabs.css, style_perfil.css
        $this->view->headLink()->appendStylesheet('/css/publicacion.min.css?v=1');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery-1.3.2.min.js');
        $this->view->headScript()->appendFile("/js/propropiedades.js?v=12");
        $this->view->appusers = $this->fbDgt->getFriendsApp();

        $uid = $this->fbDgt->get_uid();
        $this->view->uid = $uid;
        $info = $this->fbDgt->getDataUser($uid);
        //Extraemos datos del usuario
        $sql = "SELECT NombreCliente,TelefonoCliente,WebPage,EMail,NombreEmpresa,TelefonoEmpresa,profilepageid 
                FROM cliente where Uid=" . $uid;
        $stmt = $db->query($sql);
        $infoinDb = $stmt->fetch();

        $this->view->username = $info[0]['first_name'];
        $this->view->usernames = $info[0]['name'];

        $config = Zend_Registry::get('config');
        $urllocal = $config['local_app_url'];
        $urlfb = $config['fb_app_url'];
        $this->view->urlsitio = $config['fb_app_url'];

        $paginacliente = $this->view->urlsitio . "mihousebook/editcliente?" . $_SERVER['QUERY_STRING'];
        $imgmisdatos = "/img/editar.png";

        $this->view->paginacliente = "<span><a href='$paginacliente' target='_top'><img src='$imgmisdatos' alt='<i18n>LABEL_EDIT_PROFILE</i18n>' style='padding-right:5px;'/><i18n>LABEL_EDIT_PROFILE</i18n></a></span>";

        //$location = $ListValue[0]['current_location'];

        //$this->DatosClienteHM($uid);

        if ($this->TienePermisoCargaMasiva($uid)) {
            $paracarga = $urlfb . "profesional";
            $this->view->botones = "<div class='button_group'><a href='$paracarga' class='button' id='rs2' target='_top' name='bt'><i18n>LABEL_UPLOAD_LISTING</i18n></a> <br/></div>";
        } else {
            $parasolicitud = $this->view->urlsitio . "/profesional";
            $this->view->botones = "<div class='button_group'> <a href='$parasolicitud' class='button' id='bt2' target='_top' name='bt'><i18n>LABEL_REQUEST_LISTING_UPLOAD</i18n></a> <br/></div>";
        }

        // Diferentes estados en lo que se encuentra una propiedad
        $localLanguage = Zend_Registry::get('language');
        $db = Zend_Registry::get('db');
        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $rs = $db->query("select 
            cep.CodigoEdoPropiedad,
            cepi.ValorIdioma as Estado
            from catedopropiedad cep
            INNER JOIN catedoproidioma cepi ON cep.CodigoEdoPropiedad = cepi.CodigoEdoPropiedad
            WHERE cep.CodigoEdoPropiedad <>4 AND cepi.CodigoIdioma = '" . $language . "'");
        $states = $rs->fetchAll();

        $this->view->states = $states;

        //Categorias
        $rs = $db->query("select c.CodigoCategoria, ci.ValorIdioma as Categoria
            FROM catcategoria c
            INNER JOIN catcategoriaidioma ci ON c.CodigoCategoria = ci.CodigoCategoria
            WHERE ci.CodigoIdioma = '" . $language . "' AND c.filtro = 1 ORDER BY c.orden");
        $categories = $rs->fetchAll();
        $this->view->categories = $categories;

        if (strlen($info[0]['pic_small']) == 0) {
            $rutaimagen = $urllocal . "img/q_silhouette.gif";
            $pic = $rutaimagen;
        } else {
            $pic = $info[0]['pic_small'];
        }
        $this->view->profile = $pic;
        $pages = Zend_Registry::get('pages');
        if ($infoinDb && !empty($infoinDb->profilepageid)) {
            $filter = sprintf("{page_id} = %s", $infoinDb->profilepageid);
            $pages->filtrar($filter);
            $page = array_values((array) $pages->obtener());
            $page = $page[0];
            //Filtrando datos con la tabla page
            $sql = "SELECT nombre as NombreCliente,telefono as TelefonoCliente,EMail,twitter,WebPage,NombreEmpresa,TelefonoEmpresa,youVideo FROM page WHERE pageid=" . $infoinDb->profilepageid;
            $stmt = $db->query($sql);

            $resCliente = $stmt->fetch();
            $this->view->nombre = $resCliente->NombreCliente;
            $this->view->email = $resCliente->EMail;
            $this->view->twitter = $resCliente->twitter;
            $this->view->telefono = $resCliente->TelefonoCliente;
            $this->view->paginaweb = $resCliente->WebPage;
            $this->view->empresa = $resCliente->NombreEmpresa;
            $this->view->telempresa = $resCliente->TelefonoEmpresa;
            $this->view->youvideo = $resCliente->youVideo;
            $this->view->username = $page['name'];
            $this->view->usernames = $page['name'];
            $this->view->previd = $this->view->uid;
            $this->view->uid = $page['page_id'];

            $this->view->profile = $page['pic_small'];
            //echo "Info cargada";
        }
        //Datos del cliente arriba
        $this->view->infoclientehm = "<table width='100%' class='infoclientehm'>
                <tr>
                    <td style='width:50%'> <span><i18n>LABEL_EMAIL</i18n>: <strong>".$this->view->email."</strong> </span></td>
                    <td style='width:50%'><span><i18n>LABEL_PHONE</i18n>: <strong>".$this->view->telefono."</strong></span></td>
                </tr>
                <tr>
                    <td style='width:50%'><span><i18n>LABEL_COMPANY</i18n>: <strong>".$this->view->empresa."</strong></span></td>
                    <td style='width:50%'><span><i18n>LABEL_TELEPHONE_COMPANY</i18n>: <strong>".$this->view->telempresa."</strong></span></td>
                </tr>
                <tr><td colspan='2' style='width:100%'><span><i18n>LABEL_WEBSITE</i18n>: <strong><a href='".$this->view->paginaweb."' target='_blank' alt=''>".$this->view->paginaweb."</a></strong> </span></td></tr>
            </table>";
        
        $dirUrl = $config['fb_app_url'];
        $totalprop = 0;
        $totalpub = 0;
        $totalcred = 0;
        $totaldisp = 0;
        $totales = $this->ObtenerTotalesPropropiedad($uid);
        if (count($totales) > 0) {
            $totalprop = $totales[0]->TotalPropiedad;
            $totalpub = $totales[0]->TotalPublicadas;
            if (strlen(trim($totales[0]->TotalCreditos)) > 0)
                $totalcred = $totales[0]->TotalCreditos;
            if (strlen(trim($totales[0]->TotalDisponible)) > 0)
                $totaldisp = $totales[0]->TotalDisponible;
        }
        $this->view->totalpropiedades = $totalprop;
        $this->view->totalpublicadas = $totalpub;
        $this->view->totalcreditos = $totalcred;
        $this->view->totaldisponibles = $totaldisp;
    }

    public function addcreditAction() {
        header('Content-type: application/json');
        $msg['success'] = false;

        if ($this->getRequest()->isPost()) {
            $db = Zend_Registry::get('db');
            $uid = Zend_Registry::get('uid');
            $id = $this->_request->getParam('propiedad', 0);
            $qty = $this->_request->getParam('quantity', 0);
            $qty = intval($qty);

            if ($id) {
                //Consultar si hay credito disponible

                $sql = sprintf("SELECT CC.saldo,CC.codigocredito FROM crecredito CC 
                                INNER JOIN cliente CL ON(CC.CodigoCliente=CL.CodigoCliente) 
                                WHERE CC.EstadoCredito='A' AND CL.Uid=%s 
                                ORDER BY saldo DESC", $uid);
                $saldoCredito = new Table($db->fetchAll($sql));
                $creditoDisponible = $saldoCredito->sumaColumna('saldo');
                //Previendo cantidades negativas
                $qty = ($qty < 0) ? $qty * (-1) : $qty;
                $qty*=10;

                if ($creditoDisponible > 0 && $qty <= $creditoDisponible) {
                    $db->beginTransaction();
                    try {
                        $data = array();
                        foreach ($saldoCredito->obtener() as $credito) {
                            if ($qty > 0) {
                                $sub = ($credito['saldo'] <= $qty) ? $credito['saldo'] : $qty;
                                $qty-=$sub;
                                $creditoDisponible-=$sub;
                                $data = array(
                                    'CodigoPropiedad' => $id,
                                    'CodigoCredito' => $credito['codigocredito'],
                                    'FechaAsigna' => new Zend_Db_Expr('CURDATE()'),
                                    'FechaRetira' => new Zend_Db_Expr('DATE_ADD(CURDATE(), INTERVAL 1 MONTH)'),
                                    'Cantidad' => $sub,
                                    'EstadoAsigna' => 'A'
                                );

                                $db->insert('crecreditopropiedad', $data);
                                $dataup['Saldo'] = $credito['saldo'] - $sub;
                                $where['CodigoCredito = ?'] = $credito['codigocredito'];

                                $db->update('crecredito', $dataup, $where);
                            } else {
                                break;
                            }
                        }
                        $db->commit();
                        $msg['success'] = true;
                        $msg['credito'] = $creditoDisponible;
                    } catch (Exception $e) {
                        $db->rollBack();
                        $msg['msg'] = $e->getMessage();
                    }
                } else {
                    $msg['msg'] = "You don't have enough credits";
                }
            }
        } else {
            $msg['msg'] = "The property ID is empty";
        }


        echo json_encode($msg);
        exit();
    }

    public function getpropropiedadesAction() {
        header('Content-type: application/json');
        if ($this->getRequest()->isPost()) {
            try {

                $db = Zend_Registry::get('db');

                $fb = Dgt_Fb::getInstance();
                $uid = $fb->get_uid();

                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 50);
                $sort = $this->_request->getParam('sort', "CodigoPropiedad");
                $dir = $this->_request->getParam('dir', "DESC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);
                $estado = $f->filter($this->_request->getParam('Estado', null));
                $categoria = $f->filter($this->_request->getParam('Categoria', null));


                $rs = Dgt_Enlistar::getPropropiedad($sort, $dir, $start, $limit, $uid, $estado, $categoria);

//$rs_count = Dgt_Enlistar::getCountPropropiedad($uid, $estado, $categoria);
                //$results = $rs_count[0]->total;

                echo '{"success":true, "results":' . $rs->results . ', "rows":' . Zend_Json::encode($rs->rows) . '}';
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                debug($e);
                Zend_Debug::dump("Expiracion de la session.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
    }

    public function previewprofileAction() {
        header('Content-type: application/json');
        $id = $this->_request->getPost("id", 0);
        $db = Zend_Registry::get('db');
        if ($id != 0) {
            $pages = Zend_Registry::get('pages');
            $pages->filtrar(sprintf("{page_id} = %s", $id));
            $page = array_values((array) $pages->obtener());

            //Consulta a info de pagina
            $sql = "SELECT nombre as NombreCliente,telefono AS TelefonoCliente,EMail,twitter,WebPage,NombreEmpresa,TelefonoEmpresa,youVideo 
                FROM page WHERE pageid=" . $id;
            $stmt = $db->query($sql);
            $rs = $stmt->fetch();
            $infouser = $page;
            if (!empty($rs['NombreCliente'])) {
                $infouser[0]['name'] = $rs['NombreCliente'];
            }
            $infouser[0]['phone'] = $rs['TelefonoCliente'];
            $infouser[0]['web'] = $rs['WebPage'];
            $infouser[0]['email'] = $rs['EMail'];
            $infouser[0]['twitter'] = $rs['twitter'];
            $infouser[0]['bness'] = $rs['NombreEmpresa'];
            $infouser[0]['phoneb'] = $rs['TelefonoEmpresa'];
            $infouser[0]['yvideo'] = $rs['youVideo'];
            echo json_encode($infouser);
            exit();
        }
        $uid = Zend_Registry::get('uid');

        $infouser = $this->fbDgt->getDataUser($uid, array('uid', 'name', 'pic_big'));

        $sql = "SELECT NombreCliente,TelefonoCliente,EMail,twitter,WebPage,NombreEmpresa,TelefonoEmpresa,youVideo
            FROM cliente WHERE Uid=" . $uid;
        $stmt = $db->query($sql);
        $rs = $stmt->fetch();
        $infouser[0]['name'] = $rs['NombreCliente'];
        $infouser[0]['phone'] = $rs['TelefonoCliente'];
        $infouser[0]['web'] = $rs['WebPage'];
        $infouser[0]['email'] = $rs['EMail'];
        $infouser[0]['twitter'] = $rs['twitter'];
        $infouser[0]['bness'] = $rs['NombreEmpresa'];
        $infouser[0]['phoneb'] = $rs['TelefonoEmpresa'];
        $infouser[0]['yvideo'] = $rs['youVideo'];
        echo json_encode($infouser);
        exit();
    }

    public function editclienteAction() {
    	$this->currentActionNav = 'editcliente';
    	$this->navAction();
        //Iniciando sesiones y variables base
        $config = Zend_Registry::get('config');
        $db = Zend_Registry::get('db');
        //probamos de obtener los datos de la sesion
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la session.");
            die;
        }

        // minified and concatenated: colorbox.css
        $this->view->headLink()->appendStylesheet('/css/colorbox/colorbox.min.css?v=1');
        // minified and concatenated: style_perfil.css, search.css
        $this->view->headLink()->appendStylesheet('/css/editcliente.min.css?v=1');

        $this->view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/validate/jquery.validate1.8.1.min.js?v=1', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery.colorbox-min.js?v=1.3.17.2', 'text/javascript');
        $this->view->headScript()->appendFile('/js/editcliente.js?v=19', 'text/javascript');

        $this->view->appusers = $fb->getFriendsApp();

        $uid = $fb->get_uid();
        $this->view->uid = $uid;

        //Verificamos si hay un envio por POST
        $datos = array();
        if (isset($_POST['userid'])) {
            $dirUrl = $config['fb_app_url'];
            $dirUrl = $dirUrl . "/mihousebook/editcliente";
            $esNuevo = 1;

            try {
                $movCliente = new Hm_Cli_Cliente();
                if ($_POST['fanpage'] != 1) {
                    $_POST['profilepageid'] = "";
                }
                //Cambiar a fanpages
                $ispage = (!empty($_POST['profilepageid'])) ? true : false;

                $datos = array('NombreCliente' => $_POST['nombre'],
                    'TelefonoCliente' => $_POST['telefono'],
                    'WebPage' => $_POST['paginaweb'],
                    'Email' => $_POST['email'],
                    'NombreEmpresa' => $_POST['empresa'],
                    'TelefonoEmpresa' => $_POST['telempresa'],
                    'profilePageID' => $_POST['profilepageid'],
                    'youVideo' => $_POST['youvideo'],
                    'twitter' => $_POST['twitter']
                );

                if ($_POST['hdnuevo'] == 1) {//insertamos
                    $datos['FechaRegistro'] = new Zend_Db_Expr('CURDATE()');
                    $datos['Origen'] = $config['org_fbk'];
                    $datos['Uid'] = $uid;
                    $movCliente->insert($datos);
                } else {//actualizamos
                    $esNuevo = 0;

                    //Evaluamos la existencia del fanpage
                    if ($ispage) {

                        $sql = "SELECT pageid FROM page WHERE pageid=" . $_POST['profilepageid'];
                        $pageRegistrada = $db->fetchOne($sql);
                        $datapage = array(
                            'nombre' => $_POST['nombre'],
                            'email' => $_POST['email'],
                            'youvideo' => $_POST['youvideo'],
                            'twitter' => $_POST['twitter'],
                            'telefono' => $_POST['telefono'],
                            'telefonoempresa' => $_POST['telempresa'],
                            'nombreempresa' => $_POST['empresa'],
                            'webpage' => $_POST['paginaweb'],
                            'pageid' => $_POST['profilepageid']
                        );
                        if (!$pageRegistrada) {
                            $db->insert('page', $datapage);
                        } else {
                            $where = "pageid=" . $_POST['profilepageid'];
                            $db->update('page', $datapage, $where);
                        }

                        $datos = array('profilePageID' => $_POST['profilepageid']);
                        $datos['FechaCambio'] = new Zend_Db_Expr('CURDATE()');
                        $where = $movCliente->getAdapter()->quoteInto('Uid =?', $uid);
                        $movCliente->update($datos, $where);
                    } else {
                        $datos['FechaCambio'] = new Zend_Db_Expr('CURDATE()');
                        $where = $movCliente->getAdapter()->quoteInto('Uid =?', $uid);
                        $movCliente->update($datos, $where);
                    }
                }


                $this->_flashMessenger->addSuccess("<i18n>MSG_SAVE_DATA</i18n>");
                //$add ="<div class='success'>.</div>";
                //$this->view->fms=$add;
            } catch (Exception $e) {
                $this->_flashMessenger->addError("<i18n>LABEL_ERROR_PROPERTY</i18n>.");
            }
            $datos['hdnuevo'] = $esNuevo;
        }

        //Inicializando los datos        
        $sql = "SELECT NombreCliente,TelefonoCliente,WebPage,EMail,NombreEmpresa,TelefonoEmpresa,profilepageid,twitter,youVideo 
                FROM cliente where Uid=" . $fb->get_uid();
        $stmt = $db->query($sql);
        $resCliente = $stmt->fetch();

        //Extrayendo datos de facebook
        $urllocal = $config['local_app_url'];

        $ListValue = $this->ObtenerDatosUsuario($uid);

        $fbuser = $ListValue[0];
        $this->view->username = $fbuser['first_name'];
        $this->view->usernames = $fbuser['name'];
        $this->view->profile = (empty($fbuser['pic_big'])) ? $urllocal . "img/q_silhouette.gif" : $fbuser['pic_big'];
        $this->view->isActivePage = false;
        
        $user_granted_permissions = Zend_Registry::get('user_granted_permissions');
        $pages = Zend_Registry::get('pages');
        
        if (isset($user_granted_permissions['manage_pages']) && $user_granted_permissions['manage_pages'] === '1') {        
            //Extraemos las paginas asociadas
            $this->view->pages = $pages->obtener();
        }
        else
        {
            $this->view->request_perm_url = 'https://graph.facebook.com/oauth/authorize?client_id=' . $config['facebook_appid'] . ' &redirect_uri=' . urlencode($config['fb_app_url'] . 'mihousebook/editcliente') . '&scope=manage_pages&type=user_agent&display=page';
            $this->view->fan_page_disabled = 'disabled="disabled"';
        }
        
        $this->view->selected = 0;

        if ($resCliente && count($resCliente) > 0) {
            $this->view->nombre = $resCliente->NombreCliente;
            $this->view->email = $resCliente->EMail;
            $this->view->twitter = $resCliente->twitter;
            $this->view->telefono = $resCliente->TelefonoCliente;
            $this->view->paginaweb = formatUrl($resCliente->WebPage);
            $this->view->empresa = $resCliente->NombreEmpresa;
            $this->view->telempresa = $resCliente->TelefonoEmpresa;
            $this->view->youvideo = $resCliente->youVideo;
            $this->view->hdnuevo = 0;
            //Agregamos url del perfil por defecto
            $this->view->profileurl = "ttp://www.facebook.com/profile.php?id=" . $this->view->uid;
            if (!empty($resCliente->profilepageid)) {
                $this->view->isActivePage = true;
                $this->view->selected = $resCliente->profilepageid;
                //Filtrando datos con facebook
                $filter = sprintf("{page_id} = %s", $resCliente->profilepageid);
                $pages->filtrar($filter);
                $page = array_values((array) $pages->obtener());
                $page = $page[0];
                //Filtrando datos con la tabla page
                $sql = "SELECT nombre as NombreCliente,telefono as TelefonoCliente,EMail,twitter,WebPage,NombreEmpresa,TelefonoEmpresa,youVideo FROM page WHERE pageid=" . $resCliente->profilepageid;
                $stmt = $db->query($sql);
                $resCliente = $stmt->fetch();

                $this->view->nombre = $resCliente->NombreCliente;
                $this->view->email = $resCliente->EMail;
                $this->view->twitter = $resCliente->twitter;
                $this->view->telefono = $resCliente->TelefonoCliente;
                $this->view->paginaweb = formatUrl($resCliente->WebPage);
                $this->view->empresa = $resCliente->NombreEmpresa;
                $this->view->telempresa = $resCliente->TelefonoEmpresa;
                $this->view->youvideo = $resCliente->youVideo;
                $this->view->username = $page['name'];
                $this->view->usernames = $page['name'];
                $this->view->uid = $page['page_id'];
                $this->view->profile = $page['pic_big'];
                //Si existe la info lo llevamos a la aplicacion
                $this->view->profileurl = sprintf("https://www.facebook.com/pages/%s/%s?sk=app_%s", $page['name'], $page['page_id'], $config['facebook_appid']);
            }
        } else {
            $this->view->nombre = $this->view->usernames;
            $this->view->hdnuevo = 1;
        }
    }

    public function solcargaAction() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la session.");
            die;
        }

        // minified and concatenated: ext-all.css, xtheme-gray.css
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/extjs-all.gray-theme.min.css?v=1');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        $this->view->appusers = $fb->getFriendsApp();
        $uid = $fb->get_uid();
        $this->view->uid = $uid;
        $config = Zend_Registry::get('config');

        $db = Zend_Registry::get('db');

        //comprobando si no tiene acceso a carga masiva
        if ($this->TienePermisoCargaMasiva($this->view->uid)) {
            $paginacarga = "/mihousebook/showload?" . $_SERVER['QUERY_STRING'];
            //$this->_redirector->gotoUrl($paginacarga);
            $this->_helper->viewRenderer("showload");
        }

        if (!session_id()) {
            session_start();
        }
        $ListValue = $this->ObtenerDatosUsuario($uid);
        $this->view->username = iconv("utf-8", "latin1", $ListValue[0]['first_name']);
        $this->view->usernames = iconv("utf-8", "latin1", $ListValue[0]['name']);
        $urllocal = $config['local_app_url'];

        if (strlen($ListValue[0]['pic_small']) == 0) {
            $rutaimagen = $urllocal . "img/q_silhouette.gif";
            $pic = $rutaimagen;
        } else {
            $pic = $ListValue[0]['pic_small'];
        }
        $this->view->profile = $pic;

        if (strlen($ListValue[0]['pic_small']) == 0) {
            $rutaimgsmall = $urllocal . "img/q_silhouette.gif";
            $pic_small = $rutaimgsmall;
        } else {
            $pic_small = $ListValue[0]['pic_small'];
        }

        $imgprofile = $pic_small;

        $hmpp = '100000972827845';

        if ($this->getRequest()->isPost()) {
            $dirUrl = $config['fb_app_url'];
            $dirUrl = $dirUrl . "/mihousebook/solcarga";
            $esNuevo = 1;
            try {

                $movCliente = new Hm_Cli_Cliente();
                $datos = array('NombreCliente' => $_POST['nombre'],
                    'TelefonoCliente' => $_POST['telefono'],
                    'WebPage' => $_POST['paginaweb'],
                    'Email' => $_POST['email'],
                    'NombreEmpresa' => $_POST['empresa'],
                    'TelefonoEmpresa' => $_POST['telempresa'],
                    'FechaSolicitaCarga' => new Zend_Db_Expr('CURDATE()')
                );
                if ($_POST['hdnuevo'] == 1) {//insertamos
                    $datos['FechaRegistro'] = new Zend_Db_Expr('CURDATE()');
                    $datos['Origen'] = $config['org_fbk'];
                    $datos['Uid'] = $uid;
                    $movCliente->insert($datos);
                } else {//actualizamos
                    $esNuevo = 0;
                    $datos['FechaCambio'] = new Zend_Db_Expr('CURDATE()');
                    $where = $movCliente->getAdapter()->quoteInto('Uid =?', $uid);
                    $movCliente->update($datos, $where);
                }
                $this->_flashMessenger->addSuccess("<i18n>MSG_SAVE_DATA</i18n>. <i18n>MSG_LOAD_REQUEST</i18n>");

            } catch (Exception $e) {
                $this->_flashMessenger->addError("<i18n>LABEL_ERROR_PROPERTY</i18n>.");
            }
            $datos['hdnuevo'] = $esNuevo;
            $_SESSION["ArrayCliente"] = $datos;
            $this->_helper->viewRenderer("showload");
            // $this->_redirector->gotoUrl($dirUrl);
        } else {
            if (strlen($_SESSION["ArrayCliente"]) > 0) {
                $aCliente = $_SESSION["ArrayCliente"];
                $this->view->nombre = $aCliente['NombreCliente'];
                $this->view->email = $aCliente['Email'];
                $this->view->telefono = $aCliente['TelefonoCliente'];
                $this->view->paginaweb = $aCliente['WebPage'];
                $this->view->empresa = $aCliente['NombreEmpresa'];
                $this->view->telempresa = $aCliente['TelefonoEmpresa'];
                $this->view->hdnuevo = $aCliente['hdnuevo'];
                session_unset($_SESSION["ArrayCliente"]);
            } else {
                $resCliente = $db->fetchAll("SELECT NombreCliente,TelefonoCliente,WebPage,EMail,NombreEmpresa,
                                          TelefonoEmpresa FROM cliente where Uid=" . $this->view->uid);
                if (count($resCliente) > 0) {
                    $this->view->nombre = $resCliente[0]->NombreCliente;
                    $this->view->email = $resCliente[0]->EMail;
                    $this->view->telefono = $resCliente[0]->TelefonoCliente;
                    $this->view->paginaweb = $resCliente[0]->WebPage;
                    $this->view->empresa = $resCliente[0]->NombreEmpresa;
                    $this->view->telempresa = $resCliente[0]->TelefonoEmpresa;
                    $this->view->hdnuevo = 0;
                } else {
                    $this->view->nombre = $this->view->usernames;
                    $this->view->hdnuevo = 1;
                }
            }
        }
    }

    public function editAction() {
        $db = Zend_Registry::get('db');
        $this->view->messagefoto = $this->_messagefotos == "" ? "Haga clic en Examinar para subir las fotos de su propiedad" : "Haga Clic en Examinar para subir sus fotos";

        if ($this->getRequest()->isPost()) {
            // no post data
            // no retornamos nada
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_GET'));

            $keys_form = array('propiedad');

            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }

            $fb = Dgt_Fb::getInstance();
            $uid = $fb->get_uid();
            $this->view->appusers = $fb->getFriendsApp();

            $filter = array(
                '*' => array('StringTrim', 'StripTags'),
                'propiedad' => 'Digits'
            );

            $validators = array(
                'propiedad' => array('Digits', 'allowEmpty' => false)
            );

            $input = new Zend_Filter_Input($filter, $validators, $valid_data);

            if ($input->isValid()) {
                if ($input->isValid('propiedad')) {
                    if (strlen($input->propiedad) > 0) {
                        $formupload = new Dgt_Formupload();
                        $idpropiedad = $input->propiedad;
                        $formupload->create_form($idpropiedad);
                        $this->view->form_foto = $formupload->formulario;

                        $cfg = Zend_Registry::get('config');
                        $google_api_key = $cfg['google_api_key'];
                        $this->view->headScript()->appendFile("https://maps.google.com/maps?file=api&v=2&sensor=false&key=$google_api_key", 'text/javascript');
                        $this->view->loadmap = 'onunload="GUnload();"';

                        $p = new Dgt_Propiedad();

                        $perfil_propiedad = $p->getPropiedad($idpropiedad);
                        $propiedad_habilitada = $p->getPropiedad_foto_Habilitado($idpropiedad);
                        $this->view->idprop = $idpropiedad;
                        $this->view->descripcion_propiedad = $perfil_propiedad[0]->descripcion;
                        $this->view->direccion = utf8_decode($perfil_propiedad[0]->direccion);
                        $this->view->pais = $perfil_propiedad[0]->pais;
                        $this->view->estado = $perfil_propiedad[0]->estado;
                        $this->view->ciudad = utf8_decode($perfil_propiedad[0]->ciudad);
                        $this->view->zip_code = $perfil_propiedad[0]->zip_code;
                        $this->view->precio_venta = $perfil_propiedad[0]->precio_venta;
                        $this->view->precio_alquiler = $perfil_propiedad[0]->precio_alquiler;
                        $this->view->tipo_enlistamiento = $perfil_propiedad[0]->tipo_enlistamiento;

                        $fecha = ($perfil_propiedad[0]->fecha_ingreso == "") || ($perfil_propiedad[0]->fecha_ingreso == "0000-00-00 00:00:00") ? "" : new Zend_Date(strtotime($perfil_propiedad[0]->fecha_ingreso));
                        $this->view->fecha_ingreso = $fecha == "" ? "" : $fecha->toString("dd/MM/YYYY");
                        $fecha_mod = $perfil_propiedad[0]->ultima_modificacion == "" ? "" : new Zend_Date(strtotime($perfil_propiedad[0]->ultima_modificacion));
                        $this->view->ultima_modificacion = $fecha_mod == "" ? "" : $fecha_mod->toString("dd/MM/YYYY");
                        $this->view->tags = $perfil_propiedad[0]->tags;
                        $this->view->area = $perfil_propiedad[0]->area;
                        $this->view->cuartos = $perfil_propiedad[0]->cuartos;
                        $this->view->banos = $perfil_propiedad[0]->banos;
                        $this->view->tamano_lote = $perfil_propiedad[0]->tamano_lote;
                        $this->view->destacada = $perfil_propiedad[0]->destacada;
                        $this->view->roomate = $perfil_propiedad[0]->roomate;
                        $this->view->estatus = $perfil_propiedad[0]->estatus;
                        $this->view->tipo_propiedad = $perfil_propiedad[0]->tipo_propiedad;
                        $this->view->lat = $perfil_propiedad[0]->lat;
                        $this->view->lng = $perfil_propiedad[0]->lng;

                        $this->view->foto = $perfil_propiedad[0]->foto;
                        $this->view->src = $propiedad_habilitada[0]->src;
                        $this->view->default = $propiedad_habilitada[0]->habilitado;

                        $uidproperty = $perfil_propiedad[0]->uid;
                        $broker = $fb->getDataUser($uidproperty, array('uid', 'name', 'first_name', 'last_name', 'pic_small','website', 'proxied_email', 'email_hashes', 'hometown_location', 'profile_url'));

                        $this->view->uidPropiedadBD = $uidproperty;

                        $this->view->fn = $broker[0]['first_name'];
                        $this->view->ln = $broker[0]['last_name'];
                        $this->view->name = $broker[0]['name'];
                        $this->view->ps = $broker[0]['pic_small'];
                        $this->view->uid = $broker[0]['uid'];
                        $this->view->website = $broker[0]['website'];
                        $this->view->hometown = $broker[0]['hometown_location']["country"];
                        $this->view->pro_url = $broker[0]['profile_url'];

                        $this->view->administrador = $broker[0]['uid'] == $uid ? 1 : 0;
                        //Agregar primer combo del tipo pais
                        $this->view->CboCountry = $this->getCboCountry($this->view->pais);
                        //Agregar primer combo del tipo de propiedad
                        $this->view->Cbotype_property = $this->getCbotype_property($this->view->tipo_propiedad);
                    }
                }
            }
            if ($input->hasInvalid() || $input->hasMissing()) {
                $messages = $input->getMessages();
                //echo '{ "success": false, "errormsg": '. Zend_Json::encode($messages) .' }';
                //@todo: cambiar mensaje y redirigir a mihousebook.
                echo '<div class="error">ID de propiedad no encontrado o invalido</div>';
            }
        }
    }

    public function fotosuploadAction() {
        $destiny = getenv('DOCUMENT_ROOT') . '/propiedades/';
        $urlImages = '/propiedades/';
        $db = Zend_Registry::get('db');
        $this->view->message = "";
        if ($this->getRequest()->isGet()) {
            // no post data
            // no retornamos nada
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));
            $f = new Zend_Filter_StripTags();
            $idpropiedad = $f->filter($this->_request->getParam('idpropiedad'));
            $fb = Dgt_Fb::getInstance();
            $uid = $fb->get_uid();
            $thumb_method = "gd2";
            $thumb_use = "any";
            $default_file_mode = '0666';
            $establecerfotoPrincipal = false;

            $GIF_support = function_exists('imagecreatefromgif') ? 1 : 0;

            $location = $destiny . $idpropiedad;
            $locationBD = $urlImages . $idpropiedad;

            $formupload = new Dgt_Formupload();
            $formupload->creardirectorio_main($location, $idpropiedad, $uid);
            $albumid = $formupload->get_album_BD($idpropiedad);

            // Add 'edit' directory if it doesn't exist
            // Set access to read+write only
            $album = $location . '/' . $albumid;

            $formupload->creardirectorio($album);
            $albumnormal = $album . "/normal";
            $urlImagesfinal = $urlImages . $idpropiedad . '/' . $albumid . '/normal/';
            $formupload->creardirectorio($albumnormal);
            $albumthumb = $album . "/marker";
            $formupload->creardirectorio($albumthumb);
            $albumdestacada = $album . "/destacada";
            $formupload->creardirectorio($albumdestacada);
            $albumdestacada = $album . "/destacada_small";
            $formupload->creardirectorio($albumdestacada);
            $albumdestacada = $album . "/cercanas";
            $formupload->creardirectorio($albumdestacada);
            $albumthumb = $album . "/thumb";
            $formupload->creardirectorio($albumthumb);

            $adapter = new Zend_File_Transfer_Adapter_Http(array('ignoreNoFile' => true)); /* No procesa los input file que no contienen archivos */

            // Limit the size a given file to maximum 4MB and mimimum 10kB
            // Also returns the plain number in case of an error
            // instead of a user-friendly number
            $adapter->addValidator('Size', false, array('min' => '2kB', 'max' => '1MB', 'bytestring' => true));

            $adapter->addValidator('Count', false, array('min' => 1, 'max' => 6));
            $adapter->addValidator('Extension', false, 'jpg,jpeg,jpe,gif,png');
            $adapter->addValidator('ExcludeExtension', false, 'php,exe,sh,zip,rar,html,htm,bat');

            $permitted = array('image/jpg', 'image/jpeg', 'image/jpe', 'image/gif', 'image/png');

            // Set a new destination path and overwrites existing files
            $ext = array("jpg", "jpeg", "jpe", "gif", "png");

            try {

                $fotos_upload = $adapter->getFileInfo();
                $increase = $formupload->get_size_album_BD($idpropiedad, $albumid);
                $increase++;
                $mensaje = "";

                $files2 = new Dgt_Getfiles($albumnormal, $ext);
                $list2 = $files2->getlist();
                $size2 = count($list2);
                $primerafoto = $size2;

                foreach ($fotos_upload as $file => $info) {
                    $typeOK = false;
                    if (!$adapter->isValid($file)) {
                        $mensaje = sprintf("El archivo %s no cumple con los requisitos validos.", $info['name']);
                        Zend_Debug::dump($mensaje);
                        continue;
                    }

                    // check that file is of a permitted MIME type
                    foreach ($permitted as $type) {
                        if ($type == $info['type']) {
                            $typeOK = true;
                            break;
                        }
                    }

                    if ($typeOK) {
                        if ($adapter->isUploaded($file)) {

                            $path_to_image = $info['tmp_name'];
                            $path_to_normal = $albumnormal . "/" . $increase . '_' . strtolower($info['name']);
                            $path_to_urlImagesfinal = $urlImagesfinal . $increase . '_' . strtolower($info['name']);
                            $caption = $increase . '_' . strtolower($info['name']);
                            $path_to_thumb = $albumthumb . "/" . $increase . '_' . strtolower($info['name']);

                            //Funciones para Insertar el registro de las fotos y los thumbs
                            try {
                                $formupload->resize_image($path_to_image, $path_to_normal, '400', $thumb_method, 'wd', $default_file_mode, $GIF_support);
                                $formupload->resize_image($path_to_image, $path_to_thumb, '90', $thumb_method, 'wd', $default_file_mode, $GIF_support);
                            } catch (Zend_Exception $e) {
                                $mensaje.=$e->getMessage() . " - Exepcion en el thumb de la imagen normal";
                                $this->view->message.=$mensaje;
                            }

                            //Registrar la foto en la base de datos
                            $formupload->insert_fotos_album_db($albumid, $path_to_urlImagesfinal, "", "", "", $caption);

                            if ($primerafoto == 0) {
                                $fotopredeterminada = strtolower($info['name']);
                                $establecerfotoPrincipal = true;
                            }

                            $this->view->message = "Imagen " . $info['name'] . " subida exitosamente<br>";
                            $this->_flashMessenger->addSuccess($this->view->message);
                            $increase++;
                            $primerafoto++;
                        }
                    }
                }

                $files = new Dgt_Getfiles($albumnormal, $ext);
                $list = $files->getlist();

                $size = count($list);
                Zend_Debug::dump($fotopredeterminada);
                Zend_Debug::dump($establecerfotoPrincipal);

                if ($establecerfotoPrincipal) {
                    $nameimage = "1_" . $fotopredeterminada;
                    $album = $location . '/' . $albumid;
                    $idfoto = $formupload->get_id_fot_albumBD($idpropiedad, $albumid, $nameimage);


                    $path_to_image = $album . "/normal/" . $nameimage;
                    $path_to_marker = $album . '/marker/marker_' . $nameimage;
                    $path_to_preview = $album . '/destacada/destacada_' . $nameimage;
                    $path_to_preview2 = $album . '/destacada_small/destacada_small_' . $nameimage;
                    $path_to_preview3 = $album . '/cercanas/cercanas_' . $nameimage;

                    $urlImagesfinalNormal = $urlImages . $idpropiedad . '/' . $albumid . '/normal/' . $nameimage;
                    $urlImagesfinalMarker = $urlImages . $idpropiedad . '/' . $albumid . '/marker/marker_' . $nameimage;
                    $urlImagesfinalDestacada = $urlImages . $idpropiedad . '/' . $albumid . '/destacada/destacada_' . $nameimage;
                    $urlImagesfinalDestacadaSmall = $urlImages . $idpropiedad . '/' . $albumid . '/destacada_small/destacada_small_' . $nameimage;
                    $urlImagesfinalCercanas = $urlImages . $idpropiedad . '/' . $albumid . '/cercanas/cercanas_' . $nameimage;


                    try {
                        $formupload->resize_image($path_to_image, $path_to_marker, '116', $thumb_method, 'wd', $default_file_mode, $GIF_support);
                    } catch (Zend_Exception $e) {
                        $mensaje.=$e->getMessage() . " - <br> Exepcion en el thumb de la imagen de busqueda en el marcador";
                        echo $mensaje;
                        $this->view->message.=$mensaje;
                    }

                    try {
                        $formupload->resize_image($path_to_image, $path_to_preview, '182', $thumb_method, 'wd', $default_file_mode, $GIF_support);
                    } catch (Zend_Exception $e) {
                        $mensaje.=$e->getMessage() . " - Exepcion en el thumb de la imagen destacada";
                        echo $mensaje;
                        $this->view->message.=$mensaje;
                    }

                    try {
                        $formupload->resize_image($path_to_image, $path_to_preview2, '75', $thumb_method, 'wd', $default_file_mode, $GIF_support);
                    } catch (Zend_Exception $e) {
                        $mensaje.=$e->getMessage() . " - Exepcion en el thumb de la imagen destacada pequeña";
                        echo $mensaje;
                        $this->view->message.=$mensaje;
                    }

                    try {
                        $formupload->resize_image($path_to_image, $path_to_preview3, '123', $thumb_method, 'wd', $default_file_mode, $GIF_support);
                    } catch (Zend_Exception $e) {
                        $mensaje.=$e->getMessage() . " - Exepcion en el thumb de la imagen en cercania";
                        echo $mensaje;
                        $this->view->message.=$mensaje;
                    }

                    $formupload->update_foto_album_BD($idpropiedad, $albumid, $idfoto, $urlImagesfinalNormal, $urlImagesfinalMarker, $urlImagesfinalDestacada, $urlImagesfinalDestacadaSmall, $urlImagesfinalCercanas);
                }

                $formupload->update_album_BD($idpropiedad, $albumid, $locationBD, $size);
            } catch (Zend_File_Transfer_Exceptio $e) {
                echo $e->getMessage() . $location;
                $this->view->message = $e->getMessage() . $location;
            }

            $fbparams = Zend_Registry::get('fbparams');
            $urlvars = Dgt_Utils::make_query($fbparams, false);
            $this->_redirector->gotoUrl('/Mihousebook/edit?propiedad=' . $idpropiedad . "&$urlvars");

            exit();
        }
    }

    public function getimagesAction() {
        $destiny = getenv('DOCUMENT_ROOT') . '/propiedades/';
        $urlimages = '/propiedades/';

        if ($this->getRequest()->isGet()) {
            $db = Zend_Registry::get('db');

            $this->getRequest()->setParamSources(array('_GET'));

            $f = new Zend_Filter_StripTags();
            $idpropiedad = urldecode($f->filter($this->_request->getParam('propiedad')));

            $fb = Dgt_Fb::getInstance();

            $uid = $fb->get_uid();
            $idpropiedad = str_replace('"', '', $idpropiedad);
            $idpropiedad = substr($idpropiedad, 0, 7);
            $location = $destiny . $idpropiedad;

            $formupload = new Dgt_Formupload();
            $albumid = $formupload->get_album_BD($idpropiedad);

            $pathfinalimage = $urlimages . $idpropiedad . "/" . $albumid . "/normal/";
            $album = $location . '/' . $albumid;
            $albumnormal = $album . "/normal";

            try {
                $dir = $albumnormal . "/";

                $images = array();
                $d = dir($dir);

                while ($name = $d->read()) {

                    if (!preg_match('/\.(jpg|JPG|jpeg|jpe|gif|png)$/', $name))
                        continue;
                    $size = filesize($dir . $name);
                    $lastmod = filemtime($dir . $name) * 1000;
                    $images[] = array('name' => $name, 'size' => $size, 'lastmod' => $lastmod, 'url' => $pathfinalimage . $name);
                }

                $d->close();
                $o = array('images' => $images);
                echo json_encode($o);
                exit();
            } catch (Zend_File_Transfer_Exception $e) {
                echo $e->getMessage() . $location;
                exit();
            }
        } else {
            // no post data
            // no retornamos nada
            exit();
        }
    }

    public function establecerdeterminadaAction() {
        $destiny = getenv('DOCUMENT_ROOT') . '/propiedades/';
        $urlimages = '/propiedades/';

        $db = Zend_Registry::get('db');
        if ($this->getRequest()->isGet()) {

            // no post data
            // no retornamos nada
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));

            $f = new Zend_Filter_StripTags();

            $idpropiedad = $f->filter($this->_request->getParam('propiedad'));
            $nameimage = $f->filter($this->_request->getParam('nameimage'));

            $fb = Dgt_Fb::getInstance();

            $uid = $fb->get_uid();
            $thumb_method = "gd2";
            $thumb_use = "any";
            $default_file_mode = '0666';


            $formupload = new Dgt_Formupload();
            $albumid = $formupload->get_album_BD($idpropiedad);
            $idfoto = $formupload->get_id_fot_albumBD($idpropiedad, $albumid, $nameimage);
            $location = $destiny . $idpropiedad;
            $album = $location . '/' . $albumid;

            $path_to_image = $album . "/normal/" . $nameimage;
            $path_to_marker = $album . '/marker/marker_' . $nameimage;
            $path_to_preview = $album . '/destacada/destacada_' . $nameimage;
            $path_to_preview2 = $album . '/destacada_small/destacada_small_' . $nameimage;
            $path_to_preview3 = $album . '/cercanas/cercanas_' . $nameimage;

            $urlImagesfinalNormal = $urlimages . $idpropiedad . '/' . $albumid . '/normal/' . $nameimage;
            $urlImagesfinalMarker = $urlimages . $idpropiedad . '/' . $albumid . '/marker/marker_' . $nameimage;
            $urlImagesfinalDestacada = $urlimages . $idpropiedad . '/' . $albumid . '/destacada/destacada_' . $nameimage;
            $urlImagesfinalDestacadaSmall = $urlimages . $idpropiedad . '/' . $albumid . '/destacada_small/destacada_small_' . $nameimage;
            $urlImagesfinalCercanas = $urlimages . $idpropiedad . '/' . $albumid . '/cercanas/cercanas_' . $nameimage;

            try {
                try {
                    $formupload->resize_image($path_to_image, $path_to_marker, '116', $thumb_method, 'wd', $default_file_mode, $GIF_support);
                } catch (Zend_Exception $e) {
                    $mensaje.=$e->getMessage() . " - <br> Exepcion en el thumb de la imagen de busqueda en el marcador";
                    echo $mensaje;
                    $this->view->message.=$mensaje;
                }

                try {
                    $formupload->resize_image($path_to_image, $path_to_preview, '182', $thumb_method, 'wd', $default_file_mode, $GIF_support);
                } catch (Zend_Exception $e) {
                    $mensaje.=$e->getMessage() . " - Exepcion en el thumb de la imagen destacada";
                    echo $mensaje;
                    $this->view->message.=$mensaje;
                }

                try {
                    $formupload->resize_image($path_to_image, $path_to_preview2, '75', $thumb_method, 'wd', $default_file_mode, $GIF_support);
                } catch (Zend_Exception $e) {
                    $mensaje.=$e->getMessage() . " - Exepcion en el thumb de la imagen destacada pequeña";
                    echo $mensaje;
                    $this->view->message.=$mensaje;
                }

                try {
                    $formupload->resize_image($path_to_image, $path_to_preview3, '123', $thumb_method, 'wd', $default_file_mode, $GIF_support);
                } catch (Zend_Exception $e) {
                    $mensaje.=$e->getMessage() . " - Exepcion en el thumb de la imagen en cercania";
                    echo $mensaje;
                    $this->view->message.=$mensaje;
                }

                $formupload->update_foto_album_BD($idpropiedad, $albumid, $idfoto, $urlImagesfinalNormal, $urlImagesfinalMarker, $urlImagesfinalDestacada, $urlImagesfinalDestacadaSmall, $urlImagesfinalCercanas);
                echo "success";
                exit();
            } catch (Zend_File_Transfer_Exception $e) {
                echo $e->getMessage() . $location;
                $this->view->message = $e->getMessage() . $location;
            }
            exit();
        }
    }

    public function savedescripcionAction() {

        $db = Zend_Registry::get('db');
        if ($this->getRequest()->isGet()) {
            // no post data
            // no retornamos nada
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));

            $f = new Zend_Filter_StripTags();

            $descripcion = $f->filter($this->_request->getParam('descripcion'));
            $idpropiedad = $f->filter($this->_request->getParam('idpropiedad'));

            // Insert the new record.
            $row = array(
                'descripcion' => $descripcion,
                'ultima_modificacion' => new Zend_Db_Expr('CURDATE()')
            );
            try {
                $db->beginTransaction();
                $where[] = $db->quoteInto("idpropiedad=?", $idpropiedad, 'INTEGER');
                $db->update('propiedad', $row, $where);
                $db->commit();
                echo "success";
                exit();
            } catch (Exception $ex) {
                echo "Error al actualizar el default del perfil de la propiedad<br>";
                $db->rollBack();
                return FALSE;
            }
        }

        exit();
    }

    public function eliminarfotoAction() {
        $destiny = getenv('DOCUMENT_ROOT') . '/propiedades/';
        $urlimages = '/propiedades/';

        $db = Zend_Registry::get('db');
        if ($this->getRequest()->isGet()) {

            // no post data
            // no retornamos nada
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));


            $f = new Zend_Filter_StripTags();

            $idpropiedad = $f->filter($this->_request->getParam('propiedad'));

            $nameimage = $f->filter($this->_request->getParam('nameimage'));

            $fb = Dgt_Fb::getInstance();

            $uid = $fb->get_uid();
            $thumb_method = "gd2";
            $thumb_use = "any";
            $default_file_mode = '0666';

            $formupload = new Dgt_Formupload();
            $albumid = $formupload->get_album_BD($idpropiedad);
            $idfoto = $formupload->get_id_fot_albumBD($idpropiedad, $albumid, $nameimage);
            $location = $destiny . $idpropiedad;
            $album = $location . '/' . $albumid;

            $path_to_image = $album . "/normal/" . $nameimage;
            $path_to_marker = $album . '/marker/marker_' . $nameimage;
            $path_to_preview = $album . '/destacada/destacada_' . $nameimage;
            $path_to_preview2 = $album . '/destacada_small/destacada_small_' . $nameimage;
            $path_to_preview3 = $album . '/cercanas/cercanas_' . $nameimage;
            $path_to_thumb = $album . "/thumb/" . $nameimage;

            $urlImagesfinalNormal = "/img/no_photoNormal.jpg";
            $urlImagesfinalMarker = "/img/no_photo.gif";
            $urlImagesfinalDestacada = "/img/no_photoDestacada.jpg";
            $urlImagesfinalDestacadaSmall = "/img/no_photoDestacadaSmall.jpg";
            $urlImagesfinalCercanas = "/img/no_photoCercanas.jpg";

            try {
                try {
                    if (file_exists($path_to_image)) {
                        unlink($path_to_image);
                    }
                } catch (Zend_Exception $e) {
                    $mensaje.=$e->getMessage() . " - <br> Exepcion al eliminar la imagen principal";
                    echo $mensaje;
                    $this->view->message.=$mensaje;
                }

                try {
                    if (file_exists($path_to_marker)) {
                        unlink($path_to_marker);
                    }
                } catch (Zend_Exception $e) {
                    $mensaje.=$e->getMessage() . " - <br> Exepcion al eliminar la imagen de busqueda en el marcador";
                    echo $mensaje;
                    $this->view->message.=$mensaje;
                }

                try {
                    if (file_exists($path_to_preview)) {
                        unlink($path_to_preview);
                    }
                } catch (Zend_Exception $e) {
                    $mensaje.=$e->getMessage() . " - Exepcion al eliminar la imagen destacada";
                    echo $mensaje;
                    $this->view->message.=$mensaje;
                }

                try {
                    if (file_exists($path_to_preview2)) {
                        unlink($path_to_preview2);
                    }
                } catch (Zend_Exception $e) {
                    $mensaje.=$e->getMessage() . " - Exepcion al eliminar la imagen destacada pequeña";
                    echo $mensaje;
                    $this->view->message.=$mensaje;
                }

                try {
                    if (file_exists($path_to_preview3)) {
                        unlink($path_to_preview3);
                    }
                } catch (Zend_Exception $e) {
                    $mensaje.=$e->getMessage() . " - Exepcion al eliminar la imagen en cercania";
                    echo $mensaje;
                    $this->view->message.=$mensaje;
                }

                try {
                    if (file_exists($path_to_thumb)) {
                        unlink($path_to_thumb);
                    }
                } catch (Zend_Exception $e) {
                    $mensaje.=$e->getMessage() . " - Exepcion al eliminar el thumb de la galeria de imagenes";
                    echo $mensaje;
                    $this->view->message.=$mensaje;
                }

                $formupload->delete_foto_album_BD($idpropiedad, $albumid, $idfoto, $urlImagesfinalNormal, $urlImagesfinalMarker, $urlImagesfinalDestacada, $urlImagesfinalDestacadaSmall, $urlImagesfinalCercanas);
                echo "success";
                exit();
            } catch (Zend_File_Transfer_Exception $e) {
                echo $e->getMessage() . $location;
                $this->view->message = $e->getMessage() . $location;
            }
            exit();
        }
    }

    public function saveAction() {

        $db = Zend_Registry::get('db');

        $f = new Zend_Filter_StripTags();

        if ($this->_request->isPost()) {

            try {

                $Precio = trim($f->filter($this->_request->getParam('precio')));
                $actualizar_data_propiedad = array(
                    'tipo_enlistamiento' => (int) trim($f->filter($this->_request->getParam('tipo_enlistamiento'))),
                    'tipo_propiedad' => trim($f->filter($this->_request->getParam('type_property'))),
                    'descripcion' => trim($f->filter($this->_request->getParam('descripcion'))),
                    'direccion' => trim($f->filter($this->_request->getParam('direction'))),
                    'pais' => trim($f->filter($this->_request->getParam('country'))),
                    'estado' => trim($f->filter($this->_request->getParam('estado'))),
                    'ciudad' => trim($f->filter($this->_request->getParam('ciudad'))),
                    'area' => (int) trim($f->filter($this->_request->getParam('area'))),
                    'cuartos' => (int) trim($f->filter($this->_request->getParam('cuarto'))),
                    'banos' => (int) trim($f->filter($this->_request->getParam('bano'))),
                    'tamano_lote' => trim((float) $f->filter($this->_request->getParam('tamano_lote'))),
                    'zip_code' => trim($f->filter($this->_request->getParam('zip_code'))),
                    'lat' => trim($f->filter($this->_request->getParam('latitud'))),
                    'lng' => trim($f->filter($this->_request->getParam('longitud'))),
                    'uid' => trim($f->filter($this->_request->getParam('uid'))),
                    'ultima_modificacion' => new Zend_Db_Expr('CURDATE()'),
                    'estatus' => trim($f->filter($this->_request->getParam('estatus')))
                );


                $tipoPrecio = $f->filter($this->_request->getParam('tipo_enlistamiento'));

                if ($tipoPrecio == 1)
                    $actualizar_data_propiedad['precio_venta'] = $Precio; //$precio_venta;
                if ($tipoPrecio == 2)
                    $actualizar_data_propiedad['precio_alquiler'] = $Precio; //$precio_alquiler;
                if ($tipoPrecio == 3) {
                    $actualizar_data_propiedad['precio_venta'] = $Precio; //$precio_venta;
                    $actualizar_data_propiedad['precio_alquiler'] = $Precio; //$precio_alquiler;
                }

                $idpropiedad = $f->filter($this->_request->getParam('idpropiedad'));

                $db->beginTransaction();
                $where[] = $db->quoteInto("idpropiedad=?", $idpropiedad, 'INTEGER');
                $db->update('propiedad', $actualizar_data_propiedad, $where);

                Zend_Registry::set('message', "Su propiedad #$idpropiedad ha sido actualizada exitosamente");
                $this->view->message = Zend_Registry::get('message');
                $db->commit();

                $fb = Dgt_Fb::getInstance();
                $uid = $fb->get_uid();
                $this->view->appusers = $fb->getFriendsApp();
                $fbparams = Zend_Registry::get('fbparams');
                $urlvars = Dgt_Utils::make_query($fbparams, false);
                $this->_redirector->gotoUrl('/Mihousebook/edit?propiedad=' . $idpropiedad . "&$urlvars");
                exit();
            } catch (Exception $ex) {
                Zend_Debug::dump($ex);
            }
        } else {
            Zend_Debug::dump($this->getRequest()->getPost());
        }
    }

    public function getCboCountry($selected) {
        //Agregando campo vacio
        $empty = new stdClass();
        $empty->id = '';
        $empty->label = 'Seleccione una opción';

        $option = Dgt_Enlistar::getPais();
        array_unshift($option, $empty);

        $cbo = array();
        foreach ($option as $index => $value) {
            if ($selected == $value->id)
                $cbo[] = sprintf("<option value='%s' selected='selected'>%s</option>", $value->id, $value->label);
            else
                $cbo[] = sprintf("<option value='%s'>%s</option>", $value->id, $value->label);
        }
        return join(" ", $cbo);
    }

    public function getCbotype_property($selected) {
        //Agregando campo vacio
        $empty = new stdClass();
        $empty->id = '';
        $empty->label = 'Seleccione una opción';

        $option = Dgt_Enlistar::gettype_property();
        array_unshift($option, $empty);

        $cbo = array();
        foreach ($option as $index => $value) {
            if ($selected == $value->id)
                $cbo[] = sprintf("<option value='%s' selected='selected'>%s</option>", $value->id, $value->label);
            else
                $cbo[] = sprintf("<option value='%s'>%s</option>", $value->id, $value->label);
        }
        return join(" ", $cbo);
    }

    public function deleteAction() {
        $db = Zend_Registry::get('db');
        if ($this->getRequest()->isPost()) {
            $id = $this->_request->getParam('id', null);
            if (isset($id) && is_numeric($id)) {
                $rs = $db->delete('usuarios', 'idusuario =' . $id);
                echo '{"success": true}';
                exit();
            } else {
                echo '{"success": false}';
                exit();
            }
        } else {
            echo '{"success": false}';
            exit();
        }
    }

    public function uploadAction() {
        $this->view->title = "Upload new Image";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $form = new Form_ImageUpload();
        $form->submit->setLabel('Upload');
        if (!$this->getRequest()->isPost()) {
            $this->view->form = $form;
        } elseif (!$form->isValid($_POST)) {
            $this->view->failedValidation = true;
            $this->view->form = $form;
            return;
        } else {
            $values = $form->getValues();
            $uploadname = $form->image->getFileName();

            chmod('<path info>/uploads/' . $uploadname, 755); // not necessary but worked fine for me
            Zend_Session::start();
            $fb = Dgt_Fb::getInstance();
            $uid = $fb->get_uid();
            $table = new Model_DbTable_Images;
            $table->insertImage($values['caption'], $values['src'], $uid);
            $fullFilePath = '/propiedad/' . $table->lastID() . self::findexts($uploadname);
            $filterFileRename = new Zend_Filter_File_Rename(array('target' => $fullFilePath, 'overwrite' => true));
            $filterFileRename->filter($uploadname);
            $this->view->imageUploaded = true;
            chmod($fullFilePath, 0755);
            exit();
        }
    }

    public function changestatusAction() {
        $db = Zend_Registry::get('db');
        if ($this->getRequest()->isPost()) {

            $id = $this->_request->getParam('id', null);
            if (isset($id) && is_numeric($id)) {
                $select = $db->select()->from('propiedad', array('estatus'))->where('idpropiedad = ?', $id);
                $stmt = $db->query($select);
                $obj = $stmt->fetchObject();
                $estadocambiado = $obj->estatus == 3 ? 4 : 3;

                $update_data = array(
                    'estatus' => $estadocambiado,
                    'ultima_modificacion' => new Zend_Db_Expr('CURDATE()')
                );
                $db->update('propiedad', $update_data, 'idpropiedad = ' . $id);
                echo '{"success": true}';
                exit();
            } else {
                echo '{"success": false}';
                exit();
            }
        } else {
            echo '{"success": false}';
            exit();
        }
    }

    public function changestatus2Action() {
        $db = Zend_Registry::get('db');
        if ($this->getRequest()->isPost()) {

            $id = $this->_request->getParam('id', null);
            if (isset($id) && is_numeric($id)) {
                $select = $db->select()->from('propiedad', array('estatus'))->where('idpropiedad = ?', $id);
                $stmt = $db->query($select);
                $obj = $stmt->fetchObject();
                $estadocambiado = $obj->estatus == 1 ? 2 : 1;

                $update_data = array(
                    'estatus' => $estadocambiado,
                    'ultima_modificacion' => new Zend_Db_Expr('CURDATE()')
                );
                $db->update('propiedad', $update_data, 'idpropiedad = ' . $id);
                echo '{"success": true}';
                exit();
            } else {
                echo '{"success": false}';
                exit();
            }
        } else {
            echo '{"success": false}';
            exit();
        }
    }

    /* Ajax functions */

    public function getusersAction() {
        $rs = Dgt_Data_Utils::getUsers();
        echo '{ "rows": ' . Zend_Json::encode($rs) . '}';
        exit();
    }

    public function getregistersAction() {
        if ($this->getRequest()->isPost()) {
            try {

                $db = Zend_Registry::get('db');

                $fb = Dgt_Fb::getInstance();
                $uid = $fb->get_uid();

                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 50);
                $sort = $this->_request->getParam('sort', "idpropiedad");
                $dir = $this->_request->getParam('dir', "ASC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);
                $rs = Dgt_Enlistar::getProperty($sort, $dir, $start, $limit, $uid);
                $rs_count = Dgt_Enlistar::getPropertyCount($uid);

                $results = $rs_count[0]->total;
                echo '{"success":true, "results":' . $results . ', "rows":' . Zend_Json::encode($rs) . '}';
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la session.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
    }

}
