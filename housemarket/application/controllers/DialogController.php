<?php

class DialogController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function deleteAction(){
        // minified and concatenated: send-inquiry.css
        $this->view->headLink()->appendStylesheet("/css/sendinquiry.min.css?v=1");
    }
    
    public function confirmshareAction(){
    	$f = new Zend_Filter_StripTags();
        // minified and concatenated: send-inquiry.css
        $this->view->headLink()->appendStylesheet("/css/sendinquiry.min.css?v=1");
        $this->view->correo = $f->filter($this->_request->getParam('correo', null));
    }


}

