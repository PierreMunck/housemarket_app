<?php
class RoommatesController extends MainController {
    protected $_flashMessenger = null;
    protected $_redirector = null;
    protected $_applicationParameter = null;
    protected $_fb = null;
    protected $_id_item = null;
    /**
     * Cantidad de personas que habitan una casa
     * @var Array
     */
    public static $_CANTITY_OCCUPY = array(1, 19);

    /**
     * Cantidad de espacios disponibles
     * @var Array
     */
    public static $_AVAILABLE_SPACES = array(1, 10);

    /**
     * Cantidad maxima de ocupantes permitidos
     * @var Array
     */
    public static $_CANTITY_MAX_OCCUPIERS = array(1, 10);

    /**
     * Edades comprendida en un rango para determinar la edad de preferencia para el roommate
     * @var Array rango de valores
     */
    public static $_AGES_RANGE = array(16, 100);

    /**
     * Meses de estad�a en el cuarto
     */

    /**
    *
    * fonction de retour de label de navigation courant
    */
    public function getCurrentLabelnav() {
    	return 'roommates';
    }
    
    /**
     * Etiquetas para los valores de los campos en la tabla
     * @var Array
     */
    public static $_STAY_MONTHS = array(1,12);

    public static $_TITLE_BY_LABEL  = array(
                'TipoAlojamiento'   => "<i18n>LABEL_ROOM_STATE</i18n>",
                'EstadoRoommate'    => "<i18n>LABEL_ROOM_STATE</i18n>",
                'TipoPropiedad'     => "<i18n>LABEL_PROPERTY_TYPE</i18n>",
                'Amueblado'         => "<i18n>LABEL_RMT_FURNISHED_TYPE</i18n>",
                'TipoCuarto'        => "<i18n>LABEL_ROOM_TYPE</i18n>",
                'TipoBano'          => "<i18n>LABEL_ROOM_TYPE_BATH</i18n>",
                'FechaDisponible'   => "<i18n>ADD_ROOM_AVAILABLE_DATE</i18n>",
                'FechaMudanza'      => "<i18n>LABEL_RMT_DATE_CHANGE</i18n>",
                'EstanciaMinima'    => "<i18n>LABEL_ROOM_STAY_MONTHS</i18n>",
                'CortoPlazo'        => "<i18n>ADD_ROOM_BASIC_INFO_SHORT_TERM</i18n>",
                'MaxCapacidad'      => "<i18n>LABEL_ROOM_MAX_CAPACITY</i18n>",
                'FumadorHabitan'    => "<i18n>LABEL_ROOM_SMOKER</i18n>",
                'IdiomaHabitan'     => "<i18n>LABEL_ROOM_LANGUAGE</i18n>",
                'OcupacionHabitan'  => "<i18n>LABEL_ROOM_OCCUPATION</i18n>",
                'GeneroHabitan'     => "<i18n>LABEL_ROOM_GENDER</i18n>",
                'MascotaHabitan'    => "<i18n>LABEL_ROOM_PET</i18n>",
                'HijosHabitan'      => "<i18n>LABEL_ROOM_CHILDREN</i18n>",
                'OrientacionHabitan'=> "<i18n>LABEL_ROOM_SEXUAL_PREFERENCES</i18n>",
                'GeneroAcepta'      => "<i18n>LABEL_ROOM_GENDER</i18n>",
                'ParejaAcepta'      => "<i18n>LABEL_ROOM_COUPLE</i18n>",
                'NinosAcepta'      => "<i18n>LABEL_ROOM_CHILDREN</i18n>",
                'MascotaAcepta'      => "<i18n>LABEL_ROOM_PET</i18n>",
                'FumadorAcepta'      => "<i18n>LABEL_ROOM_SMOKER</i18n>",
                'OcupacionAcepta'      => "<i18n>LABEL_ROOM_OCCUPATION</i18n>",
                'FumadorBusca'      => "<i18n>LABEL_ROOM_SMOKER</i18n>",
                'IdiomaBusca'      => "<i18n>LABEL_ROOM_LANGUAGE</i18n>",
                'OcupacionBusca'      => "<i18n>LABEL_ROOM_OCCUPATION</i18n>",
                'GeneroBusca'      => "<i18n>LABEL_ROOM_GENDER</i18n>",
                'MascotaBusca'      => "<i18n>LABEL_ROOM_PET</i18n>",
                'HijosBusco'      => "<i18n>LABEL_ROOM_CHILDREN</i18n>",
                'CantBusca'      => "<i18n>LABEL_RMT_CANTITY_SEARCH</i18n>",
                );

    const TIPO_AMUEBLADO_BUSCO = 18;
    const TIPO_PROPIEDAD_BUSCO = 19;
    const TIPO_BUSQUEDA = 21;
    const TIPO_CUARTO_BUSCO = 22;
    const TIPO_BANO_BUSCO = 23;

    const ESTADO_CUARTO = 20;
    const TIPO_BANO_TENGO = 4;
    const TIPO_CUARTO_TENGO = 3;
    const FUMADOR_TENGO = 5;
    const OCUPACION_TENGO = 6;
    const GENERO_TENGO = 7;
    const MASCOTAS_TENGO = 8;
    const ORIENTACION_TENGO = 9;
    const HIJOS_TENGO = 10;

    const TIPO_ALOJAMIENTO = 1;
    const TIPO_PROPIEDAD_TENGO = 2;
    const GENERO_ACEPTADO = 11;
    const PAREJA_ACEPTADA = 12;
    const NINOS_ACEPTADO = 13;
    const MASCOTA_ACEPTADA = 14;
    const FUMADOR_ACEPTADO = 15;
    const ORIENTACION_ACEPTADA = 16;
    const OCUPACION_ACEPTADA = 17;
    const PUBLISHED_RMT_STATE = 64;

    public function init() {
    	parent::init();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
        $this->view->web_path = $this->cfg['fb_app_url'];
    }

    public function preDispatch() {
   		// pasamos directamente sobre busco un cuarto si el usuario no tiene anuncio
    	$uid = Zend_Registry::get('uid');
    	$params = $this->_request->getParams();
    	if($params['action'] == 'busquedacuarto' || $params['action'] == 'busquedaroommate'){
    		if($this->view->seeAddRoommate){
    			if($params['action'] == 'busquedacuarto'){
    				$params['action'] = 'agrbusco';
    			}
    			if($params['action'] == 'busquedaroommate'){
    				$params['action'] = 'agrtengo';
    			}
    			$params['controller'] = 'roommates';
    			$params['module'] = 'default';
    			if($this->view->seeAddRoommate){
    				$this->view->urltengo = $this->cfg['fb_app_url']. "roommates/agrtengo";
    				$this->view->urlbusco = $this->cfg['fb_app_url']. "roommates/agrbusco";
    				$this->_forward($params['action'], $params['controller'], $params['module'], $params);
    			}
    		}
    		
    	}
    	
    }

     public function busquedacuartoAction() {
     	$this->currentActionNav = 'busquedacuarto';
     	$this->navAction();
		// menu de navegaci�n
        
        $appusers = $this->fbDgt->getFriendsApp();
        // action body
        $google_api_key = $this->cfg['google_api_key'];

        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
        $this->view->headLink()->appendStylesheet('/css/search.css');
        $this->view->headScript()->appendFile("https://maps.google.com/maps?file=api&v=2&sensor=false&key=$google_api_key",'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        $this->view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery.tools.1.1.2.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery.labelify.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/buscar_cuarto.js', 'text/javascript');
        $this->view->loadmap = 'onunload="GUnload();"';

        $cliente = new Hm_Cli_Cliente();
        $prop = new Dgt_Propiedad();

        

        foreach($appusers as &$user){
            $user["first_name"] = implode("<br/>",$cliente->SectionString($user["first_name"]));
            $user["last_name"] = implode("<br/>",$cliente->SectionString($user["last_name"]));
        }

        $this->view->appusers = $appusers;
        $this->view->totalAppUsers = count($this->fbDgt->getFriendsAppAll());

        $cate = Hm_Cat_Categoria::getInstance();

        $searchTypes = Hm_Cat_Enlistamiento::GetListingTypes();
        $listing = "<li valor='' class='active'><a href='#tab1'><i18n>LABEL_SEARCH_FILTER_LISTING_TYPE</i18n></a></li>";
        $i = 1;
        foreach ($searchTypes as $index => $type) {
            $listing .= "<li valor='".$index."' ><a href='#tab".$i."'>".$type."</a></li>";
            $i++;
        }

        $this->view->searchTypes = $listing;
        $this->view->brokers = $cliente->GetConsultaClientes();
        $this->view->totalBrokers = count($cliente->GetAllFacebookBrokers());
        $this->view->UMSuperficies = array("vr2"=>"vr2", "mt2"=>"mt2", "ft2"=>"ft2");

        $this->view->web_path = $this->cfg['fb_app_url'];
     }

     public function elegiranuncioAction() {
     	$this->view->urltengo = $this->cfg['fb_app_url']. "roommates/agrtengo";
     	$this->view->urlbusco = $this->cfg['fb_app_url']. "roommates/agrbusco";
     }
     
     public function busquedaroommateAction() {
     	$this->currentActionNav = 'busquedaroommate';
     	$this->navAction();

        $appusers = $this->fbDgt->getFriendsApp();
        $google_api_key = $this->cfg['google_api_key'];

        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
        $this->view->headLink()->appendStylesheet('/css/search.css?v=2');
        $this->view->headScript()->appendFile("https://maps.google.com/maps?file=api&v=2&sensor=false&key=$google_api_key",'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        $this->view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery.tools.1.1.2.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery.labelify.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/buscar_roommate.js?v=60', 'text/javascript');
        $this->view->loadmap = 'onunload="GUnload();"';

        $cliente = new Hm_Cli_Cliente();
        $prop = new Dgt_Propiedad();

        

        foreach($appusers as &$user){
            $user["first_name"] = implode("<br/>",$cliente->SectionString($user["first_name"]));
            $user["last_name"] = implode("<br/>",$cliente->SectionString($user["last_name"]));
        }

        $this->view->appusers = $appusers;
        $this->view->totalAppUsers = count($this->fbDgt->getFriendsAppAll());

        $cate = Hm_Cat_Categoria::getInstance();
        $language = Zend_Registry::get('language');
        $categories = $cate->GetCategoriesList($language);
        $this->view->categories = $categories;

        // Recuperar todos los atributos por las categorias.
        $form = new Zend_Form();

        $atrributes = array();
        if(count($categories) > 0) {
            foreach($categories as $categoryId=>$categoryName) {
                $subForm = new Zend_Form_SubForm();
                $subForm->setName(str_replace(" ", "", $categoryName));
                $subForm->setLegend($categoryName);
                $curAttributes = &$cate->GetAtributes($categoryId);

                // Por cada atributo.
                // Por cada categoria, agregar los atributos en grupos de despliegue.
              if(count($curAttributes)>0) {
                $attributes = array();
                foreach($curAttributes as $attribute) {
                    $typeAttribute = &$attribute["tipo"];
                    $idAttribute = &$attribute["codigo"];
                    $nameAttribute = iconv("UTF-8", "ISO-8859-1", $attribute["nombre"]);
                    $attributes[] = $idAttribute;
                    // Recuperar el tipo de atributo a renderizar
                    $elements = array();
                    if($typeAttribute=="Range") {
                        // Minimo
                        $min = new Zend_Form_Element_Text("min_".$idAttribute);
                        $min->setValue("MIN");
                        $min->setAttribs(array("class"=>"labelHighlight", "size"=>"5", "style"=>"width:30px;", "onfocus"=>"if(this.value=='MIN') { this.value = '';}"));
                        $min->clearDecorators();
                        $min->addDecorator("ViewHelper");
                        $subForm->addElement($min);

                        // Maximo
                        $max =  new Zend_Form_Element_Text("max_".$idAttribute);
                        $max->setValue("MAX");
                        $max->setAttribs(array("class"=>"labelHighlight", "size"=>"5", "style"=>"width:30px;", "onfocus"=>"if(this.value=='MAX') { this.value = '';}"));
                        $max->clearDecorators();
                        $max->addDecorator("ViewHelper");
                        $subForm->addElement($max);
                        $elements[] = "min_".$idAttribute;
                        $elements[] = "max_".$idAttribute;
                    } else if($typeAttribute=="Check"){
                        $opt = new Zend_Form_Element_Checkbox("value_".$idAttribute);
                        $opt->setValue("true");
                        $opt->setCheckedValue("S");
                        $opt->setUncheckedValue("N");
                        $opt->clearDecorators();
                        $opt->addDecorator("ViewHelper");
                        $subForm->addElement($opt);
                        $elements[] = "value_".$idAttribute;
                    } else if($typeAttribute=="Like"){
                        $value =  new Zend_Form_Element_Text("value_".$idAttribute);
                        $value->setValue("");
                        $value->setAttribs(array("class"=>"labelHighlight", "size"=>"5", "style"=>"width:30px;"));
                        $value->clearDecorators();
                        $value->addDecorator("ViewHelper");
                        $subForm->addElement($value);
                        $elements[] = "value_".$idAttribute;
                    }
                    // Agregarlas al subform en un display group.
                    if(count($elements)>0) {
                        $subForm->addDisplayGroup(
                                $elements,
                                $nameAttribute,
                                array("codigo"=>$idAttribute)
                        );
                    }
                 }
              }
                $decorator = new Forms_Decorator_Attributes();
                $subForm->clearDecorators();
                $subForm->addDecorator($decorator);
                $form->addSubForm($subForm, $categoryName);
            }
        }

        $this->view->form = $form;
        $this->view->attributes = $attributes;


        $searchTypes = Hm_Cat_Enlistamiento::GetListingTypes();
        $listing = "<li valor='' class='active'><a href='#tab1'><i18n>LABEL_SEARCH_FILTER_LISTING_TYPE</i18n></a></li>";
        $i = 1;
        foreach ($searchTypes as $index => $type) {
            $listing .= "<li valor='".$index."' ><a href='#tab".$i."'>".$type."</a></li>";
            $i++;
        }

        $this->view->searchTypes = $listing;
        $this->view->brokers = $cliente->GetConsultaClientes();
        $this->view->totalBrokers = count($cliente->GetAllFacebookBrokers());
        $this->view->UMSuperficies = array("vr2"=>"vr2", "mt2"=>"mt2", "ft2"=>"ft2");
        $this->view->web_path = $this->cfg['fb_app_url'];
     }


     public function agrtengoAction(){
     	$this->currentActionNav = 'agrtengo';
     	$this->navAction();
     	
        header("Connection: close");
        $this->_flashMessenger->clearMessages();
        
        $this->view->uid= $this->fbDgt->get_uid();
        $this->view->headTitle('Cuarto');
        $this->view->loadmap = 'onunload="GUnload();"';
        $google_api_key = $this->cfg['google_api_key'];
    
        $urlRoom="/roommates/busquedacuarto";
        
        $localLangague = Zend_Registry::get('Zend_Locale');
        
        $this->view->headLink()->appendStylesheet('/css/colorbox/colorbox.css');
        $this->view->headLink()->appendStylesheet("https://www.google.com/uds/css/gsearch.css");
        $this->view->headLink()->appendStylesheet("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.css");
        $this->view->headLink()->appendStylesheet("/css/custom-theme/jquery-ui-1.7.2.custom.css");
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
        $this->view->headLink()->appendStylesheet('/css/tabs.css');
        $this->view->headLink()->appendStylesheet('/css/style_form.css');

        $this->view->headScript()->appendFile("https://maps.google.com/maps?file=api&v=2&sensor=false&key=$google_api_key",'text/javascript');
        $this->view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/lang/'.$localLangague.'.js', 'text/javascript');
        $this->view->headScript()->appendFile("https://www.google.com/uds/api?file=uds.js&v=1.0&source=uds-msw&key=$google_api_key");
        $this->view->headScript()->appendFile("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.js?mode=new");
        $this->view->headScript()->appendFile("/js/agregar_cuarto.js", "text/javascript");
        $this->view->headScript()->appendFile('/js/jquery.forms.js', 'text/javascript');
        $this->view->headScript()->appendFile("/js/jquery.scrollTo.js", "text/javascript");
        $this->view->headScript()->appendFile("/js/validate/jquery.validate1.8.1.min.js", "text/javascript");
        $this->view->headScript()->appendFile("/js/jquery-ui-1.7.2.custom.min.js", "text/javascript");
        $this->view->headScript()->appendFile("/js/jquery.formatCurrency-1.4.0.min.js", "text/javascript");
        $this->view->headScript()->appendFile('/js/jquery.colorbox-min.js?v=1.3.17.2', 'text/javascript');

        $this->view->language = $localLangague;
        
        $room = new Hm_Rmt_Cuarto($_POST);
        $this->view->room = $room;
        
        $_BENEFICIOS_CUARTO = 24;

        //Comboboxes desplegado
        $this->view->cantBusca = self::GetNumberList(self::$_CANTITY_OCCUPY);
        $this->view->minAgeRmt = self::GetNumberList(self::$_AGES_RANGE);
        $this->view->maxAgeRmt = self::GetNumberList(self::$_AGES_RANGE);
        $this->view->espacioDisponible =  self::GetNumberList(self::$_AVAILABLE_SPACES);
        $this->view->estanciaMinima = self::GetNumberList(self::$_STAY_MONTHS);
        $this->view->maxCapacidad = self::GetNumberList(self::$_AVAILABLE_SPACES);

        $this->view->idiomaTengo = Hm_Cat_Idioma::getInstance()->LanguagueComboBox();
        $this->view->estadoCuarto = $room->GetValuesDomain(self::ESTADO_CUARTO);
        $this->view->tipoAlojamiento = $room->GetValuesDomain(self::TIPO_ALOJAMIENTO);
        $this->view->tipoPropiedad = $room->GetValuesDomain(self::TIPO_PROPIEDAD_TENGO);
        $this->view->cuartoTengo = $room->GetValuesDomain(self::TIPO_CUARTO_TENGO);
        $this->view->banoTengo = $room->GetValuesDomain(self::TIPO_BANO_TENGO);
        $this->view->fumadorTengo = $room->GetValuesDomain(self::FUMADOR_TENGO);
        $this->view->ocupacionTengo = $room->GetValuesDomain(self::OCUPACION_TENGO);
        $this->view->generoTengo = $room->GetValuesDomain(self::GENERO_TENGO);
        $this->view->mascotaTengo = $room->GetValuesDomain(self::MASCOTAS_TENGO);
        $this->view->hijosTengo = $room->GetValuesDomain(self::HIJOS_TENGO);
        $this->view->generoAcepta = $room->GetValuesDomain(self::GENERO_ACEPTADO);
        $this->view->parejaAcepta = $room->GetValuesDomain(self::PAREJA_ACEPTADA);
        $this->view->ninosAcepta = $room->GetValuesDomain(self::NINOS_ACEPTADO);
        $this->view->mascotaAcepta = $room->GetValuesDomain(self::MASCOTA_ACEPTADA);
        $this->view->fumadorAcepta = $room->GetValuesDomain(self::FUMADOR_ACEPTADO);
        $this->view->ocupacionAcepta = $room->GetValuesDomain(self::OCUPACION_ACEPTADA);
        $this->view->rsBen = $room->GetBenefits($_BENEFICIOS_CUARTO);

        $this->view->isNew = "S";

        $this->view->publicado = "N";
        $this->view->web_path = $this->cfg['fb_app_url'];
        $this->view->url_path = $this->cfg['local_app_url'];
        $this->view->urltengo = $this->cfg['fb_app_url']. "roommates/agrtengo";
        $this->view->urlbusco = $this->cfg['fb_app_url']. "roommates/agrbusco";
        $this->view->location = new stdClass();
        $this->view->location->cc = null;
        $this->view->location->city = null;
        $this->view->location->zip = null;
        $this->view->location->latc = null;
        $this->view->location->lng = null;
        
        // Determinar si es nueva o no.
        $f = new Zend_Filter_StripTags();
        $roomId = null;
        $InfoUser = Hm_Rmt_Roommate::GetTypeRoommateUser(Zend_Registry::get('uid'));

        $roomId = $f->filter($this->getRequest()->getParam("id", null)) ;
        
         if($roomId == null and $InfoUser["type"] != "newUser"){
             $this->_redirector->gotoUrlAndExit($urlRoom);
        }
       
        if( $this->getRequest()->isget() || $roomId !== null) {
            //Verficamos si es el propietario del id solicitado
            if($InfoUser["id"] != null and ($roomId != $InfoUser["id"])){
                 $this->_redirector->gotoUrlAndExit($urlRoom);
            }

            $this->view->isPublicada = $f->filter($this->getRequest()->getParam("publicada", null));


        // Recuperar la propiedad a partir del id proporcionado.
            $rs = $room->find($roomId);
            $uid = Zend_Registry::get('uid');

            if($rs != null && $rs->count() > 0) {
                $room = $rs->current();

                if($room->Uid == $uid ) { // El usuario es propietario
                    $this->view->room = $room;
                    $this->view->isNew = "N";
                    $this->view->Correo = $room->Correo;
                    
                    $this->view->location->cc = $room->ZipCode;
                    $this->view->location->city = $room->Ciudad;
                    $this->view->location->zip = $room->ZipCode;
                    $this->view->location->latc = $room->Latitud;
                    $this->view->location->lng = $room->Longitud;
                    
                    //Beneficios
                    $benefit = new Hm_Cat_BeneficioCuarto();
                    $rsBenefits = $benefit->fetchAll(
                                   $benefit->select()
                                    ->where('CodigoCuarto = ?', $room->CodigoCuarto));

                    if($rsBenefits->count() > 0){
                        foreach ($rsBenefits as $benefit) {
                            $benefits[] = $benefit->BeneficioCuarto;
                        }
                    }
                    $this->view->benRoom = $benefits;

                    $parametersRoom = array($room->TipoPropiedad);
                    $rmtCuardoObj = new Hm_Rmt_Cuarto();
                    $roomResult = $rmtCuardoObj->ObtenerDatosCuarto($parametersRoom);
                    
                    $this->view->lTipoPropiedad = $roomResult[$room->TipoPropiedad];

                    // Recuperar las fotos.
                    $this->view->rsPhotos = array();
                        $findPhoto = new Hm_Rmt_Foto();
                        $rsPhotos = $findPhoto->fetchAll(
                                                $findPhoto->select()
                                                ->where('CodigoCuarto = ?', $roomId)
                                                ->order('RMTFotoID')
                                    );

                        if($rsPhotos != null && $rsPhotos->count() > 0) { // La propiedad tiene fotos
                            $this->view->photoID = $rsPhotos->current()->RMTFotoID;
                            foreach($rsPhotos as $photo) {
                                if($photo->Principal === chr(0x00)) { // Si no es una foto destacada
                                    $this->view->rsPhotos["otras"][] = $photo;
                                } else { // Si es Principal agregamela a destacada
                                    $this->view->rsPhotos["princ"] = $photo;
                                }
                            }
                        }

                   if($room->EstadoRoommate==64) {
                        $this->view->publicado = "S";
                    }
                }
                else{
                    $this->_redirector->gotoUrlAndExit($urlRoom);
                }
            }

        }
        $isNew = $this->view->isNew=="S"?true:false;
        if($isNew){
            $user = $this->fbDgt->getDataUser($uid);
            $this->view->Correo = $user[0]['email'];
        }

        $this->view->totalPhotos = $this->GetAllowedTotalPhotos($isNew, $roomId);


     }

     public function agrbuscoAction(){
     	$this->currentActionNav = 'agrbusco';
		$this->navAction();
         header("Connection: close");
         $this->_flashMessenger->clearMessages();

        $urlRoommate="/roommates/busquedaroommate";
        $this->view->uid=$this->fbDgt->get_uid();
        $this->view->appusers = $this->fbDgt->getFriendsApp();

        $this->view->headTitle('Roommate');
        $this->view->loadmap = 'onunload="GUnload();"';
        $google_api_key = $this->cfg['google_api_key'];

        $localLangague = Zend_Registry::get('Zend_Locale');
        $this->view->headLink()->appendStylesheet('/css/colorbox/colorbox.css');
        $this->view->headLink()->appendStylesheet("https://www.google.com/uds/css/gsearch.css");
        $this->view->headLink()->appendStylesheet("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.css");
        $this->view->headLink()->appendStylesheet("/css/custom-theme/jquery-ui-1.7.2.custom.css");
        $this->view->headLink()->appendStylesheet('/css/layout.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
        $this->view->headLink()->appendStylesheet('/css/tabs.css');
        $this->view->headLink()->appendStylesheet("/css/messages.css");
        $this->view->headLink()->appendStylesheet('/css/style_form.css');

        $this->view->headScript()->appendFile("https://maps.google.com/maps?file=api&v=2&sensor=false&key=$google_api_key",'text/javascript');
        $this->view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/lang/'.$localLangague.'.js', 'text/javascript');
        $this->view->headScript()->appendFile("https://www.google.com/uds/api?file=uds.js&v=1.0&source=uds-msw&key=$google_api_key");
        $this->view->headScript()->appendFile("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.js?mode=new");
        $this->view->headScript()->appendFile("/js/agregar_roommate.js", "text/javascript");
        $this->view->headScript()->appendFile('/js/jquery.forms.js', 'text/javascript');
        $this->view->headScript()->appendFile("/js/jquery.scrollTo.js", "text/javascript");
        $this->view->headScript()->appendFile("/js/validate/jquery.validate1.8.1.min.js", "text/javascript");
        $this->view->headScript()->appendFile("/js/jquery-ui-1.7.2.custom.min.js", "text/javascript");
        $this->view->headScript()->appendFile("/js/jquery.formatCurrency-1.4.0.min.js", "text/javascript");
        $this->view->headScript()->appendFile('/js/jquery.colorbox-min.js?v=1.3.17.2', 'text/javascript');

        $this->view->language = $localLangague;
        // llenar con los informacion que ya tenemos del usuario
        $user_info = Zend_Registry::get('user_info');
        $signed = Zend_Registry::get('signed');
        $uid = Zend_Registry::get('uid');
        if(!isset($_POST['correo']) && isset($user_info['extra_data']) && isset($user_info['extra_data']['email']) ) {
        	$_POST['correo'] = $user_info['extra_data']['email'];
        }
        if(!isset($_POST['titulo']) && isset($user_info['name']) ) {
        	$_POST['titulo'] = $user_info['name'];
        }
        
        $roommate = new Hm_Rmt_Roommate($_POST);
        $this->view->roommate = $roommate;

        $this->view->urltengo = $this->cfg['fb_app_url']. "roommates/agrtengo";
        $this->view->urlbusco = $this->cfg['fb_app_url']. "roommates/agrbusco";

        //Comboboxes desplegado
        $this->view->cantBusca = self::GetNumberList(self::$_CANTITY_OCCUPY);
        $this->view->minAgeRmt = self::GetNumberList(self::$_AGES_RANGE);
        $this->view->maxAgeRmt = self::GetNumberList(self::$_AGES_RANGE);
        $this->view->espacioDisponible =  self::GetNumberList(self::$_AVAILABLE_SPACES);
        $this->view->estanciaMinima = self::GetNumberList(self::$_STAY_MONTHS);
        $this->view->maxCapacidad = self::GetNumberList(self::$_AVAILABLE_SPACES);
        
        $room = new Hm_Rmt_Cuarto();
        $this->view->estadoCuarto = $room->GetValuesDomain(self::ESTADO_CUARTO);
        $this->view->tipoBusqueda = $room->GetValuesDomain(self::TIPO_BUSQUEDA);
        $this->view->tipoPropiedad = $room->GetValuesDomain(self::TIPO_PROPIEDAD_BUSCO);
        $this->view->amuebladoBusca = $room->GetValuesDomain(self::TIPO_AMUEBLADO_BUSCO);
        $this->view->cuartoBusca = $room->GetValuesDomain(self::TIPO_CUARTO_BUSCO);
        $this->view->banoBusca = $room->GetValuesDomain(self::TIPO_BANO_BUSCO);
                
        $this->view->generoAcepta = $room->GetValuesDomain(self::GENERO_ACEPTADO);
        $this->view->parejaAcepta = $room->GetValuesDomain(self::PAREJA_ACEPTADA);
        $this->view->ninosAcepta = $room->GetValuesDomain(self::NINOS_ACEPTADO);
        $this->view->mascotaAcepta = $room->GetValuesDomain(self::MASCOTA_ACEPTADA);
        $this->view->fumadorAcepta = $room->GetValuesDomain(self::FUMADOR_ACEPTADO);
        $this->view->ocupacionAcepta = $room->GetValuesDomain(self::OCUPACION_ACEPTADA);
        
        $this->view->idiomaBusca = Hm_Cat_Idioma::getInstance()->LanguagueComboBox();
        $this->view->fumadorBusca = $room->GetValuesDomain(self::FUMADOR_TENGO);
        $this->view->ocupacionBusca = $room->GetValuesDomain(self::OCUPACION_TENGO);
        $this->view->generoBusca = $room->GetValuesDomain(self::GENERO_TENGO);
        $this->view->mascotaBusca = $room->GetValuesDomain(self::MASCOTAS_TENGO);
        $this->view->hijosBusca = $room->GetValuesDomain(self::HIJOS_TENGO);

        $this->view->isNew = "S";
        $this->view->publicado = "N";

        $this->view->location = new stdClass();
        $this->view->location->cc = null;
        $this->view->location->city = null;
        $this->view->location->zip = null;
        $this->view->location->latc = null;
        $this->view->location->lng = null;
        
        // Determinar si es nueva o no.
        $f = new Zend_Filter_StripTags();
        $roommateId = null;
        $currentLanguage = Hm_Cat_Idioma::getInstance()->GetLanguage();
         
        $InfoUser = Hm_Rmt_Roommate::GetTypeRoommateUser(Zend_Registry::get('uid'));
        $roommateId = $f->filter($this->getRequest()->getParam("id", null)) ;
        
        if($roommateId == null and $InfoUser["type"] != "newUser"){
             $this->_redirector->gotoUrlAndExit($urlRoommate);
        }
        
        if( $this->getRequest()->isget() || $roommateId !== null) {
            $this->view->isPublicada = $f->filter($this->getRequest()->getParam("publicada", null));

            //Verficamos si es el propietario del id solicitado
            if($InfoUser["id"] != null and ($roommateId != $InfoUser["id"])){
                 $this->_redirector->gotoUrlAndExit($urlRoommate);
            }

            $roommate = new Hm_Rmt_Roommate();
            $uid = Zend_Registry::get('uid');

            $rs = $roommate->find($roommateId);
            if($rs != null && $rs->count() > 0) {
                $roommate = $rs->current();
                if($roommate->Uid == $uid ) { // El usuario es propietario
                    $this->view->roommate = $roommate;
                    $this->view->isNew = "N";
                    $this->view->correo = $roommate->Correo;
                    $this->view->escuela = $roommate->Escuela;
                    
                    $this->view->location->cc = $roommate->ZipCode;
                    $this->view->location->city = $roommate->Ciudad;
                    $this->view->location->zip = $roommate->ZipCode;
                    $this->view->location->latc = $roommate->Latitud;
                    $this->view->location->lng = $roommate->Longitud;
                    
                    if($roommate->EstadoRoommate==64){
                        $this->view->publicado = "S";
                    }
                    
                    $tblPais = new Hm_Cat_Pais();
                    $tblCurrency = new Hm_Cat_Moneda();
                    $select = $tblCurrency->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
                    $select->setIntegrityCheck(false)
                            ->join("catpais","catpais.CodigoMoneda = catmoneda.CodigoMoneda")
                            ->where($tblCurrency->getAdapter()->quoteInto("catpais.ZipCode = ?", $roommate->ZipCode));
                    $this->view->currency = $tblCurrency->fetchRow($select);
                } 
                else{
                    $this->_redirector->gotoUrlAndExit($urlRoommate);
                }
            }
            if($this->view->isNew == "S"){
                $user = $this->fbDgt->getDataUser($uid);
                $this->view->correo = $user[0]['email'];
            }
        }
     }

     public function saveroomAction() {

         // Reiniciando los parametros de flashmessenger
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashmessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
        $status = array();
        $status['status'] = "error";

        $urlFacebook = $this->cfg['fb_app_url'];
        $translator = Zend_Registry::get('Zend_Translate');

        $f = new Zend_Filter_StripTags();
        $singularRoom = null;
        if( $this->getRequest()->isPost()) {
             $limit = 3;
            $limitSize = $limit * 1024 * 1024;
            try {
                foreach ($_FILES as $key => $file) {
                    if ($file['size'] > $limitSize) {
                        throw new Exception;
                    }
                }
            } catch (Exception $ex) {
                $this->_flashMessenger->addError("Images can't exceed " . $limit . "MB");
                exit();
            }
            try {
                $room = new Hm_Rmt_Cuarto();

                $publicado = $f->filter($this->_request->getParam("publicado"), "N");
                $new = $f->filter($this->_request->getParam("isNew"), "S");
                $isNew = $new=="S"?true:false;

                if(!$isNew) {
                    $id = $this->_request->getParam("codigoCuarto");
                    try {
                        $rsRoom = $room->find($id);
                        if($rsRoom->count() > 0) {
                        	$val = $rsRoom->getRow(0)->toArray();
                        	$room = new Hm_Rmt_Cuarto($val);
                            $publicar = ($publicado=="S" && $room->EstadoRoommate == self::PUBLISHED_RMT_STATE)?true:false;
                        } else {
                            $publicar = false;
                        }
                    } catch(Exception $ex) {
                        $publicar = ($publicado == "S") ? true : false;
                    }

                } else {
                    $publicar = ($publicado == "S") ? true : false;
                }
                $result = $room->SaveOrUpdate($this->_request->getParams());

                // Mensajes de validacion
                if(array_key_exists("fail", $result)) {
                    $messages = $result["reasons"];
                    $messageError = "";
                    if(is_array($messages)) {
                        $messageError .= "<ul>";
                        foreach($messages as $field=>$messageBlock) {
                            if(is_array($messageBlock)) {
                                foreach ($messageBlock as $message) {
                                    $messageError .= "<li>" . $message . "</li>";
                                }
                            } else {
                                $messageError .= "<li>" . $messageBlock . "</li>";
                            }
                        }
                        $messageError .= "</ul>";
                    } else { 
                        $messageError .= "<ul><li>" . $messages ."</li></ul>";
                    }
                    $this->_flashMessenger->addError("<i18n>Please check your data</i18n>" . $messageError);
                } else {
                    // Mensajes de advertencia al guardar el archivo.
                    // Revisar el campo de status y revisar si hay algun mensaje.
                    $isOk = true;
                    $warning = null;

                    $state = $result["status"];
                    $id = $result["id"];
                    if(is_array($state)) {
                        // Por el momento, solo revisaremos las fotos.
                        $photoStatus = isset($state["Album"]) ? $state["Album"] : array();
                        if(array_key_exists("fail", $photoStatus)) {
                            // Ocurrio una excepcion. Mostrarla al usuario.
                            $warning = $photoStatus["reason"];
                            $isOk = false;
                        } else {
                            if(isset($photoStatus["photos"]) && is_array($photoStatus["photos"])) {
                                $photosResult = $photoStatus["photos"];
                                if($photosResult["fails"] === true) { // Ocurrio un error, mostrar mensajes
                                    $warning = "<ul>";
                                    if(is_array($photosResult["reasons"])) {
                                        foreach($photosResult["reasons"] as $reasons) {
                                            if(is_array($reasons)) {
                                                foreach($reasons as $reason)
                                                    $warning .= "<li>" . $reason . "</li>";
                                            } else {
                                                $warning .= "<li>" . $reasons . "</li>";
                                            }
                                        }
                                    } else {
                                        $warning .= "<li>".$photosResult["reasons"]."</li>";
                                    }
                                    $warning .= "</ul>";
                                    $isOk = false;
                                }
                            }
                        }
                    }
                    
                    if($isOk) {
                        $this->_flashMessenger->addSuccess($translator->getAdapter()->translate("ADD_ROOM_SUCCESS_SAVE"));
                        $publicada = ($publicar)?'&publicada=1': '&publicada=0';

                        $singularRoom = $room->find($id);
                        $objFoto = new Hm_Rmt_Foto();
                        $photo = $objFoto->fetchRow(
                                $objFoto->select()
                                        ->from($objFoto,array('RMTFotoID'))
                                        ->where('CodigoCuarto = ? AND Principal = 1', $id)
                                        ->order('Principal DESC')
                        );

                        $status['publicar'] = $publicar;
                        $status['status'] = "success";
                        $status['redirect'] = $urlFacebook . "roommates/busquedaroommate/";
                        
                    } else {
                        $this->_flashMessenger->addSuccess($translator->getAdapter()->translate("ADD_ROOM_SUCCESS_SAVE"));
                        $this->_flashMessenger->addWarning("Warning" . $warning);
                        if($isNew) {
                            $status['redirect'] = ($urlFacebook . 'rommmates/agrtengo?id=' . $id);
                        }
                    }
                }
            }catch(Exception $ex) {
                $status['status'] = "error";
                $this->_flashMessenger->addError('Ocurrio un error al guardar los datos de su propiedad '. $ex->getMessage());
                
            }
        }

        $status['message'] = $this->view->flashMessenger();
        $xml = simplexml_load_string("<response />");
        $xml->addChild('status', $status['status']);
        $xml->addChild('message', $status['message']);
        
        if(isset($room)){
        	$tblCurrency = new Hm_Cat_Moneda();
        	$select = $tblCurrency->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
        	$select->setIntegrityCheck(false)
        	->join("catpais","catpais.CodigoMoneda = catmoneda.CodigoMoneda")
        	->where($tblCurrency->getAdapter()->quoteInto("catpais.ZipCode = ?", $room->ZipCode));
        	$currency = $tblCurrency->fetchRow($select);
        }
        
        if (($singularRoom != null) and $singularRoom->count() > 0) {
            $singularRoom = $singularRoom->current();
            $ListValue=Hm_Usr_UserFb::ObtenerDatosUsuario($singularRoom->Uid);
            $urlProfile = $urlFacebook . 'roommates/perfiltengo?valroom=' . $id;
            
            if($singularRoom->IncluyeServicio == "S"){
                $servicesInRoom = $translator->getAdapter()->translate("INFO_ROOM_INCLUDED_SERVICES");
            }else{
            	if(isset($currency)){
                	$servicesInRoom = $translator->getAdapter()->translate("ADD_ROOM_BASIC_INFO_SERVICE"). " " . $currency->SimboloMoneda  . number_format($singularRoom->MontoServicios, 2) . " " . $currency->CodigoEstandar;
            	}else{
            		$servicesInRoom = $translator->getAdapter()->translate("ADD_ROOM_BASIC_INFO_SERVICE"). " " . number_format($singularRoom->MontoServicios, 2);
            	}
            	
            }
            $mainPhoto = $this->cfg['local_app_url'] . 'imagenrmt.php?id=' . $photo->RMTFotoID;
            $xml->addChild('title', $ListValue[0]['name'] . " " . $translator->getAdapter()->translate("MSG_ROOM_SHARE_TITLE"));
            $xml->addChild('href', $urlProfile);
            $xml->addChild('caption', $singularRoom->Titulo);
            if(isset($currency)){
            	$xml->addChild('description', $translator->getAdapter()->translate("LABEL_FOR") . " " . $translator->getAdapter()->translate("ADD_ROOM_BASIC_INFO_RENT"). " " . $currency->SimboloMoneda  . number_format($singularRoom->MontoRenta, 2) . " " . $currency->CodigoEstandar .", " . $servicesInRoom);
            }
            $xml->addChild('image', $mainPhoto);
            $xml->addChild('conditions', $translator->getAdapter()->translate("LABEL_ROOM_STAY_MONTHS"). ": " . $singularRoom->EstanciaMinima ." | "  .  $translator->getAdapter()->translate("ADD_ROOM_AVAILABLE_DATE") . ": " . $singularRoom->FechaDisponible);
            $xml->addChild('location', $translator->getAdapter()->translate("ADD_ROOM_ENTER_LOCATION") . ": " . $singularRoom->Direccion);
            $xml->addChild('correo', $singularRoom->Correo);

        }
        if(isset($status['redirect'])){
        	$xml->addChild('redirect', $status['redirect']);
        }
        header('Content-type: text/xml');
        echo $xml->asXML();
        
        //Envio de correo cada vez que se publica el anuncio
        if(($singularRoom != null) && ($publicado=="S")){
            $html = new Zend_View();
            $html->setScriptPath(APPLICATION_PATH . '/views/scripts/emails/');

            $html->urlProfile = $urlProfile;
            $body = $html->render('confirmacion.phtml');

            $textbody = preg_replace("'<style[^>]*>.*</style>'siU", '', $body);
            $textbody = trim(strip_tags($textbody));
            //Configuracion del servidor smtp
            $smtpServer = $this->cfg['gmail_smtpserver'];
            $username = $this->cfg['gmail_username'];
            $password = $this->cfg['gmail_password'];

            $config = array('ssl' => 'tls',
                'port' => 587,
                'auth' => 'login',
                'username' => $username,
                'password' => $password);
            
            try {
                $transport = new Zend_Mail_Transport_Smtp($smtpServer, $config);

                $mail = new Zend_Mail('UTF-8');
                $mail->setFrom($username, "Housemarket Notification");
                $mail->setReplyTo($username, "No-Reply");
                $mail->setReturnPath($username, "No-Reply");
                $mail->addTo($singularRoom->Correo, $ListValue[0]['name']);
                $mail->setSubject('You have published an ad on Housemarket');
                $mail->setBodyHtml($body);
                $mail->setBodyText($textbody);

                $status = $mail->send($transport);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        }
//        $this->_helper->viewRenderer->setNoRender();
        exit();
    }
    
    public function saveroommateAction() {
        // Reiniciando los parametros de flashmessenger
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashmessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
        $status = array();
        $status['status'] = "error";

        $urlFacebook = $this->cfg['fb_app_url'];
        $translator = Zend_Registry::get('Zend_Translate');

        $f = new Zend_Filter_StripTags();
        $rsRoommate = null;
        
        if( $this->getRequest()->isPost()) {
            try {
                $roommate = new Hm_Rmt_Roommate($_POST);
                $publicado = $f->filter($this->_request->getParam("publicado"), "N");
                $new = $f->filter($this->_request->getParam("isNew"), "S");
                $isNew = $new=="S"?true:false;

                if(!$isNew) {
                    $id = $this->_request->getParam("codigoRoommate");
                    try {
                        $rsRoommate = $roommate->find($id);
                        if($rsRoommate->count() > 0) {
                        	$val = $rsRoommate->getRow(0)->toArray();
                        	$roommate = new Hm_Rmt_Roommate($val);
                            $publicar = ($publicado=="S" && $roommate->EstadoRoommate == self::PUBLISHED_RMT_STATE)?true:false;
                        } else {
                            $publicar = false;
                        }
                    } catch(Exception $ex) {
                        $publicar = ($publicado == "S") ? true : false;
                    }

                } else {
                    $publicar = ($publicado == "S") ? true : false;
                }
                $result = $roommate->SaveOrUpdate($this->_request->getParams());

                // Mensajes de validacion
                if(array_key_exists("fail", $result)) {
                    $messages = $result["reasons"];
                    $messageError = "";
                    if(is_array($messages)) {
                        $messageError .= "<ul>";
                        foreach($messages as $field=>$messageBlock) {
                            if(is_array($messageBlock)) {
                                foreach ($messageBlock as $message) {
                                    $messageError .= "<li>" . $message . "</li>";
                                }
                            } else {
                                $messageError .= "<li>" . $messageBlock . "</li>";
                            }
                        }
                        $messageError .= "</ul>";
                    } else { // Mensaje sencillo.
                        $messageError .= "<ul><li>" . $messages ."</li></ul>";
                    }
                    $this->_flashMessenger->addError("<i18n>Please check your data</i18n>" . $messageError);
                } else {
                    // Mensajes de advertencia al guardar el archivo.
                    // Revisar el campo de status y revisar si hay algun mensaje.
                    $isOk = true;
                    $warning = null;

                    $state = $result["status"];
                    $id = $result["id"];
                    if($isOk) {
                        $this->_flashMessenger->addSuccess($translator->getAdapter()->translate("ADD_ROOMMATE_SUCCESS_SAVE"));
                        $publicada = ($publicar)? '&publicada=1' : '&publicada=0';

                        $rsRoommate = $roommate->find($id);
                        $status['publicar'] = $publicar;
                        $status['status'] = "success";
                        $status['redirect'] = $urlFacebook . "roommates/busquedacuarto/";

                    } else {
                        $this->_flashMessenger->addWarning("Warning" . $warning);
                        $status['status'] = "warning";

                        if($isNew) {
                            $status['redirect'] = ($urlFacebook . 'roommates/agrbusco?id=' . $id);
                        }
                    }
                }
            }catch(Exception $ex) {
                $this->_flashMessenger->addError('Ocurrio un error al guardar los datos de su propiedad'. $ex->getMessage());
            }
        }

        if(isset($roommate)){
	        $tblCurrency = new Hm_Cat_Moneda();
	        $select = $tblCurrency->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
	        $select->setIntegrityCheck(false)
	        ->join("catpais","catpais.CodigoMoneda = catmoneda.CodigoMoneda")
	        ->where($tblCurrency->getAdapter()->quoteInto("catpais.ZipCode = ?", $roommate->ZipCode));
	        $currency = $tblCurrency->fetchRow($select);
        }
        $status['message'] = $this->view->flashMessenger();
        $xml = simplexml_load_string("<response />");
        $xml->addChild('status', $status['status']);
        $xml->addChild('message', $status['message']);
        if ($rsRoommate != null and $rsRoommate->count() > 0) {
            $singularRoommate = $rsRoommate->current();
            $texto = $translator->getAdapter()->translate("MSG_RMT_SHARE_TITLE");
            $ListValue=Hm_Usr_UserFb::ObtenerDatosUsuario($singularRoommate->Uid);
            
            $xml->addChild('title', $ListValue[0]['name'] . " " . $texto );
            $xml->addChild('href', $urlFacebook . 'roommates/perfilbusco?valroommate=' . $id);
            if(isset($currency)){
            	$xml->addChild('caption', $singularRoommate->Titulo . " - " .  $translator->getAdapter()->translate("LABEL_RMT_GENERAL_INFO_RENT"). " " . $currency->SimboloMoneda  . number_format($singularRoommate->MontoRenta, 2) . " " . $currency->CodigoEstandar);
            }
            $xml->addChild('description',  $translator->getAdapter()->translate("LABEL_ROOM_STAY_MONTHS") . ": " . $singularRoommate->EstanciaMinima ." | "  .  $translator->getAdapter()->translate("LABEL_RMT_DATE_CHANGE") . ": " . $singularRoommate->FechaMudanza);
            $xml->addChild('image', $this->cfg['local_app_url']."housemarketlogo.jpg");
            $xml->addChild('location', $translator->getAdapter()->translate("LABEL_SEARCH_IN_MAP") . ": " . $singularRoommate->Direccion);
            $xml->addChild('correo', $singularRoommate->Correo);
        }
        if(isset($status['redirect'])){
        	$xml->addChild('redirect', $status['redirect']);
        }
        header('Content-type: text/xml');
        echo $xml->asXML();
        
        //Envio de correo cada vez que un anuncio es publicado
        if(($singularRoommate != null) && ($publicado=="S")){
            
                $html = new Zend_View();
                $html->setScriptPath(APPLICATION_PATH . '/views/scripts/emails/');
                
                
                $html->urlProfile = $urlFacebook . 'roommates/perfilbusco?valroommate=' . $id;
                $body = $html->render('confirmacion.phtml');
                $textbody = preg_replace("'<style[^>]*>.*</style>'siU", '', $body);
                $textbody = trim(strip_tags($textbody));
                //Configuracion del servidor smtp
                $smtpServer = $this->cfg['gmail_smtpserver'];
                $username = $this->cfg['gmail_username'];
                $password = $this->cfg['gmail_password'];

                $configuration = array(
                    'ssl' => 'tls',
                    'port' => 587,
                    'auth' => 'login',
                    'username' => $username,
                    'password' => $password);
                
            try { 
                $transport = new Zend_Mail_Transport_Smtp($smtpServer, $configuration);

                $mail = new Zend_Mail('UTF-8');
                $mail->setFrom($username, "Housemarket Notification");
                $mail->setReplyTo($username, "No-Reply");
                $mail->setReturnPath($username, "No-Reply");
                $mail->addTo($singularRoommate->Correo, $ListValue[0]['name']);
                $mail->setSubject('You have published an ad on Housemarket');
                $mail->setBodyHtml($body);
                $mail->setBodyText($textbody);
                $status = $mail->send($transport);
                
           } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        }
         exit();
    }

    public function deleteroomAction(){
        $urlFacebook = $this->cfg['fb_app_url'];
        $this->_flashMessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
        
        $object = new stdClass();
        $this->_request->getParam("id");
        $f = new Zend_Filter_StripTags();
        
        if( $this->getRequest()->isPost()) {
        	$translator = Zend_Registry::get('Zend_Translate');
            $roomId = $this->_request->getParam("id");
            $room = new Hm_Rmt_Cuarto();
            $result = $room->DeleteRoom($roomId);
            if(array_key_exists("fail", $result)) {
                $messages = $result["reasons"];
                $messageError = "";
                if(is_array($messages)) {
                    $messageError .= "<ul>";
                    foreach($messages as $field=>$messageBlock) {
                        if(is_array($messageBlock)) {
                            foreach ($messageBlock as $message) {
                                $messageError .= "<li>" . $message . "</li>";
                            }
                        } else {
                            $messageError .= "<li>" . $messageBlock . "</li>";
                        }
                    }
                    $messageError .= "</ul>";
                } else { 
                    $messageError .= "<ul><li>" . $messages ."</li></ul>";
                }
                $this->_flashMessenger->addError($messageError);
                $object->message = $this->view->flashMessenger();
                echo '{"success": false, "objeto":' . Zend_Json::encode($object) . '}';
            }else{
                $object->redirect = $urlFacebook . "roommates/busquedacuarto/";
                $object->message = "<div class='success'><ul><li>" .$translator->getAdapter()->translate("DEL_ROOM") . "</li></ul></div>";
                echo '{"success":true, "objeto":' . Zend_Json::encode($object) . '}';
            }
        }
        exit();
    }

    public function deleteroommateAction(){
        $urlFacebook = $this->cfg['fb_app_url'];
        $this->_flashMessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
        
        $object = new stdClass();
        $this->_request->getParam("id");
        $f = new Zend_Filter_StripTags();
        
        if( $this->getRequest()->isPost()) {
        	$translator = Zend_Registry::get('Zend_Translate');
            $roommateId = $this->_request->getParam("id");
            $roommate = new Hm_Rmt_Roommate();
            $result = $roommate->DeleteRoommate($roommateId);
            if(array_key_exists("fail", $result)) {
                $messages = $result["reasons"];
                $messageError = "";
                if(is_array($messages)) {
                    $messageError .= "<ul>";
                    foreach($messages as $field=>$messageBlock) {
                        if(is_array($messageBlock)) {
                            foreach ($messageBlock as $message) {
                                $messageError .= "<li>" . $message . "</li>";
                            }
                        } else {
                            $messageError .= "<li>" . $messageBlock . "</li>";
                        }
                    }
                    $messageError .= "</ul>";
                } else { 
                    $messageError .= "<ul><li>" . $messages ."</li></ul>";
                }
                $this->_flashMessenger->addError($messageError);
                $object->message = $this->view->flashMessenger();
                echo '{"success": false, "objeto":' . Zend_Json::encode($object) . '}';
            }else{
                $object->redirect = $urlFacebook . "roommates/busquedaroommate/";
                $object->message = "<div class='success'><ul><li>" .$translator->getAdapter()->translate("DEL_ROOMMATE") . "</li></ul></div>";
                echo '{"success":true, "objeto":' . Zend_Json::encode($object) . '}';
            }
        }
        exit();
    }
    
    /**
     * Retorna un array de numeros consecutivos acotados por dos limites incluyentes
     * @param Integer $limit_inf
     * @param Integer $limit_sup
     * @return Array
     */
    public static function GetNumberList($limits){
        $translator = Zend_Registry::get('Zend_Translate');
        $numbers = array();
        $numbers[""] = $translator->getAdapter()->translate('LABEL_GENERAL_SELECT');
        for($i= $limits[0]; $i<= $limits[1]; $i++){
            $numbers[$i] = $i;
        }
        return $numbers;
    }

    /**
     * Retorna el total de fotos permitidas, despu�s de las que tiene ya guardadas en la BBDD
     * @param Bool $isNew Determina si el cuarto es nuevo
     * @param Integer $roomId Identificador del cuarto
     * @return Integer Cantidad de fotos permitidas
     */
     private function GetAllowedTotalPhotos($isNew, $roomId = null) {

        $maxAllowedPhotos = 4; // Recuperar de acuerdo al maximo de photos permitidas
        if($isNew) {

            return $maxAllowedPhotos; // Es un nuevo registro.
        } else {
            // Recuperar el total de photos.
            $room = new Hm_Rmt_Cuarto();
            $fotos = $room->GetPhotos($roomId);
            if(count($fotos) > 0 ) {
                $totalPhotos = count($fotos);
            } else {
                $totalPhotos = 0;
            }
            return $maxAllowedPhotos - $totalPhotos;
        }
    }

     public function perfiltengoAction() {
     	$this->currentActionNav = 'perfiltengo';
     	$this->navAction();
         
        $this->view->appusers = $this->fbDgt->getFriendsApp();
        $this->view->showResponde = false;

        $this->view->loadmap = 'onunload="GUnload();"';
        $google_api_key = $this->cfg['google_api_key'];
        $this->view->headScript()->appendFile("https://maps.google.com/maps?file=api&v=2&sensor=false&key=$google_api_key",'text/javascript');
        $this->view->headScript()->appendFile("https://www.google.com/uds/api?file=uds.js&v=1.0&source=uds-msw&key=ABQIAAAAvCeUucOYY74-kYy_PW657BTXiVpCZbJoaksdLXfYkWMngqmDrxS6-TiTbctgJsKVCf909V4KNSXSaA");
        $this->view->headScript()->appendFile("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.js?mode=new");

        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
        $this->view->headLink()->appendStylesheet("https://www.google.com/uds/css/gsearch.css");
        $this->view->headLink()->appendStylesheet("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.css");
        $this->view->headLink()->appendStylesheet("/css/custom-theme/jquery-ui-1.7.2.custom.css");
        $this->view->headLink()->appendStylesheet("/css/style_form.css");
        $this->view->headLink()->appendStylesheet("/css/list_autor_properties.css");
        $this->view->headLink()->appendStylesheet("/css/errores.css");
        $this->view->headLink()->appendStylesheet('/css/style_perfil.css');
        $this->view->headLink()->appendStylesheet('/css/colorbox/colorbox.css');
        $this->view->headLink()->appendStylesheet('/js/galleriffic/css/galleriffic.css');
        $this->view->headLink()->appendStylesheet('/css/messages.css');
        $this->view->headLink()->appendStylesheet('/css/static.ak.fbcdn.net.I0YvHLrgaAw.css');
        $this->view->headLink()->appendStylesheet('/css/messages.css');

        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all-debug.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery-1.3.2.min.js');
        $this->view->headScript()->appendFile('/js/jquery.colorbox-min.js?v=1.3.17.2', 'text/javascript');
        $this->view->headScript()->appendFile("/js/galleriffic/js/jquery.galleriffic.js");
        $this->view->headScript()->appendFile("/js/galleriffic/js/jquery.opacityrollover.js");
        $this->view->headScript()->appendFile("/js/mapa_en_perfiltengo.js");
        $this->view->headScript()->appendFile("/js/perfil.js?v=16");

        $language = Zend_Registry::get('language');

        if(!empty ($_GET['valroom'])) {
            $idRoom=$_GET['valroom'];
            if(!is_numeric($idRoom)) {
                $this->_redirector->gotoUrl($urlhome);
            }

            $room = new Hm_Rmt_Cuarto();
            
            $rs = $room->find($idRoom);
            $uid = Zend_Registry::get('uid');
            
             if($rs == null || $rs->count() <= 0) {
                $this->_redirector->gotoUrl($urlhome);
            }

            $this->view->fb_path = $this->cfg['fb_app_url']. "/";
            $this->view->urlLocal = $this->cfg['local_app_url'];

            $curRoom = $rs->current();
            $this->view->room = $curRoom;
            $this->view->IdRoom=$idRoom;
            
            //Moneda
            $tblPais = new Hm_Cat_Pais();
            $tblCurrency = new Hm_Cat_Moneda();
            $select = $tblCurrency->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
            $select->setIntegrityCheck(false)
                    ->join("catpais","catpais.CodigoMoneda = catmoneda.CodigoMoneda")
                    ->where($tblCurrency->getAdapter()->quoteInto("catpais.ZipCode = ?", $curRoom->ZipCode));
            $this->view->currency = $tblCurrency->fetchRow($select);

            Zend_Layout::getMvcInstance()->assign(array("showRoommates"=> "school", "nombre" => "high"));

            $roomValueByLanguage = array(
                'TipoAlojamiento'  => $curRoom->TipoAlojamiento,
                'TipoPropiedad'    => $curRoom->TipoPropiedad,
                'TipoCuarto'       => $curRoom->TipoCuarto,
                'TipoBano'         => $curRoom->TipoBano,
                'FumadorHabitan'   => $curRoom->FumadorHabitan,
                'OcupacionHabitan' => $curRoom->OcupacionHabitan,
                'GeneroHabitan'    => $curRoom->GeneroHabitan,
                'MascotaHabitan'   => $curRoom->MascotaHabitan,
                'OrientacionHabitan'=> $curRoom->OrientacionHabitan,
                'HijosHabitan'      => $curRoom->HijosHabitan,
                'GeneroAcepta'      => $curRoom->GeneroAcepta,
                'ParejaAcepta'      => $curRoom->ParejaAcepta,
                'NinosAcepta'       => $curRoom->NinosAcepta,
                'MascotaAcepta'     => $curRoom->MascotaAcepta,
                'FumadorAcepta'     => $curRoom->FumadorAcepta,
                'OrientacionAcepta' => $curRoom->OrientacionAcepta,
                'OcupacionAcepta'   => $curRoom->OcupacionAcepta,
            );
			
            $rmtCuardoObj = new Hm_Rmt_Cuarto();
            $fieldRoomTable = $rmtCuardoObj->ObtenerDatosCuarto($roomValueByLanguage);

            foreach ($roomValueByLanguage as $index => $value) {
                if(array_key_exists($value, $fieldRoomTable)){
                    $roomValueByLanguage[$index] = $fieldRoomTable[$value];
                }
            }

            $benefit = new Hm_Cat_BeneficioCuarto();
            $rsBenefits = $benefit->fetchAll(
                           $benefit->select()
                            ->where('CodigoCuarto = ?', $curRoom->CodigoCuarto));

            if($rsBenefits->count() > 0){
                foreach ($rsBenefits as $benefit) {
                    $benefits[] = $benefit->BeneficioCuarto;
                }
                $rmtCuardoObj = new Hm_Rmt_Cuarto();
                $benefitsByLanguage = $rmtCuardoObj->ObtenerDatosCuarto($benefits);
                $labelBenefits = implode(", ", $benefitsByLanguage);
            }

            //Valores extraidos de la bbdd según el idioma
            $this->view->tipoAlojamiento = $roomValueByLanguage["TipoAlojamiento"];

            $genRoomInfo = array("FechaDisponible", "EstanciaMinima", "TipoPropiedad",
                                   "TipoCuarto", "TipoBano");

            $occupiersRoom = array("MaxCapacidad", "FumadorHabitan", "IdiomaHabitan", "OcupacionHabitan",
                                    "GeneroHabitan", "MascotaHabitan", "HijosHabitan");

            $roommateRoom = array("GeneroAcepta", "ParejaAcepta", "NinosAcepta", "MascotaAcepta", "FumadorAcepta",
                                    "OcupacionAcepta");

            $caractRoom = $this->InformationFields($genRoomInfo, $curRoom, $roomValueByLanguage, self::$_TITLE_BY_LABEL);
            $caractRoom["CortoPlazo"]["title"] = self::$_TITLE_BY_LABEL["CortoPlazo"];
            $caractRoom["CortoPlazo"]["value"] = $curRoom->CortoPlazo == "S"? "<i18n>LABEL_YES</i18n>" : "<i18n>LABEL_NO</i18n>";
            
            $caractOccupiers = $this->InformationFields($occupiersRoom, $curRoom, $roomValueByLanguage, self::$_TITLE_BY_LABEL);
            $caractOccupiers["RangoEdad"]["title"] = "<i18n>LABEL_AGE_RANGE</i18n>";
            $caractOccupiers["RangoEdad"]["value"] = $curRoom->EdadMinimaHabitan . " - " . $curRoom->EdadMaximaHabitan;
            $caractOccupiers["IdiomaHabitan"]["title"] = self::$_TITLE_BY_LABEL["IdiomaHabitan"];
            if($caractOccupiers["IdiomaHabitan"]["value"] == "" || $caractOccupiers["IdiomaHabitan"]["value"] == NULL){
                $caractOccupiers["IdiomaHabitan"]["value"] = " -";
            }else{
                $caractOccupiers["IdiomaHabitan"]["value"] = Hm_Cat_Idioma::getInstance()->GetLabelByLanguage($curRoom->IdiomaHabitan);
            }

            $caractRoommate = $this->InformationFields($roommateRoom, $curRoom, $roomValueByLanguage, self::$_TITLE_BY_LABEL);
            $caractRoommate["RangoEdad"]["title"] = "<i18n>LABEL_AGE_RANGE</i18n>";
            $caractRoommate["RangoEdad"]["value"] = $curRoom->EdadMinimaAcepta . " - " . $curRoom->EdadMaximaAcepta;

            if(trim($curRoom->Direccion) != "")
               $this->view->addressRoom =  $curRoom->Direccion;
            
            $this->view->uid=$curRoom->Uid;
            $this->view->showResponde = $this->fbDgt->hasPermission($this->cfg['facebook_perms']);
            $this->view->totalMutualFriends = $this->fbDgt->GetTotalMutualFriends($this->view->uid);
            $arrMutualFriends = $this->fbDgt->GetMutualFriends($this->view->uid);

            if($this->view->totalMutualFriends > 0) {
                foreach($arrMutualFriends as &$friend){
                    $friend['pic_square'] = (!empty ($friend['pic_square']))? $friend['pic_square'] : "/img/q_silhouette.gif";
                }
            }
            
            if($curRoom->IncluyeServicio == "S"){
                $this->view->servicesInRoom = '<i18n>INFO_ROOM_INCLUDED_SERVICES</i18n>';
            }else{
                $this->view->servicesInRoom = '<i18n>ADD_ROOM_BASIC_INFO_SERVICE</i18n>' . $currency->SimboloMoneda . number_format($curRoom->MontoServicios, 2) . ' <abbr title="' 
                . ($language == 'en' ? $this->view->currency->NombreEN : $this->view->currency->NombreMoneda) . '">'
                . $this->view->currency->CodigoEstandar . '</abbr>';
            }

            $this->view->mutualFriends = $arrMutualFriends;

            //si es el propietario presenta botones
            $verifUid=$this->fbDgt->get_uid();
            if($verifUid==$this->view->uid) {
                $url_en_fbc=$this->cfg['fb_app_url'];
                $editar=$this->view->fb_path . "roommates/agrtengo?id=".$curRoom->CodigoCuarto;
                $publicar=$this->view->fb_path . "roommates/agrtengo?id=".$curRoom->CodigoCuarto;
                $botonesprop="<div class='button_group'>
                        <a href='$editar' class='button' id='bt2' name='bt' target='_top'><i18n>LABEL_EDIT</i18n></a> ";

                $botonesprop.="</div>";
                $this->view->botonesprop=$botonesprop;
            }
            
            $ListValue=Hm_Usr_UserFb::ObtenerDatosUsuario($this->view->uid);
            $location='';
            $rutaimagen = $this->cfg['local_app_url'] ."img/q_silhouette.gif";
            $pic = $rutaimagen;
            
            $logged_in_user = Zend_Registry::get('user_info');
            $this->view->logged_in_user = $logged_in_user;
            
            if(is_array($ListValue)) {
                $this->view->username= $ListValue[0]['first_name'];
                $this->view->name = $ListValue[0]['name'];

                if($curRoom->TipoAlojamiento == 1)
                    $this->view->usernames = $ListValue[0]['name'];
                else
                    $this->view->usernames = "<i18n>ADD_ROOM_OCCUPIERS</i18n>";

                if(strlen($ListValue[0]['pic_big']) > 0) {
                    $pic = $ListValue[0]['pic_big'];
                }

                $location=$ListValue[0]['current_location'];
            }
            $this->view->profile=$pic;

            $objFoto = new Hm_Rmt_Foto();
            $photosRS = $objFoto->fetchAll(
                    $objFoto->select()
                    ->from(array('f' => 'RMTFoto'),
                    array('RMTFotoID', 'Principal', 'Comentario'))
                    ->where('CodigoCuarto = ?', $curRoom->CodigoCuarto)
                    ->order('Principal DESC')
            );

            if($photosRS != null && $photosRS->count() > 0) {
                $this->view->photos = $photosRS;
            }


            $generalInformationRoom = '';

            foreach ($caractRoom as $val) {
                $generalInformationRoom .= "<tr><td>".$val["title"].'</td><td class="value_details">'.$val["value"]."</td></tr> ";
            }

            if($rsBenefits->count() > 0){
                $generalInformationRoom  = $generalInformationRoom . '<tr><td><i18n>LABEL_ROOM_BENEFITS</i18n></td><td class="value_details">'.$labelBenefits.'<td></tr>';
            }

            $this->view->caracteristicas = '<table>' . $generalInformationRoom . '</table>';

            $occupiers = '';
            $i = 1;
            foreach($caractOccupiers as $val){
                $val["value"] = ($val["value"] == "No mostrar")? "No" : $val["value"];
                    $occupiers .= "<tr><td><b>".$val["title"].'</b></td><td>'.$val["value"]."</td></tr>";
                $i++;
            }

            $this->view->occupiers = $occupiers ;

            $roommates = '';
            foreach($caractRoommate as $val){   
                    $roommates .= "<tr><td>".$val["title"].'</td><td class="value_details">'.$val["value"]."</td></tr> ";
            }

            $this->view->roommates ='<table>' . $roommates . '</table>';
        }
    }

    public function perfilbuscoAction() {
    	
		$this->currentActionNav = 'perfilbusco';
     	$this->navAction();

        $this->view->appusers = $this->fbDgt->getFriendsApp();
        $this->view->showResponde = false;

        $this->view->loadmap = 'onunload="GUnload();"';
        $google_api_key = $this->cfg['google_api_key'];
        $this->view->headScript()->appendFile("https://maps.google.com/maps?file=api&v=2&sensor=false&key=$google_api_key",'text/javascript');
        $this->view->headScript()->appendFile("https://www.google.com/uds/api?file=uds.js&v=1.0&source=uds-msw&key=ABQIAAAAvCeUucOYY74-kYy_PW657BTXiVpCZbJoaksdLXfYkWMngqmDrxS6-TiTbctgJsKVCf909V4KNSXSaA");
        $this->view->headScript()->appendFile("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.js?mode=new");

        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
        $this->view->headLink()->appendStylesheet("https://www.google.com/uds/css/gsearch.css");
        $this->view->headLink()->appendStylesheet("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.css");
        $this->view->headLink()->appendStylesheet("/css/custom-theme/jquery-ui-1.7.2.custom.css");
        $this->view->headLink()->appendStylesheet("/css/style_form.css");
        $this->view->headLink()->appendStylesheet("/css/list_autor_properties.css");
        $this->view->headLink()->appendStylesheet("/css/errores.css");
        $this->view->headLink()->appendStylesheet('/css/colorbox/colorbox.css');
        $this->view->headLink()->appendStylesheet('/css/messages.css');
        $this->view->headLink()->appendStylesheet('/css/static.ak.fbcdn.net.I0YvHLrgaAw.css');
        $this->view->headLink()->appendStylesheet('/css/style_perfil.css');
        $this->view->headLink()->appendStylesheet('/js/galleriffic/css/galleriffic.css');
        $this->view->headLink()->appendStylesheet('/css/messages.css');

        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery-1.3.2.min.js');
        $this->view->headScript()->appendFile('/js/jquery.colorbox-min.js?v=1.3.17.2', 'text/javascript');
        $this->view->headScript()->appendFile("/js/galleriffic/js/jquery.galleriffic.js");
        $this->view->headScript()->appendFile("/js/galleriffic/js/jquery.opacityrollover.js");
        $this->view->headScript()->appendFile("/js/mapa_en_perfil_busco.js");
        $this->view->headScript()->appendFile("/js/perfil.js?v=16");

        $language = Zend_Registry::get('language');

        $urlhome = "index?". $_SERVER['QUERY_STRING'];
        
        if(!empty ($_GET['valroommate'])) {
            $idRoommate=$_GET['valroommate'];
            if(!is_numeric($idRoommate)) {
                $this->_redirector->gotoUrl($urlhome);
            }

            $roommate = new Hm_Rmt_Roommate();

            $rs = $roommate->find($idRoommate);
            $uid = Zend_Registry::get('uid');

             if($rs == null || $rs->count() <= 0) {
                $this->_redirector->gotoUrl($urlhome);
            }

            $this->view->fb_path = $this->cfg['fb_app_url'];
            $this->view->urlLocal = $this->cfg['local_app_url'];
            $curRoommate = $rs->current();
            $this->view->IdRoommate=$idRoommate;
            $this->view->roommate = $curRoommate;
            
            $tblPais = new Hm_Cat_Pais();
            $tblCurrency = new Hm_Cat_Moneda();
            $select = $tblCurrency->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
            $select->setIntegrityCheck(false)
                    ->join("catpais","catpais.CodigoMoneda = catmoneda.CodigoMoneda")
                    ->where($tblCurrency->getAdapter()->quoteInto("catpais.ZipCode = ?", $curRoommate->ZipCode));
            $this->view->currency = $tblCurrency->fetchRow($select);
            

            $roomValueByLanguage = array(
                'TipoBusqueda'      => $curRoommate->TipoBusqueda,
                'TipoPropiedad'     => $curRoommate->TipoPropiedad,
                'Amueblado'         => $curRoommate->Amueblado,
                'TipoCuarto'        => $curRoommate->TipoCuarto,
                'TipoBano'          => $curRoommate->TipoBano,
                'FumadorBusca'      => $curRoommate->FumadorBusca,
                'OcupacionBusca'    => $curRoommate->OcupacionBusca,
                'GeneroBusca'       => $curRoommate->GeneroBusca,
                'MascotaBusca'      => $curRoommate->MascotaBusca,
                'HijosBusco'        => $curRoommate->HijosBusco,
                'GeneroAcepta'      => $curRoommate->GeneroAcepta,
                'ParejaAcepta'      => $curRoommate->ParejaAcepta,
                'NinosAcepta'       => $curRoommate->NinosAcepta,
                'MascotaAcepta'     => $curRoommate->MascotaAcepta,
                'FumadorAcepta'     => $curRoommate->FumadorAcepta,
                'OcupacionAcepta'   => $curRoommate->OcupacionAcepta,
            );

            $rmtCuardoObj = new Hm_Rmt_Cuarto();
            $fieldRoomTable = $rmtCuardoObj->ObtenerDatosCuarto($roomValueByLanguage);
            foreach ($roomValueByLanguage as $index => $value) {
            	if(array_key_exists($value, $fieldRoomTable)){
            		$roomValueByLanguage[$index] = $fieldRoomTable[$value];
                }
            }

            //Valores extraidos de la bbdd seg�n el idioma
            $this->view->tipoBusqueda = $roomValueByLanguage["TipoBusqueda"];

            $genRoomInfo = array("FechaMudanza", "EstanciaMinima", "TipoPropiedad",
                                   "Amueblado", "TipoCuarto", "TipoBano");

            $roommateRoom = array("GeneroAcepta", "ParejaAcepta", "NinosAcepta", "MascotaAcepta", "FumadorAcepta",
                                    "OcupacionAcepta");

            $personalInformation = array("FumadorBusca", "IdiomaBusca", "OcupacionBusca",
                                    "MascotaBusca", "HijosBusco");

            $caractRoom = $this->InformationFields($genRoomInfo, $curRoommate, $roomValueByLanguage, self::$_TITLE_BY_LABEL);

            $myInfo = $this->InformationFields($personalInformation, $curRoommate, $roomValueByLanguage, self::$_TITLE_BY_LABEL);
            $myInfo["RangoEdad"]["title"] = ($curRoommate->EdadMaximaBusca > 0)? "<i18n>LABEL_AGE_RANGE</i18n>" :"<i18n>LABEL_AGE</i18n>";
            $myInfo["RangoEdad"]["value"] = ($curRoommate->EdadMaximaBusca > 0)? $curRoommate->EdadMinimaBusca. " - " . $curRoommate->EdadMaximaAcepta : $curRoommate->EdadMinimaBusca;
            $myInfo["IdiomaBusca"]["title"] = self::$_TITLE_BY_LABEL["IdiomaBusca"];
            if($curRoommate->CantBusca > 0){
                $myInfo["Personas_Buscan"]["title"] = "<i18n>LABEL_RMT_CANTITY_SEARCH</i18n>";
                $myInfo["Personas_Buscan"]["value"] = $curRoommate->CantBusca;
            }

            if($myInfo["IdiomaBusca"]["value"] == "" || $myInfo["IdiomaBusca"]["value"] == NULL){
                $myInfo["IdiomaBusca"]["value"] = " -";
            }else{
                $myInfo["IdiomaBusca"]["value"] = Hm_Cat_Idioma::getInstance()->GetLabelByLanguage($curRoommate->IdiomaBusca);
            }

            $caractRoommate = $this->InformationFields($roommateRoom, $curRoommate, $roomValueByLanguage, self::$_TITLE_BY_LABEL);
            $caractRoommate["RangoEdad"]["title"] = "<i18n>LABEL_AGE_RANGE</i18n>";
            $caractRoommate["RangoEdad"]["value"] = $curRoommate->EdadMinimaAcepta . " - " . $curRoommate->EdadMaximaAcepta;


            if(trim($curRoommate->Direccion) != "")
               $this->view->addressRoommate =  $curRoommate->Direccion;

            $this->view->uid=$curRoommate->Uid;

            //si es el propietario presenta botones
            $verifUid=$this->fbDgt->get_uid();
            if($verifUid == $this->view->uid) {
                $url_en_fbc=$this->cfg['fb_app_url'];
                $editar=$this->view->fb_path . "roommates/agrbusco?id=".$curRoommate->CodigoRoommate;
                $botonesprop="<div class='button_group'>
                        <a href='$editar' class='button' id='bt2' name='bt' target='_top'><i18n>LABEL_EDIT</i18n></a> ";

                $botonesprop.="</div>";
                $this->view->botonesprop=$botonesprop;
            }

            $ListValue = Hm_Usr_UserFb::ObtenerDatosUsuario($this->view->uid);

            //Usuario actual, ver implementación en index.php
            $logged_in_user = Zend_Registry::get('user_info');
            $this->view->logged_in_user = $logged_in_user;
            
            $location='';
            $rutaimagen = $this->cfg['local_app_url'] ."img/q_silhouette.gif";
            $pic = $rutaimagen;
            if(is_array($ListValue)) {
                $this->view->username=$ListValue[0]['first_name'];
                $this->view->usernames = $ListValue[0]['name'];
                if(strlen($ListValue[0]['pic_big']) > 0) {
                    $pic = $ListValue[0]['pic_big'];
                }

                $location=$ListValue[0]['current_location'];
            }
            $this->view->profile=$pic;

            $generalInformationRoom = '';

            foreach ($caractRoom as $val) {
                $generalInformationRoom .= "<tr><td>".$val["title"].'</td><td  class="value_details">'.$val["value"]."</td></tr> ";
            }

            $this->view->idealRoom = '<table>'. $generalInformationRoom . '</table>';

            $roommate = '';
            foreach($caractRoommate as $val){
                $roommate .= "<tr><td>".$val["title"].'</td><td class="value_details">'.$val["value"]."</td></tr>";
            }

            $this->view->idealRoommate = '<table>' . $roommate . '</table>';

            $info = '';
            $i = 1;
            foreach($myInfo as $val){
                $val["value"] = $val["value"] == "No mostrar"? "No" : $val["value"];
                    $info .= "<tr><td><b>".$val["title"].'</b></td><td>'.$val["value"]."</td></tr> ";
                $i++;
            }

            $this->view->personalInfo = $info;
        }
    }
    
    /**
     * Funcion para retornar los valores de los campos con su respectivo titulo y valor
     * @param Array $parameters Es el array para identificar los datos a tratar
     * @param Object $objRoom Objeto de tipo cuarto para recuperar los valores en la tabla
     * @param Array $valueByTable Valores traidos segun idioma
     * @param Array $titleByLabel Titulos traidos por etiquetas
     * @return Array Arreglo con los valores para titulo y valor correspondiente
     */
    public function InformationFields($parameters, $objRoom, $valueByTable = array(), $titleByLabel = array()){
        if(count($parameters) > 0){
            $result = array();
            foreach ($parameters as $key => $value) {
                if(count($valueByTable) > 0){
                    if(array_key_exists($value, $valueByTable)){
                        $result[$value] = array("title" => $titleByLabel[$value], "value" => $valueByTable[$value]);
                    }
                }
                
                if(isset ($objRoom->$value) && !array_key_exists($value, $valueByTable)){
                        $result[$value] = array("title" => $titleByLabel[$value], "value" => $objRoom->$value);
                }
            }
        }else{
            return NULL;
        }
        return $result;
    }

     

    public function roommateenmapaAction() {
        $f = new Zend_Filter_StripTags();
        $roommate = new Hm_Rmt_Roommate();
        $roommateId = $f->filter($this->getRequest()->getParam("valroommate", null)) ;
        $rs = $roommate->find($roommateId);
        $curRoommate = $rs->current();
        $locationProperty = Array ('cc' =>$curRoommate->ZipCode , 'city' => $curRoommate->Ciudad, 'zip' =>'' ,'lat' =>$curRoommate->Latitud , 'lng' => $curRoommate->Longitud );
        $locationProperty =(object)$locationProperty;
        $locationProperty = array($locationProperty);

        if(!empty ($locationProperty)) {
            echo '{"success":"true","data": '. Zend_Json::encode($locationProperty) .'}';
        } else {
            $ip = $this->_request->getClientIP();
            $location = Dgt_GeoData::getLocation($ip);
            if (!empty($location)) {
                echo '{"success":"true","data": '. Zend_Json::encode($location) .'}';
            } else {
                echo '{"success":"false"}';
            }
            exit();
        }
        exit();
    }

      public function centermapAction() {
        $ip = $this->_request->getClientIP();
        $location = Dgt_GeoData::getLocation($ip);

        if (!empty($location)) {
            echo '{"success":"true","data": '. Zend_Json::encode($location) .'}';
        } else {
            echo '{"success":"false"}';
        }
        exit();
    }   
}
?>
