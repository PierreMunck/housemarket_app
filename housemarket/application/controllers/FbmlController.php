<?php

/**
 * Servidor de contenido de fbml
 *
 * @author Roberto Cordero
 */
class FbmlController extends MainController {

    /**
     * Mensajes al usuario
     * @var
     */
    protected $_flashMessenger = null;
    /**
     * Redirector de Zend
     * @var
     */
    protected $_redirector = null;

    /**
     * Inicializa las variables del controller
     */
    public function init() {
    	parent::init();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
    }

    /**
     * Antes de procesar el action
     */
    public function preDispatch() {
        $this->_helper->layout()->disableLayout();
    }

    public function responderequestAction() {
        try {
            //echo ("test complicated");
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessi�n.");
            die;
        }

        echo $test = "This is a test by neon...Responde button";
        //$this->view->testeo = $test;
        exit();

        // Interfaz para recuperar los datos.
        $f = new Zend_Filter_StripTags();

        // Recuperar el id de la propiedad a consultar.
        $estateId = $f->filter($this->getRequest()->getParam("id", null));

        // Recuperar el id del usuario en sesion.
        // Recuperar la propiedad.
        $estate = new Hm_Pro_Propiedad();
        $rs = $estate->find($estateId);
        if ($rs->count() > 0) {

            $curEstate = $rs->current();

            // Setear el usuario due�o de la propiedad
            $this->view->uidTo = $curEstate->Uid;

            // Setear el usuario que quiere consultar
            $this->view->uidFrom = $fb->get_uid();

            // Setear el titulo de la propiedad
            $this->view->subject = htmlentities($curEstate->NombrePropiedad);

            // Setear el codigo de la propiedad
            $this->view->codigoPropiedad = $estateId;
            //echo("Esta es la consulta para el responde");
        } else {
            // Ocurri� un error
            $this->view->error = "Sorry, we can not found this property in this moment";
        }
    }

    public function respondeRequestRoommateAction() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessi�n.");
            die;
        }

        // Interfaz para recuperar los datos.
        $f = new Zend_Filter_StripTags();

        // Recuperar el id de la propiedad a consultar.
        $roommateId = $f->filter($this->getRequest()->getParam("id", null));

        // Recuperar el id del usuario en sesion.
        // Recuperar la propiedad.
        $roommmate = new Hm_Rmt_Roommate();
        $rs = $roommmate->find($roommateId);
        if ($rs->count() > 0) {

            $curRoommmate = $rs->current();

            // Setear el usuario due�o de la propiedad
            $this->view->uidTo = $curRoommmate->Uid;

            // Setear el usuario que quiere consultar
            $this->view->uidFrom = $fb->get_uid();

            // Setear el titulo de la propiedad
            $this->view->subject = htmlentities($curRoommmate->Titulo);

            // Setear el codigo de la propiedad
            $this->view->codigoRoommate = $roommateId;
        } else {
            // Ocurri� un error
            $this->view->error = "Sorry, we can not found this property in this moment";
        }
    }

    public function respondeRequestRoomAction() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessi�n.");
            die;
        }

        // Interfaz para recuperar los datos.
        $f = new Zend_Filter_StripTags();

        // Recuperar el id de la propiedad a consultar.
        $roomId = $f->filter($this->getRequest()->getParam("id", null));

        // Recuperar el id del usuario en sesion.
        // Recuperar la propiedad.
        $room = new Hm_Rmt_Cuarto();
        $rs = $room->find($roomId);
        if ($rs->count() > 0) {

            $curRoom = $rs->current();

            // Setear el usuario due�o de la propiedad
            $this->view->uidTo = $curRoom->Uid;

            // Setear el usuario que quiere consultar
            $this->view->uidFrom = $fb->get_uid();

            // Setear el titulo de la propiedad
            $this->view->subject = htmlentities($curRoom->Titulo);

            // Setear el codigo de la propiedad
            $this->view->codigoCuarto = $roomId;
        } else {
            // Ocurri� un error
            $this->view->error = "Sorry, we can not found this property in this moment";
        }
    }

    public function respondeResponseAction() {

        // Recuperar el id del usuario conectado
        $uid = Zend_Registry::get('uid');

        // Consultar usuario de facebook
        $from_user = $this->fbDgt->execFql("SELECT uid, first_name, last_name FROM user WHERE uid IN (" . $uid . ") AND is_app_user");
        if ($from_user && is_array($from_user)) {
            $fromUserName = $from_user[0]["first_name"] . ", " . $from_user[0]["last_name"];
        }

        // Recuperar la informacion del usuario conectado.
        $f = new Zend_Filter_StripTags();
        $parametersKeys = array('codigoprop', 'txtFrom');
        $validData = array();
        foreach ($parametersKeys as $parameterName) {
            $validData[$parameterName] = $f->filter($this->getRequest()->getParam($parameterName, ''));
        }

        $filter = array(
            'codigoprop' => array('StripTags', 'Digits'),
            'txtFrom' => array('StripTags', 'StringTrim')
        );

        $validators = array(
            'codigoprop' => 'Digits',
            'txtFrom' => new Zend_Validate_NotEmpty()
        );

        $parameters = new Zend_Filter_Input($filter, $validators, $validData);
        $emailValidator = new Zend_Validate_EmailAddress();
        if ($parameters->isValid()) {

            $codeEstate = $parameters->codigoprop;
            $emailFrom = $parameters->txtFrom;
            $subject = mb_convert_encoding($this->getRequest()->getParam('subject', ''), "ISO-8859-1", "UTF-8");
            $message = mb_convert_encoding($this->getRequest()->getParam('message', ''), "ISO-8859-1", "UTF-8");

            // Recuperar la propiedad.
            $estate = new Hm_Pro_Propiedad();
            $rs = $estate->find($codeEstate);

            $curEstate = $rs->current();

            // Consultar usuario de facebook
            $to_user = $this->fbDgt->execFql("SELECT uid, first_name, last_name, email FROM user WHERE uid IN (" . $curEstate->Uid . ") AND is_app_user");

            if ($to_user && is_array($to_user)) {
                $toUserName = $to_user[0]["first_name"] . ", " . $to_user[0]["last_name"];
                $toEmail = $to_user[0]["email"];
            }

            try {
                $mail = new Zend_Mail();
                $mail->setBodyHtml($message);
                $mail->setFrom($emailFrom, $fromUserName);
                $mail->addTo($toEmail, $toUserName);
                $mail->setSubject($subject);
                $mail->send();
                $this->view->title = 'Success responde';
                $this->view->message = 'Your responde was sent to ' . $toUserName;
                $this->view->status = "status";
            } catch (Exception $ex) {
                $this->view->title = 'Error responde';
                $this->view->message = 'Your responde was not sent to ' . $toUserName . " because " . $ex->getMessage();
                $this->view->status = "error";
            }
        } else {
            $this->view->title = 'Error responde';
            $this->view->message = 'Your responde was not sent because some data is invalid ';
            $this->view->status = "error";
        }
    }

    public function respondeResponseRoommateAction() {

        // Recuperar el id del usuario conectado
        $uid = Zend_Registry::get('uid');

        // Consultar usuario de facebook
        $from_user = $this->fbDgt->execFql("SELECT uid, first_name, last_name FROM user WHERE uid IN (" . $uid . ") AND is_app_user");
        if ($from_user && is_array($from_user)) {
            $fromUserName = $from_user[0]["first_name"] . ", " . $from_user[0]["last_name"];
        }

        // Recuperar la informacion del usuario conectado.
        $f = new Zend_Filter_StripTags();
        $parametersKeys = array('codigoRoommate', 'txtFrom');
        $validData = array();
        foreach ($parametersKeys as $parameterName) {
            $validData[$parameterName] = $f->filter($this->getRequest()->getParam($parameterName, ''));
        }

        $filter = array(
            'codigoRoommate' => array('StripTags', 'Digits'),
            'txtFrom' => array('StripTags', 'StringTrim')
        );

        $validators = array(
            'codigoRoommate' => 'Digits',
            'txtFrom' => new Zend_Validate_NotEmpty()
        );

        $parameters = new Zend_Filter_Input($filter, $validators, $validData);
        $emailValidator = new Zend_Validate_EmailAddress();
        if ($parameters->isValid()) {

            $codeRoommate = $parameters->codigoRoommate;
            $emailFrom = $parameters->txtFrom;
            $subject = mb_convert_encoding($this->getRequest()->getParam('subject', ''), "ISO-8859-1", "UTF-8");
            $message = mb_convert_encoding($this->getRequest()->getParam('message', ''), "ISO-8859-1", "UTF-8");

            // Recuperar la propiedad.
            $roommate = new Hm_Rmt_Roommate();
            $rs = $roommate->find($codeRoommate);

            $curRoommate = $rs->current();

            // Consultar usuario de facebook
            $to_user = $this->fbDgt->execFql("SELECT uid, first_name, last_name, email FROM user WHERE uid IN (" . $curRoommate->Uid . ") AND is_app_user");

            if ($to_user && is_array($to_user)) {
                $toUserName = $to_user[0]["first_name"] . ", " . $to_user[0]["last_name"];
                $toEmail = $to_user[0]["email"];
            }

            try {
                $mail = new Zend_Mail();
                $mail->setBodyHtml($message);
                $mail->setFrom($emailFrom, $fromUserName);
                $mail->addTo($toEmail, $toUserName);
                $mail->setSubject($subject);
                $mail->send();
                $this->view->title = 'Success responde';
                $this->view->message = 'Your responde was sent to ' . $toUserName;
                $this->view->status = "status";
            } catch (Exception $ex) {
                $this->view->title = 'Error responde';
                $this->view->message = 'Your responde was not sent to ' . $toUserName . " because " . $ex->getMessage();
                $this->view->status = "error";
            }
        } else {
            $this->view->title = 'Error responde';
            $this->view->message = 'Your responde was not sent because some data is invalid ';
            $this->view->status = "error";
        }
    }

    public function respondeResponseRoomAction() {

        // Recuperar el id del usuario conectado
        $uid = Zend_Registry::get('uid');

        // Consultar usuario de facebook
        $from_user = $this->fbDgt->execFql("SELECT uid, first_name, last_name FROM user WHERE uid IN (" . $uid . ") AND is_app_user");
        if ($from_user && is_array($from_user)) {
            $fromUserName = $from_user[0]["first_name"] . ", " . $from_user[0]["last_name"];
        }

        // Recuperar la informacion del usuario conectado.
        $f = new Zend_Filter_StripTags();
        $parametersKeys = array('codigoRoom', 'txtFrom');
        $validData = array();
        foreach ($parametersKeys as $parameterName) {
            $validData[$parameterName] = $f->filter($this->getRequest()->getParam($parameterName, ''));
        }

        $filter = array(
            'codigoRoom' => array('StripTags', 'Digits'),
            'txtFrom' => array('StripTags', 'StringTrim')
        );

        $validators = array(
            'codigoRoom' => 'Digits',
            'txtFrom' => new Zend_Validate_NotEmpty()
        );

        $parameters = new Zend_Filter_Input($filter, $validators, $validData);
        $emailValidator = new Zend_Validate_EmailAddress();
        if ($parameters->isValid()) {

            $codeRoom = $parameters->codigoRoom;
            $emailFrom = $parameters->txtFrom;
            $subject = mb_convert_encoding($this->getRequest()->getParam('subject', ''), "ISO-8859-1", "UTF-8");
            $message = mb_convert_encoding($this->getRequest()->getParam('message', ''), "ISO-8859-1", "UTF-8");

            // Recuperar la propiedad.
            $room = new Hm_Rmt_Cuarto();
            $rs = $room->find($codeRoom);

            $curRoom = $rs->current();

            // Consultar usuario de facebook
            $to_user = $this->fbDgt->execFql("SELECT uid, first_name, last_name, email FROM user WHERE uid IN (" . $curRoom->Uid . ") AND is_app_user");

            if ($to_user && is_array($to_user)) {
                $toUserName = $to_user[0]["first_name"] . ", " . $to_user[0]["last_name"];
                $toEmail = $to_user[0]["email"];
            }

            try {
                $mail = new Zend_Mail();
                $mail->setBodyHtml($message);
                $mail->setFrom($emailFrom, $fromUserName);
                $mail->addTo($toEmail, $toUserName);
                $mail->setSubject($subject);
                $mail->send();
                $this->view->title = 'Success responde';
                $this->view->message = 'Your responde was sent to ' . $toUserName;
                $this->view->status = "status";
            } catch (Exception $ex) {
                $this->view->title = 'Error responde';
                $this->view->message = 'Your responde was not sent to ' . $toUserName . " because " . $ex->getMessage();
                $this->view->status = "error";
            }
        } else {
            $this->view->title = 'Error responde';
            $this->view->message = 'Your responde was not sent because some data is invalid ';
            $this->view->status = "error";
        }
    }

    public function mutualFriendsAction() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessión.");
            die;
        }

        $uid = $_GET["id"];
        $this->view->title = htmlentities($this->view->translate('LABEL_MUTUAL_FRIENDS'));
        $arrMutualFriends = $fb->GetMutualFriends($uid, '');

        if (is_array($arrMutualFriends)) {
            foreach ($arrMutualFriends as &$friend) {
                $friend['pic_square'] = (!empty($friend['pic_square'])) ? $friend['pic_square'] : "/img/q_silhouette.gif";
                $friend['first_name'] = iconv("UTF-8", "ISO-8859-1", $friend['first_name']);
                $friend['last_name'] = iconv("UTF-8", "ISO-8859-1", $friend['last_name']);
            }

            $this->view->mutualFriends = $arrMutualFriends;
        }
    }

    public function requestPermissionAction() {
    
    }

    public function tabAction() {
    	//Datos de la firma
    	$appfb = Zend_Registry::get('fb');
    	$signed = $appfb->getSignedRequest();
    	$pageId = $signed['page']['id'];
    	
    	// style 
    	
    	$this->view->headLink()->appendStylesheet('/css/new_tab.css');
    	
    	$this->view->pageid = $pageId;
    	if(isset($signed['page']['admin']) &&  $signed['page']['admin'] == 1){
    		$message = '<strong>&iexcl;Ya casi agregas Housemarket Tab!</strong><br/>';
    		$message .= 'Estas a un paso de agregar un buscador de propiedades a tu p&aacute;gina de Facebook. Para completar la instalaci&oacute;n haz clic en instalar.';
    		$this->view->admin = 1;
    	}else{
    		$message = '<strong>Vuelve pronto...</strong><br/>';
    		$message .= 'Pronto encontraras en esta p&aacute;gina un buscador de propiedades con mapas de Google y otras herramientas de filtro para ayudarte encontrar la propiedad que andas buscando.';
    		
    	}
    	$this->view->message = $message;
    }
    
    public function tab2Action() {
        //Datos de la firma
        $datafb = Zend_Registry::get('signed');

        $config = Zend_Registry::get('config');
        $db = Zend_Registry::get('db');
        
        $rsUserInfo = array();
        if (Zend_Registry::isRegistered('uid')) {
            $uid = Zend_Registry::get('uid');
            
            $sqlUserInfo = sprintf("SELECT Uid, NombreCliente, EMail FROM cliente WHERE Uid = %s", $uid);
            $rsUserInfo = reset($db->query($sqlUserInfo)->fetchAll());
        }
        
        $pageId = $datafb['page']['id'];
        $pageInfo = $this->fbDgt->execFql('SELECT page_id, name, username FROM page WHERE page_id = ' . $pageId);
        $pageInfo = reset($pageInfo);
        $tabEnabled = false;

        $sqlFbPageInternalInfo = sprintf('SELECT fb_page.uid FROM fb_page WHERE fb_page.pageid = %s', $pageId);
        $rsFbPageInternalInfo = $db->fetchAll($sqlFbPageInternalInfo);
		$ownersUids = array();
		if(isset($rsFbPageInternalInfo[0]) && is_array($rsFbPageInternalInfo[0])){
			$ownersUids = HMUtils::build_simple_indexed_array_from_array($rsFbPageInternalInfo, 'uid');	
		}
		if(isset($rsFbPageInternalInfo[0]) && is_object($rsFbPageInternalInfo[0])){
			$ownersUids = HMUtils::build_simple_indexed_array_from_object($rsFbPageInternalInfo, 'uid');
		}
        
        $ownersUidsConcatenated = join(",", $ownersUids);

        $this->view->isadmin = ($datafb['page']['admin']) ? true : false;

        $sqlPageServiceInfo = sprintf('' .
                'SELECT `ps`.`pageId` AS pageId, `ps`.`serviceStatus`, `ps`.`hmNotification` ' .
                'FROM `page_service_status` AS ps ' .
                'WHERE `ps`.`pageId` = %s ' .
                'AND `ps`.`serviceType` = %s ', $pageId, 1 // Service type = 1 is tab on page
        );

        $rsPageServiceInfo = reset($db->fetchAll($sqlPageServiceInfo));
		if(is_array($rsPageServiceInfo)){
			$rsPageServiceInfo = (object) $rsPageServiceInfo;
		}

        // first time tab visited
        if (empty($rsPageServiceInfo) || (is_object($rsPageServiceInfo) && $rsPageServiceInfo->serviceStatus === 0)) {
            $tabEnabled = false;

            $HMNotificationSent = $this->sendNotificationMailToHM($pageInfo, $config, $this->view->isadmin, get_object_vars($rsUserInfo));

            $insert_data = array(
                'pageId' => $pageInfo['page_id'],
                'pageName' => $pageInfo['name'],
                'serviceType' => 1, // tab on fan page
                'serviceStatus' => 1, // active by default (as trial)
                'hmNotification' => (!empty($HMNotificationSent) ? 1 : 0), // 1 = sent. 0 = not sent
                'clientNotification' => 0, // 1 = sent. 0 = not sent
                'requestDate' => new Zend_Db_Expr("NOW()"),
            );
            $db->insert('page_service_status', $insert_data);
        }

        // hm notification not sent
        if (is_object($rsPageServiceInfo) && $rsPageServiceInfo->serviceStatus === '0' && $rsPageServiceInfo->hmNotification === '0') {
            $HMNotificationSent = $this->sendNotificationMailToHM($pageInfo, $config, $this->view->isadmin, $rsUserInfo);

            if (!empty($HMNotificationSent)) {
                $update_data = array(
                    'hmNotification' => 1,
                );
                $update_where_clause = 'pageId = ' . $pageId . ' AND serviceType = 1';
                $db->update('page_service_status', $update_data, $update_where_clause);
            }
        }
        if ($rsPageServiceInfo->serviceStatus === '1') {
            $tabEnabled = true;
        }
        
        if ($tabEnabled === false) {
            // minified and concatenated: disabled-tab.css
            $this->view->headLink()->appendStylesheet("/css/tab.disabled.min.css?v=1");
            $this->_helper->layout->setLayout('fbml');
            $this->_helper->layout()->enableLayout();

            $this->view->tabDisabled = true;
            $this->view->pageName = $pageInfo['name'];
            $this->view->local_app_url = $config['local_app_url'];
        } else {
            try {
                // minified and concatenated: general.css, fanpage.css
                $this->view->headLink()->appendStylesheet("/css/tab.enabled.min.css?v=1");
                $this->view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js', 'text/javascript');
                $this->view->headScript()->appendFile('/js/popup.js?v=14', 'text/javascript');

                $this->view->pageid = $pageId;
                $this->view->showMsg = 0;

                //Si usa la aplicacion
                // verificar la parte curent request
                if ($uid) {

                    if (!$this->view->isadmin && in_array($uid, $ownersUids)) {
                        $where = sprintf("uid = %s AND pageid = %s", $uid, $pageId);
                        $db->delete('fb_page', $where);

						$sql = sprintf('SELECT fb_page.uid FROM fb_page WHERE fb_page.pageid = %s', $pageId);
                        $rs = $db->fetchAll($sql);
						$ownersUids = array();
						if(is_array($rs)){
							$ownersUids = HMUtils::build_simple_indexed_array_from_array($rs, 'uid');	
						}
						if(is_object($rs)){
							$ownersUids = HMUtils::build_simple_indexed_array_from_object($rs, 'uid');
						}
                        $ownersUidsConcatenated = join(",", $ownersUids);
                    }

                    if ($this->view->isadmin) {
                        try {

                            if (!$ownersUidsConcatenated || (is_array($ownersUids) && !in_array($uid, $ownersUids))) {
                                $data = array(
                                    'uid' => $uid,
                                    'pageid' => $pageId,
                                    'dateIngreso' => new Zend_Db_Expr('CURDATE()')
                                );

								$sql = sprintf('SELECT fb_page.uid FROM fb_page WHERE fb_page.pageid = %s', $pageId);
                                $rs = $db->fetchAll($sql);
								$ownersUids = array();
								if(isset($rsFbPageInternalInfo[0]) && is_array($rsFbPageInternalInfo[0])){
									$ownersUids = HMUtils::build_simple_indexed_array_from_array($rsFbPageInternalInfo, 'uid');	
								}
								if(isset($rsFbPageInternalInfo[0]) && is_object($rsFbPageInternalInfo[0])) {
									$ownersUids = HMUtils::build_simple_indexed_array_from_object($rsFbPageInternalInfo, 'uid');
								}
                                $ownersUids = HMUtils::build_simple_indexed_array_from_object($rs, 'uid');
                                //@todo verificar la parte inscription por primera ves
                                if(!in_array($uid, $ownersUids)){
                                	$db->insert('fb_page', $data);
                                	$ownersUids[] = $uid;
                                }
                                $ownersUidsConcatenated = join(",", $ownersUids);
                            }
                        } catch (Exception $ex) {
                            debug($ex);
                        }
                    }
                } // fin de if ($uid)

                $this->_helper->layout->setLayout('fbml');
                $this->_helper->layout()->enableLayout();

                $this->view->itemselected = 'search';
                $this->view->novideo = true;

                try {
                    //Verificamos que la pagina tenga algun video y extraemos sus datos
                    $sql = sprintf("SELECT P.nombre,P.email,P.twitter,P.telefono,P.webpage,P.youvideo,P.nombreempresa,P.telefonoempresa FROM page P
                                INNER JOIN cliente C ON(C.profilepageid=P.pageid)
                                WHERE P.pageid=%s AND P.youvideo IS NOT NULL", $pageId);
                    $stmt = $db->query($sql);
                    $profile = $stmt->fetch();
                    
					$v = false;
                    if (is_object($profile)) {
                        $base = parse_query($profile->youvideo);
                        $v = isset($base['query']['v']) ? $base['query']['v'] : "";
                    }
                    
                    //Asignamos la pestaña por defecto
                    $appdata = (isset($datafb['app_data'])) ? $datafb['app_data'] : (empty($v)?"search":"welcome");
                    $param = $this->_request->getParam("app_data", "");
                    if (!empty($param)) {
                        $appdata = $param;
                    }

                    if ($v) {
                        $this->view->novideo = false;
                        if ($appdata == "welcome") {
                            $profile->youvideo = "https://www.youtube.com/v/" . $v . "?version=3";
                            $this->view->profile = $profile;

                            //Cambiamos la vista 
                            $this->view->itemselected = 'welcome';
                            $this->view->headScript()->appendFile("/js/fanpage.js?v=21", 'text/javascript');
                            $this->_helper->viewRenderer('welcome');
                        }
                    }

                    //Nos interesa agregar el perfil a ambas vistas, igual solo se puede usar la variable 
                    //del video, pero se mantiene más legible
                    if($appdata == "profile" || $appdata == "welcome"){
                        $this->view->headScript()->appendFile("/js/fanpage.js?v=21", 'text/javascript');
                        $this->view->profile = $profile;
                    }


                    if($appdata == "profile"){
                        $this->view->itemselected = 'profile';
                        $this->_helper->viewRenderer('perfil');
                    }

                    if ($this->view->isadmin) {                
                        if ($appdata == "welcome") {                    
                            $this->view->itemselected = 'welcome';
                            $this->_helper->viewRenderer('welcome');
                        }
                    }
                    
                } catch (Exception $ex) {
                    debug($ex);
                }


                //Asignar propietario        
                $this->view->owner = $ownersUidsConcatenated;
                Zend_Registry::set('owner', $this->view->owner);

                $this->view->urlsitio = $config['fb_app_url'];
                $translator = Zend_Registry::get('Zend_Translate');
                $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
                $currentPage = $this->_getParam('page', 1);
                $_ITEMS_PER_PAGE = 6;
                $_PAGE_RANGE = 10;

                $urlLocal = $config['local_app_url'];

                function ComboArea($max_min, $rang1, $rang2, $rang3, $post) {
                    $label_c = "";
                    $option_c = "<option value='' >" . $max_min . "</option>";
                    $MIN_C = 0;
                    $AUM;
                    for ($j = 1; $j <= 12; $j++) {
                        if ($j <= 10)
                            $AUM = $rang1;
                        if ($j > 10 && $j <= 11) {
                            $AUM = $rang2;
                        }
                        if ($j > 11 && $j <= 12) {
                            $AUM = $rang3;
                        }
                        $MIN_C += $AUM;
                        $label_c = $MIN_C;
                        $label_c = number_format($label_c, 0);
                        if ($post == $MIN_C)
                            $option_c .= "<option selected='selected' value= '" . $MIN_C . "'>" . $label_c . " mt<sup>2</sup>" . "</option>";
                        else
                            $option_c .= "<option value= '" . $MIN_C . "'>" . $label_c . " mt<sup>2</sup>" . "</option>";
                    }
                    if ($post == '10000')
                        $option_c .= "<option selected='selected' value= '10000'>1 hect</option>";
                    else
                        $option_c .= "<option value= '10000'>1 hect</option>";
                    if ($post == '50000')
                        $option_c .= "<option selected='selected' value= '50000'>5 hect</option>";
                    else
                        $option_c .= "<option value= '50000'>5 hect</option>";
                    if ($post == '500000')
                        $option_c .= "<option selected='selected' value= '500000'>50 hect</option>";
                    else
                        $option_c .= "<option value= '500000'>50 hect</option>";
                    if ($post == '1000000')
                        $option_c .= "<option selected='selected' value= '1000000'>100 hect</option>";
                    else
                        $option_c .= "<option value= '1000000'>100 hect</option>";
                    return $option_c;
                }

                function ComboAreaFt2($max_min, $rang1, $post) {
                    $label_c = "";
                    $option_c = "<option value='' >" . $max_min . "</option>";
                    $MIN_C = 0;
                    $AUM;
                    for ($j = 1; $j <= 10; $j++) {
                        if ($j <= 10)
                            $AUM = $rang1;
                        $MIN_C += $AUM;
                        $label_c = $MIN_C;
                        $label_c = number_format($label_c, 0);
                        if ($post == $MIN_C)
                            $option_c .= "<option selected='selected' value= '" . $MIN_C . "'>" . $label_c . " ft<sup>2</sup>" . "</option>";
                        else
                            $option_c .= "<option value= '" . $MIN_C . "'>" . $label_c . " ft<sup>2</sup>" . "</option>";
                    }
                    if ($post == '43560')
                        $option_c .= "<option selected='selected' value= '43560'>1 Acre</option>";
                    else
                        $option_c .= "<option value= '43560'>1 Acre</option>";
                    if ($post == '217800')
                        $option_c .= "<option selected='selected' value= '217800'>5 Acre</option>";
                    else
                        $option_c .= "<option value= '217800'>5 Acre</option>";
                    if ($post == '2178000')
                        $option_c .= "<option selected='selected' value= '2178000'>50 Acre</option>";
                    else
                        $option_c .= "<option value= '2178000'>50 Acre</option>";
                    if ($post == '4356000')
                        $option_c .= "<option selected='selected' value= '4356000'>100 Acre</option>";
                    else
                        $option_c .= "<option value= '4356000'>100 Acre</option>";
                    return $option_c;
                }

                function ComboRent($max_min, $rang1, $rang2, $rang3, $rang4, $post) {

                    $label_c = "";
                    $option_c .= "<option value='' >" . $max_min . "</option>";
                    $MIN_C = 0;
                    $AUM = 0;
                    for ($j = 1; $j < 22; $j++) {
                        if ($j <= 10)
                            $AUM = $rang1;
                        if ($j > 10 && $j <= 15) {
                            $AUM = $rang2;
                        }
                        if ($j > 15 && $j <= 19) {
                            $AUM = $rang3;
                        }
                        if ($j > 19 && $j <= 22) {
                            $AUM = $rang4;
                        }

                        $MIN_C += $AUM;
                        $label_c = $MIN_C;
                        if ($post == $MIN_C)
                            $option_c .= "<option selected='selected' value= '" . $MIN_C . "'>" . number_format($label_c, 0) . "</option>";
                        else
                            $option_c .= "<option value= '" . $MIN_C . "'>" . number_format($label_c, 0) . "</option>";
                        //echo $option_c;  
                    }
                    return $option_c;
                }

                function ComboSale($max_min, $rang1, $rang2, $rang3, $rang4, $rang5, $rang6, $post) {
                    $label_c = "";
                    $option_c = "<option value='' >" . $max_min . "</option>";
                    $MIN_C = 0;
                    $AUM = 0;
                    for ($j = 1; $j < 24; $j++) {
                        if ($j <= 10)
                            $AUM = $rang1;
                        if ($j > 10 && $j <= 12) {
                            $AUM = $rang2;
                        }
                        if ($j > 12 && $j <= 14) {
                            $AUM = $rang3;
                        }
                        if ($j > 14 && $j <= 17) {
                            $AUM = $rang4;
                        }
                        if ($j > 17 && $j <= 19) {
                            $AUM = $rang5;
                        }
                        if ($j > 19 && $j <= 23) {
                            $AUM = $rang6;
                        }
                        $MIN_C += $AUM;
                        if ($j >= 12) {
                            $label_c = ($MIN_C / 1000000) . " " . "<i18n>LABEL_SEARCH_MILLION</i18n>";
                        } else {
                            $label_c = $MIN_C;
                            $label_c = number_format($label_c, 0);
                        }
                        if ($post == $MIN_C)
                            $option_c .= "<option selected='selected' value= '" . $MIN_C . "'>" . $label_c . "</option>";
                        else
                            $option_c .= "<option value= '" . $MIN_C . "'>" . $label_c . "</option>";
                    }
                    return $option_c;
                }

                $this->view->fb_path = $config['fb_app_url'];
                $this->view->web_path = $config['fb_app_url'];
				if(isset($datafb['uid'])){
                	$this->view->uid = $datafb['uid'];
                }
                $var_index = 1;
                $rs = $db->query("select c.CodigoCategoria, ci.ValorIdioma as Categoria
            FROM catcategoria c
            INNER JOIN catcategoriaidioma ci ON c.CodigoCategoria = ci.CodigoCategoria
            WHERE ci.CodigoIdioma = '" . $language . "' AND c.filtro = 1 ORDER BY c.orden");
                $categories = $rs->fetchAll();
                $this->view->categories = $categories;
                $this->view->UMSuperficies = array("ft2" => "ft2", "mt2" => "mt2");
                $rsType = $db->query("select c.CodigoEnlista, ci.ValorIdioma as Categoria
            FROM cattipoenlista c
            INNER JOIN catenlistaidioma ci ON c.CodigoEnlista = ci.CodigoEnlista
            WHERE ci.CodigoIdioma = '" . $language . "' AND c.MostrarBusqueda = 'S' ORDER BY c.CodigoEnlista Desc");
                $searchTypes = $rsType->fetchAll();

                $this->view->searchTypes = $searchTypes;

                if ($this->view->itemselected == "search") {

                    $property = new Hm_Pro_Propiedad();
                    $properties = array();

                    $rsProperties = Hm_Pro_Propiedad::GetPropertyDataByUid($ownersUidsConcatenated);

                    if ($rsProperties != null && count($rsProperties) > 0) { //Tiene propiedades
                        foreach ($rsProperties as $proproperty) {
                            $properties[$var_index]["property"] = $proproperty;

                            $objFoto = new Hm_Pro_Foto();

                            $idMainPhoto = $objFoto->fetchAll(
                                            $objFoto->select()
                                                    ->from(array('f' => 'profoto'), array('PROFotoID'))
                                                    ->where('CodigoPropiedad = ?', $proproperty->CodigoPropiedad)
                                                    ->where('Principal = ?', 1)
                            );

                            //Asignacion id foto principal
                            if ($idMainPhoto->current()->PROFotoID) {
                                $properties[$var_index]["mainPhoto"] = "/pic" . $idMainPhoto->current()->PROFotoID . "_" . Hm_Pro_Foto::$_PHOTO_TYPES["Destacada"] . ".jpg";
                            } else {
                                $properties[$var_index]["mainPhoto"] = "/img/" . strtolower($language) . "/ad_no_image.jpg";
                            }

                            //Recuperar todos los atributos
                            $rsAttributes = $db->fetchAll("SELECT CA.CodigoAtributo, AI.ValorIdioma as NombreAtributo,AP.ValorEntero,AP.ValorDecimal,AP.ValorS_N,
                                            AP.ValorCaracter FROM proatributopropiedad AP
                                            INNER JOIN catatributo CA on AP.CodigoAtributo=CA.CodigoAtributo
                                            INNER JOIN propropiedad P on AP.CodigoPropiedad=P.CodigoPropiedad
                                            INNER JOIN catatributoidioma AI ON AI.CodigoAtributo = CA.CodigoAtributo
                                            WHERE P.CodigoPropiedad=" . $proproperty->CodigoPropiedad . " AND AI.CodigoIdioma = '" . $language . "'" .
                                            " AND P.CodigoEdoPropiedad<>4 AND CA.CodigoAtributo IN(1,2,3)");

                            $attributes = array();

                            if ($proproperty->Area > 0) {
                                $attributes[] = $proproperty->Area . ' ' . $proproperty->UnidadMedida . ' ' . (strtolower($translator->getAdapter()->translate('ADD_ESTATE_ADDITIONAL_INFO_AREA')));
                            }

                            foreach ($rsAttributes as $val) {
                                $SiNo = '';
                                if (strtoupper($val->ValorS_N) == 'S')
                                    $SiNo = "<i18n>LABEL_YES</i18n>";
                                else if (strtoupper($val->ValorS_N) == 'N')
                                    $SiNo = "<i18n>LABEL_NO</i18n>";
                                $attributes[] = $val->ValorEntero . $val->ValorDecimal . $SiNo . $val->ValorCaracter . ' ' . ($val->NombreAtributo);
                            }

                            $properties[$var_index]["attributes"] = implode(" | ", $attributes);

                            $var_index++;
                        }
                    }

                    $paginator = Zend_Paginator::factory($properties);
                    $paginator->setCurrentPageNumber($currentPage);
                    $paginator->setItemCountPerPage($_ITEMS_PER_PAGE);
                    $paginator->setPageRange($_PAGE_RANGE);

                    $this->view->properties = $paginator;
                }
            } catch (Exception $ex) {
                $err = $ex->getMessage();
            }
        } // fin de if($tabEnabled === false)
    }

    public function paypalrequestAction() {

        // Recuperar el minimo de los creditos.
        $parametro = new Hm_Util_Parametros();
        $minimoCreditos = Hm_Pp_Purchase::DEFAULT_MIN_CREDITS;
        $rs = $parametro->fetchAll($parametro->getAdapter()->quoteInto("NombreParametro=?", "MINIMO_CREDITOS"));
        if ($rs->count() > 0) {
            $minimoCreditos = intval($rs->current()->ValorParametro);
        }

        $f = new Zend_Filter_StripTags();

        // Recueprar los creditos;
        $credits = $this->_request->getParam("txtPrice");

        // Recuperar las propiedades.
        $propertyCodes = $f->filter($this->_request->getParam("CodigoPropiedad"), NULL);
        $this->view->invoice = $f->filter($this->_request->getParam("invoice"), NULL);

        if ($propertyCodes) {
            // Recuperar el listado de las propiedades
            $properties = new Hm_Pro_Propiedad();
            $where = "CodigoPropiedad IN (" . $propertyCodes . ")";
            $rs = $properties->fetchAll($where);
            $propertiesList = array();
            if ($rs->count() > 0) {
                foreach ($rs as $property) {
                    $creditos = $minimoCreditos;
                    if (is_array($credits) && array_key_exists($property->CodigoPropiedad, $credits)) {
                        $creditos = $credits[$property->CodigoPropiedad];
                    }
                    $propertiesList[] = array(
                        "Codigo" => $property->CodigoPropiedad,
                        "CodigoBroker" => $property->CodigoBroker ? $property->CodigoBroker : "-",
                        "Nombre" => $property->NombrePropiedad,
                        "Direccion" => $property->Direccion,
                        "Credits" => $creditos
                    );
                }
            }
            $this->view->properties = $propertiesList;
        } else {
            // Mensaje de error de no encontrar las propiedades a cuales comprar creditos.
        }
    }

    public function paypalconfirmAction() {

        try {

            $f = new Zend_Filter_StripTags();

            // Recuperar las propiedades.
            $propertyCodes = $f->filter($this->_request->getParam("CodigoPropiedad"), NULL);

            // Recuperar la cantidad de creditos por propiedades
            $this->view->prices = $_POST["txtPrice"];

            $btnCredit = $f->filter($this->_request->getParam("btnCreditos"), NULL);

            if ($btnCredit != null) {
                // Regresar a la pantalla anterior
                $this->_forward("paypalRequest", "Fbml", null, array("CodigoPropiedad" => $propertyCodes, "txtPrice" => $this->view->prices));
                return;
            }

            // Recuperar el Id del usuario conectado
            $uid = Zend_Registry::get('uid');

            // Recuperar el codigo del cliente.
            $cliente = new Hm_Cli_Cliente();
            $where = $cliente->getAdapter()->quoteInto("Uid=?", $uid);
            $rsClientes = $cliente->fetchAll($where);
            if ($rsClientes->count() > 0) {
                $this->view->clientCode = $rsClientes->current()->CodigoCliente;
            }

            // Recuperar el valor unitario de los creditos
            $parametro = new Hm_Util_Parametros();
            $precioCredito = Hm_Pp_Purchase::DEFAULT_PRICE_BY_ITEMS;
            $rs = $parametro->fetchAll($parametro->getAdapter()->quoteInto("NombreParametro=?", "PRECIO_CREDITO_PAYPAL"));

            if ($rs->count() > 0) {
                $precioCredito = doubleval($rs->current()->ValorParametro);
            }

            $minimoCreditos = Hm_Pp_Purchase::DEFAULT_MIN_CREDITS;
            $rs = $parametro->fetchAll($parametro->getAdapter()->quoteInto("NombreParametro=?", "MINIMO_CREDITOS"));
            if ($rs->count() > 0) {
                $minimoCreditos = intval($rs->current()->ValorParametro);
            }

            // Calcular el total de creditos a comprar
            $invoice = $f->filter($this->_request->getParam("invoice"), NULL);
            // echo "Invoice : " . $invoice;
            $totalCredito = 0;
            if (is_array($_POST["txtPrice"])) {
                foreach ($_POST["txtPrice"] as $price) {
                    $price = doubleval($price);
                    if ($price % $minimoCreditos != 0) {
                        $this->_flashMessenger->clearMessages();
                        $this->_flashMessenger->addError("Los creditos deben ser multiplos de " . $minimoCreditos);
                        $this->_forward("paypalRequest", "Fbml", null, array("CodigoPropiedad" => $propertyCodes, "txtPrice" => $this->view->prices, "invoice" => $invoice));
                        return;
                    } else if ($price < $minimoCreditos) {
                        $this->_flashMessenger->clearMessages();
                        $this->_flashMessenger->addError("Los creditos por propiedad debe ser por lo minimo de " . $minimoCreditos);
                        $this->_forward("paypalRequest", "Fbml", null, array("CodigoPropiedad" => $propertyCodes, "txtPrice" => $this->view->prices, "invoice" => $invoice));
                        return;
                    }
                    $totalCredito += $price;
                }
            }

            if ($propertyCodes) {
                // Crear la compra en linea.
                $purchaseData = array();
                $purchase = new Hm_Pp_Purchase();

                $purchaseData["Price"] = $precioCredito;
                $purchaseData["Quantity"] = $totalCredito;
                $purchaseData["Amount"] = $precioCredito * $totalCredito;
                $purchaseData["Date"] = new Zend_Db_Expr('NOW()');
                $purchaseData["User"] = $this->view->clientCode;
                $purchaseData["Status"] = "Pn";

                if ($invoice) {
                    $this->view->invoice = $invoice;
                    $where = $purchase->getAdapter()->quoteInto("InvoiceCode = ?", $invoice) .
                            " AND " .
                            $purchase->getAdapter()->quoteInto("MD5(CONCAT(PurchaseId,'-',User))=?", $invoice);

                    $curPurchase = $purchase->fetchRow($where);
                    $purchaseId = $curPurchase->PurchaseId;

                    $purchase->update($purchaseData, $where);
                } else {
                    $purchase->insert($purchaseData);
                    $purchaseId = $purchase->getAdapter()->lastInsertId();
                    $this->view->invoice = $purchase->GenerateInvoiceCode($purchaseId, $this->view->clientCode);

                    $purchaseData = array();
                    $purchaseData["InvoiceCode"] = $this->view->invoice;
                    $purchase->update($purchaseData, $purchase->getAdapter()->quoteInto("PurchaseId=?", $purchaseId));
                }

                // Recuperar el listado de las propiedades
                $properties = new Hm_Pro_Propiedad();
                $where = "CodigoPropiedad IN (" . $propertyCodes . ")";
                $rs = $properties->fetchAll($where);
                $propertiesList = array();
                if ($rs->count() > 0) {
                    $purchaseDetail = new Hm_Pp_PurchaseDetail();
                    foreach ($rs as $property) {
                        $propertiesList[] = array(
                            "Codigo" => $property->CodigoPropiedad,
                            "CodigoBroker" => $property->CodigoBroker ? $property->CodigoBroker : "-",
                            "Nombre" => $property->NombrePropiedad,
                            "Direccion" => $property->Direccion
                        );
                        // Crear registro de detalle de compra.
                        $purchaseDetailData = array();
                        $purchaseDetailData["PurchaseId"] = $purchaseId;
                        $purchaseDetailData["PropertyId"] = $property->CodigoPropiedad;
                        $purchaseDetailData["Quantity"] = $this->view->prices[$property->CodigoPropiedad];
                        $purchaseDetailData["Date"] = new Zend_Db_Expr('CURDATE()');
                        $purchaseDetailData["User"] = $this->view->clientCode;
                        $purchaseDetailData["Status"] = "Pn";
                        if ($invoice) {
                            $where = $purchaseDetail->getAdapter()->quoteInto("PurchaseId=?", $purchaseId) .
                                    " AND " .
                                    $purchaseDetail->getAdapter()->quoteInto("PropertyId=?", $property->CodigoPropiedad);
                            $purchaseDetail->update($purchaseDetailData, $where);
                        } else {
                            $purchaseDetail->insert($purchaseDetailData);
                        }
                    }
                }
                $this->view->properties = $propertiesList;
            } else {
                // Mensaje de error de no encontrar las propiedades a cuales comprar creditos.
            }

            $test = new Zend_Currency(en_US);
            $test->setFormat(array("display" => Zend_Currency::USE_NAME, "name" => "USD $"));
            $this->view->totalCredito = $totalCredito;
            $this->view->totalCash = $test->toCurrency($totalCredito * $precioCredito, array());
            $this->view->paypalAmount = $totalCredito * $precioCredito;
            $this->view->paypalQuantity = $totalCredito;

            // Generar los datos encriptados a enviar a paypal.
            // Esto evitara que algun usuario malicioso modifique los datos
            // susceptibles de la compra
            $paypal = new Hm_PayPalEWP();
            $paypal->setTempFileDirectory("/tmp");
            $paypal->setCertificate("/home/hmadmin/certificados/my-pubcert.pem", "/home/hmadmin/certificados/my-prvkey.pem");
            $paypal->setCertificateID("WJDD2G4MMEWWG");
            $paypal->setPayPalCertificate("/home/hmadmin/certificados/paypal_cert_pem.txt");

            $paypalParam = array(
                "cmd" => "_xclick",
                "business" => "info@hmapp.com",
                "item_name" => "Creditos en linea",
                "invoice" => $this->view->invoice,
                "quantity" => $this->view->paypalQuantity,
                "amount" => 1/* $this->view->paypalAmount */
            );

            $this->view->form =
                    "<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\" target=\"_top\">
                       <input type=\"hidden\" name=\"cmd\" value=\"_s-xclick\"/>
                       <input type=\"hidden\" name=\"encrypted\" value=\"-----BEGIN PKCS7-----\n" . $paypal->encryptButton($paypalParam) . "\n-----END PKCS7-----\"/>
                       <input class=\"fb_button\" type=\"submit\" value=\"Comprar >>\" name=\"btnComprar\" />
                    </form>";
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function paypalcancelAction() {
        
    }

    public function paypalokAction() {

        $this->view->paypal = array();
        $this->view->hm = array();
        $this->view->transactionId = $_GET["tx"];

        //read the post from PayPal system and add ?cmd?
        $tx_token = $_GET["tx"];
        $auth_token = "9hH_1gYAEPJFuQ2tmNiTWj-0-zp0Ed_QqxBqs7Hezf9nAmky0OQ3-tUHOEa";
        $req = "cmd=_notify-synch";
        $req .= "&tx=$tx_token&at=$auth_token";
        $header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
        $fp = fsockopen("www.sandbox.paypal.com", 80, $errno, $errstr, 30);
        $isError = 0;
        if (!$fp) {
            $isError = 2; //error HTTP
        } else {
            fputs($fp, $header . $req);
            // read the body data
            $res = "";
            $headerdone = false;
            while (!feof($fp)) {
                $line = fgets($fp, 1024);
                if (strcmp($line, "\r\n") == 0) {
                    // read the header
                    $headerdone = true;
                } else if ($headerdone) {
                    // header has been read. now read the contents
                    $res .= $line;
                }
            }    // parse the data
            $lines = explode("\n", $res);
            $keyarray = array();

            if (strcmp($lines[0], "SUCCESS") == 0) {
                for ($i = 1; $i < count($lines); $i++) {
                    list($key, $val) = explode("=", $lines[$i]);
                    $keyarray[urldecode($key)] = urldecode($val);
                }

                $isError = 0; //no error
                $this->view->paypal["productName"] = $keyarray["item_name"];
                $this->view->paypal["productNumber"] = $keyarray["item_number"];
                $this->view->paypal["price"] = $keyarray["payment_gross"];
                $this->view->paypal["priceCC"] = $keyarray["mc_currency"];
                $this->view->paypal["quantity"] = $keyarray["quantity"];
                $this->view->paypal["amount"] = doubleval($keyarray["payment_gross"]) * doubleval($keyarray["quantity"]);
                $this->view->paypal["amountCC"] = $keyarray["mc_currency"];
                $this->view->paypal["invoice"] = $keyarray["invoice"];
                $this->view->paypal["date"] = $keyarray["payment_date"];
                $this->view->paypal["status"] = $keyarray["payment_status"];
                $invoice = $keyarray["invoice"];

                // @todo: Verificar que las cantidades coincidan con las registradas
                // Recuperar la compra con la factura indicada como respuesta.
                $purchase = new Hm_Pp_Purchase();

                $where = $purchase->getAdapter()->quoteInto("InvoiceCode = ?", $invoice) .
                        " AND " .
                        $purchase->getAdapter()->quoteInto("MD5(CONCAT(PurchaseId,'-',User))=?", $invoice);
                $record = $purchase->fetchRow($where);

                if ($record) {
                    if ($this->view->paypal["status"] == "Completed") {
                        echo "Completed?" . $this->view->paypal["status"];
                        $purchaseData = array();
                        $purchaseData["Status"] = "Pr";
                        $purchase->update($purchaseData, $purchase->getAdapter()->quoteInto("PurchaseId=?", $record->PurchaseId));

                        // Asignar los creditos segun el detalle de la compra.
                        $credit = new Hm_Cre_Credito();
                        $creditData = array();
                        $creditData["FechaCompra"] = new Zend_Db_Expr("NOW()");
                        $creditData["Cantidad"] = $record->Quantity;
                        $creditData["Saldo"] = 0;
                        $creditData["EstadoCredito"] = "A";
                        $creditData["Codigocliente"] = $record->User;
                        $creditData["FechaPedido"] = $record->Date;
                        $creditData["MontoCompra"] = $record->Amount;
                        $creditData["TipoCompra"] = "ONLINE";

                        $credit->insert($creditData);
                        $creditId = $credit->getAdapter()->lastInsertId();
                    }

                    $purchaseDetail = new Hm_Pp_PurchaseDetail();
                    $purchaseDetails = $purchaseDetail->fetchAll(
                                    $purchaseDetail->getAdapter()->quoteInto("PurchaseId=?", $record->PurchaseId)
                    );

                    if ($purchaseDetails->count() > 0) {
                        // Recuperar el valor de cada credito de la tabla de par�metros.
                        $parametro = new Hm_Util_Parametros();
                        $precioCredito = Hm_Pp_Purchase::DEFAULT_PRICE_BY_ITEMS;
                        $rs = $parametro->fetchAll($parametro->getAdapter()->quoteInto("NombreParametro=?", "PRECIO_CREDITO_PAYPAL"));

                        if ($rs->count() > 0) {
                            $precioCredito = doubleval($rs->current()->ValorParametro);
                        }

                        $creditProperty = new Hm_Cre_CreditoPropiedad();
                        $purchaseDetailData = array();
                        $purchaseDetailData["Status"] = "Pr";

                        foreach ($purchaseDetails as $detail) {
                            $hmDetail = array();
                            $hmDetail["name"] = $detail->PropertyId;
                            $hmDetail["quantity"] = $detail->Quantity;
                            $hmDetail["amount"] = $detail->Quantity * $precioCredito;
                            $this->view->hm["details"][] = $hmDetail;

                            if ($this->view->paypal["status"] == "Completed") {
                                $creditPropertyData = array();
                                $creditPropertyData["CodigoPropiedad"] = $detail->PropertyId;
                                $creditPropertyData["CodigoCredito"] = $creditId;
                                $creditPropertyData["FechaAsigna"] = $detail->Date;
                                $creditPropertyData["Cantidad"] = $detail->Quantity;
                                $creditPropertyData["EstadoAsigna"] = "A";
                                $creditProperty->insert($creditPropertyData);

                                // Actualizar los datos del detalle de la compra.
                                $purchaseDetail->update($purchaseDetailData, $purchaseDetail->getAdapter()->quoteInto("PurchaseDetailId=?", $detail->PurchaseDetailId));
                            }
                        }
                    }

                    $this->view->hm["price"] = $record->Price;
                    $this->view->hm["priceCC"] = "USD";
                    $this->view->hm["quantity"] = $record->Quantity;
                    $this->view->hm["amount"] = $record->Amount;
                    $this->view->hm["amountCC"] = "USD";
                    $this->view->hm["status"] = $record->Status;
                }
            } else if (strcmp($lines[0], "FAIL") == 0) {
                $isError = 1; //error de transaccion
            }
        }
        fclose($fp);

        $this->_helper->layout()->enableLayout();
    }

    public function paypalipnAction() {

        // read the post from PayPal system and add ?cmd?
        $req = "cmd=_notify-validate";
        foreach ($_POST as $key => $value) {
            $value = urlencode(stripslashes($value));
            $req .= "&$key=$value";
        }

        // post back to PayPal system to validate
        $header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
        $fp = fsockopen("www.sandbox.paypal.com", 80, $errno, $errstr, 30);

        // assign posted variables to local variables
        $item_name = $_POST["item_name"];
        $item_number = $_POST["item_number"];
        $payment_status = $_POST["payment_status"];
        $payment_amount = $_POST["mc_gross"];
        $payment_currency = $_POST["mc_currency"];
        $txn_id = $_POST["txn_id"];
        $receiver_email = $_POST["receiver_email"];
        $payer_email = $_POST["payer_email"];
        $transid = $_POST["txn_id"];
        $invoice = $_POST["invoice"];
        $idUsuario = $_POST["item_number"];
        $cantidad = $_POST["mc_gross"];
        $creditos = 100;

        if (!$fp) {
            //CONTROL DE ERRORES; NO SE PUEDE CONECTAR CON PAYPAL
            //NO ES GRAVE, COMO NO LE CONFIRMAMOS LA TRANSACCION
            //ELLOS MISMOS LA REINTENTAR??N M??S ADELANTE
        } else {
            // Guardamos el mensaje recibido de paypal,
            // Permitira realizar analisis posteriores
            $ipnMessage = new Hm_Pp_IpnMessage();
            $ipnMessageData = array(
                "Date" => new Zend_Db_Expr("NOW()"),
                "Message" => "GET: " . serialize($_GET) . "\r\nPOST: " . serialize($_POST)
            );
            $ipnMessage->insert($ipnMessageData);
            $profiler = $ipnMessage->getAdapter()->getProfiler();
            $query = $profiler->getLastQueryProfile();
            fputs($fp, $header . $req);
            while (!feof($fp)) {
                $res = fgets($fp, 1024);
                if (strcmp($res, "VERIFIED") == 0) {
                    // Recuperar el pago de acuerdo a la factura (invoice)
                    $purchase = new Hm_Pp_Purchase();

                    $where = $purchase->getAdapter()->quoteInto("InvoiceCode = ?", $invoice) .
                            " AND " .
                            $purchase->getAdapter()->quoteInto("MD5(CONCAT(PurchaseId,'-',User))=?", $invoice);
                    $record = $purchase->fetchRow($where);
                    if ($record) {
                        //compruebo que no se haya procesado ya la transaccion
                        if ($record->Status == "Pn") {
                            if ($payment_status == "Completed") {
                                $purchaseData = array();
                                $purchaseData["Status"] = "Pr";
                                $purchase->update($purchaseData, $purchase->getAdapter()->quoteInto("PurchaseId=?", $record->PurchaseId));

                                // Asignar los creditos segun el detalle de la compra.
                                $credit = new Hm_Cre_Credito();
                                $creditData = array();
                                $creditData["FechaCompra"] = new Zend_Db_Expr("NOW()");
                                $creditData["Cantidad"] = $record->Quantity;
                                $creditData["Saldo"] = 0;
                                $creditData["EstadoCredito"] = "A";
                                $creditData["Codigocliente"] = $record->User;
                                $creditData["FechaPedido"] = $record->Date;
                                $creditData["MontoCompra"] = $record->Amount;
                                $creditData["TipoCompra"] = "ONLINE";

                                $credit->insert($creditData);
                                $creditId = $credit->getAdapter()->lastInsertId();
                            }
                        }

                        $purchaseDetail = new Hm_Pp_PurchaseDetail();
                        $purchaseDetails = $purchaseDetail->fetchAll(
                                        $purchaseDetail->getAdapter()->quoteInto("PurchaseId=?", $record->PurchaseId)
                        );

                        if ($purchaseDetails->count() > 0) {
                            // Recuperar el valor de cada credito de la tabla de par�metros.
                            $parametro = new Hm_Util_Parametros();
                            $precioCredito = Hm_Pp_Purchase::DEFAULT_PRICE_BY_ITEMS;
                            $rs = $parametro->fetchAll($parametro->getAdapter()->quoteInto("NombreParametro=?", "PRECIO_CREDITO_PAYPAL"));

                            if ($rs->count() > 0) {
                                $precioCredito = doubleval($rs->current()->ValorParametro);
                            }

                            $creditProperty = new Hm_Cre_CreditoPropiedad();
                            $purchaseDetailData = array();
                            $purchaseDetailData["Status"] = "Pr";

                            foreach ($purchaseDetails as $detail) {
                                if ($payment_status == "Completed") {
                                    $creditPropertyData = array();
                                    $creditPropertyData["CodigoPropiedad"] = $detail->PropertyId;
                                    $creditPropertyData["CodigoCredito"] = $creditId;
                                    $creditPropertyData["FechaAsigna"] = $detail->Date;
                                    $creditPropertyData["Cantidad"] = $detail->Quantity;
                                    $creditPropertyData["EstadoAsigna"] = "A";
                                    $creditProperty->insert($creditPropertyData);

                                    // Actualizar los datos del detalle de la compra.
                                    $purchaseDetail->update($purchaseDetailData, $purchaseDetail->getAdapter()->quoteInto("PurchaseDetailId=?", $detail->PurchaseDetailId));
                                }
                            }
                        }
                    }
                } else if (strcmp($res, "INVALID") == 0) {
                    //CONTROL DE ERRORES
                    echo "Invalid";
                }
            }
            fclose($fp);
        }
        $this->getHelper('viewRenderer')->setNoRender();
    }

    protected function sendNotificationMailToHM(array $pageInfo, array $config, $isAdmin, array $rsUserInfo = array()) {
        $html = new Zend_View();
        $html->setScriptPath(APPLICATION_PATH . '/views/scripts/emails/');

        $email_destiny = "info@hmapp.com";
        if($isAdmin === true) {
            $html->uid = $rsUserInfo['Uid'];
            $html->username = $rsUserInfo['NombreCliente'];
            $html->email = $rsUserInfo['EMail'];
        }

        $html->page_name = $pageInfo['name'];
        $html->page_id = $pageInfo['page_id'];
        $html->page_url = 'www.facebook.com/pages/' . $pageInfo['name'] . '/' . $pageInfo['page_id'];

        $body = $html->render('tabonfanpage.phtml');
        $textbody = preg_replace("'<style[^>]*>.*</style>'siU", '', $body);
        $textbody = trim(strip_tags($textbody));

        //Configuracion del servidor smtp
        $smtpServer = $config['gmail_smtpserver'];
        $username = $config['gmail_username'];
        $password = $config['gmail_password'];

        $mailConfig = array(
            'ssl' => 'tls',
            'port' => 587,
            'auth' => 'login',
            'username' => $username,
            'password' => $password
        );

        try {
            $transport = new Zend_Mail_Transport_Smtp($smtpServer, $mailConfig);

            $mail = new Zend_Mail('UTF-8');
            $mail->setFrom($username, "Housemarket Notificacion");
            $mail->setReplyTo($username, "No-Reply");
            $mail->setReturnPath($username, "No-Reply");
            $mail->addTo($email_destiny, "Housemarket");
            $mail->setSubject('Tab agregado a Fan Page');
            $mail->setBodyHtml($body);
            $mail->setBodyText($textbody);
            $status = $mail->send($transport);
            return $status;
        } catch (Exception $e) {
            //echo "Error message: " . $e->getMessage() . "\n";
        }
    }

}

?>