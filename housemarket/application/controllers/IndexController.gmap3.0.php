<?php

/**
 * Controller por defecto
 */
class IndexController extends Zend_Controller_Action {

    /**
     *
     * @var <type>
     */
    protected $_flashMessenger = null;
    /**
     *
     * @var
     */
    protected $_redirector = null;

    /**
     * Inicializacion del controller
     */
    public function init() {
        
        $this->_redirector = $this->_helper->getHelper('Redirector'); 
        //$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
    }

    /**
     * Antes de procesar alguna accion
     */
    public function preDispatch() {
        
    }

    /**
     * Buscar en el mapa
     */
    public function indexAction() {
        // action body
        
        $cfg = Zend_Registry::get('config');
        $google_api_key = $cfg['google_api_key'];
        $db = Zend_Registry::get('db');

        //Utilizando Google Maps V3...Hibrido con la V2
        
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
        $this->view->headLink()->appendStylesheet('/css/search.css?v=2');
        
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/facebook.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        $this->view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/gmaps_utility/markerclusterer_v3.js?v=1', 'text/javascript');      
       
        $this->view->headScript()->appendFile('https://maps.google.com/maps/api/js?sensor=false','text/javascript');
         $this->view->headScript()->appendFile('/js/index.js?v=56', 'text/javascript');
         //$this->view->loadmap = 'onunload="GUnload();"';

        $cliente = new Hm_Cli_Cliente();
        $prop = new Dgt_Propiedad();

        $fb = Dgt_Fb::getInstance();
        $appusers = $fb->getFriendsApp();

        if (is_array($appusers)) {
            /* foreach($appusers as $index => $user){
              $user["first_name"] = implode("<br/>",$cliente->SectionString($user["first_name"]));
              $user["last_name"] = implode("<br/>",$cliente->SectionString($user["last_name"]));
              $appusers[$index]=$user;
              } */
            $this->view->appusers = $appusers;
        } else {
            $this->view->appusers = '';
        }
        $totalappusers = count($fb->getFriendsAppAll());
        $this->view->totalAppUsers = $totalappusers > 0 ? $totalappusers : 0;


        $cate = Hm_Cat_Categoria::getInstance();
        $language = Zend_Registry::get('language');

        $categories = $cate->GetCat();
        $test = $cate->GetCat();
        $this->view->categories = $categories;
        // Recuperar todos los atributos por las categorias.
        $form = new Zend_Form();

        $atrributes = array();
        if (count($categories) > 0) {
            foreach ($categories as $categoryId => $categoryName) {
                $subForm = new Zend_Form_SubForm();
                $subForm->setName(str_replace(" ", "", $categoryName));
                $subForm->setLegend($categoryName);
                $curAttributes = &$cate->GetAtributes($categoryId);

                // Por cada atributo.
                // Por cada categoria, agregar los atributos en grupos de despliegue.
                if (count($curAttributes) > 0) {
                    $attributes = array();
                    foreach ($curAttributes as $attribute) {
                        $typeAttribute = &$attribute["tipo"];
                        $idAttribute = &$attribute["codigo"];
                        $nameAttribute = &$attribute["nombre"];

                        $attributes[] = $idAttribute;
                        // Recuperar el tipo de atributo a renderizar
                        $elements = array();

                        if ($typeAttribute == "Range") {
                            //Test Combo 
                            //Input Hide para el Filtro Automatico
                            // Minimo
                            $min = new Zend_Form_Element_Select("min_" . $idAttribute);
                            $min->setLabel($nameAttribute);
                            //echo ($nameAtribute);
                            $min->setValue("MIN");
                            $min->setMultiOptions(array('' => 'Any', '1' => '1+', '2' => '2+', '3' => '3+', '4' => '4+', '5' => '5+'));
                            //$min->setRequired(true)->addValidator('NotEmpty', true);
                            $min->setAttribs(array("class" => "labelHighlight", "size" => "1", "style" => "width:70px;align:left;"));
                            $min->clearDecorators();
                            $min->addDecorator("ViewHelper");
                            $subForm->addElement($min);


                            $elements[] = "min_" . $idAttribute;
                        } else if ($typeAttribute == "Check") {
                            $opt = new Zend_Form_Element_Checkbox("value_" . $idAttribute);
                            $opt->setValue("true");
                            $opt->setCheckedValue("S");
                            $opt->setUncheckedValue("N");
                            $opt->clearDecorators();
                            $opt->addDecorator("ViewHelper");
                            $subForm->addElement($opt);
                            $elements[] = "value_" . $idAttribute;
                        } else if ($typeAttribute == "Like") {
                            $value = new Zend_Form_Element_Text("value_" . $idAttribute);
                            $value->setValue("");
                            $value->setAttribs(array("class" => "labelHighlight", "size" => "5", "style" => "width:30px;"));
                            $value->clearDecorators();
                            $value->addDecorator("ViewHelper");
                            $subForm->addElement($value);
                            $elements[] = "value_" . $idAttribute;
                        }
                        // Agregarlas al subform en un display group.
                        if (count($elements) > 0) {
                            $name2 = str_replace(" ", "", $nameAttribute);
                            $subForm->addDisplayGroup(
                                    $elements, $name2, array("codigo" => $idAttribute)
                            );
                        }
                    }
                }
                $decorator = new Forms_Decorator_Attributes();
                $subForm->clearDecorators();
                $subForm->addDecorator($decorator);
                $form->addSubForm($subForm, $categoryName);
            }
        }
        $this->view->form = $form;
        $this->view->attributes = $attributes;

        
        $searchTypes = Hm_Cat_Enlistamiento::GetListingTypes();
        $searchTypes = array_merge(array($this->view->translate("LABEL_SEARCH_FILTER_LISTING_TYPE")), $searchTypes);
        $listing = "<li valor='' class='active'><a href='#tab1'><i18n>LABEL_SEARCH_FILTER_LISTING_TYPE</i18n></a></li>";
        $i = 1;
        foreach ($searchTypes as $index => $type) {
            $listing .= "<li valor='" . $index . "' ><a href='#tab" . $i . "'>" . $type . "</a></li>";
            $i++;
        }

        // $this->view->searchTypes = $listing;
        $this->view->searchTypes = $searchTypes;
        //$this->view->brokers = $cliente->GetConsultaClientes();
        //$this->view->totalBrokers = count($cliente->GetAllFacebookBrokers());
        $this->view->UMSuperficies = array("ft2" => "ft2", "mt2" => "mt2");

        $this->view->web_path = $cfg['fb_app_url'];
        
    }
 
    /** 
     * Recupera la informacion a mostrarse en el google map
     * cuando se busca por localidad
     */  
    public function estatetogmAction() {
        //echo "TESTHD";exit();
        $f = new Zend_Filter_StripTags();
        $p = new Hm_Pro_Propiedad();

        if ($this->getRequest()->isPost()) {
          $country = $f->filter($this->_request->getParam('country',null));
            $city = $f->filter($this->_request->getParam('city',null));
            $category = $f->filter($this->_request->getParam('category',null));
            $type = $f->filter($this->_request->getParam('type',null));
            $min_price = $f->filter($this->_request->getParam('min_price',null));
            $max_price = $f->filter($this->_request->getParam('max_price',null));
            $max_area = $f->filter($this->_request->getParam('max_area',null));
            $min_area = $f->filter($this->_request->getParam('min_area',null));
            $uni_area = $f->filter($this->_request->getParam('uni_area',null));
            $max_area_lote = $f->filter($this->_request->getParam('max_area_lote',null));
            $min_area_lote = $f->filter($this->_request->getParam('min_area_lote',null));
            $uni_area_lote = $f->filter($this->_request->getParam('uni_area_lote',null));
            $parameters_raw = $f->filter($this->_request->getParam('parameters',null));
            $coordinates_raw = $f->filter($this->_request->getParam('coordinates',null));

            $start = $f->filter($this->_request->getParam('start', 0));
            $limit = $f->filter($this->_request->getParam('limit', 10));
            $coordinates = null;
            if ($coordinates_raw != null) {
                if (get_magic_quotes_gpc()) {
                    $coordinates_raw = stripslashes($coordinates_raw);
                }
                $coordinates = Zend_Json_Decoder::decode($coordinates_raw);
            }
         
          
            $propiedades = $p->FilterStatesOnDir($country,$city,$category,$type,$min_price,$ma_price,$max_area,$min_area,$uni_area,$max_area_lote,$min_area_lote,$uni_area_lote,$extra_filters,$coordinates, $start, $limit);

            if (!empty($propiedades)) {
                echo '{"success":true, "results":' . count($propiedades) . ', "rows": ' . Zend_Json::encode($propiedades) . '}';
            } else {
                echo '{"success":true, "results":0, "rows": []}';
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }

    /**
     * Recupera el listado de las fotos destacadas
     */
    public function fotosdestacadaAction() {

        $f = new Zend_Filter_StripTags();
        $p = new Hm_Pro_Propiedad();

        if ($this->getRequest()->isPost()) {
            $country = $f->filter($this->_request->getParam('country', null));
            $city = $f->filter($this->_request->getParam('city', null));
            $coordinates_raw = $f->filter($this->_request->getParam('coordinates', null));

            $start_raw = $this->_request->getParam('start', 0);
            $limit_raw = $this->_request->getParam('limit', 6);

            $start = $f->filter($start_raw);
            $limit = $f->filter($limit_raw);
            $coordinates = null;
            if ($coordinates_raw != null) {
                if (get_magic_quotes_gpc()) {
                    $coordinates_raw = stripslashes($coordinates_raw);
                }
                $coordinates = Zend_Json_Decoder::decode($coordinates_raw);
            }

            $fotos = $p->GetFotosDestacadas($country, $city, $coordinates, $start, $limit);

            try {
                if (count($fotos) > 0) {
                    echo '{"success":true, "results":' . count($fotos) . ', "rows":' . Zend_Json::encode($fotos) . '}';
                } else {
                    echo '{"success":true, "results":0, "rows":[]}';
                }
            } catch (Excepcion $e) {
                echo $e->getMessage();
            }
        } else {
            echo '{"success":"false", "msg":"method get no allowed"}';
        }
        exit();
    }

    /**
     * Recupera los atributos de la categoria seleccionada
     */
    public function getatributosAction() {


        $f = new Zend_Filter_StripTags();

        if ($this->getRequest()->isPost()) {
            $categoryId = $f->filter($this->_request->getParam('category', null));

            $c = Hm_Cat_Categoria::getInstance();
            $attributes = $c->GetAtributes($categoryId);

            if (!empty($attributes)) {
                echo '{"success":true, "results":' . count($attributes) . ', "rows":' . Zend_Json::encode($attributes) . '}';
            } else {
                echo '{"rows":[]}';
            }
        } else {
            echo '{"success":"false", "msg":"method get no allowed"}';
        }
        exit();
    }

    /**
     * Recupera el listado de las fotos destacadas
     */
    public function fotosfiltradasAction() {

        $f = new Zend_Filter_StripTags();
        $p = new Hm_Pro_Propiedad();

        if ($this->getRequest()->isPost()) {
            $sPage = $f->filter($this->_request->getParam('sPage', null)); //pagina seleccionada en paginacion propiedades
            $cPage = $f->filter($this->_request->getParam('cPage', null)); //pagina actual en paginacion propiedades
            $category = $f->filter($this->_request->getParam('category', null));
            $type = $f->filter($this->_request->getParam('type', null));
            $min_price = $f->filter($this->_request->getParam('min_price', null));
            $max_price = $f->filter($this->_request->getParam('max_price', null));
            $max_area = $f->filter($this->_request->getParam('max_area', null));
            $min_area = $f->filter($this->_request->getParam('min_area', null));
            $uni_area = $f->filter($this->_request->getParam('uni_area', null));
            $max_area_lote = $f->filter($this->_request->getParam('max_area_lote', null));
            $min_area_lote = $f->filter($this->_request->getParam('min_area_lote', null));
            $uni_area_lote = $f->filter($this->_request->getParam('uni_area_lote', null));
            $parameters_raw = $f->filter($this->_request->getParam('parameters', null));
            $coordinates_raw = $f->filter($this->_request->getParam('coordinates', null));

            $start_raw = $this->_request->getParam('start', 0);
            $limit_raw = $this->_request->getParam('limit', 5);
            $start = $f->filter($start_raw);
            $limit = $f->filter($limit_raw);

            $start = $f->filter($start_raw);
            $limit = $f->filter($limit_raw);


            $parameters = array();
            if ($parameters_raw != null) {
                if (get_magic_quotes_gpc()) {
                    $parameters_raw = stripslashes($parameters_raw);
                }
                $parameters = Zend_Json_Decoder::decode($parameters_raw);
            }

            $coordinates = null;
            if ($coordinates_raw != null) {
                if (get_magic_quotes_gpc()) {
                    $coordinates_raw = stripslashes($coordinates_raw);
                }
                $coordinates = Zend_Json_Decoder::decode($coordinates_raw);
            }
            
            if ($min_area == '' && $max_area == '')
                $uni_area = '';
            $rsPics = $p->GetFotosFiltradas($category, $type, $min_price, $max_price, $max_area, $min_area, $uni_area, $max_area_lote, $min_area_lote, $uni_area_lote, $parameters, $coordinates, $start, $limit);
            $fotos=$rsPics->rows;
            $totalFotos = $rsPics->results;

            if (!empty($fotos)) {
                echo '{"success":true, "cPage":' . $cPage . ', "sPage":' . $sPage . ', "total":' . $totalFotos . ', "results":' . count($fotos) . ', "rows":' . Zend_Json::encode($fotos) . '}';
            } else {
                echo '{"success":true, "cPage":0, "sPage":0, "total":0, "results":0, "rows":[]}';
            }
        } else {
            echo '{"success":"false", "msg":"method get no allowed"}';
        }

        exit();
    }
      
    public function brokersbyareaAction() {

        $f = new Zend_Filter_StripTags();
        $c = new Hm_Cli_Cliente();

        if ($this->getRequest()->isPost()) {

            $coordinates_raw = $f->filter($this->_request->getParam('coordinates', null));

            $parameters = array();
            if ($parameters_raw != null) {
                if (get_magic_quotes_gpc()) {
                    $parameters_raw = stripslashes($parameters_raw);
                }
                $parameters = Zend_Json_Decoder::decode($parameters_raw);
            }

            $coordinates = null;
            if ($coordinates_raw != null) {
                if (get_magic_quotes_gpc()) {
                    $coordinates_raw = stripslashes($coordinates_raw);
                }
                $coordinates = Zend_Json_Decoder::decode($coordinates_raw);
            }

            $brokers = $c->GetBrokersByArea($coordinates);

            if (!empty($brokers)) {

                echo '{"success":true, "results":' . count($brokers) . ', "rows": ' . Zend_Json::encode($brokers) . '}';
            } else {
                echo '{"rows": []}';
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }

    /**
     * Recupera la informacion a mostrarse en el listao de resultados.
     * Dentro de la grid de resultados.
     */
    public function getmarksAction() {

        $f = new Zend_Filter_StripTags();
        $p = new Dgt_Propiedad();
        if ($this->getRequest()->isPost()) {
            $dir = $f->filter($this->_request->getParam('dir', null));
            $pais = $f->filter($this->_request->getParam('pais', null));
            $city = $f->filter($this->_request->getParam('ciudad', null));
            //$compra = $f->filter($this->_request->getParam('buy', 0));
            //$alquiler = $f->filter($this->_request->getParam('rent', 0));
            $tipo = $f->filter($this->_request->getParam('tipo', null));

            $min_price = $f->filter($this->_request->getParam('min_price', 0));
            $min_price = is_numeric($min_price) ? $min_price : 0;

            $max_price = $f->filter($this->_request->getParam('max_price', 0));
            $max_price = is_numeric($max_price) ? $max_price : 0;

            $min_beds = $f->filter($this->_request->getParam('min_beds', 0));
            $min_beds = is_numeric($min_beds) ? $min_beds : 0;

            $min_baths = $f->filter($this->_request->getParam('min_baths', 0));
            $min_baths = is_numeric($min_baths) ? $min_baths : 0;

            $min_area = $f->filter($this->_request->getParam('min_area', 0));
            $min_area = is_numeric($min_area) ? $min_area : 0;

            $max_beds = $f->filter($this->_request->getParam('max_beds', 0));
            $max_beds = is_numeric($max_beds) ? $max_beds : 0;

            $max_baths = $f->filter($this->_request->getParam('max_baths', 0));
            $max_baths = is_numeric($max_baths) ? $max_baths : 0;

            $max_area = $f->filter($this->_request->getParam('max_area', 0));
            $max_area = is_numeric($max_area) ? $max_area : 0;

            $cat = $f->filter($this->_request->getParam('cat', null));


            $start_raw = $this->_request->getParam('start', 0);
            $limit_raw = $this->_request->getParam('limit', 10);

            $start = $f->filter($start_raw);
            $limit = $f->filter($limit_raw);


            $x0 = $f->filter($this->_request->getParam('minX', 0));
            $y0 = $f->filter($this->_request->getParam('minY', 0));
            $x1 = $f->filter($this->_request->getParam('maxX', 0));
            $y1 = $f->filter($this->_request->getParam('maxY', 0));


            $propiedades = $p->filter($pais, trim($city), $dir, $tipo, $min_price, $max_price, $min_beds, $min_baths, $min_area, $max_beds, $max_baths, $max_area, $cat, $x0, $y0, $x1, $y1, $start, $limit);
            //$propiedades = $p->filter($pais, trim($city), $dir, $alquiler, $compra, $min_price, $max_price, $beds, $baths, $area, $cat, $x0, $y0, $x1, $y1);
            $prop_count = $p->filter($pais, trim($city), $dir, $tipo, $min_price, $max_price, $min_beds, $min_baths, $min_area, $max_beds, $max_baths, $max_area, $cat, $x0, $y0, $x1, $y1);
            if (!empty($propiedades)) {
                echo '{"success":true, "results":' . count($prop_count) . ', "rows": ' . Zend_Json::encode($propiedades) . '}';
            } else {
                // si ciudad falla, buscar por pais.
                $city = null;
                $propiedades = $p->filter($pais, $city, $dir, $tipo, $min_price, $max_price, $min_beds, $min_baths, $min_area, $max_beds, $max_baths, $max_area, $cat, $x0, $y0, $x1, $y1, $start, $limit);
                //$propiedades = $p->filter($pais, $city, $dir, $alquiler, $compra, $min_price, $max_price, $beds, $baths, $area, $cat, $x0, $y0, $x1, $y1);
                $prop_count = $p->filter($pais, $city, $dir, $tipo, $min_price, $max_price, $min_beds, $min_baths, $min_area, $max_beds, $max_baths, $max_area, $cat, $x0, $y0, $x1, $y1);
                if (!empty($propiedades)) {
                    echo '{"success":true, "results":' . count($prop_count) . ', "rows": ' . Zend_Json::encode($propiedades) . '}';
                } else {
                    echo '{"rows": []}';
                }
                //echo '{"success":"false", "msg": "no data"}';
                //echo '{"rows": []}';
            }
            exit();
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
    }

    /**
     * Recupera el listado de amigos.
     */
    public function showallfriendsAction() {
        $this->view->headLink()->appendStylesheet('/css/search.css');
        $this->view->headLink()->appendStylesheet('/css/style_menu.css');
        $this->view->linkactual = $this->getRequest()->getControllerName();
        $fb = Dgt_Fb::getInstance();
        $f = $fb->getFriendsAppAll();
        $this->view->friends = $f;
    }

    /**
     * Recupera el listado de amigos.
     */
    public function showallbrokersAction() {
        $this->view->headLink()->appendStylesheet('/css/search.css');
        $this->view->headLink()->appendStylesheet('/css/style_menu.css');


        $f = new Zend_Filter_StripTags();
        $cliente = new Hm_Cli_Cliente();

        if ($this->getRequest()->isGet()) {
            $coordinates = array();
            $coordinates["maxX"] = $_GET["maxX"];
            $coordinates["maxY"] = $_GET["maxY"];
            $coordinates["minX"] = $_GET["minX"];
            $coordinates["minY"] = $_GET["minY"];
        }

        $this->view->brokers = $cliente->GetBrokersByArea($coordinates);
    }

    /**
     * Regresa la posicion geografica donde poner el google map por primera vez
     */
    public function centermapAction() {
        // @todo, check fb-params.
        //$ip2 = "$_SERVER[REMOTE_ADDR]";   
        //echo str_replace(" ", "_", "Thi is the test", 3);
        $ip = ($this->_request->getClientIP()) ? $this->_request->getClientIP() : $_SERVER[REMOTE_ADDR];

        $location = Dgt_GeoData::getLocation($ip);
        if (!$location || count($location) <= 0) {
            $service = sprintf("https://api.ipinfodb.com/v3/ip-city/?key=932bba7f501e4c498d6fbb851f9133eec3d3f2b8520b4db8c6f05dfd77e8e068&ip=%s&format=xml", $ip);
            $xmlservice = simplexml_load_file($service);

            $ipLocation = new stdClass();
            $ipLocation->cc = (string) $xmlservice->countryCode;
            $ipLocation->city = (string) $xmlservice->cityName;
            $ipLocation->lat = (string) $xmlservice->latitude;
            $ipLocation->lng = (string) $xmlservice->longitude;
            $location[0] = $ipLocation;
        }

        //{"success":"true","data": [{"cc":"US","city":"Mountain View","zip":"94043","lat":"37.4192","lng":"-122.0574"}]}
        //Zend_Debug::dump($location);
        if (!empty($location)) {
            echo '{"success":"true","data": ' . Zend_Json::encode($location) . '}';
        } else {
            echo '{"success":"false"}';
        }
        exit();
    }

}

?>