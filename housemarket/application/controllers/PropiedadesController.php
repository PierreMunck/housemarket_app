<?php

class PropiedadesController extends MainController {

    protected $_flashMessenger = null;
    protected $_redirector = null;
    protected $_applicationParameter = null;
    
    protected $LabelNavigation = 'propiedades';

    public function init() {
    	parent::init();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
        $this->view->web_path = $this->cfg['fb_app_url'];
    }

    public function preDispatch() {
        
    }

    /**
    *
    * fonction de retour de label de navigation courant
    */
    public function getCurrentLabelnav() {
    	return $this->LabelNavigation;
    }
    
    public function showAction() {
    	
    	$this->currentActionNav = 'propiedades';
    	
    	$this->navAction();
    	
        header("Connection: close");
        $this->_flashMessenger->clearMessages();
        $signed = Zend_Registry::get('signed');
        try {
            $fb = Dgt_Fb::getInstance();
            if ($this->fbDgt->hasPermission($this->cfg['facebook_perms'])) {
                $loginUrl = $fb->facebook->getLoginUrl(array('scope' => $this->cfg['facebook_perms']));
                echo "<script>top.location.href='" . $loginUrl . "';</script>";
                exit();
            }
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la session.");
            die;
        }

        $parasolicitud = $this->view->web_path . "profesional";
        $this->view->botones = "<div class='button_group' style='float:right;height: 17px;' align='right'> <a href='$parasolicitud' class='button' id='bt2' target='_top' name='bt' style='margin-top: 0px;font-size: 11px'><i18n>LABEL_REQUEST_LISTING_UPLOAD_NEW</i18n></a> <br/></div>";

        $this->view->uid = $fb->get_uid();
        $this->view->appusers = $fb->getFriendsApp();

        $this->view->headTitle('Propiedades');
        //$this->view->loadmap = 'onunload="GUnload();"';

        $google_api_key = $this->cfg['google_api_key'];
        $localLangague = Zend_Registry::get('Zend_Locale');

        /**
         * Ubicacion por IP
         */
        $ip = $this->_request->getClientIP();
        $location = Dgt_GeoData::getLocation($ip);
        if ($location) {
            $location = $location[0];
        }
        if (empty($location)) {
            $location->cc = strtoupper($signed['user']['country']);
        }
        
        $c = new Hm_Cat_Pais();
        $currenciesByCountry = $c->GetCurrenciesByCountry();
        
        $currenciesByCountryArr = array();
        foreach ($currenciesByCountry as $value) {
            $currenciesByCountryArr[$value->CountryCode] = array(
                'CountryName' => $value->CountryName,
                'CurrencyCode' => $value->CurrencyCode,
            );
        }
        
        if(isset($currenciesByCountryArr[$location->cc])) {
            $location->codigoModeda = $currenciesByCountryArr[$location->cc]['CurrencyCode'];
            $location->nombrePais = $currenciesByCountryArr[$location->cc]['CountryName'];
        }
        
        $this->view->location = $location;
        
        /**
         * Version Google Maps 3.0
         */
        $this->view->language = $localLangague;
        $this->view->headLink()->appendStylesheet("https://code.google.com/apis/maps/documentation/javascript/examples/default.css");
        $this->view->headScript()->appendFile('https://maps.google.com/maps/api/js?v=3&sensor=false&language=' . $localLangague . '&region=' . $location->cc, 'text/javascript');

        $this->view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/jquery.forms.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/lang/' . $localLangague . '.js', 'text/javascript');
        $this->view->headScript()->appendFile("/js/jquery.scrollTo.js", "text/javascript");
        $this->view->headScript()->appendFile("/js/validate/jquery.validate1.8.1.min.js", "text/javascript");
        $this->view->headScript()->appendFile("/js/agregar_prop.js?v=" . rand(1, 100), "text/javascript");


        // minified and concatenated: general.css, tabs.css, style_form
        $this->view->headLink()->appendStylesheet('/css/show.min.css');

        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $cate = Hm_Cat_Categoria::getInstance();
        $categories = $cate->GetCat();
        $categories[""] = ($language == "ES") ? "Elegir" : "Choose";

        $this->view->categories = $categories;
        $langunidad = $this->view->translate("LABEL_UNIT");
        $this->view->UMSuperficies = array("" => $langunidad, "vr2" => "vr2", "mt2" => "mt2", "ft2" => "ft2");
        $estate = new Hm_Pro_Propiedad();
        $property = new Hm_Pro_Propiedad();
        $category = Hm_Cat_Categoria::getInstance();
        $this->view->isNew = "S";
        $this->view->registro = "S";
        $this->view->publicado = "N";
        $this->view->estate = $estate;
        $this->view->web_path = $this->cfg['fb_app_url'];
        $this->view->url_path = $this->cfg['local_app_url'];
        $this->view->application_url = $this->cfg['fb_app_url'] . 'profesional/currenciesbycountry';
        
        // Determinar si es nueva o no.
        $f = new Zend_Filter_StripTags();
        $estateId = null;
        $this->view->property = $property;
        if ($this->getRequest()->isPost()) {

            // Intentar recuperar el parametro de la propiedad.
            $estateId = $f->filter($this->getRequest()->getParam("id", null));
            $this->view->isPublicada = $f->filter($this->getRequest()->getParam("publicada", null));


            // Recuperar la propiedad a partir del id proporcionado.
            $rs = $estate->find($estateId);
            if ($rs != null && $rs->count() > 0) {
                $estate = $rs->current();
                $uid = Zend_Registry::get('uid');
                if ($estate->Uid == $uid || in_array($uid, HMUtils::$developers_uid)) { // El usuario es propietario
                    $this->view->estate = $estate;
                    $this->view->isNew = "N";
                    $propResult = Hm_Pro_Propiedad::ObtenerDatosProPropiedad($estateId);
                    $this->view->property = $propResult[0];
                    
                    if(isset($currenciesByCountryArr[$this->view->property->ZipCode])) {
                        $this->view->property->nombrePais = $currenciesByCountryArr[$this->view->property->ZipCode]['CountryName'];
                    }

                    $categoria = Hm_Cat_Categoria::getInstance();
                    $categoria = $categoria->find($estate->CodigoCategoria);

                    // Recuperar todos los atributos dada la propiedad.
                    $atributos = $categoria->current()->findHm_Cat_AtributoCategoria();
                    $atributosInfo = array();
                    foreach ($atributos as $atributo) {
                        $atributosInfo[$atributo->CodigoAtributo] = $atributo->CampoDestino;
                    }

                    // Recuperar los atributos / valores asociados a la propiedad.
                    $attrEstate = $estate->findHm_Pro_AtributoPropiedad();
                    $attrEstateSelected = array();
                    foreach ($attrEstate as $attrSelected) {
                        switch ($atributosInfo[$attrSelected->CodigoAtributo]) {
                            case "ValorEntero":
                                $attrEstateSelected[$attrSelected->CodigoAtributo] = $attrSelected->ValorEntero;
                                break;
                            case "ValorDecimal":
                                $attrEstateSelected[$attrSelected->CodigoAtributo] = $attrSelected->ValorDecimal;
                                break;
                            case "ValorS_N":
                                $attrEstateSelected[$attrSelected->CodigoAtributo] = $attrSelected->ValorS_N;
                                break;
                            case "ValorCaracter":
                                $attrEstateSelected[$attrSelected->CodigoAtributo] = $attrSelected->ValorCaracter;
                                break;
                        }
                    }

                    $this->view->attrEstate = $attrEstateSelected;

                    // Recuperar todos los beneficios de la propiedad
                    $benEstate = $property->GetBeneficiosByProperty($estate->CodigoPropiedad);
                    $this->view->benEstate = array();
                    foreach ($benEstate as $benefitEstate) {
                        $this->view->benEstate[] = $benefitEstate->CodigoBeneficio;
                    }

                    // Recuperar todos los atributos por categoria.

                    $attributes = new Hm_Cat_AtributoIdioma();
                    $selectAttrib = $attributes->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
                    $selectAttrib->setIntegrityCheck(false)
                            ->where($attributes->getAdapter()->quoteInto("CodigoIdioma = ?", $language))
                            ->join("catatributo", "catatributo.CodigoAtributo = catatributoidioma.CodigoAtributo")
                            ->join("catatributocategoria", "catatributocategoria.CodigoAtributo = catatributo.CodigoAtributo")
                            ->where("catatributocategoria.CodigoCategoria = ?", $categoria->current()->CodigoCategoria);

                    $rsAttributes = $attributes->fetchAll($selectAttrib);

                    $this->view->rsAttr = $rsAttributes;

                    // Recuperar todos los beneficios por categoria.
                    $benefit = new Hm_Cat_BeneficioIdioma();
                    $select = $benefit->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
                    $select->setIntegrityCheck(false)
                            ->where($benefit->getAdapter()->quoteInto("CodigoIdioma = ?", $language))
                            ->join("catbeneficio", "catbeneficio.CodigoBeneficio=catbeneficioidioma.CodigoBeneficio")
                            ->join("catbeneficiocategoria", "catbeneficiocategoria.CodigoBeneficio = catbeneficio.CodigoBeneficio")
                            ->where("catbeneficiocategoria.CodigoCategoria = ?", $categoria->current()->CodigoCategoria);

                    $rsBenefits = $benefit->fetchAll($select);

                    $this->view->rsBen = array();
                    if ($rsBenefits->count() > 0) {
                        foreach ($rsBenefits as $rsBenefit) {
                            $this->view->rsBen[$rsBenefit->CodigoBeneficio] = $rsBenefit->ValorIdioma;
                        }
                    }

                    // Recuperar todas las fotos de la propiedad.
                    $this->view->rsPhotos = array();
                    // Recuperar las fotos.
                    $findPhoto = new Hm_Pro_Foto();
                    $rsPhotos = $findPhoto->fetchAll(
                            $findPhoto->select()
                                    ->where('CodigoPropiedad = ?', $estateId)
                                    ->order('PROFotoID')
                    );

                    if ($rsPhotos != null && $rsPhotos->count() > 0) { // La propiedad tiene fotos
                        $this->view->photoID = $rsPhotos->current()->PROFotoID;
                        foreach ($rsPhotos as $photo) {
                            if ($photo->Principal === chr(0x00)) { // Si no es una foto destacada
                                $this->view->rsPhotos["otras"][] = $photo;
                            } else { // Si es Principal agregamela a destacada
                                $this->view->rsPhotos["princ"] = $photo;
                            }
                        }
                    }

                    if ($estate->CodigoEdoPropiedad == 2) {
                        $this->view->publicado = "S";
                    }
                }
            }
        }
        $isNew = $this->view->isNew == "S" ? true : false;
        $this->view->totalPhotos = $this->GetAllowedTotalPhotos($isNew, $estateId);
    }

    public function favoritoAction() {
    
    	$this->currentActionNav = 'propiedades';
    	$this->LabelNavigation = 'favorito';
    	
    	$this->navAction();
    
    	$this->view->headLink()->appendStylesheet("/css/propiedad.css");
    	header("Connection: close");
    	$this->_flashMessenger->clearMessages();
    	$signed = Zend_Registry::get('signed');
    	try {
    		$fb = Dgt_Fb::getInstance();
    		if ($this->fbDgt->hasPermission($this->cfg['facebook_perms'])) {
    			$loginUrl = $fb->facebook->getLoginUrl(array('scope' => $this->cfg['facebook_perms']));
    			echo "<script>top.location.href='" . $loginUrl . "';</script>";
    			exit();
    		}
    	} catch (Exception $e) {
    		Zend_Debug::dump("Expiracion de la session.");
    		die;
    	}
    
    	$uid = $fb->get_uid();
    	$this->view->uid = $uid;
    
    	$this->view->headTitle('Propiedades Favoritas');
    	
    	// get list de las propiedades favoritas
    	$favorito = new Hm_Pro_Favorito($uid);
    	$listpropiedad = $favorito->getlistByUid();
    	// trasform en array
    	$propiedadesId = array();
    	foreach ($listpropiedad as $propiedadfavorito){
    		$propiedadesId[] = $propiedadfavorito->CodigoPropiedad;
    	}
    	$propiedades = new Hm_Pro_Propiedad();
    	$datos = $propiedades->ObtenerDatosProPropiedad($propiedadesId);
    	$listdatos = array();
    	foreach ($datos as $propiedad){
    		$propiedad->lable_cuarto = $this->view->translate('LABEL_CUARTO');
    		$listdatos[] = get_object_vars($propiedad);
    	}
    	
    	if(empty($listdatos)){
    		$this->view->body = $this->view->translate('MSG_NO_TIENE_FAVORITO');
    	}else{
    		$this->view->body = $this->view->partialLoop('propiedades/block_propiedad.phtml',$listdatos);
    	}
    	$this->view->comparte = $favorito->comparteFavorito();
    	
    	$localLangague = Zend_Registry::get('Zend_Locale');
    }
    
    public function favoritosamigosAction() {
    	
    	$this->currentActionNav = 'propiedades';
    	$this->LabelNavigation = 'favorito';
    
    	$this->navAction();
    
    	$this->view->headLink()->appendStylesheet("/css/propiedad.css");
    	header("Connection: close");
    	$this->_flashMessenger->clearMessages();
    	$signed = Zend_Registry::get('signed');
    	try {
    		$fb = Dgt_Fb::getInstance();
    		if ($this->fbDgt->hasPermission($this->cfg['facebook_perms'])) {
    			$loginUrl = $fb->facebook->getLoginUrl(array('scope' => $this->cfg['facebook_perms']));
    			echo "<script>top.location.href='" . $loginUrl . "';</script>";
    			exit();
    		}
    	} catch (Exception $e) {
    		Zend_Debug::dump("Expiracion de la session.");
    		die;
    	}
    
    	$parasolicitud = $this->view->web_path . "profesional";
    	$this->view->botones = "<div class='button_group' style='float:right;height: 17px;' align='right'> <a href='$parasolicitud' class='button' id='bt2' target='_top' name='bt' style='margin-top: 0px;font-size: 11px'><i18n>LABEL_REQUEST_LISTING_UPLOAD_NEW</i18n></a> <br/></div>";
    
    	$uid = $fb->get_uid();
    	$this->view->uid = $uid;
    	
    
    	$this->view->headTitle($this->view->translate('TITLE_FAVORITO'));
    
    	// get list de las propiedades favoritas
    	$favorito = new Hm_Pro_Favorito($uid);
    	
    	// get list de los amigos en la applicacion
    	$listaamigos = $fb->getFriendsAppAll();
    	
    	// reducir la lista de amigos a los que comparte Favorito
    	$favorito->filtroUserPermitFavorito($listaamigos);
    	
    	// creacion del selector
    	$this->view->selectoptionamigos = array('0' => $this->view->translate('OPTION_ALL_AMIGO'));
    	foreach ($listaamigos as $amigo){
    		$this->view->selectoptionamigos[$amigo['uid']] = $amigo['first_name'] ." ". $amigo['last_name'];
    	}
    	
    	// en caso de amigo especial reducir la lista de amigos
    	$amigoUid = null;
    	$this->view->amigoUid = 0;
    	if(isset($_POST['amigoUid']) && $_POST['amigoUid'] !=0 ){
    		$amigoUid = $_POST['amigoUid'];
    		$this->view->amigoUid = $amigoUid;
    		foreach ($listaamigos as $key => $amigo) {
    			if($amigo['uid'] != $amigoUid){
    				unset($listaamigos[$key]);
    			}
    		}
    	}
    	
    	$favorito->filtroUserpropiedadFavorito($listaamigos);
    	
    	$listpropiedad = $favorito->getlistByUid();
    	$fb->getFriendsApp();
    	// trasform en array
    	$propiedadesId = array();
    	foreach ($listpropiedad as $propiedadfavorito){
    		$propiedadesId[] = $propiedadfavorito->CodigoPropiedad;
    	}
    	$propiedades = new Hm_Pro_Propiedad();
    	$datos = $propiedades->ObtenerDatosProPropiedad($propiedadesId);
    	$listdatos = array();
    	foreach ($datos as $propiedad){
    		$propiedad->commun = array();
    		foreach ($listaamigos as $amigo) {
    			if(in_array($propiedad->CodigoPropiedad,$amigo['propiedadfavorita'])){
    				$propiedad->commun[] = $amigo;
    			}
    		}
    		$listdatos[] = get_object_vars($propiedad);
    	}
    	if(empty($listdatos)){
    		$this->view->body = "<span class=\"messageprincipal\"><i18n>MSG_NO_TIENE_PROPIEDAD_EN_COMMUN</i18n></span>";
    	}else{
    		$this->view->body = $this->view->partialLoop('propiedades/block_propiedad.phtml',$listdatos);
    	}
    
    
    	$localLangague = Zend_Registry::get('Zend_Locale');
    }
    
    public function compartirfavoritoAction() {
    	$userId = Zend_Registry::get('uid');
    	$favorito = new Hm_Pro_Favorito($userId);
    	$favorito->compartirFavorito();
    
    	//@TODO 
    	// clear cache
    	/*
    	$cache = new HmCache();
    	$param = array();
    	$cache->clear('/propiedades/favorito',$param);
    	*/
    	// Redirigir al perfil
    	$redirect = '/propiedades/favorito';
    	if(isset($_GET['signed_request'])){
    		$redirect .= '?signed_request='. $_GET['signed_request'];
    	}
    	$this->_redirect($redirect);
    }
    
    public function cerrarcompartirfavoritoAction() {
    	$userId = Zend_Registry::get('uid');
    	$favorito = new Hm_Pro_Favorito($userId);
    	$favorito->cerrarcompartirFavorito();
    
    	//@TODO
    	// clear cache
    	/*
    	 $cache = new HmCache();
    	$param = array();
    	$cache->clear('/propiedades/favorito',$param);
    	*/
    	// Redirigir al perfil
    	$redirect = '/propiedades/favorito';
    	if(isset($_GET['signed_request'])){
    		$redirect .= '?signed_request='. $_GET['signed_request'];
    	}
    	$this->_redirect($redirect);
    }
    
    public function saveAction() {

        // Reiniciando los parametros de flashmessenger
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashmessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
        $status = array();
        $status['status'] = "error";
        
        $urlLocal = $this->cfg['local_app_url'];
        $urlFacebook = $this->cfg['fb_app_url'];
        
        $translator = Zend_Registry::get('Zend_Translate');

        $f = new Zend_Filter_StripTags();
        //session_start();
        $messages = array();
        if ($this->getRequest()->isPost()) {
            //Validando tamaño de fotos, el limit esta en KB
            $limit = 3;
            $limitSize = $limit * 1024 * 1024;
            try {

                foreach ($_FILES as $key => $file) {
                    if ($file['size'] > $limitSize) {
                        throw new Exception;
                    }
                }
            } catch (Exception $ex) {
                $this->_flashMessenger->addError("Images can't exceed " . $limit . "MB");
                exit();
            }
            try {
                $propiedad = new Hm_Pro_Propiedad();

                $publicado = $f->filter($this->_request->getParam("publicado"), "N");
                $new = $f->filter($this->_request->getParam("isNew"), "S");
                $isNew = $new == "S" ? true : false;

                // Recuperar el estado anterior de la propiedad.
                // Si estado es diferente de 2 y publicado es igual a S entonces, se debe poner a
                // publicar.
                if (!$isNew) {
                    $id = $this->_request->getParam("id");
                    try {
                        $rsPropiedad = $propiedad->find($id);
                        if ($rsPropiedad->count() > 0) {
                            $publicar = ($publicado == "S" && $rsPropiedad->current()->CodigoEdoPropiedad != 2) ? true : false;
                        } else {
                            $publicar = false;
                        }
                    } catch (Exception $ex) {
                        $publicar = false;
                    }
                } else {
                    $publicar = ($publicado == "S") ? true : false;
                }

                
                $result = $propiedad->SaveOrUpdate($this->_request->getParams());

                // Mensajes de validacion
                if (array_key_exists("fail", $result)) {
                    $messages = $result["reasons"];
                    $messageError = "";
                    if (is_array($messages)) {
                        $messageError .= "<ul>";
                        foreach ($messages as $field => $messageBlock) {
                            if (is_array($messageBlock)) {
                                foreach ($messageBlock as $message) {
                                    $messageError .= "<li>" . $message . "</li>";
                                }
                            } else {
                                $messageError .= "<li>" . $messageBlock . "</li>";
                            }
                        }
                        $messageError .= "</ul>";
                    } else { // Mensaje sencillo.
                        $messageError .= "<ul><li>" . $messages . "</li></ul>";
                    }
                    $this->_flashMessenger->addError("Please check your data" . $messageError);
                    //redireccionando si es correcto
                    //$this->_redirect($this->view->web_path . 'propiedades/show?' . $_SERVER['QUERY_STRING']);
                } else {
                    // Mensajes de advertencia al guardar el archivo.
                    // Revisar el campo de status y revisar si hay algun mensaje.
                    $isOk = true;
                    $warning = null;

                    $state = $result["status"];
                    $id = $result["id"];
                    if (is_array($state)) {
                        // Por el momento, solo revisaremos las fotos.
                        $photoStatus = isset($state["Album"]) ? $state["Album"] : array();
                        if (array_key_exists("fail", $photoStatus)) {
                            // Ocurrio una excepcion. Mostrarla al usuario.
                            $warning = $photoStatus["reason"];
                            $isOk = false;
                        } else {
                            if (isset($photoStatus["photos"]) && is_array($photoStatus["photos"])) {
                                $photosResult = $photoStatus["photos"];
                                if ($photosResult["fails"] === true) { // Ocurrio un error, mostrar mensajes
                                    $warning = "<ul>";
                                    if (is_array($photosResult["reasons"])) {
                                        foreach ($photosResult["reasons"] as $reasons) {
                                            if (is_array($reasons)) {
                                                foreach ($reasons as $reason)
                                                    $warning .= "<li>" . $reason . "</li>";
                                            } else {
                                                $warning .= "<li>" . $reasons . "</li>";
                                            }
                                        }
                                    } else {
                                        $warning .= "<li>" . $photosResult["reasons"] . "</li>";
                                    }
                                    $warning .= "</ul>";
                                    $isOk = false;
                                }
                            }
                        }
                    }

                    if ($isOk) {
                        $link = "/enlistar/perfil?valprop=" . $id;
                        $this->_flashMessenger->addSuccess($translator->getAdapter()->translate('ADD_ESTATE_SUCCESS_SAVE') . sprintf("<a href='%s'>%s</a>", $link, $translator->getAdapter()->translate('LABEL_GOTO_PROFILE')));
                        $publicada = ($publicar) ? '&publicada=1' : '&publicada=0';

                        //Extraer la info de la propiedad
                        $property = Hm_Pro_Propiedad::ObtenerDatosProPropiedad($id);
                        $objFoto = new Hm_Pro_Foto();
                        $photo = $objFoto->fetchRow(
                                $objFoto->select()
                                        ->from($objFoto,array('PROFotoID'))
                                        ->where('CodigoPropiedad = ? AND Principal = 1', $id)
                                        ->order('Principal DESC')
                        );
                        
                        $status['publicar'] = $publicar;
                        $status['status'] = "success";
                        $status['redirect'] = $urlFacebook . "mihousebook/publicacion/";
                    } else {
                        $this->_flashMessenger->addSuccess($translator->getAdapter()->translate('ADD_ESTATE_SUCCESS_SAVE'));
                        $this->_flashMessenger->addWarning("Warning" . $warning);
                        $status['status'] = "warning";
                        if ($isNew) {
                            $status['redirect'] = ($urlFacebook . 'propiedades/show?id=' . $id);
                        }
                    }
                }
            } catch (Exception $ex) {
                $status['status'] = "error";
                $this->_flashMessenger->addError("Ocurrio un error al guardar los datos de su propiedad");
                //$this->_redirect($this->view->web_path . '/propiedades/show?' . $_SERVER['QUERY_STRING']);
            }
        }
        
        $property = $property[0];
        
        $datallePrecioVenta = '';
        $datallePrecioAlquiler = '';
        $datallePrecioRemate = '';
    // Si bandera Accion === 'F' entonces la propiedad se está rematando.
    // No puede tener precio de alquiler y se usa el campo 'PrecioVenta' como
    // el precio de remante.
    if( !empty ($property->Accion) && $property->Accion !== 'F' ) {
        if( !empty ($property->PrecioVenta) ) {
            $datallePrecioVenta = ' | ' . $translator->getAdapter()->translate('LABEL_SALE') . ': '
                    . $property->MonedaSimbolo
                    . $property->PrecioVenta . ' '
                    . $property->MonedaCodidoEstandar;
        }
        
        if( !empty ($property->PrecioAlquiler) ) {
            $datallePrecioAlquiler = ' | ' . $translator->getAdapter()->translate('LABEL_RENT') . ': '
                    . $property->MonedaSimbolo
                    . $property->PrecioAlquiler . ' '
                    . $property->MonedaCodidoEstandar;
        }
    }
    else if( !empty ($property->Accion) && $property->Accion === 'F' ) {
        if( !empty ($property->PrecioVenta) ) {
            $datallePrecioRemate = ' | ' . $translator->getAdapter()->translate('LABEL_FORECLOSURE') . ': '
                    . $property->MonedaSimbolo
                    . $property->PrecioVenta . ' '
                    . $property->MonedaCodidoEstandar;
        }
    }


        $status['message'] = $this->view->flashMessenger();
        //header('Content-type: text/plain');
        $xml = simplexml_load_string("<response />");
        $xml->addChild('status', $status['status']);
        $xml->addChild('message', $status['message']);
        if (isset($property)) {
            //$property = $property[0];
// echo $propiedad->NombreCategoria; if(!empty($propiedad->datallePrecioVenta)) echo $propiedad->datallePrecioVenta; if(!empty($propiedad->datallePrecioAlquiler)) echo $propiedad->datallePrecioAlquiler; if(!empty($propiedad->datallePrecioRemate)) echo $propiedad->datallePrecioRemate; 
            $xml->addChild('location', $property->Direccion);
            $xml->addChild('title', $property->NombrePropiedad);
            $xml->addChild('href', $urlFacebook . 'enlistar/perfil?valprop=' . $id);
            $xml->addChild('caption', $property->NombreCategoria . ' ' . $datallePrecioVenta . ' ' . $datallePrecioAlquiler . ' ' . $datallePrecioRemate);
            $xml->addChild('description', '');
            $xml->addChild('image', $urlLocal . "pic" .$photo->PROFotoID. "_4.jpg");
        }
        $xml->addChild('redirect', $status['redirect']);
        header('Content-type: text/xml');
        echo $xml->asXML();
        exit();
    }

    private function GetAllowedTotalPhotos($isNew, $estateId = null) {
        $maxAllowedPhotos = 4; // Recuperar de acuerdo al maximo de photos permitidas
        if ($isNew) {
            return $maxAllowedPhotos; // Es un nuevo registro.
        } else {
            // Recuperar el total de photos.
            $propiedad = new Hm_Pro_Propiedad();
            $fotos = $propiedad->GetPhotos($estateId);
            if (count($fotos) > 0) {
                $totalPhotos = count($fotos);
            } else {
                $totalPhotos = 0;
            }
            return $maxAllowedPhotos - $totalPhotos;
        }
    }

    public function getbeneficiosAction() {
        try {
            $categoria = Hm_Cat_Categoria::getInstance();
            $f = new Zend_Filter_StripTags();

            $categoryId = $f->filter($this->_request->getParam('category'), null);

            $beneficios = $categoria->GetBeneficios($categoryId);
            if (is_array($beneficios) && count($beneficios) > 0) {
                echo '{"success":true, "results":' . count($attributes) . ', "rows": ' . Zend_Json::encode($beneficios) . '}';
            } else {
                echo '{"rows": []}';
            }
        } catch (Exception $e) {
            echo '{"rows": []}';
        }
        exit();
    }

    /**
     * Recupera los atributos de la categoria seleccionada
     */
    public function getatributosAction() {

        $f = new Zend_Filter_StripTags();

        if ($this->getRequest()->isPost()) {
            $categoryId = $f->filter($this->_request->getParam('category', null));

            $c = Hm_Cat_Categoria::getInstance();
            $attributes = $c->GetAllAtributes($categoryId);

            if (!empty($attributes)) {
                echo '{"success":true, "results":' . count($attributes) . ', "rows": ' . Zend_Json::encode($attributes) . '}';
            } else {
                echo '{"rows": []}';
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }
    
    /**
     * Obtiene una lista de código de paises con información relacionada
     * a la moneda que utiliza.
     */
    protected function getcurrencybycountryAction() {

        if ($this->getRequest()->isPost()) {
            
            $c = new Hm_Cat_Pais();
            $currenciesByCountry = $c->GetCurrenciesByCountry();
            
            $currenciesByCountryArr = array();
            foreach ($currenciesByCountry as $value) {
                $currenciesByCountryArr[$value->CountryCode] = array(
                    'CountryName' => $value->CountryName,
                    'CurrencySymbol' => $value->CurrencySymbol,
                    'CurrencyCode' => $value->CurrencyCode,
                );
            }

            if (!empty($currenciesByCountryArr)) {
                echo '{"success":true, "results":' . count($currenciesByCountryArr) . ', "rows": ' . Zend_Json::encode($currenciesByCountryArr) . '}';
            } else {
                echo '{"rows": []}';
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }

    public function addAction() {

        $db = Zend_Registry::get('db');
        $f = new Zend_Filter_StripTags();

        if ($this->getRequest()->isGet()) {

            try {
                $Precio = $f->filter($this->_request->getParam('precio'));
                $insert_data_propiedad = array(
                    'tipo_enlistamiento' => (int) trim($f->filter($this->_request->getParam('tipo_enlistamiento'))),
                    'tipo_propiedad' => trim($f->filter($this->_request->getParam('type_property'))),
                    'direccion' => trim($f->filter($this->_request->getParam('direction_propiedad'))),
                    'pais' => trim($f->filter($this->_request->getParam('country'))),
                    'estado' => trim($f->filter($this->_request->getParam('estado'))),
                    'ciudad' => trim($f->filter($this->_request->getParam('ciudad'))),
                    'area' => (int) trim($f->filter($this->_request->getParam('area'))),
                    'cuartos' => (int) trim($f->filter($this->_request->getParam('cuarto'))),
                    'banos' => (int) trim($f->filter($this->_request->getParam('bano'))),
                    'tamano_lote' => trim($f->filter($this->_request->getParam('tamano_lote'))),
                    'zip_code' => trim($f->filter($this->_request->getParam('zip_code'))),
                    'lat' => trim($f->filter($this->_request->getParam('latitud'))),
                    'lng' => trim($f->filter($this->_request->getParam('longitud'))),
                    'uid' => trim($f->filter($this->_request->getParam('userid'))),
                    'fecha_ingreso' => new Zend_Db_Expr('CURDATE()'),
                    'estatus' => 3
                );

                $tipoPrecio = $f->filter($this->_request->getParam('tipo_enlistamiento'));


                if ($tipoPrecio == 1)
                    $insert_data_propiedad['precio_venta'] = $Precio; //$precio_venta;
                if ($tipoPrecio == 2)
                    $insert_data_propiedad['precio_alquiler'] = $Precio; //$precio_alquiler;
                if ($tipoPrecio == 3) {
                    $insert_data_propiedad['precio_venta'] = $Precio; //$precio_venta;
                    $insert_data_propiedad['precio_alquiler'] = $Precio; //$precio_alquiler;
                }
                $PropiedadEnlistar = new Propiedad();

                // @todo: agregar limit de propiedades a enlistar.
                // maximo = 25.
                // 1- verificar si el uid tienes propiedades enlistadas y si son <= 30
                // 2- si es igual a 30 - enviar mensaje de error y no continuar
                // 3- si es menor a 30 - continuar el registro.
                $uid = Zend_Registry::get('uid');
                if (Hm_Pro_Propiedad::checkLimit($uid)) {
                    $PropiedadEnlistar->insert($insert_data_propiedad);
                    $idpropiedad = $db->lastInsertId();
                    Zend_Registry::set('message', "Su propiedad #$idpropiedad ha sido enlistada exitosamente");
                    $this->view->message = "Su propiedad #$idpropiedad ha sido enlistada exitosamente";
                    $this->_redirector->gotoUrl('https://apps.facebook.com/hmproject/Mihousebook/edit?propiedad=' . $idpropiedad);
                } else {
                    Zend_Registry::set('message', "No esta permitido enlistar mas de 30 propiedades");
                    $this->view->message = "No esta permitido enlistar mas de 30 propiedades";
                }
                exit();
            } catch (Exception $ex) {
                
            }
        } else {
            
        }
    }

    public function centermapAction() {


        $this->_helper->viewRenderer->setNoRender();

        $f = new Zend_Filter_StripTags();
        $estateId = null;
        if (!empty($_GET['valprop'])) {
            // Intentar recuperar el parametro de la propiedad.
            $estateId = $f->filter($this->getRequest()->getParam("id", null));

            try {
                $locationProperty = Array('cc' => $estate->Zipcode, 'city' => $estate->Ciudad, 'zip' => '', 'lat' => $estate->Latitud, 'lng' => $estate->Longitud);
                $locationProperty = (object) $locationProperty;
                $locationProperty = array($locationProperty);

                if (!empty($locationProperty)) {
                    echo '{"success Property":"true","data": ' . Zend_Json::encode($locationProperty) . '}';
                } else {
                    $ip = $this->_request->getClientIP();
                    $location = Dgt_GeoData::getLocation($ip);
                    if (!empty($location)) {
                        echo '{"success":"true","data": ' . Zend_Json::encode($location) . '}';
                        exit();
                    } else {
                        echo '{"success":"false"}';
                    }
                    exit();
                }
            } catch (Exception $e) {
                echo "Error";
            }

            exit();
        } else {
            if (!empty($location)) {
                echo '{"success":"true","data": ' . Zend_Json::encode($location) . '}';
                exit();
            } else {
                echo '{"success":"false"}';
            }
            exit();
        }
    }

    public function propiedadenmapaAction() {
        if (!session_id()) {
            session_start();
        }
        if (!empty($_SESSION['posmapa'])) {
            echo '{"success":"true","data": ' . Zend_Json::encode($_SESSION['posmapa']) . '}';
        } else {
            echo '{"success":"false"}';
        }
        exit();
    }
    
    public function propiedadfavoritoAction() {
    	$propiedadId = $_GET['id'];
    	$userId = Zend_Registry::get('uid');
    	$favorito = new Hm_Pro_Favorito($userId,$propiedadId);
    	$favorito->save();
    	
    	// clear cache
    	$cache = new HmCache();
    	$param = array();
    	$param['get'] = array('valprop' => $propiedadId);
    	$cache->clear('/enlistar/perfil',$param);
    	
    	// Redirigir al perfil
    	$redirect = '/enlistar/perfil?valprop='. $propiedadId;
    	if(isset($_GET['signed_request'])){
    		$redirect .= '&signed_request='. $_GET['signed_request'];
    	}
    	$this->_redirect($redirect);
    }
    
    public function propiedadunfavoritoAction() {
    	$propiedadId = $_GET['id'];
    	if(isset($_GET['signed_request'])){
    		$signed_request = $_GET['signed_request'];
    	}
    	$userId = Zend_Registry::get('uid');
    	$favorito = new Hm_Pro_Favorito($userId,$propiedadId);
    	$favorito->delete();
    	
    	// clear cache
    	$cache = new HmCache();
    	$param = array();
    	$param['get'] = array('valprop' => $propiedadId);
    	$cache->clear('/enlistar/perfil',$param);
    	
    	// Redirigir al perfil
    	$redirect = '/enlistar/perfil?valprop='. $propiedadId;
    	if(isset($_GET['signed_request'])){
    		$redirect .= '&signed_request='. $_GET['signed_request'];
    	}
    	$this->_redirect($redirect);
    }

    public function perfilAction() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessi�n.");
            die;
        }
        $this->view->appusers = $fb->getFriendsApp();
        $this->view->headTitle('Perfil');
        $this->view->loadmap = 'onunload="GUnload();"';

        $google_api_key = $this->cfg['google_api_key'];
        $this->view->headScript()->appendFile("https://maps.google.com/maps?file=api&v=2&sensor=false&key=$google_api_key", 'text/javascript');

        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/extjs-all.gray-theme.min.css?v=1');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');

        $db = Zend_Registry::get('db');

        if (!empty($_GET['valprop'])) {
            $IdProp = $_GET['valprop'];
            $DetaProp = $this->ObtenerDatosProPropiedad($IdProp);
            
            if (count($DetaProp) > 0) {
                $this->view->prenta = number_format($DetaProp[0]->PrecioAlquiler, 2);
                $this->view->pventa = number_format($DetaProp[0]->PrecioVenta, 2);
                $this->view->descripcionprop = $DetaProp[0]->DescPropiedad;
                $caracteristicasprop = "<span>Area: " . $DetaProp[0]->Area . ' ' . $DetaProp[0]->UnidadMedida . '</span> <span>Area Lote: ' . $DetaProp[0]->Area . ' ' . $DetaProp[0]->UnidadMedida . '</span> ';
                $this->view->direccionprop = $DetaProp[0]->Direccion . ', ' . $DetaProp[0]->Calle . ', ' . $DetaProp[0]->Ciudad . ', ' . $DetaProp[0]->Estado . '. ' . $DetaProp[0]->Pais . '. ' . $DetaProp[0]->ZipCode;
                $this->view->transaccionprop = $DetaProp[0]->Transaccion;
                $this->view->tituloprop = $DetaProp[0]->NombreCategoria . ' - ' . $DetaProp[0]->NombrePropiedad;
                $this->view->uid = $DetaProp[0]->Uid;

                //arreglo para ubicar en el mapa
                $enmapa = Array('cc' => $DetaProp[0]->ZipCode, 'city' => $DetaProp[0]->Ciudad, 'zip' => '', 'lat' => $DetaProp[0]->Latitud, 'lng' => $DetaProp[0]->Longitud);
                $enmapa = (object) $enmapa;
                $enmapa = array($enmapa);
                if (!session_id()) {
                    session_start();
                }
                $_SESSION['posmapa'] = $enmapa;

                $ListValue = $this->ObtenerDatosUsuario($this->view->uid);
                $this->view->username = $ListValue[0]['first_name'];
                $this->view->usernames = $ListValue[0]['name'];
                if (strlen($ListValue[0]['pic_big']) == 0) {
                    $pic = '/img/q_silhouette.gif';
                } else {
                    $pic = $ListValue[0]['pic_big'];
                }
                $this->view->profile = $pic;
                $location = $ListValue[0]['current_location'];
                $this->view->direccion = $location['city'] . ', ' . $location['country'];
            }

            $TotProp = $db->fetchAll("SELECT count(CodigoPropiedad) as total from propropiedad where CodigoEdoPropiedad=2 and Uid=" . $this->view->uid);
            $this->view->cantprop = $TotProp[0]->total;
            $Atributos = $this->ObtenerDatosAtributos($IdProp);
            foreach ($Atributos as $val) {
                $SiNo = '';
                if (strtoupper($val->ValorS_N) == 'S')
                    $SiNo = 'Si';
                else if (strtoupper($val->ValorS_N) == 'N')
                    $SiNo = 'No';

                $caracteristicasprop.="<span>" . $val->NombreAtributo . ' ' . $val->ValorEntero . $val->ValorDecimal . $SiNo . $val->ValorCaracter . "</span> ";
            }
            $this->view->caracteristicas = $caracteristicasprop;
            $Beneficios = $this->ObtenerDatosBeneficios($IdProp);
            $beneficiosprop = '';
            foreach ($Beneficios as $val) {
                $beneficiosprop.="<li>" . $val->NombreBeneficio . "</li>";
            }
            $this->view->beneficiosprop = $beneficiosprop;
        }
    }

}

?>
