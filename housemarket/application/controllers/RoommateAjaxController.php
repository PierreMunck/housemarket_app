<?php
class RoommateAjaxController extends Zend_Controller_Action {

    public function preDispatch(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    public function fotosdestacadasAction() {

        $f = new Zend_Filter_StripTags();
        $roommate = new Hm_Rmt_Roommate();

        if($this->getRequest()->isPost()) {
            $country = $f->filter($this->_request->getParam('country',null));
            $city = $f->filter($this->_request->getParam('city',null));
            $coordinates_raw = $f->filter($this->_request->getParam('coordinates',null));

            $start_raw = $this->_request->getParam('start', 0);
            $limit_raw = $this->_request->getParam('limit', 6);

            $start = $f->filter($start_raw);
            $limit = $f->filter($limit_raw);
            $coordinates = null;

            try {
                if($coordinates_raw != null) {
                    if(get_magic_quotes_gpc()) {
                        $coordinates_raw = stripslashes($coordinates_raw);
                    }
                    $coordinates = Zend_Json_Decoder::decode($coordinates_raw);
                }

                $fotos = $roommate->GetFotosDestacadas($country, $city, $coordinates, $start, $limit);
                if (count($fotos) > 0) {
                    echo '{"success":true, "results":'.count($fotos).', "rows":'. Zend_Json::encode($fotos) .'}';
                } else {
                    echo '{"success":true, "results":0, "rows":[]}';
                }
            } catch (Exception $exc) {

            }
            } else {
                echo '{"success":"false", "msg":"method get no allowed"}';
            }
    }

    public function fotosfiltradasAction() {

        $f = new Zend_Filter_StripTags();
        $roommate = new Hm_Rmt_Roommate();

        if($this->getRequest()->isPost()) {
            $sPage = $f->filter($this->_request->getParam('sPage',null));//pagina seleccionada en paginacion roommate
            $cPage = $f->filter($this->_request->getParam('cPage',null));//pagina actual en paginacion roommate
            $coordinates_raw = $f->filter($this->_request->getParam('coordinates', null));
            $minPrice = $f->filter($this->_request->getParam('min_price', null));
            $maxPrice = $f->filter($this->_request->getParam('max_price', null));

            $start_raw = $this->_request->getParam('start', 0);
            $limit_raw = $this->_request->getParam('limit', 5);
            $start = $f->filter($start_raw);
            $limit = $f->filter($limit_raw);

            $coordinates = null;
            if($coordinates_raw != null) {
                if(get_magic_quotes_gpc()) {
                    $coordinates_raw = stripslashes($coordinates_raw);
                }
                $coordinates = Zend_Json_Decoder::decode($coordinates_raw);
            }

            $uid = Zend_Registry::get('uid');

            try {
                $fotos = $roommate->GetFotosFiltradas($uid, null, null, null, null, $coordinates, $minPrice, $maxPrice, $start, $limit);
            } catch (Exception $exc) {
            }

         try {
                $totalFotos = $roommate->GetTotalFotosFiltradas($uid, null, null, null, null, $minPrice, $maxPrice, $coordinates);
            } catch (Exception $exc) {
            }

            try {
                if (!empty($fotos)) {
                    echo '{"success":true, "cPage":' . $cPage . ', "sPage":' . $sPage . ', "total": ' . $totalFotos . ', "results": '.count($fotos).', "rows": '. Zend_Json::encode($fotos) .'}';
                } else {
                    echo '{"success":true, "cPage":0, "sPage":0, "total":0, "results":0, "rows":[]}';
                }
            } catch (Exception $exc) {
            }
        } else {
            echo '{"success":"false", "msg":"method get no allowed"}';
        }
     //   exit();
    }

    /**
     * Extrae todos los compa�eros de cuarto por �rea.
     */
     public function roommatesbyareaAction(){

        $f = new Zend_Filter_StripTags();
        $c = new Hm_Rmt_Roommate();

        if($this->getRequest()->isPost()) {

            $coordinates_raw = $f->filter($this->_request->getParam('coordinates', null));
            $actual_roommate = $f->filter($this->_request->getParam('actual_roommate', null));
            $uid = Zend_Registry::get('uid');

            $parameters = array();
            if($parameters_raw != null) {
                if(get_magic_quotes_gpc()) {
                    $parameters_raw = stripslashes($parameters_raw);
                }
                $parameters = Zend_Json_Decoder::decode($parameters_raw);
            }

            $coordinates = null;
            if($coordinates_raw != null) {
                if(get_magic_quotes_gpc()) {
                    $coordinates_raw = stripslashes($coordinates_raw);
                }
                $coordinates = Zend_Json_Decoder::decode($coordinates_raw);
            }
            $roommates =  $c->GetFotosFiltradas($uid, null, null, null, null, $coordinates, null, null, 0, 6, $actual_roommate);

            if (!empty($roommates)) {

                echo '{"success":true, "results":'.count($roommates).', "rows": '. Zend_Json::encode($roommates) .'}';

            } else {
                echo '{"rows": []}';
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
    //    exit();
    }
    
     public function roommateenmapaAction() {
        $f = new Zend_Filter_StripTags();
        $roommate = new Hm_Rmt_Roommate();
        $roommateId = $f->filter($this->getRequest()->getParam("valroommate", null)) ;
        $rs = $roommate->find($roommateId);
        $curRoommate = $rs->current();
        $locationProperty = Array ('cc' =>$curRoommate->ZipCode , 'city' => $curRoommate->Ciudad, 'zip' =>'' ,'lat' =>$curRoommate->Latitud , 'lng' => $curRoommate->Longitud );
        $locationProperty =(object)$locationProperty;
        $locationProperty = array($locationProperty);

        if(!empty ($locationProperty)) {
            echo '{"success":"true","data": '. Zend_Json::encode($locationProperty) .'}';
        } else {
            $ip = $this->_request->getClientIP();
            $location = Dgt_GeoData::getLocation($ip);
            if (!empty($location)) {
                echo '{"success":"true","data": '. Zend_Json::encode($location) .'}';
            } else {
                echo '{"success":"false"}';
            }
            exit();
        }
       // exit();
    }

     /**
     * Recupera la informacion a mostrarse en el google map
     * cuando se busca por localidad
     */
    public function estatetogmAction() {

        $f = new Zend_Filter_StripTags();
        $roommate = new Hm_Rmt_Roommate();

        if($this->getRequest()->isPost()) {
            $country = $f->filter($this->_request->getParam('country',null));
            $city = $f->filter($this->_request->getParam('city',null));
            $about = $f->filter($this->_request->getParam('about', NULL));//Acerca del cuarto
            $occupier = $f->filter($this->_request->getParam('occupier', NULL));//Habitantes actuales
            $general = $f->filter($this->_request->getParam('general', NULL));//Condiciones Generales
            $ideal_partner = $f->filter($this->_request->getParam('ideal_partner', NULL));//Compa�ero Ideal
            $coordinates_raw = $f->filter($this->_request->getParam('coordinates',null));
            $minPrice = $f->filter($this->_request->getParam('min_price', null));
            $maxPrice = $f->filter($this->_request->getParam('max_price', null));

            $start = $f->filter($this->_request->getParam('start', 0));
            $limit = $f->filter($this->_request->getParam('limit', 10));

            $uid = Zend_Registry::get('uid');

            $coordinates = null;
            if($coordinates_raw != null) {
                if(get_magic_quotes_gpc()) {
                    $coordinates_raw = stripslashes($coordinates_raw);
                }
                $coordinates = Zend_Json_Decoder::decode($coordinates_raw);
            }

            $parameters = null;
            /*if($parameters_raw != null) {
                if(get_magic_quotes_gpc()) {
                    $parameters_raw = stripslashes($parameters_raw);
                }
                $parameters = Zend_Json_Decoder::decode($parameters_raw);
            }*/

            try {
                    // si ciudad falla, buscar por pais.
                    $roommates = $roommate->FilterStatesOnDir($uid, $about, $occupier, $general, $ideal_partner, $coordinates, $start, $limit, $minPrice, $maxPrice);
                    if(!empty($roommates)) {
                        echo '{"success":true, "results":'.count($roommates).', "rows":'. Zend_Json::encode($roommates) .'}';
                    } else {
                        echo '{"success":true, "results":0, "rows": []}';
                    }
            } catch (Exception $exc) {
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
      //  exit();
    }
}
?>