<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CreditosController extends Zend_Controller_Action {

    protected $_application;
    protected $_flashMessenger = null;
    protected $_redirector = null;

    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
    }

    public function preDispatch() {
        
    }

    public function addAction() {

        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessión.");
            die;
        }

        $db = Zend_Registry::get('db');
        $cfg = Zend_Registry::get('config');
        //Capturo el codigo encriptado. Ahora tambien se captura el Codigo de La Promocion
        $xprom = base64_decode($_GET["xprom"]);
        $part = explode("/", $xprom);
        $codigo_prom = end($part);
        $uid_ref = $part[0];
        $uid = $fb->get_uid();
        //Esto ocurre si la promocion es creditos por referencias
        if ($codigo_prom == 2) {            
            $Own = Array();
            $AdminIsList = 0;
            $page_reference = $part[1];
            //Determino los adminstradores de la pagina segun la BASE DE DATOS
            $sql = "SELECT uid FROM fb_page WHERE pageid = " . $page_reference;
            $ref = $db->fetchAll($sql);
            if ($ref) {
                foreach ($ref as $index => $refe) {
                    $Own[] = $refe['uid'];
                    $sql = "SELECT c.CodigoCliente FROM crecredito cr 
                        INNER JOIN cliente c ON (c.CodigoCliente = cr.CodigoCliente) 
                        WHERE Uid = " . $refe['uid'] . " AND CodigoPromocion = 2 AND UidReference = ".$uid_ref;
                    $adminprom = $db->fetchOne($sql);
                    //Si ya hay alguien con la promocion
                    if ($adminprom)
                        $AdminIsList++;
                }               
                //Si estoy en la lista de administradores de la pagina segun BD Y los Otros Administradores no han Activado la promocion
                if (in_array($uid, $Own) && $AdminIsList<1) {                    
                    $sql = "SELECT CodigoCliente FROM cliente WHERE Uid = " . $uid;
                    $CodigoCliente = $db->fetchOne($sql);
                    //Si todavia no es Registrado la promocion
                    $data = array(
                        'CodigoCredito' => null,
                        'FechaCompra' => new Zend_Db_Expr('CURDATE()'),
                        'FechaVence' => new Zend_Db_Expr('DATE_ADD(CURDATE(), INTERVAL 1 MONTH)'),
                        'Cantidad' => 10,
                        'Saldo' => 10,
                        'EstadoCredito' => 'A',
                        'CodigoRepresentante' => '1',
                        'CodigoCliente' => $CodigoCliente,
                        'FechaPedido' => new Zend_Db_Expr('CURDATE()'),
                        'MontoCompra' => '10',
                        'TipoCompra' => 'Referencia',
                        'CodigoPromocion' => $codigo_prom,
                        'PageId' => null,
                        'UidReference' => $uid_ref
                    );
                    try {
                        $db->insert('crecredito', $data);
                    } catch (Exception $e) {
                        echo "Error message: " . $e->getMessage() . "\n";
                    }
                }               
                //debug('Se ha ingresado un nuevo cliente');            
                $this->_redirector->gotoUrl($urlfb . "mihousebook/publicacion");
                exit();
            }
        } elseif ($codigo_prom == 1) {
            $xprom_creditos = $part[0]; //Este codigo no vale....
            $xprom_codigoCliente = $part[1]; //Codigo del Cliente ha beneficiar
            $xprom_Profile_PageId = $part[2];

            //echo ($_GET["xprom"]." [ ".$xprom." ]"); exit();
            //Defino el Codigo de Promocion referente a Creditos de Corterias : 1
            $CodigoPromocion = 1;
            $sql = " SELECT cr.CodigoCliente,p.CantidadCredito FROM crecredito cr INNER JOIN promocion p ON" .
                    " (cr.CodigoPromocion = p.CodigoPromocion) WHERE CodigoCliente=" . $xprom_codigoCliente .
                    " AND p.CodigoPromocion = " . $CodigoPromocion .
                    " AND ( p.FechaVence >= Current_Date() OR p.FechaVence is null )" .
                    " AND p.CodigoEdoPromocion = 'A' AND cr.PageId = " . $xprom_Profile_PageId;
            //Consulto si el usuario ya ha sido beneficiado con la promocion
            $PROMOCION = $db->fetchAll($sql);
            if (count($PROMOCION) > 0) {//ya ha utilizado la promocion
                //Capturamos la cantidad de Creditos de la promocion            
            } else {//es un nuevo usuario en la promocion 
                $sqlpro = "SELECT CantidadCredito FROM promocion  WHERE CodigoPromocion = " . $CodigoPromocion;
                //Consulto si el usuario ya ha sido beneficiado con la promocion
                $PROM = $db->fetchAll($sqlpro);
                $xprom_creditos = $PROM[0]["CantidadCredito"];
                $urlfb = $config['fb_app_url'];
                $data = array(
                    'CodigoCredito' => null,
                    'FechaCompra' => new Zend_Db_Expr('CURDATE()'),
                    'FechaVence' => new Zend_Db_Expr('DATE_ADD(CURDATE(), INTERVAL 1 MONTH)'),
                    'Cantidad' => $xprom_creditos,
                    'Saldo' => $xprom_creditos,
                    'EstadoCredito' => 'A',
                    'CodigoRepresentante' => '1',
                    'CodigoCliente' => $xprom_codigoCliente,
                    'FechaPedido' => new Zend_Db_Expr('CURDATE()'),
                    'MontoCompra' => '10',
                    'TipoCompra' => 'Cortesia',
                    'CodigoPromocion' => $CodigoPromocion,
                    'PageId' => $xprom_Profile_PageId
                );
                try {
                    $db->insert('crecredito', $data);
                } catch (Exception $e) {
                    echo "Error message: " . $e->getMessage() . "\n";
                }
            }
            $this->_redirector->gotoUrl($urlfb . "mihousebook/publicacion");
            exit();
        }
    }

    public function getlistbrokerAction() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessión.");
            die;
        }
        $db = Zend_Registry::get('db');
        $cfg = Zend_Registry::get('config');
        //Armando correo con plantilla promocion.phtml
        $html = new Zend_View();
        $html->setScriptPath(APPLICATION_PATH . '/views/scripts/emails/');
        $body = $html->render('broker.phtml');
        //Configuracion del servidor smtp
        $smtpServer = "smtp.gmail.com";
        $username = "hmappfb@gmail.com";
        $password = "xolotlan";
        $config = array('ssl' => 'tls',
            'port' => 587,
            'auth' => 'login',
            'username' => $username,
            'password' => $password);
        //$fb_user = $fbb ->api_client->fql_query("SELECT uid, first_name, last_name,email FROM user WHERE is_app_user");


        $sql = "SELECT CodigoCliente,uid,NombreCliente,email FROM ListaBroker WHERE NOT email='' AND NOT NombreCliente='' AND email='lor.blue@hotmail.com'";
        $brokers = $db->fetchAll($sql);
        //echo "<select>";
        if (count($brokers) > 0) {
            $i = 0;
            foreach ($brokers as $broker) {
                // echo "<option>".$broker["email"]."</option>";
                //CodigoCredito,FechaCompra, FechaVence, Cantidad, Saldo, EstadoCredito,
                //CodigoRepresentante,
                //CodigoCliente, FechaPedido, MontoCompra, TipoCompra
                $data = array(
                    'CodigoCredito' => null,
                    'FechaCompra' => new Zend_Db_Expr('CURDATE()'),
                    'FechaVence' => new Zend_Db_Expr('DATE_ADD(CURDATE(), INTERVAL 1 MONTH)'),
                    'Cantidad' => '10',
                    'Saldo' => 10,
                    'EstadoCredito' => 'A',
                    'CodigoRepresentante' => '1',
                    'CodigoCliente' => '',
                    'FechaPedido' => new Zend_Db_Expr('CURDATE()'),
                    'MontoCompra' => '10',
                    'TipoCompra' => 'Cortesia'
                );
                try {
                    $db->insert('crecredito', $data);
                } catch (Exception $e) {
                    echo "Error message: " . $e->getMessage() . "\n";
                }

                // assign valeues   
                // echo $broker["NombreCliente"];echo"sip";
                /* $html->namec = $broker['NombreCliente'];
                  $html->email = $broker['email'];// Email destinatarioa de La Carga Masiva
                  try {
                  $transport = new Zend_Mail_Transport_Smtp($smtpServer, $config);
                  $mail = new Zend_Mail('UTF-8');
                  $mail->setFrom($username, "Housemarket Notification");
                  $mail->setReplyTo($username, "No-Reply");
                  $mail->setReturnPath($username, "No-Reply");
                  $mail->addTo($broker["email"], $broker["NombreCliente"]);
                  $mail->setSubject('');
                  $mail->setBodyHtml($body);
                  $mail->setBodyText($body);
                  //if($mail->send($transport)) Habilitar si hay que enviar EMAIL
                  //echo "ok,";
                  } catch (Exception $e) {
                  echo "Error message: " . $e->getMessage() . "\n";
                  } */
            }
        }
        //echo "</select>";

        exit();
    }

    public function notificacionAction() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessi�n.");
            die;
        }
        $this->view->uid = $fb->get_uid();
        $this->view->appusers = $fb->getFriendsApp();

        $this->view->headTitle('Cargar Propiedades desde Archivo');
        $this->view->loadmap = 'onunload="GUnload();"';
        //$fb->catchInvalidSession();
        $config = Zend_Registry::get('config');
        $dirUrl = $config['fb_app_url'];
        $dirUrl = $dirUrl . "/mihousebook/loadresult";
        
        // minified and concatenated: ext-all.css, xtheme-gray.css
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/extjs-all.gray-theme.min.css?v=1');
        
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');

        if ($this->getRequest()->isPost()) {
            if (!$_FILES["file"]["tmp_name"]) {
                if (!session_id()) {
      session_start();
    }
                $_SESSION["valor"] = false;
                $_SESSION["mensaje"] = "Es necesario que seleccione un archivo.";
                $this->_redirector->gotoUrl($dirUrl);
            } else {
                if (($_FILES["file"]["type"] == "text/plain") && ($_FILES["file"]["size"] < 90000)) {
                    $db = Zend_Registry::get('db');

                    $resCategoria = $db->fetchAll("SELECT CodigoCategoria, NombreCategoria FROM catcategoria");
                    $resMoneda = $db->fetchAll("SELECT CodigoMoneda, NombreMoneda FROM catmoneda");

                    $fp = fopen($_FILES["file"]["tmp_name"], "r");
                    $fallas = 0;
                    $total = 0;
                    $posiciones;
                    $enPantalla;
                    $enDB;
                    $posGrid = 0;

                    $Encabezado;
                    $PrimeraPos = true;
                    while ($linea = fgets($fp, 5000)) {
                        if ($PrimeraPos) {
                            $PrimeraPos = false;
                            $Encabezado = explode("|", $linea);
                        } else {
                            $total++;
                            $Arreglo = explode("|", $linea);
                            $insert_data_propropiedad = "";
                            for ($i = 0; $i < count($Encabezado); $i++) {
                                if (trim(strtoupper($Encabezado[$i])) == 'CODIGOCATEGORIA') {
                                    $insert_data_propropiedad[trim($Encabezado[$i])] = $this->ObtieneCategoria($Arreglo[$i], $resCategoria);
                                } else if (trim(strtoupper($Encabezado[$i])) == 'CODIGOMONEDA') {
                                    $insert_data_propropiedad[trim($Encabezado[$i])] = $this->ObtieneMoneda($Arreglo[$i], $resMoneda);
                                }
                                else
                                    $insert_data_propropiedad[trim($Encabezado[$i])] = $Arreglo[$i];
                            }
                            $insert_data_propropiedad['FechaRegistro'] = new Zend_Db_Expr('CURDATE()');
                            $insert_data_propropiedad['Uid'] = $this->view->uid;
                            $insert_data_propropiedad['CodigoEdoPropiedad'] = 1;
                            $insert_data_propropiedad['CargaMasiva'] = true;

                            $ingreso = new Hm_Pro_Propiedad();
                            try {
                                $ingreso->insert($insert_data_propropiedad);
                                for ($i = 0; $i < count($Encabezado); $i++) {
                                    if (trim(strtoupper($Encabezado[$i])) == 'ZIPCODE') {
                                        $enPantalla[$posGrid][strtoupper(trim($Encabezado[$i]))] = $Arreglo[$i];
                                    } else if (trim(strtoupper($Encabezado[$i])) == 'NOMBREPROPIEDAD') {
                                        $enPantalla[$posGrid][strtoupper(trim($Encabezado[$i]))] = $this->CodificaTexto($Arreglo[$i]);
                                    } else if (trim(strtoupper($Encabezado[$i])) == 'ESTADO') {
                                        $enPantalla[$posGrid][strtoupper(trim($Encabezado[$i]))] = $this->CodificaTexto($Arreglo[$i]);
                                    } else if (trim(strtoupper($Encabezado[$i])) == 'CIUDAD') {
                                        $enPantalla[$posGrid][strtoupper(trim($Encabezado[$i]))] = $this->CodificaTexto($Arreglo[$i]);
                                    }
                                }

                                $posGrid++;
                            } catch (Exception $e) {
                                $fallas++;
                                $posiciones = $posiciones . $total . ", ";
                            }
                        }
                    }//en while

                    if ($posGrid > 0) {
                        require '../library/hm/Cliente.php';
                        L_cliente::NuevoCliente($this->view->uid);
                    }
                    fclose($fp);
                    $mensaje = "";
                    if ($fallas > 0) {
                        $posiciones = substr($posiciones, 0, strlen($posiciones) - 2);
                        $Tfallas = $total - $fallas;
                        $mensaje = "Se insertaron " . $Tfallas . " de " . $total . " registros. Posiciones de registros que fallaron (" . $posiciones . ")";
                    } else {
                        $mensaje = "Total de registros insertados: " . $total . " de " . $total . "";
                    }
                    if (!session_id()) {
      session_start();
    }
                    $_SESSION["contenido"] = $this->Ordenar($enPantalla, 'ZIPCODE', 'NOMBREPROPIEDAD', 'ESTADO', 'CIUDAD');
                    $_SESSION["valor"] = true;
                    $_SESSION["mensaje"] = $mensaje;
                    $this->_redirector->gotoUrl($dirUrl);
                } else {
                    if (!session_id()) {
      session_start();
    }
                    $_SESSION["valor"] = false;
                    $_SESSION["mensaje"] = "El archivo que intenta cargar no es valido.";
                    $this->_redirector->gotoUrl($dirUrl);
                }
            }
        }
    }

}

?>
