<?php

class GalleryController  extends Zend_Controller_Action {

    //protected $_application ;
    protected $_flashMessenger = null;
    protected $_redirector = null;
  //  public $_rol;
    public $_messagefotos = "";

    public function init() {
       
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
         
        $this->_helper->layout()->disableLayout();
      
    }
    public function preDispatch() {
        
    }

   public function indexAction() {
      
       //$this->_helper->viewRenderer->setNoRender(true);
       if($this->getRequest()->isPost()) {
        // no post data
        // no retornamos nada
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_GET'));
            $db = Zend_Registry::get('db');
            $this->view->headTitle('<< MyGallery >>');
            $f = new Zend_Filter_StripTags();
            $idpropiedad = urldecode($f->filter($this->_request->getParam('propiedad')));

//            Zend_Debug::dump($idpropiedad);
//            die;
            $gallery= new Dgt_Gallery();
            $this->view->gallery =$gallery->getImages_propiedad($idpropiedad);
          //  Zend_Debug::dump($this->view->gallery);
        }
 }




 

  public function getimagesAction() {
        $destiny=getenv('DOCUMENT_ROOT').'/propiedades/';
        $urlimages='/propiedades/';
       
      
     // Zend_Debug::dump($this->_request->getParam('propiedad'));
       
       if($this->getRequest()->isGet()) {
            $db = Zend_Registry::get('db');
            
            $this->getRequest()->setParamSources(array('_GET'));
          
            $f = new Zend_Filter_StripTags();
            $idpropiedad = urldecode($f->filter($this->_request->getParam('propiedad')));

            $fb= Dgt_Fb::getInstance();

            $uid=$fb->get_uid();
         // $idpropiedad="9903319";
            $idpropiedad = str_replace('"','',$idpropiedad);
            $idpropiedad = substr($idpropiedad, 0, 7);
            $location=$destiny.$idpropiedad;
 
            $formupload=new Dgt_Formupload();
            $albumid=$formupload->get_album_BD($idpropiedad);

             $pathfinalimage= $urlimages.$idpropiedad."/".$albumid."/normal/";
            $album=$location.'/'.$albumid;
            $albumnormal=$album."/normal";
                  
            try{
                $dir =$albumnormal."/";
              
                $images = array();
                $d = dir($dir);
               
               
                while($name = $d->read()){
                  
                    if(!preg_match('/\.(jpg|JPG|jpeg|jpe|gif|png)$/', $name)) continue;
                    $size = filesize($dir.$name);
                    $lastmod = filemtime($dir.$name)*1000;
                    $images[] = array('name'=>$name, 'size'=>$size,'lastmod'=>$lastmod, 'url'=>$pathfinalimage.$name);
                }
                
                $d->close();
                $o = array('images'=>$images);
                echo json_encode($o);
                exit();
            }catch (Zend_File_Transfer_Exception $e){
                echo $e->getMessage().$location;
                exit();
             }


        } else {
            // no post data
            // no retornamos nada
          exit();
        }

 }



}
