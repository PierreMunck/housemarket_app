<?php
class RoomAjaxController extends Zend_Controller_Action{
     public function fotosdestacadasAction() {
        $f = new Zend_Filter_StripTags();
        $c = new Hm_Rmt_Cuarto();

        if($this->getRequest()->isPost()) {
            $country = $f->filter($this->_request->getParam('country',null));
            $city = $f->filter($this->_request->getParam('city',null));
            $coordinates_raw = $f->filter($this->_request->getParam('coordinates',null));

            $start_raw = $this->_request->getParam('start', 0);
            $limit_raw = $this->_request->getParam('limit', 6);

            $start = $f->filter($start_raw);
            $limit = $f->filter($limit_raw);

            $coordinates = null;
            if($coordinates_raw != null) {
                if(get_magic_quotes_gpc()) {
                    $coordinates_raw = stripslashes($coordinates_raw);
                }
                $coordinates = Zend_Json_Decoder::decode($coordinates_raw);
            }

            try {
                $fotos = $c->GetFotosDestacadas($country, $city, $coordinates, $start, $limit);
            } catch (Exception $exc) {
            	
            }
            
           try {
               if (count($fotos) > 0) {
                    echo '{"success":true, "results":'.count($fotos).', "rows":'. Zend_Json::encode($fotos) .'}';
                } else {
                    echo '{"success":true, "results":0, "rows":[]}';
                }
            }catch (Excepcion $e){
               echo $e->getMessage();
            }
        } else {
            echo '{"success":"false", "msg":"method get no allowed"}';
        }
        exit();
    }

     public function fotosfiltradasAction() {

        $f = new Zend_Filter_StripTags();
        $cuarto = new Hm_Rmt_Cuarto();

        if($this->getRequest()->isPost()) {
            $sPage = $f->filter($this->_request->getParam('sPage',null));//pagina seleccionada en paginacion propiedades
            $cPage = $f->filter($this->_request->getParam('cPage',null));//pagina actual en paginacion propiedades
            $conditions = $f->filter($this->_request->getParam('conditions', NULL));//Acerca del cuarto
            $myInfo = $f->filter($this->_request->getParam('myInfo', NULL));//Habitantes actuales
            $idealRoom = $f->filter($this->_request->getParam('ideal_room', NULL));//Condiciones Generales
            $idealPartner = $f->filter($this->_request->getParam('ideal_partner', NULL));//Compa�ero Ideal
            $coordinates_raw = $f->filter($this->_request->getParam('coordinates', null));
            $minPrice = $f->filter($this->_request->getParam('min_price', null));
            $maxPrice = $f->filter($this->_request->getParam('max_price', null));

            $start_raw = $this->_request->getParam('start', 0);
            $limit_raw = $this->_request->getParam('limit', 5);
            $start = $f->filter($start_raw);
            $limit = $f->filter($limit_raw);

            $start = $f->filter($start_raw);
            $limit = $f->filter($limit_raw);
            $uid = Zend_Registry::get('uid');

            $coordinates = null;
            if($coordinates_raw != null) {
                if(get_magic_quotes_gpc()) {
                    $coordinates_raw = stripslashes($coordinates_raw);
                }
                $coordinates = Zend_Json_Decoder::decode($coordinates_raw);
            }

            try {
                $rsPhotos = $cuarto->GetFotosFiltradas($uid, $conditions, $idealRoom, $myInfo, $idealPartner, $coordinates, $minPrice, $maxPrice, $start, $limit);
                $fotos = $rsPhotos->rows;
                $totalFotos = $rsPhotos->total;
            } catch (Exception $exc) {
                echo "Exception room filtradas Query: ". $exc->getMessage();
            }

            try {
                if (!empty($fotos)) {
                    echo '{"success":true, "cPage":' . $cPage . ', "sPage":' . $sPage . ', "total":' . $totalFotos . ', "results":'.count($fotos).', "rows": '. Zend_Json::encode($fotos) .'}';
                } else {
                    echo '{"success":true, "cPage":0, "sPage":0, "total":0, "results":0, "rows":[]}';
                }
            } catch (Exception $exc) {
                echo "Exception room filtradas: ". $exc->getTraceAsString();
            }

        } else {
            echo '{"success":"false", "msg":"method get no allowed"}';
        }
        exit();
    }

    public function cuartoenmapaAction() {
        $f = new Zend_Filter_StripTags();
        $room = new Hm_Rmt_Cuarto();
        $roomId = $f->filter($this->getRequest()->getParam("valroom", null)) ;
        $rs = $room->find($roomId);
        $curRoom = $rs->current();
        $locationProperty = Array ('cc' =>$curRoom->ZipCode , 'city' => $curRoom->Ciudad, 'zip' =>'' ,'lat' =>$curRoom->Latitud , 'lng' => $curRoom->Longitud );
        $locationProperty =(object)$locationProperty;
        $locationProperty = array($locationProperty);

        try {
            if(!empty ($locationProperty)) {
                echo '{"success":"true","data": '. Zend_Json::encode($locationProperty) .'}';
            } else {
                $ip = $this->_request->getClientIP();
                    $location = Dgt_GeoData::getLocation($ip);
                if (!empty($location)) {
                    echo '{"success":"true","data": '. Zend_Json::encode($location) .'}';
                } else {
                    echo '{"success":"false"}';
                }
                exit();
            }
        } catch (Exception $exc) {

        }
        exit();
    }

     /**
     * Recupera la informacion a mostrarse en el google map
     * cuando se busca por localidad
     */
    public function estatetogmAction() {

        $f = new Zend_Filter_StripTags();
        $c = new Hm_Rmt_Cuarto();

        if($this->getRequest()->isPost()) {
            $country = $f->filter($this->_request->getParam('country',null));
            $city = $f->filter($this->_request->getParam('city',null));
            $conditions = $f->filter($this->_request->getParam('conditions', NULL));//Acerca del cuarto
            $myInfo = $f->filter($this->_request->getParam('myInfo', NULL));//Habitantes actuales
            $idealRoom = $f->filter($this->_request->getParam('ideal_room', NULL));//Condiciones Generales
            $idealPartner = $f->filter($this->_request->getParam('ideal_partner', NULL));//Compa�ero Ideal
            $coordinates_raw = $f->filter($this->_request->getParam('coordinates',null));
            $minPrice = $f->filter($this->_request->getParam('min_price', null));
            $maxPrice = $f->filter($this->_request->getParam('max_price', null));

            $start = $f->filter($this->_request->getParam('start', 0));
            $limit = $f->filter($this->_request->getParam('limit', 10));
            $uid = Zend_Registry::get('uid');

            $coordinates = null;
            if($coordinates_raw != null) {
                if(get_magic_quotes_gpc()) {
                    $coordinates_raw = stripslashes($coordinates_raw);
                }
                $coordinates = Zend_Json_Decoder::decode($coordinates_raw);
            }

            $parameters = null;
            /*if($parameters_raw != null) {
                if(get_magic_quotes_gpc()) {
                    $parameters_raw = stripslashes($parameters_raw);
                }
                $parameters = Zend_Json_Decoder::decode($parameters_raw);
            }*/

            try {
                    // si ciudad falla, buscar por pais.
                    $cuartos = $c->FilterStatesOnDir($uid, $conditions, $idealRoom, $myInfo, $idealPartner, $coordinates, $minPrice, $maxPrice, $start, $limit);
                    if(!empty($cuartos)) {
                        echo '{"success":true, "results":'.count($cuartos).', "rows": '. Zend_Json::encode($cuartos) .'}';
                    } else {
                        echo '{"success":true, "results":0, "rows": []}';
                    }
            } catch (Exception $exc) {

            }

        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }

     public function roomsbyareaAction(){
        $f = new Zend_Filter_StripTags();
        $c = new Hm_Rmt_Cuarto();

        if($this->getRequest()->isPost()) {
            $coordinates_raw = $f->filter($this->_request->getParam('coordinates', null));
            $codigo_cuarto = $f->filter($this->_request->getParam('codigo_cuarto', null));
            $parameters = array();
            /*if($parameters_raw != null) {
                if(get_magic_quotes_gpc()) {
                    $parameters_raw = stripslashes($parameters_raw);
                }
                $parameters = Zend_Json_Decoder::decode($parameters_raw);
            }*/

            $coordinates = null;
            if($coordinates_raw != null) {
                if(get_magic_quotes_gpc()) {
                    $coordinates_raw = stripslashes($coordinates_raw);
                }
                $coordinates = Zend_Json_Decoder::decode($coordinates_raw);
            }
            try {
                $object =  $c->GetRoomsByArea($codigo_cuarto, $coordinates);
                $rooms = $object->rows;
                $total = $object->total;
            } catch (Exception $exc) {
            }

            try {
                if (!empty($rooms)) {
                    echo '{"success":true, "results":'.$total.', "rows": '. Zend_Json::encode($rooms) .'}';
                } else {
                    echo '{"rows": []}';
                }
            } catch (Exception $exc) {
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }
}
?>