<?php

/**
 * Controlador de documentos que se muestran en Housemarket
 */
class DocumentosController extends MainController {
    /* Directorio principal de documentos */
    const DIRECTORY_DOCUMENT_INCLUDE = "/lang/documentos";

    const FILE_EXTENSION_INCLUDE = ".inc.phtml";

    const SUB_DIRECTORY_UPLOADINGLIST = "/uploadlisting";

    const SUB_DIRECTORY_ABOUTHOUSEMARKET = "/abouthousemarket";

    /**
     * Mensajes al usuario
     * @var
     */
    protected $_flashMessenger = null;
    /**
     * Redirector de Zend
     * @var
     */
    protected $_redirector = null;

    /**
     * Inicializa las variables del controller
     */
    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
        $config = Zend_Registry::get('config');
        $this->view->web_path = $config['fb_app_url'];
    }

    /**
     * Antes de procesar el action
     */
    public function preDispatch() {
        
    }

    /**
     * Subir archivos a housemarket
     */
    public function uploadlistingAction() {

        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessión.");
            die;
        }
        $db = Zend_Registry::get('db');
        $cfg = Zend_Registry::get('config');
        //Armando correo con plantilla promocion.phtml
        $html = new Zend_View();
        $html->setScriptPath(APPLICATION_PATH . '/views/scripts/emails/');
        $body = $html->render('broker.phtml');
        //Configuracion del servidor smtp
        $smtpServer = "smtp.gmail.com";
        $username = "hmappfb@gmail.com";
        $password = "xolotlan";
        $config = array('ssl' => 'tls',
            'port' => 587,
            'auth' => 'login',
            'username' => $username,
            'password' => $password);
        
        $sql = "SELECT uid,NombreCliente,email FROM ListaBroker WHERE NOT email='' AND NOT NombreCliente=''";
        $brokers = $db->fetchAll($sql);
        //echo "<select>";
        if (count($brokers) > 0) {
            $i = 0;
            foreach ($brokers as $broker) {
                $html->namec = $broker->NombreCliente;
                $html->email = $broker->email; // Email destinatarioa de La Carga Masiva        
                try {
                    $transport = new Zend_Mail_Transport_Smtp($smtpServer, $config);
                    $mail = new Zend_Mail('UTF-8');
                    $mail->setFrom($username, "Housemarket Notification");
                    $mail->setReplyTo($username, "No-Reply");
                    $mail->setReturnPath($username, "No-Reply");
                    $mail->addTo($broker->email, $broker->NombreCliente);
                    $mail->setSubject('');
                    $mail->setBodyHtml($body);
                    $mail->setBodyText($body);
                    
                } catch (Exception $e) {
                    echo "Error message: " . $e->getMessage() . "\n";
                }
            }
        }
        //echo "</select>";

        exit();
    }

    /**
     * Sobre housemarket
     */
    public function abouthousemarketAction() {

        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessión.");
            die;
        }
        $config = Zend_Registry::get('config');
        $localLanguage = Zend_Registry::get('language');

        $this->view->urlsite = $config['fb_app_url'];


        $path = APPLICATION_PATH . self::DIRECTORY_DOCUMENT_INCLUDE .
                self::SUB_DIRECTORY_ABOUTHOUSEMARKET . "/" .
                $localLanguage . self::FILE_EXTENSION_INCLUDE;

        $localLanguage = $this->GiveLanguage($path, $localLanguage);

        $this->view->scriptPath = APPLICATION_PATH .
                self::DIRECTORY_DOCUMENT_INCLUDE .
                self::SUB_DIRECTORY_ABOUTHOUSEMARKET;
        $this->view->includeFile = $localLanguage . self::FILE_EXTENSION_INCLUDE;
    }

    /**
     * Terminos de uso
     */
    public function termsuseAction() {

        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessión.");
            die;
        }
    }

    /**
     * Politicas de privacidad
     */
    public function privacypolicyAction() {

        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessión.");
            die;
        }
    }

    /**
     * Reportar imagenes ofensivas
     */
    public function reportAction() {

        $uid = Zend_Registry::get('uid');
        $user_name = "";

        $fb_user = $this->fbDgt->execFql("SELECT uid, first_name, last_name, pic_square, profile_url FROM user WHERE uid IN (" . $uid . ") AND is_app_user");
        if ($fb_user && is_array($fb_user)) {
            $user_name = $fb_user[0]["first_name"] . ", " . $fb_user[0]["last_name"];
        }
        // Recuperar la informacion del usuario conectado.
        $f = new Zend_Filter_StripTags();

        $state_code = $f->filter($this->getRequest()->getParam("state", ''));
        $comment = $f->filter($this->getRequest()->getParam("comment", ''));
        $url = $f->filter($this->getRequest()->getParam("url", ''));

        $state = "No proporcionado";

        if ($state_code == "1") {
            $state = "Miscategorized";
        } else if ($state_code == "2") {
            $state = "Illegal or inappropriate content";
        } else if ($state_code == "3") {
            $state = "Spam or fraud";
        } else if ($state_code == "4") {
            $state = "Unavalaible";
        }

        $html = '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center"><table width="620" border="0" cellspacing="0" cellpadding="0" style="background-color:#dbe4eb; border:1px solid #88b3da">' .
                '<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>' .
                '<tr><td>&nbsp;</td><td><table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Tahoma, Geneva, sans-serif; font-size:14px">' .
                '<tr><td align="center"><span style="font-size:24px; color:#01519c" ><strong>Reporte de imagen ofensiva</strong></span></td></tr>' .
                '<tr><td><br>El usuario <strong>' . $user_name . '</strong> envia el siguiente reporte:<br><br>' .
                '<strong>Estado:</strong> <br>' . $state . '<br><br><strong>Comentario:</strong> <br>' . $comment . '.<br />En la url se puede encontrar la imagen ofensiva: ' . $url . '</td></tr>' .
                '</table></td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table></td></tr></table>';

        try {
            $mail = new Zend_Mail();
            $mail->setBodyHtml($html);
            $mail->setFrom('hmappfb@gmail.com', 'Housemarket');
            $mail->addTo('housemarket.app@gmail.com', 'Housemarket Support');
            $mail->addTo('esomarriba@hmapp.com', 'Eduardo Somarriba L.');
            $mail->setSubject('Reporte de imagenes ofensivas');
            $mail->send();
            $this->view->message = 'Your report has been received!';
        } catch (Exception $ex) {
            $this->view->message = 'Your report was not sent! Please try again!';
        }

        $this->_helper->layout()->disableLayout();
    }

    /**
     * Accion a realizar como medida de responder al anuncio de una propiedad.
     */
    public function respondeAction() {
        echo "<fb:error><fb:message>This is the heading text for the message.</fb:message>555</fb:error>";
        $this->getHelper('viewRenderer')->setNoRender();
    }

    private function GiveLanguage($path, $language) {
        if (!file_exists($path)) {
            return "en";
        } else {
            return $language;
        }
    }

}

