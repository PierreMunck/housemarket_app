<?php
require_once 'Hm/HeaderMeta.php';

/**
 * Controller por defecto
 */
class MainController extends Zend_Controller_Action {

	protected $translator;
	protected $cfg;
	protected $db;
	protected $userInfo;
	protected $currentActionNav;
	protected $fbparams;
	protected $app;
	protected $fbDgt;
	
    /**
     * Inicializacion del controller
     */
    public function init() {
    	$this->translator=Zend_Registry::get('Zend_Translate');
    	$this->cfg = Zend_Registry::get('config');
    	$this->db = Zend_Registry::get('db');
		if(Zend_Registry::isRegistered('user_info')){
	    	$this->userInfo=Zend_Registry::get('user_info');
		}
    	$this->fbparams = Zend_Registry::get('fbparams');
    	$this->app = Zend_Registry::get('app');
    	$this->fbDgt = Dgt_Fb::getInstance();
    	$this->view->headerMeta = new Hm_HeaderMeta();
    	// recuperación de información si el user tiene un curto para roomates o busca cuarto
    	//Decidir si que menu mostrar al Roommate
    	$this->view->seeEditRoom = false;
    	$this->view->seeEditPerfil = false;
    	$this->view->seeAddRoommate =false;
    	// calculo del tipo de usuario en la parte roommate
    	$params = $this->_request->getParams();
    	if($params['controller'] == 'roommates'){
    		$uid = Zend_Registry::get('uid');
    	
    		$roommate = $this->db->fetchAll("SELECT * FROM RMTRoommate WHERE Uid = " . $uid);
    		if(is_array($roommate) && !empty($roommate)){
    			$this->view->CodigoRoommate = $roommate[0]->CodigoRoommate;
    		}
    		$roomOwner = $this->db->fetchAll("SELECT * FROM RMTCuarto WHERE Uid = " . $uid);
    		if(is_array($roomOwner) && !empty($roomOwner)){
    			$this->view->CodigoCuarto = $roomOwner[0]->CodigoCuarto;
    		}
    		if(count($roomOwner) > 0){
    			//Editar Cuarto
    			$this->view->seeEditRoom = true;
    		}elseif(count($roommate) > 0){
    			//Editar perfil
    			$this->view->seeEditPerfil = true;
    		}else{//Agregar cuarto o perfil de roommate
    			$this->view->seeAddRoommate = true;
    		}
    	}

    	//header fb
    	$this->view->headerMeta->setProperty('fb:admins','100000804282863,766658592,100000041940062');
    	$this->view->headerMeta->setProperty('fb:app_id',$this->cfg['facebook_appid']);
    	
    	//Meta clasic
    	$this->view->headerMeta->setName('title','House Market');
    	$this->view->headerMeta->setName('description',$this->view->appDescription);
    	
    	// default meta Og
    	$this->view->headerMeta->setProperty('og:title','Housemarket');
    	$this->view->headerMeta->setProperty('og:type','website');
    	$this->view->headerMeta->setProperty('og:url', $this->view->fbAppUrl);
    	$this->view->headerMeta->setProperty('og:image','https://photos-h.ak.fbcdn.net/photos-ak-snc1/v27562/245/109002199131497/app_1_109002199131497_7294.gif');
    	$this->view->headerMeta->setProperty('og:site_name','Housemarket');
    	$this->view->headerMeta->setProperty('og:country-name','Nicaragua');
    	$this->view->headerMeta->setProperty('og:description',$this->view->appDescription);

    }

    /**
     * Antes de procesar alguna accion
     */
    public function preDispatch() {
        
    }

    /**
     * 
     * fonction de retour de label de navigation courant
     */
    public function getCurrentLabelnav() {
    	return '';
    }
    
    /**
    *
    * fonction de retour de label de navigation courant
    */
    public function getCurrentActionNav() {
    	return $this->currentActionNav;
    }
    /**
     * Buscar en el mapa
     */
    public function navAction() {
    	
    	$this->view->fbAppUrl = $this->cfg['fb_app_url'];
    	$this->view->validUrl = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    	$this->view->indexUrl = $this->cfg['webhost'];
    	$this->view->localAppUrl = $this->cfg['local_app_url'];
    	$this->view->facebookAppId = $this->cfg['facebook_appid'];
    	$this->view->facebookApiKey = $this->cfg['facebook_api_key'];
    	
    	$this->view->uv = substr($this->view->validUrl, 21, 1);
    	
    	$this->view->currentActionNav = $this->getCurrentActionNav();
    	$this->view->currentLabelNav = $this->getCurrentLabelnav();
    	
    	$this->view->fbparams = $this->fbparams;
    	$this->view->appDescription = $this->app['description'];
    	//echo $urlvalid;
    	$uv = substr($this->view->validUrl, 21, 1);
    	
    	// recuperación de información si el user tiene el derecho de broker (see all)
    	if (isset($this->userInfo) && isset($this->userInfo['nbPropriedad']) && $this->userInfo['nbPropriedad'] > 0) {
    		$this->view->seeAllTab = 1;
    	} else {
    		$this->view->seeAllTab = 0;
    	}
    	
    	//Preguntar si tiene agregada la aplicacion como fan page
    	$pages = Zend_Registry::get('pages');
    	
    	
    	$this->view->hasAppAdded = true;
    	$columns_has_pagged = $pages->obtenerColumna('has_added_app');
    	if($columns_has_pagged != null and is_array($columns_has_pagged)){
    		foreach ($pages->obtenerColumna('has_added_app') as $page) {
    			if ($page === 0) {
    				$this->view->hasAppAdded = false;
    				break;
    			}
    		}
    	}
    	
    	
    	
    }
    
    public function setOgVariable($variables,$value = null) {
    	if(is_string($variables) && !is_null($value)){
    		$variables_array = array($variables => $value);
    		$variables = $variables_array;
    	}
    	
    	if(!is_array($variables)){
    		return null;
    	}
    	foreach ($variables as $key => $value){
    		$this->view->headMeta()->setProperty('og:'. $key,$value);
    	}
    	
    }

}

?>