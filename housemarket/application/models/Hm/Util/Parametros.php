<?php

class Hm_Util_Parametros extends Zend_Db_Table {

    /**
     * Nombre de la tabla
     * @var String
     */
    protected $_name = 'parametros';

    /**
     * Nombre de la llave primaria
     * @var String
     */
    protected $_primary = 'CodigoParametro';

    /**
     *
     * @var Zend_DbAdapter
     */
    public $dbAdapter;

}
?>