<?php
class Hm_Rmt_Roommate extends Zend_Db_Table {
    /**
     * Nombre de la tabla de Roommate para un cuarto
     * @var String
     */
    protected $_name = 'RMTRoommate';

    /**
     * Llave primaria de la tabla de RMTRoommate
     * @var String
     */
    protected $_primary = 'CodigoRoommate';

    public $CodigoRoommate = null;
    public $Titulo = null;
    public $Descripcion = null;
    public $Correo = null;
    public $TipoBusqueda = null;
    public $TipoPropiedad = null;
    public $TipoCuarto = null;
    public $TipoBano = null;
    public $MontoRenta = null;
    public $FechaMudanza = null;
    public $Uid = null;
    public $CantBusca = 1;
    public $EdadMinimaBusca = 16;
    public $EdadMaximaBusca = 100;
    public $FumadorBusca = 5;
    public $IdiomaBusca = null;
    public $OcupacionBusca = null;
    public $Escuela = null;
    public $GeneroBusca = null;
    public $MascotaBusca = 22;
    public $HijosBusco = 29;
    public $GeneroAcepta = 32;
    public $ParejaAcepta = 35;
    public $NinosAcepta = 38;
    public $MascotaAcepta = 41;
    public $FumadorAcepta = 44;
    public $OcupacionAcepta = 51;
    public $EdadMinimaAcepta = 16;
    public $EdadMaximaAcepta = 100;
    public $EstanciaMinima = null;
    public $Amueblado = null;
    public $EstadoRoommate = null;
    public $Direccion = null;
    public $Estado = null;
    public $Ciudad = null;
    public $ZipCode = null;
    public $Calle = null;
    public $CodigoPostal = null;
    public $Latitud = null;
    public $Longitud = null;
    public $Pais = null;
    
    const PUBLISHED_STATE = 64;
    const INACTIVE_STATE = 65;
    /**
     * List de los campos de la base de datos
     *
     * @return list de campo
     */
    private function CampoList(){
    	$campolist = array(
    		"CodigoRoommate",
		    "Titulo",
		    "Descripcion",
		    "Correo",
		    "TipoBusqueda",
		    "TipoPropiedad",
		    "TipoCuarto",
		    "TipoBano",
		    "MontoRenta",
		    "FechaMudanza",
		    "Uid",
		    "CantBusca",
		    "EdadMinimaBusca",
		    "EdadMaximaBusca",
		    "FumadorBusca",
		    "IdiomaBusca",
		    "OcupacionBusca",
    		"Escuela",
		    "GeneroBusca",
		    "MascotaBusca",
		    "HijosBusco",
		    "GeneroAcepta",
		    "ParejaAcepta",
		    "NinosAcepta",
		    "MascotaAcepta",
		    "FumadorAcepta",
		    "OcupacionAcepta",
		    "EdadMinimaAcepta",
		    "EdadMaximaAcepta",
		    "EstanciaMinima",
		    "Amueblado",
		    "EstadoRoommate",
		    "Direccion",
		    "Estado",
		    "Ciudad",
		    "ZipCode",
		    "Calle",
		    "CodigoPostal",
		    "Latitud",
		    "Longitud",
    	);
    	return $campolist;
    }

    function __construct($val = null) {
    	if($val != null){
    		foreach($this->CampoList() as $campo){
    			$campoval = lcfirst($campo);
    			if(isset($val[$campoval])){
    				$this->{$campo} = $val[$campoval];
    			}elseif (isset($val[$campo])){
    				$this->{$campo} = $val[$campo];
    			}
    		}
    	}
    	parent::__construct();
    }
    
    /**
     *
     * @param Array $parameters P�rametros recibido del POST de buscar cuarto
     */
    public function Validate($parameters = array()){
    	$isValid = true;
    	$f = new Zend_Filter_StripTags();
    
    	$errors = array();
    	$new = $f->filter($parameters["isNew"], "S");
    	$isNew = $new=="S"?true:false;
    
    	if(isset($parameters["cantBusca"])){
    		$searchCantity = $f->filter($parameters["cantBusca"], null);
    	}else{
    		$searchCantity = null;
    	}
    	$block = array();
    	if($searchCantity != null && !is_numeric($searchCantity)) {
    		$block[] = "<i18n>MSG_SEARCH_CANTITY_REQUIRED</i18n>";
    		$isValid = false;
    	}
    	if(count($block)> 0) {
    		$errors["Search Cantity"] = $block;
    	}
    
    	if(isset($parameters["amueblado"])){
    		$furnished = $f->filter($parameters["amueblado"], null);
    	}else{
    		$furnished = null;
    	}
    	$block = array();
    	if($furnished == null) {
    		$block[] = "<i18n>MSG_FURNISHED_REQUIRED</i18n>";
    		$isValid = false;
    	} else {
    		if(!is_numeric($furnished) ) {
    			$block[] = "<i18n>MSG_FURNISHED_REQUIRED</i18n>";
    			$isValid = false;
    		}
    	}
    	if(count($block)> 0) {
    		$errors["Accommodation Type"] = $block;
    	}
    
    	if(isset($parameters["tipoCuarto"])){
    		$roomType = $f->filter($parameters["tipoCuarto"], null);
    	}else{
    		$roomType = null;
    	}
    	$block = array();
    	if($roomType == null) {
    		$block[] = "<i18n>MSG_ROOM_TYPE_REQUIRED</i18n>";
    		$isValid = false;
    	} else {
    		if(!is_numeric($roomType) ) {
    			$block[] = "<i18n>MSG_ROOM_TYPE_NO_VALID</i18n>";
    			$isValid = false;
    		}
    	}
    	if(count($block)> 0) {
    		$errors["Room Type"] = $block;
    	}
    
    	$bathType = $f->filter($parameters["tipoBano"], null);
    	$block = array();
    	if($bathType == null) {
    		$block[] = "<i18n>MSG_BATH_TYPE_REQUIRED</i18n>";
    		$isValid = false;
    	} else {
    		if(!is_numeric($bathType) ) {
    			$block[] = "<i18n>MSG_BATH_TYPE_NO_VALID</i18n>";
    			$isValid = false;
    		}
    	}
    	if(count($block)> 0) {
    		$errors["Bath Type"] = $block;
    	}
    
    	$isForRent = $f->filter($parameters["rentaUS"], null)==null?false:true;
    
    	if($isForRent) {
    		$precioAlquiler = $this->validanumcoma($f->filter($parameters["rentaUS"], null));
    		$block = array();
    		if($precioAlquiler == null || !is_numeric($precioAlquiler)) {
    			$block[] = "Please, enter a valid rent price";
    			$isValid = false;
    		}
    		if(count($block) > 0) {
    			$errors["Rent Price"] = $block;
    		}
    	}else{
    		$errors["Prices"] = array("Rent price is required");
    		$isValid = false;
    	}
    
    	//Estas validaciones son comunes a cuarto y roommate
    	if(!$isNew){
    		$estateRoom = $f->filter($parameters["estadoRoommate"], null);
    		$block = array();
    		if($estateRoom == null) {
    			$block[] = "<i18n>MSG_STATE_ROOM_REQUIRED</i18n>";
    			$isValid = false;
    		} else {
    			if(!is_numeric($estateRoom) ) {
    				$block[] = "<i18n>MSG_STATE_ROOM_REQUIRED</i18n>";
    				$isValid = false;
    			}
    		}
    		if(count($block)> 0) {
    			$errors["Room State"] = $block;
    		}
    	}
    
    	$propertyType = $f->filter($parameters["tipoPropiedad"], null);
    	$block = array();
    	if($propertyType == null) {
    		$block[] = "<i18n>MSG_PROPERTY_TYPE_REQUIRED</i18n>";
    		$isValid = false;
    	} else {
    		if(!is_numeric($propertyType) ) {
    			$block[] = "<i18n>MSG_PROPERTY_TYPE_NO_VALID</i18n>";
    			$isValid = false;
    		}
    	}
    	if(count($block)> 0) {
    		$errors["Property Type"] = $block;
    	}
    
    	$zipcode = $f->filter($parameters["zipCode"], null);
    	$block = array();
    	if($zipcode == null || strlen($zipcode) == 0) {
    		$block[] = "Please, enter a country code";
    		$isValid = false;
    	} else {
    		if(strlen($zipcode) > 64) {
    			$block[] = "Please, enter a valid country code";
    			$isValid = false;
    		}
    	}
    	if(count($block) > 0) {
    		$errors["Zip Code"] = $block;
    	}
    
    	$state = $f->filter($parameters["estado"], null);
    	$block = array();
    	if($state == null || strlen($state) < 1 || strlen($state) > 64) {
    		$block[] = "Please, enter a valid state";
    		$isValid = false;
    	}
    	if(count($block)>0) {
    		$errors["State"] = $block;
    	}
    
    	$ciudad = $f->filter($parameters["ciudad"], null);
    	$block = array();
    	if($ciudad == null or strlen($ciudad) < 0 || strlen($ciudad) > 64){
    		$block[] = "Please, enter a valid city";
    		$isValid = false;
    	}
    	if(count($block)>0) {
    		$errors["City"] = $block;
    	}
    
    	if(isset($parameters["calle"])){
    		$calle = $f->filter($parameters["calle"], null);
    	}else{
    		$calle = null;
    	}
    	
    	$block = array();
    	if($calle != null) {
    		if(strlen($calle) > 32) {
    			$block[] = "Please, enter a valid street";
    			$isValid = false;
    		}
    		if(count($block)>0) {
    			$errors["Street"] = $block;
    		}
    	}
    
    	$direccion = $f->filter($parameters["direccion"], null);
    	$block = array();
    	if($direccion == null || strlen($direccion) == 0 ) {
    		$block[] = "Please, enter an address";
    		$isValid = false;
    	} else {
    		if(strlen($direccion) > 255) {
    			$block[] = "Please, Address is too long. Address must be between 1 and  255 characters";
    			$isValid = false;
    		}
    	}
    	if(count($block)>0) {
    		$errors["Address"] = $block;
    	}
    
    
    	//Tipos de ocupantes que tiene
    	$smokerHave = $f->filter($parameters["fumadorBusca"], null);
    	$block = array();
    	if($smokerHave == null) {
    		$block[] = "<i18n>MSG_SMOKER_TYPE_REQUIRED</i18n>";
    		$isValid = false;
    	} else {
    		if(!is_numeric($smokerHave) ) {
    			$block[] = "<i18n>MSG_SMOKER_TYPE_NO_VALID</i18n>";
    			$isValid = false;
    		}
    	}
    	if(count($block)> 0) {
    		$errors["Smoker Have Type"] = $block;
    	}
    
    	$occupationHave = $f->filter($parameters["ocupacionBusca"], null);
    	$block = array();
    	if($occupationHave == null) {
    		$block[] = "<i18n>MSG_ROOM_OCCUPATION_REQUIRED</i18n>";
    		$isValid = false;
    	} else {
    		if(!is_numeric($occupationHave) ) {
    			$block[] = "<i18n>MSG_ROOM_OCCUPATION_NO_VALID</i18n>";
    			$isValid = false;
    		}
    	}
    	if(count($block)> 0) {
    		$errors["Occupation Have Type"] = $block;
    	}
    
    	$genderHave = $f->filter($parameters["generoBusca"], null);
    	$block = array();
    	if($genderHave == null) {
    		$block[] = "<i18n>MSG_ROOM_GENDER_NO_VALID</i18n>";
    		$isValid = false;
    	} else {
    		if(!is_numeric($genderHave) ) {
    			$block[] = "<i18n>MSG_ROOM_GENDER_NO_VALID</i18n>";
    			$isValid = false;
    		}
    	}
    	if(count($block)> 0) {
    		$errors["Gender Have Type"] = $block;
    	}
    
    	$petHave = $f->filter($parameters["mascotaBusca"], null);
    	$block = array();
    	if($petHave == null) {
    		$block[] = "<i18n>MSG_ROOM_PET_REQUIRED</i18n>";
    		$isValid = false;
    	} else {
    		if(!is_numeric($petHave) ) {
    			$block[] = "<i18n>MSG_ROOM_PET_NO_VALID</i18n>";
    			$isValid = false;
    		}
    	}
    	if(count($block)> 0) {
    		$errors["Pet Have Type"] = $block;
    	}
    
    	$childrenHave = $f->filter($parameters["hijosBusco"], null);
    	$block = array();
    	if($childrenHave == null) {
    		$block[] = "<i18n>MSG_ROOM_CHILDREN_REQUIRED</i18n>";
    		$isValid = false;
    	} else {
    		if(!is_numeric($childrenHave) ) {
    			$block[] = "<i18n>MSG_ROOM_CHILDREN_NO_VALID</i18n>";
    			$isValid = false;
    		}
    	}
    	if(count($block)> 0) {
    		$errors["Children Have Type"] = $block;
    	}
    
    	$languageHave = $f->filter($parameters["idiomaBusca"], null);
    	$block = array();
    	if($languageHave != null && !is_numeric($languageHave) ) {
    		$block[] = "<i18n>MSG_ROOM_LANGUAGE_NO_VALID</i18n>";
    		$isValid = false;
    	}
    
    	$minAgeHave = $f->filter($parameters["edadMinimaBusca"], null);
    	$block = array();
    	if($minAgeHave == null) {
    		$block[] = "<i18n>MSG_ROOM_MIN_AGE_REQUIRED</i18n>";
    		$isValid = false;
    	} else {
    		if(!is_numeric($minAgeHave) ) {
    			$block[] = "<i18n>MSG_ROOM_MIN_AGE_NO_VALID</i18n>";
    			$isValid = false;
    		}
    	}
    	if(count($block)> 0) {
    		$errors["Minimum Have Type"] = $block;
    	}
    
    	//Tipos de ocupantes que acepta
    	$genderAccept = $f->filter($parameters["generoAcepta"], null);
    	$block = array();
    	if($genderAccept == null) {
    		$block[] = "<i18n>MSG_ROOM_GENDER_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
    		$isValid = false;
    	} else {
    		if(!is_numeric($genderAccept) ) {
    			$block[] = "<i18n>MSG_ROOM_GENDER_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
    			$isValid = false;
    		}
    	}
    	if(count($block)> 0) {
    		$errors["Gender Accept Type"] = $block;
    	}
    
    	$coupleAccept = $f->filter($parameters["parejaAcepta"], null);
    	$block = array();
    	if($coupleAccept == null) {
    		$block[] = "<i18n>MSG_ROOM_COUPLE_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
    		$isValid = false;
    	} else {
    		if(!is_numeric($coupleAccept) ) {
    			$block[] = "<i18n>MSG_ROOM_COUPLE_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
    			$isValid = false;
    		}
    	}
    	if(count($block)> 0) {
    		$errors["Couple Accept Type"] = $block;
    	}
    
    	$childrenAccept = $f->filter($parameters["ninosAcepta"], null);
    	$block = array();
    	if($childrenAccept == null) {
    		$block[] = "<i18n>MSG_ROOM_CHILDREN_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
    		$isValid = false;
    	} else {
    		if(!is_numeric($childrenAccept) ) {
    			$block[] = "<i18n>MSG_ROOM_CHILDREN_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
    			$isValid = false;
    		}
    	}
    	if(count($block)> 0) {
    		$errors["Children Accept Type"] = $block;
    	}
    
	    $petAccept = $f->filter($parameters["mascotaAcepta"], null);
	    $block = array();
	    if($petAccept == null) {
	    	$block[] = "<i18n>MSG_ROOM_PET_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
	    	$isValid = false;
	    } else {
	    	if(!is_numeric($petAccept) ) {
	    		$block[] = "<i18n>MSG_ROOM_PET_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
	    		$isValid = false;
	    	}
	    }
	    if(count($block)> 0) {
	    	$errors["Pet Accept Type"] = $block;
	    }
	    
	    $smokerAccept = $f->filter($parameters["fumadorAcepta"], null);
	    $block = array();
	    if($smokerAccept == null) {
	    	$block[] = "<i18n>MSG_SMOKER_TYPE_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
	    	$isValid = false;
	    } else {
	    	if(!is_numeric($smokerAccept) ) {
	    		$block[] = "<i18n>MSG_SMOKER_TYPE_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
	    		$isValid = false;
	    	}
	    }
	    if(count($block)> 0) {
	    	$errors["Smoker Accept Type"] = $block;
	    }
	    
	    $occupationAccept = $f->filter($parameters["ocupacionAcepta"], null);
	    $block = array();
	    if($occupationAccept == null) {
	    	$block[] = "<i18n>MSG_ROOM_OCCUPATION_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
	    	$isValid = false;
	    } else {
	    	if(!is_numeric($occupationAccept) ) {
	    		$block[] = "<i18n>MSG_ROOM_OCCUPATION_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
	    		$isValid = false;
	    	}
	    }
	    if(count($block)> 0) {
	    	$errors["Occupation Accept Type"] = $block;
	    }
	    
	    $minAgeAccept = $f->filter($parameters["edadMinimaAcepta"], null);
	    $block = array();
	    if($minAgeAccept == null) {
	    	$block[] = "<i18n>MSG_ROOM_MIN_AGE_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
	    	$isValid = false;
	    } else {
	    	if(!is_numeric($minAgeAccept) ) {
	    		$block[] = "<i18n>MSG_ROOM_MIN_AGE_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
	    		$isValid = false;
	    	}
	    }
	    if(count($block)> 0) {
	    	$errors["Minimum Accept Type"] = $block;
	    }
	    
	    $maxAgeAccept = $f->filter($parameters["edadMaximaAcepta"], null);
	    $block = array();
	    if($maxAgeAccept == null) {
	    	$block[] = "<i18n>MSG_ROOM_MAX_AGE_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
	    	$isValid = false;
	    } else {
	    	if(!is_numeric($maxAgeAccept) ) {
	    		$block[] = "<i18n>MSG_ROOM_MAX_AGE_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
	    		$isValid = false;
	    	}
	    }
	    if(count($block)> 0) {
	    	$errors["Maximum Accept Type"] = $block;
	    }
    
    
    	if($minAgeAccept != null && $maxAgeAccept != null){
    		if($minAgeAccept > $maxAgeAccept){
    			$errors["Ages Compare Rmt"] = "MSG_AGES_COMPARE_RESTRICTING <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
    			$isValid = false;
    		}
    	}
    
    	##### Resto de validaciones #########
    
	    $availableDate = $f->filter($parameters["fechaMudanza"], null);
	    $format = "yyyy-MM-dd";
	    $block = array();
	    if($availableDate == null || strlen($availableDate) == 0) {
	    	$block[] = "Please, enter available date";
	    	$isValid = false;
	    } else {
	    	if(!Zend_Date::isDate($availableDate, $format)) {
	    		$block[] = "Availability Date is not valid";
	    		$isValid = false;
	    	}elseif($isNew){
	    		$today = new Zend_Date(null, $format);
	    		$xmas = new Zend_Date($availableDate, $format);
	    		$diffTs = $xmas->get(Zend_Date::TIMESTAMP) - $today->get(Zend_Date::TIMESTAMP);
	    		$tsInDays = floor((($diffTs / 60) / 60) / 24);
	    		if($tsInDays < 0){
	    			$block[] = "Availability date is lesser than actual date";
	   				$isValid = false;
	    		}
	    	}
	    }
	    if(count($block)>0) {
	    	$errors["Date"] = $block;
	    }
	    
	    $stayMonth = $f->filter($parameters["stayMonth"], null);
	    $block = array();
	    if($stayMonth == null) {
	    	$block[] = "Please, choose number of stay Months";
	    	$isValid = false;
	    } else {
	    	if(!is_numeric($minAgeAccept) ) {
	    		$block[] = "Stay Months type selected is not a valid number";
	    		$isValid = false;
	    	}
	    }
	    if(count($block)> 0) {
	    	$errors["Stay Month"] = $block;
	    }
	    
	    $titleRoommate = $f->filter($parameters["titulo"], null);
	    $block = array();
	    if($titleRoommate == null || strlen($titleRoommate) == 0) {
	    	$block[] = "Please, enter a title";
	    	$isValid = false;
	    } else {
	    	if(strlen($titleRoommate) > 128) {
	    		$block[] = "Title is too long, please enter a title equal or less than characters";
	    		$isValid = false;
	    	}
	    }
	    if(count($block)>0) {
	    	$errors["Title"] = $block;
	    }
	    
	    $EmailRoommate = $f->filter($parameters["correo"], null);
	    $block = array();
	    if($EmailRoommate == null || strlen($EmailRoommate) == 0) {
	    	$block[] = "Please, enter an Email";
	    	$isValid = false;
	    }
	    
	    if(count($block)>0) {
	    	$errors["Email"] = $block;
	    }
	    
	    $DescRoom = $f->filter($parameters["descripcion"], null);
	    $block = array();
	    if($DescRoom != null) {
	    	if(strlen($DescRoom) > 512) {
	    		$block[] = "Description is too long, please enter a title equal or less than 512 characters";
	    		$isValid = false;
	    	}
	    }
	    if(count($block)>0) {
	    	$errors["Description"] = $block;
	    }
	    
	    $latitud = $f->filter($parameters["latitud"], null);
	    $block = array();
	    if($latitud == null || !is_numeric($latitud) ) {
	    	$block[] = "Please, enter a valid latitud";
	    	$isValid = false;
	    }
	    if(count($block)>0) {
	    	$errors["Latitud"] = $block;
	    }
	    
	    $longitud = $f->filter($parameters["longitud"], null);
	    $block = array();
	    if($longitud == null || !is_numeric($longitud)) {
	    	$block[] = "Please, enter a valid longitud";
	    	$isValid = false;
	    }
	    if(count($block) > 0) {
	    	$errors["Longitud"] = $block;
	    }
	    return array("valid" =>$isValid , "errors"=> $errors);
    }
    /**
     * Guarda los datos para el Roommate en Tengo cuarto.
     * @global Array $_FILES Contiene las referencias a las fotos de la propiedad
     * @param Array $parameters Contiene los datos de la propiedad
     * @return <type> Array
     */
    public function SaveOrUpdate($parameters = array()) {
        global $_FILES;

        $id = null;
        $status = array();
        $isValid = $this->Validate($parameters);
        if(!$isValid["valid"] && count($isValid["errors"]) > 0) {
            // Mandar en el flash messenger todos los errores.
            return array("fail"=>true, "reasons"=>$isValid["errors"]);
        }

        $f = new Zend_Filter_StripTags();

        // Recuperar los datos del registro
        $roommateArr = array();
        foreach($this->CampoList() as $campo){
        	$campoval = lcfirst($campo);
        	if(isset($parameters[$campoval])){
        		$roommateArr[$campo] = $f->filter($parameters[$campoval], null);
        	}else{
        		$roommateArr[$campo] = null;
        	}
        }
        if(isset($parameters['rentaUS'])){
        	$roommateArr['MontoRenta'] = $this->validanumcoma($f->filter($parameters["rentaUS"], null));
        }else{
        	$roommateArr['MontoRenta'] = null;
        }
        
        $roommateArr['Uid'] = Zend_Registry::get('uid');
        
        if(isset($parameters["cantBusca"])){
        	$roommateArr['CantBusca'] = $f->filter($parameters["cantBusca"], 1);
        }else{
        	$roommateArr["CantBusca"] = 0;
        	if(isset($roommateArr["TipoBusqueda"]) && $roommateArr["TipoBusqueda"] == 66){
        		$roommateArr["CantBusca"] = 1;
        	}
        }
        if(isset($parameters['edadMaximaHabitan'])){
        	$roommateArr["EdadMaximaBusca"] = $f->filter($parameters["edadMaximaHabitan"], 0);
        }else{
        	$roommateArr["EdadMaximaBusca"] = null;
        }
        
        if(isset($parameters["stayMonth"])){
        	$roommateArr["EstanciaMinima"] = $f->filter($parameters["stayMonth"], null);
        }else{
        	$roommateArr["EstanciaMinima"] = null;
        }
        

        $publicado = $f->filter($parameters["publicado"], "S");
        
        $new = $f->filter($parameters["isNew"], "S");
        $isPublicada = $publicado=="S"?true:false;
        $isNew = $new=="S"?true:false;
        
        if($isNew) {
            // Determinar si se puede crear el registro del cliente
            $cliente = new Hm_Cli_Cliente();

            $result=$cliente->CreateCliente(Zend_Registry::get('uid'));
            if(is_array($result) && $result["status"]===false && $result["reason"] != "Duplicated") {
                return array("fail"=>true, "reasons"=>$result["reason"]);
            }

            $roommateArr["FechaRegistro"] = new Zend_Db_Expr('CURDATE()');           
        }

        if($new==false) {
            $roommateArr["FechaModifica"] = new Zend_Db_Expr('CURDATE()');
        }
        try {
	        if($isNew) { // Crear
	            $result = $this->insert($roommateArr);
	            $id = $this->getAdapter()->lastInsertId();
	        } else { // Actualizar
	        	$id = $this->CodigoRoommate;
	            $result = $this->update($roommateArr, $this->getAdapter()->quoteInto("CodigoRoommate=?", $this->CodigoRoommate) );
	        }
        } catch(Exception $ex) {
        	return array("fail" => true, "reasons"=>$ex->getMessage());
        }
        $roommate = new Hm_Rmt_Roommate();
        $rsRoommate = $roommate->find($id);
        if($rsRoommate == null || $rsRoommate->count() == 0) {
            return array("fail"=>true, "reasons"=>"Estate not created");
        }
        return array("id" => $id, "status" => $status);
    }

    public function DeleteRoommate($roommateId){
        $translator = Zend_Registry::get('Zend_Translate'); 
        $status = array();
        $result = 0;
        if(is_numeric($roommateId)){            
            try {
                $roommateRowset = $this->find($roommateId);
                if($roommateRowset != null and $roommateRowset->count() > 0){
                    $roommateItem = $roommateRowset->current();
                    $result = $roommateItem->delete();
                    //$result = $this->delete($this->getAdapter()->quoteInto('CodigoCuarto=?', $roomId));
                }
            } catch (Exception $exc) {}
        }
        if($result > 0){
            $status = array("status"=>"success");
        }else{
            $status = array("fail"=>true, "reasons"=> $translator->getAdapter()->translate("DEL_FAIL"));
        }
        return $status;
    }

    /*validando los numeros con coma*/
    public function validanumcoma($numero){
        $resultado=$numero;
        if($numero!=null && strlen($numero)>2) {
            $validando=explode('.',$numero);
            if(count($validando)<=2)
                $resultado=str_replace(',', '', $numero);
        }
        return $resultado;
    }


    /**
    * Recupera las fotos de los cuartos destacados, ordenados por cr�ditos
    * @param <type> $country Codigo del pa�s
    * @param <type> $city Nombre de la ciudad sobre la cual mostrar los resultados
    * @param <type> $coordinates Coordenadas del mapa actual
    * @param <type> $start offset inicial de consulta
    * @param <type> $limit offset final o cota superior de consulta
    * @return Array_OBJ Listado de fotos a desplegar como destacadas
    */
    public function GetFotosDestacadas($country, $city, $coordinates, $start, $limit) {
        $this->dbAdapter = Zend_Registry::get('db');

        $publicada = "Publicada";
        $estadoCredito = 1;

        $sql_select = 'SELECT DISTINCT '.
                'rmt.CodigoRoommate id, '.
                'rmt.Uid '.
                ' FROM '.
                'RMTRoommate rmt '.
                ' INNER JOIN catpais p ON (p.ZipCode = rmt.ZipCode) ';

        $sql_where = array();
        $sql_where[] = " rmt.EstadoRoommate = " . self::PUBLISHED_STATE;

        if($coordinates && is_array($coordinates)) {
            $sql_where[] = " MBRContains(GeomFromText('Polygon((". $coordinates["minX"] . " " . $coordinates["minY"] . " " . "," . $coordinates["minX"] . " " . $coordinates["maxY"] . "," . $coordinates["maxX"] . " " . $coordinates["maxY"] . "," . $coordinates["maxX"] . " " . $coordinates["minY"] . "," . $coordinates["minX"] . " " . $coordinates["minY"]. "))' ), GeomFromWKB(Point(rmt.Longitud, rmt.Latitud))) ";
        }

        if(count($sql_where) > 0) {
            $sql_where = " WHERE " . implode(" AND " , $sql_where);
        } else {
            $sql_where = "";
        }

        $sql_order_by = "ORDER BY id DESC";
        $sql_limit = "LIMIT $start, $limit";
        $sql = $sql_select . " " . $sql_where . " " . $sql_order_by . " " . $sql_limit;

        $resultSet = $this->DoQuery($sql);

        $result = $this->InfoFacebookUsers($resultSet);

        return $result;
    }

    /**
     * Fotos filtradas de los cuartos
     * @param String $city Nombre de la ciudad sobre la cual mostrar los resultados
     * @return RowSet Listado de fotos a desplegar como destacadas
     */
    public function GetFotosFiltradas(
            $uid,
            $about,
            $conditions,
            $occupiers,
            $ideal,
            $coordinates,
            $min_price,
            $max_price,
            $start,
            $limit, $actual_roommate = null) {

        $translator = Zend_Registry::get('Zend_Translate');
        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $sql_where_conditions = array();

        $localLangague = Zend_Registry::get('Zend_Locale');
        $resultingQuery = $this->PrepareWherePartStatement($uid, "rc", $about, $conditions, $occupiers, $ideal, $coordinates, $min_price, $max_price);

        $sql_part = $resultingQuery["query"];

        $sql_select = 'SELECT DISTINCT ' .
                ' rc.CodigoRoommate as profilId, ' .
                ' (CASE WHEN CHAR_LENGTH(rc.Titulo) > 20 THEN CONCAT(SUBSTR(rc.Titulo,1,20),"...") ELSE rc.Titulo END) as Titulo, ' .
                ' rc.Descripcion as Descripcion, ' .
                ' rc.Direccion as Direccion, ' .
                ' rc.Estado as Estado, ' .
                ' rc.Ciudad as Ciudad, ' .
                ' rc.ZipCode as Zipcode, ' .
                ' cvi.ValorIdioma as Tipobusqueda, ' .
                ' FORMAT(rc.MontoRenta,2) as MontoRenta, ' .
                ' cm.SimboloMoneda as Simbolomoneda, ' .
                ' cm.CodigoEstandar as Codigomoneda, ' .
                ' rc.Uid ' .
                ' FROM ' .
                ' RMTRoommate rc '.
                ' INNER JOIN CATValorDominioIdioma cvi ON rc.TipoBusqueda = cvi.CodigoValor '.
                ' INNER JOIN catpais ON rc.ZipCode = catpais.ZipCode '.
                ' LEFT OUTER JOIN catmoneda cm ON catpais.CodigoMoneda = cm.CodigoMoneda '.
                $sql_part;

        $sql_where = ' WHERE ';
        $sql_where_conditions[] = $resultingQuery["conditions"];
        $sql_where_conditions[] = " rc.EstadoRoommate = " . self::PUBLISHED_STATE;
        $sql_where_conditions[] = " cvi.CodigoIdioma = '$localLangague'";
        
        //Para cuando este en el perfil de un determinado roommate
        if($actual_roommate != null){
            $sql_where_conditions[] = "rc.CodigoRoommate != " . $actual_roommate;
        }

        if(count($sql_where_conditions) > 0) {
            $sql_where .= implode(" AND ", $sql_where_conditions);
        } else {
            $sql_where = "";
        }

        $sql_order_by = " ORDER BY profilId DESC";

        $sql_limit = " LIMIT ". $start . ", " . $limit;

        $sql = $sql_select . $sql_where . $sql_order_by . $sql_limit;

        $resultset = $this->DoQuery($sql, null);
        
        return $this->GetUserFbPhoto($resultset);
    }

     /**
     * Total de fotos filtradas
     */
    public function GetTotalFotosFiltradas(
           $uid,
            $about,
            $conditions,
            $occupiers,
            $ideal,
            $min_price,
            $max_price,
            $coordinates){
    	
        $sql_where_conditions = array();
        $resultingQuery = $this->PrepareWherePartStatement($uid, "rc", $about, $conditions, $occupiers, $ideal, $coordinates, $min_price, $max_price);

        $sql_select = 'SELECT DISTINCT ' .
                'count(DISTINCT rc.CodigoRoommate) as numero ' .
                'FROM ' .
                'RMTRoommate rc ' . $resultingQuery["query"];

        $sql_where = ' WHERE ';
        $sql_where_conditions[] = $resultingQuery["conditions"];
        $sql_where_conditions[] = " rc.EstadoRoommate = " . self::PUBLISHED_STATE;

        if(count($sql_where_conditions) > 0) {
            $sql_where .= implode(" AND ", $sql_where_conditions);
        } else {
            $sql_where = "";
        }

        $sql = $sql_select . $sql_where;

        $totalFotos = $this->DoQuery($sql, null);

        return $totalFotos[0]->numero;
    }

     /**
     * Realiza una consulta con la base de datos
     * @param String $sql Consulta a realizar
     * @param array $params Parametros que se le pasan a la consulta a realizar
     * @return RowSet Resultado de la consulta
     */
    private function DoQuery ($sql, $params=null) {
        $db= $this->dbAdapter = Zend_Registry::get('db');
		$data = array();
        if (isset($params)) {
            try {
                $rs = $this->dbAdapter->query($sql,$params);    
            } catch (Zend_Db_Exception $exc) {
            }
        } else {
            try {
                $rs = $this->dbAdapter->query($sql, null);
            } catch (Exception $exc) {

            }
        }


        try {
           $rs=new stdClass();
            $rs->rows=$db->fetchAll($sql);
            $data = $rs->rows;
        } catch (Zend_Db_Exception $exc) {
        	print_r($exc);
        }

        return $data;
    }

    public function InfoFacebookUsers($resultSet){
		$cli = new Hm_Cli_Cliente();
        $clientesFb = array();
        // Recuperamos los ids para luego extraer la informacion en facebook.
        $uids = array();
        if(is_array($resultSet)){
            foreach($resultSet as $cliente) {
                $uids[$cliente->Uid] = $cliente->Uid;
                $clientInfo[$cliente->Uid] = $cliente;
            }

            // Extraemos la informacion en facebook
            $fbDgt = Dgt_Fb::getInstance();
            $clientesFbinfo = $fbDgt->execFql("SELECT uid, first_name, last_name, pic_square, profile_url FROM user WHERE uid IN (" . implode(",", $uids) . ") AND is_app_user");
        }
        
        // Ordenamos los usuarios de facebooks por el el uid, para luego ser consultados

        $result = array();
        
		if(is_array($clientesFbinfo)) {
			foreach($clientInfo as $cliente) {
				foreach($clientesFbinfo as $infoFB) {
					if($infoFB["uid"] == $cliente->Uid){
						$clientInfo[$cliente->Uid]->first_name = $infoFB["first_name"];
						$clientInfo[$cliente->Uid]->last_name = $infoFB["last_name"];
						$clientInfo[$cliente->Uid]->pic_square = $infoFB["pic_square"];
						$clientInfo[$cliente->Uid]->profile_url = $infoFB["profile_url"];
						break;
					}
				}
			}
		}
		
        $lengChunkString = 10;
		if(is_array($clientInfo)) {
			foreach($clientInfo as $cliente) {
				
				if(strlen($cliente->first_name) > $lengChunkString){
					$first_name =  implode("<br/>",$cli->SectionString($cliente->first_name));
				} else{
					$first_name = $cliente->first_name;
				}
				if(strlen($cliente->last_name) > $lengChunkString){
					$last_name =  implode("<br/>",$cli->SectionString($cliente->last_name));
				} else{
					$last_name = $cliente->last_name;
				}
				
				$dbCliente["pic_square"] = $cliente->pic_square;
				$dbCliente["profile_url"] = $cliente->profile_url;
				$dbCliente["id"] = $cliente->Uid;
				$dbCliente["nombre"] =  $first_name .' '.$last_name;
				
				$result[] = $dbCliente;
			}
			return $result;
		}
	}

    public function GetUserFbPhoto($resultset){
        $cli = new Hm_Cli_Cliente();
         $clientesFb = array();
        // Recuperamos los ids para luego extraer la informacion en facebook.
        $uids = array();
        if(is_array($resultset)){
            foreach($resultset as $roommate) {
                $uids[$roommate->Uid] = $roommate->Uid;
                $roommateInfo[$roommate->Uid] = $roommate;
            }

            $registry = Zend_Registry::getInstance();
            $fb_client = $registry->get('fb');
            // Extraemos la informacion en facebook
            $fbDgt = Dgt_Fb::getInstance();
            $clientesFb = $fbDgt->execFql("SELECT uid, pic_square FROM user WHERE uid IN (" . implode(",", $uids) . ") AND is_app_user");
            
        }

        // Ordenamos los usuarios de facebooks por el el uid, para luego ser consultados
       $resultFb = array();
       $result = array();

	if(is_array($clientesFb)) {
           foreach($clientesFb as $roommate) {
               $resultFb[$roommate["uid"]] = $roommate;
           }
	}

        $lengChunkString = 10;
	if(is_array($roommateInfo)) {
           foreach($roommateInfo as $roommate) {
               $fbClient = $resultFb[$roommate->Uid];
               $dbCliente["titulo"]     = $roommate->Titulo;
               $dbCliente["descripcion"] = $roommate->Descripcion;
               $dbCliente["direccion"] = $roommate->Direccion;
               $dbCliente["estado"] = $roommate->Estado;
               $dbCliente["ciudad"] = $roommate->Ciudad;
               $dbCliente["zipcode"] = $roommate->Zipcode;
               $dbCliente["pic_square"] = $fbClient["pic_square"];
               $dbCliente["rent"] = $roommate->MontoRenta;
               $dbCliente["id"] = $roommate->profilId;
               $dbCliente["simbolomoneda"] = $roommate->Simbolomoneda;
               $dbCliente["codigomoneda"] = $roommate->Codigomoneda;
               $dbCliente["tipobusqueda"] = $roommate->Tipobusqueda;

               if(isset($roommate->Longitud) && isset($roommate->Latitud)){
                   $dbCliente["longitud"] = $roommate->Longitud;
                   $dbCliente["latitud"] = $roommate->Latitud;
               }

               $result[] = $dbCliente;
           }
           return $result;
       }
    }

    /**
     * Filtro a aplicar cuando se pulsa el boton: Ubicar en el mapa
     * Debe retornar un listado de las propiedades con sus fotos destacadas.
     *
     * @param String $country Codigo del Pais
     * @param String $city Nombre de la ciudad
     * @param String $category Codigo de la categoria
     * @param String $type Tipo de propiedad ( Accion )
     * @param Double $min_price Precio minimo
     * @param Double $max_price Precio maximo
     * @param Array $extra_filters Parametros extras dependiendo de la categoria y si los proporciona
     * @param Integer $start Inicio del listado
     * @param Integer $limit Fin del listado
     * @return Array Listado de las propiedades.
     */
    public function FilterStatesOnDir(
            $uid,
            $about,
            $occupier,
            $general,
            $ideal_partner,
            $coordinates,
            $start = 0,
            $limit = 10,
            $min_price,
            $max_price) {

        $sql_filter = array();
        $localLangague = Zend_Registry::get('Zend_Locale');

        $resultingQuery = $this->PrepareWherePartStatement($uid, "rc", $about, $general, $occupier, $ideal_partner, $coordinates, $min_price, $max_price);

        $sql_select = "SELECT DISTINCT ".
                'rc.CodigoRoommate as profilId, '.
                ' (CASE WHEN CHAR_LENGTH(rc.Titulo) > 20 THEN CONCAT(SUBSTR(rc.Titulo,1,20),"...") ELSE rc.Titulo END) as Titulo, ' .
                'cvi.ValorIdioma as Tipobusqueda, '.
                'rc.Descripcion as Descripcion, '.
                'rc.Direccion as Direccion, '.
                'rc.Estado as Estado, '.
                'rc.Ciudad as Ciudad, '.
                'rc.ZipCode as Zipcode, '.
                'FORMAT(rc.MontoRenta,2) as MontoRenta, '.
                'cm.SimboloMoneda as Simbolomoneda, '.
                'rc.Latitud as Latitud, '.
                'rc.Longitud as Longitud, '.
                'cm.CodigoEstandar as Codigomoneda, '.
                'rc.Uid '.
                //"coalesce((select sum(coalesce(cr.cantidad,0)) from crecreditopropiedad cr left outer join crecredito c on (c.CodigoCredito = cr.CodigoCredito) where cr.CodigoPropiedad = p.CodigoPropiedad and (c.FechaVence >= Current_Date() OR c.FechaVence is null) and c.EstadoCredito='A' ),0) as credito ".
                'FROM '.
                'RMTRoommate rc '.
                ' INNER JOIN CATValorDominioIdioma cvi on (rc.TipoBusqueda = cvi.CodigoValor) '.
                ' INNER JOIN catpais pa ON (rc.ZipCode=pa.ZipCode) 
                  LEFT OUTER JOIN catmoneda cm ON (pa.CodigoMoneda = cm.CodigoMoneda) 
                  ' . $resultingQuery["query"];


        $sql_filter[] = " rc.EstadoRoommate = " . self::PUBLISHED_STATE;
        $sql_filter[] = $resultingQuery["conditions"];
        $sql_filter[] = " cvi.CodigoIdioma = '$localLangague'";

        if(count($sql_filter) > 0) {
            $sql_filter = " WHERE " . implode(" AND ", $sql_filter);
        } else {
            $sql_filter = "";
        }

        $sql_orderBy = " ORDER BY rc.CodigoRoommate DESC";

        $sql = $sql_select . $sql_filter . $sql_orderBy;

        $resultset = $this->DoQuery($sql, null);
        return $this->GetUserFbPhoto($resultset);
    }


    public function PrepareWherePartStatement($uid,
            $table_roommate,
            $about,
            $conditions,
            $occupiers,
            $ideal,
            $coordinates,
            $min_price = null,
            $max_price = null) {

        $sql_query = array();
        $sql_filter = array();
        $room = new Hm_Rmt_Cuarto();
        
        $rs = $room->fetchAll(
                    $room->select()
                    ->where('Uid = ? ', $uid));
        if($rs !=null and $rs->count() > 0){
            $curRoom = $rs->current();

            $room = array();

            $roomValueByLanguage = array(
                    'TipoAlojamiento'  => $curRoom->TipoAlojamiento,
                    'TipoPropiedad'    => $curRoom->TipoPropiedad,
                    'TipoCuarto'       => $curRoom->TipoCuarto,
                    'TipoBano'         => $curRoom->TipoBano,
                    'FumadorHabitan'   => $curRoom->FumadorHabitan,
                    'OcupacionHabitan' => $curRoom->OcupacionHabitan,
                    'GeneroHabitan'    => $curRoom->GeneroHabitan,
                    'MascotaHabitan'   => $curRoom->MascotaHabitan,
                    'OrientacionHabitan'=> $curRoom->OrientacionHabitan,
                    'HijosHabitan'      => $curRoom->HijosHabitan,
                    'GeneroAcepta'      => $curRoom->GeneroAcepta,
                    'ParejaAcepta'      => $curRoom->ParejaAcepta,
                    'NinosAcepta'       => $curRoom->NinosAcepta,
                    'MascotaAcepta'     => $curRoom->MascotaAcepta,
                    'FumadorAcepta'     => $curRoom->FumadorAcepta,
                    'OrientacionAcepta' => $curRoom->OrientacionAcepta,
                    'OcupacionAcepta'   => $curRoom->OcupacionAcepta,
            );

           $roomValueByVal = array(
                    'MaxCapacidad', 'EspacioDisponible', 'FechaDisponible', 'CantHabitan', 'EdadMinimaHabitan', 
                    'EdadMaximaHabitan', 'EdadMinimaAcepta', 'EdadMaximaAcepta', 'EstanciaMinima', 'MontoRenta', 'IdiomaHabitan'
            );
			
           $rmtCuardoObj = new Hm_Rmt_Cuarto();
           $fieldRoomTable = $rmtCuardoObj->GetValueDomains($roomValueByLanguage);

            foreach ($roomValueByLanguage as $index => $value) {
                if(array_key_exists($value, $fieldRoomTable)){
                    $room[$index] = $fieldRoomTable[$value];
                }
            }

           foreach ($roomValueByVal as $value) {
               $room[$value] = $curRoom->$value;
           }

            $_DOESNT_MATTER = "No importa";
            $_DONT_SHOW = "No mostrar";
            $_OTHER = "Otro";
            $_TABLE_VALUE_DOMAIN = "CATValorDominio";

            if($about != NULL){
                //Tipo de Propiedad
               $sql_query[] = "$_TABLE_VALUE_DOMAIN VTP ON " .$table_roommate .".TipoPropiedad = VTP.CodigoValor";
               $sql_filter[] = "(   ('" . $room["TipoPropiedad"]. "' = '$_OTHER' )" . " OR (
                                    VTP.NombreValor = '". $room["TipoPropiedad"] . "') OR
                                    (VTP.NombreValor = '$_DOESNT_MATTER')
                                ) ";

               //Cantidad de Habitantes
               $sql_filter[] = $room["MaxCapacidad"] . " >= ".$table_roommate.".CantBusca";

               //Tipo de Cuarto
               $sql_query[] = "$_TABLE_VALUE_DOMAIN VTC ON " . $table_roommate .".TipoCuarto = VTC.CodigoValor";
               $sql_filter[] = "(
                                    (VTC.NombreValor = '$_DOESNT_MATTER') OR
                                    (
                                        (VTC.NombreValor = 'Individual' AND '".$room["TipoCuarto"] ."' = 'Individual') OR
                                        (VTC.NombreValor = 'Compartido' AND '" .$room["TipoCuarto"] . "' = 'Dos personas') OR
                                        (VTC.NombreValor = 'Compartido' AND '" .$room["TipoCuarto"] . "' = '+2 Personas')
                                    )
                                 )";

               //Tipo de Ba�o
               $sql_query[] = $_TABLE_VALUE_DOMAIN ." VTB ON ". $table_roommate .".TipoBano = VTB.CodigoValor";
               $sql_filter[] = "(
                                     (VTB.NombreValor = '$_DOESNT_MATTER') OR
                                    ('".$room["TipoBano"] ."' = VTB.NombreValor)
                                )";
            }

            //Condiciones Generales
            if($conditions != NULL){
                $sql_filter[] = "(" .
                                "(". $table_roommate . ".MontoRenta >= " . $room["MontoRenta"] . ") OR " .
                                $room["MontoRenta"] . " = NULL OR " . $room["MontoRenta"] . " = 0"
                                .")";

                $sql_filter[] = "(".
                                   "(" . $table_roommate .".FechaMudanza >= '" . $room["FechaDisponible"] . "') OR " .
                                    $table_roommate . ".FechaMudanza = NULL OR '" . $room["FechaDisponible"] . "' = NULL " .
                                ")";

                $sql_filter[] = "(".
                                   "(" . $table_roommate .".EstanciaMinima >= " . $room["EstanciaMinima"] .") OR " .
                                   $table_roommate.".EstanciaMinima = NULL OR " . $room["EstanciaMinima"] . " = NULL " .
                                ")";
            }

            //Ocupantes Actuales
            if($occupiers != NULL){
                $sql_filter[] = "(" .
                                    "(" . $table_roommate .".EdadMinimaAcepta <= " . $room["EdadMinimaHabitan"] . " AND " .
                                        $table_roommate .".EdadMaximaAcepta >= " . $room["EdadMaximaHabitan"] .
                                    ") OR ".
                                    "(" . $table_roommate .".EdadMinimaAcepta = NULL AND " .$table_roommate .".EdadMaximaAcepta = NULL) OR" .
                                    "(" . $room["EdadMinimaHabitan"] . " = NULL OR " . $room["EdadMaximaHabitan"] . " = NULL )" .
                                ")";

                //Tipo de FumadorAcepta VTF
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTF ON ". $table_roommate . ".FumadorAcepta = VTF.CodigoValor";
                $sql_filter[] = "(".
                                    "(VTF.NombreValor = '" . $room["FumadorHabitan"] ."') OR " .
                                    "(VTF.NombreValor = '". $_DOESNT_MATTER ."') OR ".
                                    "('" .$room["FumadorHabitan"] . "' = '" . $_DONT_SHOW ."')".
                                ")";

               $sql_filter[] = "(".
                                "(" . $table_roommate. ".IdiomaBusca = '" . $room["IdiomaHabitan"] ."') OR ".
                                $table_roommate. ".IdiomaBusca = NULL OR '" . $room["IdiomaHabitan"] . "' = NULL" .
                             ")";

               //OcupacionAcepta VTOC
               $sql_query[] = $_TABLE_VALUE_DOMAIN. " VTOC ON ". $table_roommate . ".OcupacionAcepta = VTOC.CodigoValor";
               $sql_filter[] = "(".
                                "(VTOC.NombreValor = '" . $room["OcupacionHabitan"] . "') OR ".
                                "(VTOC.NombreValor = '" . $_DOESNT_MATTER . "') OR ".
                                "('". $room["OcupacionHabitan"] . "' = '" . $_OTHER . "')".
                               ")";

               //GeneroAcepta VTG
               $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTG ON " . $table_roommate . ".GeneroAcepta = VTG.CodigoValor";
               $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTPA ON " . $table_roommate . ".ParejaAcepta = VTPA.CodigoValor";
               $sql_filter[] = "(".
                                "(VTG.NombreValor = '" . $room["GeneroHabitan"] ."') OR ".
                                "(VTG.NombreValor = '" .$_DOESNT_MATTER . "') OR ".
                                "(VTPA.NombreValor = 'Si' AND '". $room["GeneroHabitan"] . "' = 'Pareja') OR ".
                                "(VTPA.NombreValor = '" . $_DOESNT_MATTER . "' AND '" . $room["GeneroHabitan"] . "' = 'Pareja')".
                             ")";

               //Mascota Acepta VTM
               $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTM ON " . $table_roommate . ".MascotaAcepta = VTM.CodigoValor";
               $sql_filter[] = "(".
                                   "(VTM.NombreValor = '" . $room["MascotaHabitan"] . "') OR " .
                                   "(VTM.NombreValor = '". $_DOESNT_MATTER ."') OR ".
                                   "('". $room["MascotaHabitan"] . "' = '" .$_DONT_SHOW . "')".
                               ")";

               //OrientacionAcepta VTO
               $sql_query[] = $_TABLE_VALUE_DOMAIN. " VTO ON " . $table_roommate . ".OrientacionAcepta = VTO.CodigoValor";
               $sql_filter[] = "(".
                                "(VTO.NombreValor = '" . $room["OrientacionHabitan"] . "') OR ".
                                "(VTO.NombreValor = '". $_DOESNT_MATTER . "') OR ".
                                "('". $room["OrientacionHabitan"] . "' = '" . $_DONT_SHOW . "')".
                               ")";

               //Ni�osAcepta VTN
               $sql_query[] = $_TABLE_VALUE_DOMAIN. " VTN ON " . $table_roommate . ".NinosAcepta = VTN.CodigoValor";
               $sql_filter[] = "(".
                                "(VTN.NombreValor = '" . $room["HijosHabitan"] . "') OR " .
                                "(VTN.NombreValor = '" . $_DOESNT_MATTER . "') OR ".
                                "('". $room["HijosHabitan"] . "' = '" . $_DONT_SHOW . "')" .
                               ")";
            }

            if($ideal != NULL){
                if($occupiers == NULL){
                    $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTG ON " . $table_roommate . ".GeneroAcepta = VTG.CodigoValor";
                }

                //GeneroBusca VTGB
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTGB ON " . $table_roommate . ".GeneroBusca = VTGB.CodigoValor";
                $sql_filter[] = "(" .
                                    "(VTG.NombreValor = '" . $_DOESNT_MATTER . "') OR ".
                                    "(VTGB.NombreValor = '" . $room["GeneroAcepta"] . "') OR ".
                                    "(VTGB.NombreValor = 'Pareja' AND '". $room["ParejaAcepta"] ."' = 'Si') OR " .
                                    "(VTGB.NombreValor = 'Pareja' AND '". $room["ParejaAcepta"] . "' = '" . $_DOESNT_MATTER . "')".
                                ")";

                $sql_filter[] = "(".
                                    "(VTGB.NombreValor = 'Pareja' AND '" . $room["ParejaAcepta"] ."' = 'Si') OR ".
                                    "('" . $room["ParejaAcepta"] . "' = '" . $_DOESNT_MATTER . "')".
                                ")";

                //HijosBusco VTHB
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTHB ON " . $table_roommate . ".HijosBusco = VTHB.CodigoValor";
                $sql_filter[] = "(".
                                    "(VTHB.NombreValor = '" . $_DONT_SHOW . "') OR ".
                                    "('". $room["NinosAcepta"] . "' = '". $_DOESNT_MATTER . "') OR ".
                                    "(VTHB.NombreValor = '" . $room["NinosAcepta"] . "')".
                                ")";

                //MascotaBusca VTMB
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTMB ON " . $table_roommate . ".MascotaBusca = VTMB.NombreValor";
                $sql_filter[] = "(".
                                    "(VTMB.NombreValor = '" . $_DONT_SHOW . "') OR ".
                                    "('" . $room["MascotaAcepta"] . "' = '" . $_DOESNT_MATTER . "') OR " .
                                    "(VTMB.NombreValor = '" . $room["MascotaAcepta"] . "') ".
                                ")";

                //FumadorBusca VTFB
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTFB ON ". $table_roommate . ".FumadorBusca = VTFB.NombreValor";
                $sql_filter[] = "(".
                                    "(VTFB.NombreValor = '" . $_DONT_SHOW . "') OR ".
                                    "('" . $room["FumadorAcepta"] . "' = '" . $_DOESNT_MATTER . "') OR " .
                                    "(VTFB.NombreValor = '" . $room["FumadorAcepta"] . "') ".
                                 ")";

                //OrientacionBusca VTOB
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTOB ON ". $table_roommate . ".OrientacionBusca = VTOB.NombreValor";
                $sql_filter[] = "(".
                                    "(VTOB.NombreValor = '" . $_DONT_SHOW . "') OR ".
                                    "('" . $room["OrientacionAcepta"] . "' = '" . $_DOESNT_MATTER . "') OR " .
                                    "(VTOB.NombreValor = '" . $room["OrientacionAcepta"] . "') ".
                                ")";

                //OcupacionBusca VTOCB
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTOCB ON ". $table_roommate. ".OcupacionBusca = VTOCB.NombreValor";
                $sql_filter[] = "(".
                                    "('".$room["OcupacionAcepta"] . "' = '" . $_DOESNT_MATTER . "') OR ".
                                    "(VTOCB.NombreValor = '" . $room["OcupacionAcepta"] . "')".
                                ")";
            }
        }
        
        if ($min_price != null && $max_price != null) {
                    $sql_filter[] = " " . $table_roommate . ".MontoRenta >= " . doubleval($min_price) . " AND " . $table_roommate . ".MontoRenta <= " . doubleval($max_price);
        } else {
            if ($min_price != null) {
                        $sql_filter[] = " " . $table_roommate . ".MontoRenta >= " . doubleval($min_price);
            } elseif ($max_price != null) {
                        $sql_filter[] = " " . $table_roommate . ".MontoRenta <= " . doubleval($max_price);
            }
        }

        // Coordenadas del mapa
        if($coordinates != null && is_array($coordinates)) {
        	
            $sql_filter[] = " MBRContains(".
                    "GeomFromText('Polygon((". $coordinates["minX"] . " " . $coordinates["minY"] . " " . "," . $coordinates["minX"] . " " . $coordinates["maxY"] . "," . $coordinates["maxX"] . " " . $coordinates["maxY"] . "," . $coordinates["maxX"] . " " . $coordinates["minY"]. "," . $coordinates["minX"] . " " . $coordinates["minY"] . "))' ), " .
                    "GeomFromWKB(Point(". $table_roommate . ".Longitud, ". $table_roommate . ".Latitud))) ";

        }

        $sql_filter = implode(" AND ", $sql_filter);
        if(count($sql_query) > 0){
            $sql_query = " LEFT OUTER JOIN " . implode(" LEFT OUTER JOIN ", $sql_query);
        }else{
            $sql_query = '';
        }

        return array("conditions" => $sql_filter, "query" => $sql_query);
    }

     /**
    * Retorna si es compa�ero o es propietario de un cuarto o nuevo en base a la cantidad
    * de registro en tablas correspondientes
    * @param Integer $uid Uid del usuario actual
    */
   public static function GetTypeRoommateUser($uid){
      $objRoom = new Hm_Rmt_Cuarto();
      $rsRoom = $objRoom->fetchAll(
               $objRoom->select()
               ->where('Uid = ?', $uid)
               );

       if($rsRoom->count() > 0){
            return array("type"=>"roomOwner", "id"=>$rsRoom->current()->CodigoCuarto);
       }

       $objRoommate = new Hm_Rmt_Roommate();
       $rsRoommate = $objRoommate->fetchAll(
               $objRoommate->select()
                            ->where('Uid = ?', $uid)
                             );

       if($rsRoommate->count() > 0){
           return array("type"=>"roommate", "id"=>$rsRoommate->current()->CodigoRoommate);
       }

       return array("type"=>"newUser", "id"=> null);
   }
}
?>