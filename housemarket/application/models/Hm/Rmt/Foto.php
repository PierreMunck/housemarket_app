<?php
class Hm_Rmt_Foto extends Zend_Db_Table {
    /**
     * Nombre de la tabla de Roommate para un cuarto
     * @var String
     */
    protected $_name = 'RMTFoto';

    /**
     * Llave primaria de la tabla de RMTCuarto
     * @var String
     */
    protected $_primary = 'RMTFotoID';

    /**
     * Adaptador para la base de datos
     * @var
     */
    public $dbAdapter;

     protected $_referenceMap    = array(
            'Hm_Rmt_Cuarto' => array(
                            'columns'           => 'CodigoCuarto',
                            'refTableClass'     => 'Hm_Rmt_Cuarto',
                            'refColumns'        => 'CodigoCuarto'
            )
         );

     /**
     * Guarda las fotos del cuarto para Roommate en la bbdd
     * Las fotos est�n dividida en cuatro categor�as
     * @param Integer $roomId Codigo del cuarto al que se quiere asociar
     * @param array $photos Fotos a ser subidas al servidor
     * @return Estado del proceso de guardar o actualizar la bbdd.
     *         fails => true si fallo el proceso, true en otro caso
     *         reasons => Razones por las cuales fallo el proceso de guardar los datos de la foto
     */
    public static function SaveOrUpdate($roomId, $photos = array()) {
        $status = array();
        $tiposFotos = array("principal", "secundarias");
        try {
            $block = array();
            foreach($tiposFotos as $tipo) { // Para cada tipo de foto
                $result = self::ProcessPhotos($roomId, $tipo, $photos);
                if(is_array($result) && $result["fails"] === true) { // Fallo algun archivo en el directorio
                    $block[] = $result["reasons"];
                }
            }
            if(count($block)>0) { // Ocurrio mas de un fallo en algun directorio
                $status = array("fails" => true, "reasons"=> $block);
            }
        } catch(Exception $ex) {
            $status = array("fails"=>true, "reasons" => array($ex->getMessage()));
        }
        return $status;
    }

    /**
     * Procesa las fotos.
     * @param Integer $roomId Id de la propiedad a la que pertenece la foto
     * @param Integer $albumId Id del album al que pertenece la foto
     * @param String $tipo tipo foto a procesar
     * @param FILE $photos Referencia al archivo de foto a subir
     */
    private static function ProcessPhotos(&$roomId, &$tipo, &$photos) {
        // Recuperar las fotos para el tipo en particular
        $photos_in = $photos["photos"][$tipo];
        $status = array("fails"=>false);
        $block = array();
        if(is_array($photos_in)) { // Si hay fotos.
            foreach($photos_in as $key => $photo) { // Por cada foto.
                if($photo["photo"]["error"] == UPLOAD_ERR_OK || $photo["photo"]["error"] == 4) { // Si estan correctamente subidas.
                    $result = self::ProcessPhoto($roomId, $tipo, $key, $photo);
                    if(is_array($result) && $result["fails"] === true) {
                        $block[] = $result["reasons"];
                    }
                } else {
                    if($photo["photo"]["error"] != UPLOAD_ERR_NO_FILE) {
                        $block[] = Hm_Pro_Foto::FileUploadErrorMessage($photo["photo"]["error"]);
                    }
                }
            }
        }
        if(count($block)>0) {
            $status = array("fails" => true, "reasons"=>$block);
        }
        return $status;
    }

    /**
     * Procesa la foto. Guarda los datos en el registro de la foto
     * @param Integer $roomId Id del cuarto al que pertenece la foto
     * @param Integer $tipo Si es destacada o secundaria
     * @param String $directory Directorio en el cual crear la foto.
     * @param String $key Identificador del archivo al subirse
     * @param FILE $photo Referencia al archivo de foto a subir
     */
    private static function ProcessPhoto(&$roomId, &$tipo, &$key, &$photo) {
        $status = array("fails"=>false);
        $block = array();
        $tipo = trim($tipo);
        // Create Photo in Database
        $dbPhoto = new Hm_Rmt_Foto();
        $data = array();
        $data["Comentario"] = $photo["caption"]; // Caption de la foto
        $isNew = false;

        if(stristr($key, "New") !== false) {
            $isNew = true; // La foto es nueva
        }

        try {
            if($isNew and $photo["photo"]["error"] == UPLOAD_ERR_OK) { // Crear nuevo registro de la foto
                $data["FechaCreacion"] = new Zend_Db_Expr('CURDATE()'); // Fecha de creacion
                $data["CodigoCuarto"] = $roomId;
                $dbPhoto->insert($data); // Crear registro
                $photoId = $dbPhoto->getAdapter()->lastInsertId(); // Recuperar id de la foto recien creada
            } else { // Actualizar informacion de la foto
                $data["FechaModificacion"] = new Zend_Db_Expr('CURDATE()'); // Fecha de modificacion
                $photoId = intval(str_replace("fprinc", "", str_replace("fotra", "", $key))); // Recuperar el id de la foto a actualizar
                $dbPhoto->update($data, $dbPhoto->getAdapter()->quoteInto("RMTFotoID=?", $photoId)); // Actualizar registro
            }
        } catch(Exception $ex) {
            $block[] = "Couldn't update featured photo reference in database (general info)";
        }

        if($photo["photo"]["error"] == UPLOAD_ERR_OK){
            $data = array();
            $sourceImage = $photo["photo"]["tmp_name"];

            /* Se utilizan siempre estas dos resoluciones */
            $methodsLarge = array("methodname"=> "resize",
                        "params" =>array(429, 321));
            $methodsGallery = array("methodname"=> "adaptiveResize",
                        "params" =>array(80, 60));
            //Procesamiento de imagen
            if($tipo == "principal") {
                $data["Principal"] = 1;
            } else {
                $data["Principal"] = false;
            }
            $arrData = Hm_Pro_Foto::ProcessThumbnail($methodsLarge, $sourceImage);
            $data["FotoGrande"] = $arrData["ImageString"];
            $dimensions = $arrData["Dimensions"];
            $data["AnchoFotoGrande"] = $dimensions["width"];
            $data["AltoFotoGrande"] = $dimensions["height"];
            $data["Mime"] = $arrData["Mime"];

            $arrData = Hm_Pro_Foto::ProcessThumbnail($methodsGallery, $sourceImage);
            $data["FotoGaleria"] = $arrData["ImageString"];

            $methodsFeature = array("methodname"=> "adaptiveResize",
                    "params" =>array(120, 90));
            $arrData = Hm_Pro_Foto::ProcessThumbnail($methodsFeature, $sourceImage);
            $data["FotoDestacada"] = $arrData["ImageString"];


            $methodsResults = array("methodname"=> "adaptiveResize",
                    "params" =>array(100, 75));
            $arrData = Hm_Pro_Foto::ProcessThumbnail($methodsResults, $sourceImage);
            $data["FotoResultado"] = $arrData["ImageString"];

            try {
                $dbPhoto->update($data, $dbPhoto->getAdapter()->quoteInto('RMTFotoID = ?', $photoId));
            } catch(Exception $ex) {
                $block[] = "Couldn't update featured photo reference in database (physical filename)";
            }
        }
        if(count($block) > 0) {
            $status = array("fails"=>true, "reasons"=>$block);
        }
        return $status;
    }
}
?>
