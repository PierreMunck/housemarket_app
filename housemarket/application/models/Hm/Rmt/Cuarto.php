<?php
class Hm_Rmt_Cuarto extends Zend_Db_Table {
    /**
     * Nombre de la tabla de Roommate para un cuarto
     * @var String
     */
    protected $_name = 'RMTCuarto';

    /**
     * Llave primaria de la tabla de RMTCuarto
     * @var String
     */
    protected $_primary = 'CodigoCuarto';

    protected $_dependentTables = array(
            'Hm_Rmt_Foto'
    );

    /**
     * caracteristica de la propriedad
     */ 
    
    public $CodigoCuarto = null;
    public $Titulo = null;
    public $Descripcion = null;
    public $Correo = null;
    public $TipoAlojamiento = null;
    public $TipoPropiedad = null;
    public $MaxCapacidad = null;
    public $EspacioDisponible = null;
    public $TipoCuarto = null;
    public $TipoBano = null;
    public $MontoRenta = null;
    public $IncluyeServicio = null;
    public $FechaDisponible = null;
    public $CortoPlazo = null;
    public $Direccion = null;
    public $Estado = null;
    public $Ciudad = null;
    public $ZipCode = null;
    public $Calle = null;
    public $CodigoPostal = null;
    public $Latitud = null;
    public $Longitud = null;
    public $Uid = null;
    public $EdadMinimaHabitan = null;
    public $EdadMaximaHabitan = null;
    public $FumadorHabitan = null;
    public $IdiomaHabitan = null;
    public $OcupacionHabitan = null;
    public $GeneroHabitan = null;
    public $MascotaHabitan = null;
    public $HijosHabitan = null;
    public $GeneroAcepta = null;
    public $ParejaAcepta = null;
    public $NinosAcepta = null;
    public $MascotaAcepta = null;
    public $FumadorAcepta = null;
    public $OcupacionAcepta = null;
    public $EdadMinimaAcepta = null;
    public $EdadMaximaAcepta = null;
    public $EstanciaMinima = null;
    public $EstadoRoommate = null;
    public $MontoServicios = null;

    const PUBLISHED_STATE = 64;

    /**
     * List de los campos de la base de datos
     * 
     * @return list de campo
     */
    private function CampoList(){
    	$campolist = array(
    		"CodigoCuarto",
  			"Titulo",
   			"Descripcion",
   			"Correo",
   			"TipoAlojamiento",
   			"TipoPropiedad",
   			"MaxCapacidad",
   			"EspacioDisponible",
   			"TipoCuarto",
   			"TipoBano",
   			"MontoRenta",
   			"IncluyeServicio",
   			"FechaDisponible",
   			"CortoPlazo",
   			"Direccion",
   			"Estado",
   			"Ciudad",
   			"ZipCode",
   			"Calle",
   			"CodigoPostal",
   			"Latitud",
   			"Longitud",
   			"Uid",
   			"EdadMinimaHabitan",
   			"EdadMaximaHabitan",
   			"FumadorHabitan",
   			"IdiomaHabitan",
   			"OcupacionHabitan",
   			"GeneroHabitan",
   			"MascotaHabitan",
   			"HijosHabitan",
   			"GeneroAcepta",
   			"ParejaAcepta",
   			"NinosAcepta",
   			"MascotaAcepta",
   			"FumadorAcepta",
   			"OcupacionAcepta",
   			"EdadMinimaAcepta",
   			"EdadMaximaAcepta",
   			"EstanciaMinima",
   			"EstadoRoommate",
	    	"MontoServicios",
    	);
    	return $campolist;
    }
    
    function __construct($val = null) {
    	if($val != null){
    		foreach($this->CampoList() as $campo){
    			$campoval = lcfirst($campo);
    			if(isset($val[$campoval])){
    				$this->{$campo} = $val[$campoval];
    			}elseif (isset($val[$campo])){
    				$this->{$campo} = $val[$campo];
    			}
    		}
    	}
    	parent::__construct();
    }
    /**
     * Valida los datos de la propiedad a ser guardados.
     * @param Array $parameters Conjunto de datos de la propiedad a ser guardados
     * @return Array isValid => true si es valido, false en caso contrario
     *               errors => Conjunto de mensajes de errores a mostrar al usuario.
     */
    private function Validate($parameters = array()) {

        $isValid = true;
        $f = new Zend_Filter_StripTags();
        $errors = array();
        $new = $f->filter($parameters["isNew"], "S");
        $isNew = $new=="S"?true:false;

        $accommodation = $f->filter($parameters["tipoAlojamiento"], null);
        $block = array();
        if($accommodation == null) {
            $block[] = "<i18n>MSG_ACCOMMODATION_REQUIRED</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($accommodation) ) {
                $block[] = "<i18n>MSG_ACCOMMODATION_NO_VALID</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Accommodation Type"] = $block;
        }

        $roomType = $f->filter($parameters["tipoCuarto"], null);
        $block = array();
        if($roomType == null) {
            $block[] = "<i18n>MSG_ROOM_TYPE_REQUIRED</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($roomType) ) {
                $block[] = "<i18n>MSG_ROOM_TYPE_NO_VALID</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Room Type"] = $block;
        }

        $bathType = $f->filter($parameters["tipoBano"], null);
        $block = array();
        if($bathType == null) {
            $block[] = "<i18n>MSG_BATH_TYPE_REQUIRED</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($bathType) ) {
                $block[] = "<i18n>MSG_BATH_TYPE_NO_VALID</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Bath Type"] = $block;
        }

        if(isset($parameters["incluyeServicio"])){
        	$ServiceIncluido = $parameters["incluyeServicio"];
        }else{
        	$ServiceIncluido = true;
        }
        
        if(isset($parameters["montoRenta"])){
        	$isForRent = $f->filter($parameters["montoRenta"], null)==null?false:true;
        }else{
        	$isForRent = true;
        }

        if($isForRent) {
            $precioAlquiler = $this->validanumcoma($f->filter($parameters["montoRenta"], null));
            $block = array();
            if($precioAlquiler == null || !is_numeric($precioAlquiler)) {
                $block[] = "Please, enter a valid rent price";
                $isValid = false;
            }
            if(count($block) > 0) {
                $errors["Rent Price"] = $block;
            }
        }

        if(!$isForRent) {
            $errors["Prices"] = array("Rent price is required");
            $isValid = false;
        }

        if(!$ServiceIncluido) {
        	if(isset($parameters["montoServicios"])){
        		$precioServicio =$this->validanumcoma($f->filter($parameters["montoServicios"], null));
        	}else{
        		$precioServicio = null;
        	}
            
            $block = array();
            if($precioServicio == null || !is_numeric($precioServicio)) {
                $block[] = "Please, enter a valid service price";
                $isValid = false;
            }else{
                if(($precioServicio > $precioAlquiler) && $precioAlquiler != null){
                    $block[] = "Rent price should be greater than service price";
                    $isValid = false;
                }
            }
            if(count($block) > 0) {
                $errors["Service Price"] = $block;
            }
        }

        //Estas validaciones son comunes a cuarto y roommate se podr�a implementar un objeto que las encapsule.

        if(!$isNew){
            $estateRoom = $f->filter($parameters["estadoCuarto"], null);
            $block = array();
            if($accommodation == null) {
                $block[] = "<i18n>MSG_STATE_ROOM_REQUIRED</i18n>";
                $isValid = false;
            } else {
                if(!is_numeric($accommodation) ) {
                    $block[] = "<i18n>MSG_STATE_ROOM_REQUIRED</i18n>";
                    $isValid = false;
                }
            }
            if(count($block)> 0) {
                $errors["Accommodation Type"] = $block;
            }
        }

        $propertyType = $f->filter($parameters["tipoPropiedad"], null);
        $block = array();
        if($propertyType == null) {
            $block[] = "<i18n>MSG_PROPERTY_TYPE_REQUIRED</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($propertyType) ) {
                $block[] = "<i18n>MSG_PROPERTY_TYPE_NO_VALID</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Property Type"] = $block;
        }
        $zipcode = $f->filter($parameters["zipCode"], null);
        $block = array();
        if($zipcode == null || strlen($zipcode) == 0) {
            $block[] = "Please, enter a country code";
            $isValid = false;
        } else {
            if(strlen($zipcode) > 64) {
                $block[] = "Please, enter a valid country code";
                $isValid = false;
            }
        }
        if(count($block) > 0) {
            $errors["Zip Code"] = $block;
        }

        $state = $f->filter($parameters["estado"], null);
        $block = array();
        if($state == null || strlen($state) < 1 || strlen($state) > 64) {
            $block[] = "Please, enter a valid state";
            $isValid = false;
        }
        if(count($block)>0) {
            $errors["State"] = $block;
        }

        $ciudad = $f->filter($parameters["ciudad"], null);
        $block = array();
        if($ciudad == null || strlen($ciudad) < 1 || strlen($ciudad) > 64) {
            $block[] = "Please, enter a valid city";
            $isValid = false;
        }
        if(count($block)>0) {
            $errors["City"] = $block;
        }

        if(isset($parameters["calle"])){
        	$calle = $f->filter($parameters["calle"], null);
        }else{
        	$calle = null;
        }
        $block = array();
        if($calle != null) {
            if(strlen($calle) > 32) {
                $block[] = "Please, enter a valid street";
                $isValid = false;
            }
            if(count($block)>0) {
                $errors["Street"] = $block;
            }
        }

        $direccion = $f->filter($parameters["direccion"], null);
        $block = array();
        if($direccion == null || strlen($direccion) == 0 ) {
            $block[] = "Please, enter an address";
            $isValid = false;
        } else {
            if(strlen($direccion) > 255) {
                $block[] = "Please, Address is too long. Address must be between 1 and  255 characters";
                $isValid = false;
            }
        }
        if(count($block)>0) {
            $errors["Address"] = $block;
        }


        //Tipos de ocupantes que tiene
        $smokerHave = $f->filter($parameters["fumadorTengo"], null);
        $block = array();
        if($smokerHave == null) {
            $block[] = "<i18n>MSG_SMOKER_TYPE_REQUIRED</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($smokerHave) ) {
                $block[] = "<i18n>MSG_SMOKER_TYPE_NO_VALID</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Smoker Have Type"] = $block;
        }

        $occupationHave = $f->filter($parameters["ocupacionTengo"], null);
        $block = array();
        if($occupationHave == null) {
            $block[] = "<i18n>MSG_ROOM_OCCUPATION_REQUIRED</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($occupationHave) ) {
                $block[] = "<i18n>MSG_ROOM_OCCUPATION_NO_VALID</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Occupation Have Type"] = $block;
        }

        $genderHave = $f->filter($parameters["generoTengo"], null);
        $block = array();
        if($genderHave == null) {
            $block[] = "<i18n>MSG_ROOM_GENDER_NO_VALID</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($genderHave) ) {
                $block[] = "<i18n>MSG_ROOM_GENDER_NO_VALID</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Gender Have Type"] = $block;
        }

        $petHave = $f->filter($parameters["mascotaTengo"], null);
        $block = array();
        if($petHave == null) {
            $block[] = "<i18n>MSG_ROOM_PET_REQUIRED</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($petHave) ) {
                $block[] = "<i18n>MSG_ROOM_PET_NO_VALID</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Pet Have Type"] = $block;
        }

        $childrenHave = $f->filter($parameters["hijosTengo"], null);
        $block = array();
        if($childrenHave == null) {
            $block[] = "<i18n>MSG_ROOM_CHILDREN_REQUIRED</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($childrenHave) ) {
                $block[] = "<i18n>MSG_ROOM_CHILDREN_NO_VALID</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Children Have Type"] = $block;
        }

        $languageHave = $f->filter($parameters["idiomaTengo"], null);
        $block = array();
        if($languageHave != null && !is_numeric($languageHave) ) {
            $block[] = "<i18n>MSG_ROOM_LANGUAGE_NO_VALID</i18n>";
            $isValid = false;
        }

        $minAgeHave = $f->filter($parameters["edadMinimaHabitan"], null);
        $block = array();
        if($minAgeHave == null) {
            $block[] = "<i18n>MSG_ROOM_MIN_AGE_REQUIRED</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($minAgeHave) ) {
                $block[] = "<i18n>MSG_ROOM_MIN_AGE_NO_VALID</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Minimum Have Type"] = $block;
        }

        $maxAgeHave = $f->filter($parameters["edadMaximaHabitan"], null);
        $block = array();
        if($maxAgeHave == null) {
            $block[] = "<i18n>MSG_ROOM_MAX_AGE_REQUIRED</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($maxAgeHave) ) {
                $block[] = "<i18n>MSG_ROOM_MAX_AGE_NO_VALID</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Maximum Have Type"] = $block;
        }

        if($minAgeHave != null && $maxAgeHave != null){
            if($minAgeHave > $maxAgeHave){
                $errors["Ages Compare"] = "MSG_AGES_COMPARE_RESTRICTING";
                $isValid = false;
            }
        }

    //Tipos de ocupantes que acepta
        $genderAccept = $f->filter($parameters["generoAcepta"], null);
        $block = array();
        if($genderAccept == null) {
            $block[] = "<i18n>MSG_ROOM_GENDER_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($genderAccept) ) {
                $block[] = "<i18n>MSG_ROOM_GENDER_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Gender Accept Type"] = $block;
        }

        $coupleAccept = $f->filter($parameters["parejaAcepta"], null);
        $block = array();
        if($coupleAccept == null) {
            $block[] = "<i18n>MSG_ROOM_COUPLE_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($coupleAccept) ) {
                $block[] = "<i18n>MSG_ROOM_COUPLE_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Couple Accept Type"] = $block;
        }

        $childrenAccept = $f->filter($parameters["ninosAcepta"], null);
        $block = array();
        if($childrenAccept == null) {
            $block[] = "<i18n>MSG_ROOM_CHILDREN_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($childrenAccept) ) {
                $block[] = "<i18n>MSG_ROOM_CHILDREN_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Children Accept Type"] = $block;
        }

        $petAccept = $f->filter($parameters["mascotaAcepta"], null);
        $block = array();
        if($petAccept == null) {
            $block[] = "<i18n>MSG_ROOM_PET_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($petAccept) ) {
                $block[] = "<i18n>MSG_ROOM_PET_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Pet Accept Type"] = $block;
        }

        $smokerAccept = $f->filter($parameters["fumadorAcepta"], null);
        $block = array();
        if($smokerAccept == null) {
            $block[] = "<i18n>MSG_SMOKER_TYPE_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($smokerAccept) ) {
                $block[] = "<i18n>MSG_SMOKER_TYPE_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Smoker Accept Type"] = $block;
        }

        $occupationAccept = $f->filter($parameters["ocupacionAcepta"], null);
        $block = array();
        if($occupationAccept == null) {
            $block[] = "<i18n>MSG_ROOM_OCCUPATION_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($occupationAccept) ) {
                $block[] = "<i18n>MSG_ROOM_OCCUPATION_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Occupation Accept Type"] = $block;
        }

        $minAgeAccept = $f->filter($parameters["edadMinimaAcepta"], null);
        $block = array();
        if($minAgeAccept == null) {
            $block[] = "<i18n>MSG_ROOM_MIN_AGE_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($minAgeAccept) ) {
                $block[] = "<i18n>MSG_ROOM_MIN_AGE_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Minimum Accept Type"] = $block;
        }

        $maxAgeAccept = $f->filter($parameters["edadMaximaAcepta"], null);
        $block = array();
        if($maxAgeAccept == null) {
            $block[] = "<i18n>MSG_ROOM_MAX_AGE_REQUIRED</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
            $isValid = false;
        } else {
            if(!is_numeric($maxAgeAccept) ) {
                $block[] = "<i18n>MSG_ROOM_MAX_AGE_NO_VALID</i18n> <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Maximum Accept Type"] = $block;
        }


        if($minAgeAccept != null && $maxAgeAccept != null){
            if($minAgeAccept > $maxAgeAccept){
                $errors["Ages Compare Rmt"] = "MSG_AGES_COMPARE_RESTRICTING <i18n>LABEL_FOR_LOWERCASE</i18n> <i18n>ADD_ROOM_ADDITIONAL_INFO_IDEAL_ROOMMATE</i18n>";
                $isValid = false;
            }
        }

        ##### Resto de validaciones #########
        $availableDate = $f->filter($parameters["fechaDisponible"], null);
        $format = "yyyy-MM-dd";
        $block = array();
        if($availableDate == null || strlen($availableDate) == 0) {
            $block[] = "Please, enter available date";
            $isValid = false;
        }
        else {
            if(!Zend_Date::isDate($availableDate, $format)) {
                $block[] = "Availability Date is not valid";
                $isValid = false;
            }elseif($isNew){
                $today = new Zend_Date(null, $format);
                $xmas = new Zend_Date($availableDate, $format);
                $diffTs = $xmas->get(Zend_Date::TIMESTAMP) - $today->get(Zend_Date::TIMESTAMP);
                $tsInDays = floor((($diffTs / 60) / 60) / 24);
                if($tsInDays < 0){
                    $block[] = "Availability date is lesser than actual date";
                    $isValid = false;
                }
            }
        }
        if(count($block)>0) {
            $errors["Date"] = $block;
        }

        $stayMonth = $f->filter($parameters["stayMonth"], null);
        $block = array();
        if($stayMonth == null) {
            $block[] = "Please, choose number of stay Months";
            $isValid = false;
        } else {
            if(!is_numeric($minAgeAccept) ) {
                $block[] = "Stay Months type selected is not a valid number";
                $isValid = false;
            }
        }
        if(count($block)> 0) {
            $errors["Stay Month"] = $block;
        }

        $titleRoom = $f->filter($parameters["titulo"], null);
        $block = array();
        if($titleRoom == null || strlen($titleRoom) == 0) {
            $block[] = "Please, enter a title";
            $isValid = false;
        } else {
            if(strlen($titleRoom) > 128) {
                $block[] = "Title is too long, please enter a title equal or less than characters";
                $isValid = false;
            }
        }
        if(count($block)>0) {
            $errors["Title"] = $block;
        }

        $EmailRoom = $f->filter($parameters["correo"], null);
        $block = array();
        if($EmailRoom == null || strlen($EmailRoom) == 0) {
            $block[] = "Please, enter an Email";
            $isValid = false;
        }
        
        if(count($block)>0) {
            $errors["Email"] = $block;
        }
        
        $DescRoom = $f->filter($parameters["descripcion"], null);
        $block = array();
        if($DescRoom != null) {
            if(strlen($DescRoom) > 512) {
                $block[] = "Description is too long, please enter a title equal or less than 512 characters";
                $isValid = false;
            }
        }
        if(count($block)>0) {
            $errors["Description"] = $block;
        }

        $latitud = $f->filter($parameters["latitud"], null);
        $block = array();
        if($latitud == null || !is_numeric($latitud) ) {
            $block[] = "Please, enter a valid latitud";
            $isValid = false;
        }
        if(count($block)>0) {
            $errors["Latitud"] = $block;
        }

        $longitud = $f->filter($parameters["longitud"], null);
        $block = array();
        if($longitud == null || !is_numeric($longitud)) {
            $block[] = "Please, enter a valid longitud";
            $isValid = false;
        }
        if(count($block) > 0) {
            $errors["Longitud"] = $block;
        }
        return array("valid" =>$isValid , "errors"=> $errors);
    }
    
    
    public function DeleteRoom($roomId){
        $translator = Zend_Registry::get('Zend_Translate'); 
        $status = array();
        $result = 0;
        if(is_numeric($roomId)){            
            try {
                $roomRowset = $this->find($roomId);
                if($roomRowset != null and $roomRowset->count() > 0){
                    $roomItem = $roomRowset->current();
                    $result = $roomItem->delete();
                    //$result = $this->delete($this->getAdapter()->quoteInto('CodigoCuarto=?', $roomId));
                }
            } catch (Exception $exc) {}
        }
        if($result > 0){
            $status = array("status"=>"success");
        }else{
            $status = array("fail"=>true, "reasons"=> $translator->getAdapter()->translate("DEL_FAIL"));
        }
        return $status;
    }

    /**
     * Guarda los datos para el Roommate en Tengo cuarto.
     * @global Array $_FILES Contiene las referencias a las fotos de la propiedad
     * @param Array $parameters Contiene los datos de la propiedad
     * @return <type> Array
     */
    public function SaveOrUpdate($parameters = array()) {
        global $_FILES;

        $id = null;
        $status = array();
        $isValid = $this->Validate($parameters);
        if(!$isValid["valid"] && count($isValid["errors"]) > 0) {
            // Mandar en el flash messenger todos los errores.
            return array("fail"=>true, "reasons"=>$isValid["errors"]);
        }

        $f = new Zend_Filter_StripTags();

        // Recuperar los datos del registro
        $estateRoom = array();
        foreach($this->CampoList() as $campo){
        	$campoval = lcfirst($campo);
        	if(isset($parameters[$campoval])){
        		$estateRoom[$campo] = $f->filter($parameters[$campoval], null);
        	}else{
        		$estateRoom[$campo] = null;
        	}
        }
        
        if(isset($parameters["incluyeServicio"])){
        	$estateRoom["IncluyeServicio"] = $parameters["incluyeServicio"] ? "S": "N";
        }else{
        	$estateRoom["IncluyeServicio"] = "N";
        }
        
        if(isset($parameters["cortoPlazo"])){
        	$estateRoom["CortoPlazo"] = $parameters["cortoPlazo"] ? "S": "N";
        }else{
        	$estateRoom["CortoPlazo"] = "N";
        }
        if(isset($parameters["montoRenta"])){
        	$estateRoom["MontoRenta"] = $this->validanumcoma($f->filter($parameters["montoRenta"], null));
        }else{
        	$estateRoom["MontoRenta"] = null;
        }
        if(isset($parameters["montoRenta"])){
        	$estateRoom["MontoRenta"] = $this->validanumcoma($f->filter($parameters["montoRenta"], null));
        }else{
        	$estateRoom["MontoRenta"] = null;
        }
        
        $estateRoom["Uid"] = Zend_Registry::get('uid');
        
        $estateRoom["FumadorHabitan"] = $f->filter($parameters["fumadorTengo"], null);
        $estateRoom["IdiomaHabitan"] = $f->filter($parameters["idiomaTengo"], null);
        $estateRoom["OcupacionHabitan"] = $f->filter($parameters["ocupacionTengo"], null);
        $estateRoom["GeneroHabitan"] = $f->filter($parameters["generoTengo"], null);
        $estateRoom["MascotaHabitan"] = $f->filter($parameters["mascotaTengo"], null);
        $estateRoom["HijosHabitan"] = $f->filter($parameters["hijosTengo"], null);
        $estateRoom["EstanciaMinima"] = $f->filter($parameters["stayMonth"], null);
        $estateRoom["EstadoRoommate"] = $f->filter($parameters["estadoCuarto"], null);

        /*$valService = $f->filter($parameters["montoServicios"], null);
        if($valService != null){
           $estateRoom["MontoServicios"] = $this->validanumcoma($f->filter($parameters["serviceUS"], null));
        }*/

        $publicado = $f->filter($parameters["publicado"], "S");
        $new = $f->filter($parameters["isNew"], "S");
        $isPublicada = $publicado=="S"?true:false;
        $isNew = $new=="S"?true:false;

        if($isNew) {
            // Determinar si se puede crear el registro del cliente
            $cliente = new Hm_Cli_Cliente();

            $result=$cliente->CreateCliente(Zend_Registry::get('uid'));
            if(is_array($result) && $result["status"]===false && $result["reason"] != "Duplicated") {
                return array("fail"=>true, "reasons"=>$result["reason"]);
            }

            $estateRoom["FechaRegistro"] = new Zend_Db_Expr('CURDATE()');
        }

        if($new==false) {
            $estateRoom["FechaModifica"] = new Zend_Db_Expr('CURDATE()');
        }
        try {
	        if($isNew) { // Crear
	            $result = $this->insert($estateRoom);
	            $id = $this->getAdapter()->lastInsertId();
	        } else { // Actualizar
	        	$id = $this->CodigoCuarto;
	            $result = $this->update($estateRoom, $this->getAdapter()->quoteInto("CodigoCuarto=?", $this->CodigoCuarto) );
	        }
        } catch(Exception $ex) {
        	return array("fail" => true, "reasons"=>$ex->getMessage());
        }
        $room = new Hm_Rmt_Cuarto();
        $rsRoom = $room->find($id);
        $val = $rsRoom->getRow(0)->toArray();
        $room = new Hm_Rmt_Cuarto($val);
        if($rsRoom == null || $rsRoom->count() == 0) { // Si es creacion, entonces revisar que se haya creado el registro
            return array("fail"=>true, "reasons"=>"Estate not created");
        }

        try {
        	if(isset($parameters["benef"])){
        		$benefits = $parameters["benef"];
        		$status["Benefits"] = Hm_Cat_BeneficioCuarto::SaveOrUpdate($id, $benefits);
        	}
        } catch(Exception $ex) {
            $status["Benefits"] = array("fail" => true, "reason"=>$ex->getMessage());
        }

        // Guardar o actualizar Fotos
        try {
            $photos["size"] = count($_FILES);
            $photos["photos"]["secundarias"] = array();
            $photoCaptions = $parameters["fdescotra"];
            foreach($_FILES as $key=>$file) {
                if(!(stristr($key, "fotra") === false)) {
                    // Transformando
                    $captionKey = str_replace("fotra", "", $key);
                    if(stristr($captionKey, "New") !== false) {
                        $captionKey = str_replace("New", "new_", $captionKey);
                    }
                    // Transformar el key hasta obtener el caption correspondiente.
                    $photos["photos"]["secundarias"][$key]["caption"] = $photoCaptions[$captionKey];
                    $photos["photos"]["secundarias"][$key]["photo"] = $file;
                } else if(!(stristr($key, "fprinc") === false)) { // Foto Principal
                    $photos["photos"]["principal"][$key]["photo"] = $_FILES[$key];
                    $photos["photos"]["principal"][$key]["caption"] = $f->filter($parameters["fdescprinc"], "");
                }
            }

            $status["photos"] = Hm_Rmt_Foto::SaveOrUpdate($id, $photos);
        } catch(Exception $ex) {
            $status["photos"] = array("fail"=>true, "reason"=>$ex->getMessage());
        }
        

        return array("id" => $id, "status" => $status);
    }

    /**
     * Recupera las fotos asociadas al cuarto
     * @param Integer $roomId Codigo del cuarto
     * @return Array Listado de fotos del cuarto
     */
    public function GetPhotos($roomId) {
        if($roomId == null) {
            return array();
        }
        $_photos = array();
        $_photo = array();
        $rsRoom = $this->find($roomId);
        if($rsRoom != null || $rsRoom->count() > 0) {
            $photos = $rsRoom->current()->findHm_Rmt_Foto();
            foreach($photos as $photo) {
                // No es la foto principal
                if($photo->Principal === chr(0x00)) {
                    $_photo[] = $photo;
                }
            }
        }
        return $_photo;
    }

    public function GetValuesDomain($idDomain){
        $translator = Zend_Registry::get('Zend_Translate');
        $db = Zend_Registry::get('db');

        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $dataRoom=$db->fetchAll("SELECT CVD.CodigoValor as indice, CVI.ValorIdioma as nombre FROM CATValorDominio CVD INNER JOIN
                CATValorDominioIdioma CVI ON CVD.CodigoValor = CVI.CodigoValor
                WHERE CVI.CodigoIdioma = '". $language."' AND CVD.CodigoDominio = ". $idDomain);

        $data = array();
        $data[""] = $translator->getAdapter()->translate('LABEL_GENERAL_SELECT');
        foreach($dataRoom as $key => $value) {
            $data[$value->indice] = $value->nombre;
        }
        return $data;
    }

    public function GetBenefits($idDomain){
        $translator = Zend_Registry::get('Zend_Translate');
        $db = Zend_Registry::get('db');

        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $dataRoom=$db->fetchAll("SELECT CVD.CodigoValor as indice, CVI.ValorIdioma as nombre FROM CATValorDominio CVD INNER JOIN
                CATValorDominioIdioma CVI ON CVD.CodigoValor = CVI.CodigoValor
                WHERE CVI.CodigoIdioma = '". $language."' AND CVD.CodigoDominio = ". $idDomain);

        $data = array();
        foreach($dataRoom as $key => $value) {
            $data[$value->indice] = $value->nombre;
        }
        return $data;
    }

    /*validando los numeros con coma*/
    public function validanumcoma($numero){
        $resultado=$numero;
        if($numero!=null && strlen($numero)>2) {
            $validando=explode('.',$numero);
            if(count($validando)<=2)
                $resultado=str_replace(',', '', $numero);
        }
        return $resultado;
    }

     /**
     *
     * @param Integer $idRoom Identificador de un cuarto
     * @return arreglo de objetos Un arreglo el valor Segun la cantidad que se le hayan pasado.
     */
    public function ObtenerDatosCuarto($parameters = array()) {
        $db = Zend_Registry::get('db');
        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $paramsQuery = implode(", " , $parameters);
        
        $dataRoom=$db->fetchAll("SELECT CodigoValor, ValorIdioma
                                    FROM CATValorDominioIdioma
                                    WHERE  CodigoValor IN (" . $paramsQuery . ")
                                    AND CodigoIdioma = '".$language."'");

        $data = array();
        foreach ($dataRoom as $value) {
            $data[$value->CodigoValor] = $value->ValorIdioma;
        }
        return $data;
    }

      /**
     *
     * @param Integer $idRoom Identificador de un cuarto
     * @return arreglo de objetos Un arreglo el valor Seg�n la cantidad que se le hayan pasado.
     */
    public function GetValueDomains($parameters = array()) {
        $db = Zend_Registry::get('db');
        //$language = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $paramsQuery = implode(", " , $parameters);

       $dataRoom = $db->fetchAll("SELECT CodigoValor, NombreValor as ValorIdioma
                                    FROM CATValorDominio
                                    WHERE  CodigoValor IN (" . $paramsQuery . ")"
                                    );

        $data = array();
        foreach ($dataRoom as $value) {
            $data[$value->CodigoValor] = $value->ValorIdioma;
        }
        return $data;
    }
    
   /**
    * Recupera las fotos de los cuartos destacados, ordenados por cr�ditos
    * @param <type> $country Codigo del pa�s
    * @param <type> $city Nombre de la ciudad sobre la cual mostrar los resultados
    * @param <type> $coordinates Coordenadas del mapa actual
    * @param <type> $start offset inicial de consulta
    * @param <type> $limit offset final o cota superior de consulta
    * @return Array_OBJ Listado de fotos a desplegar como destacadas
    */
    public function GetFotosDestacadas($country, $city, $coordinates, $start, $limit) {
        $this->dbAdapter = Zend_Registry::get('db');
        $publicada = "Publicada";
        $estadoCredito = 1;
        $sql_select = 'SELECT DISTINCT '.
                'rc.CodigoCuarto id, '.
                'f.RMTFotoID, '.
                'f.CodigoCuarto FKCodigoCuarto'.
                ' FROM '.
                'RMTCuarto rc ' .
                ' INNER JOIN catpais p ON (p.ZipCode = rc.ZipCode) '.
                'LEFT OUTER JOIN RMTFoto f ON (rc.CodigoCuarto = f.CodigoCuarto) ';
        
        $sql_where = array();
        $sql_where[] = " rc.EstadoRoommate = " . self::PUBLISHED_STATE;
        
        $sql_where[] = " (  f.Principal = 1 or f.Principal is null ) ";

        if($coordinates && is_array($coordinates)) {
            $sql_where[] = " MBRContains(GeomFromText('Polygon((". $coordinates["minX"] . " " . $coordinates["minY"] . " " . "," . $coordinates["minX"] . " " . $coordinates["maxY"] . "," . $coordinates["maxX"] . " " . $coordinates["maxY"] . "," . $coordinates["maxX"] . " " . $coordinates["minY"] . "," . $coordinates["minX"] . " " . $coordinates["minY"]. "))' ), GeomFromWKB(Point(rc.Longitud, rc.Latitud))) ";
        }
        if(count($sql_where) > 0) {
            $sql_where = " WHERE " . implode(" AND " , $sql_where);
        } else {
            $sql_where = "";
        }

        $sql_order_by = "ORDER BY id DESC";
        $sql_limit = "LIMIT $start, $limit";
        $sql = $sql_select . " " . $sql_where . " " . $sql_order_by . " " . $sql_limit;

        return $this->DoQuery($sql);
    }
    
     /**
     * Realiza una consulta con la base de datos
     * @param String $sql Consulta a realizar
     * @param array $params Parametros que se le pasan a la consulta a realizar
     * @return RowSet Resultado de la consulta
     */
    private function DoQuery ($sql, $params=null) {
        $this->dbAdapter = Zend_Registry::get('db');

        if (isset($params)) {
            $rs = $this->dbAdapter->query($sql,$params);
        } else {
            $rs = $this->dbAdapter->query($sql);
        }
        $data = $rs->fetchAll();
        $test = $rs->rowCount();
        return $data;
    }

    /**
     * Fotos filtradas de los cuartos
     * @param String $city Nombre de la ciudad sobre la cual mostrar los resultados
     * @return RowSet Listado de fotos a desplegar como destacadas
     */
    public function GetFotosFiltradas(
            $uid,
            $conditions,
            $ideal_room,
            $myInfo,
            $ideal_partner,
            $coordinates,
            $min_price,
            $max_price,
            $start,
            $limit) {

        $translator = Zend_Registry::get('Zend_Translate');
        $db = $this->dbAdapter = Zend_Registry::get('db');
        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $sql_where_conditions = array();

        $localLangague = Zend_Registry::get('Zend_Locale');
        $resultingQuery = $this->PrepareWherePartStatement($uid, "rc", $conditions, $ideal_room, $myInfo, $ideal_partner, $coordinates, $min_price, $max_price);

        $sql_select = 'SELECT DISTINCT SQL_CALC_FOUND_ROWS ' .
                ' rc.CodigoCuarto as id, ' .
                ' (CASE WHEN CHAR_LENGTH(rc.Titulo) > 20 THEN CONCAT(SUBSTR(rc.Titulo,1,20),"...") ELSE rc.Titulo END) as nombre, ' .
                ' rc.Descripcion as descripcion, ' .
                ' rc.Direccion as direccion, ' .
                ' rc.Estado as estado, ' .
                ' rc.Ciudad as ciudad, ' .
                ' rc.ZipCode as zipcode, ' .
                ' rc.FechaRegistro as fechareg, ' .
                ' cvi.ValorIdioma as tipocuarto, '.
                ' m.SimboloMoneda as simbolomoneda, '.
                ' m.CodigoEstandar as codigomoneda, '.
                ' FORMAT(rc.MontoRenta,2) as renta, '.
                ' FORMAT(rc.MontoServicios,2) as servicio, '.
                ' rc.IncluyeServicio as incluyeservicio, '.
                ' CONCAT(\''. $translator->getAdapter()->translate('ADD_ESTATE_BASIC_INFO_RENT') . ' US $\', FORMAT(rc.MontoRenta,2), \'<br/>\') as rent, ' .
                ' (CASE WHEN rc.IncluyeServicio=\'S\' THEN \'' . $translator->getAdapter()->translate('INFO_ROOM_INCLUDED_SERVICES') . '\' ELSE CONCAT(\'' . $translator->getAdapter()->translate('ADD_ROOM_BASIC_INFO_SERVICE') .'\', \' US $\', FORMAT(rc.MontoServicios, 2))  END) as services, ' .
                ' rc.Uid, ' .
                ' f.RMTFotoID as RMTFotoID ' .
                ' FROM ' .
                ' RMTCuarto rc ' .
                " INNER JOIN CATValorDominioIdioma cvi ON (rc.TipoCuarto = cvi.CodigoValor) ".
                ' LEFT OUTER JOIN RMTFoto f ON (rc.CodigoCuarto = f.CodigoCuarto) '.
                ' LEFT OUTER JOIN catpais p ON (p.ZipCode = rc.ZipCode) '.
                ' LEFT OUTER JOIN catmoneda m ON (p.CodigoMoneda = m.CodigoMoneda ) '.
                $resultingQuery["query"];

        $sql_where = ' WHERE ';
        $sql_where_conditions[] = $resultingQuery["conditions"];
        $sql_where_conditions[] = " rc.EstadoRoommate = " . self::PUBLISHED_STATE;
        $sql_where_conditions[] = " cvi.CodigoIdioma = '$localLangague'";
        $sql_where_conditions[] = " (  f.Principal = 1 or f.Principal is null) ";

        if(count($sql_where_conditions) > 0) {
            $sql_where .= implode(" AND ", $sql_where_conditions);
        } else {
            $sql_where = "";
        }

        $sql_order_by = " ORDER BY id DESC";

        $sql_limit = " LIMIT ". $start . ", " . $limit;

        $sql = $sql_select . $sql_where . $sql_order_by . $sql_limit;
        $rs = new stdClass();
        $result = $db->fetchAll($sql);
        foreach ($result as $key => $r){
        	$result[$key] = get_object_vars($r);
        }
        $rs->rows= $result;
        $rs->total=$db->fetchOne("SELECT FOUND_ROWS()");
        
        return $rs;
    }
    
    public function GetRoomsByArea(
            $codigo_cuarto,
            $coordinates
            ) {

        $translator = Zend_Registry::get('Zend_Translate');
        $db= $this->dbAdapter = Zend_Registry::get('db');
        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $sql_where_conditions = array();

        $localLangague = Zend_Registry::get('Zend_Locale');
        $resultingQuery = $this->PrepareWherePartStatement($uid, "rc", null, null, null, null, $coordinates);

        $sql_select = 'SELECT DISTINCT SQL_CALC_FOUND_ROWS ' .
                ' rc.CodigoCuarto as id, ' .
                ' (CASE WHEN CHAR_LENGTH(rc.Titulo) > 20 THEN CONCAT(SUBSTR(rc.Titulo,1,20),"...") ELSE rc.Titulo END) as nombre, ' .
                ' rc.Uid, ' .
                ' f.RMTFotoID as RMTFotoID ' .
                ' FROM ' .
                ' RMTCuarto rc ' .
                ' LEFT OUTER JOIN RMTFoto f ON (rc.CodigoCuarto = f.CodigoCuarto) '.
                ' INNER JOIN catpais p ON (p.ZipCode = rc.ZipCode) ';

        $sql_where = ' WHERE ';
        $sql_where_conditions[] = $resultingQuery["conditions"];
        $sql_where_conditions[] = " rc.CodigoCuarto != " . $codigo_cuarto ;
        $sql_where_conditions[] = " rc.EstadoRoommate = " . self::PUBLISHED_STATE;
        $sql_where_conditions[] = " (  f.Principal = 1 or f.Principal is null) ";

        if(count($sql_where_conditions) > 0) {
            $sql_where .= implode(" AND ", $sql_where_conditions);
        } else {
            $sql_where = "";
        }

        $sql_order_by = " ORDER BY id DESC";

        $sql_limit = " LIMIT  0, 6" ;

        $sql = $sql_select . $sql_where . $sql_order_by . $sql_limit;
        $result = $db->fetchAll($sql);
        foreach ($result as $key => $r){
        	$result[$key] = get_object_vars($r);
        }
        $rs->rows = $result;
        $rs->total=$db->fetchOne("SELECT FOUND_ROWS()");
        return $rs;
    }

    /**
     *
     * @param Integer $uid Uid de Facebook del usuario
     * @param String $table_room Tabla de roommates
     * @param String $conditions NULL, No Aplicar condiciones, cualquier valor si aplicar
     * @param String $ideal_room NULL, No Aplicar cuarto ideal, cualquier valor si aplicar
     * @param String $myInfo NULL, No Aplicar Informaci�n personal, cualquier valor si aplicar
     * @param String $ideal_partner
     * @param Array $coordinates
     * @return Array Condiciones y Uniones para formar consulta
     */
    public function PrepareWherePartStatement($uid,
            $table_room,
            $conditions,
            $ideal_room,
            $myInfo,
            $ideal_partner,
            $coordinates,
            $min_price = null,
            $max_price = null) {

        $sql_query = array();
        $sql_filter = array();
        
        $roommate = new Hm_Rmt_Roommate();

        try{
            $rs = $roommate->fetchAll(
                        $roommate->select()
                        ->where('Uid = ? ', $uid));
        }catch (Exception $exc){

        }
        
        if($rs != null and $rs->count() > 0){
            $curRoommate = $rs->current();
            $roommate = array();

            //Asignamos cada uno de los valores que tiene el roommate
            $roommateValueByLanguage = array(
                    'TipoPropiedad'     => $curRoommate->TipoPropiedad,
                    'TipoCuarto'        => $curRoommate->TipoCuarto,
                    'TipoBano'          => $curRoommate->TipoBano,
                    'Amueblado'         => $curRoommate->Amueblado,
                    'GeneroBusca'       => $curRoommate->GeneroBusca,
                    'HijosBusco'        => $curRoommate->HijosBusco,
                    'MascotaBusca'      => $curRoommate->MascotaBusca,
                    'FumadorBusca'      => $curRoommate->FumadorBusca,
                    'OcupacionBusca'    => $curRoommate->OcupacionBusca,
                    'FumadorAcepta'     => $curRoommate->FumadorAcepta,
                    'OcupacionAcepta'   => $curRoommate->OcupacionAcepta,
                    'GeneroAcepta'      => $curRoommate->GeneroAcepta,
                    'ParejaAcepta'      => $curRoommate->ParejaAcepta,
                    'MascotaAcepta'     => $curRoommate->MascotaAcepta,
                    'NinosAcepta'       => $curRoommate->NinosAcepta
                );


           $roomValueByVal = array(
                    'FechaMudanza', 'EdadMinimaBusca', 'EdadMaximaBusca', 'EdadMinimaAcepta',
                    'EdadMaximaAcepta', 'EstanciaMinima', 'MontoRenta'
            );

           
           //Obtenemos el valor de cada atributo del roommate
           $fieldRoomTable = $this->GetValueDomains($roommateValueByLanguage);

            foreach ($roommateValueByLanguage as $index => $value) {
                if(array_key_exists($value, $fieldRoomTable)){
                    $roommate[$index] = $fieldRoomTable[$value];
                }
            }

           foreach ($roomValueByVal as $value) {
               $roommate[$value] = $curRoommate->$value;
           }
            $_DOESNT_MATTER = "No importa";
            $_DONT_SHOW = "No mostrar";
            $_OTHER = "Otro";
            $_TABLE_VALUE_DOMAIN = "CATValorDominio";

            //Condiciones Generales
            if($conditions != NULL){
                $sql_filter[] = "(" .
                                "(". $table_room . ".MontoRenta <= " . $roommate["MontoRenta"] . ") OR " .
                                "(". $table_room. ".MontoRenta = NULL ) OR ".
                                $roommate["MontoRenta"] . " = NULL OR " . $roommate["MontoRenta"] . " = 0"
                                .")";

                $sql_filter[] = "(".
                                   "(" . $table_room .".FechaDisponible <= '" . $roommate["FechaMudanza"] . "') OR " .
                                    $table_room . ".FechaDisponible = NULL OR '" . $roommate["FechaMudanza"] . "' = NULL " .
                                ")";

                $sql_filter[] = "(".
                                   "(" . $table_room .".EstanciaMinima <= " . $roommate["EstanciaMinima"] .") OR " .
                                   $table_room.".EstanciaMinima = NULL OR " . $roommate["EstanciaMinima"] . " = NULL " .
                                ")";
            }

            if($ideal_room != NULL){
                //Amueblado
               /* $slq_query[] = $_TABLE_VALUE_DOMAIN . " VTA ON " . $table_room . ".Amueblado = VTA.CodigoValor";
                $sql_filter[] = "(" .
                                    "("
                                ")";*/

                //Tipo de Propiedad
               $sql_query[] = "$_TABLE_VALUE_DOMAIN VTP ON " .$table_room .".TipoPropiedad = VTP.CodigoValor";
               $sql_filter[] = "(   ('" . $roommate["TipoPropiedad"]. "' = '$_DOESNT_MATTER' )" . " OR (
                                    VTP.NombreValor = '". $roommate["TipoPropiedad"] . "') OR
                                    (VTP.NombreValor = '$_OTHER')
                                ) ";

               //Tipo de Cuarto
               $sql_query[] = "$_TABLE_VALUE_DOMAIN VTC ON " . $table_room .".TipoCuarto = VTC.CodigoValor";
               $sql_filter[] = "(
                                    ('". $roommate["TipoCuarto"] ."' = '$_DOESNT_MATTER') OR
                                    (
                                        (VTC.NombreValor = 'Individual' AND '".$roommate["TipoCuarto"] ."' = 'Individual')
                                         OR ('". $roommate["TipoCuarto"]."' = 'Compartido' AND VTC.NombreValor IN ('Dos personas','+2 Personas'))
                                    )
                                 )";

               //Tipo de Ba�o
               $sql_query[] = $_TABLE_VALUE_DOMAIN ." VTB ON ". $table_room .".TipoBano = VTB.CodigoValor";
               $sql_filter[] = "(
                                    ('".$roommate["TipoBano"] ."' = VTB.NombreValor) OR
                                     ('".$roommate["TipoBano"] ."' = '$_DOESNT_MATTER')
                                )";
            }


    #############Informaci�n Personal Roommate############
            if($myInfo != NULL){
                $sql_filter[] = "(" .
                                    "(" . $table_room .".EdadMinimaAcepta <= " . $roommate["EdadMinimaBusca"] . " AND " .
                                        $table_room .".EdadMaximaAcepta >= " . $roommate["EdadMinimaBusca"] .
                                    ") OR ".
                                    "(" . $table_room .".EdadMinimaAcepta = NULL AND " .$table_room .".EdadMaximaAcepta = NULL) OR" .
                                    "(" . $roommate["EdadMinimaBusca"] . " = NULL OR " . $roommate["EdadMaximaBusca"] . " = NULL )" .
                                ")";

                //Tipo de FumadorAcepta VTF
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTF ON ". $table_room . ".FumadorAcepta = VTF.CodigoValor";
                $sql_filter[] = "(".
                                    "(VTF.NombreValor = '" . $roommate["FumadorBusca"] ."') OR " .
                                    "(VTF.NombreValor = '". $_DOESNT_MATTER ."') OR ".
                                    "('" .$roommate["FumadorBusca"] . "' = '" . $_DONT_SHOW ."')".
                                ")";

               //OcupacionAcepta VTOC
               $sql_query[] = $_TABLE_VALUE_DOMAIN. " VTOC ON ". $table_room . ".OcupacionAcepta = VTOC.CodigoValor";
               $sql_filter[] = "(".
                                "(VTOC.NombreValor = '" . $roommate["OcupacionBusca"] . "') OR ".
                                "(VTOC.NombreValor = '" . $_DOESNT_MATTER . "') ".
                               ")";

               //GeneroAcepta VTG
               $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTG ON " . $table_room . ".GeneroAcepta = VTG.CodigoValor";
               $sql_filter[] = "(".
                                "(VTG.NombreValor = '" . $roommate["GeneroBusca"] ."') OR".
                                "(VTG.NombreValor = '" .$_DOESNT_MATTER . "') ".
                             ")";

               //ParejaAcepta VTPA
               $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTPA ON " . $table_room . ".GeneroAcepta = VTPA.CodigoValor";
               $sql_filter[] = "(".
                                "(VTPA.NombreValor = 'Si' AND '" . $roommate["GeneroBusca"] . "' = 'Pareja') OR ".
                                "(VTPA.NombreValor = '" . $_DOESNT_MATTER ."' AND '" . $roommate["GeneroBusca"] . "' = 'Pareja') ".
                               ")";

               //Mascota Acepta VTM
               $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTM ON " . $table_room . ".MascotaAcepta = VTM.CodigoValor";
               $sql_filter[] = "(".
                                   "(VTM.NombreValor = '" . $roommate["MascotaBusca"] . "') OR " .
                                   "('". $roommate["MascotaBusca"] . "' = '" .$_DONT_SHOW . "') OR ".
                                   "(VTM.NombreValor = '". $_DOESNT_MATTER ."') ".
                               ")";

               //OrientacionAcepta VTO
               $sql_query[] = $_TABLE_VALUE_DOMAIN. " VTO ON " . $table_room . ".OrientacionAcepta = VTO.CodigoValor";
               $sql_filter[] = "(".
                                "(VTO.NombreValor = '" . $roommate["OrientacionBusca"] . "') OR ".
                                "(VTO.NombreValor = '". $_DOESNT_MATTER . "') OR ".
                                "('". $roommate["OrientacionBusca"] . "' = '" . $_DONT_SHOW . "')".
                               ")";

               //Ni�osAcepta VTN
               $sql_query[] = $_TABLE_VALUE_DOMAIN. " VTN ON " . $table_room . ".NinosAcepta = VTN.CodigoValor";
               $sql_filter[] = "(".
                                "(VTN.NombreValor = '" . $roommate["HijosBusco"] . "') OR " .
                                "('". $roommate["HijosBusco"] . "' = '" . $_DONT_SHOW . "') OR " .
                                "(VTN.NombreValor = '" . $_DOESNT_MATTER . "') ".
                               ")";
            }

    ########## Compa�ero Ideal ###########
            if($ideal_partner != NULL){
                if($myInfo == NULL){
                   $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTPA ON " . $table_room . ".GeneroAcepta = VTPA.CodigoValor";
                }
                //GeneroHabitan VTGH
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTGH ON " . $table_room . ".GeneroHabitan = VTGH.CodigoValor";
                $sql_filter[] = "(" .
                                    "(VTGH.NombreValor = '" . $roommate["GeneroAcepta"] . "') OR ".
                                    "('". $roommate["GeneroAcepta"] . "' = '" . $_DOESNT_MATTER . "')
                                  )";

                //Parejas
                $sql_filter[] ="((VTGH.NombreValor = 'Pareja' AND '". $roommate["ParejaAcepta"] ."' = 'Si') OR " .
                                    "(VTPA.NombreValor = '" . $_DOESNT_MATTER . "')".
                                ")";


                //HijosHabitan VTHH
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTHH ON " . $table_room . ".HijosHabitan = VTHH.CodigoValor";
                $sql_filter[] = "(".
                                    "(VTHH.NombreValor = '" . $roommate["NinosAcepta"] . "') OR ".
                                    "(VTHH.NombreValor = '" . $_DONT_SHOW . "') OR ".
                                    "('". $roommate["NinosAcepta"] . "' = '". $_DOESNT_MATTER . "')".
                                ")";

                //MascotaHabitan VTMH
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTMH ON " . $table_room . ".MascotaHabitan = VTMH.CodigoValor";
                $sql_filter[] = "(".
                                    "(VTMH.NombreValor = '" . $roommate["MascotaAcepta"] . "') OR ".
                                    "('" . $roommate["MascotaAcepta"] . "' = '" . $_DOESNT_MATTER . "') OR " .
                                    "(VTMH.NombreValor = '" . $_DONT_SHOW . "')".
                                ")";

                //FumadorHabitan VTFH
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTFH ON ". $table_room . ".FumadorHabitan = VTFH.CodigoValor";
                $sql_filter[] = "(".
                                    "(VTFH.NombreValor = '" . $roommate["FumadorAcepta"] . "') OR ".
                                    "('" . $roommate["FumadorAcepta"] . "' = '" . $_DOESNT_MATTER . "') OR " .
                                    "(VTFH.NombreValor = '" . $_DONT_SHOW . "')".
                                 ")";

                //OrientacionHabitan VTOH
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTOH ON ". $table_room . ".OrientacionHabitan = VTOH.CodigoValor";
                $sql_filter[] = "(".
                                    "(VTOH.NombreValor = '" . $roommate["OrientacionAcepta"] . "') OR ".
                                    "('" . $roommate["OrientacionAcepta"] . "' = '" . $_DOESNT_MATTER . "') OR " .
                                    "(VTOH.NombreValor = '" . $_DONT_SHOW . "')".
                                ")";

                //OcupacionHabitan VTOCH
                $sql_query[] = $_TABLE_VALUE_DOMAIN . " VTOCH ON ". $table_room. ".OcupacionHabitan = VTOCH.CodigoValor";
                $sql_filter[] = "(".
                                    "('".$roommate["OcupacionAcepta"] . "' = '" . $_DOESNT_MATTER . "') OR ".
                                    "(VTOCH.NombreValor = '" . $roommate["OcupacionAcepta"] . "')".
                                ")";

                //EdadHabitan
                $sql_filter[] = "(".
                                    "(". $table_room . ".EdadMinimaHabitan >= " . $roommate["EdadMinimaAcepta"] . " AND " . $table_room .".EdadMaximaHabitan <= ". $roommate["EdadMaximaAcepta"] . ") OR ".
                                    $table_room. ".EdadMinimaHabitan = NULL OR " . $table_room . ".EdadMaximaHabitan = NULL OR ".
                                    $roommate["EdadMinimaAcepta"] . " = NULL OR " . $roommate["EdadMaximaAcepta"] . " = NULL" .
                                ")";
            }
        }


        
         // Precio minimo
        if ($min_price != null && $max_price != null) {
                    $sql_filter[] = " " . $table_room . ".MontoRenta >= " . doubleval($min_price) . " AND " . $table_room . ".MontoRenta <= " . doubleval($max_price);
        } else {
            if ($min_price != null) {
                        $sql_filter[] = " " . $table_room . ".MontoRenta >= " . doubleval($min_price);
            } elseif ($max_price != null) {
                        $sql_filter[] = " " . $table_room . ".MontoRenta <= " . doubleval($max_price);
            }
        }

        // Coordenadas del mapa
        if($coordinates != null && is_array($coordinates)) {
            $sql_filter[] = " MBRContains(".
                    "GeomFromText('Polygon((". $coordinates["minX"] . " " . $coordinates["minY"] . " " . "," . $coordinates["minX"] . " " . $coordinates["maxY"] . "," . $coordinates["maxX"] . " " . $coordinates["maxY"] . "," . $coordinates["maxX"] . " " . $coordinates["minY"]. "," . $coordinates["minX"] . " " . $coordinates["minY"] . "))' ), " .
                    "GeomFromWKB(Point(". $table_room . ".Longitud, ". $table_room . ".Latitud))) ";

        }

        $sql_filter = implode(" AND ", $sql_filter);
        if(count($sql_query) > 0){
            $sql_query = " LEFT OUTER JOIN " . implode(" LEFT OUTER JOIN ", $sql_query);
        }else{
            $sql_query = '';
        }
        return array("conditions" => $sql_filter, "query" => $sql_query);
    }

    /**
     *
     * @param Integer $uid Uid de Facebook
     * @param String $conditions S, para saber si aplican condiciones
     * @param String $ideal_room S, para aplicar filtros relacionados
     * @param String $myInfo S, para aplicar filtros relacionados
     * @param String $ideal_partner S, para aplicar filtros relacionados
     * @param String $coordinates S, para aplicar filtros relacionados
     * @param <type> $start cota inferior consulta
     * @param <type> $limit cota superior consulta
     * @return <type>
     */
    public function FilterStatesOnDir(
            $uid,
            $conditions,
            $ideal_room,
            $myInfo,
            $ideal_partner,
            $coordinates,
            $min_price,
            $max_price,
            $start = 0,
            $limit = 10) {

        $sql_filter = array();
        $localLangague = Zend_Registry::get('Zend_Locale');
        $resultingQuery = $this->PrepareWherePartStatement($uid, "rc", $conditions, $ideal_room, $myInfo, $ideal_partner, $coordinates, $min_price, $max_price);

        $sql_select = "SELECT DISTINCT ".
                "rc.CodigoCuarto, ".
                "rc.Titulo, ".
                "rc.Descripcion, ".
                "rc.Direccion, ".
                "rc.Estado, ".
                "rc.Ciudad, ".
                "rc.ZipCode, ".
                "FORMAT(rc.MontoRenta,2) as PrecioRenta, ".
                "FORMAT(rc.MontoServicios,2) as Servicios, ".
                "cvi.ValorIdioma as TipoCuarto, ".
                "rc.FechaRegistro, ".
                "rc.IncluyeServicio, ".
                "rc.Latitud, ".
                "rc.Longitud, ".
                "rc.Uid, ".
                "CM.SimboloMoneda as SimboloMoneda, ".
                "CM.CodigoEstandar as CodigoMoneda, ".
                "f.RMTFotoID ".
                "FROM ".
                "RMTCuarto rc ".
                " INNER JOIN CATValorDominioIdioma cvi on (rc.TipoCuarto = cvi.CodigoValor) ".
                " LEFT OUTER JOIN RMTFoto f on (rc.CodigoCuarto = f.CodigoCuarto) ".
                " LEFT OUTER JOIN catpais pa on (rc.ZipCode=pa.ZipCode) 
                  LEFT OUTER JOIN catmoneda CM ON pa.CodigoMoneda = CM.CodigoMoneda " . $resultingQuery["query"];
                  


        $sql_filter[] = $resultingQuery["conditions"];
        $sql_filter[] = " rc.EstadoRoommate = " . self::PUBLISHED_STATE;
        $sql_filter[] = " ( f.Principal = 1 or f.Principal is null) ";
        $sql_filter[] = " cvi.CodigoIdioma = '$localLangague'";

        if(count($sql_filter) > 0) {
            $sql_filter = " WHERE " . implode(" AND ", $sql_filter);
        } else {
            $sql_filter = "";
        }

        $sql_orderBy = " ORDER BY rc.CodigoCuarto DESC";

        $sql = $sql_select . $sql_filter . $sql_orderBy ;
        
        return $this->DoQuery($sql, null);
    }

}
?>
