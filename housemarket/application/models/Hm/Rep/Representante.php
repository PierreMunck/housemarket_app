<?php

/**
 * Representantes de HouseMarket
 */
class Hm_Rep_Representante extends Zend_Db_Table {

    /**
     * Nombre de la tabla de representantes
     */
    protected $_name = 'representante';

    /**
     * Nombre de la llave primaria de la tabla de representantes
     * @var String
     */
    protected $_primary = 'CodigoRepresenta';

    /**
     * Nombre del adaptador
     * @var Zend_DbAdapter
     */
    public $dbAdapter;

    protected $_referenceMap    = array(
        'Hm_Cat_Pais' => array(
            'columns'           => 'ZipCode',
            'refTableClass'     => 'Hm_Cat_Pais',
            'refColumns'        => 'ZipCode'
        )
    );

    /**
     * Recupera todos los representantes
     * @return Recordset
     */
    public static function GetRepresentats() {
        $representante = new Hm_Rep_Representante();
        $representantes = $representante->fetchAll();

        if($representantes != null && count($representantes) > 0) {
            $idsRep = array();
            $repr = array();
            foreach($representantes as $r) {
                // Guardar los ids
                $idsRep[] = $r->Uid;
                // Indexarlos para luego facilmente recuperarlos
                $repr[$r->Uid] = $r;
            }
            // Recuperar los usuarios de facebook
            $fbDgt = Dgt_Fb::getInstance();
            $repFb = $fbDgt->execFql("SELECT uid, first_name, last_name, pic_square, profile_url, current_location, hometown_location FROM user WHERE uid IN (" . implode(",", $idsRep) . ") AND is_app_user");
            $representantes = array();
            if(is_array($repFb)) {
                foreach($repFb as $r) {
                    $curRep = $repr[$r["uid"]];
                    $curRepresentante = array();
                    $curRepresentante["uid"] = $r["uid"];
                    $curRepresentante["first_name"] = $r["first_name"];
                    $curRepresentante["last_name"] = $r["last_name"];
                    $curRepresentante["pic_square"] = $r["pic_square"];
                    $curRepresentante["profile_url"] = $r["profile_url"];
                    $curRepresentante["current_location"] = $r["current_location"];
                    $curRepresentante["hometown_location"] = $r["hometown_location"];
                    $curRepresentante["CodigoRepresenta"] = $curRep->CodigoRepresenta;
                    $curRepresentante["NombreRepresenta"] = $curRep->NombreRepresenta;
                    $curRepresentante["NombreEmpresa"] = $curRep->NombreEmpresa;
                    $curRepresentante["Direccion"] = $curRep->Direccion;
                    $curRepresentante["ZipCode"] = $curRep->ZipCode;
                    $curRepresentante["Telefono"] = $curRep->Telefono;
                    $curRepresentante["EMail"] = $curRep->EMail;
                    $curRepresentante["Fax"] = $curRep->Fax;
                    $curRepresentante["idusuario"] = $curRep->idusuario;

                    $representantes[] = $curRepresentante;
                }
            }
        }
        return $representantes;
    }

}

?>
