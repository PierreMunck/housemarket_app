<?php
/**
 * Description of Hm_Pro_EnlistamientoPropiedad
 *
 * @author Administrador
 */
class Hm_Cat_EnlistamientoIdioma extends Zend_Db_Table {

    protected $_name = 'catenlistaidioma';

    protected  $_primary = 'CATEnlistaIdiomaID';

    public $dbAdapter;

    protected $_referenceMap = array(
        'Hm_Cat_Enlistamiento' => array(
            'columns' => 'CodigoEnlistamiento',
            'refTableClass' => 'Hm_Cat_Enlistamiento',
            'refColumns' => 'CATEnlistaIdiomaID'
        ),
        'Hm_Cat_Idioma' => array(
            'columns' => 'CodigoIdioma',
            'refTableClass' => 'Hm_Cat_Idioma',
            'refColumns' => 'CodigoIdioma'
        )
    );
}
?>
