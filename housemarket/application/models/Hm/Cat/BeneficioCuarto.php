<?php
/**
 * Description of BeneficioCuarto
 *
 * @author scottaviles@yahoo.es
 */
class Hm_Cat_BeneficioCuarto extends Zend_Db_Table {

    protected $_name = 'RMTBeneficio';

    protected $_primary = 'RMTBeneficioID';

    public $dbAdapter;

    protected $_referenceMap    = array(
        'Hm_Rmt_Cuarto' => array(
            'columns'           => 'CodigoCuarto',
            'refTableClass'     => 'Hm_Rmt_Cuarto',
            'refColumns'        => 'CodigoCuarto'
        )
    );

    /**
     * Guarda o actualiza las propiedades.
     * @param <type> $roomId Id de la propiedad
     * @param <type> $benefits Listado de atributos
     */

    public static function SaveOrUpdate($roomId, $benefits = array()) {
        $rowsId = array();
        $fails = array();
        $beneficioCuarto = new Hm_Cat_BeneficioCuarto();
        if($roomId != null && is_array($benefits)) {
            // Remover los beneficios que estan deseleccionado
            $where = $beneficioCuarto->getAdapter()->quoteInto("CodigoCuarto=? ", $roomId);
            $beneficioCuarto->delete($where);

            // Guardar los benefits que estan seleccionados
            foreach($benefits as $id=>$benefit) {
                try {
                    try {
                        $rs = $beneficioCuarto->find($roomId, $benefit);
                    } catch (Exception $exc) {
                    }


                    if($rs == null || $rs->count() == 0) { // Verificar que ya exista

                        $data["BeneficioCuarto"] = $benefit;
                        $data["CodigoCuarto"] = $roomId;
                        $beneficioCuarto->insert($data);
                        $rowsId[] = $beneficioCuarto->getAdapter()->lastInsertId();
                    } else {
                        // No se inserta una nueva
                    }
                }catch(Exception $ex) {
                    $fails[] = array("id" => $benefit , "reason"=>$ex->getMessage());
                }
            }
        } else {
            if(!is_array($benefits)) { // Remover todos los beneficios
                $where = $beneficioCuarto->getAdapter()->quoteInto("CodigoCuarto=?", $roomId);
                $beneficioCuarto->delete($where);
            }
            if($estatedId == null) {
                $fails[] = array("id" => $roomId, "reason"=>"Estate id is null");
            }
        }
        return array("success"=>$rowsId, "fails"=>$fails);
    }

    
}
?>
