<?php
class Hm_Cat_Atributo extends Zend_Db_Table {

    /**
     * Nombre de la tabla
     * @var String
     */
    protected $_name = 'catatributo';

    /**
     * Nombre de la llave primaria
     * @var String
     */
    protected $_primary = 'CodigoAtributo';

    protected $_dependentTables = array(
                                    'Hm_Cat_AtributoCategoria',
                                    'Hm_Pro_AtributoPropiedad'
                                  );

    /**
     *
     * @var Zend_DbAdapter
     */
    public $dbAdapter;
}
?>
