<?php

class Hm_Cat_BeneficioIdioma extends Zend_Db_Table {
    protected $_name = 'catbeneficioidioma';

    protected $_primary = 'CATBeneficioIdiomaID';

    public $dbAdapter;

    protected $_referenceMap    = array(
        'Hm_Cat_Beneficio' => array(
            'columns'           => 'CodigoBeneficio',
            'refTableClass'     => 'Hm_Cat_Beneficio',
            'refColumns'        => 'CodigoBeneficio'
        ),
        'Hm_Cat_Idioma'   => array(
            'columns'           => 'CodigoIdioma',
            'refTableClass'     => 'Hm_Cat_Idioma',
            'refColumns'        => 'CodigoIdioma'
        )

    );
}
?>
