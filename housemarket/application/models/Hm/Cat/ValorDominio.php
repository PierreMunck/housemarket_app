<?php
class Hm_Cat_ValorDominio extends Zend_Db_Table {
    protected $_name = 'CATValorDominio';

    protected $_primary = 'CodigoValor';

    public $dbAdapter;

    protected $_referenceMap    = array(
        'Hm_Cat_Categoria' => array(
            'columns'           => 'CodigoDominio',
            'refTableClass'     => 'Hm_Cat_Dominio',
            'refColumns'        => 'CodigoDominio'
        )
    );
}
?>
