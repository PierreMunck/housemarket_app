<?php
 
class Hm_Cat_Categoria extends Zend_Db_Table { 

	private static $instance;
	
    /**
     * Nombre de la tabla de Categoria
     * @var String
     */
    protected $_name = 'catcategoria';

    /**
     * Nombre de la llave primaria
     * @var String
     */
    protected $_primary = 'CodigoCategoria';

    /**
     * Referencias
     * @var array
     */
    protected $_dependentTables = array(
                                        'Hm_Cat_AtributoCategoria',
                                        'Hm_Cat_BeneficioCategoria'
                                  );

    /**
     * Adaptador
     * @var Zend_DbAdapter
     */
    public $dbAdapter;

    // m�todo singleton
    public static function getInstance()
    {
    	if (!isset(self::$instance)) {
    		$c = __CLASS__;
    		self::$instance = new $c;
    	}
    
    	return self::$instance;
    }
    /**
     * Recupera las categorias registradas en el sistema
     * @param Zend_Locale $locale Localizacion de las categorias.
     * @return array Listado de las categorias
     */
    public function GetCategoriesList($locale){
        $translator = Zend_Registry::get('Zend_Translate');
        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $categoriesTypes = new Hm_Cat_CategoriaIdioma();
        $where = $categoriesTypes->getAdapter()->quoteInto("CodigoIdioma = ?",$language);    

        $order = "ValorIdioma ASC";
        $rs = $categoriesTypes->fetchAll($where, $order);         
       
        $categoriesList = array();

        $categoriesList[""] = $translator->getAdapter()->translate('SEARCH_PROPERTY_ALL');
        if($rs->count() > 0) {
            foreach($rs as $type) {
                $categoriesList[$type->CodigoCategoria] = $type->ValorIdioma;
            }
        }
       
        return $categoriesList;  
       

    }
	
	public function GetCat() {
	    $translator = Zend_Registry::get('Zend_Translate');
        $language = Zend_Registry::get('language');

        $sql = "  SELECT c.CodigoCategoria,ai.ValorIdioma  as nombre";
		$sql .= "  FROM catcategoria c";
        $sql .= " INNER JOIN catcategoriaidioma ai ON ai.CodigoCategoria = c.CodigoCategoria";   
        $sql .= " WHERE ".$this->_db->quoteInto("ai.CodigoIdioma = ?", strtoupper($language));
        $sql .= " AND c.filtro = 1 ";
        $sql .= " ORDER BY c.orden";
        
        // Resultados
        $rs = $this->_db->query($sql, null); 
        $att = $rs->fetchAll();
	   
       $categoriesList = array();
        $categoriesList[""] = $translator->getAdapter()->translate('SEARCH_PROPERTY_ALL');
	   foreach($att as $type) {						   
           $categoriesList[$type->CodigoCategoria] = $type->nombre;         
        }		
        return $categoriesList;  
		
    }

    public function GetAtributes($categoryId) {
        $language = Zend_Registry::get('language');

        $sql = "SELECT a.CodigoAtributo as codigo, ai.ValorIdioma  as nombre, (CASE ac.CampoDestino WHEN 'ValorEntero' THEN 'Range' WHEN 'ValorDecimal' THEN 'Range' WHEN 'ValorS_N' THEN 'Check' WHEN 'ValorCaracter' THEN 'Like' ELSE ac.CampoDestino END) as tipo FROM catatributocategoria ac INNER JOIN catatributo a ON (ac.CodigoAtributo = a.CodigoAtributo)";
        $sql .= " INNER JOIN catatributoidioma ai ON ai.CodigoAtributo = a.CodigoAtributo";
        $sql .= " INNER JOIN catcategoria c ON ac.CodigoCategoria = c.CodigoCategoria ";
        $sql .= " WHERE ac.CodigoCategoria = " . intval($categoryId) . " AND ac.Filtro=1 AND " . $this->_db->quoteInto("ai.CodigoIdioma = ?", strtoupper($language));
        $sql .= " AND c.filtro = 1 ";
        $sql .= " ORDER BY ac.OrdenPresenta";

        // Resultados
        $rs = $this->_db->query($sql, null); 
        $_attributes = $rs->fetchAll();
        $attributes = array();
        $total = $rs->rowCount();
        foreach($_attributes as $key=>$attribute) {
        	$attributes[$key] = array();
            $attributes[$key]["nombre"] = ($attribute->nombre);
            $attributes[$key]["tipo"] = ($attribute->tipo);
        }
        return $attributes;
    }
    

    public function GetAllAtributes($categoryId) {
    	
        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();

        $sql = "SELECT a.CodigoAtributo as codigo, ai.ValorIdioma as nombre, (CASE ac.CampoDestino WHEN 'ValorEntero' THEN 'Range' WHEN 'ValorDecimal' THEN 'Range' WHEN 'ValorS_N' THEN 'Check' WHEN 'ValorCaracter' THEN 'Like' ELSE ac.CampoDestino END) as tipo FROM catatributocategoria ac INNER JOIN catatributo a ON (ac.CodigoAtributo = a.CodigoAtributo)";
        $sql .= " INNER JOIN catatributoidioma ai ON ai.CodigoAtributo = a.CodigoAtributo
                  INNER JOIN catidioma i ON ai.CodigoIdioma = i.CodigoIdioma";
        $sql .= " WHERE ai.CodigoIdioma = '" . $language . "' AND ac.CodigoCategoria = " . intval($categoryId);
        $sql .= " ORDER BY ac.OrdenPresenta";

        // Resultados
        $rs = $this->_db->query($sql, null);
        $_attributes = $rs->fetchAll();
        $attributes = array();
        $total = $rs->rowCount();

        foreach($_attributes as $key=>$attribute) {
        	$attributes[$key] = array();
            $attributes[$key]["nombre"] = $attribute->nombre;
            $attributes[$key]["tipo"] = $attribute->tipo;
        }
        return $attributes;
    }

   public function GetBeneficios($categoryId) {

        $language = Zend_Registry::get('language');

        $sql = "SELECT b.CodigoBeneficio as codigo, bi.ValorIdioma as nombre 
                FROM catbeneficio b
                INNER JOIN catbeneficiocategoria bc ON b.CodigoBeneficio=bc.CodigoBeneficio ".
               "INNER JOIN catbeneficioidioma bi ON b.CodigoBeneficio = bi.CodigoBeneficio ".
               "INNER JOIN catidioma i ON bi.CodigoIdioma = i.CodigoIdioma ".
               "WHERE bc.CodigoCategoria=" . $categoryId . " AND " . $this->_db->quoteInto("bi.CodigoIdioma = ?", strtoupper($language)) ;

        // Resultados
        $rs = $this->_db->query($sql, null);
        $beneficios = $rs->fetchAll();
        foreach($beneficios as $key=>$beneficio) {
        	$beneficios[$key] = array();
			
            $beneficios[$key]["codigo"] = $beneficio->codigo;
            $beneficios[$key]["nombre"] = $beneficio->nombre;
        }
        return $beneficios;
    }
}

?>
