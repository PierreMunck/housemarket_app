<?php

class Hm_Cat_BeneficioCategoria extends Zend_Db_Table {

    protected $_name = 'catbeneficiocategoria';

    protected $_primary = array('CodigoCategoria','CodigoBeneficio');

    public $dbAdapter;

    protected $_referenceMap    = array(
        'Hm_Cat_Categoria' => array(
            'columns'           => 'CodigoCategoria',
            'refTableClass'     => 'Hm_Cat_Categoria',
            'refColumns'        => 'CodigoCategoria'
        ),
        'Hm_Cat_Beneficio' => array(
            'columns'           => 'CodigoBeneficio',
            'refTableClass'     => 'Hm_Cat_Beneficio',
            'refColumns'        => 'CodigoBeneficio'
        )
    );

}

?>
