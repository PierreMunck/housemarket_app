<?php

class Hm_Cat_AtributoIdioma extends Zend_Db_Table {

    protected $_name = 'catatributoidioma';

    protected $_primary = 'CATAtributoIdiomaID';

    public $dbAdapter;

    protected $_referenceMap    = array(
        'Hm_Cat_Atributo' => array(
            'columns'           => 'CodigoAtributo',
            'refTableClass'     => 'Hm_Cat_Atributo',
            'refColumns'        => 'CodigoAtributo'
        ),
        'Hm_Cat_Idioma'   => array(
            'columns'           => 'CodigoIdioma',
            'refTableClass'     => 'Hm_Cat_Idioma',
            'refColumns'        => 'CodigoIdioma'
        )

    );
}
?>
