<?php

class Hm_Cat_ValorDominioIdioma {
     /**
     * Nombre de la tabla de Dominios
     * @var String
     */
    protected $_name = 'CATValorDominioIdioma';

    /**
     * Nombre de la llave primaria
     * @var String
     */
    protected $_primary = 'CATValorDominioIdiomaID';

    public $dbAdapter;

    protected $_referenceMap    = array(
        'Hm_Cat_ValorDominio' => array(
            'columns'           => 'CodigoValor',
            'refTableClass'     => 'Hm_Cat_ValorDominio',
            'refColumns'        => 'CodigoValor'
        ),
        'Hm_Cat_Idioma'   => array(
            'columns'           => 'CodigoIdioma',
            'refTableClass'     => 'Hm_Cat_Idioma',
            'refColumns'        => 'CodigoIdioma'
        )

    );


}
?>
