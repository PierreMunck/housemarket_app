<?php

class Hm_Cat_CategoriaIdioma extends Zend_Db_Table{
    protected $_name = 'catcategoriaidioma';

    protected  $_primary = 'CATCategoriaIdiomaID';

    protected $_referenceMap = array(
        'Hm_Cat_Categoria' => array(
            'columns'      => 'CodigoCategoria',
            'refTableClass'=> 'Hm_Cat_Categoria',
            'refColumns'   => 'CodigoCategoria'            
        ),
       
              
        'Hm_Cat_Idioma'    => array(
            'columns'      => 'CodigoIdioma',
            'refTableClass'=> 'Hm_Cat_Idioma',
            'refColumns'   => 'CodigoIdioma'
        )
    );

        
}
?>
