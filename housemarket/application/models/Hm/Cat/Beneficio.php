<?php

class Hm_Cat_Beneficio extends Zend_Db_Table {

    /**
     * Nombre de la tabla
     * @var String
     */
    protected $_name = 'catbeneficio';

    /**
     * Nombre de la llave primaria
     */
    protected $_primary = 'CodigoBeneficio';

    /**
     *
     * @var Zend_DbAdapter
     */
    public $dbAdapter;

    protected $_dependentTables = array(
                                    'Hm_Pro_BeneficioPropiedad',
                                    'Hm_Cat_BeneficioCategoria'
                                  );


}

?>
