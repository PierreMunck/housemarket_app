<?php
/**
 * Description of Idioma
 *
 * @author Administrador
 */
class Hm_Cat_Enlistamiento extends Zend_Db_Table {
     /**
     * Nombre de la tabla de Categoria
     * @var String
     */
    protected $_name = 'cattipoenlista';

    /**
     * Nombre de la llave primaria
     * @var String
     */
    protected $_primary = 'CATEnlistaIdiomaID';

    /**
     * Referencias
     * @var array
     */

    protected $_dependentTables = array(
                                    'Hm_Cat_EnlistamientoIdioma'
                                  );

     /**
     * Retorna los tipos de propiedades. Por ejemplo, Alquiler, compra.
     * @return array Tipo de las propiedades. Por ejemplo: Alquiler
     */
    public static function GetListingTypes() {

        $_SHOW = "S";
        $translator = Zend_Registry::get('Zend_Translate');
        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $listingTypes = new Hm_Cat_EnlistamientoIdioma();
        $select = $listingTypes->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
        $select->setIntegrityCheck(false)
             ->where($listingTypes->getAdapter()->quoteInto("CodigoIdioma = ?", $language))
             ->join("cattipoenlista", "cattipoenlista.CodigoEnlista=catenlistaidioma.CodigoEnlista")
             ->where($listingTypes->getAdapter()->quoteInto("cattipoenlista.MostrarBusqueda = ?", $_SHOW))
             ->order("ValorIdioma ASC");

        $rs = $listingTypes->fetchAll($select);

        $propertiesTypes = array();

        //$propertiesTypes[""] = $translator->getAdapter()->translate('LABEL_SEARCH_FILTER_LISTING_TYPE');
        if($rs->count() > 0) {
            foreach($rs as $type) {
                $propertiesTypes[$type->CodigoEnlista] = $type->ValorIdioma;
            }
        }
        return $propertiesTypes;
    }
}
?>
