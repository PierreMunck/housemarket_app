<?php
class Hm_Cat_Dominio extends Zend_Db_Table{
     /**
     * Nombre de la tabla de Dominios
     * @var String
     */
    protected $_name = 'CATDominio';

    /**
     * Nombre de la llave primaria
     * @var String
     */
    protected $_primary = 'CodigoDominio';

    /**
     * Referencias
     * @var array
     */
    protected $_dependentTables = array(
                                        'Hm_Cat_DominioValor'
                                  );

    /**
     * Adaptador
     * @var Zend_DbAdapter
     */
    public $dbAdapter;

    
}
?>
