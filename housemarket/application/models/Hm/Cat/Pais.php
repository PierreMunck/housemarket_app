<?php

/**
 * Catalogo de paises
 */
class Hm_Cat_Pais extends Zend_Db_Table {

    /**
     * Nombre de la tabla
     * @var String
     */
    protected $_name = 'catpais';

    /**
     * Nombre de la llave primaria
     * @var String
     */
    protected $_primary = 'ZipCode';

    /**
     * Adaptador
     * @var Zend_DbAdapter
     */
    public $dbAdapter;

    protected $_dependentTables = array(
                                        'Hm_Rep_Representante'
                                    );
    
    public function GetCurrenciesByCountry() {
        $translator = Zend_Registry::get('Zend_Translate');

        $sql_select = 'SELECT ' .
                'p.ZipCode as CountryCode, p.Pais as CountryName, m.SimboloMoneda as CurrencySymbol, m.CodigoEstandar as CurrencyCode' .
                ' FROM ' .
                ' catpais p ' .
                ' LEFT JOIN catmoneda m ON (p.CodigoMoneda = m.CodigoMoneda) ' .
                ' ORDER BY CountryName';

        $sql = $sql_select;

        $currenciesByCountry = $this->DoQuery($sql, null);
        return $currenciesByCountry;
    }
    
    /**
     * Realiza una consulta con la base de datos sobre la tabla catpais
     * @param String $sql Consulta a realizar
     * @param array $params Parametros que se le pasan a la consulta a realizar
     * @return RowSet Resultado de la consulta
     */
    private function DoQuery($sql, $params=null) {

        if (isset($params)) {
            $rs = $this->_db->query($sql, $params);
        } else {
            $rs = $this->_db->query($sql);
        }
        $data = $rs->fetchAll();
        
        return $data;
    }

}

?>
