<?php

class Hm_Cat_AtributoCategoria extends Zend_Db_Table {

    protected $_name = 'catatributocategoria';
    
    protected $_primary = array('CodigoCategoria','CodigoAtributo');

    public $dbAdapter;

    protected $_referenceMap    = array(
        'Hm_Cat_Categoria' => array(
            'columns'           => 'CodigoCategoria',
            'refTableClass'     => 'Hm_Cat_Categoria',
            'refColumns'        => 'CodigoCategoria'
        ),
        'Hm_Cat_Atributo' => array(
            'columns'           => 'CodigoAtributo',
            'refTableClass'     => 'Hm_Cat_Atributo',
            'refColumns'        => 'CodigoAtributo'
        )
    );

}

?>
