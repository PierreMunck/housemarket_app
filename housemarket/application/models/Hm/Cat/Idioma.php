<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Idioma
 *
 * @author Administrador
 */
class Hm_Cat_Idioma extends Zend_Db_Table {
	
	private static $instance;
	
     /**
     * Nombre de la tabla de Categoria
     * @var String
     */
    protected $_name = 'catidioma';

    /**
     * Nombre de la llave primaria
     * @var String
     */
    protected $_primary = 'CodigoIdioma';
    
    /**
     * Adaptador para la base de datos
     * @var
     */
    public $dbAdapter;
    
    /**
	 * Current Language del user
	 * @var String
     */
    protected $currentlang = null;

    /**
     * Referencias
     * @var array
     */
    protected $_dependentTables = array(
                                        'Hm_Cat_EnlistamientoIdioma'
                                    );

    /**
     * Nombre del idioma por defecto
     */
    const DEFAULT_LANGUAGE = "EN";

    public static function getInstance() {
    	if (!isset(self::$instance)) {
    		$c = __CLASS__;
    		self::$instance = new $c;
    	}
    	
    	return self::$instance;
    }
    
    // Evita que el objeto se pueda clonar
    public function __clone() {
    	trigger_error('Clone is not allowed.', E_USER_ERROR);
    }
    
    
    public function GetLanguage(){
    	if(!isset($this->currentlang)){
    		$language = Zend_Registry::get('language');
    		$rs = $this->find(strtoupper($language));
    		
    		if($rs->count() > 0) {
    			$curLanguage = $rs->current();
    			if($curLanguage->Activo == 'S') {
    				$this->currentlang=  $curLanguage->CodigoIdioma;
    			}
    		}
    		if(!isset($this->currentlang)){
    			$this->currentlang = self::DEFAULT_LANGUAGE;
    		}
    	}
        return $this->currentlang;
    }

    /**
     * Retorna los idiomas que se dispondra para elecci�n al usuario seg�n el idioma en que se encuentre
     * @return Array Array asociativo con los idiomas.
     */
    public function LanguagueComboBox(){
        $translator = Zend_Registry::get('Zend_Translate');
        $db = Zend_Registry::get('db');
        $language = $this->GetLanguage();
        $dataLanguage = $db->fetchAll("SELECT
              CodigoIdiomaOrigen as CodigoIdioma, ValorIdioma as NombreIdioma FROM catidiomaidioma
             WHERE CodigoIdiomaDestino = '".$language."'");

        $data = array();
        $data[""] = $translator->getAdapter()->translate('LABEL_GENERAL_SELECT');
        foreach($dataLanguage as $key => $value) {
            $data[$value->CodigoIdioma] = $value->NombreIdioma;
        }
        return $data;
    }

    public function GetLabelByLanguage($languageTable){
        $translator = Zend_Registry::get('Zend_Translate');
        $db = Zend_Registry::get('db');
        $language = $this->GetLanguage();
        $dataLanguage = $db->fetchAll("SELECT
              ValorIdioma FROM catidiomaidioma
             WHERE CodigoIdiomaDestino = '".$language."' AND CodigoIdiomaOrigen = '". $languageTable ."'" );

        return $dataLanguage[0]->ValorIdioma;
    }
 
}
?>
