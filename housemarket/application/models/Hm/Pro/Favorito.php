<?php

class Hm_Pro_Favorito extends Zend_Db_Table {
	
	
	/**
     * Nombre de la tabla de propiedad con la cual se asocia
     * @var String
     */
    protected $_name = 'profavorito';
    /**
     * Llave primaria de la tabla de propiedad
     * @var String
     */
    protected $_primary = 'CodigoFavorito';
    /**
     * Adaptador para la base de datos
     * @var
     */
    public $dbAdapter;
    protected $_dependentTables = array(
        'Hm_Cli_Cliente',
        'Hm_Pro_Propiedad',
    );
	
    public $CodigoFavorito = null;
    public $CodigoPropiedad = null;
    public $CodigoCliente = null;
    
    function __construct($CodigoCliente = null, $CodigoPropiedad = null) {
    	$this->dbAdapter = Zend_Registry::get('db');
    	if($CodigoCliente == null){
    		$this->CodigoCliente = Zend_Registry::get('uid');
    	}else{
    		$this->CodigoCliente = $CodigoCliente;
    	}
    	if($CodigoPropiedad != null){
    		$this->CodigoPropiedad = $CodigoPropiedad;
    		if($this->CodigoCliente != null){
    			$params = array($this->CodigoPropiedad,$this->CodigoCliente);
    			$sql = 'SELECT fa.CodigoFavorito ' .
    					'FROM profavorito fa ' .
    					' WHERE fa.CodigoPropiedad = ? AND fa.CodigoCliente = ?';
    			$rs = $this->dbAdapter->query($sql, $params);
    			$result = $rs->fetchAll();
    			if(isset($result[0])){
    				if(isset($result[0]->CodigoFavorito)){
    					$this->CodigoFavorito = $result[0]->CodigoFavorito;
    				}
    			}
    		}
    	}
    	parent::__construct();
    }
    
    public function getlistByUid(){
    	$this->dbAdapter = Zend_Registry::get('db');
    	$params = array($this->CodigoCliente);
    	$sql = 'SELECT fa.CodigoFavorito, fa.CodigoPropiedad ' .
    			'FROM profavorito fa ' .
    			' WHERE fa.CodigoCliente = ?';
    	$rs = $this->dbAdapter->query($sql, $params);
    	$result = $rs->fetchAll();
    	return $result;
    }
    
    public function save() {
    	if($this->CodigoFavorito != null){
    		//update
    		$params = array($this->CodigoPropiedad,$this->CodigoCliente, $this->CodigoFavorito);
    		$sql = 'UPDATE profavorito ' .
    				'SET CodigoPropiedad = ?, CodigoCliente = ?' .
    				'WHERE CodigoFavorito = ?';
    		$this->dbAdapter->query($sql, $params);
    	}else{
    		//insert
    		$params = array($this->CodigoPropiedad,$this->CodigoCliente);
    		$sql = 'INSERT INTO profavorito ' .
    				'(CodigoPropiedad, CodigoCliente)' .
    				'VALUES (? , ?)';
    		$this->dbAdapter->query($sql, $params);
    	}
    }
    
    public function delete() {
    	if($this->CodigoFavorito != null){
    		//delete con codigo
    		$params = array($this->CodigoFavorito);
    		$sql = 'DELETE FROM profavorito ' .
    				'WHERE CodigoFavorito = ?';
    		$this->dbAdapter->query($sql, $params);
    	}else{
    		//delet con las otras informacion
    		if(isset($this->CodigoPropiedad) && isset($this->CodigoCliente)){
	    		$params = array($this->CodigoPropiedad,$this->CodigoCliente);
	    		$sql = 'DELETE FROM profavorito ' .
	    				'WHERE CodigoPropiedad = ? and CodigoCliente = ?';
	    		//$this->dbAdapter->query($sql, $params);
    		}
    	}
    }
    
    public function isfavorit() {
    	if($this->CodigoFavorito != null){
    		return true;
    	}
    	return false;
    }
    
    public function compartirFavorito(){
    	$params = array($this->CodigoCliente);
    	$sql = 'UPDATE cliente ' .
    			'SET ComparteFavorito = 1 ' .
    			'WHERE Uid = ?';
    	$this->dbAdapter->query($sql, $params);
    }
    
    public function cerrarcompartirFavorito(){
    	$params = array($this->CodigoCliente);
    	$sql = 'UPDATE cliente ' .
    			'SET ComparteFavorito = 0 ' .
    			'WHERE Uid = ?';
    	$this->dbAdapter->query($sql, $params);
    }
    
    public function comparteFavorito(){
    	$params = array($this->CodigoCliente);
    	$sql = 'SELECT ComparteFavorito ' .
    			'FROM cliente ' .
    			'WHERE ComparteFavorito = true AND Uid = ?';
    	$rs = $this->dbAdapter->query($sql, $params);
    	$result = $rs->fetchAll();
    	if (count($result) > 0) {
    		return true;
    	}
    	return false;
    }
    
    public function filtroUserPermitFavorito(&$list){
    	$this->dbAdapter = Zend_Registry::get('db');
    	if(empty($list)) {
    		return null;
    	}
    	foreach ($list as $user){
    		$listUid[] = $user['uid'];
    	}
    	$uids = implode(', ',$listUid);
    	if(empty($uids)) {
    		return null;
    	}
    	$sql = 'SELECT Uid ' .
    			'FROM cliente ' .
    			'WHERE ComparteFavorito = true  AND Uid in ('. $uids .') ';
    	$rs = $this->dbAdapter->query($sql);
    	$result = $rs->fetchAll();
    	foreach ($list as $key => $user) {
    		$gardar = false;
    		foreach ($result as $res){
    			if($user['uid'] == $res->Uid){
    				$gardar = true;
    				break;
    			}
    		}
    		if(!$gardar ){
    			unset($list[$key]);
    		}
    	}
    }
    
    public function filtroUserPropiedadFavorito(&$list){
    	if(empty($list)) {
    		return null;
    	}
    	foreach ($list as $key => $user){
    		$list[$key]['propiedadfavorita'] = array();
    		$listUid[] = $user['uid'];
    	}
    	$uids = implode(',',$listUid);
    	if(empty($uids)) {
    		return null;
    	}
    	$sql = 'SELECT fa.CodigoCliente, fa.CodigoPropiedad ' .
    			'FROM profavorito fa ' .
    			' WHERE fa.CodigoCliente in ('. $uids .')';
    	$rs = $this->dbAdapter->query($sql);
    	$result = $rs->fetchAll();
    	
    	foreach ($result as $prop){
    		foreach ($list as $key => $user){
    			if($prop->CodigoCliente == $user['uid']){
    				$list[$key]['propiedadfavorita'][] = $prop->CodigoPropiedad; 
    				break;
    			}
    		}
    	}
    	
    }
}
?>