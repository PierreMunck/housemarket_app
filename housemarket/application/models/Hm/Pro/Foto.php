<?php

/**
 * Clase que se encarga de crear los registros
 * de fotos para un album en particular.
 */
class Hm_Pro_Foto extends Zend_Db_Table {

    /**
     * Nombre de la tabla asociada a las fotos
     * @var String
     */
    protected $_name = 'profoto';

    /**
     * Llave primaria de la tabla de las fotos
     * @var String
     */
    protected $_primary = 'PROFotoID';

    /**
     * Interface para consultar la base de datos
     * @var Zend_DbAdapter
     */
    public $dbAdapter;

    protected $_referenceMap    = array(
            'Hm_Pro_Propiedad' => array(
                            'columns'           => 'CodigoPropiedad',
                            'refTableClass'     => 'Hm_Pro_Propiedad',
                            'refColumns'        => 'CodigoPropiedad'
            )
    );

    public static $_PHOTO_TYPES = array(
        "Principal" => 1,
        "Destacada" => 2,
        "Resultado" => 3,
        "Galeria" => 4
    );

    /**
     * Validos tipos mime reconocidos como fotos
     */
    private $_VALIDS_PHOTO_MIME_TYPES = array(
            "image/gif",
            "image/jpeg",
            "image/png"
    );

    /**
     * Tamano maximo de la foto
     */
    const MAX_PHOTO_SIZE = 8405000; // En bytes, aproximadamente 8MB

    /**
     * Constante con el separador entre nombre de archivo y su extension
     */
    const PHOTO_NAME_DOT_SEPARATOR = ".";

    const WEB_DIRECTORY_SEPARATOR = "/";

    /**
     * Retorna una imagen como string
     * @param array $methods metodos que se aplicaran a la imagen
     * @param FILE $sourceImage Imagen a ser procesada
     * @return String Imagen procesada para guardar en bbdd
     */
    public static function ProcessThumbnail($methods, $sourceImage, $isDataStream = false, $sendHeaders = true) {
        require_once 'ThumbLib.inc.php';
        $arrReturn = array();
        $thumb = Hm_Pro_PhpThumbFactory::create($sourceImage, array('jpegQuality' => 80), $isDataStream);        
        $thumb->setFormat("JPG");
        foreach($methods as $func => $pam ) {
            $function = $methods["methodname"];
            list ($ancho, $alto) = $methods["params"];
            $thumb->$function($ancho, $alto);
        }
        try {
            $arrReturn["ImageString"] = base64_encode($thumb->getImageAsString($sendHeaders));
            $arrReturn["WorkingImage"] = $thumb->getWorkingImage();
            $arrReturn["Dimensions"] = $thumb->getCurrentDimensions();
            $arrReturn["Mime"] = $thumb->getMime();
        } catch (Exception $e) {
            $err = $e->getMessage();
            //var_dump($err);
        }
        
        return $arrReturn;
    }

    /**
     * Recupera la extension del archivo (foto)
     * @param FILE $filename Referencia al archivo a subirse
     * @return String Extension del archivo
     */
    private static function GetExtension($file) {
        if(is_array($file)) {
            $filename = strtolower($file["name"]);
            $exts = split("[/\\.]", $filename);
            $n = count($exts)-1;
            $exts = $exts[$n];
            return $exts;
        } else {
            return null;
        }
    }

    /**
     * Recupera el mensaje de error personalizado a mostrar al usuario cuando
     * falle la subida del archivo de una foto.
     * @param Integer $error_code Codigo del error
     * @return String Mensaje de error personalizado para el usuario sobre el error
     * ocurrido cuando se intentaba subir el archivo de la foto.
     */
    private static function FileUploadErrorMessage($error_code) {
        $translator = Zend_Registry::get('Zend_Translate');
        switch ($error_code) {
            case UPLOAD_ERR_INI_SIZE:
                return $translator->getAdapter()->translate('ERROR_FILEUPLOAD_PHOTO_INI_SIZE');
            case UPLOAD_ERR_FORM_SIZE:
                return $translator->getAdapter()->translate('ERROR_FILEUPLOAD_PHOTO_FORM_SIZE');
            case UPLOAD_ERR_PARTIAL:
                return $translator->getAdapter()->translate('ERROR_FILEUPLOAD_PHOTO_PARTIAL');
            case UPLOAD_ERR_NO_FILE:
                return $translator->getAdapter()->translate('ERROR_FILEUPLOAD_PHOTO_NO_FILE');
            case UPLOAD_ERR_NO_TMP_DIR:
                return $translator->getAdapter()->translate('ERROR_FILEUPLOAD_PHOTO_NO_TMP_DIR');
            case UPLOAD_ERR_CANT_WRITE:
                return $translator->getAdapter()->translate('ERROR_FILEUPLOAD_PHOTO_NO_CANT_WRITE');
            case UPLOAD_ERR_EXTENSION:
                return $translator->getAdapter()->translate('ERROR_FILEUPLOAD_PHOTO_EXTENSION');
            default:
                return $translator->getAdapter()->translate('ERROR_FILEUPLOAD_PHOTO_UNKNOW');
        }
    }


    /**
     * Procesa la foto. Guarda los datos del registro de la foto
     * @param Integer $estateId Id de la propiedad a la que pertenece la foto
     * @param Integer $tipo Si es destacada o secundaria
     * @param String $directory Directorio en el cual crear la foto.
     * @param String $key Identificador del archivo al subirse
     * @param FILE $photo Referencia al archivo de foto a subir
     */
    private static function ProcessPhoto(&$estateId, &$tipo, &$key, &$photo) {
        $status = array("fails"=>false);
        $block = array();

        // Create Photo in Database
        $dbPhoto = new Hm_Pro_Foto();
        $data = array();
        $data["Comentario"] = $photo["caption"]; // Caption de la foto
        $isNew = false;

        if(stristr($key, "New") !== false) {
            $isNew = true; // La foto es nueva
        }
        
        try {
            if($isNew and $photo["photo"]["error"] == UPLOAD_ERR_OK) { // Crear nuevo registro de la foto
                $data["FechaCreacion"] = new Zend_Db_Expr('CURDATE()'); // Fecha de creacion
                $data["CodigoPropiedad"] = $estateId;
                $dbPhoto->insert($data); // Crear registro
                $photoId = $dbPhoto->getAdapter()->lastInsertId(); // Recuperar id de la foto recien creada
            } else { // Actualizar informacion de la foto
                $data["FechaModifica"] = new Zend_Db_Expr('CURDATE()'); // Fecha de modificacion
                $photoId = intval(str_replace("fprinc", "", str_replace("fotra", "", $key))); // Recuperar el id de la foto a actualizar
                $dbPhoto->update($data, $dbPhoto->getAdapter()->quoteInto("PROFotoID=?", $photoId)); // Actualizar registro
            }
        } catch(Exception $ex) {
            $block[] = "Couldn't update featured photo reference in database (general info)";
        }

        // Recuperar el nombre anterior en caso de que se vaya a actualizar una foto anterior.
        /*$oldPhoto = null;
        if(!$isNew) {
            // Recuperar la foto anterior.
            $rs = $dbPhoto->find($photoId);
            if($rs->count() > 0) {
                $oldPhoto = $rs->current()->src;
            }
        }*/

        if($photo["photo"]["error"] == UPLOAD_ERR_OK){
            $data = array();
            $sourceImage = $photo["photo"]["tmp_name"];

            /* Se utilizan siempre estas dos resoluciones */
            $methodsLarge = array("methodname"=> "resize",
                        "params" =>array(429, 321));
            $methodsGallery = array("methodname"=> "adaptiveResize",
                        "params" =>array(80, 60));

            // Recuperar para actualizar
            //Procesamiento de imagen
            if($tipo == "principal") {
                $arrData = self::ProcessThumbnail($methodsLarge, $sourceImage);
                $data["FotoGrande"] = $arrData["ImageString"];
                $dimensions = $arrData["Dimensions"];
                $data["AnchoFotoGrande"] = $dimensions["width"];
                $data["AltoFotoGrande"] = $dimensions["height"];
                $data["Mime"] = $arrData["Mime"];

                $arrData = self::ProcessThumbnail($methodsGallery, $sourceImage);
                $data["FotoGaleria"] = $arrData["ImageString"];


                $methodsFeature = array("methodname"=> "adaptiveResize",
                        "params" =>array(120, 90));
                $arrData = self::ProcessThumbnail($methodsFeature, $sourceImage);
                $data["FotoDestacada"] = $arrData["ImageString"];


                $methodsResults = array("methodname"=> "adaptiveResize",
                        "params" =>array(100, 75));
                $arrData = self::ProcessThumbnail($methodsResults, $sourceImage);
                $data["FotoResultado"] = $arrData["ImageString"];

                $data["Principal"] = 1;
            } else {

                $arrData = self::ProcessThumbnail($methodsLarge, $sourceImage);
                $data["FotoGrande"] = $arrData["ImageString"];
                $dimensions = $arrData["Dimensions"];
                $data["AnchoFotoGrande"] = $dimensions["width"];
                $data["AltoFotoGrande"] = $dimensions["height"];
                $data["Mime"] = $arrData["Mime"];
                $data["Principal"] = false;


                $arrData = self::ProcessThumbnail($methodsGallery, $sourceImage);
                $data["FotoGaleria"] = $arrData["ImageString"];
                
                $methodsFeature = array("methodname"=> "adaptiveResize",
                        "params" =>array(120, 90));
                $arrData = self::ProcessThumbnail($methodsFeature, $sourceImage);
                $data["FotoDestacada"] = $arrData["ImageString"];


                $methodsResults = array("methodname"=> "adaptiveResize",
                        "params" =>array(100, 75));
                $arrData = self::ProcessThumbnail($methodsResults, $sourceImage);
                $data["FotoResultado"] = $arrData["ImageString"];
            }



            try {
                $dbPhoto->update($data, $dbPhoto->getAdapter()->quoteInto('PROFotoID = ?', $photoId));
            } catch(Exception $ex) {
                $block[] = "Couldn't update featured photo reference in database (physical filename)";
            }
        }
        if(count($block) > 0) {
            $status = array("fails"=>true, "reasons"=>$block);
        }
        return $status;
    }

    /**
     * Procesa el directorio en particular.
     * Este directorio generalmente corresponde a un subdirectorio del album sobre
     * el cual se esta trabajando
     * @param Integer $estateId Id de la propiedad a la que pertenece la foto
     * @param Integer $albumId Id del album al que pertenece la foto
     * @param String $tipo tipo foto a procesar
     * @param FILE $photos Referencia al archivo de foto a subir
     */
    private static function ProcessPhotos(&$estateId, &$tipo, &$photos) {
        // Recuperar las fotos para el tipo en particular
        $photos_in = $photos["photos"][$tipo];
        $status = array("fails"=>false);
        $block = array();
        if(is_array($photos_in)) { // Si hay fotos.
            foreach($photos_in as $key=>$photo) { // Por cada foto.
                if($photo["photo"]["error"] == UPLOAD_ERR_OK || $photo["photo"]["error"] == 4) { // Si estan correctamente subidas.
                    $result = self::ProcessPhoto($estateId, $tipo, $key, $photo);
                    if(is_array($result) && $result["fails"] === true) {
                        $block[] = $result["reasons"];
                    }
                } else {
                    if($photo["photo"]["error"] != UPLOAD_ERR_NO_FILE) {
                        $block[] = self::FileUploadErrorMessage($photo["photo"]["error"]);
                    }
                }
            }
        }
        if(count($block)>0) {
            $status = array("fails" => true, "reasons"=>$block);
        }
        return $status;
    }

    /**
     * Guarda las fotos en el album
     * Las fotos se agrupan en cuatro grupos de acuerdo a la carpeta de destino
     * @param Integer $estateId Codigo del album al cual asociar
     * @param array $photos Fotos a ser subidas al servidor
     * @return Estado del proceso de guardar o actualizar el album.
     *         fails => true si fallo el proceso, true en otro caso
     *         reasons => Razones por las cuales fallo el proceso de guardar los datos de la foto
     */
    public static function SaveOrUpdate($estateId, $photos = array()) {
        $status = array();
        $tiposFotos = array("principal", "secundarias");

        try {
            $block = array();
            foreach($tiposFotos as $tipo) { // Para cada subdirectorio
                $result = self::ProcessPhotos($estateId, $tipo, $photos);
                if(is_array($result) && $result["fails"] === true) { // Fallo algun archivo en el directorio
                    $block[] = $result["reasons"];
                }
            }
            if(count($block)>0) { // Ocurrio mas de un fallo en algun directorio
                $status = array("fails" => true, "reasons"=> $block);
            }
        } catch(Exception $ex) {

            $status = array("fails"=>true, "reasons" => array($ex->getMessage()));
        }
        return $status;
    }
}

?>
