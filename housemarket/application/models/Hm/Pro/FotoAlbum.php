<?php

define("PHOTO_ALBUM_DIRECTORY", "/propiedades/");

class Hm_Pro_FotoAlbum extends Zend_Db_Table {

    /**
     * Nombre de la tabla de albumes de fotos
     * @var String
     */
    protected $_name = 'prop_foto_album';

    /**
     * Nombre de la llave primaria
     * @var String
     */
    protected $_primary = 'aid';

    /**
     * 
     * @var Zend_DbAdapter
     */
    public $dbAdapter;

    protected $_dependentTables = array(
                                        'Hm_Pro_Foto'
                                    );

    protected $_referenceMap    = array(
        'Hm_Pro_Propiedad' => array(
            'columns'           => 'idPropiedad',
            'refTableClass'     => 'Hm_Pro_Propiedad',
            'refColumns'        => 'CodigoPropiedad'
        )
    );

    static protected $subDirectories = array("/cercanas", "/destacada", "/destacada_small", "/marker", "/normal");

    public static function GetPhysicalPath($path){
        $current_script = dirname($_SERVER['SCRIPT_NAME']);
        $current_path   = dirname($_SERVER['SCRIPT_FILENAME']);
        $adjust = explode("/", $current_script);
        $adjust = count($adjust)-2;
        $traverse = str_repeat("../", $adjust);
        $adjusted_path = sprintf("%s/%s", $current_path, $traverse);
        // return $adjusted_path;
        return $adjusted_path.$path;
    }

    public static function GetAlbum($estateId) {
        $album = new Hm_Pro_FotoAlbum();
        return $album->fetchAll(
                $album->select()
                      ->where("idpropiedad=:id")
                      ->bind(array(":id"=>$estateId))
                );
    }

    /**
     * Indica si la propiedad ya tiene un album
     * @param Integer $estateId Id de la propiedad
     * @return Boolean true si ya tiene un album, false en caso contrario
     */
    public static function HasAlbumInDB($estateId) {
        // Recuperamos un album de la base de datos
        $album = Hm_Pro_FotoAlbum::GetAlbum($estateId);
        if($album->count() > 0) {
            return true;
        }
        return false;
    }

    /**
     * Indica si la propiedad ya tiene un album
     * @param Integer $estateId Id de la propiedad
     * @return Boolean true si ya tiene un album, false en caso contrario
     */
    public static function HasAlbumDirectory($estateId) {
        // No existe la carpeta para la propiedad
        if(is_dir(Hm_Pro_FotoAlbum::GetPhysicalPath(PHOTO_ALBUM_DIRECTORY . $estateId)) == false) {
            return false;
        }

        $album = Hm_Pro_FotoAlbum::GetAlbum($estateId);
        if($album->count() > 0) {
            // Recuperamos el album
            $row = $album->current();

            // Determinamos si en la carpeta propiedad, existe una subcarpeta con nombre igual al id de la propiedad
            // con una subcarpeta con id del album.
            if(is_dir(Hm_Pro_FotoAlbum::GetPhysicalPath(PHOTO_ALBUM_DIRECTORY . $estateId."/".$row->aid))) {
                return true;
            }
        }
        return false;
    }

    public static function EnsureAlbumDirectory($estateId, $albumId) {
        // No existe la carpeta para la propiedad, crearla
        if(is_dir(Hm_Pro_FotoAlbum::GetPhysicalPath(PHOTO_ALBUM_DIRECTORY . $estateId)) == false) {
            mkdir(Hm_Pro_FotoAlbum::GetPhysicalPath(PHOTO_ALBUM_DIRECTORY . $estateId));
        }
        // No existe la carpeta para el album, crearla
        if(is_dir(Hm_Pro_FotoAlbum::GetPhysicalPath(PHOTO_ALBUM_DIRECTORY . $estateId."/".$albumId)) == false) {
            mkdir(Hm_Pro_FotoAlbum::GetPhysicalPath(PHOTO_ALBUM_DIRECTORY . $estateId."/".$albumId));
        }
        
        if(Hm_Pro_FotoAlbum::HasAlbumDirectory($estateId)) {
            // Revisar las subcarpetas que debe tener.
            foreach(Hm_Pro_FotoAlbum::$subDirectories as $subdirectory) {
                if(!is_dir(Hm_Pro_FotoAlbum::GetPhysicalPath(PHOTO_ALBUM_DIRECTORY . $estateId."/".$albumId.$subdirectory))) {
                    mkdir(Hm_Pro_FotoAlbum::GetPhysicalPath(PHOTO_ALBUM_DIRECTORY . $estateId."/".$albumId.$subdirectory));
                }
            }
        } else {
            // Crear el directory con sus respectivas carpetas.
            mkdir(Hm_Pro_FotoAlbum::GetPhysicalPath(PHOTO_ALBUM_DIRECTORY . $estateId."/".$albumId));
            // Crear el directorio con las siguientes subcarpetas
            foreach(Hm_Pro_FotoAlbum::$subDirectories as $subdirectory) {
                mkdir(Hm_Pro_FotoAlbum::GetPhysicalPath(PHOTO_ALBUM_DIRECTORY . $estateId."/".$albumId.$subdirectory));
            }
        }
    }

    public static function GetSubDirectories() {
        return Hm_Pro_FotoAlbum::$subDirectories;
    }

    public static function GetPhotoAlbumDirectory() {
        return PHOTO_ALBUM_DIRECTORY;
    }

    /**
     *
     * @param Integer $estateId Codigo de la Propiedad a la cual se le va a crear el album
     */
    public static function CreateAlbum($estateId, $photos, $isNew = true) {
        $status = array();
        $created = false;
        if($estateId != null) {
            // Verificar si existe el registro del album en la base de datos.
            if(!Hm_Pro_FotoAlbum::HasAlbumInDB($estateId)) {
                // Crear el album
                $album = new Hm_Pro_FotoAlbum();
                $data["locacion"]= PHOTO_ALBUM_DIRECTORY . $estateId;
                $data["idpropiedad"] = $estateId;
                $data["size"] = $photos["size"];
                $data["creado"] = new Zend_Db_Expr('CURDATE()');
                $data["fbUser_id"] = Zend_Registry::get('uid');
                // La foto por defecto del album se asigna cuando se crean las fotos del album.
                //$data["img_default"] = PHOTO_ALBUM_DIRECTORY . $estateId;
                $album->insert($data);
                $albumId = $album->getAdapter()->lastInsertId();
            }
            $album = Hm_Pro_FotoAlbum::GetAlbum($estateId);
            $albumId = $album->current()->aid;

            Hm_Pro_FotoAlbum::EnsureAlbumDirectory($estateId, $albumId);

            // Crear las fotos
            $status["photos"] = Hm_Pro_Foto::SaveOrUpdate($estateId, $albumId, $photos);
        }
        $status["album_created"] = $created;
        
        return $status;
    }

}

?>