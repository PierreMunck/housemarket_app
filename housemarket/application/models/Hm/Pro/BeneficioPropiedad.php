<?php

class Hm_Pro_BeneficioPropiedad extends Zend_Db_Table {
    /**
     * Nombre de la tabla
     * @var String
     */
    protected $_name = 'probeneficiopropiedad';

    /**
     * Nombre de la llave primaria
     * @var String
     */
    protected $_primary = array('CodigoPropiedad','CodigoBeneficio');

    /**
     * Adaptador de la base de datos
     * @var Zend_DbAdapter
     */
    public $dbAdapter;

    protected $_referenceMap    = array(
        'Hm_Pro_Propiedad' => array(
            'columns'           => 'CodigoPropiedad',
            'refTableClass'     => 'Hm_Pro_Propiedad',
            'refColumns'        => 'CodigoPropiedad'
        ),
        'Hm_Cat_Beneficio' => array(
            'columns'           => 'CodigoBeneficio',
            'refTableClass'     => 'Hm_Cat_Beneficio',
            'refColumns'        => 'CodigoBeneficio'
        )
    );

    /**
     * Guarda o actualiza las propiedades.
     * @param <type> $estateId Id de la propiedad
     * @param <type> $benefits Listado de atributos
     */
    public static function SaveOrUpdate($estateId, $benefits = array()) {
        $rowsId = array();
        $fails = array();
        $beneficioPropiedad = new Hm_Pro_BeneficioPropiedad();
        if($estateId != null && is_array($benefits)) {
            // Remover los beneficios que estan deseleccionado
            $where = $beneficioPropiedad->getAdapter()->quoteInto("CodigoPropiedad=? AND CodigoBeneficio NOT IN (". implode(array_values($benefits)) .")", $estateId);
            $beneficioPropiedad->delete($where);

            // Guardar los benefits que estan seleccionados
            foreach($benefits as $id=>$benefit) {
                try {
                    $rs = $beneficioPropiedad->find($estateId, $benefit);
                    if($rs == null || $rs->count() == 0) { // Verificar que ya exista
                        $data["CodigoBeneficio"] = $benefit;
                        $data["CodigoPropiedad"] = $estateId;
                        $beneficioPropiedad->insert($data);
                        $rowsId[] = $beneficioPropiedad->getAdapter()->lastInsertId();
                    } else {
                        // No se inserta una nueva
                    }
                }catch(Exception $ex) {
                    $fails[] = array("id" => $benefit , "reason"=>$ex->getMessage());
                }
            }
        } else {
            if(!is_array($benefits)) { // Remover todos los beneficios
                $where = $beneficioPropiedad->getAdapter()->quoteInto("CodigoPropiedad=?", $estateId);
                $beneficioPropiedad->delete($where);
            }
            if($estatedId == null) {
                $fails[] = array("id" => $estateId, "reason"=>"Estate id is null");
            }
        }
        return array("success"=>$rowsId, "fails"=>$fails);
    }

}

?>
