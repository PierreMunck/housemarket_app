<?php

/**
 * @file
 * Common public static functions that may be used by the application.
 *
 * The public static functions that are critical and need to be available even when serving
 * a cached page are instead located in Bootstrap.php.
 */
class Hm_Pro_ProtertyHandling {

    /**
     * Asigna los valores de una fila a un contenedor donde se almacena la
     * informaciÃ³n de la propiedad para futuro procesamiento. TODO: Simplificar
     * la manera en que se asignan los valores. Considerar cambios a la
     * estructura de la base de datos para facilitar el proceso.
     * 
     * @param Array $contenedor_datos_propiedad Contenedor de los datos de la propiedad. Se pasa por referencia.
     * @param Integer $indice_propiedad Ã�ndice de la propiedad en procesamiento.
     * @param Array $valoresFilaPropiedad Valores leÃ­dos en una fila del archivo.
     * @param Array $encabezadosEnArchivo Encabezados en archivo leÃ­do.
     * @param Integer $totalEncabezadosEnArchivo Total de encabezados del archivo.
     * @param Array $encabezadosPermitos Encabezados permitidos segÃºn especificaciÃ³n de carga masiva.
     * @param Array $atributosPermitidosPorTipoDePropiedad Arreglo que almacena, para cada tipo de propiedad, los atributos que se le pueden asignar.
     * @param Array $encabezadosPermitidosAttributos Lista de encabezados posibles para atributos.
     * @param Array $encabezadosPermitosFotos Encabezados permitidos para fotos.
     * @param Array $urlPhotos Arreglo donde se almacenan los URL de las fotos, si las hay. Se pasa por referencia.
     * @param Array $errorMessages Almacena errores producidos en los datos de la fila. Se pasa por referencia.
     * 
     */
    public function asignarValoresEnFilaAContenedorDePropiedad(array &$contenedor_datos_propiedad, $indice_propiedad, array $valoresFilaPropiedad, array $encabezadosEnArchivo, $totalEncabezadosEnArchivo, array $encabezadosPermitos, array $atributosPermitidosPorTipoDePropiedad, array $encabezadosPermitidosAttributos, array $encabezadosPermitosFotos, array &$urlPhotos, array &$errorMessages) {

        // Asignar los valores introducidos por el usuario a objeto a guardarse en la propiedad
        for ($i = 0; $i < $totalEncabezadosEnArchivo; $i++) {

            // Si valor pertenece a un encabezado invÃ¡lido, no procesarlo
            if (!in_array($encabezadosEnArchivo[$i], $encabezadosPermitos)) {
                continue;
            }

            if ($encabezadosEnArchivo[$i] === 'PROPERTYTYPE') {
                $categoria = $this->obtenerPropiedadCategoria($valoresFilaPropiedad[$i]);

                if ($categoria) {
                    $contenedor_datos_propiedad[$encabezadosEnArchivo[$i]] = $categoria;
                } else {
                    $errorMessages['ROWS'][$indice_propiedad]['INVALID_VALUE'][] = $encabezadosEnArchivo[$i];
                }
            } elseif ($encabezadosEnArchivo[$i] === 'LOTSIZEUNIT' || $encabezadosEnArchivo[$i] === 'AREAUNIT') {
                $unidadDeMedida = $this->obtenerUnidadDeMedida($valoresFilaPropiedad[$i]);

                if ($unidadDeMedida) {
                    $contenedor_datos_propiedad[$encabezadosEnArchivo[$i]] = $unidadDeMedida;
                } else {
                    $errorMessages['ROWS'][$indice_propiedad]['INVALID_VALUE'][] = $encabezadosEnArchivo[$i];
                }
            } elseif (in_array($encabezadosEnArchivo[$i], $encabezadosPermitidosAttributos) && !empty($valoresFilaPropiedad[$i]) && isset($contenedor_datos_propiedad['PROPERTYTYPE']) && !empty($contenedor_datos_propiedad['PROPERTYTYPE'])) {
                $atributoValido = $this->validarAtributoPuedeAsignarseATipoDePropiedad($contenedor_datos_propiedad, $atributosPermitidosPorTipoDePropiedad, $encabezadosEnArchivo[$i]);

                if ($atributoValido === true) {
                    $contenedor_datos_propiedad[$encabezadosEnArchivo[$i]] = $valoresFilaPropiedad[$i];
                } else {
                    $errorMessages['ROWS'][$indice_propiedad]['ATTRIBUTE_DOES_NOT_APPLY'][] = $encabezadosEnArchivo[$i];
                }
            } elseif (in_array($encabezadosEnArchivo[$i], $encabezadosPermitosFotos) && !empty($valoresFilaPropiedad[$i])) {
                $urlValido = $this->isImagen($valoresFilaPropiedad[$i]);

                if ($urlValido) {
                    $contenedor_datos_propiedad[$encabezadosEnArchivo[$i]] = $valoresFilaPropiedad[$i];
                    $urlPhotos[$encabezadosEnArchivo[$i]] = $valoresFilaPropiedad[$i];
                } else {
                    $errorMessages['ROWS'][$indice_propiedad]['INVALID_VALUE'][] = $encabezadosEnArchivo[$i];
                }
            } else {
                $contenedor_datos_propiedad[$encabezadosEnArchivo[$i]] = $valoresFilaPropiedad[$i];
            }
        } // Fin de for ($i = 0; $i < $totalEncabezadosEnArchivo; $i++) $atributosPermitidosPorTipoDePropiedad
    }

    // Fin de funciÃ³n asignarValoresEnFilaAContenedorDePropiedad()

    /**
     * Valida que un atributo pueda asignarse a una propiedad de acuerdo a su
     * su tipo: Casa, terreno, etc.
     * 
     * @param Array $contenedor_datos_propiedad Contenedor de los datos de la propiedad. Se pasa por referencia.
     * @param Array $atributosPermitidosPorTipoDePropiedad Arreglo que almacena, para cada tipo de propiedad, los atributos que se le pueden asignar.
     * @param String $encabezadoAEvaluar Encabezado para el cual se desea determinar si puede ser asignado a al tipo de propiedad.
     */
    public function validarAtributoPuedeAsignarseATipoDePropiedad(array $contenedor_datos_propiedad, array $atributosPermitidosPorTipoDePropiedad, $encabezadoAEvaluar) {
        // Para los atributos de propiedades, mapea el valor que debe
        // almacenarce en la base de datos (los cuales estÃ¡n definidos en la
        // tabla 'catatributo') al encabezado que le corresponde en el archivo
        // de carga masiva.
        static $mapCodigoAtributoAEncabezadoArchivo;

        if (!isset($mapCodigoAtributoAEncabezadoArchivo)) {
            $mapCodigoAtributoAEncabezadoArchivo = array(
                1 => 'ROOMS', // En la base de datos se usa 1 para referirse a los cuartos. Ver tabla 'catatributo'.
                2 => 'BATHROOMS',
                3 => 'PARKINGLOTS',
                4 => 'PETSALLOWED',
                5 => 'STORIES',
                6 => 'MODEL',
                7 => 'HOA',
                8 => 'YEAR',
            );
        }

        // Almacena un arreglo cuyos valores son otro arreglo que contienen tanto el cÃ³digo del atributo en la BD como el nombre del campo donde se debe guardar en la tabla ''
        $atributosPermitidoATipoDePropiedadActual = $atributosPermitidosPorTipoDePropiedad[$contenedor_datos_propiedad['PROPERTYTYPE']];

        // Almacena Ãºnicamente el cÃ³digo de los atributos permitdos
        $codigosDeAtributosPermitidoATipoDePropiedadActual = HMUtils::build_simple_indexed_array_from_array($atributosPermitidoATipoDePropiedadActual, 'CodigoAtributo', false);

        // Almacena el encabezado que debe usarse para asignar este atributo permitido
        $encabezadosDeAtributosPermitidoATipoDePropiedadActual = array();

        foreach ($codigosDeAtributosPermitidoATipoDePropiedadActual as $codigoEncabezado) {
            $encabezadosDeAtributosPermitidoATipoDePropiedadActual[] = $mapCodigoAtributoAEncabezadoArchivo[$codigoEncabezado];
        }

        return in_array($encabezadoAEvaluar, $encabezadosDeAtributosPermitidoATipoDePropiedadActual);
    }

// Fin de funciÃ³n validarAtributoPuedeAsignarseATipoDePropiedad()
    // Fin de funciÃ³n validarAtributoPuedeAsignarseATipoDePropiedad()

    /**
     * Valida los datos en el contenedor de la propiedad.
     * 
     * @param Array $contenedor_datos_propiedad Contenedor de los datos de la propiedad.
     * @param Integer $indice_propiedad Ã�ndice de la propiedad en procesamiento.
     * @param Array $encabezadosRequeridos Encabezados requeridos.
     * @param Array $errorMessages Almacena errores producidos en los datos de la fila. Se pasa por referencia.
     */
    public function validarValoresEnContenedorDePropiedad(array $contenedor_datos_propiedad, $indice_propiedad, array $encabezadosRequeridos, array &$errorMessages) {

        // almacena los campos de deben ser tratados de tipo entero,
        // junto con cualquier restricciÃ³n que deban cumplir en case de estar presentes
        static $camposEnteros;
        if (!isset($camposEnteros)) {
            $camposEnteros = array(
                'PROPERTYTYPE' => array('valor' => array(1, 4, 6, 8, 11), 'operador' => 'in'), // valores en arreglo a como deben ingresarse en la bd. opciones vÃ¡lidas presentes en funciÃ³n obtenerPropiedadCategoria()
                'STORIES' => array('valor' => 0, 'operador' => '>'),
                'YEAR' => array('valor' => 0, 'operador' => '>'),
            );
        }

        // almacena los campos de deben ser tratados de tipo numÃ©rico,
        // junto con cualquier restricciÃ³n que deban cumplir en case de estar presentes
        static $camposNumericos;
        if (!isset($camposNumericos)) {
            $camposNumericos = array(
                'SALEPRICE' => array('valor' => 0, 'operador' => '>='),
                'RENTALPRICE' => array('valor' => 0, 'operador' => '>='),
                'AREA' => array('valor' => 0, 'operador' => '>'),
                'LOTSIZE' => array('valor' => 0, 'operador' => '>'),
                'LATITUDE' => null,
                'LONGITUDE' => null,
                'ROOMS' => array('valor' => 0, 'operador' => '>'),
                'BATHROOMS' => array('valor' => 0, 'operador' => '>'),
                'PARKINGLOTS' => array('valor' => 0, 'operador' => '>'),
                'HOA' => array('valor' => 0, 'operador' => '>'),
            );
        }

        // almacena los campos de deben ser tratados de tipo cadena,
        // junto con cualquier restricciÃ³n que deban cumplir en case de estar presentes
        static $camposCadena;
        if (!isset($camposCadena)) {
            $camposCadena = array(
                'TITLE' => null,
                'DESCRIPTION' => null,
                'AREAUNIT' => array('valor' => array('mt2', 'vr2', 'ft2'), 'operador' => 'in'), // valores en arreglo a como deben ingresarse en la bd. opciones vÃ¡lidas presentes en funciÃ³n obtenerUnidadDeMedida()
                'LOTSIZEUNIT' => array('valor' => array('mt2', 'vr2', 'ft2'), 'operador' => 'in'),
                'PROPERTYID' => null,
                'PHOTO1' => null,
                'PHOTO2' => null,
                'PHOTO3' => null,
                'PHOTO4' => null,
                'PHOTO5' => null,
                'MODEL' => null,
            );
        }

        // almacena los campos de deben ser tratados de tipo caracter,
        // junto con cualquier restricciÃ³n que deban cumplir en case de estar presentes
        static $camposCaracter;
        if (!isset($camposCaracter)) {
            $camposCaracter = array(
                'LISTINGTYPE' => array('valor' => array('S', 'R', 'B', 'F'), 'operador' => 'in'),
                'PETSALLOWED' => array('valor' => array('Y', 'N'), 'operador' => 'in'),
            );
        }

        $datoEsValido = false; // indica si se ha encontrado un error en el procesamiento de algÃºn valor
        // verificar que los campos requeridos estÃ¡n presentes y se les ha asignado un valor (SALEPRICE y RENTALPRICE requieren procesamiento especial)
        foreach ($encabezadosRequeridos as $encabezadoRequerido) {
            if (!isset($contenedor_datos_propiedad[$encabezadoRequerido])) {
                $errorMessages['ROWS'][$indice_propiedad]['MISSING_REQUIRED_FIELD'][] = $encabezadoRequerido;
            }

            if (!(($encabezadoRequerido === 'SALEPRICE') || ($encabezadoRequerido === 'RENTALPRICE')) && empty($contenedor_datos_propiedad[$encabezadoRequerido])) {
                $errorMessages['ROWS'][$indice_propiedad]['EMPTY_REQUIRED_FIELD'][] = $encabezadoRequerido;
            }
        }

        $this->validarPreciosVentaYRentaSegunTipoDePropiedad($contenedor_datos_propiedad, $indice_propiedad, $errorMessages);

        $this->validarCamposEnteros($contenedor_datos_propiedad, $indice_propiedad, $camposEnteros, $errorMessages);
        $this->validarCamposNumericos($contenedor_datos_propiedad, $indice_propiedad, $camposNumericos, $errorMessages);
        $this->validarCamposCadena($contenedor_datos_propiedad, $indice_propiedad, $camposCadena, $errorMessages);
        $this->validarCamposCaracter($contenedor_datos_propiedad, $indice_propiedad, $camposCaracter, $errorMessages);
    }

    // Fin de funciÃ³n validarValoresEnContenedorDePropiedad()

    /**
     * Intenta determinar la direcciÃ³n y paÃ­s de la propiedad si la latitud
     * y longitud estÃ¡n presentes. Esto se hace usando el API para Geocoding
     * de Google por lo que existen lÃ­mites y condiciones de su uso. Para
     * mÃ¡s informaciÃ³n al respecto visitar:
     *   http://bit.ly/bf3hOT The Google Geocoding API Usage Limits
     *   http://bit.ly/hW31y Google Maps/Google Earth APIs Terms of Service
     * 
     * @param Array $contenedor_datos_propiedad Contenedor de los datos de la propiedad.
     * @param Integer $indice_propiedad Ã�ndice de la propiedad en procesamiento.
     * @param Array $errorMessages Almacena errores producidos en los datos de la fila. Se pasa por referencia.
     */
    public function tratarDeEncontrarPais(array &$contenedor_datos_propiedad, $indice_propiedad, array &$errorMessages) {

        // Indica si se ha sobrepasado la cuota de nÃºmero de peticioes por dÃ­a permitidas.
        static $requestQuotaExceeded = false;

        if (isset($contenedor_datos_propiedad['LATITUDE']) && isset($contenedor_datos_propiedad['LONGITUDE']) && $requestQuotaExceeded === false) {
            $latitude = $contenedor_datos_propiedad['LATITUDE'];
            $longitude = $contenedor_datos_propiedad['LONGITUDE'];

            $validLatitude = HMUtils::es_numero($latitude, array('min' => -90, 'max' => 90), 'inclusive range');
            $validLongitude = HMUtils::es_numero($longitude, array('min' => -180, 'max' => 180), 'inclusive range');

            if ($validLatitude === true && $validLongitude === true) {
                // DOC: http://bit.ly/he0iZK The Google Geocoding API
                // DOC: http://bit.ly/cCnCmT Reverse Geocoding With Google Map API And PHP
                // format this string with the appropriate latitude longitude
                $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $latitude . ',' . $longitude . '&sensor=false';

                // make the HTTP request
                $data = @file_get_contents($url);

                // parse the json response
                $jsondata = json_decode($data, true);

                switch ($jsondata['status']) { // DOC: http://bit.ly/me4591 Status Codes
                    case 'OK': // indica que se pudo realizar el reverse geocode
                        $results = $jsondata['results']; // DOC: http://bit.ly/9Njrzj Results

                        $mostExactAddress = reset($results);
                        $leastExactAddress = end($results);

                        $address = $mostExactAddress['formatted_address'];
                        $countryCode = $leastExactAddress['address_components'][0]['short_name'];

                        $contenedor_datos_propiedad['Direccion'] = $address;
                        $contenedor_datos_propiedad['ZipCode'] = $countryCode;
                        break;

                    case 'OVER_QUERY_LIMIT': // indica que se ha superado el lÃ­mite de peticiones permitidas por dÃ­a
                        $requestQuotaExceeded = true;
                        $this->enviarCorreoNotificacionLimiteRevereseGeocodingExcedido();
                        break;
                } // Fin de switch ($jsondata['status'])
            } // Fin de if( $validLatitude && $validLongitude )
            else {

                // Reverse geocoding requiere ambos datos. Por tanto
                // eliminar ambos valores aunque sÃ³lo uno sea invÃ¡lido.
                unset($contenedor_datos_propiedad['LATITUDE']);
                unset($contenedor_datos_propiedad['LONGITUDE']);

                if ($validLatitude === false) {
                    $errorMessages['ROWS'][$indice_propiedad]['LATITUDE_LONGITUDE'][] = '<i18n>LISTING_UPLOAD_ERROR_INVALID_LATITUDE</i18n>';
                }
                if ($validLongitude === false) {
                    $errorMessages['ROWS'][$indice_propiedad]['LATITUDE_LONGITUDE'][] = '<i18n>LISTING_UPLOAD_ERROR_INVALID_LONGITUDE</i18n>';
                }
            }
        } // Fin de if( isset($contenedor_datos_propiedad['LATITUDE'])  && isset($contenedor_datos_propiedad['LONGITUDE']) && $requestQuotaExceeded === false )
    }

    // Fin de tratarDeEncontrarPais()

    /**
     * Valida los campos precios de venta y alquiler de acuerdo al tipo de
     * enlistamiento: Venta, Alquiler, Ambos o Remate.
     * 
     * @param Array $contenedor_datos_propiedad Contenedor de los datos de la propiedad. Se pasa por referencia.
     * @param Integer $indice_propiedad Ã�ndice de la propiedad en procesamiento.
     * @param Array $errorMessages Almacena errores producidos en los datos de la fila. Se pasa por referencia.
     */
    public function validarPreciosVentaYRentaSegunTipoDePropiedad(array &$contenedor_datos_propiedad, $indice_propiedad, array &$errorMessages) {

        switch ($contenedor_datos_propiedad['LISTINGTYPE']) {
            case 'S': // Venta | Sale
            case 'F': // Remate | Foreclosure
                if (!isset($contenedor_datos_propiedad['SALEPRICE']) || !HMUtils::es_numero($contenedor_datos_propiedad['SALEPRICE'], 0, '>')) {
                    $errorMessages['ROWS'][$indice_propiedad]['LISTING_TYPE'][] = '<i18n>LISTING_UPLOAD_ERROR_LISTING_TYPE_MISSING_VALID_SALEPRICE</i18n>';
                }
                if (isset($contenedor_datos_propiedad['RENTALPRICE']) && !empty($contenedor_datos_propiedad['RENTALPRICE'])) {
                    unset($contenedor_datos_propiedad['RENTALPRICE']);
                    $errorMessages['ROWS'][$indice_propiedad]['LISTING_TYPE'][] = '<i18n>LISTING_UPLOAD_ERROR_LISTING_TYPE_RENTALPRICE_NOT_REQUIRED</i18n>';
                }
                break;

            case 'R': // Renta | Rent
                if (!isset($contenedor_datos_propiedad['RENTALPRICE']) || !HMUtils::es_numero($contenedor_datos_propiedad['RENTALPRICE'], 0, '>')) {
                    $errorMessages['ROWS'][$indice_propiedad]['LISTING_TYPE'][] = '<i18n>LISTING_UPLOAD_ERROR_LISTING_TYPE_MISSING_VALID_RENTALPRICE</i18n>';
                }
                if (isset($contenedor_datos_propiedad['SALEPRICE']) && !empty($contenedor_datos_propiedad['SALEPRICE'])) {
                    unset($contenedor_datos_propiedad['SALEPRICE']);
                    $errorMessages['ROWS'][$indice_propiedad]['LISTING_TYPE'][] = '<i18n>LISTING_UPLOAD_ERROR_LISTING_TYPE_SALEPRICE_NOT_REQUIRED</i18n>';
                }
                break;

            case 'B': // Ambos | Both
                if (!isset($contenedor_datos_propiedad['RENTALPRICE']) || !HMUtils::es_numero($contenedor_datos_propiedad['RENTALPRICE'], 0, '>')) {
                    $errorMessages['ROWS'][$indice_propiedad]['LISTING_TYPE'][] = '<i18n>LISTING_UPLOAD_ERROR_LISTING_TYPE_MISSING_VALID_RENTALPRICE</i18n>';
                }
                if (isset($contenedor_datos_propiedad['SALEPRICE'])) {
                    unset($contenedor_datos_propiedad['SALEPRICE']);
                    $errorMessages['ROWS'][$indice_propiedad]['LISTING_TYPE'][] = '<i18n>LISTING_UPLOAD_ERROR_LISTING_TYPE_MISSING_VALID_SALEPRICE</i18n>';
                }
                break;
        }
    }

    // Fin de funciÃ³n validarPreciosVentaYRentaSegunTipoDePropiedad()

    /**
     * Valida los campos precios de venta y alquiler de acuerdo al tipo de
     * enlistamiento: Venta, Alquiler, Ambos o Remate.
     * 
     * @param Array $contenedor_datos_propiedad Contenedor de los datos de la propiedad. Se pasa por referencia.
     * @param Integer $indice_propiedad Ã�ndice de la propiedad en procesamiento.
     * @param Array $errorMessages Almacena errores producidos en los datos de la fila. Se pasa por referencia.
     */
    public function validarAtributosEspecialesSegunTipoDePropiedad(array &$contenedor_datos_propiedad, $indice_propiedad, array &$errorMessages) {

        switch ($contenedor_datos_propiedad['PROPERTYTYPE']) { // valores a evaluar deben estÃ¡r a como tienen que ingresarse en la bd. opciones vÃ¡lidas presentes en funciÃ³n obtenerPropiedadCategoria()
            case 11: // Terreno | Lot
                if (isset($contenedor_datos_propiedad['AREA'])) {
                    $contenedor_datos_propiedad['AREA'] = 0;
                    $errorMessages['ROWS'][$indice_propiedad]['ATTRIBUTE_DOES_NOT_APPLY'][] = 'AREA';
                    //$errorMessages['ROWS'][$indice_propiedad]['PROPERTY_TYPE'][] = 'LISTING_UPLOAD_ERROR_ATTRIBUTE_CANNOT_BE_ASIGNED';
                }
                if (isset($contenedor_datos_propiedad['AREAUNIT'])) {
                    $contenedor_datos_propiedad['AREAUNIT'] = '';
                    $errorMessages['ROWS'][$indice_propiedad]['ATTRIBUTE_DOES_NOT_APPLY'][] = 'AREAUNIT';
                }
                break;
        }
    }

    // Fin de funciÃ³n validarAtributosEspecialesSegunTipoDePropiedad()

    /**
     * Valida los campos de tipo entero.
     * 
     * @param Array $contenedor_datos_propiedad Contenedor de los datos de la propiedad.
     * @param Integer $indice_propiedad Ã�ndice de la propiedad en procesamiento.
     * @param Array $camposEnteros Conjunto de campos de tipo entero.
     * @param Array $errorMessages Almacena errores producidos en los datos de la fila. Se pasa por referencia.
     */
    public function validarCamposEnteros(array $contenedor_datos_propiedad, $indice_propiedad, array $camposEnteros, array &$errorMessages) {

        $datoEsValido = false; // indica si se ha encontrado un error en el procesamiento de algÃºn valor
        // Validar campos enteros
        foreach ($camposEnteros as $encabezado => $condicionEspecial) {
            if (isset($contenedor_datos_propiedad[$encabezado])) {
                $datoEsValido = false;

                $contenedor_datos_propiedad[$encabezado] += 0;

                if (empty($condicionEspecial)) {
                    $datoEsValido = HMUtils::es_numero_entero($contenedor_datos_propiedad[$encabezado]);
                } else {
                    $datoEsValido = HMUtils::es_numero_entero($contenedor_datos_propiedad[$encabezado], $condicionEspecial['valor'], $condicionEspecial['operador']);
                }

                if ($datoEsValido === false) {
                    $errorMessages['ROWS'][$indice_propiedad]['INVALID_VALUE'][] = $encabezado;
                }
            } // Fin de isset
        } // Fin de foreach
    }

    // Fin de funciÃ³n validarCamposEnteros()

    /**
     * Valida los campos de tipo numÃ©rico.
     * 
     * @param Array $contenedor_datos_propiedad Contenedor de los datos de la propiedad.
     * @param Integer $indice_propiedad Ã�ndice de la propiedad en procesamiento.
     * @param Array $camposNumericos Conjunto de campos de tipo numÃ©rico.
     * @param Array $errorMessages Almacena errores producidos en los datos de la fila. Se pasa por referencia.
     */
    public function validarCamposNumericos(array $contenedor_datos_propiedad, $indice_propiedad, array $camposNumericos, array &$errorMessages) {

        $datoEsValido = false; // indica si se ha encontrado un error en el procesamiento de algÃºn valor
        // Validar campos numÃ©ricos
        foreach ($camposNumericos as $encabezado => $condicionEspecial) {
            if (isset($contenedor_datos_propiedad[$encabezado])) {
                $datoEsValido = false;

                if (empty($condicionEspecial)) {
                    $datoEsValido = HMUtils::es_numero($contenedor_datos_propiedad[$encabezado]);
                } else {
                    $datoEsValido = HMUtils::es_numero($contenedor_datos_propiedad[$encabezado], $condicionEspecial['valor'], $condicionEspecial['operador']);
                }

                if ($datoEsValido === false) {
                    $errorMessages['ROWS'][$indice_propiedad]['INVALID_VALUE'][] = $encabezado;
                }
            } // Fin de isset
        } // Fin de foreach
    }

    // Fin de funciÃ³n validarCamposNumericos()

    /**
     * Valida los campos de tipo cadena.
     * 
     * @param Array $contenedor_datos_propiedad Contenedor de los datos de la propiedad.
     * @param Integer $indice_propiedad Ã�ndice de la propiedad en procesamiento.
     * @param Array $camposCadena Conjunto de campos de tipo cadena.
     * @param Array $errorMessages Almacena errores producidos en los datos de la fila. Se pasa por referencia.
     */
    public function validarCamposCadena(array $contenedor_datos_propiedad, $indice_propiedad, array $camposCadena, array &$errorMessages) {

        $datoEsValido = false; // indica si se ha encontrado un error en el procesamiento de algÃºn valor
        // Validar campos cadena
        foreach ($camposCadena as $encabezado => $condicionEspecial) {
            if (isset($contenedor_datos_propiedad[$encabezado]) && !empty($contenedor_datos_propiedad[$encabezado])) {
                $datoEsValido = false;

                if (empty($condicionEspecial)) {
                    $datoEsValido = HMUtils::es_cadena($contenedor_datos_propiedad[$encabezado]);
                } else {
                    $datoEsValido = HMUtils::es_cadena($contenedor_datos_propiedad[$encabezado], $condicionEspecial['valor'], $condicionEspecial['operador']);
                }

                if ($datoEsValido === false) {
                    $errorMessages['ROWS'][$indice_propiedad]['INVALID_VALUE'][] = $encabezado;
                }
            } // Fin de isset
        } // Fin de foreach
    }

    // Fin de funciÃ³n validarCamposCadena()

    /**
     * Valida los campos de tipo caracter.
     * 
     * @param Array $contenedor_datos_propiedad Contenedor de los datos de la propiedad.
     * @param Integer $indice_propiedad Ã�ndice de la propiedad en procesamiento.
     * @param Array $camposCaracter Conjunto de campos de tipo caracter.
     * @param Array $errorMessages Almacena errores producidos en los datos de la fila. Se pasa por referencia.
     */
    public function validarCamposCaracter(array $contenedor_datos_propiedad, $indice_propiedad, array $camposCaracter, array &$errorMessages) {

        $datoEsValido = false; // indica si se ha encontrado un error en el procesamiento de algÃºn valor
        // Validar campos caracter
        foreach ($camposCaracter as $encabezado => $condicionEspecial) {
            if (isset($contenedor_datos_propiedad[$encabezado]) && !empty($contenedor_datos_propiedad[$encabezado])) {
                $datoEsValido = false;

                if (empty($condicionEspecial)) {
                    $datoEsValido = HMUtils::es_caracter($contenedor_datos_propiedad[$encabezado]);
                } else {
                    $datoEsValido = HMUtils::es_caracter($contenedor_datos_propiedad[$encabezado], $condicionEspecial['valor'], $condicionEspecial['operador']);
                }

                if ($datoEsValido === false) {
                    $errorMessages['ROWS'][$indice_propiedad]['INVALID_VALUE'][] = $encabezado;
                }
            } // Fin de isset
        } // Fin de foreach
    }

    // Fin de funciÃ³n validarCamposCaracter()

    /**
     * Convierte las llaves en el arreglo contenedor de las propiedades, del valor
     * utilizado como encabezado en el archivo al valor que debe usarse para
     * almacenar el registro (nombre de columna en tabla 'propropiedad').
     * 
     * @param Array $contenedor_datos_propiedad Contenedor de los datos de la propiedad. Se pasa por referencia.
     * @param Array $mapNombreColArchivoANombreColTablaPropiedad Mapa de encabezados del archivo y nombres de columnas donde se almacena la informaciÃ³n.
     */
    public function convertirLlavesEnContenedorDePropiedad(array &$contenedor_datos_propiedad, array $mapNombreColArchivoANombreColTablaPropiedad) {

        // convierta las llaves del arreglo de los encabezados
        // en archivo a nombre de las columnas en tabla propropiedad
        foreach ($contenedor_datos_propiedad as $key => $value) {
            $mapNombreColArchivoANombreColTablaPropiedad[$key];
            if (isset($mapNombreColArchivoANombreColTablaPropiedad[$key])) {
                $contenedor_datos_propiedad[$mapNombreColArchivoANombreColTablaPropiedad[$key]] = $value;
                unset($contenedor_datos_propiedad[$key]);
            }
        }
    }

    // Fin de funciÃ³n convertirLlavesEnContenedorDePropiedad()

    /**
     * Determina si todos los campos requeridos de la tabla propiedad estÃ¡n
     * presentes en el contenedor de datos de una propiedad.
     * 
     * @param Array $contenedor_datos_propiedad Contenedor de los datos de la propiedad.
     * @param Array $columnasRequeridasEnTablaPropiedad Conjunto de columnas requeridas de la tabla propiedad.
     * @param Integer $totalColumnasRequeridasEnTablaPropiedad NÃºmero de columnas requeridas de la tabla propiedad.
     * 
     * @return Boolean
     *   Verdadero si todos los campos requeridos en tabla propiedad estÃ¡n presentes,
     *   o falso en caso contrario.
     */
    public function validarCamposRequeridosEnTablaPropiedadEstanPresentes(array $contenedor_datos_propiedad, $indice_propiedad, array $columnasRequeridasEnTablaPropiedad, $totalColumnasRequeridasEnTablaPropiedad) {

        // contador de columnas requeridas presentes en contendor de propiedad
        $contadorColumnasRequeridas = 0;

        foreach ($columnasRequeridasEnTablaPropiedad as $columnaRequerida) {
            if ((array_key_exists($columnaRequerida, $contenedor_datos_propiedad)) && !empty($contenedor_datos_propiedad[$columnaRequerida])) {
                $contadorColumnasRequeridas += 1;
            }
        }

        if ($totalColumnasRequeridasEnTablaPropiedad === $contadorColumnasRequeridas) {
            return true;
        }

        return false;
    }

    // Fin de funciÃ³n validarCamposRequeridosEnTablaPropiedadEstanPresentes()

    /**
     * Toma los datos de una fila del documento y devuelve un arreglo con los
     * registros que deben ingresarse para asignar atributos a una propiedad.
     * 
     * @param Integer $idPropiedad ID de la propiedad a procesar. Campo 'CodigoPropiedad' en tabla 'propropiedad'.
     * @param Array $contenedor_datos_propiedad Contenedor de los datos de la propiedad. Se pasa por referencia.
     * @param Array $atributosPermitidosPorTipoDePropiedad Contiene la lista de atributos permitidos segÃºn el tipo de la propiedad.
     * 
     * @return Array
     *   Lista de registros a ingresar a la base de datos para asignar los
     *   atributos definidos a la propiedad.
     */
    public function asignarAtributosAPropiedad($idPropiedad, array &$contenedor_datos_propiedad, array $atributosPermitidosPorTipoDePropiedad) {

        // Para los atributos de propiedades, mapea el valor que debe
        // almacenarce en la base de datos (los cuales estÃ¡n definidos en la
        // tabla 'catatributo') al encabezado que le corresponde en el archivo
        // de carga masiva.
        static $mapCodigoAtributoAEncabezadoArchivo;

        if (!isset($mapCodigoAtributoAEncabezadoArchivo)) {
            $mapCodigoAtributoAEncabezadoArchivo = array(
                1 => 'ROOMS', // En la base de datos se usa 1 para referirse a los cuartos. Ver tabla 'catatributo'.
                2 => 'BATHROOMS',
                3 => 'PARKINGLOTS',
                4 => 'PETSALLOWED',
                5 => 'STORIES',
                6 => 'MODEL',
                7 => 'HOA',
                8 => 'YEAR',
            );
        }

        if (isset($contenedor_datos_propiedad['PETSALLOWED'])) {
            switch ($contenedor_datos_propiedad['PETSALLOWED']) {
                case 'Y': // Se guarda como 'S' en la base de datos
                    $contenedor_datos_propiedad['PETSALLOWED'] = 'S';
                    break;

                case 'N': // Se guarda como 'N' en la base de datos. No necesita procesamineto
                    break;

                default: // Dato invÃ¡lido. Eliminarlo.
                    unset($contenedor_datos_propiedad['PETSALLOWED']);
                    break;
            }
        }

        $tipoDePropiedad = $contenedor_datos_propiedad['CodigoCategoria'];
        $listaDeAtributosAAgregar = array();

        foreach ($atributosPermitidosPorTipoDePropiedad[$tipoDePropiedad] as $atributo) {
            $valorAtributo = $contenedor_datos_propiedad[$mapCodigoAtributoAEncabezadoArchivo[$atributo['CodigoAtributo']]];

            if (!empty($valorAtributo)) {
                $listaDeAtributosAAgregar[] = array(
                    'CodigoPropiedad' => $idPropiedad,
                    'CodigoAtributo' => $atributo['CodigoAtributo'],
                    $atributo['CampoDestino'] => $valorAtributo,
                );
            }
        }

        return $listaDeAtributosAAgregar;
    }

    // Fin de funciÃ³n asignarAtributosAPropiedad()

    /**
     * Retorna la unidad de medida a usar
     * 
     * @param Integer valor a como lo debe ingresar el usuario
     * @return
     *      CÃ³digo a registarse en la base de datos o false si es invalido.
     */
    public function obtenerUnidadDeMedida($llave) {

        // Mapea el valor que debe usar el usuario, para definir la unidad de
        // medida, al valor usado en la aplicaciÃ³n.
        static $mapLlaveAUnidadDeMedida;

        // This was changed after implementation.
        // Kept in case it want to be reverted.
        if (!isset($mapLlaveAUnidadDeMedida)) {
            $mapLlaveAUnidadDeMedida = array(
                'mt2' => 'mt2', // metros cuadrados
                'vr2' => 'vr2', // varas cuadradas
                'ft2' => 'ft2', // pies cuadrados
            );
        }

        if ($mapLlaveAUnidadDeMedida[$llave]) {
            return $mapLlaveAUnidadDeMedida[$llave];
        }

        return false;
    }

    // Fin de funciÃ³n obtenerUnidadDeMedida()

    /**
     * Retorna el valor en metros cuadrados equivalentes al valor del Ã¡rea
     * unidad de medida origen provistas.
     * 
     * @param Double $area Valor del Ã¡rea a procesar.
     * @param String $unidadOrigen Unidad desde la cual se desea convertir a mt2.
     * @return
     *      Equivalente del Ã¡rea provista en metros cuadrados,
     *      o falso la unidad origen es invÃ¡lida.
     */
    public function obtenerEquivalenciaAreaEnMT2($area, $unidadOrigen) {

        // Almacena el factor de equivalencia de una unidad de medida
        // en relaciÃ³n al metro cuadrado.
        static $factorDeEquivalenciaDeUnidadDeMedidaAMT2;

        if (!isset($factorDeEquivalenciaDeUnidadDeMedidaAMT2)) {
            $factorDeEquivalenciaDeUnidadDeMedidaAMT2 = array(
                'mt2' => 1, // mt2 -> mt2
                'ft2' => 0.092903, // mt2 -> ft2
                'vr2' => 0.702579, // mt2 -> vr2
            );
        }

        if (isset($factorDeEquivalenciaDeUnidadDeMedidaAMT2[$unidadOrigen])) {
            return $area * $factorDeEquivalenciaDeUnidadDeMedidaAMT2[$unidadOrigen];
        }

        return false;
    }

    // Fin de funciÃ³n obtenerEquivalenciaAreaEnMT2()

    /**
     * Retorna el codigo de la categoria de la propiedad
     * 
     * @param Integer valor a como lo debe ingresar el usuario
     * @return
     *      CÃ³digo a registarse  la en la base de datos en campo 'CodigoCategoria'
     *      o false si es invÃ¡lido.
     */
    public function obtenerPropiedadCategoria($llave) {

        // Mapea el valor que debe usar el usuario, para definir la categorÃ­a 
        // de una propiedad, al valor que debe almacenarce en la base de datos,
        // los cuales estÃ¡n definidos en la tabla 'catcategoria'
        static $mapLlaveACodigoCategoria;

        if (!isset($mapLlaveACodigoCategoria)) {
            $mapLlaveACodigoCategoria = array(
                '1' => 1, // Casa | Single Family
                '2' => 6, // Apartamento | Apartment
                '3' => 8, // Multifamiliar | Multifamily
                '4' => 11, // Terreno | Lot
                '5' => 4, // Comercial | Commercial
            );
        }

        if (isset($mapLlaveACodigoCategoria[$llave])) {
            return $mapLlaveACodigoCategoria[$llave];
        }

        return false;
    }

    // Fin de funciÃ³n obtenerPropiedadCategoria()

    /**
     * Convierte el arreglo que almacena los mensajes de error en HTML.
     * 
     * @param Array $errorMessages Arreglo que contiene los mensaje de error.
     * 
     * @return String
     *      Cadena de HTML que contiene los mensajes de error.
     */
    public function convertirArregloDeErrorAHTML(array $errorMessages) {
        $html = '';

        $html .= '<a id="upload-listing-toggle-errors-content" target="#"><i18n>LISTING_UPLOAD_CLICK_HERE_TO_SHOW_ERROR_DETAILS</i18n></a>';

        $html .= '<div id="upload-listing-errors-content">';

        if (isset($errorMessages['GENERAL'])) {
            $generalErrors = $errorMessages['GENERAL'];

            $html_general_errors = '';

            foreach ($generalErrors as $generalError) {
                $html_general_errors .= HMUtils::wrapInHTMLTag($generalError, 'p');
            }

            $html .= HMUtils::wrapInHTMLTag($html_general_errors, 'div', FALSE, 'upload-listing-general-errors', array('upload-listing-errors'));
        }

        if (isset($errorMessages['HEADERS'])) {

            $headerErrors = $errorMessages['HEADERS'];

            $html_header_error = HMUtils::wrapInHTMLTag('<i18n>LISTING_UPLOAD_HEADER_ERRORS_FOUND</i18n>', 'p');

            $html_header_error .= '<ul>';

            if (isset($headerErrors['MISSING_REQUIRED_HEADERS'])) {
                $mensajeEncabezado = '<i18n>LISTING_UPLOAD_MISSING_REQUIRED_HEADERS</i18n>';
                $htmlTag = 'em';

                $html_missing_required_headers = $this->convertirSubarregloDeErrorAHTML($headerErrors['MISSING_REQUIRED_HEADERS'], $mensajeEncabezado, $htmlTag, TRUE);

                $html_header_error .= HMUtils::wrapInHTMLTag($html_missing_required_headers, 'li');
            } // Fin de if(isset($headerErrors['MISSING_REQUIRED_HEADERS']))

            if (isset($headerErrors['INVALID_HEADERS'])) {
                $mensajeEncabezado = '<i18n>LISTING_UPLOAD_INVALID_HEADERS</i18n>';
                $htmlTag = 'em';

                $html_invalid_headers = $this->convertirSubarregloDeErrorAHTML($headerErrors['INVALID_HEADERS'], $mensajeEncabezado, $htmlTag, TRUE);

                $html_header_error .= HMUtils::wrapInHTMLTag($html_invalid_headers, 'li');
            } // Fin de if(isset($headerErrors['INVALID_HEADERS']))

            $html_header_error .= '</ul>';

            $html .= HMUtils::wrapInHTMLTag($html_header_error, 'div', FALSE, 'upload-listing-header-errors', array('upload-listing-errors'));
        } // Fin de if(isset($errorMessages['HEADERS']))

        if (isset($errorMessages['ROWS'])) {

            $rowsErrors = $errorMessages['ROWS'];

            // Types of error:
            // HEADER_COUNT_MISMATCH *
            // EMPTY_REQUIRED_FIELD *
            // MISSING_REQUIRED_FIELD *
            // INVALID_VALUE *
            // LISTING_TYPE *
            // PROPERTY_TYPE *
            // ATTRIBUTE *
            // COULD_NOT_BE_INSERTED *

            $html_row_error = HMUtils::wrapInHTMLTag('<i18n>LISTING_UPLOAD_ROW_ERRORS_FOUND</i18n>', 'p');

            $html_row_error .= '<ul>';

            foreach ($rowsErrors as $indice => $errores) {

                $html_row_error_detail = HMUtils::wrapInHTMLTag('<i18n>LISTING_UPLOAD_IN_PROPERTY_NUMBER</i18n>' . $indice . ':', 'p');

                $html_row_error_detail .= '<ul>';

                if (isset($errores['HEADER_COUNT_MISMATCH'])) {
                    $error_header_count_mismatch = HMUtils::wrapInHTMLTag('<i18n>LISTING_UPLOAD_HEADER_COUNT_MISMATCH</i18n>', 'strong');

                    $html_row_error_detail .= HMUtils::wrapInHTMLTag($error_header_count_mismatch, 'li');
                } // Fin de if(isset($errores['HEADER_COUNT_MISMATCH']))

                if (isset($errores['MISSING_REQUIRED_FIELD'])) {
                    $mensajeEncabezado = '<i18n>LISTING_UPLOAD_MISSING_REQUIRED_FIELD</i18n>';
                    $htmlTag = 'em';

                    $error_missing_required_field = $this->convertirSubarregloDeErrorAHTML($errores['MISSING_REQUIRED_FIELD'], $mensajeEncabezado, $htmlTag, TRUE);

                    $html_row_error_detail .= HMUtils::wrapInHTMLTag($error_missing_required_field, 'li');
                } // Fin de if(isset($errores['PROPERTY_TYPE']))

                if (isset($errores['EMPTY_REQUIRED_FIELD'])) {
                    $mensajeEncabezado = '<i18n>LISTING_UPLOAD_EMPTY_REQUIRED_FIELD</i18n>';
                    $htmlTag = 'em';

                    $error_empty_required_field = $this->convertirSubarregloDeErrorAHTML($errores['EMPTY_REQUIRED_FIELD'], $mensajeEncabezado, $htmlTag, TRUE);

                    $html_row_error_detail .= HMUtils::wrapInHTMLTag($error_empty_required_field, 'li');
                } // Fin de if(isset($errores['PROPERTY_TYPE']))

                if (isset($errores['INVALID_VALUE'])) {
                    $mensajeEncabezado = '<i18n>LISTING_UPLOAD_INVALID_VALUE</i18n>';
                    $htmlTag = 'em';

                    $error_invalid_value = $this->convertirSubarregloDeErrorAHTML($errores['INVALID_VALUE'], $mensajeEncabezado, $htmlTag, TRUE);

                    $html_row_error_detail .= HMUtils::wrapInHTMLTag($error_invalid_value, 'li');
                } // Fin de if(isset($errores['INVALID_VALUE']))

                if (isset($errores['LISTING_TYPE'])) {
                    $mensajeEncabezado = '<i18n>LISTING_UPLOAD_LISTING_TYPE</i18n>';
                    $htmlTag = 'em';

                    $error_listing_type = $this->convertirSubarregloDeErrorAHTML($errores['LISTING_TYPE'], $mensajeEncabezado, $htmlTag, FALSE);

                    $html_row_error_detail .= HMUtils::wrapInHTMLTag($error_listing_type, 'li');
                } // Fin de if(isset($errores['LISTING_TYPE']))

                if (isset($errores['LATITUDE_LONGITUDE'])) {
                    $error_latitude_longitude = implode(' ', $errores['LATITUDE_LONGITUDE']);

                    $html_row_error_detail .= HMUtils::wrapInHTMLTag($error_latitude_longitude, 'li');
                } // Fin de if(isset($errores['LISTING_TYPE']))

                if (isset($errores['ATTRIBUTE_DOES_NOT_APPLY'])) {
                    $mensajeEncabezado = '<i18n>LISTING_UPLOAD_ATTRIBUTE_DOES_NOT_APPLY</i18n>';
                    $htmlTag = 'em';

                    $error_property_type = $this->convertirSubarregloDeErrorAHTML($errores['ATTRIBUTE_DOES_NOT_APPLY'], $mensajeEncabezado, $htmlTag, TRUE);

                    $html_row_error_detail .= HMUtils::wrapInHTMLTag($error_property_type, 'li');
                } // Fin de if(isset($errores['ATTRIBUTE_DOES_NOT_APPLY']))

                if (isset($errores['COULD_NOT_BE_INSERTED'])) {
                    $error_could_not_be_inserted = HMUtils::wrapInHTMLTag('<i18n>LISTING_UPLOAD_COULD_NOT_BE_INSERTED</i18n>', 'strong');

                    $html_row_error_detail .= HMUtils::wrapInHTMLTag($error_could_not_be_inserted, 'li');
                } // Fin de if(isset($errores['COULD_NOT_BE_INSERTED']))

                $html_row_error_detail .= '</ul>';

                $html_row_error .= HMUtils::wrapInHTMLTag($html_row_error_detail, 'li');
            } // Fin de foreach ($rowsErrors as $indice => $errores)

            $html_row_error .= '</ul>';

            $html .= HMUtils::wrapInHTMLTag($html_row_error, 'div', FALSE, 'upload-listing-row-errors', array('upload-listing-errors'));
        } // Fin de if(isset($errorMessages['HEADERS']))

        $html .= '</div>'; // Fin de div.upload-listing-errors-content

        $html = HMUtils::wrapInHTMLTag($html, 'div', FALSE, 'upload-listing-errors', array('upload-listing-errors-wrapper'));

        return $html;
    }

    // Fin de funciÃ³n convertirArregloDeErrorAHTML()

    /**
     * Retorna la representaciÃ³n en HTML de una porciÃ³n especificada del arreglo de error.
     * 
     * @param Array $errores Conjunto de errores a imprimir.
     * @param String $mensajeEncabezado Mensaje que se mostrarÃ¡ como encabezado de la lista de errores.
     * @param String $htmlTag Tag de HTML que debe usarse alrededor de cada error a mostrar.
     * @param Boolean $escapeContent Indica si el contenido del error escapar caracteres especiales de HTML.
     * @return String
     *      Cadena de HTML que contiene la porciÃ³n del los mensajes de error provista.
     */
    public function convertirSubarregloDeErrorAHTML(array $errores, $mensajeEncabezado, $htmlTag, $escapeContent = FALSE) {
        $error_as_html = array();

        foreach ($errores as $error) {
            $error_as_html[] = HMUtils::wrapInHTMLTag($error, $htmlTag, $escapeContent); // Zend Framework specific tag. used for translation
        }

        return $mensajeEncabezado .= implode(', ', $error_as_html);
    }

    // Fin de funciÃ³n convertirSubarregloDeErrorAHTML()

    /**
     * EnvÃ­a una notificaciÃ³n al administrador de Housemarket cuando el lÃ­mite
     * de peticiones para hacer reverse geocoding usando la API de Google Maps
     * se ha excedido.
     */
    public function enviarCorreoNotificacionLimiteRevereseGeocodingExcedido() {
        try {
            $fb = Dgt_Fb::getInstance();
        } catch (Exception $e) {
            Zend_Debug::dump("Expiracion de la sessiÃ³n.");
            return;
        }

        $config = Zend_Registry::get('config');
        $uid = $fb->get_uid();
        $user_data = reset($this->ObtenerDatosUsuario($uid));

        $html = new Zend_View();
        $html->setScriptPath(APPLICATION_PATH . '/views/scripts/emails/');

        $email_destiny = $config['info_email']; // Housemarket info mail
        $html->UD = $uid; // user's id
        $html->name = $user_data['name']; // user's name
        $html->email = $user_data['email']; // user's email

        $body = $html->render('cargamasiva_geocode_error.phtml');
        $textbody = preg_replace("'<style[^>]*>.*</style>'siU", '', $body);
        $textbody = trim(strip_tags($textbody));

        //ConfiguraciÃ³n del servidor smtp
        $smtpServer = $config['gmail_smtpserver'];
        $username = $config['gmail_username'];
        $password = $config['gmail_password'];

        $mailConfig = array('ssl' => 'tls',
            'port' => 587,
            'auth' => 'login',
            'username' => $username,
            'password' => $password);
        try {
            $transport = new Zend_Mail_Transport_Smtp($smtpServer, $mailConfig);

            $mail = new Zend_Mail('UTF-8');
            $mail->setFrom($username, "Housemarket Notificacion");
            $mail->setReplyTo($username, "No-Reply");
            $mail->setReturnPath($username, "No-Reply");
            $mail->addTo($email_destiny, "Housemarket");
            $mail->setSubject('LÃ­mite de peticiones para Reverse Geocoding excedido');
            $mail->setBodyHtml($body);
            $mail->setBodyText($textbody);
            $status = $mail->send($transport);
        } catch (Exception $e) {
            
        }
    }

    // Fin de enviarCorreoNotificacionLimiteRevereseGeocodingExcedido

    /* obtiene datos de usuario */
    public function ObtenerDatosUsuario($uid) {
        //$config = Zend_Registry::get('config');
        //$UserValue = new FacebookRestClient($config['facebook_api_key'],$config['facebook_secret']);
    	$fbDgt = Dgt_Fb::getInstance();
        $ListValue = $fbDgt->getDataUser($uid, array('first_name', 'name', 'email', 'pic_big', 'current_location', 'pic_small'));
        return $ListValue;
    }

    /* verifica que url apunte a una imagen */

    public function isImagen($url_image) {
        $valid_url = Zend_Uri::check($url_image);
        if ($valid_url) {
            $client = new Zend_Http_Client($url_image);
            try {
                $response = $client->request();
                $headers = $response->getHeaders();
                if (isset($headers['Content-type']) && stristr($headers['Content-type'], 'image')) {
                    return true;
                } else {
                    return false;
                }
            } catch (Zend_Http_Client_Adapter_Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }

    /* gets the data from a URL */

    public function get_data($url) {
        $datos = array();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $data = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        $datos["data"] = $data;
        $datos["info"] = $info;
        return $datos;
    }

}

?>
