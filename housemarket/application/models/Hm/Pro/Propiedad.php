<?php

class Hm_Pro_Propiedad extends Zend_Db_Table {
    /**
     * Factor de conversion de varas cuadradas a metros cuadrados
     */
    const CONV_VARA_CUADRADA_A_METRO = 0.702579;

    /**
     * Factor de conversion de pies cuadrados a metros cuadrados
     */
    const CONV_PIE_CUADRADO_A_METRO = 0.092903;

    /**
     * Nombre de la tabla de propiedad con la cual se asocia
     * @var String
     */
    protected $_name = 'propropiedad';
    /**
     * Llave primaria de la tabla de propiedad
     * @var String
     */
    protected $_primary = 'CodigoPropiedad';
    /**
     * Adaptador para la base de datos
     * @var
     */
    public $dbAdapter;
    protected $_dependentTables = array(
        'Hm_Pro_Foto',
        'Hm_Pro_AtributoPropiedad',
        'Hm_Pro_BeneficioPropiedad'
    );

    /**
     * caracteristica de la propriedad
     */
    
    public $CodigoCategoria = null;
    public $CodigoPropiedad = null;
    public $CodigoBroker = null;
    public $UnidadMedida = null;
    public $AreaLote = null;
    public $Area = null;
    public $Accion = null;
    public $NombrePropiedad = null;
    public $Direccion = null;
    public $DescPropiedad = null;
    public $descPrincipal = null;
    public $UnidadMedidaLote = null;
    public $MonedaCodidoEstandar = null;
    public $nombrePais = null;
    public $ZipCode = null;
    public $Estado = null;
    public $Ciudad = null;
    public $CodigoPostal = null;
    
    
    
    
    /**
     *
     * @param Integer $IdProp Identificador de una propiedad
     * @return rowSet Un arreglo de objetos con las propiedades
     */
    public static function ObtenerDatosProPropiedad($IdProp) {
        $db = Zend_Registry::get('db');
        $translator = Zend_Registry::get('Zend_Translate');
        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
		if($IdProp == null){
			return null;
		}
        $identifier = '1 = 1';
        if(is_array($IdProp)){
        	$IdProps = implode(",", $IdProp);
        	$identifier = "P.CodigoPropiedad in ($IdProps)";
        } else{
        	$identifier = "P.CodigoPropiedad = $IdProp";
        }
        
		$sql = "SELECT P.CodigoPropiedad,P.CodigoBroker, P.NombrePropiedad,P.DescPropiedad,P.Direccion,
                P.Calle,P.Ciudad,P.Estado,P.ZipCode,P.Area,P.UnidadMedida,
                P.AreaLote,P.UnidadMedidaLote,FORMAT(P.PrecioVenta,0) AS PrecioVenta,FORMAT(P.PrecioAlquiler,0) AS PrecioAlquiler,
                P.Uid,P.Latitud,P.Longitud, P.CodigoEdoPropiedad,
                P.Accion,    
				ak.cuarto as Cuarto,
        		ei.ValorIdioma, P.PrecioVenta,P.PrecioAlquiler,
        		foto.PROFotoID as IdFoto,
				CASE P.Accion
                 WHEN 'S' THEN CONCAT( '". $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') ."', ' ' , CAST(ei.ValorIdioma AS CHAR), ' US $',  FORMAT(P.PrecioVenta,2))
                 WHEN 'R' THEN CONCAT( '". $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') ."', ' ' , CAST(ei.ValorIdioma AS CHAR), ' US $',  FORMAT(P.PrecioAlquiler,2))
                 WHEN 'F' THEN CONCAT( '". $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') ."', ' ' , CAST(ei.ValorIdioma AS CHAR), ' US $',  FORMAT(P.PrecioVenta,2))
                 WHEN 'B' THEN CONCAT( '". $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') ."', ' ' , '". $translator->getAdapter()->translate('LABEL_SALE') ."', ' US $',  FORMAT(P.PrecioVenta,2), ' ', '". $translator->getAdapter()->translate('LABEL_OR') ."', ' ', '". $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') ."', ' ', '". $translator->getAdapter()->translate('LABEL_RENT') ."', ' US $' ,  FORMAT(P.PrecioAlquiler,2))
                end as Transaccion,
                ci.ValorIdioma as NombreCategoria,
                CP.Pais,
                CM.SimboloMoneda as MonedaSimbolo, CM.NombreMoneda as MonedaNombreESP,
                CM.NombreEN as MonedaNombreENG, CM.CodigoEstandar as MonedaCodidoEstandar

                FROM propropiedad P
                LEFT OUTER JOIN catpais CP ON CP.ZipCode=P.ZipCode
                LEFT OUTER JOIN catmoneda CM ON CP.CodigoMoneda = CM.CodigoMoneda
                LEFT JOIN profoto foto on (P.CodigoPropiedad = foto.CodigoPropiedad AND foto.Principal = 1)
                INNER JOIN catcategoria C ON P.CodigoCategoria=C.CodigoCategoria
                INNER JOIN catcategoriaidioma ci ON C.CodigoCategoria = ci.CodigoCategoria
                INNER JOIN cattipoenlista e ON e.CodigoEnlista = P.Accion
                INNER JOIN catenlistaidioma ei ON e.Codigoenlista = ei.CodigoEnlista
                INNER JOIN catidioma i ON ci.CodigoIdioma = i.CodigoIdioma
				LEFT JOIN atributopropiedad ak ON (ak.CodigoPropiedad = P.CodigoPropiedad)
                WHERE $identifier
                AND ei.CodigoIdioma = '" . $language . "' AND P.CodigoEdoPropiedad <> 4 AND ci.CodigoIdioma='" . $language . "'";
        $DataProp = $db->fetchAll($sql);
                //"AND P.ZipCode IS NOT NULL AND P.ZipCode != ''");
                //DC
        return $DataProp;
    }

    /**
     * Recupera la informacion de las propiedades que tiene el cliente y que no son codigo 4
     * @param Integer $uid Id de facebook del cliente
     * @return RowObjectSet retorna un arreglo de objetos
     */
    
     public static function GetPropertyDataByUid2($uid,$currentPage) { 
        $db = Zend_Registry::get('db');   
        $page = $currentPage;
        $pg_per_page = 5;
        $limit_init = $pg_per_page *($page -1 );    
        $limit = 'LIMIT '.$limit_init.",".$pg_per_page;
        $translator = Zend_Registry::get('Zend_Translate');
        $language =  Zend_Registry::get('language');
        $min_price = $_REQUEST["minprice"];
        $max_price = $_REQUEST["maxprice"];
        $type = $_REQUEST["type"];
        $category = $_REQUEST["cbCategoria"];
        $min_area = $_REQUEST["minarea"];
        $max_area = $_REQUEST["maxarea"];
        $uni_area = $_REQUEST["area"];
        $room = $_REQUEST['room'];
        //Si filtra por categoria
        if ($category != "")
            $whereCategory = " AND P.CodigoCategoria = '" . $category . "'";
        //Si filtra por Cuartos, Esto ocurre solo si ha Elegido una Categoria que no sea LOTE
        if ($room != '' && $category != 11) {            
            $whereRoom = ' AND ak.cuarto >= ' . $room;
        }
        //Si filtra por Tipo Listing
        if ($type != "0" && $type != "")
            $whereType = " AND (P.Accion = '" . $type . "' OR P.Accion = 'B')";
        //si filtra por area validamos el maximo o el minimo
        if ($category == 11)
            $field = "P.AreaLoteMt2";
        else
            $field = "P.AreaMt2";
        //$factor = 1;
        $factor = self:: GetFactorConversionArea($uni_area);

        if (($min_area != '' && $max_area != '')) {
            //$factor = $this->GetFactorConversionArea($uni_area);
            if ($max_area < $min_area) {
                $tem_area = $max_area;
                $max_area = $min_area;
                $min_area = $tem_area;
            }
            $whereArea = " AND " . $field . " >= " . $factor * doubleval($min_area) . " AND " . $field . " <= " . $factor * doubleval($max_area);
        } else {
            if ($min_area != '')
                $whereArea = " AND " . $field . " >= " . $factor * doubleval($min_area);
            elseif ($max_area != '')
                $whereArea = " AND " . $field . " <= " . $factor * doubleval($max_area);
        }
        // Area Lote. Solo para Terreno.
        //Si filtra por precio. Esto solo puede pasar si ya ha filtrado por Tipo Listing
        if ($min_price != '' && $max_price != '') {
            //Validamos si el precio maximo es mayor al precio minimo
            if ($max_price < $min_price) {
                $tem_price = $max_price;
                $max_price = $min_price;
                $min_price = $tem_price;
            }
            if ($type == "R") {
                $wherePrice = " AND P.PrecioAlquiler >= " . doubleval($min_price) . " AND P.PrecioAlquiler <= " . doubleval($max_price);
            } elseif ($type == "S" || $type == "F") {
                $wherePrice = " AND P.PrecioVenta >= " . doubleval($min_price) . " AND P.PrecioVenta <= " . doubleval($max_price);
            }
        } else {
            if ($min_price != '') {
                if ($type == "R") {
                    $wherePrice = " AND P.PrecioAlquiler >= " . doubleval($min_price);
                } elseif ($type == "S" || $type == "F") {
                    $wherePrice = " AND P.PrecioVenta >= " . doubleval($min_price);
                }
            } elseif ($max_price != '') {
                if ($type == "R") {
                    $wherePrice = " AND P.PrecioAlquiler <= " . doubleval($max_price);
                } elseif ($type == "S" || $type == "F") {
                    $wherePrice = " AND P.PrecioVenta <= " . doubleval($max_price);
                }
            } 
        } 
    
        $sql = "SELECT SQL_CALC_FOUND_ROWS P.CodigoPropiedad as CodigoPropiedad,(select MAX(pro.PROFotoID) from profoto pro where pro.CodigoPropiedad = P.CodigoPropiedad and pro.Principal = 1) as PROFotoID,P.CodigoBroker, P.NombrePropiedad,ak.Cuarto,ak.bano,P.DescPropiedad,P.Direccion,
                                     CP.Pais,P.ZipCode,P.Area,P.UnidadMedida,
                                     coalesce(
                       (
                        select
                          sum(coalesce(cr.cantidad,0)) 
                        from  
                          crecreditopropiedad cr    
                          inner join crecredito c on (c.CodigoCredito = cr.CodigoCredito)
                        where cr.CodigoPropiedad = P.CodigoPropiedad and (cr.FechaRetira >= Current_Date() OR cr.FechaRetira is null) and c.EstadoCredito='A'
                      ), 
                      0) as creditos  ,
                                    P.AreaLote,P.UnidadMedidaLote,P.PrecioVenta,P.PrecioAlquiler,
                                    CASE P.Accion" .
                ' WHEN \'S\' THEN CONCAT( \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " " , CAST(ei.ValorIdioma AS CHAR), " US $",  FORMAT(P.PrecioVenta,2))' .
                ' WHEN \'R\' THEN CONCAT( \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " " , CAST(ei.ValorIdioma AS CHAR), " US $",  FORMAT(P.PrecioAlquiler,2))' .
                ' WHEN \'F\' THEN CONCAT( \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " " , CAST(ei.ValorIdioma AS CHAR), " US $",  FORMAT(P.PrecioVenta,2))' .
                ' WHEN \'B\' THEN CONCAT( \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " " , \'' . $translator->getAdapter()->translate('LABEL_SALE') . '\', " US $",  FORMAT(P.PrecioVenta,2), ' .
                '" ", \'' . $translator->getAdapter()->translate('LABEL_OR') . '\', " ", \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " ", \'' .
                $translator->getAdapter()->translate('LABEL_RENT') . '\', " US $" ,  FORMAT(P.PrecioAlquiler,2))' .
                "end as Transaccion, ci.ValorIdioma as NombreCategoria,
                                    P.Uid,P.Latitud,P.Longitud,
                                    P.CodigoEdoPropiedad
                                    FROM propropiedad P
                                    LEFT JOIN catpais CP ON CP.ZipCode=P.ZipCode
                                    INNER JOIN catcategoria C ON P.CodigoCategoria=C.CodigoCategoria
                                    INNER JOIN catcategoriaidioma ci ON C.CodigoCategoria = ci.CodigoCategoria
                                    INNER JOIN cattipoenlista e ON e.CodigoEnlista = P.Accion
                                    INNER JOIN catenlistaidioma ei ON e.Codigoenlista = ei.CodigoEnlista
                                    INNER JOIN catidioma i ON ci.CodigoIdioma = i.CodigoIdioma 
                                    LEFT JOIN atributopropiedad ak ON (ak.CodigoPropiedad = P.CodigoPropiedad)" .
                " WHERE P.Uid IN($uid) AND P.CodigoEdoPropiedad=2 AND ei.CodigoIdioma = '" . $language . "' AND ci.CodigoIdioma = '" . $language . "' " .
                $whereCategory . " " . $whereRoom . " " . $whereArea . " " . $whereType . " " . $wherePrice . " GROUP BY CodigoPropiedad ORDER BY creditos desc,CodigoPropiedad desc ".$limit;
        //debug($sql);
        $DataProp->rows = $db->fetchAll($sql);      
        $DataProp->results = $db->fetchOne("SELECT FOUND_ROWS()");      
        return $DataProp;
    }
    
    public static function GetPropertyDataByUid($uid) {
        $db = Zend_Registry::get('db');
              
        $translator = Zend_Registry::get('Zend_Translate');
        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $min_price = isset($_REQUEST["minprice"])? $_REQUEST["minprice"] : '';
        $max_price = isset($_REQUEST["maxprice"])? $_REQUEST["maxprice"] : '';
        $type = isset($_REQUEST["type"])? $_REQUEST["type"] : '';
        $category = isset($_REQUEST["cbCategoria"])? $_REQUEST["cbCategoria"] : '';
        $min_area = isset($_REQUEST["minarea"])? $_REQUEST["minarea"] : '';
        $max_area = isset($_REQUEST["maxarea"])? $_REQUEST["maxarea"] : '';
        $uni_area = isset($_REQUEST["area"])? $_REQUEST["area"] : '';
        $room = isset($_REQUEST['room'])? $_REQUEST['room'] : '';
        //Si filtra por categoria
        $whereCategory = '';
        if ($category != "")
            $whereCategory = " AND P.CodigoCategoria = '" . $category . "'";
        //Si filtra por Cuartos, Esto ocurre solo si ha Elegido una Categoria que no sea LOTE
        $innerRoom = '';
		$whereRoom = '';
        if ($room != '' && $category != 11) {
            $innerRoom = ' INNER JOIN atributopropiedad ak ON (ak.CodigoPropiedad = P.CodigoPropiedad)';
            $whereRoom = ' AND ak.cuarto >= ' . $room;
        }
        //Si filtra por Tipo Listing
        $whereType = '';
        if ($type != "0" && $type != "")
            $whereType = " AND (P.Accion = '" . $type . "' OR P.Accion = 'B')";
        //si filtra por area validamos el maximo o el minimo
        if ($category == 11)
            $field = "P.AreaLoteMt2";
        else
            $field = "P.AreaMt2";
        //$factor = 1;
        $factor = self:: GetFactorConversionArea($uni_area);
		$whereArea = '';
        if (($min_area != '' && $max_area != '')) {
            //$factor = $this->GetFactorConversionArea($uni_area);
            if ($max_area < $min_area) {
                $tem_area = $max_area;
                $max_area = $min_area;
                $min_area = $tem_area;
            }
            $whereArea = " AND " . $field . " >= " . $factor * doubleval($min_area) . " AND " . $field . " <= " . $factor * doubleval($max_area);
        } else {
            if ($min_area != '')
                $whereArea = " AND " . $field . " >= " . $factor * doubleval($min_area);
            elseif ($max_area != '')
                $whereArea = " AND " . $field . " <= " . $factor * doubleval($max_area);
        }
        // Area Lote. Solo para Terreno.
        //Si filtra por precio. Esto solo puede pasar si ya ha filtrado por Tipo Listing
        $wherePrice = '';
        if ($min_price != '' && $max_price != '') {
            //Validamos si el precio maximo es mayor al precio minimo
            if ($max_price < $min_price) {
                $tem_price = $max_price;
                $max_price = $min_price;
                $min_price = $tem_price;
            }
            if ($type == "R") {
                $wherePrice = " AND P.PrecioAlquiler >= " . doubleval($min_price) . " AND P.PrecioAlquiler <= " . doubleval($max_price);
            } elseif ($type == "S" || $type == "F") {
                $wherePrice = " AND P.PrecioVenta >= " . doubleval($min_price) . " AND P.PrecioVenta <= " . doubleval($max_price);
            }
        } else {
            if ($min_price != '') {
                if ($type == "R") {
                    $wherePrice = " AND P.PrecioAlquiler >= " . doubleval($min_price);
                } elseif ($type == "S" || $type == "F") {
                    $wherePrice = " AND P.PrecioVenta >= " . doubleval($min_price);
                }
            } elseif ($max_price != '') {
                if ($type == "R") {
                    $wherePrice = " AND P.PrecioAlquiler <= " . doubleval($max_price);
                } elseif ($type == "S" || $type == "F") {
                    $wherePrice = " AND P.PrecioVenta <= " . doubleval($max_price);
                }
            }
        }

        $sql = "SELECT P.CodigoPropiedad,P.CodigoBroker, P.NombrePropiedad,P.DescPropiedad,P.Direccion,
                                     CP.Pais,P.ZipCode,P.Area,P.UnidadMedida,
                                     coalesce(
                       (
                        select
                          sum(coalesce(cr.cantidad,0)) 
                        from  
                          crecreditopropiedad cr    
                          inner join crecredito c on (c.CodigoCredito = cr.CodigoCredito)
                        where cr.CodigoPropiedad = P.CodigoPropiedad and (cr.FechaRetira >= Current_Date() OR cr.FechaRetira is null) and c.EstadoCredito='A'
                      ), 
                      0) as creditos  ,
                                    P.AreaLote,P.UnidadMedidaLote,P.PrecioVenta,P.PrecioAlquiler,
                                    CASE P.Accion" .
                ' WHEN \'S\' THEN CONCAT( \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " " , CAST(ei.ValorIdioma AS CHAR), " US $",  FORMAT(P.PrecioVenta,2))' .
                ' WHEN \'R\' THEN CONCAT( \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " " , CAST(ei.ValorIdioma AS CHAR), " US $",  FORMAT(P.PrecioAlquiler,2))' .
                ' WHEN \'F\' THEN CONCAT( \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " " , CAST(ei.ValorIdioma AS CHAR), " US $",  FORMAT(P.PrecioVenta,2))' .
                ' WHEN \'B\' THEN CONCAT( \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " " , \'' . $translator->getAdapter()->translate('LABEL_SALE') . '\', " US $",  FORMAT(P.PrecioVenta,2), ' .
                '" ", \'' . $translator->getAdapter()->translate('LABEL_OR') . '\', " ", \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " ", \'' .
                $translator->getAdapter()->translate('LABEL_RENT') . '\', " US $" ,  FORMAT(P.PrecioAlquiler,2))' .
                "end as Transaccion, ci.ValorIdioma as NombreCategoria,
                                    P.Uid,P.Latitud,P.Longitud,
                                    P.CodigoEdoPropiedad
                                    FROM propropiedad P
                                    LEFT JOIN catpais CP ON CP.ZipCode=P.ZipCode
                                    INNER JOIN catcategoria C ON P.CodigoCategoria=C.CodigoCategoria
                                    INNER JOIN catcategoriaidioma ci ON C.CodigoCategoria = ci.CodigoCategoria
                                    INNER JOIN cattipoenlista e ON e.CodigoEnlista = P.Accion
                                    INNER JOIN catenlistaidioma ei ON e.Codigoenlista = ei.CodigoEnlista
                                    INNER JOIN catidioma i ON ci.CodigoIdioma = i.CodigoIdioma " .
                $innerRoom .
                " WHERE P.Uid IN($uid) AND P.CodigoEdoPropiedad=2 AND ei.CodigoIdioma = '" . $language . "' AND ci.CodigoIdioma = '" . $language . "' " .
                $whereCategory . " " . $whereRoom . " " . $whereArea . " " . $whereType . " " . $wherePrice . " GROUP BY P.CodigoPropiedad ORDER BY creditos desc";

        //debug($sql);
        $DataProp = $db->fetchAll($sql);

        return $DataProp;
    }

    /*
     * Retorna todas las propiedad con el id de la foto principal para un determinado
     * cliente a partir de su uid
     */

    public static function GetDataPropertiesWithPhoto($uid) {
        $db = Zend_Registry::get('db');
        $translator = Zend_Registry::get('Zend_Translate');
        $localLanguage = Hm_Cat_Idioma::getInstance()->GetLanguage();
        $columnName = ($localLanguage == "ES") ? " C.NombreCategoria " : " C.NombreEn ";

        $DataProp = $db->fetchAll("SELECT P.CodigoPropiedad, P.NombrePropiedad,P.DescPropiedad,P.Direccion,
                                    P.Area,P.UnidadMedida,
                                    P.AreaLote,P.UnidadMedidaLote,P.PrecioVenta,P.PrecioAlquiler,
                                    foto.PROFotoID as IdFoto,
                                    CASE P.Accion" .
                        ' WHEN \'S\' THEN CONCAT( \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " " , CAST(ei.ValorIdioma AS CHAR), " US $",  FORMAT(P.PrecioVenta,2))' .
                        ' WHEN \'R\' THEN CONCAT( \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " " , CAST(ei.ValorIdioma AS CHAR), " US $",  FORMAT(P.PrecioAlquiler,2))' .
                        ' WHEN \'F\' THEN CONCAT( \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " " , CAST(ei.ValorIdioma AS CHAR), " US $",  FORMAT(P.PrecioVenta,2))' .
                        ' WHEN \'B\' THEN CONCAT( \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " " , \'' . $translator->getAdapter()->translate('LABEL_SALE') . '\', " US $",  FORMAT(P.PrecioVenta,2), ' .
                        '" ", \'' . $translator->getAdapter()->translate('LABEL_OR') . '\', " ", \'' . $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') . '\', " ", \'' .
                        $translator->getAdapter()->translate('LABEL_RENT') . '\', " US $" ,  FORMAT(P.PrecioAlquiler,2))' .
                        "end as Transaccion, C.NombreCategoria,
                                    " . $columnName . " as CategoriaEn,P.Uid,P.Latitud,P.Longitud,
                                    P.CodigoEdoPropiedad
                                    from propropiedad P
                                    left join catpais CP on CP.ZipCode=P.ZipCode
                                    LEFT JOIN profoto foto on P.CodigoPropiedad = foto.CodigoPropiedad
                                    LEFT JOIN proatributopropiedad
                                    inner join catcategoria C on P.CodigoCategoria=C.CodigoCategoria
                                    INNER JOIN cattipoenlista e ON e.CodigoEnlista = P.Accion
                                    INNER JOIN catenlistaidioma ei ON e.Codigoenlista = ei.CodigoEnlista
                                    INNER JOIN catidioma i ON ci.CodigoIdioma = i.CodigoIdioma
                                    where P.CodigoPropiedad=$IdProp and P.CodigoEdoPropiedad<>4 AND ei.CodigoIdioma = '" . $language . "'");

        return $DataProp;
    }

    /**
     * Realiza una consulta con la base de datos sobre la tabla Propiedad
     * @param String $sql Consulta a realizar
     * @param array $params Parametros que se le pasan a la consulta a realizar
     * @return RowSet Resultado de la consulta
     */
    private function DoQuery($sql, $params = null) {
        $this->dbAdapter = Zend_Registry::get('db');

        if (isset($params)) {
            $rs = $this->dbAdapter->query($sql, $params);
        } else {
            $rs = $this->dbAdapter->query($sql);
        }
        $data = $rs->fetchAll();
        return $data;
    }

    /**
     * Recupera una propiedad por identificador
     * @param String $propId Id de la propiedad a recuperar
     * @return RowSet
     */
    public function GetPropiedad($propId) {
        $params = array($propId);
        return $this->find($params);
    }

    public static function GetDetailsLocationProperty($propId) {
        $data = $this->fetchAll(
                        $this->select(/* array("ZipCode", "Ciudad", "Latitud", "Longitud") */)
                                ->where("CodigoPropiedad", $propId));
        return $data;
    }

    /**
     * Recupera la imagen a mostrar como default
     * @param String $propId Identificador de la propiedad de la cual obtener la foto habilitada
     * @return <type>
     */
    public function GetFotoHabilitada($propId) {
        $params = array($propId);
        $sql = 'SELECT f.habilitado, f.src ' .
                'FROM Prop_Foto_Album fa' .
                'LEFT OUTER JOIN Prop_Foto f ON (fa.CodigoAlbum=f.CodigoAlbum) ' .
                'WHERE (f.Habilitado = 1) AND (fa.CodigoPropiedad = ?)';
        return $this->DoQuery($sql, $params);
    }

    /**
     * Recupera las propiedades a poner como destacadas de acuerdo a
     * los parametros de b�squeda.
     * Corresponden a las imagenes del carrusel superior.
     * @param String $city Nombre de la ciudad sobre la cual mostrar los resultados
     * @return RowSet Listado de fotos a desplegar como destacadas
     */
    public function GetFotosDestacadas($country, $city, $coordinates, $start, $limit) {
        $this->dbAdapter = Zend_Registry::get('db');

        $publicada = "Publicada";
        $estadoCredito = 1;

        $sql_select = 'SELECT ' .
                'p.CodigoPropiedad id, ' .
                'f.PROFotoID, ' .
                'coalesce(
                       (
                        select
                          sum(coalesce(cr.cantidad,0)) 
                        from  
                          crecreditopropiedad cr    
                          inner join crecredito c on (c.CodigoCredito = cr.CodigoCredito)
                        where cr.CodigoPropiedad = p.CodigoPropiedad and (cr.FechaRetira >= Current_Date() OR cr.FechaRetira is null) and c.EstadoCredito=\'A\'
                      ), 
                      0) as creditos ' .
                //'f.CodigoPropiedad FKCodigoPropiedad'. 
                ' FROM ' .
                'propropiedad p ' .
                'LEFT JOIN profoto f ON (p.CodigoPropiedad = f.CodigoPropiedad) ';

        $sql_where = array();
        $sql_where[] = " p.CodigoEdoPropiedad = 2";
        $country = null;
        if ($country != null) {
            $sql_where[] = $this->dbAdapter->quoteInto('p.ZipCode LIKE ? ', "%$country%");
        }

        $city = null;
        if ($city != null) {
            $sql_where[] = $this->dbAdapter->quoteInto('p.Ciudad LIKE ? ', "%$city%");
        }
        $sql_where[] = " (  f.Principal = 1 or f.Principal is null) ";

        if ($coordinates && is_array($coordinates)) {
            $sql_where[] = " MBRContains(GeomFromText('Polygon((" . $coordinates["minX"] . " " . $coordinates["minY"] . " " . "," . $coordinates["minX"] . " " . $coordinates["maxY"] . "," . $coordinates["maxX"] . " " . $coordinates["maxY"] . "," . $coordinates["maxX"] . " " . $coordinates["minY"] . "," . $coordinates["minX"] . " " . $coordinates["minY"] . "))' ), GeomFromWKB(Point(p.Longitud, p.Latitud))) ";
        }
        if (count($sql_where) > 0) {
            $sql_where = " WHERE " . implode(" AND ", $sql_where);
        } else {
            $sql_where = "";
        }

        $sql_order_by = "GROUP BY p.CodigoPropiedad ORDER BY creditos DESC, p.CodigoPropiedad DESC";
        $sql_limit = "LIMIT $start, $limit";
        $sql = $sql_select . " " . $sql_where . " " . $sql_order_by . " " . $sql_limit;
        //echo $sql;exit();           
        $log = Zend_Registry::get('log');
        $log->info( __FILE__ .' '. __LINE__ .' : '. $sql);
        
		
        return $this->DoQuery($sql, $parameters);
    }

    /**
     * Regresa las condiciones de filtro
     * @param String $country Codigo del pais
     * @param String $city Nombre de la ciudad
     * @param String $category Codigo de la categoria de la propiedad
     * @param String $type Tipo de busqueda a realizar (Alquiler, Renta, Ambas, Ninguna)
     * @param Double $min_price Precio minimo de la propiedad
     * @param Double $max_price Precio maximo de la propiedad
     * @param Array $parameters Parametros extras de acuerdo a la categoria de la propiedad
     * @param Integer $start Inicio del listado
     * @param Integer $limit Total de propiedades
     */
    public function PrepareWherePartStatement(
    $table_estate, $table_attribute_estate, $table_attribute_category, $table_view_attribute, $country, $city, $category, $type, $min_price, $max_price, $max_area, $min_area, $uni_area, $max_area_lote, $min_area_lote, $uni_area_lote, $parameters, $coordinates) {

        $sql_filter = array();

        // Codigo de la categoria de la propiedad
        if ($category != null) {
            $sql_filter[] = $this->dbAdapter->quoteInto(" " . $table_estate . ".CodigoCategoria = ? ", $category);
        }

        // Tipo de busqueda : Alquiler / Compra / Ambas
        if ($type != null) {
            if ($type == "F") {
                $sql_filter[] = $this->dbAdapter->quoteInto($table_estate . ".Accion = ? ", $type);
            } else {
                $sql_filter[] = $this->dbAdapter->quoteInto(" (" . $table_estate . ".Accion = ? OR " . $table_estate . ".Accion='B')", $type);
            }
        }

        // Precio minimo
        if ($min_price != null && $max_price != null) {
            if ($type != null) {
                if ($type == "R") {
                    $sql_filter[] = " " . $table_estate . ".PrecioAlquiler >= " . doubleval($min_price) . " AND " . $table_estate . ".PrecioAlquiler <= " . doubleval($max_price);
                } else if ($type == "S") {
                    $sql_filter[] = " " . $table_estate . ".PrecioVenta >= " . doubleval($min_price) . " AND " . $table_estate . ".PrecioVenta <= " . doubleval($max_price);
                } else if ($type == "F") {
                    $sql_filter[] = " " . $table_estate . ".PrecioVenta >= " . doubleval($min_price) . " AND " . $table_estate . ".PrecioVenta <= " . doubleval($max_price);
                } else {
                    $sql_filter[] = " ( (" . $table_estate . ".PrecioAlquiler >= " . doubleval($min_price) . " AND " . $table_estate . ".PrecioAlquiler <= " . doubleval($max_price) . ") OR " .
                            " (" . $table_estate . ".PrecioVenta >= " . doubleval($min_price) . " AND " . $table_estate . ".PrecioVenta <= " . doubleval($max_price) . ") ) ";
                }
            } else {
                $sql_filter[] = " ( (" . $table_estate . ".PrecioAlquiler >= " . doubleval($min_price) . " AND " . $table_estate . ".PrecioAlquiler <= " . doubleval($max_price) . ") OR " .
                        " (" . $table_estate . ".PrecioVenta >= " . doubleval($min_price) . " AND " . $table_estate . ".PrecioVenta <= " . doubleval($max_price) . ") ) ";
            }
        } else {
            if ($min_price != null) {
                if ($type != null) {
                    if ($type == "R") {
                        $sql_filter[] = " " . $table_estate . ".PrecioAlquiler >= " . doubleval($min_price);
                    } else if ($type == "S") {
                        $sql_filter[] = " " . $table_estate . ".PrecioVenta >= " . doubleval($min_price);
                    } else if ($type == "F") {
                        $sql_filter[] = " " . $table_estate . ".PrecioVenta >= " . doubleval($min_price);
                    } else {
                        $sql_filter[] = " ( (" . $table_estate . ".PrecioAlquiler >= " . doubleval($min_price) . ") OR " .
                                " (" . $table_estate . ".PrecioVenta >= " . doubleval($min_price) . ") ) ";
                    }
                } else {
                    $sql_filter[] = " ( (" . $table_estate . ".PrecioAlquiler >= " . doubleval($min_price) . ") OR " .
                            " (" . $table_estate . ".PrecioVenta >= " . doubleval($min_price) . ") ) ";
                }
            } elseif ($max_price != null) {
                if ($type != null) {
                    if ($type == "R") {
                        $sql_filter[] = " " . $table_estate . ".PrecioAlquiler <= " . doubleval($max_price);
                    } else if ($type == "S") {
                        $sql_filter[] = " " . $table_estate . ".PrecioVenta <= " . doubleval($max_price);
                    } else if ($type == "F") {
                        $sql_filter[] = " " . $table_estate . ".PrecioVenta <= " . doubleval($max_price);
                    } else {
                        $sql_filter[] = " ( (" . $table_estate . ".PrecioAlquiler <= " . doubleval($max_price) . ") OR " .
                                " (" . $table_estate . ".PrecioVenta <= " . doubleval($max_price) . ") ) ";
                    }
                } else {
                    $sql_filter[] = " ( (" . $table_estate . ".PrecioAlquiler <= " . doubleval($max_price) . ") OR " .
                            " (" . $table_estate . ".PrecioVenta <= " . doubleval($max_price) . ") ) ";
                }
            }
        }

        // Area Construida
        if ($uni_area != null) {
            $factor = $this->GetFactorConversionArea($uni_area);

            if ($max_area != null && $min_area != null) {
                $sql_filter[] = " " . $table_estate . ".AreaMt2 >= " . $factor * doubleval($min_area) . " AND " . $table_estate . ".AreaMt2 <= " . $factor * doubleval($max_area);
            } else {
                if ($max_area != null) {
                    $sql_filter[] = " " . $table_estate . ".AreaMt2 <= " . $factor * doubleval($max_area);
                } elseif ($min_area != null) {
                    $sql_filter[] = " " . $table_estate . ".AreaMt2 >= " . $factor * doubleval($min_area);
                }
            }
        }

        // Area Lote. Solo para Terreno.
        if ($uni_area_lote != null) {
            $factor = $this->GetFactorConversionArea($uni_area_lote);
            if ($max_area_lote != null && $min_area_lote != null) {
                $sql_filter[] = " " . $table_estate . ".AreaLoteMt2 >= " . $factor * doubleval($min_area_lote) . " AND " . $table_estate . ".AreaLoteMt2 <= " . $factor * doubleval($max_area_lote);
            } elseif ($max_area_lote != null) {
                $sql_filter[] = " " . $table_estate . ".AreaLoteMt2 <= " . $factor * doubleval($max_area_lote);
            } elseif ($min_area_lote != null) {
                $sql_filter[] = " " . $table_estate . ".AreaLoteMt2 >= " . $factor * doubleval($min_area_lote);
            }
        }

        // Coordenadas del mapa
        if ($coordinates != null && is_array($coordinates)) {
            $sql_filter[] = " MBRContains(" .
                    "GeomFromText('Polygon((" . $coordinates["minX"] . " " . $coordinates["minY"] . " " . "," . $coordinates["minX"] . " " . $coordinates["maxY"] . "," . $coordinates["maxX"] . " " . $coordinates["maxY"] . "," . $coordinates["maxX"] . " " . $coordinates["minY"] . "," . $coordinates["minX"] . " " . $coordinates["minY"] . "))' ), " .
                    "GeomFromWKB(Point(" . $table_estate . ".Longitud, " . $table_estate . ".Latitud))) ";
        }

        // Parametros
        $sql_filter_parameters_conditions = array();
        if (is_array($parameters)) {
            foreach ($parameters as $parameter) {
                $parameter_code = $parameter['codigo']; // Codigo del Atributo
                $parameter_min = $parameter['min']; // Valor minimo
                $parameter_max = $parameter['max']; // Valor maximo
                $parameter_value = $parameter['val']; // Valor
                if ($parameter_code == "Room" or $parameter_code == "Cuartos")
                    $parameter_code = "cuarto"; // Codigo del Atributo
                if ($parameter_code == "Bathrooms" or $parameter_code == "Baños")
                    $parameter_code = "bano";
                if ($parameter_code == "Parking" or $parameter_code == "Parqueos")
                    $parameter_code = "parqueo";
                if ($parameter_code == "Pet" or $parameter_code == "Mascota Permitida")
                    $parameter_code = "mascota";

                if (($parameter_min != null && $parameter_max != null)) {
                    $sql_filter_parameters_conditions[] = " ( " . $table_view_attribute . "." . $parameter_code . " >= " . intval($parameter_min) . " OR " . $table_view_attribute . "." . $parameter_code . " >= " . doubleval($parameter_min) . "   ) ";
                } elseif ($parameter_value != null && $parameter_value != "") {
                    $sql_filter_parameters_conditions[] = " ( " . $table_view_attribute . ".mascota = " . $this->dbAdapter->quoteInto('?', $parameter_value) . " OR " . $table_view_attribute . ".mascota = Null ) ";
                }
            }
        }
        if (count($sql_filter_parameters_conditions) > 0) {
            $sql_filter[] = implode(" AND ", $sql_filter_parameters_conditions);
        }

        return $sql_filter;
    }

    /*
     * Fotos en base a filtros aplicados
     *
     */

    public function GetFotosFiltradas(
    $category, $type, $min_price, $max_price, $max_area, $min_area, $uni_area, $max_area_lote, $min_area_lote, $uni_area_lote, $parameters, $coordinates, $start, $limit) {

        $db= $this->dbAdapter = Zend_Registry::get('db');
        $translator = Zend_Registry::get('Zend_Translate');
        $language = Hm_Cat_Idioma::getInstance()->GetLanguage();

        $sql_select = 'SELECT SQL_CALC_FOUND_ROWS' .
                ' p.CodigoPropiedad as id, ' .
                'ak.cuarto, ' .
                'ak.bano, ' .
                'p.Area, ' .
                'p.UnidadMedida, ' .
                ' (CASE WHEN CHAR_LENGTH(p.NombrePropiedad) > 19 THEN CONCAT(SUBSTR(p.NombrePropiedad,1,19),"...") ELSE p.NombrePropiedad END) as nombre, ' .
                ' p.Estado as estado, ' .
                ' p.Ciudad as ciudad, FORMAT(p.PrecioVenta,0) AS PrecioVenta, FORMAT(p.PrecioAlquiler,0) AS PrecioAlquiler, ' .
                ' f.PROFotoID as PROFotoID, ' .
                ' p.Accion, ' .
                ' CP.Pais, ' .
                ' CM.SimboloMoneda as MonedaSimbolo, CM.NombreMoneda as MonedaNombreESP, ' .
                ' CM.NombreEN as MonedaNombreENG, CM.CodigoEstandar as MonedaCodidoEstandar, ' .
                ' coalesce( ' .
                '  (    select ' .
                ' sum(coalesce(cr.cantidad,0)) ' .
                ' from ' .
                ' crecreditopropiedad cr ' .
                'left outer join crecredito c on (c.CodigoCredito = cr.CodigoCredito) ' .
                ' where cr.CodigoPropiedad = p.CodigoPropiedad and (cr.FechaRetira >= Current_Date() OR cr.FechaRetira is null) and c.EstadoCredito=\'A\'
                  ),
                  0) as creditos ' .
                ' FROM ' .
                ' propropiedad p ' .
                ' LEFT OUTER JOIN catpais CP ON CP.ZipCode=p.ZipCode ' .
                ' LEFT OUTER JOIN catmoneda CM ON CM.CodigoMoneda = CP.CodigoMoneda ' .
                ' INNER JOIN cattipoenlista e ON e.CodigoEnlista = p.Accion ' .
                ' INNER JOIN catenlistaidioma ei ON e.Codigoenlista = ei.CodigoEnlista ' .
                ' INNER JOIN catidioma i ON ei.CodigoIdioma = i.CodigoIdioma ' .
                ' LEFT OUTER JOIN catmoneda m ON (m.codigomoneda = p.codigomoneda) ' .
                ' LEFT OUTER JOIN profoto f ON (p.CodigoPropiedad = f.CodigoPropiedad) ' .
                ' LEFT OUTER JOIN proatributopropiedad ap ON (p.CodigoPropiedad = ap.CodigoPropiedad) ' .
                ' LEFT OUTER JOIN crecreditopropiedad cp ON (p.CodigoPropiedad=cp.CodigoPropiedad) ' .
                ' LEFT OUTER JOIN catatributo a ON (ap.CodigoAtributo = a.CodigoAtributo) ' .
                ' LEFT OUTER JOIN catatributocategoria ac ON (ac.CodigoAtributo=a.CodigoAtributo) 
                  INNER JOIN atributopropiedad ak ON (ak.CodigoPropiedad = p.CodigoPropiedad)';

        $sql_where = ' WHERE ';
        $sql_where_conditions = $this->PrepareWherePartStatement("p", "ap", "ac", "ak", null, null, $category, $type, $min_price, $max_price, $max_area, $min_area, $uni_area, $max_area_lote, $min_area_lote, $uni_area_lote, $parameters, $coordinates);
        $sql_where_conditions[] = " p.CodigoEdoPropiedad = 2";
        //$sql_where_conditions[] = " p.ZipCode IS NOT NULL";
        $sql_where_conditions[] = " p.ZipCode != ''";
        $sql_where_conditions[] = " (  f.Principal = 1 or f.Principal is null) ";
        $sql_where_conditions[] = " ei.CodigoIdioma = '" . $language . "'";

        if (count($sql_where_conditions) > 0) {
            $sql_where .= implode(" AND ", $sql_where_conditions);
        } else {
            $sql_where = "";
        }
        $sql_order_by = " GROUP BY p.CodigoPropiedad ORDER BY creditos DESC, id DESC";

        $sql_limit = " LIMIT " . $start . ", " . $limit;

        $sql = $sql_select . $sql_where . $sql_order_by . $sql_limit;
        
        $rs=new stdClass();
        $result = $db->fetchAll($sql);
        foreach ($result as $key => $r){
        	$result[$key] = get_object_vars($r);
        }
        $rs->rows= $result;
        $rs->results=$db->fetchOne("SELECT FOUND_ROWS()");
        return $rs;
    }

    public function GetTotalFotosFiltradas($category, $type, $min_price, $max_price, $max_area, $min_area, $uni_area, $max_area_lote, $min_area_lote, $uni_area_lote, $parameters, $coordinates) {
        $this->dbAdapter = Zend_Registry::get('db');
        $translator = Zend_Registry::get('Zend_Translate');

        $sql_select = 'SELECT DISTINCT ' .
                ' count(DISTINCT p.CodigoPropiedad) as numero ' .
                ' FROM ' .
                ' propropiedad p ' .
                ' LEFT JOIN catmoneda m ON (m.codigomoneda = p.codigomoneda) ' .
                ' LEFT JOIN profoto f ON (p.CodigoPropiedad = f.CodigoPropiedad) ' .
                ' LEFT JOIN proatributopropiedad ap ON (p.CodigoPropiedad = ap.CodigoPropiedad) ' .
                ' LEFT JOIN crecreditopropiedad cp ON (p.CodigoPropiedad=cp.CodigoPropiedad) ' .
                ' LEFT JOIN catatributo a ON (ap.CodigoAtributo = a.CodigoAtributo) ' .
                ' LEFT JOIN catatributocategoria ac ON (ac.CodigoAtributo=a.CodigoAtributo) ' .
                'INNER JOIN atributopropiedad ak ON (ak.CodigoPropiedad = p.CodigoPropiedad)';
        $sql_where = ' WHERE ';
        $sql_where_conditions = $this->PrepareWherePartStatement("p", "ap", "ac", "ak", null, null, $category, $type, $min_price, $max_price, $max_area, $min_area, $uni_area, $max_area_lote, $min_area_lote, $uni_area_lote, $parameters, $coordinates);
        $sql_where_conditions[] = " p.CodigoEdoPropiedad = 2";
        $sql_where_conditions[] = " (  f.Principal = 1 or f.Principal is null) ";

        if (count($sql_where_conditions) > 0) {
            $sql_where .= implode(" AND ", $sql_where_conditions);
        } else {
            $sql_where = "";
        }

        $sql = $sql_select . $sql_where;
        $result = $this->DoQuery($sql, null);
        foreach ($result as $key => $r){
        	$result[$key] = get_object_vars($r);
        }
        $totalFotos = $result;
        return $totalFotos[0]->numero; 
    }
    
    /**
     * Recupera las propiedades a poner como destacadas de acuerdo a
     * los parametros de b�squeda.
     * Corresponden a las imagenes del carrusel superior.
     * @param String $city Nombre de la ciudad sobre la cual mostrar los resultados
     * @return RowSet Listado de fotos a desplegar como destacadas

      //echo $sql;exit();
      //return $this->cambiaAcentosGrid($this->DoQuery($sql, null), false, true);


      /**
     * Filtro a aplicar cuando se pulsa el boton: Ubicar en el mapa
     * Debe retornar un listado de las propiedades con sus fotos destacadas.
     *
     * @param String $country Codigo del Pais
     * @param String $city Nombre de la ciudad
     * @param String $category Codigo de la categoria
     * @param String $type Tipo de propiedad ( Accion )
     * @param Double $min_price Precio minimo
     * @param Double $max_price Precio maximo
     * @param Array $extra_filters Parametros extras dependiendo de la categoria y si los proporciona
     * @param Integer $start Inicio del listado
     * @param Integer $limit Fin del listado
     * @return Array Listado de las propiedades.
     */
    public function FilterStatesOnDir(
    $country = null, $city = null, $category = null, $type = null, $min_price = null, $max_price = null, $max_area = null, $min_area = null, $uni_area = null, $max_area_lote = null, $min_area_lote = null, $uni_area_lote = null, $extra_filters = null, $coordinates = null, $start = 0, $limit = 10) {

        $this->dbAdapter = Zend_Registry::get('db');
        $sql_select = "SELECT " .
                "p.CodigoPropiedad, " .
                "ak.cuarto, " .
                "ak.bano, " .
                "FORMAT(p.Area,2) as Area, " .
                "p.UnidadMedida as UnidadMedida, " .
                "p.Accion," .
                "p.Estado, " .
                "p.Ciudad, " .
                "p.ZipCode, " .
                "FORMAT(p.PrecioVenta,0) as PrecioVenta, " .
                "FORMAT(p.PrecioAlquiler,0) as PrecioAlquiler, " .
                "COALESCE(m.SimboloMoneda,'') moneda, " .
                "p.Latitud, " .
                "p.Longitud, " .
                "f.PROFotoID, " .
                ' m.SimboloMoneda as MonedaSimbolo, m.NombreMoneda as MonedaNombreESP, ' .
                ' m.NombreEN as MonedaNombreENG, m.CodigoEstandar as MonedaCodidoEstandar, ' .
                "coalesce((select sum(coalesce(cr.cantidad,0)) from crecreditopropiedad cr left outer join crecredito c on (c.CodigoCredito = cr.CodigoCredito) where cr.CodigoPropiedad = p.CodigoPropiedad and (c.FechaVence >= Current_Date() OR c.FechaVence is null) and c.EstadoCredito='A' ),0) as credito " .
                "FROM " .
                "propropiedad p " .                
                " LEFT OUTER JOIN catpais pa on (p.ZipCode=pa.ZipCode) " .
                " LEFT OUTER JOIN catmoneda m ON (m.CodigoMoneda = pa.CodigoMoneda) " .
                " LEFT OUTER JOIN profoto f on (p.CodigoPropiedad = f.CodigoPropiedad) " .
                " INNER JOIN atributopropiedad ak ON (ak.CodigoPropiedad = p.CodigoPropiedad)
                  INNER JOIN catcategoria c ON c.CodigoCategoria=p.CodigoCategoria";

        $sql_filter = $this->PrepareWherePartStatement("p", "ap", "ac", "ak", $country, $city, $category, $type, $min_price, $max_price, $max_area, $min_area, $uni_area, $max_area_lote, $min_area_lote, $uni_area_lote, $extra_filters, $coordinates);
        $sql_filter[] = " p.CodigoEdoPropiedad = 2";
        $sql_filter[] = " ( f.Principal = 1 or f.Principal is null) ";

        if (count($sql_filter) > 0) {
            $sql_filter = " WHERE " . implode(" AND ", $sql_filter);
        } else {
            $sql_filter = "";
        }

        $sql_groupBy = " GROUP BY p.CodigoPropiedad";
        $sql_orderBy = " ORDER BY credito DESC,p.CodigoPropiedad DESC";

        $sql_limit = "";
        if($limit != NULL){
        	$sql_limit = " LIMIT ". $start . "," . $limit . " ";
        }
        $parameters = null;

        $sql = $sql_select . $sql_filter . $sql_groupBy . $sql_orderBy . $sql_limit;
        return $this->DoQuery($sql, $parameters);
    }

    public static function cambiaAcentosGrid($arr, $nombrepropiedad=false, $nombre=false) {
        for ($i = 0; $i < count($arr); $i++) {
            if ($nombrepropiedad)
                $arr[$i]->NombrePropiedad = iconv("latin1", "utf-8", $arr[$i]->NombrePropiedad);

            if ($nombre)
                $arr[$i]->nombre = iconv("latin1", "utf-8", $arr[$i]->nombre);
        }
        return $arr;
    }

    /**
     * Filtra para el google map las propiedades ubicada en la ciudad
     */
    public function FilterToGoogleMapByDir($country, $city, $start, $limit) {
        $this->dbAdapter = Zend_Registry::get('db');

        $sql_select = "SELECT " .
                "p.CodigoPropiedad, " .
                "p.NombrePropiedad, " .
                "p.DescPropiedad, " .
                "p.Direccion, " .
                "p.Estado, " .
                "p.Ciudad, " .
                "p.ZipCode, " .
                "p.PrecioVenta, " .
                "p.PrecioAlquiler, " .
                "p.FechaRegistro, " .
                "p.FechaModifica, " .
                "p.CodigoCategoria, " .
                "p.Latitud, " .
                "p.Longitud, " .
                "p.Uid, " .
                "p.Accion, " .
                "f.src_destacadas_normal src_normal, " .
                "f.src_destacadas_small src_small, " .
                "f.src_medium src, " .
                "f.src_medium_width width, " .
                "f.src_medium_height height " .
                "FROM " .
                "propropiedad p " .
                "left join prop_foto_album a on (p.codigopropiedad = a.idpropiedad) " .
                "left join prop_foto f on (a.aid = f.prop_foto_album_aid) " .
                "left outer join catpais pa on (p.ZipCode=pa.ZipCode) ";

        $sql_filter = " WHERE p.CodigoEdoPropiedad = 2 AND p.ZipCode LIKE ?";
        $sql_groupBy = " GROUP BY p.CodigoPropiedad";
        $sql_limit = " LIMIT $start, $limit";

        $parameters = null;
        $sql_filter = $this->dbAdapter->quoteInto($sql_filter, "%$country%");

        if ($city == null) {
            $sql_filter .= " AND p.ciudad LIKE ?";
            $sql_filter = $this->dbAdapter->quoteInto($sql_filter, "%$city%");
        }

        $sql = $sql_select . $sql_filter . $sql_groupBy . $sql_limit;

        return $this->cambiaAcentosGrid($this->DoQuery($sql, $parameters), true, false);
    }

    /**
     * Recupera las fotos asociadas a la propiedad
     * @param Integer $estateId Codigo de la propiedad
     * @return Array Listado de fotos de la propiedad
     */
    public function GetPhotos($estateId) {
        if ($estateId == null) {
            return array();
        }
        $_photos = array();
        $_photo = array();
        $rsEstate = $this->find($estateId);
        if ($rsEstate != null || $rsEstate->count() > 0) {
            $photos = $rsEstate->current()->findHm_Pro_Foto();
            foreach ($photos as $photo) {
                // No es la foto principal
                if ($photo->Principal === chr(0x00)) {
                    $_photo[] = $photo;
                }
            }
        }

        return $_photo;
    }

    // @todo. Implementar
    /**
     * Foto de la Burbuja en google maps.
     * @param String $idpropiedad Codigo de la propiedad al cual obtener la foto
     */
    public function GetDetailPhoto($idpropiedad) {
        $this->dbAdapter = Zend_Registry::get('db');

        $sql = "SELECT p.fid, p.caption, p.creada, p.modificada, p.src, p.src_height, p.src_width, p.src_destacadas_small, " .
                "p.src_destacadas_smal_height, p.src_destacadas_small_width, p.src_destacadas_normal, p.src_destacadas_normal_height, " .
                "p.src_destacadas_normal_width,p.src_medium,p.src_medium_height,p.src_medium_width,p.prop_foto_album_aid, " .
                "p.habilitado FROM prop_foto p left outer join prop_foto_album fa on fa.aid = p.prop_foto_album_aid " .
                "where fa.idpropiedad=" . intval($idpropiedad);

		$result = $this->DoQuery($sql);
		foreach ($result as $key => $r){
			$result[$key] = get_object_vars($r);
		}
        return $result;
    }

    /**
     * Recupera los paises registrados en las tablas de GoogleMap
     * @return RowSet Informacion geografica de los paises registrados en las tablas de GoogleMap.
     */
    public function GetCountryList() {
        $sql = 'SELECT DISTINCT(cn), cc FROM GeoLite_Country ORDER BY 1 ASC;';
        return $this->DoQuery($sql);
    }

    /**
     * Recupera el factor de conversion a aplicar de acuerdo a la unidad de medida
     * @param String $unidad_medida Unidad de medida a convertir (ft2, mt2, vr2)
     * @return Double Factor de conversion a aplicar
     */
    private function GetFactorConversionArea($unidad_medida) {
        if ($unidad_medida == "ft2") {
            return self::CONV_PIE_CUADRADO_A_METRO;
        } else if ($unidad_medida == "vr2") {
            return self::CONV_VARA_CUADRADA_A_METRO;
        } else {
            return 1;
        }
    }

    /**
     * Valida los datos de la propiedad a ser guardados.
     * @param Array $parameters Conjunto de datos de la propiedad a ser guardados
     * @return Array isValid => true si es valido, false en caso contrario
     *               errors => Conjunto de mensajes de errores a mostrar al usuario.
     */
    private function Validate($parameters = array()) {

        $isValid = true;
        $f = new Zend_Filter_StripTags();

        $errors = array();

        $new = $f->filter($parameters["isNew"], "S");
        $isNew = $new == "S" ? true : false;

        if ($isNew) {
            // Validar por codigo de categoria
            $category = $f->filter($parameters["cat"], null);
            $block = array();
            if ($category == null) {
                $block[] = "Please, choose a property type";
                $isValid = false;
            } else {
                if (!is_numeric($category)) {
                    $block[] = "Property type selected is not a valid number";
                    $isValid = false;
                }
            }
            if (count($block) > 0) {
                $errors["Property Type"] = $block;
            }
        }

        $NombrePropiedad = $f->filter($parameters["titulo"], null);
        $block = array();

        if ($NombrePropiedad == null || strlen($NombrePropiedad) == 0) {
            $block[] = "Please, enter a title";
            $isValid = false;
        } else {
            if (strlen($NombrePropiedad) > 128) {
                $block[] = "Title is too long, please enter a title equal or less than characters";
                $isValid = false;
            }
        }
        if (count($block) > 0) {
            $errors["Title"] = $block;
        }

        $DescPropiedad = $f->filter($parameters["descripcion"], null);
        $block = array();
        if ($DescPropiedad != null) {
            if (strlen($DescPropiedad) > 512) {
                $block[] = "Description is too long, please enter a title equal or less than 512 characters";
                $isValid = false;
            }
        }
        if (count($block) > 0) {
            $errors["Description"] = $block;
        }

        $estado = $f->filter($parameters["latitud"], null);
        $block = array();
        if ($estado == null || !is_numeric($estado)) {
            $block[] = "Please, enter a valid latitud";
            $isValid = false;
        }
        if (count($block) > 0) {
            $errors["Latitud"] = $block;
        }

        $longitud = $f->filter($parameters["longitud"], null);
        $block = array();
        if ($longitud == null || !is_numeric($longitud)) {
            $block[] = "Please, enter a valid longitud";
            $isValid = false;
        }
        if (count($block) > 0) {
            $errors["Longitud"] = $block;
        }

        $isForForclosure = $f->filter($parameters["remateUS"], null) == null ? false : true;
        $isForSale = $f->filter($parameters["ventaUS"], null) == null ? false : true;
        $isForRent = $f->filter($parameters["alquilerUS"], null) == null ? false : true;

        if ($isForSale) {
            $PrecioVenta = $this->validanumcoma($f->filter($parameters["ventaUS"], null));
            $block = array();
            if ($PrecioVenta == null || !is_numeric($PrecioVenta)) {
                $block[] = "Please, enter a valid sale price";
                $isValid = false;
            }
            if (count($block) > 0) {
                $errors["Sale Price"] = $block;
            }
        }

        if ($isForRent) {
            $PrecioAlquiler = $this->validanumcoma($f->filter($parameters["alquilerUS"], null));
            $block = array();
            if ($PrecioAlquiler == null || !is_numeric($PrecioAlquiler)) {
                $block[] = "Please, enter a valid rent price";
                $isValid = false;
            }
            if (count($block) > 0) {
                $errors["Rent Price"] = $block;
            }
        }

        if (!$isForForclosure) {
            if (!$isForRent && !$isForSale) {
                $errors["Listing type"] = array("Please, choose for rent, for sale or both of them");
                $isValid = false;
            }
        }

        if ($isForForclosure) {
            $forclosurePrice = $this->validanumcoma($f->filter($parameters["remateUS"], null));
            $block = array();
            if ($forclosurePrice == null || !is_numeric($forclosurePrice)) {
                $block[] = "Please, enter a valid forclosure price";
            }
        }

		if(isset($parameters["areaLote"])){
	        $areaLote = $f->filter($parameters["areaLote"], null);
	        $block = array();
	        if ($areaLote != null && strlen($areaLote) > 0) {
	            if (!is_numeric($areaLote)) {
	                $block[] = "Please, enter a valid lote area";
	                $isValid = false;
	            } else {
	                // Verificar que proporcione
	                $unidadMedidaLote = $f->filter($parameters["areaLoteUM"], null);
	                $block = array();
	                if ($unidadMedidaLote == null || strlen($unidadMedidaLote) == 0) {
	                    $block[] = "Please, enter a measurement unit for lote area";
	                    $isValid = false;
	                } else {
	                    if ($unidadMedidaLote != "mt2" && $unidadMedidaLote != "ft2" && $unidadMedidaLote != "vr2") {
	                        $block[] = "Please, measurement unit for lote area must be mt2, ft2 or vr2";
	                        $isValid = false;
	                    }
	                }
	            }
	            if (count($block) > 0) {
	                $errors["Lote Area"] = $block;
	            }
	        }
        }
		
		if(isset($parameters["area"])){
			$area = $f->filter($parameters["area"], null);
	        $block = array();
	        if ($area != null && strlen($area) > 0) {
	            if (!is_numeric($area)) {
	                $block[] = "Please, enter a valid area";
	                $isValid = false;
	            } else {
	                // Verificar que proporcione
	                $unidadMedida = $f->filter($parameters["areaUM"], null);
	                $block = array();
	                if ($unidadMedida == null || strlen($unidadMedida) == 0) {
	                    $block[] = "Please, enter a measurement unit for area";
	                    $isValid = false;
	                } else {
	                    if ($unidadMedida != "mt2" && $unidadMedida != "ft2" && $unidadMedida != "vr2") {
	                        $block[] = "Please, measurement unit for area must be mt2, ft2 or vr2";
	                        $isValid = false;
	                    }
	                }
	            }
	            if (count($block) > 0) {
	                $errors["Area"] = $block;
	            }
	        }	
		}
        

		if(isset($parameters["zipcode"])){
			$zipcode = $f->filter($parameters["zipcode"], null);
	        $block = array();
	        if ($zipcode != null) {
	            if ($zipcode == null || strlen($zipcode) == 0) {
	                $block[] = "Please, enter a country code";
	                $isValid = false;
	            } else {
	                if (strlen($zipcode) > 64) {
	                    $block[] = "Please, enter a valid country code";
	                    $isValid = false;
	                }
	            }
	            if (count($block) > 0) {
	                $errors["Zip Code"] = $block;
	            }
	        }	
		}
        

		if(isset($parameters["estado"])){
	        $estado = $f->filter($parameters["estado"], null);
	        $block = array();
	        if ($estado != null) {
	            if (strlen($estado) >= 64) {
	                $block[] = "Please, enter a valid state";
	                $isValid = false;
	            }
	            if (count($block) > 0) {
	                $errors["State"] = $block;
	            }
	        }
		}

		if(isset($parameters["ciudad"])){
	        $ciudad = $f->filter($parameters["ciudad"], null);
	        $block = array();
	        if ($ciudad != null) {
	            if (strlen($ciudad) > 64) {
	                $block[] = "Please, enter a valid city";
	                $isValid = false;
	            }
	            if (count($block) > 0) {
	                $errors["City"] = $block;
	            }
	        }
		}
		if(isset($parameters["calle"])){
			$calle = $f->filter($parameters["calle"], null);
	        $block = array();
	        if ($calle != null) {
	            if (strlen($calle) > 32) {
	                $block[] = "Please, enter a valid street";
	                $isValid = false;
	            }
	            if (count($block) > 0) {
	                $errors["Street"] = $block;
	            }
	        }	
		}

        $direccion = $f->filter($parameters["direccion"], null);
        $block = array();
        if ($direccion == null || strlen($direccion) == 0) {
            $block[] = "Please, enter an address";
            $isValid = false;
        } else {
            if (strlen($direccion) > 255) {
                $block[] = "Please, Address is too long. Address must be between 1 and  255 characters";
                $isValid = false;
            }
        }
        if (count($block) > 0) {
            $errors["Address"] = $block;
        }

        return array("valid" => $isValid, "errors" => $errors);
    }

    /* validando los numeros con coma */

    public function validanumcoma($numero) {
        $resultado = $numero;
        if ($numero != null && strlen($numero) > 2) {
            $validando = explode('.', $numero);
            if (count($validando) <= 2)
                $resultado = str_replace(',', '', $numero);
        }
        return $resultado;
    }

    /**
     * Guarda los datos de la propiedad.
     * @global Array $_FILES Contiene las referencias a los archivos a subir con la propiedad
     * @param Array $parameters Contiene los datos de la propiedad
     * @return <type>
     */
    public function SaveOrUpdate($parameters = array()) {
        global $_FILES;
        
        $id = null;
        $status = array();
        $isValid = $this->Validate($parameters);
        if (!$isValid["valid"] && count($isValid["errors"]) > 0) {
            // Mandar en el flash messenger todos los errores.
            return array("fail" => true, "reasons" => $isValid["errors"]);
        }
        $f = new Zend_Filter_StripTags();
        // Recuperar los datos del registro
        $estateData = array(
            "NombrePropiedad" => $f->filter($parameters["titulo"], null),
            "CodigoBroker" => $f->filter($parameters["sourceId"],null),
            "DescPropiedad" => $f->filter($parameters["descripcion"], null) ,
            "Direccion" => $f->filter($parameters["direccion"], null),
            "Estado" => $f->filter($parameters["estado"], null),
            "Ciudad" => $f->filter($parameters["ciudad"], null),
            "ZipCode" => $f->filter($parameters["zipcode"], null),
            
            "Latitud" => $f->filter($parameters["latitud"], null),
            "Longitud" => $f->filter($parameters["longitud"], null),
            "CodigoMoneda" => 1,
            "PrecioVenta" => $this->validanumcoma($f->filter($parameters["ventaUS"], null)),
            "PrecioAlquiler" => $this->validanumcoma($f->filter($parameters["alquilerUS"], null)),
            "Uid" => Zend_Registry::get('uid'),
            "Calle" => isset($parameters["calle"])? $f->filter($parameters["calle"], null):null,
            "CodigoPostal" => $f->filter($parameters["codigoPostal"], null)
        );

        $valForclosure = $f->filter($parameters["remateUS"], null);
        if ($valForclosure != null) {
            $estateData["PrecioVenta"] = $this->validanumcoma($f->filter($parameters["remateUS"], null));
        }

        $area = $f->filter($parameters["area"], null);
        $areaLote = $f->filter($parameters["areaLote"], null);
        $micod = $_POST["cat"];
        $sourceId = $f->filter($parameters['sourceId'],null);
        if($sourceId != null)
             $estateData["CodigoBroker"] = $sourceId;
        else
            $estateData["CodigoBroker"]= null;
        if ($micod == null)
            $micod = $_POST["cat2"]; //No es una propiedad Nueva. 

        if ($micod == 11) {
            $areaLote = $f->filter($parameters["area"], null);
        }
        if ($areaLote != null) {
            $umAreaLote = $f->filter($parameters["areaLoteUM"], "mt2");
            // Determinar el factor de conversion. 
            $factorAreaLote = $this->GetFactorConversionArea($umAreaLote); // Factor de conversion para Area Lote
            $estateData["AreaLote"] = $areaLote;
            $estateData["AreaLoteMt2"] = $factorAreaLote * doubleval($areaLote);
            $estateData["UnidadMedidaLote"] = $f->filter($parameters["areaLoteUM"], null);
            if ($micod == 11) {
                $estateData["UnidadMedidaLote"] = $f->filter($parameters["areaUM"], null);
            }
        } else {
            $estateData["AreaLote"] = null;
            $estateData["UnidadMedidaLote"] = null;
            $estateData["AreaLoteMt2"] = null;
        }
        if ($area != null) {
            $estateData["Area"] = $area;
            $umArea = $f->filter($parameters["areaUM"], "mt2");
            // Determinar el factor de conversion.
            $factorArea = $this->GetFactorConversionArea($umArea); //Factor de conversion para Area
            $estateData["UnidadMedida"] = $f->filter($parameters["areaUM"], null);
            $estateData["AreaMt2"] = $factorArea * doubleval($area);
        } else {
            $estateData["Area"] = null;
            $estateData["UnidadMedida"] = null;
            $estateData["AreaMt2"] = null;
        }
        

        $registro = $f->filter($parameters["registro"], "S");
        $publicado = $f->filter($parameters["publicado"], "S");
        $new = $f->filter($parameters["isNew"], "S");
        $isRegistro = $registro == "S" ? true : false;
        $isPublicada = $publicado == "S" ? true : false;
        $isNew = $new == "S" ? true : false;
        
        if ($isNew) {
            // Determinar si se puede crear el registro del cliente
            $cliente = new Hm_Cli_Cliente();
            /* $result = $cliente->CreateCliente(Zend_Registry::get('uid'));
              if(is_array($result) && $result["fail"] == false && $result["reason"]!="Duplicated")  {
              return array("fail"=>true, "reasons"=>$result["reason"]);
              } */
            
            $result = $cliente->CreateCliente(Zend_Registry::get('uid'));
            
            if (is_array($result) && $result["status"] === false && $result["reason"] != "Duplicated") {
                return array("fail" => true, "reasons" => $result["reason"]);
            }

            if ($isRegistro) {
                $estateData["CodigoEdoPropiedad"] = 1;
                $estateData["FechaRegistro"] = new Zend_Db_Expr('CURDATE()');
                $estateData["CodigoCategoria"] = $f->filter($parameters["cat"], null);
                $estateData["CargaMasiva"] = false;
            }
        }

        if ($isPublicada) {
            $estateData["CodigoEdoPropiedad"] = 2;
            $estateData["FechaPublica"] = new Zend_Db_Expr('CURDATE()');
        }

        if ($new == false) {
            $estateData["FechaModifica"] = new Zend_Db_Expr('CURDATE()');
        }
        
        $isForSale = $f->filter($parameters["ventaUS"], null) == null ? false : true;
        $isForRent = $f->filter($parameters["alquilerUS"], null) == null ? false : true;
        $isForForclosusre = $f->filter($parameters["remateUS"], null) == null ? false : true;

        if ($isForRent && $isForSale) {
            $estateData["Accion"] = "B";
        } else if ($isForRent) {
            $estateData["Accion"] = "R";
            $estateData["PrecioVenta"] = null;
        } else if ($isForSale) {
            $estateData["Accion"] = "S";
            $estateData["PrecioAlquiler"] = null;
        } else if ($isForForclosusre) {
            $estateData["Accion"] = "F";
            $estateData["PrecioAlquiler"] = null;
        }
        
        if ($isNew) { // Crear
            $result = $this->insert($estateData);
            $id = $this->getAdapter()->lastInsertId();
        } else { // Actualizar
            $id = $f->filter($parameters["id"], null);
            $result = $this->update($estateData, $this->getAdapter()->quoteInto("CodigoPropiedad=?", $id));
        }
        $estate = new Hm_Pro_Propiedad();
        $rsEstate = $estate->find($id);
        if ($rsEstate == null || $rsEstate->count() == 0) { // Si es creacion, entonces revisar que se haya creado el registro
            return array("fail" => true, "reasons" => "Estate not created");
        }
        
        // Save or Update Beneficios
        // Recuperar el estado de la operacion
        try {
        	$benefits = isset($parameters["benef"])? $parameters["benef"] : null;
			if($benefits != null){
        		$status["Benefits"] = Hm_Pro_BeneficioPropiedad::SaveOrUpdate($id, $benefits);
        	}
        } catch (Exception $ex) {
            $status["Benefits"] = array("fail" => true, "reason" => $ex->getMessage());
        }

        // Save or Update Atributos
        // Recuperar el estado de la operacion
        try {
            $categoryId = $rsEstate->current()->CodigoCategoria;
            $attributes = $parameters["attr"];
            $status["Attributes"] = Hm_Pro_AtributoPropiedad::SaveOrUpdate($id, $categoryId, $attributes, $isNew);
        } catch (Exception $ex) {
            $status["Attributes"] = array("fail" => true, "reason" => $ex->getMessage());
        }

        // Save Album de Foto -> Save or Update Fotos
        // Recuperar el estado del album de la foto
        try {
            $photos["size"] = count($_FILES);
            $photos["photos"]["secundarias"] = array();
            $photoCaptions = $parameters["fdescotra"];
            foreach ($_FILES as $key => $file) {
                if (!(stristr($key, "fotra") === false)) {
                    // Transformando
                    $captionKey = str_replace("fotra", "", $key);
                    if (stristr($captionKey, "New") !== false) {
                        $captionKey = str_replace("New", "new_", $captionKey);
                    }
                    // Transformar el key hasta obtener el caption correspondiente.
                    $photos["photos"]["secundarias"][$key]["caption"] = $photoCaptions[$captionKey];
                    $photos["photos"]["secundarias"][$key]["photo"] = $file;
                } else if (!(stristr($key, "fprinc") === false)) { // Foto Principal
                    $photos["photos"]["principal"][$key]["photo"] = $_FILES[$key];
                    $photos["photos"]["principal"][$key]["caption"] = $f->filter($parameters["fdescprinc"], "");
                }
            }

            $status["photos"] = Hm_Pro_Foto::SaveOrUpdate($id, $photos);
        } catch (Exception $ex) {
            $status["photos"] = array("fail" => true, "reason" => $ex->getMessage());
        }

        return array("id" => $id, "status" => $status);
    }

    /**
     * Retorna los beneficios para una determinada propiedad
     * @param Integer $propertyId Id de una propiedad
     * @return Array
     */
    public function GetBeneficiosByProperty($propertyId) {
        $language = Zend_Registry::get('language');

        $this->dbAdapter = Zend_Registry::get('db');

        $sql = "SELECT pb.CodigoBeneficio FROM probeneficiopropiedad  pb INNER JOIN catbeneficio cb on pb.CodigoBeneficio=cb.CodigoBeneficio";
        $sql .= " INNER JOIN propropiedad p ON pb.CodigoPropiedad = p.CodigoPropiedad";
        $sql .= " WHERE p.CodigoPropiedad = " . intval($propertyId);

        // Resultados
        $rs = $this->dbAdapter->query($sql, null);
        $beneficios = $rs->fetchAll();
        $beneficiosArray = array();
        $total = $rs->rowCount();
        
        foreach ($beneficios as $key => $beneficio) {
            $beneficio->nombre = htmlentities($beneficio->nombre);
            $beneficiosArray[$key] = $beneficio;
        }
        return $beneficiosArray;
    }
	
	public static function checkLimit($uid) {
        $this->dbAdapter = Zend_Registry::get('db');
        $params = array($uid);
        $count_sql = "SELECT COUNT(*) num_prop FROM propiedad WHERE uid= ?";
        $rs = $db->query($count_sql, $params);
        $data = $rs->fetchAll();
        $count = $data[0]->num_prop;
        if ($count >= 40) {
            return false;
        } else {
            return true;
        }
    }

}

?>