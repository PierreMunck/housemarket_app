<?php

class Hm_Pro_AtributoPropiedad extends Zend_Db_Table {
    /**
     * Nombre de la tabla
     * @var String
     */
    protected $_name = 'proatributopropiedad';

    /**
     * Nombre de la llave primaria
     * @var String
     */
    protected $_primary = array('CodigoPropiedad','CodigoAtributo');

    /**
     * Adaptador de la base de datos
     * @var Zend_DbAdapter
     */
    public $dbAdapter;

    protected $_referenceMap    = array(
        'Hm_Pro_Propiedad' => array(
            'columns'           => 'CodigoPropiedad',
            'refTableClass'     => 'Hm_Pro_Propiedad',
            'refColumns'        => 'CodigoPropiedad'
        ),
        'Hm_Cat_Atributo' => array(
            'columns'           => 'CodigoAtributo',
            'refTableClass'     => 'Hm_Cat_Atributo',
            'refColumns'        => 'CodigoAtributo'
        )
    );

    /**
     * Guarda o actualiza las propiedades.
     * No se eliminan. Solo se actualizan los valores.
     * @param <type> $estateId Id de la propiedad
     * @param <type> $attributes Listado de atributos
     */
    public static function SaveOrUpdate($estateId, $categoryId, $attributes = array(), $isNew = true) {
        $rowsId = array();
        $fails = array();
        $atributoPropiedad = new Hm_Pro_AtributoPropiedad();
        
        if($estateId != null && $categoryId != null && is_array($attributes)) {
            foreach($attributes as $id=>$attribute) {
                try {
                    // Recuperar el campo donde se va a guardar el valor del atributo en la propiedad.
                    $catAtributo = new Hm_Cat_AtributoCategoria();
                    $where = $catAtributo->getAdapter()->quoteInto("CodigoAtributo = ?", $id);
                    $where .= $catAtributo->getAdapter()->quoteInto(" AND CodigoCategoria = ?", $categoryId);
                    $rs = $catAtributo->fetchAll($where);
                    // $rs = $catAtributo->find($id, $categoryId);
                    if($rs->count() > 0) {// Existe el atributo para la categoria indicada
                        $row = $rs->current();
                        $data["CodigoPropiedad"] = $estateId;
                        $data["CodigoAtributo"] = $id;
                        unset($data["ValorEntero"]);
                        unset($data["ValorDecimal"]);
                        unset($data["ValorS_N"]);
                        unset($data["ValorCaracter"]);
                        $data[$row->CampoDestino] = $attribute;
                        $where = $atributoPropiedad->getAdapter()->quoteInto("CodigoPropiedad=?", $estateId);
                        $where .= $atributoPropiedad->getAdapter()->quoteInto(" AND CodigoAtributo=?", $id);
                        // Revisar si es existe registro.
                        // de lo contrario, crear uno nuevo.
                        $rsAttrProp = $atributoPropiedad->fetchAll($where);
                        if($rsAttrProp->count()> 0) {
                            if($attribute != null) {
                                $atributoPropiedad->update($data, $where);
                                $rowsId[] = array("action"=>"update", "id"=>$id);
                            } else {
                                $atributoPropiedad->delete($where);
                                $rowsId[] = array("action"=>"delete", "id"=>$id);
                            }
                        } else {
                            if($attribute != "") {
                                $atributoPropiedad->insert($data);
                                $rowsId[] = array("action"=>"insert", "id"=>$catAtributo->getAdapter()->lastInsertId());
                            }
                        }
                    }
                } catch(Exception $ex) {
                    $fails[] = array("id" => $id , "reason"=>$ex->getMessage());
                }
            }
        } else {
            if(!is_array($attributes)) { // Remover todos los atributos
                $where = $atributoPropiedad->getAdapter()->quoteInto("CodigoPropiedad=?", $estateId);
                $atributoPropiedad->delete($where);
            }
            if($estateId == null) {
                $fails[] = array("id" => $estateId, "reason"=>"Estate id is null");
            }
        }
        return array("success"=>$rowsId, "fails"=>$fails);
    }

}

?>
