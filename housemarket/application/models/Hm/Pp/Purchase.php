<?php

/**
 * Compras en linea de paypal
 */
class Hm_Pp_Purchase extends Zend_Db_Table {

    /**
     * Nombre de la tabla
     */
    protected $_name = 'PP_Purchase';

    /**
     * Nombre de la llave primaria
     */
    protected $_primary = 'PurchaseId';

    /**
     * Adaptador de la base de datos
     * @var Zend_Db_Adapter
     */
    public $dbAdapter;

    protected $_dependentTables = array(
            'Hm_Pp_PurchaseDetail'
    );


    /**
     * Cantidad minima de creditos que deben comprarse por propiedad
     * Valor por defecto.
     */
    const DEFAULT_MIN_CREDITS = 10;

    /**
     * Costo de cada credito.
     * Valor por defecto.
     */
    const DEFAULT_PRICE_BY_ITEMS = 1;

    /**
     * Genera el codigo de factura a enviar a paypal.
     * @param Integer $purchaseId Id de la compra con la cual se asocia la factura
     * @param Integer $userId Id del usuario que realiza la compra
     */
    public function GenerateInvoiceCode($purchaseId, $userId) {
        $plainCode = $purchaseId . "-" . $userId;
        $sql = "SELECT md5(?) as invoice";
        $result = $this->getAdapter()->fetchOne($sql, $plainCode);
        return $result;
    }

}

?>