<?php

/**
 * Detalle de la compra en linea
 */
class Hm_Pp_PurchaseDetail extends Zend_Db_Table {

    /**
     * Nombre de la tabla
     * @var String
     */
    protected $_name = 'PP_PurchaseDetail';

    /**
     * Nombre de la llave primaria
     * @var String
     */
    protected $_primary = 'PurchaseDetailId';

    /**
     * Adaptador de la base de datos
     * @var Zend_Db_Adapter
     */
    public $dbAdapter;

    protected $_referenceMap    = array(
            'Hm_Pp_Purchase' => array(
                            'columns'           => 'PurchaseId',
                            'refTableClass'     => 'Hm_Pp_Purchase',
                            'refColumns'        => 'PurchaseId'
            )
    );

}

?>
