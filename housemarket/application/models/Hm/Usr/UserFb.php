<?php
/**
 * Description of User
 *
 * @author Administrador
 */
class Hm_Usr_UserFb {

    public static function ObtenerDatosUsuario($uid) {
        try {
            $config = Zend_Registry::get('config');
            $fbDgt = Dgt_Fb::getInstance();
            $ListValue = $fbDgt->getDataUser($uid, array('first_name', 'name', 'pic_big', 'current_location', 'pic_small'));
            return $ListValue;
        }
        catch (Exception $e) {
            return null;
        }
    }

}
?>
