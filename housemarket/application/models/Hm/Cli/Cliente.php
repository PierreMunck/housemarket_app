<?php

class Hm_Cli_Cliente extends Zend_Db_Table {
    /**
     * Nombre de la tabla
     * @var String
     */
    protected $_name = 'cliente';

    /**
     * Nombre de la llave primaria de la tabla
     * @var Integer
     */
    protected $_primary = 'CodigoCliente';

    /**
     *
     * @var Zend_Adapter
     */
    public $dbAdapter;

    /**
     * Realiza una consulta con la base de datos sobre la tabla Propiedad
     * @param String $sql Consulta a realizar
     * @param array $params Parametros que se le pasan a la consulta a realizar
     * @return RowSet Resultado de la consulta
     */
    private function DoQuery ($sql, $params=null) {
        

        if (isset($params)) {
            $rs = $this->_db->query($sql,$params);
        } else {
            $rs = $this->_db->query($sql);
        }
        $data = $rs->fetchAll();
        $test = $rs->rowCount();
        return $data;
    }

    public function GetConsultaClientes() {

        

        // Recuperar el listado de clientes ordenados por creditos
        $sql_clientes = "SELECT DISTINCT VW.CodigoCliente, VW.NombreCliente, VW.TelefonoCliente, VW.WebPage, VW.Email, VW.Uid
                        FROM vw_clientes_creditos VW INNER JOIN propropiedad P on VW.Uid = P.Uid
                        WHERE P.CodigoEdoPropiedad=2 ORDER BY VW.total_credito DESC LIMIT 0, 8";
        $clientes_id = $this->DoQuery($sql_clientes);

        $clientesFb = array();
        // Recuperamos los ids para luego extraer la informacion en facebook.
        $uids = array();
        if(is_array($clientes_id)) {
            foreach($clientes_id as $cliente) {
                $uids[$cliente->Uid] = $cliente->Uid;
                $clientInfo[$cliente->Uid] = $cliente;
            }
            // Extraemos la informacion en facebook
            $fbDgt = Dgt_Fb::getInstance();
            $clientesFb = $fbDgt->execFql("SELECT uid, first_name, last_name, pic_square, profile_url FROM user WHERE uid IN (" . implode(",", $uids) . ") AND is_app_user");
        }
        // Ordenamos los usuarios de facebooks por el el uid, para luego ser consultados
       $resultFb = array();
       $result = array();

	if(is_array($clientesFb)) {
           foreach($clientesFb as $cliente) {
               $resultFb[$cliente["uid"]] = $cliente;
           }
	}

	if(is_array($clientInfo)) {
           foreach($clientInfo as $cliente) {
               // si tenemos un resultado facebook
               if(isset($resultFb[$cliente->Uid])){
	               $fbClient = $resultFb[$cliente->Uid];
	               $dbCliente["first_name"] = $fbClient["first_name"];
	               $dbCliente["last_name"] = $fbClient["last_name"];
	               $dbCliente["pic_square"] = $fbClient["pic_square"];
	               $dbCliente["profile_url"] = $fbClient["profile_url"];
	               $dbCliente["CodigoCliente"] = $cliente->CodigoCliente;
	               $dbCliente["NombreCliente"] = $cliente->NombreCliente;
	               $dbCliente["TelefonoCliente"] = $cliente->TelefonoCliente;
	               $dbCliente["WebPage"] = $cliente->WebPage;
	               $dbCliente["Email"] = $cliente->Email;
	               $dbCliente["Uid"] = $cliente->Uid;
	               $result[] = $dbCliente;
               }
           }
       }

        return $result;
    }

    /**
     * Retorna un array a partir de una cadena, segmentada en varios trozos.
     * @param String Cadena a ser cortada en trozos
     * @param Integer longitudes en que sera cortada la cadena
     * @return Array
     */
    public function SectionString($string, $max_characters = 12){
        $length = strlen($string);
        $shunksCantity = ceil($length/$max_characters);
        $string_result = array();
        $start = 0;
        for($i=1;$i<=$shunksCantity;$i++){
            $string_result[] = substr($string, $start, $max_characters);
            $start = $start + $max_characters + 1;
        }

        return $string_result;
    }

    public function GetBrokersByArea($coordinates) {
        $db = Zend_Registry::get('db');
        $p = new Hm_Pro_Propiedad();

        //Recuperar los brokers por el area determinada en el mapa

        $sql_select = "SELECT cli.profilePageID AS pageid,cli.WebPage,(CASE WHEN (cli.profilePageID = \"\" OR cli.profilePageID IS NULL OR cli.profilePageID = 0) THEN 
					cli.NombreCliente 
		 ELSE (SELECT nombre FROM page WHERE pageid=cli.profilePageID) 
		 END) as NombreCliente,
              cli.Uid as Uid,
              coalesce(
               (    select sum(coalesce(cr.cantidad,0)) from crecreditopropiedad cr
			inner join crecredito c on (cr.CodigoCredito = c.CodigoCredito)
                       where cli.CodigoCliente = c.CodigoCliente and (cr.FechaRetira >= Current_Date() OR cr.FechaRetira is null) and c.EstadoCredito='A'
                       and p.CodigoEdoPropiedad = 2
              ),
            0) as creditos
                FROM
                cliente cli
                INNER JOIN propropiedad p ON (p.Uid = cli.Uid)                
                INNER JOIN atributopropiedad ak ON (p.CodigoPropiedad = ak.CodigoPropiedad)";

        $sql_where = ' WHERE ';
        $sql_where_conditions = $p->PrepareWherePartStatement("p", "ap", "ac","ak", null, null, null, null, null, null, null, null, null, null, null, null, null, $coordinates);
        $sql_where_conditions[] = " p.CodigoEdoPropiedad = 2";

        if(count($sql_where_conditions) > 0) {
            $sql_where .= implode(" AND ", $sql_where_conditions);
        } else {
            $sql_where = "";
        }
        $sql_order_by = " GROUP BY cli.NombreCliente ORDER BY creditos DESC, p.CodigoPropiedad DESC LIMIT 5";

        $sql = $sql_select . $sql_where . $sql_order_by;
       
        $brokers_id = $db->fetchAll($sql);
        
        $userIds = array();
        $pageIds = array();
        
        foreach ($brokers_id as $broker) {
            if($broker->pageid) {
                $pageIds[] = $broker->pageid;
            }
            else {
                $userIds[] = $broker->Uid;
            }
        }
        
        $multiqueryFql = 
            '{
                "page_pics": "SELECT page_id, pic_square FROM page WHERE page_id IN (' . implode(',', $pageIds) . ')",
                "user_pics": "SELECT uid, pic_square FROM user WHERE uid IN (' . implode(',', $userIds) . ')"
            }';
        $fbDgt = Dgt_Fb::getInstance();
        $multiqueryRs = $fbDgt->execMultiqueryFql($multiqueryFql);
        
        $aPagePic = HMUtils::build_simple_map_array_from_array($multiqueryRs[0]['fql_result_set'], 'page_id', 'pic_square');
        $aUserPic = HMUtils::build_simple_map_array_from_array($multiqueryRs[1]['fql_result_set'], 'uid', 'pic_square');
                
        $result=array();
        foreach($brokers_id as $index => $broker){
        	$brokerresult = array();
			$brokerresult['Uid'] = $broker->Uid;
			$brokerresult['pageid']= $broker->pageid;
        	if(isset($aUserPic[$broker->Uid])){
            	$brokerresult['pic_square'] = $aUserPic[$broker->Uid];
        	}
            if($broker->pageid){
                $sw = "SELECT webpage FROM page WHERE pageid = " . $broker->pageid;
                $mypage = $db->fetchAll($sw);
                $brokerresult['WebPage'] = $mypage[0]->webpage;
                $brokerresult['pic_square'] = $aPagePic[$broker->pageid];
            }
            
            $brokerresult['WebPage'] = formatUrl($broker->WebPage);
            $brokerresult['NombreCliente'] = (strlen($broker->NombreCliente)>21) ? substr($broker->NombreCliente,0,18) . "..." : $broker->NombreCliente;
            
            $result[] = $brokerresult;
        }        
        return $result;
    }

    /**
     * Retorna todos los propietarios de cuartos en un �rea determinada
     * @param Array $coordinates Coordenadas de referencia
     * @return <type>
     */
     public function GetOwnersRoomByArea(
            $coordinates
            ) {
        

        $p = new Hm_Pro_Propiedad();

        //Recuperar los brokers por el area determinada en el mapa

        $sql_select = "SELECT DISTINCT
              cli.CodigoCliente as CodigoCliente,
              cli.NombreCliente as NombreCliente,
              cli.Uid as Uid,
              coalesce(
               (   select
                 sum(coalesce(cr.cantidad,0))
                from
                crecredito cr
               where (cli.CodigoCliente = cr.CodigoCliente) and cr.EstadoCredito='A'
              ),
            0) as creditos
                FROM
                cliente cli
                INNER JOIN RMTCuarto rc ON (rc.Uid = cli.Uid)
                LEFT OUTER JOIN crecredito cre ON (cli.CodigoCliente = cre.CodigoCliente) ";

        $sql_where = ' WHERE ';
        $sql_where_conditions = $p->PrepareWherePartStatement("rc", "ap", "ac", null, null, null, null, null, null, null, null, null, null, null, null, null, null, $coordinates);
        $sql_where_conditions[] = " rc.EstadoRoommate = 64";

        if(count($sql_where_conditions) > 0) {
            $sql_where .= implode(" AND ", $sql_where_conditions);
        } else {
            $sql_where = "";
        }
        $sql_order_by = " ORDER BY creditos DESC";

        $sql = $sql_select . $sql_where . $sql_order_by;

        $brokers_id = $this->DoQuery($sql, null);

         $clientesFb = array();
        // Recuperamos los ids para luego extraer la informacion en facebook.
        $uids = array();
        if(is_array($brokers_id)) {
            foreach($brokers_id as $cliente) {
                $uids[$cliente->Uid] = $cliente->Uid;
                $clientInfo[$cliente->Uid] = $cliente;
            }
            // Extraemos la informacion en facebook
            $fbDgt = Dgt_Fb::getInstance();
            $clientesFb = $fbDgt->execFql("SELECT uid, first_name, last_name, pic_square, profile_url FROM user WHERE uid IN (" . implode(",", $uids) . ") AND is_app_user");
        }
        // Ordenamos los usuarios de facebooks por el el uid, para luego ser consultados
       $resultFb = array();
       $result = array();

	if(is_array($clientesFb)) {
           foreach($clientesFb as $cliente) {
               $resultFb[$cliente["uid"]] = $cliente;
           }
	}

        $lengChunkString = 10;
	if(is_array($clientInfo)) {
           foreach($clientInfo as $cliente) {
               $fbClient = $resultFb[$cliente->Uid];

               if(strlen($fbClient["first_name"]) > $lengChunkString){
                 $dbCliente["first_name"] =  implode("<br/>",$this->SectionString($fbClient["first_name"]));
               } else{
                    $dbCliente["first_name"] = $fbClient["first_name"];
               }
               if(strlen($dbCliente["last_name"]) > $lengChunkString){
                   $dbCliente["last_name"] =  implode("<br/>",$this->SectionString($fbClient["last_name"]));
               } else{
                   $dbCliente["last_name"] = $fbClient["last_name"];
               }

               $dbCliente["pic_square"] = $fbClient["pic_square"];
               $dbCliente["profile_url"] = $fbClient["profile_url"];
               $dbCliente["CodigoCliente"] = $cliente->CodigoCliente;
               $dbCliente["NombreCliente"] = $cliente->NombreCliente;

               $dbCliente["Uid"] = $cliente->Uid;
               $result[] = $dbCliente;
           }
       }
        return $result;
    }

    /**
     * Retorna todos los propietarios de cuartos en un �rea determinada
     * @param Array $coordinates Coordenadas de referencia
     * @return <type>
     */
     public function GetRoommatesByArea(
            $coordinates
            ) {
        

        $p = new Hm_Pro_Propiedad();

        //Recuperar los brokers por el area determinada en el mapa

        $sql_select = "SELECT DISTINCT
              cli.CodigoCliente as CodigoCliente,
              cli.NombreCliente as NombreCliente,
              cli.Uid as Uid,
              coalesce(
               (   select
                 sum(coalesce(cr.cantidad,0))
                from
                crecredito cr
               where (cli.CodigoCliente = cr.CodigoCliente) and cr.EstadoCredito='A'
              ),
            0) as creditos
                FROM
                cliente cli
                INNER JOIN RMTRoommate rmmt ON (rmmt.Uid = cli.Uid)";

        $sql_where = ' WHERE ';
        $sql_where_conditions = $p->PrepareWherePartStatement("rmmt", "ap", "ac", null, null, null, null, null, null, null, null, null, null, null, null, null, null, $coordinates);
        $sql_where_conditions[] = " rmmt.EstadoRoommate = 64";

        if(count($sql_where_conditions) > 0) {
            $sql_where .= implode(" AND ", $sql_where_conditions);
        } else {
            $sql_where = "";
        }
        $sql_order_by = " ORDER BY CodigoCliente DESC";

        $sql = $sql_select . $sql_where . $sql_order_by;

        $brokers_id = $this->DoQuery($sql, null);

         $clientesFb = array();
        // Recuperamos los ids para luego extraer la informacion en facebook.
        $uids = array();
        if(is_array($brokers_id)) {
            foreach($brokers_id as $cliente) {
                $uids[$cliente->Uid] = $cliente->Uid;
                $clientInfo[$cliente->Uid] = $cliente;
            }
            $registry = Zend_Registry::getInstance();
            
            // Extraemos la informacion en facebook
            $fbDgt = Dgt_Fb::getInstance();
            $clientesFb = $fbDgt->execFql("SELECT uid, first_name, last_name, pic_square, profile_url FROM user WHERE uid IN (" . implode(",", $uids) . ") AND is_app_user");
        }
        // Ordenamos los usuarios de facebooks por el el uid, para luego ser consultados
       $resultFb = array();
       $result = array();

	if(is_array($clientesFb)) {
           foreach($clientesFb as $cliente) {
               $resultFb[$cliente["uid"]] = $cliente;
           }
	}

        $lengChunkString = 10;
	if(is_array($clientInfo)) {
           foreach($clientInfo as $cliente) {
               $fbClient = $resultFb[$cliente->Uid];

               if(strlen($fbClient["first_name"]) > $lengChunkString){
                 $dbCliente["first_name"] =  implode("<br/>",$this->SectionString($fbClient["first_name"]));
               } else{
                    $dbCliente["first_name"] = $fbClient["first_name"];
               }
               if(strlen($dbCliente["last_name"]) > $lengChunkString){
                   $dbCliente["last_name"] =  implode("<br/>",$this->SectionString($fbClient["last_name"]));
               } else{
                   $dbCliente["last_name"] = $fbClient["last_name"];
               }

               $dbCliente["pic_square"] = $fbClient["pic_square"];
               $dbCliente["profile_url"] = $fbClient["profile_url"];
               $dbCliente["CodigoCliente"] = $cliente->CodigoCliente;
               $dbCliente["NombreCliente"] = $cliente->NombreCliente;

               $dbCliente["Uid"] = $cliente->Uid;
               $result[] = $dbCliente;
           }
       }
        return $result;
    }

    public function GetAllFacebookBrokers() {

        // Conector a la base de datos
        

        // Recuperar el listado de clientes ordenados por creditos
        $sql_clientes = "SELECT DISTINCT VW.CodigoCliente, VW.NombreCliente, VW.TelefonoCliente, VW.WebPage, VW.Email, VW.Uid
                        FROM vw_clientes_creditos VW INNER JOIN propropiedad P on VW.Uid = P.Uid WHERE P.CodigoEdoPropiedad=2
                        ORDER BY VW.total_credito DESC";
        $clientes_id = $this->DoQuery($sql_clientes);

        $clientesFb = array();
        // Recuperamos los ids para luego extraer la informacion en facebook.
        $uids = array();
        if(is_array($clientes_id)) {
            foreach($clientes_id as $cliente) {
                $uids[$cliente->Uid] = $cliente->Uid;
                $clientInfo[$cliente->Uid] = $cliente;
            }
            // Extraemos la informacion en facebook
            $fbDgt = Dgt_Fb::getInstance();
            $clientesFb = $fbDgt->execFql("SELECT uid, first_name, last_name, pic_square, profile_url FROM user WHERE uid IN (" . implode(",", $uids) . ") AND is_app_user");
        }
        // Ordenamos los usuarios de facebooks por el el uid, para luego ser consultados
        $resultFb = array();
	if(is_array($clientesFb)) {
	       foreach($clientesFb as $cliente) {
       	    $resultFb[$cliente["uid"]] = $cliente;
	       }
	}

       $result = array();
       foreach($clientInfo as $cliente) {
           // si tenemos un resultado facebook
       	   if(isset($resultFb[$cliente->Uid])){
	           $fbClient = $resultFb[$cliente->Uid];
	           $dbCliente["first_name"] = $fbClient["first_name"];
	           $dbCliente["last_name"] = $fbClient["last_name"];
	           $dbCliente["pic_square"] = $fbClient["pic_square"];
	           $dbCliente["profile_url"] = $fbClient["profile_url"];
	           $dbCliente["CodigoCliente"] = $cliente->CodigoCliente;
	           $dbCliente["NombreCliente"] = $cliente->NombreCliente;
	           $dbCliente["TelefonoCliente"] = $cliente->TelefonoCliente;
	           $dbCliente["WebPage"] = $cliente->WebPage;
	           $dbCliente["Email"] = $cliente->Email;
	           $dbCliente["Uid"] = $cliente->Uid;
	           $result[] = $dbCliente;
       	   }
       }

        return $result;
    }

    /**
     *
     * @param String $uid UId del usuario en facebook
     * @return array : status => true si se crea el registro del cliente, false en caso contrario
     *                 id => Id del cliente recien creado
     *                 reason => Razon por la cual no se pudo crear el registro del cliente
     */
    public function CreateCliente($uid){
        
        $resCliente= $this->fetchAll("Uid=".$uid);
        $fbDgt = Dgt_Fb::getInstance();
        $ListValue=  $fbDgt->getDataUser($uid, array('name','email'));

        if($resCliente != null && $resCliente->count() > 0){
            return array("status"=>false, "reason"=>"Duplicated");
        }

        $insert_cliente = array(
                        'NombreCliente' => $ListValue[0]['name'],
                        'EMail' => $ListValue[0]['email'],
                        'Uid' => $uid,
                        'FechaRegistro' => new Zend_Db_Expr('CURDATE()'),
                        'Origen' => $config['org_fbk']
                    );

       try{
        $this->insert($insert_cliente);
        return array("status"=>true, "id"=>$this->getAdapter()->lastInsertId());
       }
       catch (Exception $e){
        return array("status"=>false, "reason"=>$e->getMessage());
       }

    }

    public function NuevoCliente($uid){
        $db = Zend_Registry::get('db');
        $resCliente = $db->fetchAll("SELECT CodigoCliente FROM cliente where Uid=".$uid);
        if(count($resCliente)>0){
            return true;
        }
        $fbDgt = Dgt_Fb::getInstance();
        $ListValue=  $fbDgt->getDataUser($uid, array('name','email'));
		
        $insert_cliente = array(
                        'NombreCliente' => $ListValue['name'],
                        'EMail' => $ListValue['email'],
                        'Uid' => $uid,
                        'FechaRegistro' => new Zend_Db_Expr('CURDATE()'),
                        'Origen' => $config['org_fbk']
                    );

       $ingreso = new Hm_Cli_Cliente();
       try{
        $ingreso->insert($insert_cliente);
        return true;
       }
       catch (Exception $e){
        return false;
       }

    }

}

?>
