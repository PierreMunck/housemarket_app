<?php
class Dgt_Propiedad extends Zend_Db_Table {
    protected $_name = 'propiedad';
    protected $_primary = 'idpropiedad';

    private function doQuery ($sql, $params=null) {
        if (isset($params)) {
            $rs = $this->_db->query($sql,$params);
        } else {
            $rs = $this->_db->query($sql);
        }
        $data = $rs->fetchAll();
        $test = $rs->rowCount();
        return $data;
    }

    public function getPropiedad($propId) {
    // destacada:
    // 1 - Premium
    // 2 - Deluxe
    // 3 - Regular
    // 4 - Free
    //SELECT p.*, f.* FROM propiedad p LEFT JOIN prop_foto f ON (p.idpropiedad = f.idpropiedad)
        /* $sql = 'SELECT idpropiedad, descripcion, pais, precio_venta, area,
cuartos, banos, lat,lng,uid FROM propiedad WHERE estatus = 3
ORDER BY 3 LIMIT 100'; */
        $params = array($propId);
        //$sql = 'SELECT p.* FROM propiedad p WHERE p.idpropiedad = ?';
        $sql = 'SELECT   p.*  FROM  propiedad p
                WHERE (p.idpropiedad = ? )';
        return $this->doQuery($sql, $params);
    }

    public function getPropiedad_foto_Habilitado($propId) {
        $params = array($propId);
        //$sql = 'SELECT p.* FROM propiedad p WHERE p.idpropiedad = ?';

        $sql = 'SELECT   prop_foto.habilitado,  prop_foto.src  FROM  prop_foto_album
               LEFT OUTER JOIN prop_foto ON (prop_foto_album.aid = prop_foto.prop_foto_album_aid)
               WHERE  (prop_foto.habilitado = 1) AND   (prop_foto_album.idpropiedad = ?)';

        return $this->doQuery($sql, $params);
    }

    public function getDestacadas($city, $venta=null, $alquiler=null) {
        $sql = 'SELECT * FROM propiedad WHERE ciudad LIKE "%?%"
AND estatus = 3 AND destacada = 1 ';
        if (isset($venta)) {
            $sql .= ' AND tipo_propiedad = 1';
        }
        if (isset($alquiler)) {
            $sql .= ' AND tipo_propiedad = 2';
        }
        $params = array($city);
        return $this->doQuery($sql, $params);
    }

    public function filter($cc, $city, $dir=null, $tipo=null,
        $min_price=0, $max_price=0, $cuartos1=0, $banos1=0,
        $area1=0, $cuartos2=0, $banos2=0, $area2=0, $cat = null,
        $x0=null, $y0=null, $x1=null, $y1=null, $start = 0, $limit = 25) {


        $params = array();

        if(is_numeric($dir)) {
            $sql = 'SELECT p.*, f.src_destacadas_normal src_normal, f.src_destacadas_small src_small, f.src_medium src, f.src_medium_width width, f.src_medium_height height, f.habilitado FROM propiedad p left join prop_foto_album a on (p.idpropiedad = a.idpropiedad) left join prop_foto f on (a.aid = f.prop_foto_album_aid) WHERE p.idpropiedad = ? AND p.estatus = 3 ';
            $params[] = $dir;

            if(is_numeric($x0) && is_numeric($y0) && is_numeric($x1) && is_numeric($y1)) {
                $geom =" GeomFromText('Polygon(($x0 $y0, $x1 $y0, $x1 $y1, $x0 $y1, $x0 $y0))')";
                //$sql = 'SELECT * FROM propiedad p WHERE p.estatus = 3 AND MBRContains('. $geom .', GeomFromWKB(Point(p.lng, p.lat))) ';
                $sql = 'SELECT p.*, f.src_destacadas_normal src_normal, f.src_destacadas_small src_small ,f.src_medium src, f.src_medium_width width, f.src_medium_height height, f.habilitado FROM propiedad p left join prop_foto_album a on (p.idpropiedad = a.idpropiedad) left join prop_foto f on (a.aid = f.prop_foto_album_aid) WHERE p.estatus = 3 AND MBRContains('. $geom .', GeomFromWKB(Point(p.lng, p.lat))) ';
            } else {
                if(isset($city) && (strlen(trim($city)) > 0)) {
                    $exp1 = "%$city%";
                    $params[] = $city;
                    $sql = $this->_db->quoteInto("SELECT p.*, f.src_destacadas_normal src_normal, f.src_destacadas_small src_small,  f.src_medium src, f.src_medium_width width, f.src_medium_height height, f.habilitado FROM propiedad p left join prop_foto_album a on (p.idpropiedad = a.idpropiedad) left join prop_foto f on (a.aid = f.prop_foto_album_aid) WHERE p.ciudad LIKE ?  AND p.estatus = 3", $exp1);
                } else {
                //$sql = 'SELECT * FROM propiedad p WHERE p.estatus = 3 ';
                    $sql = 'SELECT p.*, f.src_destacadas_normal src_normal, f.src_destacadas_small src_small,  f.src_medium src, f.src_medium_width width, f.src_medium_height height, f.habilitado FROM propiedad p left join prop_foto_album a on (p.idpropiedad = a.idpropiedad) left join prop_foto f on (a.aid = f.prop_foto_album_aid) WHERE  p.estatus = 3';
                }
            }

            // tipo_enlistamiento: alquiler, venta
            // tipo_propiedad: categoria -apartemento,casa,edificio,etc.
            /*
            if ( ($venta != 0 ) && ($alquiler == 0)) {
                $sql .= ' AND p.tipo_enlistamiento = 1';
            }
            if (($venta == 0 ) && ($alquiler != 0)) {
                $sql .= ' AND p.tipo_enlistamiento = 2';
            }
            if (($venta != 0 ) && ($alquiler != 0 )) {
                $sql .= ' AND p.tipo_enlistamiento = 3';
            }
             */
            if(isset($tipo)) {
                if($tipo == 1) {
                // alquiler
                    $sql .= ' AND p.tipo_enlistamiento = 2';
                }
                if($tipo == 2) {
                // venta
                    $sql .= ' AND p.tipo_enlistamiento = 1';
                }
            }

            if( isset($cc) && (strlen(trim($cc)) > 0) ) {
                $params[] = $cc;
                $sql .= " AND p.pais = ?";
                $sql = $this->_db->quoteInto($sql, $cc);
            }

             if( isset($cat) && (strlen(trim($cat)) > 0) ) {
                $params[] = $cat;
                $sql .= " AND p.tipo_propiedad = ?";
                $sql = $this->_db->quoteInto($sql, $cat);
            }

            if ( ($min_price >= 0) ) {
            //$params[] = $min_price;
            //$params[] = $max_price;
                if($tipo == 1) {
                // alquiler
                         if($max_price==0){
                $sql .= " AND ( p.precio_alquiler >= $min_price )";
                         }else{
                $sql .= " AND ( p.precio_alquiler BETWEEN $min_price AND $max_price )";
                         }

                    
                }
                if($tipo == 2) {
                // venta
                        if($max_price==0){
                $sql .= " AND ( p.precio_venta >= $min_price )";
                         }else{
                $sql .= " AND ( p.precio_venta BETWEEN $min_price AND $max_price )";
                         }
                    
                }
            }

            if (($cuartos1 >= 0)) {
                         if($cuartos2==0){
                $sql .= " AND ( p.cuartos >= $cuartos1 )";
                         }else{
                $sql .= " AND ( p.cuartos BETWEEN $cuartos1 AND $cuartos2 )";
                         }
            }

            if (($banos1 >= 0)) {
                    if($banos2==0){
                $sql .= " AND (  p.banos >= $banos2 )";
                         }else{
                $sql .= " AND ( p.banos BETWEEN $banos1 AND $banos2 )";
                         }

            }
            if (($area1 >= 0)) {
                   if($area2==0){
                $sql .= " AND (  p.area >= $area2 )";
                         }else{
                $sql .= " AND ( p.area BETWEEN $area1 AND $area2 )";
                         }
            }

        }
        $sql .= ' GROUP BY p.idpropiedad ORDER BY p.creditos DESC ';
        $sql .= " LIMIT $start, $limit";

        return $this->doQuery($sql, $params);
    }


    public function filter_count($cc, $city, $dir=null, $tipo=null,
        $min_price=0, $max_price=0, $cuartos1=0, $banos1=0,
        $area1=0, $cuartos2=0, $banos2=0, $area2=0, $cat = null,
        $x0=null, $y0=null, $x1=null, $y1=null) {
        
        $params = array();
        
        if(is_numeric($dir)) {
        //$sql = 'SELECT * FROM propiedad WHERE idpropiedad = ? AND estatus = 3';
            $sql = 'SELECT p.*, f.src_medium src, f.src_medium_width width, f.src_medium_height height, f.habilitado FROM propiedad p left join prop_foto_album a on (p.idpropiedad = a.idpropiedad) left join prop_foto f on (a.aid = f.prop_foto_album_aid) WHERE p.idpropiedad = ? AND p.estatus = 3 ';
            $params[] = $dir;
        } else {

            if(is_numeric($x0) && is_numeric($y0) && is_numeric($x1) && is_numeric($y1)) {
                $geom =" GeomFromText('Polygon(($x0 $y0, $x1 $y0, $x1 $y1, $x0 $y1, $x0 $y0))')";
                //$sql = 'SELECT * FROM propiedad p WHERE p.estatus = 3 AND MBRContains('. $geom .', GeomFromWKB(Point(p.lng, p.lat))) ';
                $sql = 'SELECT p.*, f.src_medium src, f.src_medium_width width, f.src_medium_height height, f.habilitado FROM propiedad p left join prop_foto_album a on (p.idpropiedad = a.idpropiedad) left join prop_foto f on (a.aid = f.prop_foto_album_aid) WHERE p.estatus = 3 AND MBRContains('. $geom .', GeomFromWKB(Point(p.lng, p.lat))) ';
            } else {
                if(isset($city) && (strlen(trim($city)) > 0)) {
                    $exp1 = "%$city%";
                    $params[] = $city;
                    $sql = $this->_db->quoteInto("SELECT p.*, f.src_medium src, f.src_medium_width width, f.src_medium_height height, f.habilitado FROM propiedad p left join prop_foto_album a on (p.idpropiedad = a.idpropiedad) left join prop_foto f on (a.aid = f.prop_foto_album_aid) WHERE p.ciudad LIKE ?  AND p.estatus = 3", $exp1);
                } else {
                //$sql = 'SELECT * FROM propiedad p WHERE p.estatus = 3 ';
                    $sql = 'SELECT p.*, f.src_medium src, f.src_medium_width width, f.src_medium_height height, f.habilitado FROM propiedad p left join prop_foto_album a on (p.idpropiedad = a.idpropiedad) left join prop_foto f on (a.aid = f.prop_foto_album_aid) WHERE  p.estatus = 3';
                }
            }

            // tipo_enlistamiento: alquiler, venta
            // tipo_propiedad: categoria -apartemento,casa,edificio,etc.
            /*
            if ( ($venta != 0 ) && ($alquiler == 0)) {
                $sql .= ' AND p.tipo_enlistamiento = 1';
            }
            if (($venta == 0 ) && ($alquiler != 0)) {
                $sql .= ' AND p.tipo_enlistamiento = 2';
            }
            if (($venta != 0 ) && ($alquiler != 0 )) {
                $sql .= ' AND p.tipo_enlistamiento = 3';
            }
             */
            if(isset($tipo)) {
                if($tipo == 1) {
                // alquiler
                    $sql .= ' AND p.tipo_enlistamiento = 2';
                }
                if($tipo == 2) {
                // venta
                    $sql .= ' AND p.tipo_enlistamiento = 1';
                }
            }

            if( isset($cc) && (strlen(trim($cc)) > 0) ) {
                $params[] = $cc;
                $sql .= " AND p.pais = ?";
                $sql = $this->_db->quoteInto($sql, $cc);
            }

            if ( ($min_price >= 0) && ($max_price > 0) ) {
            //$params[] = $min_price;
            //$params[] = $max_price;
                if($tipo == 1) {
                // alquiler
                    $sql .= " AND ( p.precio_alquiler BETWEEN $min_price AND $max_price )";
                }
                if($tipo == 2) {
                // venta
                    $sql .= " AND ( p.precio_venta BETWEEN $min_price AND $max_price )";
                }

            }
            if ( ($cuartos1 >= 0) && ( $cuartos2 > 0) ) {
                $sql .= " AND ( p.cuartos BETWEEN $cuartos1 AND $cuartos2 )";
            }
            if ( ($banos1 >= 0) && ($banos2 > 0) ) {
                $sql .= " AND ( p.banos BETWEEN $banos1 AND $banos2 )";
            }
            if ( ($area1 >= 0) && ($area2 > 0) ) {
                $sql .= " AND ( p.area BETWEEN $area1 AND $area2 )";
            }

            if (isset($cat) && ( strlen(trim($cat)) > 0)) {
                $params[] = $cat;
                $sql .= " AND p.tipo_propiedad = ?";
                $sql = $this->_db->quoteInto($sql, $cat);
            }

        }
        $sql .= ' GROUP BY p.idpropiedad ORDER BY p.creditos DESC ';
        $sql .= " LIMIT $start, $limit";
        return $this->doQuery($sql, $params);
    }

    //@todo, agregar campo, foto_desc = 1 or 0
    // foto de la burbuja en google maps

    public function getDescFoto( $idpropiedad ) {
        $sql = 'SELECT * FROM prop_foto WHERE idpropiedad = ? AND foto_desc = 1';
        $params = array($idpropiedad);
        return $this->doQuery($sql, $params);
    }


    public function getCountryList() {
        $sql = 'SELECT DISTINCT(cn), cc FROM GeoLite_Country ORDER BY 1 ASC;';
        return $this->doQuery($sql);
    }

    public function getCategorias() {
        $sql = 'SELECT idtipo, es FROM prop_categoria';
        return $this->doQuery($sql);
    }

}
