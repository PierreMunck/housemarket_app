<?php
class Dgt_Gallery extends Zend_Db_Table {
    protected $_name = 'prop_foto';
    protected $_primary = 'fid';
    public $db;


    private function doQuery ($sql, $params=null) {
        $this->db = Zend_Registry::get('db');
        if (isset($params)) {
            $rs = $this->db->query($sql,$params);
        } else {
            $rs = $this->db->query($sql);
        }
        $data = $rs->fetchAll();
        return $data;
    }
    public function getImages_propiedad($propId){
        $params = array($propId);
     
        $sql = 'SELECT  prop_foto_album.aid,prop_foto_album.idpropiedad,prop_foto.src
                FROM prop_foto_album
                INNER JOIN prop_foto ON (prop_foto_album.aid = prop_foto.prop_foto_album_aid)
                WHERE (prop_foto_album.idpropiedad  = ? )';
        return $this->doQuery($sql, $params);
     }
	
	public function getImage($id)
	{
		$id = (int)$id;
		$row = $this->fetchRow('fid = ' . $id );
		if(!$row) {
			throw new Exception("No se puede encontrar el registro $id");
		}
		return $row->toArray();
	}
	
	public function insertImage($title, $src, $user)
	{
		$data = array(
			'caption' => $title,
			'src' => $comment,
			'fbUser_id' => $user
		);
		
		$this->insert($data);
	}
	
	public function lastID()
	{
		return $this->_db->lastInsertId();
	}


    //This function separates the extension from the rest of the file name and returns it 
	public function findexts ($filename) 
	{ 
	$filename = strtolower($filename) ; 
	$exts = split("[/\\.]", $filename) ; 
	$n = count($exts)-1; 
	$exts = $exts[$n]; 
	return $exts; 
	} 


	public function FindImageById($id)
	{
		$dir = '/propiedad';
		$images = scandir($dir);
		foreach($images as $image)
		{
			$path_parts = pathinfo($dir.'/'.$image);
			if( $path_parts['filename'] == $id )
			{
				$found = $image;
			}
		}
		return $found;
	}


}