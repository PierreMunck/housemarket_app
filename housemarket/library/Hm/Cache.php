<?php

require_once 'Zend/Cache.php';

/**
 * Sistema de Cache
 */
class HmCache  {

	protected $pageCache;
	
	protected $language;
	
	protected $uri;
	
	protected $get;
	
	protected $post;
	
	protected $user;
	
	protected $config = array(
					'/' => array('language','userType'), //home structure
					'/roommates/busquedacuarto' => array('language','userType'), //home cuarto
					'/index/estatetogm' => array('post','userType'),
					'/index/brokersByArea' => array('post','userType'),
					'/index/fotosfiltradas' => array('post','userType'),
					'/index/centermap' => array('get','userType'),
					'/enlistar/perfil' => array('get','userType'),
					'propiedades/show' => array('get','userType'),
					'/destacada' => array('language','userType'),
					'/mihousebook/publicacion' => array('language','userType','signed'),
					'/roommates/elegirAnuncio' => array('language'),
					'/roommates/elegirAnuncio' => array('language'),
					// error de cache cuando cambia de pagina broker
					'/fbml/tab' => array('language','userType','post',),
			);
	
    /**
     * Inicializacion del controller
     */
	function __construct() {
		$this->log = Zend_Registry::get('log');
    	// Zend_Cache_Frontend_Page
    	$this->pageCache = Zend_Cache::factory(
    			'Page',
    			'File',
    			array(
    					'lifetime'=>86400, //24h
    					'ignore_user_abort' => true,
    					// enable debug only on localhost
    					//'debug_header' => APPLICATION_ENV == 'development',
    					'default_options' => array(
    							'cache_with_post_variables'    => true,
    							'cache_with_session_variables'    => true,
    							'cache_with_cookie_variables'    => true,
    							'make_id_with_get_variables'  => true,
    					),
    			),
    			array(
    					'cache_dir' => realpath(dirname(__FILE__) . '/../../application/data/pagecache'),
    					'hashed_directory_level '=> 2,
    			)
    	);
    	$this->user = Zend_Registry::get('user_info');
    }

    public function start() {
    	$pos = strpos($_SERVER['REQUEST_URI'], '?');
    	$this->uri = $_SERVER['REQUEST_URI'];
    	if ($pos !== false) {
    		$this->uri = substr($_SERVER['REQUEST_URI'],0,$pos);
    	}
    	$this->log->info($_SERVER['REQUEST_URI']);
    	
    	$this->get = $_GET;
    	$this->post = $_POST;
    	$this->log->info($this->uri);
    	$this->language = Zend_Registry::get('language');
    	if($this->isCacheble()){
    		$this->log->info('en cache :'.$this->getId());
    		$this->pageCache->start($this->getId());
    	}
    }
    
    private function isCacheble(){
    	if(in_array($this->uri, $this->getCacheUri())){
    		return true;
    	}
    	return false;
    }
    
    private function getCacheRules(){
    	return $this->config[$this->uri];
    }
    
    private function getCacheUri(){
    	return array_keys($this->config); 
    }
    /**
     * generate an id with URI and Language
     */
    private function getId(){
    	$id = $this->uri;
    	$rules = $this->getCacheRules();
    	foreach ($rules as $rule){
    		switch($rule){
    			case 'get' :
    				foreach ($this->get as $key => $value){
    					$id .= '|'. $key .'='. $value;
    				}
    				break;
    			case 'post' :
    				foreach ($this->post as $key => $value){
    					if($key != 'signed_request'){
    						$id .= '|'. $key .'='. $value;
    					}
    				}
    				break;
    			case 'userType':
    				if(isset($this->user['type'])){
    					$id .= '|'. $this->user['type'];
    				}
    				break;
    			case 'signed':
    				$signed = Zend_Registry::get('signed');
    				if(isset($signed['oauth_token'])){
    					$id .= '|'. $signed['oauth_token'];
    				}
    				break;
    			case 'language':
    				$id .= '|'. $this->language;
    			case 'none':
    			default:
    				break;
    		}
    	}
    	
    	
    	return md5($id);
    }
    
    public function clearall(){
    	$this->pageCache->clean(Zend_Cache::CLEANING_MODE_ALL);
    }
    
    public function clear($url,$param){
    	$this->uri = $url;
    	if(isset($param['get'])){
    		$this->get = $param['get'];
    	}
    	if(isset($param['post'])){
    		$this->post = $param['post'];
    	}
    	if(isset($param['userType'])){
    		$this->userType = $param['userType'];
    	}
    	$this->pageCache->remove($this->getId());
    }
}

?>