<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Utiles
 *
 * @author Irving Medina
 */
class Utiles {

    //put your code here
    public static function verifySigned(&$facebook) {
        $fbsig = array();
        $fbparams = array();

        /*
         * Generamos la signature con los parametros GET recibidos usando 
         * el facebook secret key, para luego validarlo contra el parametros fb_sig
         * 
         */
        
        foreach ($_GET as $key => $value) {
            if (substr($key, 0, 7) == 'fb_sig_') {
                $fbsig[substr($key, 7)] = $facebook->no_magic_quotes($value);
                //$this->_logger->info('Bootstrap ' . __METHOD__ . ' ' .substr($key,7) . '= ' . $fbsig[substr($key,7)]);
            }
            if (substr($key, 0, 6) == 'fb_sig') {
                $fbparams[$key] = $facebook->no_magic_quotes($value);
            }
        }
        $string = '';
        ksort($fbsig);

        foreach ($fbsig as $k => $v) {
            $string .= "$k=$v";
        }
        $string .= $facebook_secret;
        $check_sig = (md5($string) == $_GET['fb_sig']) ? true : false;
//Si los valores GET no validan la llave se busca en POST
        if (!$check_sig) {
            foreach ($_POST as $key => $value) {
                if (substr($key, 0, 7) == 'fb_sig_') {
                    $fbsig[substr($key, 7)] = $facebook->no_magic_quotes($value);
                }
                if (substr($key, 0, 6) == 'fb_sig') {
                    $fbparams[$key] = $facebook->no_magic_quotes($value);
                }
            }
            $string = '';
            ksort($fbsig);

            foreach ($fbsig as $k => $v) {
                $string .= "$k=$v";
            }
            $string .= $facebook_secret;
            $check_sig = (md5($string) == $_POST['fb_sig']) ? true : false;
        }
        /* fin generacion y chequeo del signature */
        return $check_sig;
    }

}

?>
