<?php

require_once 'Zend/Session/Namespace.php';
require_once 'Zend/Session.php';

/**
 * Sistema de Cache
 */
class HmSession  {

	protected $UserSession;
	protected $AppSession;
	
    /**
     * Inicializacion del controller
     */
	function __construct() {
    	
    }

    /**
     * starting session
     */
    public function start() {
    	Zend_Session::start();
    	
    	$this->UserSession = new Zend_Session_Namespace('HmUserSession');
    	$this->AppSession = new Zend_Session_Namespace('HmAppSession');
    	
    	$this->UserSession->setExpirationSeconds(60);
    	$this->AppSession->setExpirationSeconds(86400);
    }
    
    /**
     * config session
     * @param unknown_type $cfg
     */
    public function config($cfg) {
    	// Create our Application instance
    	if(!isset($this->AppSession->facebook)){
    		$this->AppSession->facebook = new Facebook(array('appId' => $cfg->facebook_appid, 'secret' => $cfg->facebook_secret, 'cookie' => true));
    	}
    	
    	// Get User ID
    	if(!isset($this->UserSession->uid)){
    		$this->UserSession->uid =  $this->AppSession->facebook->getUser();
    	}
    	
    	//Si es la sesion de un fanpage
    	if(!isset($this->UserSession->signed)){
    		$this->UserSession->signed =  $this->AppSession->facebook->getSignedRequest();
    	}
    }
    
    /**
     * get information demtro de la session
     */
    public function get($type,$var) {
    	if($type == 'user'){
    		$type = 'UserSession';
    	}
    	if($type == 'application'){
    		$type = 'AppSession';
    	}
    	
    	return $this->$type->$var;
    }
    
    public function set($type,$var,$value) {
    	if($type == 'user'){
    		$type = 'UserSession';
    	}
    	if($type == 'application'){
    		$type = 'AppSession';
    	}
    
    	$this->$type->$var = $value;
    }
}

?>