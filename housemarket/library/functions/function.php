<?php

function debug($var, $exit=true) {
    echo "<pre>";
    print_r($var);
    echo "</pre>";
    if ($exit)
        exit();
}
function tostr($string){
    return (string)$string;
}

if ( false === function_exists('lcfirst') ){
	function lcfirst( $str ){
		return (string)(strtolower(substr($str,0,1)).substr($str,1));
	}
}

function google_seo_string($string, $separator = '-') {
    $string = trim($string);
    //$string = strtolower($string); // convert to lowercase text
    // Recommendation URL: http://www.webcheatsheet.com/php/regular_expressions.php
    // Only space, letters, numbers and underscore are allowed
    //$string = trim(ereg_replace("[^ A-Za-z0-9_]", " ", $string));
    /*"t" (ASCII 9 (0x09)), a tab.
      "n" (ASCII 10 (0x0A)), a new line (line feed).
      "r" (ASCII 13 (0x0D)), a carriage return.*/
    //$string = ereg_replace("[ tnr]+", "-", $string);
    $string = str_replace(" ", $separator, $string);
    //$string = ereg_replace("[ -]+", "-", $string);
    return $string;
}

function getUrlAmigable($string) {

    $spacer = "-";
    $string = trim($string);
    //$string = strtolower($string);
    //$string = trim(ereg_replace("[^ A-Za-z0-9_]", " ", $string)); 
    //$string = ereg_replace("[ \t\n\r]+", "-", $string);
    $string = str_replace(" ", $spacer, $string);
    $string = str_replace(' ', '-', $string);
    return $string;
}

function parse_query($val) {
    $base = parse_url($val);
    $var = html_entity_decode($base['query']);
    $var = explode('&', $var);
    $arr = array();

    foreach ($var as $val) {
        $x = explode('=', $val);
        $arr[$x[0]] = $x[1];
    }
    unset($val, $x, $var);
    $base['query'] = $arr;
    return $base;
}

//Correccion de http:// para las url
function formatUrl($url) {
    $url = trim($url);
    if (strpos($url, "http://") === 0) {
        return $url;
    }
    if (strpos($url, "http:/") === 0) {
        return str_replace("http:/", "http://", substr($url, 0, 6)) . substr($url, 6);
    }
    if (strpos($url, "http:") === 0) {
        return str_replace("http:", "http://", substr($url, 0, 5)) . substr($url, 5);
    }
    if (!empty($url))
        return "http://" . $url;

    return $url;
}
?>
