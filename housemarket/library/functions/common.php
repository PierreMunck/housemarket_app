<?php

/**
 * @file
 * Common public static functions that may be used by the application.
 *
 * The public static functions that are critical and need to be available even when serving
 * a cached page are instead located in Bootstrap.php.
 */
class HMUtils {
    
    /**
     * @var Array Stores the facebook id of the application's developers
     */
    public static $developers_uid = array(
        '725948997', // Mauricio Dinarte
        '1240742778', // Scott Avilés
    );
    
    /**
     * Encode special characters in a plain-text string for display as HTML.
     *
     * Also validates strings as UTF-8 to prevent cross site scripting attacks on
     * Internet Explorer 6.
     *
     * @param $text
     *   The text to be checked or processed.
     *
     * @return
     *   An HTML safe version of $text, or an empty string if $text is not
     *   valid UTF-8.
     *
     * @see drupal_validate_utf8()
     * @ingroup sanitization
     */
    public static function check_plain($text) {
        return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Strips dangerous protocols (e.g. 'javascript:') from a URI.
     *
     * This public static function must be called for all URIs within user-entered input prior
     * to being output to an HTML attribute value. It is often called as part of
     * check_url() or filter_xss(), but those public static functions return an HTML-encoded
     * string, so this public static function can be called independently when the output needs to
     * be a plain-text string for passing to t(), l(), drupal_attributes(), or
     * another public static function that will call check_plain() separately.
     *
     * @param $uri
     *   A plain-text URI that might contain dangerous protocols.
     *
     * @return
     *   A plain-text URI stripped of dangerous protocols. As with all plain-text
     *   strings, this return value must not be output to an HTML page without
     *   check_plain() being called on it. However, it can be passed to public static functions
     *   expecting plain-text strings.
     *
     * @see check_url()
     */
    public static function drupal_strip_dangerous_protocols($uri) {
        static $allowed_protocols;

        if (!isset($allowed_protocols)) {
            $allowed_protocols = array_flip(array('ftp', 'http', 'https', 'irc', 'mailto', 'news', 'nntp', 'rtsp', 'sftp', 'ssh', 'tel', 'telnet', 'webcal'));
        }

        // Iteratively remove any invalid protocol found.
        do {
            $before = $uri;
            $colonpos = strpos($uri, ':');
            if ($colonpos > 0) {
                // We found a colon, possibly a protocol. Verify.
                $protocol = substr($uri, 0, $colonpos);
                // If a colon is preceded by a slash, question mark or hash, it cannot
                // possibly be part of the URL scheme. This must be a relative URL, which
                // inherits the (safe) protocol of the base document.
                if (preg_match('![/?#]!', $protocol)) {
                    break;
                }
                // Check if this is a disallowed protocol. Per RFC2616, section 3.2.3
                // (URI Comparison) scheme comparison must be case-insensitive.
                if (!isset($allowed_protocols[strtolower($protocol)])) {
                    $uri = substr($uri, $colonpos + 1);
                }
            }
        } while ($before != $uri);

        return $uri;
    }

    /**
     * Strips dangerous protocols (e.g. 'javascript:') from a URI and encodes it for output to an HTML attribute value.
     *
     * @param $uri
     *   A plain-text URI that might contain dangerous protocols.
     *
     * @return
     *   A URI stripped of dangerous protocols and encoded for output to an HTML
     *   attribute value. Because it is already encoded, it should not be set as a
     *   value within a $attributes array passed to drupal_attributes(), because
     *   drupal_attributes() expects those values to be plain-text strings. To pass
     *   a filtered URI to drupal_attributes(), call
     *   drupal_strip_dangerous_protocols() instead.
     *
     * @see drupal_strip_dangerous_protocols()
     */
    public static function check_url($uri) {
        return self::check_plain(self::drupal_strip_dangerous_protocols($uri));
    }

    /**
     * Converts an associative array to an attribute string for use in XML/HTML tags.
     *
     * Each array key and its value will be formatted into an attribute string.
     * If a value is itself an array, then its elements are concatenated to a single
     * space-delimited string (for example, a class attribute with multiple values).
     *
     * Attribute values are sanitized by running them through check_plain().
     * Attribute names are not automatically sanitized. When using user-supplied
     * attribute names, it is strongly recommended to allow only white-listed names,
     * since certain attributes carry security risks and can be abused.
     *
     * Examples of security aspects when using drupal_attributes:
     * @code
     *   // By running the value in the following statement through check_plain,
     *   // the malicious script is neutralized.
     *   drupal_attributes(array('title' => t('<script>steal_cookie();</script>')));
     *
     *   // The statement below demonstrates dangerous use of drupal_attributes, and
     *   // will return an onmouseout attribute with JavaScript code that, when used
     *   // as attribute in a tag, will cause users to be redirected to another site.
     *   //
     *   // In this case, the 'onmouseout' attribute should not be whitelisted --
     *   // you don't want users to have the ability to add this attribute or others
     *   // that take JavaScript commands.
     *   drupal_attributes(array('onmouseout' => 'window.location="http://malicious.com/";')));
     * @endcode
     *
     * @param $attributes
     *   An associative array of key-value pairs to be converted to attributes.
     *
     * @return
     *   A string ready for insertion in a tag (starts with a space).
     *
     * @ingroup sanitization
     */
    public static function drupal_attributes(array $attributes = array()) {
        foreach ($attributes as $attribute => &$data) {
            $data = implode(' ', (array) $data);
            $data = $attribute . '="' . self::check_plain($data) . '"';
        }
        return $attributes ? ' ' . implode(' ', $attributes) : '';
    }

    /**
     * Wraps a HTML tag around some content. NOTE that for valid markup do not use
     * public static function for INLINE tags.
     *
     * @param $content String
     *   A string to be wrapped with a HTML tag.
     *
     * @param $tag String
     *   The HTML tag used as wrapper.
     *
     * @param $escapeContent Boolean
     *   Whether or not special characters should be encoded in a plain-text string for display as HTML.
     *
     * @param $htmlId String
     *   HTML id attribite to apply.
     *
     * @param $htmlClasses Array
     *   HTML class attribite to apply.
     * 
     * @param $htmlClasses Array
     *   More HTML attribite to apply.
     * 
     * @return String
     *   A string of HTML enclosed withing the specified tag, and optionally escaped.
     */
    public static function wrapInHTMLTag($content, $tag, $escapeContent = FALSE, $htmlId = '', array $htmlClasses = array(), array $htmlAttributes = array()) {
		
        $html = '<' . $tag;

        if (!empty($htmlId)) {
            $html .= ' id="' . $htmlId . '"';
        }

        if (!empty($htmlClasses)) {
            $html .= ' class="' . implode(' ', $htmlClasses) . '"';
        }

        if (!empty($htmlAttributes)) {

            // if $htmlId provided, do not allow double 'id' attribute
            if (!empty($htmlId) && !empty($htmlAttributes['id'])) {
                unset($htmlAttributes['id']);
            }

            // if $htmlClasses provided, do not allow double 'class' attribute
            if (!empty($htmlClasses) && !empty($htmlAttributes['class'])) {
                unset($htmlAttributes['class']);
            }

            $html .= self::drupal_attributes($htmlAttributes);
        }

        $html .= '>';

        if ($escapeContent === TRUE) {
            $html .= self::check_plain($content);
        } else {
            $html .= $content;
        }

        $html .= '</' . $tag . '>';

        return $html;
    }

    /**
     * Builds a really simple HTML table. Headers can be positioned horizontally
     * or vertically. It is posible to scape headers as well as table data.
     * Important: Apart from numbers of data item in a row matching the total
     * number of headers, no other validation is done so use with care. If this
     * validation fails, the whole row will be ommited and will not be rendered.
     *
     * @param $headers Array
     *   Set of Strings containing the table headers.
     *
     * @param $records Array
     *   Set of Arrays, each of which contains the data to be used for one row.
     *
     * @param $headerOrientation String
     *   Indicates whether the headers should be positioned horizontally or
     *   vertically. Possible values are 'horizontal' and 'vertical', respectively.
     *
     * @param $htmlId String
     *   HTML id attribite to apply.
     *
     * @param $htmlClasses Array
     *   HTML class attribite to apply.
     * 
     * @param $htmlClasses Array
     *   More HTML attribite to apply.
     * 
     * @return String
     *   A string of HTML enclosed withing the specified tag, and optionally escaped.
     *
     * @param $escapeHeaders Boolean
     *   Whether or not headers' special characters should be encoded in a plain-text string for display as HTML.
     *
     * @param $escapeRecords Boolean
     *   Whether or not records' special characters should be encoded in a plain-text string for display as HTML.
     */
    public static function build_simple_html_table($headers = array(), $records = array(), $headerOrientation = 'horizontal', $htmlId = '', array $htmlClasses = array(), array $htmlAttributes = array(), $escapeHeaders = FALSE, $escapeRecords = FALSE) {

        $headersCount = count($headers);
        $recordsCount = count($records);
        
        $html = '<table';

        if (!empty($htmlId)) {
            $html .= ' id="' . $htmlId . '"';
        }

        if (!empty($htmlClasses)) {
            $html .= ' class="' . implode(' ', $htmlClasses) . '"';
        }

        if (!empty($htmlAttributes)) {

            // if $htmlId provided, do not allow double 'id' attribute
            if (!empty($htmlId) && !empty($htmlAttributes['id'])) {
                unset($htmlAttributes['id']);
            }

            // if $htmlClasses provided, do not allow double 'class' attribute
            if (!empty($htmlClasses) && !empty($htmlAttributes['class'])) {
                unset($htmlAttributes['class']);
            }

            $html .= self::drupal_attributes($htmlAttributes);
        }

        $html .= '>';
        
        if($escapeHeaders === true) {
            foreach ($headers as &$header) {
                $header = self::check_plain($header);
            }
        }
        
        // records should be a two-level array
        if($escapeRecords === true) {
            foreach ($records as $key => &$record) {
                if(count($record) !== $headersCount) {
                    unset($escapeHeaders[$key]);
                    continue;
                }
                
                foreach ($record as &$tableData) {
                    $tableData = self::check_plain($tableData);
                }
            }
        }
        
        switch ($headerOrientation) {
            case 'horizontal':
                $tableInnerHTMLHeaders = '';
                $rowCounter = 0;
        
                foreach ($headers as $header) {
                    $tableInnerHTMLHeaders .= '<th>' . $header . '</th>';
                }
                $html .= self::wrapInHTMLTag($tableInnerHTMLHeaders, 'tr');
                
                foreach ($records as $record) {
                    $tableInnerHTMLRecords = '';
                    $metadata = array();
                    $rowCounter++;
                    
                    $metadata[] = $rowCounter % 2 === 0 ? 'even' : 'odd';
                    if( $rowCounter === 1 ) {
                        $metadata[] = 'leaf';
                        $metadata[] = 'first';
                    }
                    if( $rowCounter === $recordsCount ) {
                        $metadata[] = 'leaf';
                        $metadata[] = 'last';
                    }
                    
                    foreach ($record as $tableData) {
                        $tableInnerHTMLRecords .= '<td>' . $tableData . '</td>';
                    }
                    $html .= self::wrapInHTMLTag($tableInnerHTMLRecords, 'tr', false, '', $metadata);
                }
                break;
            
            case 'vertical': // not properly tested
                for( $i = 0; $i < $headersCount; $i++ ) {
                    $metadata = array();
                    $tableInnerHTMLRow = '';
                    
                    $metadata[] = $i % 2 === 0 ? 'even' : 'odd';
                    if( $i === 1 ) {
                        $metadata[] = 'leaf';
                        $metadata[] = 'first';
                    }
                    if( $i === $recordsCount ) {
                        $metadata[] = 'leaf';
                        $metadata[] = 'last';
                    }
                    
                    $tableInnerHTMLRow = '<th>' . $headers[$i] . '</th>';
                    
                    foreach ($records as $record) {
                        foreach ($record as $tableData) {
                            $tableInnerHTMLRow .= '<td>' . $tableData . '</td>';
                        }
                    }
                    
                    $html .= self::wrapInHTMLTag($tableInnerHTMLRow, 'tr', false, '', $metadata);
                }
                break;
        }

        $html .= '</table>';

        return $html;
    }

    /**
     * Creates a simple associative array from an object.
     *
     * @param $object
     *   Object to process.
     * 
     * @param $keyProperty
     *   Object's property to use as key for the associative array.
     * 
     * @param $valueProperty
     *   Object's property to use as value for the associative array.
     * 
     * @param $uppercaseKey
     *   Process the key property to make it uppercase.
     *
     * @return
     *   An associative array whose keys corresponds to the passed $object's $keyProperty
     *   and values corresponds to the passed $object's $valueProperty.
     *
     * @see drupal_strip_dangerous_protocols()
     */
    public static function build_simple_map_array_from_object($object, $keyProperty, $valueProperty, $uppercaseKey = false) {
        $_mapArray = array();
        foreach ($object as $record) {
            if ($uppercaseKey) {
                $record->$keyProperty = strtoupper($record->$keyProperty);
            }

            $_mapArray[$record->$keyProperty] = $record->$valueProperty;
        }
        return $_mapArray;
    }

    /**
     * Creates a simple associative array from an array.
     *
     * @param $array
     *   Array to process.
     * 
     * @param $keyProperty
     *   Object's property to use as key for the associative array.
     * 
     * @param $valueProperty
     *   Object's property to use as value for the associative array.
     * 
     * @param $uppercaseKey
     *   Process the key property to make it uppercase.
     *
     * @return
     *   An associative array whose keys corresponds to the passed $object's $keyProperty
     *   and values corresponds to the passed $object's $valueProperty.
     *
     * @see drupal_strip_dangerous_protocols()
     */
    public static function build_simple_map_array_from_array(array $array, $keyProperty, $valueProperty, $uppercaseKey = false) {
        $_mapArray = array();
        foreach ($array as $record) {
            if ($uppercaseKey) {
                $record[$keyProperty] = strtoupper($record[$keyProperty]);
            }

            $_mapArray[$record[$keyProperty]] = $record[$valueProperty];
        }
        return $_mapArray;
    }
    
    /**
     * Creates a simple indexed array from an object.
     *
     * @param $object
     *   Object to process.
     * 
     * @param $valueProperty
     *   Object's property to use as value for the associative array.
     * 
     * @param $uppercaseKey
     *   Process the key property to make it uppercase.
     *
     * @return
     *   An indexed array values corresponds to the passed $object's $valueProperty.
     *
     * @see drupal_strip_dangerous_protocols()
     */
    public static function build_simple_indexed_array_from_object($object, $valueProperty, $uppercaseValue = false) {
        $_mapArray = array();
        foreach ($object as $record) {
            if ($uppercaseValue) {
                $record->$valueProperty = strtoupper($record->$valueProperty);
            }

            $_mapArray[] = $record->$valueProperty;
        }
        return $_mapArray;
    }

    /**
     * Creates a simple indexed array from an object.
     *
     * @param $object
     *   Object to process.
     * 
     * @param $valueProperty
     *   Object's property to use as value for the associative array.
     * 
     * @param $uppercaseKey
     *   Process the key property to make it uppercase.
     *
     * @return
     *   An indexed array values corresponds to the passed $object's $valueProperty.
     *
     * @see drupal_strip_dangerous_protocols()
     */
    public static function build_simple_indexed_array_from_array(array $array, $valueProperty, $uppercaseValue = false) {
        $_mapArray = array();
        foreach ($array as $record) {
            if ($uppercaseValue) {
            	if(is_object($record)){
            		$record[$valueProperty] = strtoupper($record->valueProperty);	
            	}
				if(is_array($record)){
					$record[$valueProperty] = strtoupper($record[$valueProperty]);
				}
            }
			if(is_object($record)){
				$_mapArray[] = $record->valueProperty;	
        	}
			if(is_array($record)){
				$_mapArray[] = $record[$valueProperty];
			}
            
        }
        return $_mapArray;
    }

    /**
     * Removes white spaces at the begenning and end of every element in an array.
     *
     * @param Array $arrayToTrimValues
     *   Array whose values are going to be trimmed. Passed by reference.
     */
    public static function trim_array_values(array &$arrayToTrimValues) {
        // eliminar espacios al inicio o final de cada valor
        foreach ($arrayToTrimValues as $key => $value) {
            $arrayToTrimValues[$key] = trim($arrayToTrimValues[$key]);
        }
    }

    /**
     * Indica si la variable pasado es un número. Adicinalmente se puede
     * especificar un operador y un valor para verificar que la variable pasada
     * cumpla alguna condición especial. Por ejemplo, si se pasan los valores
     * (3, 0, '>') se verificará si la variable pasada (3) es un número, y además si
     * es mayor que (operador) el valor indicado (0).
     *
     * @param $variable Variable a evaluar
     * @param $valor $valor Valor a utilizar para realizar evaluar la condición especial.
     * @param String $operador Operación especual a realizar.
     * @return
     *   TRUE si el valor es numérico y cumple la condición especial de estar presente,
     *   FALSE si el valor no es numérico o cumple la condición especial de estar presente,
     *   o una cadena de error si el $operador es inválido. *
     */
    public static function es_numero($variable, $valor = NULL, $operador = NULL) {
        if ($valor === NULL || $operador === NULL) {
            return is_numeric($variable);
        } else {
            switch ($operador) {
                case 'in':
                    return (is_numeric($variable) && in_array($variable, $valor));
                    break;

                case '>':
                    return (is_numeric($variable) && $variable > $valor);
                    break;

                case '<':
                    return (is_numeric($variable) && $variable < $valor);
                    break;

                case '>=':
                    return (is_numeric($variable) && $variable >= $valor);
                    break;

                case '<':
                    return (is_numeric($variable) && $variable <= $valor);
                    break;

                case '===':
                    return (is_numeric($variable) && $variable === $valor);
                    break;

                case '!==':
                    return (is_numeric($variable) && $variable !== $valor);
                    break;

                case 'inclusive range':
                    return (is_numeric($variable) && $variable >= $valor['min'] && $variable <= $valor['max']);
                    break;

                case 'exclusive range':
                    return (is_numeric($variable) && $variable > $valor['min'] && $variable < $valor['max']);
                    break;
            }
        }

        return 'ERROR: $operador inválido.';
    }

    /**
     * Indica si la variable pasado es un número entero. Adicinalmente se puede
     * especificar un operador y un valor para verificar que la variable pasada
     * cumpla alguna condición especial. Por ejemplo, si se pasan los valores
     * (3, 0, '>') se verificará si la variable pasada (3) es un número, y además si
     * es mayor que (operador) el valor indicado (0).
     *
     * @param $variable Variable a evaluar
     * @param $valor $valor Valor a utilizar para realizar evaluar la condición especial.
     * @param String $operador Operación especual a realizar.
     * @return
     *   TRUE si el valor es un número entero y cumple la condición especial de estar presente,
     *   FALSE si el valor no es un número entero o cumple la condición especial de estar presente,
     *   o una cadena de error si el $operador es inválido. *
     */
    public static function es_numero_entero($variable, $valor = NULL, $operador = NULL) {
        if ($valor === NULL || $operador === NULL) {
            return is_int($variable);
        } else {
            switch ($operador) {
                case 'in':
                    return (is_int($variable) && in_array($variable, $valor));
                    break;

                case '>':
                    return (is_int($variable) && $variable > $valor);
                    break;

                case '<':
                    return (is_int($variable) && $variable < $valor);
                    break;

                case '>=':
                    return (is_int($variable) && $variable >= $valor);
                    break;

                case '<':
                    return (is_int($variable) && $variable <= $valor);
                    break;

                case '===':
                    return (is_int($variable) && $variable === $valor);
                    break;

                case '!==':
                    return (is_int($variable) && $variable !== $valor);
                    break;

                case 'inclusive range':
                    return (is_int($variable) && $variable >= $valor['min'] && $variable <= $valor['max']);
                    break;

                case 'exclusive range':
                    return (is_int($variable) && $variable > $valor['min'] && $variable < $valor['max']);
                    break;
            }
        }

        return 'ERROR: $operador inválida.';
    }

    /**
     * Indica si la variable pasado es un número flotante. Adicinalmente se puede
     * especificar un operador y un valor para verificar que la variable pasada
     * cumpla alguna condición especial. Por ejemplo, si se pasan los valores
     * (3.0, 0, '>') se verificará si la variable pasada (3.0) es un número, y además si
     * es mayor que (operador) el valor indicado (0).
     *
     * @param $variable Variable a evaluar
     * @param $valor $valor Valor a utilizar para realizar evaluar la condición especial.
     * @param String $operador Operación especual a realizar.
     * @return
     *   TRUE si el valor es un número flotante y cumple la condición especial de estar presente,
     *   FALSE si el valor no es un número flotante o cumple la condición especial de estar presente,
     *   o una cadena de error si el $operador es inválido. *
     */
    public static function es_numero_flotante($variable, $valor = NULL, $operador = NULL) {
        if ($valor === NULL || $operador === NULL) {
            return is_float($variable);
        } else {
            switch ($operador) {
                case 'in':
                    return (is_float($variable) && in_array($variable, $valor));
                    break;

                case '>':
                    return (is_float($variable) && $variable > $valor);
                    break;

                case '<':
                    return (is_float($variable) && $variable < $valor);
                    break;

                case '>=':
                    return (is_float($variable) && $variable >= $valor);
                    break;

                case '<':
                    return (is_float($variable) && $variable <= $valor);
                    break;

                case '===':
                    return (is_float($variable) && $variable === $valor);
                    break;

                case '!==':
                    return (is_float($variable) && $variable !== $valor);
                    break;

                case 'inclusive range':
                    return (is_float($variable) && $variable >= $valor['min'] && $variable <= $valor['max']);
                    break;

                case 'exclusive range':
                    return (is_float($variable) && $variable > $valor['min'] && $variable < $valor['max']);
                    break;
            }
        }

        return 'ERROR: $operador inválido.';
    }

    /**
     * Indica si la variable pasado es una cadena. Adicinalmente se puede
     * especificar un operador y un valor para verificar que la variable pasada
     * cumpla alguna condición especial. Por ejemplo, si se pasan los valores
     * ('si', array('si', 'no', 'tal vez'), 'in') se verificará si la variable pasada ('si') es una cadena, y además si
     * es está presente ('in') en el arreglo indicado (array('si', 'no', 'tal vez')).
     *
     * @param $variable Variable a evaluar
     * @param $valor $valor Valor a utilizar para realizar evaluar la condición especial.
     * @param String $operador Operación especual a realizar.
     * @return
     *   TRUE si el valor es una cadena y cumple la condición especial de estar presente,
     *   FALSE si el valor no es una cadena o cumple la condición especial de estar presente,
     *   o una cadena de error si el $operador es inválido. *
     */
    public static function es_cadena($variable, $valor = NULL, $operador = NULL) {
        if ($valor === NULL || $operador === NULL) {
            return is_string($variable);
        } else {

            switch ($operador) {
                case 'in':
                    return (is_string($variable) && in_array($variable, $valor));
                    break;

                case '===':
                    return (is_string($variable) && ($variable === $valor));
                    break;


                case '!==':
                    return (is_string($variable) && ($variable !== $valor));
                    break;
            }
        }

        return 'ERROR: $operador inválido.';
    }

    /**
     * Indica si la variable pasado es un caracter. Adicinalmente se puede
     * especificar un operador y un valor para verificar que la variable pasada
     * cumpla alguna condición especial. Por ejemplo, si se pasan los valores
     * ('Y', array('Y', 'N'), 'in') se verificará si la variable pasada ('Y') es una cadena, y además si
     * es está presente ('in') en el arreglo indicado (array('Y', 'N')).
     *
     * @param $variable Variable a evaluar
     * @param $valor $valor Valor a utilizar para realizar evaluar la condición especial.
     * @param String $operador Operación especual a realizar.
     * @return
     *   TRUE si el valor es un caracter y cumple la condición especial de estar presente,
     *   FALSE si el valor no es un caracter o cumple la condición especial de estar presente,
     *   o una cadena de error si el $operador es inválido. *
     */
    public static function es_caracter($variable, $valor = NULL, $operador = NULL) {
        if ($valor === NULL || $operador === NULL) {
            return (is_string($variable) && strlen($variable) === 1);
        } else {

            switch ($operador) {
                case 'in':
                    return (is_string($variable) && strlen($variable) === 1 && in_array($variable, $valor));
                    break;

                case '===':
                    return (is_string($variable) && strlen($variable) === 1 && ($variable === $valor));
                    break;

                case '!==':
                    return (is_string($variable) && strlen($variable) === 1 && ($variable !== $valor));
                    break;
            }
        }

        return 'ERROR: $operador inválido.';
    }

    /**
     * Indica si el párametro pasado es un caracter.
     *
     * @param $variable Variable a evaluar
     * @return
     *   TRUE si el valor es un caracter,
     *   FALSE si no lo es.
     */
    public static function es_booleano($variable) {
        return is_bool($variable);
    }

}