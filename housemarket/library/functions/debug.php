<?php

/**
 * @file
 * Debug functions.
 *
 * Contains some debug utility functions.
 */

/**
 * Set a message which reflects the status of the performed operation.
 *
 * If the function is called with no arguments, this function returns all set
 * messages without clearing them.
 *
 * @param $message
 *   The message to be displayed to the user. For consistency with other
 *   messages, it should begin with a capital letter and end with a period.
 * @param $type
 *   The type of the message. One of the following values are possible:
 *   - 'status'
 *   - 'warning'
 *   - 'error'
 * @param $repeat
 *   If this is FALSE and the message is already set, then the message won't
 *   be repeated.
 */
require_once 'krumo/class.krumo.php';

function z_set_message($message = NULL, $type = 'status', $repeat = TRUE) {
    if ($message) {
    	$log = Zend_Registry::get('log');
    	$log->info($message);
        if (!isset($_SESSION['messages'][$type])) {
            $_SESSION['messages'][$type] = array();
        }

        if ($repeat || !in_array($message, $_SESSION['messages'][$type])) {
            $_SESSION['messages'][$type][] = $message;
        }
    }

    return isset($_SESSION['messages']) ? $_SESSION['messages'] : NULL;
}

/**
 * Return all messages that have been set.
 *
 * @param $type
 *   (optional) Only return messages of this type.
 * @param $clear_queue
 *   (optional) Set to FALSE if you do not want to clear the messages queue
 * @return
 *   An associative array, the key is the message type, the value an array
 *   of messages. If the $type parameter is passed, you get only that type,
 *   or an empty array if there are no such messages. If $type is not passed,
 *   all message types are returned, or an empty array if none exist.
 */
function z_get_messages($type = NULL, $clear_queue = TRUE) {
    if ($messages = z_set_message()) {
        if ($type) {
            if ($clear_queue) {
                unset($_SESSION['messages'][$type]);
            }
            if (isset($messages[$type])) {
                return array($type => $messages[$type]);
            }
        } else {
            if ($clear_queue) {
                unset($_SESSION['messages']);
            }
            return $messages;
        }
    }
    return array();
}

/**
 * Returns HTML for status and/or error messages, grouped by type.
 *
 * An invisible heading identifies the messages for assistive technology.
 * Sighted users see a colored box. See http://www.w3.org/TR/WCAG-TECHS/H69.html
 * for info.
 *
 * @param $variables
 *   An associative array containing:
 *   - display: (optional) Set to 'status' or 'error' to display only messages
 *     of that type.
 */
function html_status_messages($variables) {
    $display = $variables['display'];
    $output = '';

    $status_heading = array(
        'status' => 'Status message',
        'error' => 'Error message',
        'warning' => 'Warning message',
    );

    foreach (z_get_messages($display) as $type => $messages) {
        $output .= "<div class=\"messages $type\">\n";
        if (!empty($status_heading[$type])) {
            $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
        }
        if (count($messages) > 1) {
            $output .= " <ul>\n";
            foreach ($messages as $message) {
                $output .= '  <li>' . $message . "</li>\n";
            }
            $output .= " </ul>\n";
        } else {
            $output .= $messages[0];
        }
        $output .= "</div>\n";
    }
    return $output . 'hi';
}

/**
 * z_set_message wrapper function.
 *
 * If the function is called with no arguments, this function returns all set
 * messages without clearing them.
 *
 * @param $input
 *   The message to be displayed to the user. For consistency with other
 *   messages, it should begin with a capital letter and end with a period.
 */
function zd($input) {
    z_set_message($input);
}

/**
 * Prints zd() calls' debugging information to developers only.
 */
function print_debug_info() {
    try {
        $uid = Zend_Registry::get('uid');
        $developers_uid = HMUtils::$developers_uid;

        if (in_array($uid, $developers_uid)) {
            $m = z_get_messages('status');
            if (!empty($m)) {
                krumo($m['status']);
            }
        }
    } catch (Exception $e) {
        
    }
}

function startstat($function_name){
	global $hmstat;
	if(!isset($hmstat)){
		$hmstat = array();
	}
	if(!isset ($hmstat[$function_name]['start time'])){
		$mtime = microtime();
		$mtime = explode(" ",$mtime);
		$mtime = $mtime[1] + $mtime[0];
		$hmstat[$function_name]['start time'] = $mtime;
	}
	if(!isset ($hmstat['order'])){
		$hmstat['order'] = array();
	}
	$hmstat['order'][] = $function_name;
}

function markstat($function_name){
	global $hmstat;
	if(!isset ($hmstat[$function_name]['end time'])){
		$hmstat[$function_name]['end time'] = array();
	}
	$mtime = microtime();
	$mtime = explode(" ",$mtime);
	$mtime = $mtime[1] + $mtime[0];
	$hmstat[$function_name]['end time'][] = $mtime;
}

function showstat(){
	
	global $hmstat;
	foreach($hmstat as $function_name => $stat){
		$hmstat[$function_name]['execution time'] = array();
		if(isset($stat['end time']) && isset($stat['start time'])){
			foreach($stat['end time'] as $end_time){
				$hmstat[$function_name]['execution time'][] = $end_time - $stat['start time'];
			}
		}
	}
	krumo($hmstat);
}