<?php
/**
 * Clase para inicializar aplicacion para
 * acceder a la informacion de Facebook.
 *
 * @author Luis Cruz Tapia
 */


class Dgt_Formupload {
   public $formulario;
   private $idpropiedad;
   private $uid;
    public function __construct() {
   
    }
   
    /**
     * Retorna el formulario de subir ficheros.
     * @return Array desc
     */

	public function create_form($idprop) {
          $this->idpropiedad=$idprop;
          $this->uid=$uid;
          $this->file_input('Foto', 'file_upload_array[]',6);
	}


	public function file_input($text, $name, $iterations) {
    // The  function creates  el formulario.
   $this->formulario= <<<EOT
    <form  method="post" id="upload_fotos" enctype="multipart/form-data" action="/mihousebook/fotosUpload"  onsubmit="return false;">
      <input name="$name" type="file"   accept="gif|jpg|png" maxlength="$iterations" id="input_up"/>
      <input type="hidden" value="$this->idpropiedad" name="idpropiedad"/>
      <input type="button" value="Subir Fotos" id="enviar" class="gsmsc-result-list-clear-results" />
    </form>
EOT;
   }

        /**
        * resize_image()
        *
        * Create a file containing a resized image
        *
        * @param  $src_file the source file
        * @param  $dest_file the destination file
        * @param  $new_size the size of the square within which the new image must fit
        * @param  $method the method used for image resizing
        * @return 'true' in case of success
        */
  public function resize_image($src_file, $dest_file, $new_size, $method, $thumb_use,$default_file_mode,$GIF_support)
  {
            global $lang_errors;
            $impath="";
            $jpeg_qual=80;
            $im_options="-antialias";
            $debug_mode=0;
            define("GIS_GIF", 1);
            define("GIS_JPG", 2);
            define("GIS_PNG", 3);

            $imginfo = getimagesize($src_file);
            if ($imginfo == null)
                return false;
             // GD can only handle JPG & PNG images
            if ($imginfo[2] != GIS_JPG && $imginfo[2] != GIS_PNG && ($method == 'gd1' || $method == 'gd2')) {
                $ERROR = $lang_errors['gd_file_type_err'];
                return false;
            }
            // height/width
            $srcWidth = $imginfo[0];

            $srcHeight = $imginfo[1];

            if ($thumb_use == 'ht') {
                $ratio = $srcHeight / $new_size;
            } elseif ($thumb_use == 'wd') {
                $ratio = $srcWidth / $new_size;
            } else {
                $ratio = max($srcWidth, $srcHeight) / $new_size;
            }
            $ratio = max($ratio, 1.0);
            $destWidth = (int)($srcWidth / $ratio);
            $destHeight = (int)($srcHeight / $ratio);

            // Method for thumbnails creation
            switch ($method) {
                case "im" :
                    if (preg_match("#[A-Z]:|\\\\#Ai", __FILE__)) {
                        // get the basedir, remove '/include'
                        $cur_dir = substr(dirname(__FILE__), 0, -8);
                        $src_file = '"' . $cur_dir . '\\' . strtr($src_file, '/', '\\') . '"';
                        $im_dest_file = str_replace('%', '%%', ('"' . $cur_dir . '\\' . strtr($dest_file, '/', '\\') . '"'));
                    } else {
                        $src_file = escapeshellarg($src_file);
                        $im_dest_file = str_replace('%', '%%', escapeshellarg($dest_file));
                    }

                    $output = array();
                    /*
                     * Hack for working with ImageMagick on WIndows even if IM is installed in C:\Program Files.
                     * By Aditya Mooley <aditya@sanisoft.com>
                     */
                    if (eregi("win",$_ENV['OS'])) {
                        $cmd = "\"".str_replace("\\","/", $impath)."convert\" -quality {$jpeg_qual} {$im_options} -geometry {$destWidth}x{$destHeight} ".str_replace("\\","/" ,$src_file )." ".str_replace("\\","/" ,$im_dest_file );
                        exec ("\"$cmd\"", $output, $retval);
                    } else {
                        $cmd = "{$impath}convert -quality {$jpeg_qual} {$im_options} -geometry {$destWidth}x{$destHeight} $src_file $im_dest_file";
                        exec ($cmd, $output, $retval);
                    }


                    if ($retval) {
                        $ERROR = "Error executing ImageMagick - Return value: $retval";
                        if ($debug_mode) {
                            // Re-execute the command with the backtick operator in order to get all outputs
                            // will not work is safe mode is enabled
                            $output = `$cmd 2>&1`;
                            $ERROR .= "<br /><br /><div align=\"left\">Cmd line : <br /><span style=\"font-size:120%\">" . nl2br(htmlspecialchars($cmd)) . "</span></div>";
                            $ERROR .= "<br /><br /><div align=\"left\">The convert program said:<br /><span style=\"font-size:120%\">";
                            $ERROR .= nl2br(htmlspecialchars($output));
                            $ERROR .= "</span></div>";
                        }
                        @unlink($dest_file);
                        return false;
                    }
                    break;

                case "gd1" :
                    if (!function_exists('imagecreatefromjpeg')) {
                        cpg_die(CRITICAL_ERROR, 'PHP running on your server does not support the GD image library, check with your webhost if ImageMagick is installed', __FILE__, __LINE__);
                    }
                    if ($imginfo[2] == GIS_JPG)
                        $src_img = imagecreatefromjpeg($src_file);
                    else
                        $src_img = imagecreatefrompng($src_file);
                    if (!$src_img) {
                        $ERROR = $lang_errors['invalid_image'];
                        return false;
                    }
                    $dst_img = imagecreate($destWidth, $destHeight);
                    imagecopyresized($dst_img, $src_img, 0, 0, 0, 0, $destWidth, (int)$destHeight, $srcWidth, $srcHeight);
                                touch($dest_file);
                    $fh=fopen($dest_file,'w');
                    fclose($fh);
                    imagejpeg($dst_img, $dest_file,$jpeg_qual);
                    imagedestroy($src_img);
                    imagedestroy($dst_img);
                    break;

                case "gd2" :

                    if (!function_exists('imagecreatefromjpeg')) {
                        echo 'PHP running on your server does not support the GD image library, check with your webhost if ImageMagick is installed';
                    }
                    if (!function_exists('imagecreatetruecolor')) {
                        echo 'PHP running on your server does not support GD version 2.x, please switch to GD version 1.x on the admin page';
                    }
                    if ($imginfo[2] == GIS_GIF && $GIF_support == 1){
                        $src_img = imagecreatefromgif($src_file);
                    }
                    elseif ($imginfo[2] == GIS_JPG){
                        $src_img = imagecreatefromjpeg($src_file);
                    }
                    else{
                        $src_img = imagecreatefrompng($src_file);
                    }
                    if (!$src_img) {
                        $ERROR = $lang_errors['invalid_image'];
                        return false;
                    }
                    if ($imginfo[2] == GIS_GIF){
                      $dst_img = imagecreate($destWidth, $destHeight);
                     }
                    else{
                      $dst_img = imagecreatetruecolor($destWidth, $destHeight);
                    }
                    //echo "dest img".$src_img."<br>";
                    imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $destWidth, (int)$destHeight, $srcWidth, $srcHeight);
                    touch($dest_file);
                    $fh=fopen($dest_file,'w');
                    fclose($fh);
                    imagejpeg($dst_img, $dest_file, 80);
                    imagedestroy($src_img);
                    imagedestroy($dst_img);
                    break;
            }
            // Set mode of uploaded picture
            @chmod($dest_file, octdec($default_file_mode)); //silence the output in case chmod is disabled
            // We check that the image is valid
            $imginfo = getimagesize($dest_file);
            if ($imginfo == null) {
                $ERROR = $lang_errors['resize_failed'];
                @unlink($dest_file);
                return false;
            } else {
                return true;
            }
        }


public function  replace_forbidden($str)
{
  static $forbidden_chars;
  $forbiden_fname_char="$/\\:*?&quot;'&lt;&gt;|` &amp;";
  
  if (!is_array($forbidden_chars)) {
    global $CONFIG, $mb_utf8_regex;
    if (function_exists('html_entity_decode')) {
      $chars = html_entity_decode($forbiden_fname_char, ENT_QUOTES, 'UTF-8');
    } else {
      $chars = str_replace(array('&amp;', '&quot;', '&lt;', '&gt;', '&nbsp;', '&#39;'), array('&', '"', '<', '>', ' ', "'"), $forbiden_fname_char);
    }
    preg_match_all("#$mb_utf8_regex".'|[\x00-\x7F]#', $chars, $forbidden_chars);
  }
  /**
   * $str may also come from $_POST, in this case, all &, ", etc will get replaced with entities.
   * Replace them back to normal chars so that the str_replace below can work.
   */
  $str = str_replace(array('&amp;', '&quot;', '&lt;', '&gt;'), array('&', '"', '<', '>'), $str);;
  $return = str_replace($forbidden_chars[0], '_', $str);

  /**
  * Fix the obscure, misdocumented "feature" in Apache that causes the server
  * to process the last "valid" extension in the filename (rar exploit): replace all
  * dots in the filename except the last one with an underscore.
  */
  // This could be concatenated into a more efficient string later, keeping it in three
  // lines for better readability for now.
  $extension = ltrim(substr($return,strrpos($return,'.')),'.');
  $filenameWithoutExtension = str_replace('.' . $extension, '', $return);
  $return = str_replace('.', '_', $filenameWithoutExtension) . '.' . $extension;

  return $return;
}


public function  creardirectorio($nombre){
  if (!is_dir($nombre)) {
    $cpg_umask = umask(0);
    @mkdir($nombre,0777);
    umask($cpg_umask);
    unset($cpg_umask);

  }
}



public function  creardirectorio_main($nombre,$idpropiedad,$uidfb){
  if (!is_dir($nombre)) {
    $cpg_umask = umask(0);
    @mkdir($nombre,0777);
    umask($cpg_umask);
    unset($cpg_umask);
    $this->crear_album_BD($idpropiedad,$uidfb);


  }
}

// una vez creado el directorio para las imagenes se guarda el registro en la base de datos.
public function  get_album_BD($idpropiedad) {
    $db = Zend_Registry::get('db');

    $select ="select aid  from prop_foto_album where idpropiedad=".$idpropiedad;

    $results = $db->fetchAll($select);
    $albumid=$results[0]->aid==""?0:$results[0]->aid;
    return $albumid;
}

public function  get_size_album_BD($idpropiedad,$albumid){
    $db = Zend_Registry::get('db');
    $elementos=array();

//    $select ="select size  from prop_foto_album where idpropiedad=".$idpropiedad." and aid=".$albumid;
    $select ="SELECT   prop_foto.fid,  prop_foto.caption FROM  prop_foto_album
              INNER JOIN prop_foto ON (prop_foto_album.aid = prop_foto.prop_foto_album_aid)
              WHERE (prop_foto_album.idpropiedad = $idpropiedad) AND (prop_foto.prop_foto_album_aid = $albumid)
              ORDER BY  prop_foto.fid  DESC LIMIT 1";
    $results = $db->fetchAll($select);
      $row=$results[0]->caption==""?0:$results[0]->caption;
    $elementos=explode("_", $row);
    $indice=$elementos[0];
    return $indice;
}
// una vez creado el directorio para las imagenes se guarda el registro en la base de datos.
public function  crear_album_BD($idpropiedad,$uidfb) {
    $db = Zend_Registry::get('db');

    $select ="select count(*) as total from prop_foto_album";
    $results = $db->fetchAll($select);

    $albumid=(int)($results[0]->total)+1;

     // Insert the new record.
     $row = array(
                            'aid' => (int)$albumid,
                            'idpropiedad' => $idpropiedad,
                            'fbUser_id' =>$uidfb,
							'creado' => new Zend_Db_Expr('CURDATE()')
                );
      try
     {
         $db->beginTransaction();
         $db->insert('prop_foto_album', $row);
         $db->commit();
         return true;
     }catch(Exception $ex){
         echo "Error al generar Id de la galeria<br>";
         Zend_Debug::dump($ex);
         $db->rollBack();
         return FALSE;
     }
    
}



//Insertar la referencia de las fotos en la base de datos HouseBook
public function  insert_fotos_album_db($albumid,$normal,$destacada,$destacada_small,$cercanas,$caption){
    $db = Zend_Registry::get('db');

     // Insert the new record.
     $row = array(
                            
                            'caption' => $caption,
                            'src' => $normal,
                            'src_destacadas_normal' =>$destacada,
                            'src_destacadas_small' =>$destacada_small,
                            'src_medium' =>$cercanas,
                            'prop_foto_album_aid' => (int)$albumid,
			    'creada' => new Zend_Db_Expr('CURDATE()'),
			    'modificada' => new Zend_Db_Expr('CURDATE()')
                );
      try
     {
         $db->beginTransaction();
         $db->insert('prop_foto', $row);
         $db->commit();
         return true;
     }catch(Exception $ex){
         echo "Error al generar Id de la Foto<br>";
         Zend_Debug::dump($ex);
         $db->rollBack();
         return FALSE;
     }

}


    // The update_record function. Takes the $unique_ID and $encoded_string.
public function  update_album_BD($idpropiedad, $albumid,$location,$size) {
        $db = Zend_Registry::get('db');

       $row = array(
                            'locacion' => $location,
                            'size' => (int)$size,
							'modificado' => new Zend_Db_Expr('CURDATE()')
                 );

           try
             {
                 $db->beginTransaction();
                 $where[]=$db->quoteInto("idpropiedad=?",$idpropiedad,'INTEGER');
                 $where[]=$db->quoteInto("aid=?",$albumid,'INTEGER');
                 $db->update('prop_foto_album', $row, $where);
                 $db->commit();
                 return true;
             }catch(Exception $ex){
                 echo "Error al actualizar el Album de  Foto<br>";
                 Zend_Debug::dump($ex);
                 $db->rollBack();
                 return FALSE;
             }

    }

    //Obtener el id de la foto en base al id de propiedad y del album para luego
   //actualizarla con los thumbs(destacadas, destacadas_small, cercanas y la foto en los marcadores.
 public function  get_id_fot_albumBD($idpropiedad,$albumid,$namefoto){
        $db = Zend_Registry::get('db');
        
        $select ="SELECT  prop_foto.fid,prop_foto.caption  FROM  prop_foto_album
                INNER JOIN prop_foto ON (prop_foto_album.aid = prop_foto.prop_foto_album_aid)
        WHERE
          (prop_foto_album.idpropiedad = $idpropiedad) AND
          (prop_foto.prop_foto_album_aid = $albumid) AND
          (prop_foto.caption LIKE '".$namefoto."')";
        $results = $db->fetchAll($select);

        $fotoid=$results[0]->fid;
        return $fotoid;

 }

      // The update_record function. Takes the $unique_ID and $encoded_string.
 public function  update_foto_album_BD($idpropiedad, $albumid,$idfoto,$dirMarker,$dirDestacadas,$dirDestacadasSmall,$dirCercanas) {
        $db = Zend_Registry::get('db');
  
                   // Insert the new record.
         $row = array(
                                'src_destacadas_normal' =>$dirDestacadas,
                                'src_destacadas_small' =>$dirDestacadasSmall,
                                'src_medium' =>$dirCercanas,
                                'modificada' => new Zend_Db_Expr('CURDATE()')
                    );
          try
             {
                 $db->beginTransaction();
                 $where[]=$db->quoteInto("fid=?",$idfoto,'INTEGER');
                 $where[]=$db->quoteInto("prop_foto_album_aid=?",$albumid,'INTEGER');
                 $db->update('prop_foto', $row, $where);
                 $db->commit();
               
             }catch(Exception $ex){
                 echo "Error al actualizar el default del perfil de la propiedad<br>";
                 Zend_Debug::dump($ex);
                 $db->rollBack();
                 return FALSE;
             }

          $row2 = array(
                            'foto' => $dirMarker,
			    'ultima_modificacion' => new Zend_Db_Expr('CURDATE()')
                 );

          try
             {
                 $db->beginTransaction();
                 $where=$db->quoteInto("idpropiedad=?",$idpropiedad,'INTEGER');
                 $db->update('propiedad', $row2, $where);
                 $db->commit();
                 return true;
             }catch(Exception $ex){
                 echo "Error al actualizar el default de la imagen del marcador<br>";
                 Zend_Debug::dump($ex);
                 $db->rollBack();
                 return FALSE;
             }

    }

}
?>