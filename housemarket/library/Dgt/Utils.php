<?php
/**
 * Funciones varias
 *
 * @author jms
 */
class Dgt_Utils {
    public static function make_query($m, $first=true) {
        $output = '';
        foreach($m as $key => $value) {
            if ($first) {
                $output = '?'.$key.'='.$value;
                $first = false;
            } else {
                $output .= '&'.$key.'='.$value;
            }
        }
        return $output;
    }
}
?>
