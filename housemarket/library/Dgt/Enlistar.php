<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of enlistar
 *
 * @author Luis Cruz
 */
class Dgt_Enlistar {
    //put your code here

    public static function getPais() {

        $db = Zend_Registry::get('db');
        $sql ='SELECT zip_code as id,pais as label FROM pais';
        $rs = $db->fetchAll($sql);
        return $rs;

    }

    public static function gettype_property() {

        $db = Zend_Registry::get('db');
        $sql ='SELECT idtipo as id,es as label FROM prop_categoria';
        $rs = $db->fetchAll($sql);
        return $rs;

    }

    public static function getProperty($sort="idpropiedad",$dir="ASC",$start = 0, $limit = 50,$uid= 0) {
        $db = Zend_Registry::get('db');

        $params = array();

        $sql = 'SELECT
  propiedad.idpropiedad,
  propiedad.descripcion,
  pais.pais,
  propiedad.precio_venta,
  prop_tipo.nombre as "tipo_enlistamiento",
  prop_categoria.es as "tipo_propiedad",
  propiedad.precio_alquiler,
  propiedad.fecha_ingreso,
  propiedad.ultima_modificacion,
  propiedad.area,
  propiedad.cuartos,
  propiedad.banos,
  propiedad.lat,
  propiedad.uid,
  propiedad.lng,
  propiedad.estatus,
  prop_tipo.nombre
   FROM
  propiedad
   LEFT OUTER JOIN pais ON (pais.zip_code = propiedad.pais)
   LEFT OUTER JOIN prop_tipo ON (propiedad.tipo_enlistamiento = prop_tipo.tipo)
   LEFT OUTER JOIN prop_categoria ON (propiedad.tipo_propiedad = prop_categoria.idtipo)';


        $sql .= ' WHERE uid='.$uid;
        $sql .= " ORDER BY  ".$sort." ".$dir;
        $sql .= " LIMIT $start , $limit";

//      $rs = $db->query($sql, $params);
        $rs = $db->query($sql);
        return $rs->fetchAll();
    }


    public static function getPropertyCount($uid) {
        $db = Zend_Registry::get('db');
        $sql = "SELECT COUNT(*) AS total FROM propiedad";
        $sql .= "  WHERE uid=$uid";

        $rs = $db->query($sql);
        return $rs->fetchAll();
    }

    public static function getCargaMasiva($sort,$dir,$start,$limit,$valores,$uid,$esCM=false) {

        $db = Zend_Registry::get('db');
        $sql = "SELECT P.CodigoPropiedad, P.NombrePropiedad, C.NombreEn as NombreCategoria,
                case P.Accion
                    when 'S' then 'Sale'
                    when 'R' then 'Rental'
                    when 'B' then 'Rental/Sale'
                    when 'F' then 'Foreclosure'
                end as Transaccion
                FROM propropiedad P
                inner join catcategoria C on P.CodigoCategoria=C.CodigoCategoria where P.Uid=".$uid;

        if($esCM) {
            $sql.=" and P.CodigoPropiedad in (". implode(',', $valores).")";
        }
        else {
            $sql.=" and P.CargaMasiva=true and P.CodigoEdoPropiedad=1";
        }

        $sql .= " ORDER BY ".$sort." ".$dir;
        $sql .= " LIMIT $start , $limit";
        $rs =Dgt_Enlistar::convertDataforGrid($db->fetchAll($sql));
        return $rs;

    }

    public static function getCountCargaMasiva($valores,$uid,$esCM=false) {
        $db = Zend_Registry::get('db');
        $sql ="SELECT count(P.CodigoPropiedad) as total
                FROM propropiedad P
                inner join catcategoria C on P.CodigoCategoria=C.CodigoCategoria where P.Uid=".$uid;
        if($esCM) {
            $sql.=" and P.CodigoPropiedad in (". implode(',', $valores).")";
        }
        else {
            $sql.=" and P.CargaMasiva=true and P.CodigoEdoPropiedad=1";
        }

        $rs = $db->fetchAll($sql);
        return $rs;

    }

    public static function getPropropiedad($sort,$dir,$start,$limit,$uid,$estado,$categoria) {
        //CONCAT(CEP.NombreEdoPropiedad,'/',CEP.NombreEn)
        //CONCAT(CC.NombreCategoria,'/',CC.NombreEN)
        $db = Zend_Registry::get('db');
        $sql = "SELECT SQL_CALC_FOUND_ROWS P.CodigoPropiedad,P.CodigoBroker,CC.NombreEN as TipoPropiedad,
                CASE P.Accion
                     when 'S' then 'Sale'
                     when 'R' then 'Rental'
                     when 'B' then 'Rental/Sale'
                END as TipoEnlistamiento,CP.Pais,P.Estado,P.Ciudad,
                P.CodigoEdoPropiedad as Status,P.FechaRegistro,P.Latitud,P.Longitud,
                (SELECT sum(CCP.cantidad) FROM crecreditopropiedad CCP WHERE CCP.CodigoPropiedad=P.CodigoPropiedad) as Creditos
                FROM propropiedad P
                INNER JOIN catcategoria CC ON(P.CodigoCategoria=CC.CodigoCategoria)
                LEFT JOIN catpais CP ON(P.ZipCode=CP.ZipCode)
                WHERE P.CodigoEdoPropiedad<>4 AND P.Uid=".$uid;

        if(isset($estado) && strlen($estado)>0)
            $sql.=" AND P.CodigoEdoPropiedad = ".$estado;
        if(isset($categoria) && strlen($categoria)>0)
            $sql.=" AND CC.CodigoCategoria = ".$categoria;

        $sql .= " ORDER BY ".$sort." ".$dir;
        $sql .= " LIMIT $start , $limit";
        //echo $sql;
        $rs=new stdClass();
        $rs->rows=$db->fetchAll($sql);
        $rs->results=$db->fetchOne("SELECT FOUND_ROWS()");
        return $rs;
    }

    public static function getCountPropropiedad($uid,$estado,$categoria) {
        $db = Zend_Registry::get('db');
        $sql ="select count(P.CodigoPropiedad) as total
                from propropiedad P
                inner join catcategoria CC on P.CodigoCategoria=CC.CodigoCategoria
                left join catpais CP on P.ZipCode=CP.ZipCode
                inner join catedopropiedad as CEP on P.CodigoEdoPropiedad=CEP.CodigoEdoPropiedad
                where P.CodigoEdoPropiedad<>4 and P.Uid=".$uid;

        if(isset($estado) && strlen($estado)>0)
            $sql.=" and CEP.CodigoEdoPropiedad = ".$estado;
        if(isset($categoria) && strlen($categoria)>0)
            $sql.=" and CC.CodigoCategoria = ".$categoria;

        $rs = $db->fetchAll($sql);
        return $rs;

    }

    public static function getLecturaPropiedad($sort,$dir,$start,$limit,$uid) {
        //CONCAT(CEP.NombreEdoPropiedad,'/',CEP.NombreEn)
        //CONCAT(CC.NombreCategoria,'/',CC.NombreEN)
        $db = Zend_Registry::get('db');
        $sql = "select P.CodigoPropiedad,P.CodigoBroker,CC.NombreEN as TipoPropiedad,
                case P.Accion
                     when 'S' then 'Sale'
                     when 'R' then 'Rental'
                     when 'B' then 'Rental/Sale'
                end as TipoEnlistamiento,CP.Pais,P.Estado,P.Ciudad,
                CEP.NombreEn as Status,P.FechaRegistro
                from propropiedad P
                inner join catcategoria CC on P.CodigoCategoria=CC.CodigoCategoria
                left join catpais CP on P.ZipCode=CP.ZipCode
                inner join catedopropiedad as CEP on P.CodigoEdoPropiedad=CEP.CodigoEdoPropiedad
                where P.CodigoEdoPropiedad=2 and P.Uid=".$uid;

        $sql .= " ORDER BY ".$sort." ".$dir;
        $sql .= " LIMIT $start , $limit";
        $rs =Dgt_Enlistar::convertDataforGrid($db->fetchAll($sql));
        return $rs;

    }

    public static function getCountLecturaPropiedad($uid) {
        $db = Zend_Registry::get('db');
        $sql ="select count(P.CodigoPropiedad) as total
                from propropiedad P
                inner join catcategoria CC on P.CodigoCategoria=CC.CodigoCategoria
                left join catpais CP on P.ZipCode=CP.ZipCode
                inner join catedopropiedad as CEP on P.CodigoEdoPropiedad=CEP.CodigoEdoPropiedad
                where P.CodigoEdoPropiedad=2 and P.Uid=".$uid;

        $rs = $db->fetchAll($sql);
        return $rs;

    }

    public static function convertDataforGrid($arr) {
        for($i=0;$i<count($arr);$i++) {
            $arr[$i]->Ciudad= iconv("latin1","utf-8", $arr[$i]->Ciudad);
            $arr[$i]->Estado= iconv("latin1","utf-8", $arr[$i]->Estado);
            $arr[$i]->NombrePropiedad= iconv("latin1","utf-8", $arr[$i]->NombrePropiedad);
            $arr[$i]->Pais= iconv("latin1","utf-8", $arr[$i]->Pais);
        }
        return $arr;
    }
}
?>
