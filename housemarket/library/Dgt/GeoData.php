<?php

class Dgt_GeoData { 
    public static function getLocation($ip) {
        $dbAdapter = Zend_Registry::get('db');
        $sqlpriori = sprintf('SELECT cc, city, lat, lng
                    FROM `geoipinfodb_ipcities` 
                    WHERE ipfrom <= INET_ATON("%s") AND ipto>= INET_ATON("%s")',$ip,$ip); 
        $rs = $dbAdapter->query($sqlpriori);
        $data = $rs->fetchAll();
       
        if(count($data)<=0){ 
        $sql = sprintf('SELECT l.country cc, l.city, l.latitude lat, l.longitude lng
                    FROM geolitecity_location l where
                    l.locID = (
                    SELECT locID FROM geolitecity_blocks
                    INNER JOIN (  SELECT MAX(startIPNum) AS start
                    FROM `geolitecity_blocks`
                    WHERE startIPNum <= INET_ATON("%s")
                    ) AS s ON (startIPNum = s.start)
                    WHERE endIPNum >= INET_ATON("%s"))',$ip,$ip);
        //$params = array($ip, $ip); 
     
        $rs = $dbAdapter->query($sql);
        $data = $rs->fetchAll();
       }
              
        return $data;

    }
}
?>
