<?php
/**
 *
 * Cliente para el servicio web de Google Geocode
 * para traducir direcciones en coordenadas latitud y longitud
 * haciendo uso de la libreria Zend Framework.
 *
 * @package Dgt
 * @autor Ing. Jeronimo Martinez Sanchez
 * @copyright Copyright(c) 2009, Digitech
 *
 */ 
class Dgt_GeocoderClient {
    
    // google geocode web service params
    private $url = 'https://maps.google.com/maps/geo';
    private $q = ''; // query address for geocode coords.
    private $key = 'ABQIAAAAx2y_OT2VVD9i39_pfk-9LxScxGgikEm8cvZ8Me_iQQSushZj5hTozU5u--HvUuxh2LXqrxx9smI_9A'; // Api Key
    private $sensor = 'false'; // web -> false, gps -> true
    private $output = 'csv'; // formats json, xml, csv, kml
    private $oe = 'utf8'; // better set encode to utf8.
    private $ll = '';   // latitude, longitude  ej: 40.479581,-117.773438, only for reverse geocode 
    private $spn = '';  // span ej: 11.1873,22.5, only for reverse geocode
    private $gl = '';   // country code cc_TLD, set for better acuracy
    
    // ISO 3136-1 like
    private $cc_xml = 'cc_tld.xml';
        
    public function setQuery($q) {
        $this->q = $q;
        return;
    }
    
    public function setSensor($sensor) {
        $this->sensor = $sensor;
        return;
    }  
    
    public function setOutput($output){
        $this->output = $output;
        return;
    }
    
    public function setOutputEnconde($oe) {
        $this->oe = $oe;
        return;
    }
    
    public function setLongLat($ll){
        $this->ll = $ll;
        return;
    }
    
    public function setSpn($spn) {
        $this->spn = $spn;
        return;
    }
    
    public function setCountryCode($gl) {
        $this->gl = $gl;
        return;
    }
    
    /**
     * Busca en codigo de pais cc_TLD utilizando un archivo xml
     * con los codigos previamente definidos y fija el parametro
     * gl llamando al metodo setter setCountryCode.
     *
     * 
     * @param string $country nombre del pais a buscar
     * @return string codigo de pais cc_TLD
     *
     */
    public function searchCC($country) {
        try{
            $xml = simplexml_load_file($this->cc_xml);
            foreach($xml->Country as $c) {
                if( $c->CountryName == strtoupper($country)) {
                    $code = strtolower($c->CountryCoded);
                    $this->setCountryCode($code);
                }
            }    
        } catch(Exception $e) {
            // file io or xml parse fail, leave things with no changes. fail silent.
            //print $e->getMessage();
        }
        
        return;      
    }
    
    public function getQuery() {
        return $this->q;
    }
    
    /**
     *
     * Realiza la peticion al servicio web de google Geocode
     * dependiendo del tipo de salida(output) elegida utiliza el cliente Rest
     * o el cliente http para hacer la peticio.
     *
     * @return Zend_Http_Response respuesta del servicio web
     *
     */    
    public function getGeocode() {
        // if output is xml use Rest client, other use Http client.
        if($this->output == 'xml') {
            // start new Zend Rest Client.
            $client = new Zend_Rest_Client($this->url);
        
            // set params
            $client->q($this->q);
            $client->key($this->key);
            $client->sensor($this->sensor);
            $client->output($this->output);
            $client->oe($this->oe);
            $client->ll($this->ll);
            $client->spn($this->spn);
            $client->gl($this->gl);
        
            $response = $client->get();
            
        } else {
            // start new Zend Http Client
            $client = new Zend_Http_Client();
            
            // set http client configuration
            $client->setConfig(array('maxredirects' => 0,
                                     'timeout' => 30));
            $client->setMethod(Zend_Http_Client::GET);
            $client->setUri($this->url);
            
            // set params
            $params = array('q' => $this->q,
                            'key' => $this->key,
                            'sensor' => $this->sensor,
                            'output' => $this->output,
                            'oe' => $this->oe,
                            'll' => $this->ll,
                            'spn' => $this->spn,
                            'gl' => $this->gl);
            $client->setParameterGet($params);
            $response = $client->request();
        }
        return $response;        
    }
}
