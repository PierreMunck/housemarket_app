<?php

class PropiedadesController extends Zend_Controller_Action {
    protected $_flashMessenger = null;
    protected $_redirector = null;
    protected $_applicationParameter = null;

    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('ZsamerFlashMessenger');
        $config = Zend_Registry::get('config');
        $this->view->web_path = $config['fb_app_url'];
    }

    public function preDispatch() {
    }

    public function showAction() {
        $log=Zend_Registry::get('log');
        try {
            $fb= new Dgt_Fb();
        }catch(Exception $e ) {
            Zend_Debug::dump("Expiracion de la sessi�n.");
            die;
        }
       
        $this->view->uid=$fb->get_uid();
        $this->view->appusers = $fb->getFriendsApp();

        $this->view->headTitle('Propiedades');
        $this->view->loadmap = 'onunload="GUnload();"';
        $config = Zend_Registry::get('config');
        $google_api_key = $config['google_api_key'];
        $localLangague = Zend_Registry::get('Zend_Locale');
        $this->view->headScript()->appendFile("https://maps.google.com/maps?file=api&v=2&sensor=false&key=$google_api_key",'text/javascript');
        $this->view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/lang/'.$localLangague.'.js', 'text/javascript');
        $this->view->headScript()->appendFile("https://www.google.com/uds/api?file=uds.js&v=1.0&source=uds-msw&key=$google_api_key");
        $this->view->headScript()->appendFile("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.js?mode=new");
        $this->view->headScript()->appendFile("/js/agregar_prop.js", "text/javascript");

        $this->view->headLink()->appendStylesheet("https://www.google.com/uds/css/gsearch.css");
        $this->view->headLink()->appendStylesheet("https://www.google.com/uds/solutions/mapsearch/gsmapsearch.css");
        $this->view->headLink()->appendStylesheet("/css/custom-theme/jquery-ui-1.7.2.custom.css");
        $this->view->headLink()->appendStylesheet('/css/layout.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
        $this->view->headLink()->appendStylesheet('/css/tabs.css');
        $this->view->headLink()->appendStylesheet("/css/style_menu.css");
        $this->view->headLink()->appendStylesheet("/css/messages.css");
        $this->view->headLink()->appendStylesheet('/css/style_form.css');

        $language = Hm_Cat_Idioma::GetLanguage();
        $cate = new Hm_Cat_Categoria();
        $categories = $cate->GetCategoriesList($language);
        $categories[""] = ($language == "ES")? "Elegir" : "Choose";

        $this->view->categories = $categories;
        $langunidad=$this->view->translate("LABEL_UNIT");
        $this->view->UMSuperficies = array(""=>$langunidad, "vr2"=>"vr2", "mt2"=>"mt2", "ft2"=>"ft2");
        $estate = new Hm_Pro_Propiedad();
        $property = new Hm_Pro_Propiedad();
        $category = new Hm_Cat_Categoria();
        $this->view->isNew = "S";
        $this->view->registro = "S";
        $this->view->publicado = "N";
        $this->view->estate = $estate;
        $this->view->web_path = $config['fb_app_url'];
        $this->view->url_path = $config['local_app_url'];
       
        // Determinar si es nueva o no.
        $f = new Zend_Filter_StripTags();
        $estateId = null;
        if( $this->getRequest()->isget()) {
            // Intentar recuperar el parametro de la propiedad.
            $estateId = $f->filter($this->getRequest()->getParam("id", null)) ;
            $this->view->isPublicada = $f->filter($this->getRequest()->getParam("publicada", null));
            $log->info(__METHOD__ . __LINE__ . " Id " . $estateId);

           

            // Recuperar la propiedad a partir del id proporcionado.
            $rs = $estate->find($estateId);
            if($rs != null && $rs->count() > 0) {
                $log->info(__METHOD__ . __LINE__ . " Estate found ");
                $estate = $rs->current();
                $uid = Zend_Registry::get('uid');
                if($estate->Uid == $uid ) { // El usuario es propietario
                    $log->info(__METHOD__ . __LINE__ . " Estate belongs to uid: " . $uid);
                    $this->view->estate = $estate;
                    $this->view->isNew = "N";
                    $propResult = Hm_Pro_Propiedad::ObtenerDatosProPropiedad($estateId);
                    $this->view->property = $propResult[0];
                    

                    $categoria = new Hm_Cat_Categoria();
                    $categoria = $categoria->find($estate->CodigoCategoria);

                    // Recuperar todos los atributos dada la propiedad.
                    $atributos = $categoria->current()->findHm_Cat_AtributoCategoria();
                    $atributosInfo = array();
                    foreach($atributos as $atributo) {
                        $atributosInfo[$atributo->CodigoAtributo] = $atributo->CampoDestino;
                    }

                    // Recuperar los atributos / valores asociados a la propiedad.
                    $attrEstate = $estate->findHm_Pro_AtributoPropiedad();
                    $attrEstateSelected = array();
                    foreach($attrEstate as $attrSelected ) {
                        switch($atributosInfo[$attrSelected->CodigoAtributo]) {
                            case "ValorEntero":
                                $attrEstateSelected[$attrSelected->CodigoAtributo] = $attrSelected->ValorEntero;
                                break;
                            case "ValorDecimal":
                                $attrEstateSelected[$attrSelected->CodigoAtributo] = $attrSelected->ValorDecimal;
                                break;
                            case "ValorS_N":
                                $attrEstateSelected[$attrSelected->CodigoAtributo] = $attrSelected->ValorS_N;
                                break;
                            case "ValorCaracter":
                                $attrEstateSelected[$attrSelected->CodigoAtributo] = $attrSelected->ValorCaracter;
                                break;
                        }
                        
                    }

                    $this->view->attrEstate = $attrEstateSelected;

                    // Recuperar todos los beneficios de la propiedad
                    $benEstate = $property->GetBeneficiosByProperty($estate->CodigoPropiedad);
                    $this->view->benEstate = array();
                    foreach($benEstate as $benefitEstate) {
                        $this->view->benEstate[] = $benefitEstate->CodigoBeneficio;
                    }

                    // Recuperar todos los atributos por categoria.
                    //$rsAttributes = $categoria->current()->findHm_Cat_AtributoViaHm_Cat_AtributoCategoriaByHm_Cat_Categoria();
                    $attributes = new Hm_Cat_AtributoIdioma();
                    $selectAttrib = $attributes->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
                    $selectAttrib->setIntegrityCheck(false)
                            ->where($attributes->getAdapter()->quoteInto("CodigoIdioma = ?", $language))
                            ->join("catatributo", "catatributo.CodigoAtributo = catatributoidioma.CodigoAtributo")
                            ->join("catatributocategoria", "catatributocategoria.CodigoAtributo = catatributo.CodigoAtributo")
                            ->where("catatributocategoria.CodigoCategoria = ?", $categoria->current()->CodigoCategoria);

                    $rsAttributes = $attributes->fetchAll($selectAttrib);

                    $this->view->rsAttr = $rsAttributes;

                    // Recuperar todos los beneficios por categoria.
                    $benefit = new Hm_Cat_BeneficioIdioma();
                    $select = $benefit->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
                    $select->setIntegrityCheck(false)
                            ->where($benefit->getAdapter()->quoteInto("CodigoIdioma = ?", $language))
                            ->join("catbeneficio", "catbeneficio.CodigoBeneficio=catbeneficioidioma.CodigoBeneficio")
                            ->join("catbeneficiocategoria", "catbeneficiocategoria.CodigoBeneficio = catbeneficio.CodigoBeneficio")
                            ->where("catbeneficiocategoria.CodigoCategoria = ?", $categoria->current()->CodigoCategoria);

                    $rsBenefits = $benefit->fetchAll($select);
                    
                    $this->view->rsBen = array();
                    if($rsBenefits->count() > 0){
                        foreach($rsBenefits as $rsBenefit) {
                            $this->view->rsBen[$rsBenefit->CodigoBeneficio] =  $rsBenefit->ValorIdioma;
                        }
                    }

                    // Recuperar todas las fotos de la propiedad.
                    $this->view->rsPhotos = array();
                        // Recuperar las fotos.
                        $findPhoto = new Hm_Pro_Foto();
                        $rsPhotos = $findPhoto->fetchAll(
                                                $findPhoto->select()
                                                ->where('CodigoPropiedad = ?', $estateId)
                                                ->order('PROFotoID')
                                    );

                        if($rsPhotos != null && $rsPhotos->count() > 0) { // La propiedad tiene fotos
                            $this->view->photoID = $rsPhotos->current()->PROFotoID;
                            foreach($rsPhotos as $photo) {
                                if($photo->Principal === chr(0x00)) { // Si no es una foto destacada
                                    $this->view->rsPhotos["otras"][] = $photo;
                                } else { // Si es Principal agregamela a destacada
                                    $this->view->rsPhotos["princ"] = $photo;
                                }
                            }
                        }
                     
                    if($estate->CodigoEdoPropiedad==2) {
                        $this->view->publicado = "S";
                    }
                }
            }
          
        }
        $isNew = $this->view->isNew=="S"?true:false;
        $log->info(__METHOD__ . "$isNew , $estateId");
        

        $this->view->totalPhotos = $this->GetAllowedTotalPhotos($isNew, $estateId);
    }

    public function saveAction() {
        $log=Zend_Registry::get('log');
        $log->info(__METHOD__ . "( " . __LINE__ . " ) Saving Estate ");

        $db = Zend_Registry::get('db');
        $db->setFetchMode(Zend_Db::FETCH_ASSOC);

        $f = new Zend_Filter_StripTags();
        if (!defined('SID')) {
            session_start();
        }
        if( $this->getRequest()->isPost()) {

            try {
                $propiedad = new Hm_Pro_Propiedad();

                $publicado = $f->filter($this->_request->getParam("publicado"), "N");
                $new = $f->filter($this->_request->getParam("isNew"), "S");
                $isNew = $new=="S"?true:false;

                // Recuperar el estado anterior de la propiedad.
                // Si estado es diferente de 2 y publicado es igual a S entonces, se debe poner a
                // publicar.
                if(!$isNew) {
                    $id = $this->_request->getParam("id");
                    try {
                        $rsPropiedad = $propiedad->find($id);
                        if($rsPropiedad->count() > 0) {
                            $publicar = ($publicado=="S" && $rsPropiedad->current()->CodigoEdoPropiedad!=2)?true:false;
                        } else {
                            $publicar = false;
                        }
                    } catch(Exception $ex) {
                        $log->info(__METHOD__ . "( " . __LINE__ . " ) Error : " . $ex->getMessage());
                        $publicar = false;
                    }

                } else {
                    $publicar = ($publicado=="S")?true:false;
                }
                $result = $propiedad->SaveOrUpdate($this->_request->getParams());
                $log->info(__METHOD__ . " (" . __LINE__ . ") " . print_r($result, true));

                // Mensajes de validacion
                if(array_key_exists("fail", $result)) {
                    $messages = $result["reasons"];
                    $messageError = "";
                    $log->info(__METHOD__ . " (" . __LINE__ . ") " . is_array($messages));
                    if(is_array($messages)) {
                        $messageError .= "<ul>";
                        foreach($messages as $field=>$messageBlock) {
                            $log->info(__METHOD__ . " (" . __LINE__ . ") $field=>$messageBlock");
                            if(is_array($messageBlock)) {
                                foreach ($messageBlock as $message) {
                                    $messageError .= "<li>" . $message . "</li>";
                                }
                            } else {
                                $messageError .= "<li>" . $messageBlock . "</li>";
                            }
                        }
                        $messageError .= "</ul>";
                    } else { // Mensaje sencillo.
                        $messageError .= "<ul><li>" . $messages ."</li></ul>";
                    }
                    $this->_flashMessenger->addError("<i18n>Please check your data</i18n>" . $messageError);
                    $this->_redirector->gotoUrl('/propiedades/show?'. $_SERVER['QUERY_STRING']);
                } else {
                    // Mensajes de advertencia al guardar el archivo.
                    // Revisar el campo de status y revisar si hay algun mensaje.
                    $isOk = true;
                    $warning = null;

                    $state = $result["status"];
                    $id = $result["id"];
                    if(is_array($state)) {
                        // Por el momento, solo revisaremos las fotos.
                        $photoStatus = $state["Album"];
                        if(array_key_exists("fail", $photoStatus)) {
                            // Ocurrio una excepcion. Mostrarla al usuario.
                            $warning = $photoStatus["reason"];
                            $isOk = false;
                        } else {
                            $log->info(__METHOD__ . " (" . __LINE__ . ") Photo Status : " . print_r($photoStatus["photos"], true));
                            if(is_array($photoStatus["photos"])) {
                                $photosResult = $photoStatus["photos"];
                                $log->info(__METHOD__ . " (" . __LINE__ . ") Photo Fails : " . print_r($photosResult, true));
                                if($photosResult["fails"] === true) { // Ocurrio un error, mostrar mensajes
                                    $warning = "<ul>";
                                    if(is_array($photosResult["reasons"])) {
                                        foreach($photosResult["reasons"] as $reasons) {
                                            if(is_array($reasons)) {
                                                foreach($reasons as $reason)
                                                    $warning .= "<li>" . $reason . "</li>";
                                            } else {
                                                $warning .= "<li>" . $reasons . "</li>";
                                            }
                                        }
                                    } else {
                                        $warning .= "<li>".$photosResult["reasons"]."</li>";
                                    }
                                    $warning .= "</ul>";
                                    $isOk = false;
                                }
                            }
                        }
                    }
                    if($isOk) {
                        $this->_flashMessenger->addSuccess("<i18n>ADD_ESTATE_SUCCESS_SAVE</i18n>");
                        $publicada = ($publicar)?'&publicada=1': '&publicada=0';
                        $this->_redirector->gotoUrl('/propiedades/show?id='.$id . '&'. $_SERVER['QUERY_STRING'] . $publicada);
                    } else {
                        $this->_flashMessenger->addSuccess("<i18n>ADD_ESTATE_SUCCESS_SAVE</i18n>");
                        $this->_flashMessenger->addWarning("Warning" . $warning);
                        if($isNew) {
                            $this->_redirect('/propiedades/show?id='.$id.'&'. $_SERVER['QUERY_STRING']);
                        } else {
                            $this->_redirect('/propiedades/show?'. $_SERVER['QUERY_STRING']);
                        }
                    }
                }
            }catch(Exception $ex) {
                $log->info(__METHOD__ . "( " . __LINE__ . " ) Error : " . $ex->getMessage());
                $this->_flashMessenger->addError('Ocurrio un error al guardar los datos de su propiedad');
                $this->_redirect('/propiedades/show?'. $_SERVER['QUERY_STRING']);
            }
        } else {
            $this->_redirect('/propiedades/show?'. $_SERVER['QUERY_STRING']);
        }
        exit();
    }

    private function GetAllowedTotalPhotos($isNew, $estateId = null) {
        $log=Zend_Registry::get('log');
        $log->info(__METHOD__ . "$isNew , $estateId");
        $maxAllowedPhotos = 4; // Recuperar de acuerdo al maximo de photos permitidas
        if($isNew) {
            $log->info(__METHOD__ . " is new");
            return $maxAllowedPhotos; // Es un nuevo registro.
        } else {
            // Recuperar el total de photos.
            $propiedad = new Hm_Pro_Propiedad();
            $fotos = $propiedad->GetPhotos($estateId);
            if(count($fotos) > 0 ) {
                $log->info(__METHOD__ . " photos found");
                $totalPhotos = count($fotos);
            } else {
                $log->info(__METHOD__ . " photos not found");
                $totalPhotos = 0;
            }
            return $maxAllowedPhotos - $totalPhotos;
        }
    }

    public function getbeneficiosAction() {
        $log = Zend_Registry::get('log');
       try {
            $categoria = new Hm_Cat_Categoria();
            $f = new Zend_Filter_StripTags();
            
            $categoryId = $f->filter($this->_request->getParam('category'), null);
            $log->info(__METHOD__ . " ( " . __LINE__ . " ) " . $categoryId);

            $beneficios = $categoria->GetBeneficios($categoryId);
            if(is_array($beneficios) && count($beneficios) > 0) {
                echo '{"success":true, "results":'.count($attributes).', "rows": '. Zend_Json::encode($beneficios) .'}';
            } else {
                echo '{"rows": []}';
            }
        } catch(Exception $e) {
            $log->info(__METHOD__ . " ( " . __LINE__ . " ) Error : " . $e->getMessage());
            echo '{"rows": []}';
        }
        exit();
    }

    /**
     * Recupera los atributos de la categoria seleccionada
     */
    public function getatributosAction() {

        $log = Zend_Registry::get('log');

        $f = new Zend_Filter_StripTags();

        if($this->getRequest()->isPost()) {
            $categoryId = $f->filter($this->_request->getParam('category',null));

            $log->info(__METHOD__ . $categoryId);
            $c = new Hm_Cat_Categoria();
            $attributes = $c->GetAllAtributes($categoryId);

            if(!empty($attributes)) {
                $log->info('{"success":true, "results":'.count($attributes).', "rows": '. Zend_Json::encode($attributes) .'}');
                echo '{"success":true, "results":'.count($attributes).', "rows": '. Zend_Json::encode($attributes) .'}';
            } else {
                echo '{"rows": []}';
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }

    public function checkLimit($uid) {
        $logger=Zend_Registry::get('log');
        $db = Zend_Registry::get('db');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $params = array ($uid);
        $count_sql = "SELECT COUNT(*) num_prop FROM propiedad WHERE uid= ?";
        $rs = $db->query($count_sql, $params);
        $data = $rs->fetchAll();
        $count= $data[0]->num_prop;
        $logger->info($count);
        if($count >= 40) {
            return false;
        } else {
            return true;
        }

    }

    public function addAction() {
        $logger=Zend_Registry::get('log');
        $logger->info("Insertando campos: ".Zend_Log::INFO);

        $db = Zend_Registry::get('db');
        $f = new Zend_Filter_StripTags();

        if( $this->getRequest()->isGet()) {

            try {
                $Precio= $f->filter($this->_request->getParam('precio'));
                $insert_data_propiedad = array(
                        'tipo_enlistamiento' => (int)trim($f->filter($this->_request->getParam('tipo_enlistamiento'))),
                        'tipo_propiedad' => trim($f->filter($this->_request->getParam('type_property'))),
                        'direccion' =>trim($f->filter($this->_request->getParam('direction_propiedad'))),
                        'pais' => trim($f->filter($this->_request->getParam('country'))),
                        'estado' => trim($f->filter($this->_request->getParam('estado'))),
                        'ciudad' => trim($f->filter($this->_request->getParam('ciudad'))),
                        'area' =>(int)trim($f->filter($this->_request->getParam('area'))) ,
                        'cuartos' => (int) trim($f->filter($this->_request->getParam('cuarto'))),
                        'banos' => (int)trim($f->filter($this->_request->getParam('bano'))),
                        'tamano_lote' => trim($f->filter($this->_request->getParam('tamano_lote'))),
                        'zip_code' => trim($f->filter($this->_request->getParam('zip_code'))),
                        'lat' => trim($f->filter($this->_request->getParam('latitud'))),
                        'lng' => trim($f->filter($this->_request->getParam('longitud'))),
                        'uid' =>trim($f->filter($this->_request->getParam('userid'))),
                        'fecha_ingreso' => new Zend_Db_Expr('CURDATE()'),
                        'estatus' => 3
                );

                $tipoPrecio=$f->filter($this->_request->getParam('tipo_enlistamiento'));


                if($tipoPrecio==1)
                    $insert_data_propiedad['precio_venta'] =$Precio;//$precio_venta;
                if($tipoPrecio==2)
                    $insert_data_propiedad['precio_alquiler'] =$Precio;//$precio_alquiler;
                if($tipoPrecio==3) {
                    $insert_data_propiedad['precio_venta'] =$Precio;//$precio_venta;
                    $insert_data_propiedad['precio_alquiler'] =$Precio;//$precio_alquiler;
                }
                $PropiedadEnlistar = new Propiedad();

                // @todo: agregar limit de propiedades a enlistar.
                // maximo = 25.
                // 1- verificar si el uid tienes propiedades enlistadas y si son <= 30
                // 2- si es igual a 30 - enviar mensaje de error y no continuar
                // 3- si es menor a 30 - continuar el registro.
                $uid = Zend_Registry::get('uid');
                $logger->info( __METHOD__ . ' uid = ' . $uid);
                if($this->checkLimit($uid)) {
                    $PropiedadEnlistar->insert($insert_data_propiedad);
                    $idpropiedad = $db->lastInsertId();
                    $logger->info( __METHOD__ . ' lastInsertId = ' . $idpropiedad);
                    Zend_Registry::set('message', "Su propiedad #$idpropiedad ha sido enlistada exitosamente");
                    $this->view->message="Su propiedad #$idpropiedad ha sido enlistada exitosamente";
                    $this->_redirector->gotoUrl('https://apps.facebook.com/housemarket/Mihousebook/edit?propiedad='.$idpropiedad);
                } else {
                    $logger->info(__METHOD__ . ' Error limite de propiedades > 30');
                    Zend_Registry::set('message', "No esta permitido enlistar mas de 30 propiedades");
                    $this->view->message="No esta permitido enlistar mas de 30 propiedades";
                }
                exit();
            }catch(Exception $ex) {
            }
        } else {
        }

    }

    public function centermapAction() {
        $ip = $this->_request->getClientIP();
        $location = Dgt_GeoData::getLocation($ip);

        if (!empty($location)) {
            echo '{"success":"true","data": '. Zend_Json::encode($location) .'}';
        } else {
            echo '{"success":"false"}';
        }
        exit();
    }

    public function propiedadenmapaAction() {
        if (!session_id()) {
            session_start();
        }
        if(!empty ($_SESSION['posmapa'])){
            echo '{"success":"true","data": '. Zend_Json::encode($_SESSION['posmapa']) .'}';
        } else {
            echo '{"success":"false"}';
        }
        exit();
    }

    public function perfilAction() {
        try {
            $fb= new Dgt_Fb();
        }catch(Exception $e ) {
            Zend_Debug::dump("Expiracion de la sessi�n.");
            die;
        }
        $this->view->appusers = $fb->getFriendsApp();
        $this->view->headTitle('Perfil');
        $this->view->loadmap = 'onunload="GUnload();"';
        $config = Zend_Registry::get('config');
        $google_api_key = $config['google_api_key'];
        $this->view->headScript()->appendFile("https://maps.google.com/maps?file=api&v=2&sensor=false&key=$google_api_key",'text/javascript');

        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');

        $db = Zend_Registry::get('db');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        if(!empty ($_GET['valprop'])) {
            $IdProp=$_GET['valprop'];
            $DetaProp=$this->ObtenerDatosProPropiedad($IdProp);

            if(count($DetaProp)>0) {
                $this->view->prenta= number_format($DetaProp[0]->PrecioAlquiler,2);
                $this->view->pventa=number_format($DetaProp[0]->PrecioVenta,2);
                $this->view->descripcionprop=$DetaProp[0]->DescPropiedad;
                $caracteristicasprop="<span>Area: ".$DetaProp[0]->Area .' '.$DetaProp[0]->UnidadMedida.'</span> <span>Area Lote: '.$DetaProp[0]->AreaLote.' '.$DetaProp[0]->UnidadMedidadLote.'</span> ';
                $this->view->direccionprop=$DetaProp[0]->Direccion.', '.$DetaProp[0]->Calle.', '.$DetaProp[0]->Ciudad.', '.$DetaProp[0]->Estado.'. '.$DetaProp[0]->Pais.'. '.$DetaProp[0]->ZipCode;
                $this->view->transaccionprop=$DetaProp[0]->Transaccion;
                $this->view->tituloprop=$DetaProp[0]->NombreCategoria.' - '.$DetaProp[0]->NombrePropiedad;
                $this->view->uid=$DetaProp[0]->Uid;

                //arreglo para ubicar en el mapa
                $enmapa=Array ('cc' =>$DetaProp[0]->ZipCode , 'city' => $DetaProp[0]->Ciudad, 'zip' =>'' ,'lat' =>$DetaProp[0]->Latitud , 'lng' => $DetaProp[0]->Longitud );
                $enmapa=(object)$enmapa;
                $enmapa=array($enmapa);
                if (!session_id()) {
                    session_start();
                }
                $_SESSION['posmapa']=$enmapa;

                $ListValue=$this->ObtenerDatosUsuario($this->view->uid);
                $this->view->username=$ListValue[0]['first_name'];
                $this->view->usernames=$ListValue[0]['name'];
                if(strlen($ListValue[0]['pic_big']) == 0) {
                    $pic = '/img/q_silhouette.gif';
                } else {
                    $pic = $ListValue[0]['pic_big'];
                }
                $this->view->profile=$pic;
                $location=$ListValue[0]['current_location'];
                $this->view->direccion=$location['city'].', '.$location['country'];

            }

            $TotProp=$db->fetchAll("SELECT count(CodigoPropiedad) as total from propropiedad where CodigoEdoPropiedad=2 and Uid=".$this->view->uid);
            $this->view->cantprop=$TotProp[0]->total;
            $Atributos=$this->ObtenerDatosAtributos($IdProp);
            foreach ($Atributos as $val) {
                $SiNo='';
                if(strtoupper($val->ValorS_N)=='S')
                    $SiNo='Si';
                else if(strtoupper($val->ValorS_N)=='N')
                    $SiNo='No';

                $caracteristicasprop.="<span>".$val->NombreAtributo.' '.$val->ValorEntero.$val->ValorDecimal.$SiNo.$val->ValorCaracter."</span> ";
            }
            $this->view->caracteristicas=$caracteristicasprop;
            $Beneficios=$this->ObtenerDatosBeneficios($IdProp);
            $beneficiosprop='';
            foreach ($Beneficios as $val){
                $beneficiosprop.="<li>".$val->NombreBeneficio."</li>";
            }
            $this->view->beneficiosprop=$beneficiosprop;
        }

    }

    // Duplicate ObtenerDatosProPropiedad() funtion. Use the one at /modes/Hm/Pro/Propiedad.php
    /*
    public function ObtenerDatosProPropiedad($IdProp) {
        $db = Zend_Registry::get('db');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $DetaProp=$db->fetchAll("SELECT P.NombrePropiedad,P.DescPropiedad,P.Direccion,
                                    P.Calle,P.Ciudad,P.Estado,CP.Pais,P.ZipCode,P.Area,P.UnidadMedida,
                                    P.AreaLote,P.UnidadMedidaLote,P.PrecioVenta,P.PrecioAlquiler,
                                    case P.Accion
                                        when 'S' then 'Sales'
                                        when 'R' then 'Rental'
                                        when 'B' then 'Sales/Rental'
                                    end as Transaccion, C.NombreCategoria,P.Uid,P.Latitud,P.Longitud
                                    from propropiedad P
                                    left join catpais CP on CP.ZipCode=P.ZipCode
                                    inner join catcategoria C on P.CodigoCategoria=C.CodigoCategoria
                                    where P.CodigoPropiedad=$IdProp");

        return $DetaProp;
    }
    */
}
?>