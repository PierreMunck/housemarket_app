<?php

class AutorizarController extends Zend_Controller_Action {

    protected $_application ;
    protected $_flashMessenger = null;
    protected $_redirector = null;

    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        $this->initView();

        $this->view->web_host = Zend_Registry::get("web_host");
        $this->view->web_path = Zend_Registry::get("web_path");

        Zend_Loader::loadClass('Users');

        $this->view->menu_item = 2;

        $this->view->user = Zend_Auth::getInstance()->getIdentity();

    }

    public function preDispatch() {
        $auth = Zend_Auth::getInstance();
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        if (!$auth->hasIdentity()) {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/auth/login');
        }
        $test_user = Zend_Auth::getInstance()->getIdentity();
        if($test_user->tipo != 'Administrador') {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/creditos');
        }
    }

    public function indexAction() {
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->view->headTitle('Housemarket << Autorizar Pedidos >>');

        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');

        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
    }

    public function eliminacreditosAction() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        if(!$this->getRequest()->isPost()) {
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));
            $keys_form =  array('CodigoCredito');
            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }
            $filter = array (
                    '*' => array('StringTrim', 'StripTags'),
                    'CodigoCredito' => 'Alnum'
            );
            $validators = array(
                    'CodigoCredito' => 'Alnum'
            );
            $input = new Zend_Filter_Input($filter, $validators, $valid_data);

            if($input->isValid()) {
                $verificaDetalle=$db->query("select CodigoCreditoProp,CodigoCredito from crecreditopropiedad where CodigoCredito=".$input->CodigoCredito);
                $resverifica=$verificaDetalle->fetchAll();
                $db->beginTransaction();
                if(count($resverifica)==0) {
                    if($db->delete('crecredito', 'CodigoCredito =' . $input->CodigoCredito )) {
                            $db->commit();
                            echo '{"success": true}';
                            exit();
                        }
                        else {
                            $db->rollback();
                            echo '{"success": false, "errormsg": "fallo eliminación de datos."}';
                            exit();
                        }
                }
                else {
                    if($db->delete('crecreditopropiedad', 'CodigoCredito =' . $input->CodigoCredito )) {
                        if($db->delete('crecredito', 'CodigoCredito =' . $input->CodigoCredito )) {
                            $db->commit();
                            echo '{"success": true}';
                            exit();
                        }
                        else {
                            $db->rollback();
                            echo '{"success": false, "errormsg": "fallo eliminación de datos."}';
                            exit();
                        }
                    }
                    else {
                        $db->rollback();
                        echo '{"success": false, "errormsg": "fallo eliminación de datos."}';
                        exit();
                    }
                }
            }else {
                echo '{"success":"false", "msg": "method get no allowed"}';
            }
            exit();
        }
    }

    public function getcreditosAction() {
        $log = Zend_Registry::get('log');
        if($this->getRequest()->isPost()) {
            try {
                $uid=0;
                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 10);
                $sort = $this->_request->getParam('sort', "CodigoCredito");
                $dir = $this->_request->getParam('dir', "DESC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);
                $log->info(__METHOD__ . __LINE__ . " $sort,$dir,$start,$limit");
                $rs = Dgt_Data_Utileria::getCreditos($sort,$dir,$start,$limit);
                $rs_count = Dgt_Data_Utileria::getCountCreditos();
                $results = $rs_count[0]->total;
                if (!empty($rs)) {
                    echo '{"success":true, "results":'.$results.', "rows":'.Zend_Json::encode($rs) .'}';
                }
                else {
                    echo '{"success":"false", "results":0, "rows":0}';
                }
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la sessión.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }

    public function actualizacreditosAction() {
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->view->url=$web_host.$web_path;
        $key = Zend_Registry::get('key');
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        if(!$this->getRequest()->isPost()) {
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));
            $keys_form =  array('CodigoCredito');
            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }
            $filter = array (
                    '*' => array('StringTrim', 'StripTags'),
                    'CodigoCredito' => 'Alnum'
            );
            $validators = array(
                    'CodigoCredito' => 'Alnum'
            );
            $input = new Zend_Filter_Input($filter, $validators, $valid_data);
            if($input->isValid()) {
                try {
                    /*$log = Zend_Registry::get('log');
                    $log->info(__METHOD__ . "(" . __LINE__ . ") " . implode(" , ", $valid_data));
                    $log->info(__METHOD__ . "(" . __LINE__ . ") " . $input->CodigoCredito);*/
                    $consult = $db->query("select CodigoParametro, ValorParametro from parametros where NombreParametro='DIAS_VIGENCIA_CREDITO'");
                    $result=$consult->fetchAll();
                    //$log->info(__METHOD__ . "(" . __LINE__ . ") " .new Zend_Db_Expr("ADDDATE(CURDATE(), INTERVAL ". $result[0]->ValorParametro  ." DAY)"));
                    if(count($result)==0) {
                        echo '{"success": false, "errormsg": "Error de parametro"}';
                        exit();
                    }
                    $update_credito =array(
                            'FechaCompra' => new Zend_Db_Expr('CURDATE()'),
                            'FechaVence' => new Zend_Db_Expr("ADDDATE(CURDATE(), INTERVAL ". $result[0]->ValorParametro ." DAY)"),
                            'EstadoCredito' => 'A'
                    );

                    $update_detalle = array(
                            'FechaAsigna' => new Zend_Db_Expr('CURDATE()'),
                            'EstadoAsigna' => 'A'
                    );
                    $db->beginTransaction();
                    if($db->update('crecredito', $update_credito, 'CodigoCredito = '.$input->CodigoCredito)) {
                        if($db->update('crecreditopropiedad', $update_detalle, 'CodigoCredito = ' . $input->CodigoCredito)) {
                            $db->commit();
                            echo '{ success: true }';
                            exit();
                        }
                        else {
                            $db->rollback();
                            echo '{"success": false, "errormsg": "fallo actualización de datos."}';
                            exit();
                        }
                    }
                    else {
                        $db->rollback();
                        echo '{"success": false, "errormsg": "fallo actualización de datos."}';
                        exit();
                    }
                } catch (Exception $e) {
                    echo '{"success": false, "errormsg": "fallo insercion de datos.El campo credito debe ser un valor numerico"}';
                    exit();
                }
            }
            if ($input->hasInvalid() || $input->hasMissing()) {
                $messages = $input->getMessages();
                echo '{ "success": false, "errormsg": '. Zend_Json::encode($messages) .' }';
                exit();
            }
        }
    }

    public function getpropiedadesAction() {
        $log = Zend_Registry::get('log');
        if($this->getRequest()->isPost()) {
            try {

                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 10);
                $sort = $this->_request->getParam('sort', "CodigoPropiedad");
                $dir = $this->_request->getParam('dir', "DESC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);
                $CodigoCredito = $f->filter($this->_request->getParam('CodigoCredito',null));

                $rs = Dgt_Data_Utileria::getPropiedad($sort,$dir,$start,$limit,$CodigoCredito);
                $rs_count = Dgt_Data_Utileria::getCountPropiedad($CodigoCredito);
                $results = $rs_count[0]->total;
                if (!empty($rs)) {
                    echo '{"success":true, "results":'.$results.', "rows":'.Zend_Json::encode($rs) .'}';
                }
                else {
                    echo '{"success":"false", "results":0, "rows":0}';
                }
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la sessión.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }

}
