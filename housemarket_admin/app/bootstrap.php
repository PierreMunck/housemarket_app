<?php
require_once 'krumo/class.krumo.php';
require_once 'functions/debug.php';

defined('APPLICATION_PATH')
or define('APPLICATION_PATH', dirname(__FILE__));

date_default_timezone_set('America/Managua');


// para fines de depuracion, comentar en produccion.
error_reporting(E_ALL|E_STRICT);
ini_set('display_errors', 1);

$frontController = Zend_Controller_Front::getInstance();
$frontController->setControllerDirectory(APPLICATION_PATH . '/controllers');

// APPLICATION ENVIRONMENT - Set the current environment
// Set a variable in the front controller indicating the current environment --
// commonly one of development, staging, testing, production, but wholly
// dependent on your organization and site's needs.
$frontController->setParam('env', APPLICATION_ENVIRONMENT);


Zend_Layout::startMvc(APPLICATION_PATH . '/layouts');
$view = Zend_Layout::getMvcInstance()->getView();
$view->doctype('XHTML1_STRICT');

$configuration = new Zend_Config(require APPLICATION_PATH . '/config/settings.php');

try {
    $dbAdapter = Zend_Db::factory($configuration->database->adapter,$configuration->database->params);
    Zend_Db_Table_Abstract::setDefaultAdapter($dbAdapter);
} catch (Zend_Db_Adapter_Exception $e) {
    echo $e->getMessage();
    die("Fallo de conexion, revisar host/username/password");
} catch (Zend_Exception $e) {
    die('Fallo en la carga de la clase Adapter');
}

$key = 'Z{[:/C105e09ashj23!as02nA<Ms0A!@#$_)[K<_%A';

// configurando las sessiones para usar la DB.
$ss_config = $configuration->dbsession;
Zend_Session::setSaveHandler(new Zend_Session_SaveHandler_DbTable($ss_config));
$userOptions = array('name' => 'Xolo', 'use_only_cookies' => 'on');
Zend_Session::setOptions($userOptions);

Zend_Session::start();

$web_host = $configuration->web_host;
$web_path = $configuration->web_path;
$web_con = $configuration->database->params;

$registry = Zend_Registry::getInstance();
$registry->configuration = $configuration;
$registry->dbAdapter     = $dbAdapter;
$registry->web_host = $web_host;
$registry->web_path = $web_path;
$registry->key = $key;
$registry->web_con = $web_con;

// CLEANUP - remove items from global scope
// This will clear all our local boostrap variables from the global scope of
// this script (and any scripts that called bootstrap).  This will enforce
// object retrieval through the Applications's Registry
unset($frontController, $view, $configuration, $dbAdapter, $web_host, 
    $web_path, $key, $registry);

$logger = new Zend_Log();

$writer = 'production' == APPLICATION_ENVIRONMENT ?
    new Zend_Log_Writer_Stream(APPLICATION_PATH . '/data/logs/app.log') :
    new Zend_Log_Writer_Stream(APPLICATION_PATH . '/data/logs/app.log');
//new Zend_Log_Writer_Firebug();
$logger->addWriter($writer);

if ('production' == APPLICATION_ENVIRONMENT) {
    $filter = new Zend_Log_Filter_Priority(Zend_Log::CRIT);
    $logger->addFilter($filter);
}

Zend_Registry::set('log', $logger);