<?php
    require '../app/controllers/eyemysqladap.php';
    require '../app/controllers/eyedatagrid.php';
    $db = Zend_Registry::get('dbAdapter');
    $db->setFetchMode(Zend_Db::FETCH_OBJ);
    $griddb = new EyeMySQLAdap($con->host, $con->username, $con->password, $con->dbname);
    // Load the datagrid class
    $grid = new EyeDataGrid($griddb);
    $grid->setResultsPerPage(15);

    $grid->setQuery("CodigoCliente,NombreCliente,Uid", "cliente");

   if (EyeDataGrid::isAjaxUsed())
    {
	$grid->printTable();
	exit;
    }
?>
<html>
<head>
<title>EyeDataGrid Example 1</title>
 <link rel="stylesheet" type="text/css" href="/administrador/css/table.css" />
</head>
<body>
<h1>Basic Datagrid</h1>
<p>This is a basic example of the datagrid</p>
<?php
// Print the table$grid->printTable();
$grid->printTable();
?>
</body>
</html>