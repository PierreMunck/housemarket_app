<?php

class CreditosController extends Zend_Controller_Action {

    protected $_application ;
    protected $_flashMessenger = null;
    protected $_redirector = null;

    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        $this->initView();

        $this->view->web_host = Zend_Registry::get("web_host");
        $this->view->web_path = Zend_Registry::get("web_path");

        Zend_Loader::loadClass('Users');

        $this->view->menu_item = 1;

        $this->view->user = Zend_Auth::getInstance()->getIdentity();

    }
    public function preDispatch() {
        $auth = Zend_Auth::getInstance();
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        if (!$auth->hasIdentity()) {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/auth/login');
        }
        $test_user = Zend_Auth::getInstance()->getIdentity();
        /*if($test_user->tipo == 'Representante') {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php');
        }*/
    }

    public function indexAction() {
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->view->headTitle("Housemarket << Administrar Creditos >>");
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
    }

    /* public function creditAction() {
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->view->headTitle('Housebook << Administrar Cr�ditos >>');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/table.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
    }

    public function addAction() {
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->view->url=$web_host.$web_path;
        $key = Zend_Registry::get('key');
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        if(!$this->getRequest()->isPost()) {
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));
            $keys_form = array('idpropiedad', 'creditos');
            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }
            $filter = array (
                    '*' => array('StringTrim', 'StripTags'),
                    'idpropiedad' => 'Alnum',
                    'creditos' => 'Digits'
            );
            $validators = array(
                    'idpropiedad' => 'Alnum',
                    'creditos' => 'Digits'
            );
            $input = new Zend_Filter_Input($filter, $validators, $valid_data);
            //$input->addValidatorPrefixPath('Dgt_Validate', 'Dgt/Validate'); // Agregando Custom Validator.
            if($input->isValid()) {
                $update_data = array(
                        'creditos' => $input->creditos,
                        'destacada'=>1,
                        'ultima_modificacion' => new Zend_Db_Expr('CURDATE()'),
                        'fecha_asignacion_credito' => new Zend_Db_Expr('CURDATE()'),
                        'fecha_expiracion_credito' => new Zend_Db_Expr('ADDDATE(CURDATE(), INTERVAL 1 MONTH)'),
                );
                if($db->update('propiedad', $update_data, 'idpropiedad = ' . $input->idpropiedad)) {
                    echo '{ success: true }';
                    exit();
                    //$this->_redirector->gotoUrl($web_host.$web_path.'/index.php/user/list');
                } else {
                    echo '{"success": false, "errormsg": "fallo insercion de datos.El campo credito debe ser un valor numerico"}';
                    exit();
                }
            }
            if ($input->hasInvalid() || $input->hasMissing()) {
                $messages = $input->getMessages();
                echo '{ "success": false, "errormsg": '. Zend_Json::encode($messages) .' }';
                exit();
            }
        }
    }

    public function editAction() {
        $key = Zend_Registry::get('key');
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        if(!$this->getRequest()->isPost()) {
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));
            $keys_form = array('idpropiedad','creditos', 'fecha_expiracion_credito');
            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }
            $filter = array (
                    '*' => array('StringTrim', 'StripTags'),
                    'creditos' => 'Digits',
                    'fecha_expiracion_credito' => 'Alnum',
                    'idpropiedad' => 'Alnum'
            );
            $validators = array(
                    'creditos' => 'Digits',
                    'fecha_expiracion_credito' => 'Alnum',
                    'idpropiedad' => 'Alnum'
            );
            $input = new Zend_Filter_Input($filter, $validators, $valid_data);
            //$input->addValidatorPrefixPath('Dgt_Validate', 'Dgt/Validate'); // Agregando Custom Validator.
            if($input->isValid()) {
                $update_data = array(
                        'creditos' => $input->creditos,
                        'fecha_expiracion_credito' => $input->fecha_expiracion_credito,
                        'ultima_modificacion' => new Zend_Db_Expr('CURDATE()')
                );
                if($db->update('propiedad', $update_data, 'idpropiedad = ' . $input->idpropiedad)) {
                    echo '{ "success": true }';
                    exit();
                } else {
                    echo '{"success": false, "errormsg": "fallo actualizacion de datos"}';
                    exit();
                }
            }
            if ($input->hasInvalid() || $input->hasMissing()) {
                $messages = $input->getMessages();
                echo '{ "success": false, "errormsg": '. Zend_Json::encode($messages) .' }';
                exit();
            }
        }
    }

    public function deleteAction() {
        $db = Zend_Registry::get('dbAdapter');
        if( $this->getRequest()->isPost()) {
            $id = $this->_request->getParam('id', null);
            if(isset ($id) && is_numeric($id)) {
                $rs = $db->delete('usuarios', 'idusuario =' . $id );
                echo '{"success": true}';
                exit();
            } else {
                echo '{"success": false}';
                exit();
            }
        } else {
            echo '{"success": false}';
            exit();
        }
    }

    public function getregistersAction() {
        if($this->getRequest()->isPost()) {
            try {
                $uid=0;//100000381123831;
                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 50);
                $sort = $this->_request->getParam('sort', "idpropiedad");
                $dir = $this->_request->getParam('dir', "ASC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);
                $pais = $f->filter($this->_request->getParam('pais',null));
                $ciudad = $f->filter($this->_request->getParam('ciudad',null));
                $direccion = $f->filter($this->_request->getParam('direccion',null));
                //$city = $f->filter($this->_request->getParam('ciudad', null));
                //$compra = $f->filter($this->_request->getParam('buy', 0));
                // $alquiler = $f->filter($this->_request->getParam('rent', 0));
                $tipo = $f->filter($this->_request->getParam('tipo', null));
                $min_creditos = $f->filter($this->_request->getParam('min_creditos', 0));
                $max_creditos = $f->filter($this->_request->getParam('max_creditos', 0));
                $min_price = $f->filter($this->_request->getParam('min_price', 0));
                $max_price = $f->filter($this->_request->getParam('max_price', 0));
                $min_beds = $f->filter($this->_request->getParam('min_beds', null));
                $max_beds = $f->filter($this->_request->getParam('max_beds', null));
                $min_baths = $f->filter($this->_request->getParam('min_baths', null));
                $max_baths = $f->filter($this->_request->getParam('max_baths', null));
                $area = $f->filter($this->_request->getParam('area', null));
                $cat = $f->filter($this->_request->getParam('cat', null));
                $fecha_ac = $f->filter($this->_request->getParam('fecha_ac', null));
                $fecha_cc = $f->filter($this->_request->getParam('fecha_cc', null));


                //Zend_Debug::dump(  __METHOD__ . " $pais,  $dir, $tipo, $min_price, $max_price, $beds, $baths, $area, $cat");


                $rs = Dgt_Data_Utils::getProperty($sort,$dir,$start, $limit, $uid,trim($pais),$ciudad,$direccion, $tipo,$min_creditos,$max_creditos, $min_price, $max_price, $min_beds, $max_beds,$min_baths,$max_baths, $area, $cat,$fecha_ac,$fecha_cc);
                $rs_count = Dgt_Data_Utils::getPropertyCount($start, $limit,$uid,trim($pais), $ciudad, $direccion, $tipo,$min_creditos,$max_creditos, $min_price, $max_price,$min_beds,$max_beds,$min_baths,$max_baths, $area, $cat,$fecha_ac,$fecha_cc);
                $results = $rs_count[0]->total;
                if (!empty($rs)) {
                    echo '{"success":true, "results":'.$results.', "rows":'.Zend_Json::encode($rs) .'}';
                }else {
                    // si ciudad falla, buscar por pais.
                    $city = null;
                    $rs = Dgt_Data_Utils::getProperty($sort,$dir,$start, $limit, $uid,trim($pais),$ciudad, $direccion, $tipo,$min_creditos,$max_creditos, $min_price, $max_price, $min_beds, $max_beds,$min_baths,$max_baths, $area, $cat,$fecha_ac,$fecha_cc);
                    $rs_count = Dgt_Data_Utils::getPropertyCount($start, $limit,$uid,trim($pais), $ciudad, $direccion, $tipo,$min_creditos,$max_creditos, $min_price, $max_price, $min_beds, $max_beds,$min_baths,$max_baths, $area, $cat,$fecha_ac,$fecha_cc);
                    $results = $rs_count[0]->total;
                    if(!empty($rs)) {
                        echo '{"success":true, "results":'.$results.', "rows":'.Zend_Json::encode($rs) .'}';
                    } else {
                        echo '{"success":"false", "results":0, "rows":0}';
                    }
                }
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la sessión.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
    }

    public function updatecaducidadAction() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        if($this->getRequest()->isGet()) {
            try {
                $date=Zend_Date::now();
                echo $date;
                echo "<br>";

                $sql = 'SELECT
                      propiedad.idpropiedad,
                      propiedad.fecha_ingreso,
                      propiedad.ultima_modificacion,
                      propiedad.estatus,
                      propiedad.creditos,
                      propiedad.fecha_asignacion_credito,
                      propiedad.fecha_expiracion_credito
                       FROM
                      propiedad where fecha_expiracion_credito<CURDATE()';

                $rs = $db->query($sql);
                $propiedades_caducadas=$rs->fetchAll();
                $idpropiedades=array();
                foreach($propiedades_caducadas as $key=>$value ) {
                    $idpropiedades[]=$value->idpropiedad;
                }
                $update_data = array(
                        'creditos' => 0,
                        'destacada' => 0
                );
                $where['idpropiedad IN(?)']= $idpropiedades;

                if($db->update('propiedad', $update_data,$where)) {
                    echo 'Actualizacion exitosa. '.count($idpropiedades)." propiedades actualizadas";
                    exit();
                } else {
                    echo "No hay propiedades con los creditos caducados";
                    exit();
                }
                echo 'Actualizacion exitosa';
                exit();
            } catch (Exception $e) {
                echo '{"success": false;Problemas de conexion}';
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method Post no allowed"}';
        }
    }

    public function fechatimeAction() {
        $date=Zend_Date::now();
        echo $date;
        exit();
    }*/

    public function getcreclienteAction() {
        $log = Zend_Registry::get('log');
        if($this->getRequest()->isPost()) {
            try {
                $uid=0;
                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 10);
                $sort = $this->_request->getParam('sort', "CodigoCliente");
                $dir = $this->_request->getParam('dir', "DESC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);
                $CodigoCliente = $f->filter($this->_request->getParam('CodigoCliente',null));
                $log->info(__METHOD__ . __LINE__ . " $sort,$dir,$start,$limit,$CodigoCliente");
                $rs = Dgt_Data_Utils::getCreCliente($sort,$dir,$start,$limit,$CodigoCliente);
                $rs_count = Dgt_Data_Utils::getCreClienteCount($CodigoCliente);
                $results = $rs_count[0]->total;
                if (!empty($rs)) {
                    echo '{"success":true, "results":'.$results.', "rows":'.Zend_Json::encode($rs) .'}';
                }
                else {
                    echo '{"success":"false", "results":0, "rows":0}';
                }
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la sessión.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }

    public function creeditclienteAction() {
        $key = Zend_Registry::get('key');
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        if(!$this->getRequest()->isPost()) {
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));
            $keys_form = array('CodigoCredito','CodigoCliente','FechaVence','Cantidad','Saldo');
            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }
            $filter = array (
                    '*' => array('StringTrim', 'StripTags'),
                    'FechaVence' => 'Alnum',
                    'CodigoCliente' => 'Alnum',
                    'Cantidad' => 'Alnum',
                    'Saldo' => 'Alnum',
                    'CodigoCredito' => 'Alnum'
            );
            $validators = array(
                    'FechaVence' => 'Alnum',
                    'CodigoCliente' => 'Alnum',
                    'Cantidad' => 'Alnum',
                    'Saldo' => 'Alnum',
                    'CodigoCredito' => 'Alnum'
            );
            $input = new Zend_Filter_Input($filter, $validators, $valid_data);
            if($input->isValid()) {
                $update_data = array(
                        'FechaVence' => $input->FechaVence,
                        'FechaCompra' => new Zend_Db_Expr('CURDATE()'),
                        'Cantidad' => $input->Cantidad,
                        'Saldo' => $input->Saldo
                );
                try {
                    $db->update('crecredito', $update_data, 'CodigoCredito = ' . $input->CodigoCredito);
                    echo '{ "success": true }';
                    exit();
                } catch (Exception $e) {
                    echo '{"success": false, "errormsg": "fallo actualizacion de datos"}';
                    exit();
                }
            }
            if ($input->hasInvalid() || $input->hasMissing()) {
                $messages = $input->getMessages();
                echo '{ "success": false, "errormsg": '. Zend_Json::encode($messages) .' }';
                exit();
            }
        }
    }

    public function creclienteAction() {
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->view->url=$web_host.$web_path;
        $key = Zend_Registry::get('key');
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        if(!$this->getRequest()->isPost()) {
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));
            $keys_form =  array('CodigoCliente','Cantidad','Monto');
            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }
            $valTemp=implode(',', $valid_data);
            $valPost=explode(',',$valTemp);
            $filter = array (
                    '*' => array('StringTrim', 'StripTags'),
                    'CodigoCliente' => 'Alnum',
                    'Cantidad' => 'Alnum'
            );
            $validators = array(
                    'CodigoCliente' => 'Alnum',
                    'Cantidad' => 'Alnum'
            );
            $input = new Zend_Filter_Input($filter, $validators, $valid_data);
            if($input->isValid()) {
                try {
                    $uid=Zend_Auth::getInstance()->getIdentity();
                    $consultUser = $db->query('select idusuario, CodigoRepresenta from representante where idusuario='. $uid->idusuario);
                    $resultUser=$consultUser->fetchAll();
                    if(count($resultUser)==0) {
                        echo '{"success": false, "errormsg": "Usted no tiene un representante"}';
                        exit();
                    }
                    $insert_data = array(
                            'CodigoCliente' => $input->CodigoCliente,
                            'Cantidad'=> $input->Cantidad,
                            'Saldo'=> $input->Cantidad,
                            'EstadoCredito' => 'P',
                            'FechaPedido' => new Zend_Db_Expr('CURDATE()'),
                            'MontoCompra' =>$valPost[2],
                            'CodigoRepresentante' => $resultUser[0]->CodigoRepresenta,
                            'TipoCompra' => 'Representante'
                    );
                    if($db->insert('crecredito', $insert_data)) {
                        echo '{ success: true }';
                        exit();
                    }
                    else {
                        echo '{"success": false, "errormsg": "fallo insercion de datos."}';
                        exit();
                    }
                } catch (Exception $e) {
                    echo '{"success": false, "errormsg": "fallo insercion de datos.El campo credito debe ser un valor numerico"}';
                    exit();
                }
            }
            if ($input->hasInvalid() || $input->hasMissing()) {
                $messages = $input->getMessages();
                echo '{ "success": false, "errormsg": '. Zend_Json::encode($messages) .' }';
                exit();
            }
        }
    }

    public function totalcreditAction() {
        $log = Zend_Registry::get('log');
        if($this->getRequest()->isPost()) {
            try {

                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 10);
                $sort = $this->_request->getParam('sort', "CodigoPropiedad");
                $dir = $this->_request->getParam('dir', "DESC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);
                $CodigoCliente = $f->filter($this->_request->getParam('CodigoCliente',null));
                $Categoria = $f->filter($this->_request->getParam('Categoria',-1));
                $Pais = $f->filter($this->_request->getParam('Pais',null));
                $Transaccion = $f->filter($this->_request->getParam('Transaccion',null));
                $Ciudad = $f->filter($this->_request->getParam('Ciudad',null));
                $Broker = $f->filter($this->_request->getParam('Broker',null));

                //$log->info(__METHOD__ . __LINE__ . " $sort,$dir,$start,$limit,$CodigoCliente,$Categoria,$Pais");
                $rs = Dgt_Data_Utils::getProPropiedad($sort,$dir,$start,$limit,$CodigoCliente,$Categoria,$Pais,$Transaccion,$Ciudad,$Broker);
                $rs_count = Dgt_Data_Utils::getProPropiedadCount($CodigoCliente,$Categoria,$Pais,$Transaccion,$Ciudad,$Broker);
                $results = $rs_count[0]->total;
                if (!empty($rs)) {
                    echo '{"success":true, "results":'.$results.', "rows":'.Zend_Json::encode($rs) .'}';
                }
                else {
                    echo '{"success":"false", "results":0, "rows":0}';
                }
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la sessión.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }

    public function actpropiedadAction() {
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->view->url=$web_host.$web_path;
        $key = Zend_Registry::get('key');
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        /* $log = Zend_Registry::get('log');
        $log->info(__METHOD__ . "(" . __LINE__ . ") " . implode(" , ", $_POST));*/

        if(!$this->getRequest()->isPost()) {
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));
            $keys_form =  array('CodigoCredito','CodigoPropiedad','CreditoMaximo','Cantidad','Estado');
            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }

            /*$log->info(__METHOD__ . "(" . __LINE__ . ") " . implode(",", $valid_data));*/

            $filter = array (
                    '*' => array('StringTrim', 'StripTags'),
                    'CodigoCredito' => 'Alnum',
                    'CodigoPropiedad' => 'Alnum',
                    'CreditoMaximo' => 'Alnum',
                    'Cantidad' => 'Alnum',
                    'Estado' => 'Alpha'
            );

            $validators = array(
                    'CodigoCredito' => 'Alnum',
                    'CodigoPropiedad' => 'Alnum',
                    'CreditoMaximo' => 'Alnum',
                    'Cantidad' => 'Alnum',
                    'Estado' => 'Alpha'
            );

            $input = new Zend_Filter_Input($filter, $validators, $valid_data);

            /*  $log->info(__METHOD__ . htmlentities("541"));
            $log->info(__METHOD__ . htmlentities("541", ENT_COMPAT));
            $log->info(__METHOD__ . htmlentities("541", ENT_COMPAT, "ISO-8859-1"));
            $log->info(__METHOD__ . htmlentities("541<b>", ENT_COMPAT, "ISO-8859-1", false));*/

            if($input->isValid()) {
                //insertando en crecreditopropiedad
                $insert_data = array(
                        'CodigoCredito' => $input->CodigoCredito,
                        'CodigoPropiedad'=>$input->CodigoPropiedad,
                        'Cantidad'=>$input->Cantidad,
                        'EstadoAsigna' =>$input->Estado
                );
                try {
                    $consultCredit = $db->query('select CodigoCredito,Saldo from crecredito where CodigoCredito='. $input->CodigoCredito);
                    $resultCredit=$consultCredit->fetchAll();
                    //actualizando crecredito
                    //$saldo=$input->CreditoMaximo-$input->Cantidad;
                    $saldo=$resultCredit[0]->Saldo-$input->Cantidad;
                    if($input->Estado=='A') {
                        $insert_data['FechaAsigna']=new Zend_Db_Expr('CURDATE()');
                    }
                    if($saldo>=0) {
                        $update_data = array(
                                'Saldo' => $saldo
                        );
                        $db->beginTransaction();
                        if($db->insert('crecreditopropiedad', $insert_data)) {
                            if($db->update('crecredito', $update_data, 'CodigoCredito = ' . $input->CodigoCredito)) {
                                $db->commit();
                                echo '{ success: true }';
                                exit();
                            }
                            else {
                                $db->rollback();
                                echo '{"success": false, "errormsg": "fallo actualizaci�n de datos."}';
                                exit();
                            }
                        }
                        else {
                            $db->rollback();
                            echo '{"success": false, "errormsg": "fallo insercion de datos."}';
                            exit();
                        }
                    }
                    else {
                        echo '{"success": false, "errormsg": "fallo insercion de datos. No hay suficiente saldo"}';
                        exit();
                    }
                }
                catch (Exception $e) {
                    //$db->rollBack();
                    echo '{"success": false, "errormsg": "fallo insercion de datos.El campo credito debe ser un valor numerico"}';
                    exit();
                }


            }
            if ($input->hasInvalid() || $input->hasMissing()) {
                $messages = $input->getMessages();
                echo '{ "success": false, "errormsg": '. Zend_Json::encode($messages) .' }';
                exit();
            }
        }
    }

    public function detallecreditAction() {
        $log = Zend_Registry::get('log');
        if($this->getRequest()->isPost()) {
            try {

                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 10);
                $sort = $this->_request->getParam('sort', "CodigoCreditoProp");
                $dir = $this->_request->getParam('dir', "DESC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);
                $CodigoCredito = $f->filter($this->_request->getParam('CodigoCredito',-1));
                $CodigoPropiedad = $f->filter($this->_request->getParam('CodigoPropiedad',-1));

                //$log->info(__METHOD__ . __LINE__ . " $sort,$dir,$start,$limit,$CodigoCliente,$Categoria,$Pais");
                $rs = Dgt_Data_Utils::getDetalleCredito($sort,$dir,$start,$limit,$CodigoCredito,$CodigoPropiedad);
                $rs_count = Dgt_Data_Utils::getDetalleCreditoCount($CodigoCredito,$CodigoPropiedad);
                $results = $rs_count[0]->total;
                if (!empty($rs)) {
                    echo '{"success":true, "results":'.$results.', "rows":'.Zend_Json::encode($rs) .'}';
                }
                else {
                    echo '{"success":"false", "results":0, "rows":0}';
                }
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la sessi�n.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }

    public function eliminarcreditosAction() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        if(!$this->getRequest()->isPost()) {
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));
            $keys_form =  array('CodigoCreditoProp','CodigoCredito','Cantidad');
            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }
            $filter = array (
                    '*' => array('StringTrim', 'StripTags'),
                    'CodigoCreditoProp' => 'Alnum',
                    'CodigoCredito' => 'Alnum',
                    'Cantidad' => 'Alnum'
            );
            $validators = array(
                    'CodigoCreditoProp' => 'Alnum',
                    'CodigoCredito' => 'Alnum',
                    'Cantidad' => 'Alnum'
            );
            $input = new Zend_Filter_Input($filter, $validators, $valid_data);

            if($input->isValid()) {
                $verificaCredito=$db->query("select CodigoCredito,Saldo from crecredito where CodigoCredito=".$input->CodigoCredito);
                $resverifica=$verificaCredito->fetchAll();
                $db->beginTransaction();
                if(count($resverifica)>0) {
                    if($db->delete('crecreditopropiedad', 'CodigoCreditoProp =' . $input->CodigoCreditoProp )) {
                        $update_data = array(
                                'Saldo' => $resverifica[0]->Saldo + $input->Cantidad
                        );

                        if($db->update('crecredito', $update_data, 'CodigoCredito = ' . $input->CodigoCredito)) {
                            $db->commit();
                            echo '{"success": true}';
                            exit();
                        }
                        else {
                            $db->rollback();
                            echo '{"success": false, "errormsg": "fallo eliminaci�n de datos."}';
                            exit();
                        }

                    }
                    else{
                        $db->rollback();
                        echo '{"success": false, "errormsg": "fallo eliminaci�n de datos."}';
                        exit();
                    }
                }
                else {
                    $db->rollback();
                    echo '{"success": false, "errormsg": "fallo eliminaci�n de datos."}';
                    exit();
                }
            }else {
                echo '{"success":"false", "msg": "method get no allowed"}';
            }
            exit();
        }
    }


}
