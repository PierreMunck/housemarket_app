<?php
class ReportesController extends Zend_Controller_Action {
    protected $_flashMessenger = null;
    protected $_redirector = null;

    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        $this->initView();

        $this->view->web_host = Zend_Registry::get("web_host");
        $this->view->web_path = Zend_Registry::get("web_path");

        $this->view->menu_item = 3;

        $this->view->user = Zend_Auth::getInstance()->getIdentity();
    }

    public function preDispatch() {
        $auth = Zend_Auth::getInstance();
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');

        if (!$auth->hasIdentity()) {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/auth/login');
        }
        $test_user = Zend_Auth::getInstance()->getIdentity();
        if($test_user->tipo == 'Operador') {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php');
        }
        /*
        if($test_user->usuario != 'admin') {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php');
        }elseif($test_user->usuario != 'admin') {
             $this->_redirector->gotoUrl($web_host.$web_path.'/index.php');
        }
        */

    }

    public function indexAction() {
        $this->view->headTitle('Housebook << Reportes >>');
    }

    public function doreportAction() {
        $f = new Zend_Filter_StripTags();
        if($this->getRequest()->isPost()) {
            $f1 = $f->filter( $this->_request->getParam('startdt'));
            $f2 = $f->filter( $this->_request->getParam('enddt'));
            $rId = $f->filter( $this->_request->getParam('reportid'));
            $usrId = $f->filter( $this->_request->getParam('usrId'));

            if ($rId == 1) {
                $rs = Dgt_Data_Utils::reportConsultas($f1, $f2, $usrId);
                echo '{ "rows": '. Zend_Json::encode($rs) .'}';

            }
            if($rId == 2) {
                $rs = Dgt_Data_Utils::reportRegistrados($f1, $f2, $usrId);
                echo '{ "rows": '. Zend_Json::encode($rs) .'}';
            }
        }
        exit();
    }

    public function tocsvAction() {
        $f = new Zend_Filter_StripTags();
        //if($this->getRequest()->isPost()) {
            $f1 = $f->filter( $this->_request->getParam('startdt'));
            $f2 = $f->filter( $this->_request->getParam('enddt'));
            $rId = $f->filter( $this->_request->getParam('reportid'));
            $usrId = $f->filter( $this->_request->getParam('usrId'));

            if ($rId == 1) {
                $rs = Dgt_Data_Utils::reportConsultas($f1, $f2, $usrId);
                $csv = "Id,Operador,Numero_Consultas \n";
                foreach($rs as $row) {
                    $csv .= $row->id . ',' . $row->operador. ',' . $row->numero_consultas ." \n";
                }
            //Zend_Debug::dump($rs);
            //echo $csv;

            }
            if($rId == 2) {
                $rs = Dgt_Data_Utils::reportRegistrados($f1, $f2, $usrId);
                $csv = "id,operador,movil_registrado,fecha \n";
                foreach($rs as $row) {
                    $csv .= $row->id . ',' . $row->operador. ',' . $row->movil_registrado . ',' . $row->fecha . " \n";
                }
            //Zend_Debug::dump($rs);
            //echo $csv;
            }
            header('Pragma: public');
            header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
            header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
            header ("Pragma: no-cache");
            header("Expires: 0");
            header('Content-Transfer-Encoding: none');
            header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
            header("Content-type: application/x-msexcel");                    // This should work for the rest
            header('Content-Disposition: attachment; filename="Reporte-'. date("Y-m-d_h-i-s-A").'.csv"');
            print $csv;
       // }
        exit();
    }
}
