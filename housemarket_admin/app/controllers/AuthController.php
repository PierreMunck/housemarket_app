<?php
class AuthController extends Zend_Controller_Action{

    protected $_application ;
    protected $_flashMessenger = null;
    protected $_redirector = null;

    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        $this->initView();

        $this->view->web_host = Zend_Registry::get("web_host");
        $this->view->web_path = Zend_Registry::get("web_path");

    }


    public function loginAction() {
        $db = Zend_Registry::get('dbAdapter');
        $key = Zend_Registry::get('key');

        $web_host = Zend_Registry::get("web_host");
        $web_path = Zend_Registry::get("web_path");

        $this->view->headTitle('Housemarket << Login >>');

        if ($this->_request->isPost()){
            //Zend_Debug::dump($this->_request->getPost());

            $f = new Zend_Filter_StripTags();
            $username = $f->filter($this->_request->getPost('username'));
            $password = $f->filter($this->_request->getPost('password'));

            if( empty($username) || empty($password) ) {
                $this->_flashMessenger->addMessage('Ingrese su nombre de usuario y clave');
                $this->view->messages = $this->_flashMessenger->getCurrentMessages();
                //$this->view->messages = 'Ingrese su nombre de usuario y clave';
            } else {

                $authAdapter = new Zend_Auth_Adapter_DbTable($db);
                $authAdapter->setTableName('usuarios');
                $authAdapter->setIdentityColumn('usuario');
                $authAdapter->setCredentialColumn('clave');

                $authAdapter->setIdentity($username);

                // concatenar con llave estatica
                $authAdapter->setCredential($key . $password);

                // convertir password en sha1 hash.
                // status = 0   cuenta bloqueada
                // status = 1   cuenta habilitada.
                $authAdapter->setCredentialTreatment('SHA1(?) AND estatus =1');

                $auth = Zend_Auth::getInstance();

                $result = $auth->authenticate($authAdapter);

                switch ($result->getCode()) {

                    case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
                        // usuario no existe
                        $this->_flashMessenger->addMessage('El usuario no existe en este sistema');
                        $this->view->messages = $this->_flashMessenger->getCurrentMessages();
                        //$this->view->messages = 'El usuario no existe en este sistema';
                        break;

                    case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:
                        // identidad valida, credencial erronea
                        
                        $user_name = $result->getIdentity();
                        $sql = "select idusuario, estatus from usuarios where usuario = ?";

                        $db->setFetchMode(Zend_Db::FETCH_OBJ);
                        $row = $db->fetchAll($sql, $user_name);

                        $status_code = (int) $row[0]->estatus;
                        $id_user = (int) $row[0]->idusuario;
                        if( $status_code == 0 ) {
                            $this->_flashMessenger->addMessage('La cuenta se encuentra desactivada');
                            $this->view->messages = $this->_flashMessenger->getCurrentMessages();
                            //$this->view->messages = 'La cuenta se encuentra desactivada';
                        } else {
                            $this->_flashMessenger->addMessage('El password ingresado es incorrecto');
                            $this->view->messages = $this->_flashMessenger->getCurrentMessages();
                            //$this->view->messages = 'El password ingresado es incorrecto';
                        }
                        //$log->log("usuario = " . $result->getIdentity() . " code = " . $status_code, Zend_Log::INFO);
                        break;

                    case Zend_Auth_Result::SUCCESS:
                        $returnColumns = array ('idusuario', 'usuario', 'estatus', 'tipo', 'nombre', 'apellido', 'email');
                        $data = $authAdapter->getResultRowObject($returnColumns);
                        $auth->getStorage()->write($data);
                        Zend_Session::regenerateId();
                        $timestamp =  date("Y-m-d H:i:s", time());
                        $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/creditos');
                        //$this->_redirector->gotoUrl($web_host.$web_path.'/index.php/useradmin');

                        break;

                    case Zend_Auth_Result::FAILURE:
                        // fallo desconcido
                        $this->_flashMessenger->addMessage("Fallo desconocido");
                        $this->view->messages = $this->_flashMessenger->getCurrentMessages();
                        $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/auth/login');
                        //$log->log("Acceso fallido. Posible fallo en conexión a BD", Zend_Log::WARN);
                        break;
                }
            }
        }
    }

    public function logoutAction(){
        $web_host = Zend_Registry::get("web_host");
        $web_path = Zend_Registry::get("web_path");
        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::destroy();
        $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/auth/login');
    }

}
