<?php

class CargaController extends Zend_Controller_Action {

    protected $_application;
    protected $_flashMessenger = null;
    protected $_redirector = null;

    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        $this->initView();

        $this->view->web_host = Zend_Registry::get("web_host");
        $this->view->web_path = Zend_Registry::get("web_path");

        Zend_Loader::loadClass('Users');

        $this->view->menu_item = 2;

        $this->view->user = Zend_Auth::getInstance()->getIdentity();
    }

    public function preDispatch() {
        $auth = Zend_Auth::getInstance();
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        if (!$auth->hasIdentity()) {
            $this->_redirector->gotoUrl($web_host . $web_path . '/index.php/auth/login');
        }
        $test_user = Zend_Auth::getInstance()->getIdentity();
        if ($test_user->tipo != 'Administrador') {
            $this->_redirector->gotoUrl($web_host . $web_path . '/index.php/creditos');
        }
    }

    public function indexAction() {
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->view->headTitle('Housemarket << Autorizar Carga Masiva >>');

        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');

        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
    }

    public function getcargaAction() {
        $log = Zend_Registry::get('log');
        if ($this->getRequest()->isPost()) {
            try {
                $uid = 0;
                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 50);
                $sort = $this->_request->getParam('sort', "FechaSolicitaCarga");
                $dir = $this->_request->getParam('dir', "ASC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);
                $log->info(__METHOD__ . __LINE__ . " $sort,$dir,$start,$limit");
                $rs = Dgt_Data_Utileria::getCarga($sort, $dir, $start, $limit);
                $rs_count = Dgt_Data_Utileria::getCountCarga();
                $results = $rs_count[0]->total;
                if (!empty($rs)) {
                    echo '{"success":true, "results":' . $results . ', "rows":' . Zend_Json::encode($rs) . '}';
                } else {
                    echo '{"success":"false", "results":0, "rows":0}';
                }
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la sessi�n.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }

    public function autorizasolicitudAction() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        if (!$this->getRequest()->isPost()) {
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));
            $keys_form = array('CodigoCliente');
            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }
            $filter = array(
                '*' => array('StringTrim', 'StripTags'),
                'CodigoCliente' => 'Alnum'
            );
            $validators = array(
                'CodigoCliente' => 'Alnum'
            );
            $input = new Zend_Filter_Input($filter, $validators, $valid_data);
            if ($input->isValid()) {
                $update_cliente = array(
                    'FechaRevocaCarga' => null,
                    'FechaOtorgaCarga' => new Zend_Db_Expr('CURDATE()'),
                    'CargaMasiva' => true,
                    'Broker' => true
                );
                $valores = implode(",", $valid_data);
                if ($db->update('cliente', $update_cliente, 'CodigoCliente =' . $input->CodigoCliente)) {

                    //Armando correo con plantilla likes.phtml
                    $html = new Zend_View();
                    $html->setScriptPath(APPLICATION_PATH . '/views/scripts/emails/');
                    // assign valeues

                    $html->name = $_POST['Cliente'];
                    $html->email = $_POST['Email']; // Email destinatarioa de La Carga Masiva          

                    $body = $html->render('cargamasiva.phtml');

                    //Configuracion del servidor smtp
                    $smtpServer = "smtp.gmail.com";
                    $username = "hmappfb@gmail.com";
                    $password = "xolotlan";
                    $config = array('ssl' => 'tls',
                        'port' => 587,
                        'auth' => 'login',
                        'username' => $username,
                        'password' => $password);
                    try {
                        $transport = new Zend_Mail_Transport_Smtp($smtpServer, $config);

                        $mail = new Zend_Mail('UTF-8');
                        $mail->setFrom($username, "Housemarket Notification");
                        $mail->setReplyTo($username, "No-Reply");
                        $mail->setReturnPath($username, "No-Reply");
                        $mail->addTo($_POST["Email"], $_POST["Cliente"]);
                        $mail->setSubject('Housemarket - Listing Upload');
                        $mail->setBodyHtml($body);
                        $mail->setBodyText($body);
                        $status = $mail->send($transport);
                    } catch (Exception $e) {
                        echo "Error message: " . $e->getMessage() . "\n";
                    }
                    echo '{"success": true}';
                    exit();
                } else {
                    echo '{"success": false}';
                    exit();
                }
            } else {
                echo '{"success":"false", "msg": "method get no allowed"}';
            }
            exit();
        }
    }

    public function revocasolicitudAction() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        if (!$this->getRequest()->isPost()) {
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));
            $keys_form = array('CodigoCliente');
            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }
            $filter = array(
                '*' => array('StringTrim', 'StripTags'),
                'CodigoCliente' => 'Alnum'
            );
            $validators = array(
                'CodigoCliente' => 'Alnum'
            );
            $input = new Zend_Filter_Input($filter, $validators, $valid_data);
            if ($input->isValid()) {
                $update_cliente = array(
                    'FechaRevocaCarga' => new Zend_Db_Expr('CURDATE()'),
                    'FechaOtorgaCarga' => null,
                    'CargaMasiva' => false,
                    'Broker' => false
                );
                $valores = implode(",", $valid_data);
                if ($db->update('cliente', $update_cliente, 'CodigoCliente =' . $input->CodigoCliente)) {
                    echo '{"success": true}';
                    exit();
                } else {
                    echo '{"success": false}';
                    exit();
                }
            } else {
                echo '{"success":"false", "msg": "method get no allowed"}';
            }
            exit();
        }
    }

}