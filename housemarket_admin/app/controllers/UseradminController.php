<?php

class UseradminController extends Zend_Controller_Action {

    protected $_application ;
    protected $_flashMessenger = null;
    protected $_redirector = null;

    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        $this->initView();

        $this->view->web_host = Zend_Registry::get("web_host");
        $this->view->web_path = Zend_Registry::get("web_path");

        Zend_Loader::loadClass('Users');

        $this->view->menu_item = 4;

        $this->view->user = Zend_Auth::getInstance()->getIdentity();

    }
    public function preDispatch() {
        $auth = Zend_Auth::getInstance();
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        if (!$auth->hasIdentity()) {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/auth/login');
        }
        $test_user = Zend_Auth::getInstance()->getIdentity();
        if($test_user->tipo != 'Administrador') {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/creditos');
        }
    }

    public function indexAction() {
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->view->headTitle('Housemarket << Administrar Usuarios >>');
    //$this->_redirector->gotoUrl($web_host.$web_path.'/index.php/user/list');

    }

    public function addAction() {
    //require_once 'Dgt/Validate/PasswordConfirm.php';
    //$loader = new Zend_Loader_PluginLoader();
    //$loader->addPrefixPath('Dgt_Validate', 'Dgt/Validate');

        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');

        $key = Zend_Registry::get('key');
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        if(!$this->getRequest()->isPost()) {
        // no post data
        // no retornamos nada
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));

            $keys_form = array('username', 'password', 'password_confirm', 'active', 'nombre', 'tipo',
                'apellido', 'email');

            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }

            $filter = array (
                '*' => array('StringTrim', 'StripTags'),
                'username' => 'Alnum',
                //'active' => 'Digits',   // usar en editar usuario
                'password' => 'Alnum',
                'password_confirm' => 'Alnum',
                'tipo' => 'Alpha',
                'nombre' => 'Alpha',
                'apellido' => 'Alpha',
                //email' => 'EmailAddress'
            );

            $validators = array(
                'username' => 'Alnum',
                //'active' => 'Digits',
                'password_confirm' => 'Alnum',
                'password' => 'Alnum',
                'tipo' => 'Alpha',
                'nombre' => 'Alpha',
                'apellido' => 'Alpha',
                'email' => 'EmailAddress'
            );

            $input = new Zend_Filter_Input($filter, $validators, $valid_data);
            //$input->addValidatorPrefixPath('Dgt_Validate', 'Dgt/Validate'); // Agregando Custom Validator.

            if($input->isValid()) {
                if($input->password == $input->password_confirm) {
                    $insert_data = array(
                        'usuario' => $input->username,
                        'clave' => sha1($key . $input->password),
                        'estatus' => 1,
                        'tipo' => $input->tipo,
                        'nombre' => $input->nombre,
                        'apellido' => $input->apellido,
                        'email' => $input->email);
                    if($db->insert('usuarios', $insert_data)) {
                        echo '{ success: true }';
                        exit();
                    } else {
                        echo '{"success": false, "errormsg": "fallo insercion de datos"}';
                        exit();
                    }
                }
            }
            if ($input->hasInvalid() || $input->hasMissing()) {
                $messages = $input->getMessages();
                echo '{ "success": false, "errormsg": '. Zend_Json::encode($messages) .' }';
                exit();
            }
/*
            $passwd1 = $valid_data['password'];
            $passwd2 = $valid_data['password_confirm'];

            if($passwd1 == $passwd2) {
                $password = sha1($key . $passwd1);
                $insert_data = array(
                    'username' => $valid_data['username'],
                    'password' => $password,
                    'active' => 1,
                    'nombre' => $valid_data['nombre'],
                    'apellido' => $valid_data['apellido'],
                    'email' => $valid_data['email']);
                // @todo cambiar por un if, revisar retorno de la funcion, objeto
                try {
                    $row = $db->insert('users', $insert_data);
                    Zend_Debug::dump($row);
                    echo '{ success: true }';
                    exit();
                    //$this->_redirector->gotoUrl($web_host.$web_path.'/index.php/user/list');
                } catch(Exception $e) {
                    $this->_flashMessenger->addMessage('Error al agrear nuevo usuario');
                    $this->view->messages = $this->_flashMessenger->getCurrentMessages();
                }
            } else {

            }
*/

        //Zend_Debug::dump($valid_data);
        //}

        }
    }
    public function editAction() {
        $key = Zend_Registry::get('key');
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $key = Zend_Registry::get('key');
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        if(!$this->getRequest()->isPost()) {
        // no post data
        // no retornamos nada
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));

            $keys_form = array('username', 'password', 'password_confirm', 'active', 'nombre', 'tipoe',
                'apellido', 'email', 'id');

            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }

            $filter = array (
                '*' => array('StringTrim', 'StripTags'),
                'username' => 'Alnum',
                'active' => 'Digits',
                'id' => 'Digits',
                'password' => 'Alnum',
                'password_confirm' => 'Alnum',
                'tipoe' => 'Alpha',
                'nombre' => 'Alpha',
                'apellido' => 'Alpha',
                //email' => 'EmailAddress'
            );

            $validators = array(
                'username' => 'Alnum',
                'active' => array('Digits',  'allowEmpty' => true),
                'id' => 'Digits',
                'password_confirm' => array('Alnum', 'allowEmpty' => true),
                'password' => array('Alnum', 'allowEmpty' => true),
                'tipoe' => 'Alpha',
                'nombre' => 'Alpha',
                'apellido' => 'Alpha',
                'email' => 'EmailAddress'
            );

            $input = new Zend_Filter_Input($filter, $validators, $valid_data);
            //$input->addValidatorPrefixPath('Dgt_Validate', 'Dgt/Validate'); // Agregando Custom Validator.

            if($input->isValid()) {
                if($input->password == $input->password_confirm) {
                    if (strlen($input->password == 0)) {
                        $update_data = array(
                            'usuario' => $input->username,
                            'nombre' => $input->nombre,
                            'apellido' => $input->apellido,
                            'tipo' => $input->tipoe,
                            'email' => $input->email);
                    } else {
                        $update_data = array(
                            'usuario' => $input->username,
                            'clave' => sha1($key . $input->password),
                            'nombre' => $input->nombre,
                            'apellido' => $input->apellido,
                            'tipo' => $input->tipoe,
                            'email' => $input->email);
                    }

                    if ( (strlen($input->active) == 0) || !isset($input->active) ) {

                    } else {
                        $update_data['estatus'] = $input->active;
                    }

                    if($db->update('usuarios', $update_data, 'idusuario = ' . $input->id)) {
                        echo '{ "success": true }';
                        exit();
                    } else {
                        echo '{"success": false, "errormsg": "fallo actualizacion de datos"}';
                        exit();
                    }
                }
            }
            if ($input->hasInvalid() || $input->hasMissing()) {
                $messages = $input->getMessages();
                echo '{ "success": false, "errormsg": '. Zend_Json::encode($messages) .' }';
                exit();
            }
        }
    }
    public function deleteAction() {
        $db = Zend_Registry::get('dbAdapter');
        if( $this->getRequest()->isPost()) {
            $id = $this->_request->getParam('id', null);
            if(isset ($id) && is_numeric($id)) {
                $rs = $db->delete('usuarios', 'idusuario =' . $id );
                echo '{"success": true}';
                exit();
            } else {
                echo '{"success": false}';
                exit();
            }

        } else {
            echo '{"success": false}';
            exit();
        }
    }

    /* Ajax functions */
    public function getusersAction() {
        $rs = Dgt_Data_Utils::getUsers();
        //Zend_Debug::dump($rs);
        echo '{ "rows": '. Zend_Json::encode($rs) .'}';
        exit();
    }



}
