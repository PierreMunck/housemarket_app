<?php

require_once 'Hm/Cache.php';

class ServedorController extends Zend_Controller_Action {

    protected $_application ;
    protected $_flashMessenger = null;
    protected $_redirector = null;

    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        $this->initView();

        $this->view->web_host = Zend_Registry::get("web_host");
        $this->view->web_path = Zend_Registry::get("web_path");

        Zend_Loader::loadClass('Users');

        $this->view->menu_item = 1;

        $this->view->user = Zend_Auth::getInstance()->getIdentity();

    }
    public function preDispatch() {
        $auth = Zend_Auth::getInstance();
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        if (!$auth->hasIdentity()) {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/auth/login');
        }
        $test_user = Zend_Auth::getInstance()->getIdentity();
        /*if($test_user->tipo == 'Representante') {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php');
        }*/
    }

    public function CommunHeader(){
    	$this->view->headTitle("Housemarket << Administrar Servedor >>");
    	$this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
    	$this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
    	$this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
    	$this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
    	$this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
    }
    
    public function indexAction() {
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->CommunHeader();
    }

    public function clearcacheAction() {
    	Zend_Registry::set('user_info',array());
    	$cache = new HmCache();
    	$cache->clearall();
    	$this->_helper->viewRenderer('index');
    	$this->CommunHeader();
    	$this->view->message = "Cache Clear";
    	
    }
}
