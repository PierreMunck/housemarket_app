<?php

class TabController extends Zend_Controller_Action {

    protected $_application;
    protected $_flashMessenger = null;
    protected $_redirector = null;

    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        $this->initView();

        $this->view->web_host = Zend_Registry::get("web_host");
        $this->view->web_path = Zend_Registry::get("web_path");

        Zend_Loader::loadClass('Users');

        $this->view->menu_item = 2;

        $this->view->user = Zend_Auth::getInstance()->getIdentity();
    }

    public function preDispatch() {
        $auth = Zend_Auth::getInstance();
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        if (!$auth->hasIdentity()) {
            $this->_redirector->gotoUrl($web_host . $web_path . '/index.php/auth/login');
        }
        $test_user = Zend_Auth::getInstance()->getIdentity();
        if ($test_user->tipo != 'Administrador') {
            $this->_redirector->gotoUrl($web_host . $web_path . '/index.php/creditos');
        }
    }

    public function indexAction() {
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->view->headTitle('Housemarket << Autorizar Tab >>');

        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');

        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
    }

    public function gettabAction() {
        $log = Zend_Registry::get('log');
        if ($this->getRequest()->isPost()) {
            try {
                $uid = 0;
                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 50);
                $sort = $this->_request->getParam('sort', "requestDate");
                $dir = $this->_request->getParam('dir', "ASC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);
                $log->info(__METHOD__ . __LINE__ . " $sort,$dir,$start,$limit");

                $rs = Dgt_Data_Utileria::getTab($sort, $dir, $start, $limit);
                $rs_count = Dgt_Data_Utileria::getCountTab();
                $results = $rs_count[0]->total;
                
                Zend_Json::$useBuiltinEncoderDecoder = true;

                if (!empty($rs)) {
                    echo '{"success":true, "results":' . $results . ', "rows":' . Zend_Json::encode($rs) . '}';
                } else {
                    echo '{"success":"false", "results":0, "rows":0}';
                }
                exit();
            } catch (Exception $e) {
                $err = $e->getMessage();
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la session.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }

    public function autorizarsolicitudtabAction() {
        try {
            $db = Zend_Registry::get('dbAdapter');
            $db->setFetchMode(Zend_Db::FETCH_OBJ);
            if (!$this->getRequest()->isPost()) {
                echo '{"success":"false", "msg": "method get no allowed"}';
                exit();
            } else {
                $this->getRequest()->setParamSources(array('_POST'));
                $keys_form = array('pageId');
                foreach ($keys_form as $k) {
                    $valid_data[$k] = $this->_request->getParam($k, null);
                }

                // DOC: Zend_Filter_Input documentation


                $filter = array(
                    '*' => array('StringTrim', 'StripTags'),
                    'pageId' => 'Digits',
                );
                $validators = array(
                    'pageId' => array('NotEmpty', 'Digits'),
                );
                $input = new Zend_Filter_Input($filter, $validators, $valid_data);

                if ($input->isValid()) {

                    $update_data = array(
                        'serviceStatus' => 1,
                        'serviceStartDate' => new Zend_Db_Expr('NOW()'),
                        'serviceExpirationDate' => new Zend_Db_Expr('DATE_ADD(NOW(),INTERVAL 1 YEAR)'),
                    );
                    $where_clause = 'pageId = ' . $input->pageId . ' AND serviceType = 1';
                    if ($db->update('page_service_status', $update_data, $where_clause)) {

                        $mailSent = $this->sendNotificationMailToClient($input->pageId);

                        if ($mailSent) {
                            $update_data = array(
                                'clientNotification' => 1,
                            );
                            $where_clause = 'pageId = ' . $input->pageId . ' AND serviceType = 1';
                        }
                        $db->update('page_service_status', $update_data, $where_clause);

                        echo '{"success": true}';
                        exit();
                    } else {
                        echo '{"success": false}';
                        exit();
                    }
                } else {
                    $err = $input->getMessages();
                    echo '{"success":"false", "msg": "invalid request"}';
                }
                exit();
            }
        } catch (Exception $e) {
            $err = $e->getMessage();
        }
    }

    public function revocarsolicitudtabAction() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        if (!$this->getRequest()->isPost()) {
            echo '{"success":"false", "msg": "method get no allowed"}';
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));
            $keys_form = array('pageId');
            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }

            // DOC: Zend_Filter_Input documentation


            $filter = array(
                '*' => array('StringTrim', 'StripTags'),
                'pageId' => 'Digits',
            );
            $validators = array(
                'pageId' => array('NotEmpty', 'Digits'),
            );
            $input = new Zend_Filter_Input($filter, $validators, $valid_data);

            if ($input->isValid()) {
                try {
                    $update_data = array(
                        'serviceStatus' => 0,
                        'serviceStartDate' => null,
                        'serviceExpirationDate' => null,
                        'requestDate' => null,
                        'hmNotification' => 0,
                        'clientNotification' => 0,
                    );
                    $where_clause = 'pageId = ' . $input->pageId . ' AND serviceType = 1';
                    if ($db->update('page_service_status', $update_data, $where_clause)) {
                        echo '{"success": true}';
                        exit();
                    } else {
                        echo '{"success": false}';
                        exit();
                    }
                } catch (Exception $e) {
                    $err = $e->getMessage();
                }
            } else {
                echo '{"success":"false", "msg": "invalid request"}';
            }
            exit();
        }
    }

    protected function sendNotificationMailToClient($pageId) {
        try {
            $config = Zend_Registry::get('configuration');
            $db = Zend_Registry::get('dbAdapter');
            $db->setFetchMode(Zend_Db::FETCH_ASSOC);

            $sqlClientInfo = 'SELECT c.NombreCliente AS name, c.EMail AS email, c.Uid AS uid
                FROM fb_page AS fp
                INNER JOIN cliente AS c ON fp.uid = c.Uid
                WHERE fp.pageid = ' . $pageId;
            $rsClientInfo = $db->fetchAll($sqlClientInfo);
            $clientsEmail = HMUtils::build_simple_indexed_array_from_array($rsClientInfo, 'email');

            $sqlPageInfo = 'SELECT pageid AS pageId, nombre AS name
                FROM page
                WHERE pageid = ' . $pageId;
            $rsPageInfo = $db->fetchAll($sqlPageInfo);
            $rsPageInfo = reset($rsPageInfo);
            
            /* EMAIL */

            $html = new Zend_View();
            $html->setScriptPath(APPLICATION_PATH . '/views/scripts/emails/');

            $html->page_name = $rsPageInfo['name'];
            $html->page_id = $rsPageInfo['pageId'];
            $html->page_url = 'www.facebook.com/pages/' . $rsPageInfo['name'] . '/' . $rsPageInfo['pageId'];

            $body = $html->render('tab.phtml');
            $textbody = preg_replace("'<style[^>]*>.*</style>'siU", '', $body);
            $textbody = trim(strip_tags($textbody));

            //Configuracion del servidor smtp
            $info_email = $config->info_email;
            $smtpServer = $config->gmail_smtpserver;
            $username = $config->gmail_username;
            $password = $config->gmail_password;

            $mailConfig = array(
                'ssl' => 'tls',
                'port' => 587,
                'auth' => 'login',
                'username' => $username,
                'password' => $password
            );

            $transport = new Zend_Mail_Transport_Smtp($smtpServer, $mailConfig);

            $mail = new Zend_Mail('UTF-8');
            $mail->setFrom($username, "Housemarket Notificacion");
            $mail->setReplyTo($username, "No-Reply");
            $mail->setReturnPath($username, "No-Reply");
            foreach ($clientsEmail as $clientEmail) {
                $mail->addTo($clientEmail);
            }
            $mail->addBcc($info_email);
            $mail->setSubject('Housemarket\'s tab on fan page enabled');
            $mail->setBodyHtml($body);
            $mail->setBodyText($textbody);
            $status = $mail->send($transport);
            return $status;
        } catch (Exception $e) {
            $err = $e->getMessage();
        }
    }

}