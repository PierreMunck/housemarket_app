<?php

class RepresentanteController extends Zend_Controller_Action {

    protected $_application ;
    protected $_flashMessenger = null;
    protected $_redirector = null;

    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        $this->initView();

        $this->view->web_host = Zend_Registry::get("web_host");
        $this->view->web_path = Zend_Registry::get("web_path");

        Zend_Loader::loadClass('Users');

        $this->view->menu_item = 4;

        $this->view->user = Zend_Auth::getInstance()->getIdentity();

    }

    public function preDispatch() {
        $auth = Zend_Auth::getInstance();
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        if (!$auth->hasIdentity()) {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/auth/login');
        }
        $test_user = Zend_Auth::getInstance()->getIdentity();
        if($test_user->tipo != 'Administrador') {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/creditos');
        }
    }

    public function indexAction() {
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->view->headTitle('Housemarket << Autorizar Representante >>');

        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');

        $this->view->headScript()->appendFile('/js/extjs/adapter/ext/ext-base.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/ext-all.js', 'text/javascript');
        $this->view->headScript()->appendFile('/js/extjs/build/locale/ext-lang-es-min.js', 'text/javascript');
    }

    public function eliminarepresentanteAction() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        if(!$this->getRequest()->isPost()) {
            exit();
        } else {
            $this->getRequest()->setParamSources(array('_POST'));
            $keys_form =  array('CodigoRepresenta');
            foreach ($keys_form as $k) {
                $valid_data[$k] = $this->_request->getParam($k, null);
            }
            $filter = array (
                    '*' => array('StringTrim', 'StripTags'),
                    'CodigoRepresenta' => 'Alnum'
            );
            $validators = array(
                    'CodigoRepresenta' => 'Alnum'
            );
            $input = new Zend_Filter_Input($filter, $validators, $valid_data);

            if($input->isValid()) {

                if(count($resverifica)==0) {
                    if($db->delete('representante', 'CodigoRepresenta =' . $input->CodigoRepresenta )) {
                        echo '{"success": true}';
                        exit();
                    }
                    else {
                        echo '{"success": false, "errormsg": "fallo eliminación de datos."}';
                        exit();
                    }
                }

            }else {
                echo '{"success":"false", "msg": "method get no allowed"}';
            }
            exit();
        }
    }

    public function getrepresentantesAction() {
        $log = Zend_Registry::get('log');
        if($this->getRequest()->isPost()) {
            try {
                $uid=0;
                $f = new Zend_Filter_StripTags();
                $start_raw = $this->_request->getParam('start', 0);
                $limit_raw = $this->_request->getParam('limit', 30);
                $sort = $this->_request->getParam('sort', "CodigoRepresenta");
                $dir = $this->_request->getParam('dir', "DESC");
                $start = $f->filter($start_raw);
                $limit = $f->filter($limit_raw);
                //$log->info(__METHOD__ . __LINE__ . " $sort,$dir,$start,$limit");
                $rs = Dgt_Data_Utileria::getRepresentantes($sort,$dir,$start,$limit);
                $rs_count = Dgt_Data_Utileria::getCountRepresentantes();
                $results = $rs_count[0]->total;
                if (!empty($rs)) {
                    echo '{"success":true, "results":'.$results.', "rows":'.Zend_Json::encode($rs) .'}';
                }
                else {
                    echo '{"success":"false", "results":0, "rows":0}';
                }
                exit();
            } catch (Exception $e) {
                echo '{"success": false}';
                Zend_Debug::dump("Expiracion de la sessión.");
                exit();
            }
        } else {
            echo '{"success":"false", "msg": "method get no allowed"}';
        }
        exit();
    }

    public function editAction() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->view->headTitle('Housemarket << Crear Representante >>');

        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
        $this->view->headLink()->appendStylesheet('/css/messages.css');

        $urldestino=$web_host.$web_path.'/index.php/representante';
        if($this->getRequest()->isPost()) {
            if($_POST['cancelar']){
                $this->_redirector->gotoUrl($urldestino);
            }
            try {

                $usuarioselect = $db->query("select idusuario, concat(nombre,' ',apellido) as nombre,email from usuarios where idusuario=". $_POST['idusuario']);
                $resultusuario=$usuarioselect->fetchAll();

                $datos = array('NombreRepresenta' => $resultusuario[0]->nombre,
                        'NombreEmpresa' => $_POST['empresa'],
                        'Direccion' => $_POST['direccion'],
                        'ZipCode' => $_POST['zipcode'],
                        'Telefono' => $_POST['telefono'],
                        'EMail' => $resultusuario[0]->email,
                        'Fax' => $_POST['fax'],
                        'Uid' => $_POST['usuariofb']
                );
                $db->update('representante',$datos,'CodigoRepresenta='.$_POST['idrepresenta']);
                $this->_redirector->gotoUrl($urldestino);
            }
            catch (Exception $e) {
                $this->view->msjerror=$this->mensajeerror("Error al guardar los datos.");
            }
        }
        else if(!empty ($_GET['id'])) {
            $this->view->idrepresenta=$_GET['id'];
            if(!is_numeric($this->view->idrepresenta)) {
                $this->_redirector->gotoUrl($urldestino);
            }
            $usuarioselect = $db->query("select NombreRepresenta, NombreEmpresa,Direccion,ZipCode,
                                        Telefono, EMail, Fax,idusuario, Uid from representante where CodigoRepresenta=".$this->view->idrepresenta);
            $resultusuario=$usuarioselect->fetchAll();
            if(count($resultusuario)>0){
                $this->view->representante=$resultusuario[0]->NombreRepresenta;
                $this->view->empresa=$resultusuario[0]->NombreEmpresa;
                $this->view->direccion=$resultusuario[0]->Direccion;
                $this->view->zipcode=$resultusuario[0]->ZipCode;
                $this->view->telefono=$resultusuario[0]->Telefono;
                $this->view->email=$resultusuario[0]->EMail;
                $this->view->fax=$resultusuario[0]->Fax;
                $this->view->idfacebook=$resultusuario[0]->Uid;
                $this->view->idusuario=$resultusuario[0]->idusuario;
            }
            else{
                $this->_redirector->gotoUrl($urldestino);
            }

        }

    }

    public function representanteAction() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        $this->view->headTitle('Housemarket << Crear Representante >>');

        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/extjs/resources/css/xtheme-gray.css');
        $this->view->headLink()->appendStylesheet('/css/messages.css');
        $urldestino=$web_host.$web_path.'/index.php/representante';
        if($this->getRequest()->isPost()) {
             if($_POST['cancelar']){
                $this->_redirector->gotoUrl($urldestino);
            }
            try {

                $usuarioselect = $db->query("select idusuario, concat(nombre,' ',apellido) as nombre,email from usuarios where idusuario=". $_POST['cbusuario']);
                $resultusuario=$usuarioselect->fetchAll();

                $datos = array('NombreRepresenta' => $resultusuario[0]->nombre,
                        'NombreEmpresa' => $_POST['empresa'],
                        'Direccion' => $_POST['direccion'],
                        'ZipCode' => $_POST['zipcode'],
                        'Telefono' => $_POST['telefono'],
                        'EMail' => $resultusuario[0]->email,
                        'Fax' => $_POST['fax'],
                        'idusuario' => $resultusuario[0]->idusuario,
                        'Uid' => $_POST['usuariofb']
                );
                $db->insert('representante',$datos);
                $this->_redirector->gotoUrl($urldestino);
            }
            catch (Exception $e) {
                $this->view->msjerror=$this->mensajeerror("Error al guardar los datos.");
            }
        }

    }

    public function mensajeerror($mensaje) {
        return  "<div style='border:solid 1px #e6938b;
	background:#ffd0cc url(/img/msg_error.png) 10px 50% no-repeat;
	color:#990000;
	padding:4px 4px 4px 40px;
	text-align:left;'>$mensaje</div>";

    }

}
