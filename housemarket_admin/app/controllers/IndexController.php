<?php
class IndexController extends Zend_Controller_Action {

    protected $_flashMessenger = null;
    protected $_redirector = null;

    public function init() {

        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        $this->initView();
        $this->view->web_host = Zend_Registry::get('web_host');
        $this->view->web_path = Zend_Registry::get('web_path');

        $this->view->user = Zend_Auth::getInstance()->getIdentity();
    }

    public function preDispatch() {
        $auth = Zend_Auth::getInstance();
        $web_host = Zend_Registry::get('web_host');
        $web_path = Zend_Registry::get('web_path');
        if (!$auth->hasIdentity()) {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/auth/login');
        }
        $test_user = Zend_Auth::getInstance()->getIdentity();
        if($test_user->tipo != 'Administrador') {
            $this->_redirector->gotoUrl($web_host.$web_path.'/index.php/creditos');
        }
    }

    public function indexAction() {
        $auth = Zend_Auth::getInstance();
        $data = $auth->getStorage()->read();
        $this->view->headTitle('<< Inicio >>');
        //Zend_Debug::dump($data);
    }

}
