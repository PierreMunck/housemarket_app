<?php
class Users extends Zend_Db_Table {
    protected $_name = "usuarios";

    public function colocaacentos($valor) {
        $valor=str_replace("�", "aeacute;", $valor);
        $valor=str_replace("�", "&eacute;", $valor);
        $valor=str_replace("�", "&iacute;", $valor);
        $valor=str_replace("�", "&oacute;", $valor);
        $valor=str_replace("�", "&uacute;", $valor);
        $valor=str_replace("�", "&ntilde;", $valor);
        $valor=str_replace("�", "&Ntilde;", $valor);
        return $valor;
    }

}