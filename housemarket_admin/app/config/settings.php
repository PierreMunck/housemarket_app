<?php
$settings = array(
    'gmail_smtpserver' => "smtp.gmail.com",
    'gmail_username' => "hmappfb@gmail.com",
    'gmail_password' => "xolotlan",
    'info_email' => "info@hmapp.com",
    'web_host' => 'https://app.hmapp.com',
    'web_path' => '/administrador',
    'database' => array(
    	'adapter' => 'PDO_MYSQL',
        'params' => array(
        	'host' => 'localhost',
            'username' => 'hmapp',
            'password' => 'H1M2A3p4p_',
            'dbname' => 'hmapp',
        	'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8;')
        )
    ),
	'db' => array('params' => array('charset' => 'utf8')),
    'dbsession' => array('name' => 'sessiondb',
        'primary' => array('session_id',
            'save_path',
            'name'),
        'primaryAssignment' => array('sessionId',
            'sessionSavePath',
            'sessionName'),
        'sessionName' => 'name',
        'sessionSavePath' => 'save_path',
        'modifiedColumn' => 'modified',
        'dataColumn' => 'session_data',
        'lifetimeColumn' => 'lifetime'),
    'layout' => array('layoutPath' => APPLICATION_PATH . '/layouts/')
    );

if(APPLICATION_ENVIRONMENT == 'development'){
	$settings['web_host'] = 'https://hmapp.housemarketapp.com';
	$settings['database']['params']['username'] = 'c1_hmadmin';
	$settings['database']['params']['dbname'] = 'c1_hmapp';
	$settings['database']['params']['password'] = 'H1M2A3p4p_';
}

return $settings;
