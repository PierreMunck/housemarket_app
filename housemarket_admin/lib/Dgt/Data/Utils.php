<?php

/**
 * @todo  refactoriar esta clase. comun $db, $sql, $params, $rs.
 */
class Dgt_Data_Utils {

    public static function getUsers() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        //$sql = 'SELECT id, username, active, nombre, apellido, email FROM users';
        $sql = 'SELECT idusuario, tipo, usuario, estatus as active,
                if( estatus = 1, \'activa\', \'inactiva\') estatus,
                nombre, apellido, email FROM usuarios';
        $rs = $db->query($sql);
        return $rs->fetchAll();
    }

    private static function doQuery($sql, $params=null) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        if (isset($params)) {
            $rs = $db->query($sql,$params);
        } else {
            $rs = $db->query($sql);
        }
        $data = $rs->fetchAll();
        return $data;
    }
    public static function getProperty($sort="idpropiedad",$dir="ASC",$start = 0, $limit = 50,$uid= 0,
            $cc=null,$ciudad=null,$direccion=null, $tipo=null,$min_creditos=null, $max_creditos=null,
            $min_price=null, $max_price=null,
            $min_beds=null, $max_beds=null,$min_baths=null,$max_baths=null,
            $area = null, $cat = null,$fecha_ac=null,$fecha_cc=null) {


        // Zend_Debug::dump(  __METHOD__ . "$cc, $ciudad, $direccion, $tipo, $min_price, $max_price, $cuartos, $banos, $area, $cat");

        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        $params = array();

        $sql = 'SELECT
                      propiedad.idpropiedad,
                      propiedad.descripcion,
                      propiedad.pais,
                      propiedad.ciudad,
                      pais.pais,
                      propiedad.precio_venta,
                      prop_tipo.nombre as "tipo_enlistamiento",
                      prop_categoria.es as "tipo_propiedad",
                      propiedad.precio_alquiler,
                      propiedad.fecha_ingreso,
                      propiedad.ultima_modificacion,
                      propiedad.area,
                      propiedad.cuartos,
                      propiedad.banos,
                      propiedad.lat,
                      propiedad.uid,
                      propiedad.lng,
                      propiedad.estatus,
                      propiedad.creditos,
                      propiedad.fecha_asignacion_credito,
                      propiedad.fecha_expiracion_credito,
                      prop_tipo.nombre
                       FROM
                      propiedad
                       LEFT OUTER JOIN pais ON (pais.zip_code = propiedad.pais)
                       LEFT OUTER JOIN prop_tipo ON (propiedad.tipo_enlistamiento = prop_tipo.tipo)
                       LEFT OUTER JOIN prop_categoria ON (propiedad.tipo_propiedad = prop_categoria.idtipo)';


        $sql .= ' WHERE 1=1 ';
        IF($uid<>0) {
            $sql .= ' AND uid='.$uid;
        }

        if(is_numeric($direccion)) {
            $sql .= ' AND idpropiedad = ? AND estatus = 3';
            $params[] = $direccion;
        } else {



            if (isset($direccion) && (strlen(trim($direccion)) > 0)) {
                $exp = "%$direccion%";
                $params[] = $exp;
                $sql .= " AND direccion like ? AND estatus = 3";

            } else {
                $sql .= ' AND estatus = 3 ';
            }

            if (isset($ciudad) && (strlen(trim($ciudad)) > 0)) {
                $exp = "%$ciudad%";
                $params[] = $exp;
                $sql .= " AND ciudad like ? ";

            }


            // tipo_enlistamiento: 1=venta, 2=renta , 3=venta/alquiler
            // tipo_propiedad: categoria :apartemento,casa,edificio,etc.

            if(isset($tipo)) {
                if($tipo == 1) {
                    // alquiler
                    $sql .= ' AND propiedad.tipo_enlistamiento = 2';
                }
                if($tipo == 2) {
                    // venta
                    $sql .= ' AND propiedad.tipo_enlistamiento = 1';
                }
                if($tipo == 3) {
                    // venta/Alquiler
                    $sql .= ' AND propiedad.tipo_enlistamiento = 3';
                }
            }

            if( isset($cc) && (strlen(trim($cc)) > 0) ) {
                $params[] = $cc;
                $sql .= " AND propiedad.pais = ?";
                // $sql = $db->quoteInto($sql, $cc);
            }

            if ( ($min_price >= 0) && ($max_price > 0) ) {
                //$params[] = $min_price;
                //$params[] = $max_price;
                if($tipo == 1) {
                    // alquiler
                    $sql .= " AND precio_alquiler BETWEEN $min_price AND $max_price";
                }
                if($tipo == 2) {
                    // venta
                    $sql .= " AND precio_venta BETWEEN $min_price AND $max_price";
                }
                if($tipo == 3) {
                    // venta/alquiler
                    $sql .= " AND (precio_alquiler BETWEEN $min_price AND $max_price OR precio_venta BETWEEN $min_price AND $max_price) ";
                }

                //$sql = $this->dbAdapter->quoteInto($sql, $min_price, $max_price);
            }
            if ( ($min_creditos >= 0) && ($max_creditos > 0) ) {

                $sql .= " AND creditos BETWEEN $min_creditos AND $max_creditos";

            }
            if ( ($min_beds >= 0) && ($max_beds > 0) ) {

                $sql .= " AND cuartos BETWEEN $min_beds AND $max_beds";

            }

            if ( ($min_baths >= 0) && ($max_baths > 0) ) {

                $sql .= " AND banos BETWEEN $min_baths AND $max_baths";

            }

//            if (isset($cuartos) && ( strlen(trim($cuartos)) > 0)&& ($cuartos <> 0 ) ) {
//                $cuartos_temp = split('-', $cuartos);
//                if(is_numeric($cuartos_temp[0])) {
//                    $l1 = $cuartos_temp[0];
//                }
//                if(is_numeric($cuartos_temp[1])) {
//                    $l2 = $cuartos_temp[1];
//                } else {
//                    $l2=0;
//                }
//                if($l2 <> 0 ) {
//                    //$params[] = $l1;
//                    //$params[] = $l2;
//                    $sql .= " AND cuartos BETWEEN $l1 AND $l2";
//                    //$sql = $this->dbAdapter->quoteInto($sql, $l1, $l2);
//                } else {
//                    //$params[] = $l1;
//                    $sql .= " AND cuartos >= $l1";
//                    //$sql = $this->dbAdapter->quoteInto($sql, $l1);
//
//                }
//            }

//            if (isset($banos) && ( strlen(trim($banos)) > 0)&& ($banos <> 0 )) {
//                $banos_temp = split('-', $banos);
//                if(is_numeric($banos_temp[0])) {
//                    $l1 = $banos_temp[0];
//                }
//                if(is_numeric($banos_temp[1])) {
//                    $l2 = $banos_temp[1];
//                } else {
//                    $l2=0;
//                }
//                if($l2 <> 0 ) {
//                    //$params[] = $l1;
//                    //$params[] = $l2;
//                    $sql .= " AND banos BETWEEN $l1 AND $l2";
//                    //$sql = $this->dbAdapter->quoteInto($sql, $l1, $l2);
//                } else {
//                    //$params[] = $l1;
//                    $sql .= " AND banos >= $l1";
//                    //$sql = $this->dbAdapter->quoteInto($sql, $l1);
//
//                }
//            }

            if ($area > 0) {
                //$params[] = $area;
                $sql .= " AND area >= $area";
                //$sql = $this->dbAdapter->quoteInto($sql, $area);
            }

            if (isset($cat) && ( strlen(trim($cat)) > 0)) {
                $params[] = $cat;
                $sql .= " AND tipo_propiedad = ?";

            }

            if (isset($fecha_ac) && ( strlen(trim($fecha_ac)) > 0)) {
                $params[] = Dgt_Data_Utils::ConvertirFecha($fecha_ac);
                $sql .= " AND fecha_asignacion_credito = ?";

            }

            if (isset($fecha_cc) && ( strlen(trim($fecha_cc)) > 0)) {
                $params[] = Dgt_Data_Utils::ConvertirFecha($fecha_cc);
                $sql .= " AND fecha_expiracion_credito = ?";

            }

        }
        //      $rs = $db->query($sql, $params);
        //      $rs = $db->query($sql);
        //      return $rs->fetchAll();
        $sql .= " ORDER BY  propiedad.".$sort." ".$dir;
        $sql .= " LIMIT $start , $limit";

//        Zend_Debug::dump( __METHOD__ . ' sql:  ' . $sql);
//         Zend_Debug::dump($params);
//         die;
        return Dgt_Data_Utils::doQuery($sql, $params);
    }

    public static function getPropertyCount($start = 0, $limit = 50,$uid=0,
            $cc=null,$ciudad=null,$direccion=null, $tipo=null,$min_creditos=null, $max_creditos=null,
            $min_price=null, $max_price=null,
            $min_beds=null, $max_beds=null,$min_baths=null,$max_baths=null,
            $area = null, $cat = null,$fecha_ac=null,$fecha_cc=null) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        $params = array();

        $sql = "SELECT COUNT(*) AS total FROM propiedad";

        $sql .=" WHERE 1=1 ";
        IF($uid<>0) {
            $sql .= " AND uid=".$uid;
        }

        if(is_numeric($direccion)) {
            $sql .= ' AND idpropiedad = ? AND estatus = 3';
            $params[] = $direccion;
        } else {


            if (isset($direccion) && (strlen(trim($direccion)) > 0)) {
                $exp = "%$direccion%";
                $params[] = $exp;
                $sql .= " AND direccion like ? AND estatus = 3";

            } else {
                $sql .= ' AND estatus = 3 ';
            }

            // tipo_enlistamiento: 1=venta, 2=renta , 3=venta/alquiler
            // tipo_propiedad: categoria :apartemento,casa,edificio,etc.

            if(isset($tipo)) {
                if($tipo == 1) {
                    // alquiler
                    $sql .= ' AND propiedad.tipo_enlistamiento = 2';
                }
                if($tipo == 2) {
                    // venta
                    $sql .= ' AND propiedad.tipo_enlistamiento = 1';
                }
                if($tipo == 3) {
                    // venta/Alquiler
                    $sql .= ' AND propiedad.tipo_enlistamiento = 3';
                }
            }

            if( isset($cc) && (strlen(trim($cc)) > 0) ) {
                $params[] = $cc;
                $sql .= " AND propiedad.pais = ?";
                // $sql = $db->quoteInto($sql, $cc);
            }

            if ( ($min_price >= 0) && ($max_price > 0) ) {
                //$params[] = $min_price;
                //$params[] = $max_price;
                if($tipo == 1) {
                    // alquiler
                    $sql .= " AND precio_alquiler BETWEEN $min_price AND $max_price";
                }
                if($tipo == 2) {
                    // venta
                    $sql .= " AND precio_venta BETWEEN $min_price AND $max_price";
                }
                if($tipo == 3) {
                    // venta
                    $sql .= " AND (precio_alquiler BETWEEN $min_price AND $max_price OR precio_venta BETWEEN $min_price AND $max_price) ";
                }
                //$sql = $this->dbAdapter->quoteInto($sql, $min_price, $max_price);
            }

            if ( ($min_creditos >= 0) && ($max_creditos > 0) ) {

                $sql .= " AND creditos BETWEEN $min_creditos AND $max_creditos";

            }

            if ( ($min_beds >= 0) && ($max_beds > 0) ) {
                $sql .= " AND cuartos BETWEEN $min_beds AND $max_beds";
            }

            if ( ($min_baths >= 0) && ($max_baths > 0) ) {
                $sql .= " AND banos BETWEEN $min_baths AND $max_baths";
            }


//                    if (isset($cuartos) && ( strlen(trim($cuartos)) > 0)&& ($cuartos <> 0 ) ) {
//                        $cuartos_temp = split('-', $cuartos);
//                        if(is_numeric($cuartos_temp[0])) {
//                            $l1 = $cuartos_temp[0];
//                        }
//                        if(is_numeric($cuartos_temp[1])) {
//                            $l2 = $cuartos_temp[1];
//                        } else {
//                            $l2=0;
//                        }
//                        if($l2 <> 0 ) {
//                            //$params[] = $l1;
//                            //$params[] = $l2;
//                            $sql .= " AND cuartos BETWEEN $l1 AND $l2";
//                            //$sql = $this->dbAdapter->quoteInto($sql, $l1, $l2);
//                        } else {
//                            //$params[] = $l1;
//                            $sql .= " AND cuartos >= $l1";
//                            //$sql = $this->dbAdapter->quoteInto($sql, $l1);
//
//                        }
//                    }

//                    if (isset($banos) && ( strlen(trim($banos)) > 0)&& ($banos <> 0 )) {
//                        $banos_temp = split('-', $banos);
//                        if(is_numeric($banos_temp[0])) {
//                            $l1 = $banos_temp[0];
//                        }
//                        if(is_numeric($banos_temp[1])) {
//                            $l2 = $banos_temp[1];
//                        } else {
//                            $l2=0;
//                        }
//                        if($l2 <> 0 ) {
//                            //$params[] = $l1;
//                            //$params[] = $l2;
//                            $sql .= " AND banos BETWEEN $l1 AND $l2";
//                            //$sql = $this->dbAdapter->quoteInto($sql, $l1, $l2);
//                        } else {
//                            //$params[] = $l1;
//                            $sql .= " AND banos >= $l1";
//                            //$sql = $this->dbAdapter->quoteInto($sql, $l1);
//
//                        }
//                    }

            if ($area > 0) {
                //$params[] = $area;
                $sql .= " AND area >= $area";
                //$sql = $this->dbAdapter->quoteInto($sql, $area);
            }

            if (isset($cat) && ( strlen(trim($cat)) > 0)) {
                $params[] = $cat;
                $sql .= " AND tipo_propiedad = ?";

            }
            if (isset($fecha_ac) && ( strlen(trim($fecha_ac)) > 0)) {
                $params[] = Dgt_Data_Utils::ConvertirFecha($fecha_ac);

                $sql .= " AND fecha_asignacion_credito = ?";

            }
            if (isset($fecha_cc) && ( strlen(trim($fecha_cc)) > 0)) {
                $params[] = Dgt_Data_Utils::ConvertirFecha($fecha_cc);
                $sql .= " AND fecha_expiracion_credito = ?";

            }


        }



        //Zend_Debug::dump( __METHOD__ . ' sql:  ' . $sql);
        return Dgt_Data_Utils::doQuery($sql, $params);

    }




    //Formato Anglosajon: AÃ±o, mes y dÃ­a
    private static function ConvertirFecha($fecha) {
        $fechaConvertir=array();
        $fechaConvertir=explode('/',$fecha);
        $fecha=$fechaConvertir[2].'-'.$fechaConvertir[1].'-'.$fechaConvertir[0];
        return $fecha;
    }

    public static function reportConsultas($f1, $f2, $userId = null) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $params = array();
        $params[] = $f1;
        $params[] = $f2;
        $sql = 'SELECT
                u.idusuario id,
                CONCAT(u.nombre, \' \', u.apellido) operador,
                COUNT(c.idconsulta) numero_consultas
                from
                consultas c INNER JOIN usuarios u ON c.usuarios_idusuario = u.idusuario
                WHERE
                c.fecha_consulta BETWEEN ? AND ?';
        if( isset($userId) && is_numeric($userId) ) {
            $params[] = $userId;
            $sql .= ' AND c.usuarios_idusuario = ?';
        }
        $sql .=' GROUP BY operador';
        $rs = $db->query($sql, $params);
        return $rs->fetchAll();
    }

    public static function reportRegistrados($f1, $f2, $userId = null) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $params = array();
        $params[] = $f1;
        $params[] = $f2;
        $sql = 'SELECT
                a.idactivado id,
                CONCAT(u.nombre, \' \', u.apellido) operador,
                SUBSTRING(a.numeracion_telefono, 4) movil_registrado,
                a.fecha_activacion fecha
                FROM
                activados a INNER JOIN usuarios u ON a.usuarios_idusuario = u.idusuario
                WHERE
                a.fecha_activacion BETWEEN ? and ?
                AND
                estados_idestado = 1';
        if( isset($userId) && is_numeric($userId) ) {
            $params[] = $userId;
            $sql .=' AND a.usuarios_idusuario = ?';
        }
        $sql .= ' ORDER BY fecha ASC';
        $rs = $db->query($sql, $params);
        return $rs->fetchAll();
    }

    public static function getCreCliente($sort,$dir,$start,$limit,$CodigoCliente=null) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql ="SELECT
                      CodigoCredito,
                      FechaCompra,
                      FechaVence,
                      Cantidad,
                      Saldo,
                      case EstadoCredito
                        when 'P' then 'Pendiente'
                        when 'A' then 'Autorizado'
                        when 'V' then 'Vencido'
                      end as Estado
                      FROM
                      crecredito where";
        if(isset($CodigoCliente) && strlen($CodigoCliente)>0)
            $sql.=" CodigoCliente=".$CodigoCliente;
        else
            $sql.=" CodigoCliente=-1";

        $sql.=" and Saldo>0 and EstadoCredito<>'V'";
        
        $sql .= " ORDER BY ".$sort." ".$dir;
        $sql .= " LIMIT $start , $limit";
        return Dgt_Data_Utils::doQuery($sql);
        //return Dgt_Data_Utils::convertDataforGrid($rs);
    }

    public static function getCreClienteCount($CodigoCliente=null) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = "select count(CodigoCredito) as total from crecredito where";
        if(isset($CodigoCliente) && strlen($CodigoCliente)>0)
            $sql.=" CodigoCliente=".$CodigoCliente;
        else
            $sql.=" CodigoCliente=-1";

        $sql.=" and Saldo>0 and EstadoCredito<>'V'";

        return Dgt_Data_Utils::doQuery($sql);
    }

    public static function getProPropiedad($sort,$dir,$start,$limit,$CodigoCliente=null,
            $Categoria=-1,$Pais=null,$Transaccion=null,$Ciudad=null,$Broker=null) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql ="select P.CodigoPropiedad,
            (select SUM(CCP.Cantidad) from crecreditopropiedad CCP where CCP.CodigoPropiedad = P.CodigoPropiedad) as Creditos,
            P.NombrePropiedad,
            CP.Pais,
            P.Ciudad,
            E.NombreEdoPropiedad as Estatus,
            P.FechaRegistro,
            P.CodigoBroker
            from propropiedad P
            left join catedopropiedad E on P.CodigoEdoPropiedad = E.CodigoEdoPropiedad
            left join catpais CP on P.ZipCode = CP.ZipCode
            inner join cliente CL on P.Uid = CL.Uid
            where";
        if(isset($CodigoCliente) && strlen($CodigoCliente)>0)
            $sql.=" CL.CodigoCliente = ".$CodigoCliente;
        else
            $sql.=" CL.CodigoCliente = -1";

        if($Categoria>=0)
            $sql.=" and P.CodigoCategoria=".$Categoria;
        
        if(isset($Pais) && strlen($Pais)>0)
            $sql.=" and P.ZipCode='".$Pais."'";

        if(isset($Transaccion) && strlen($Transaccion)>0)
            $sql.=" and P.Accion='".$Transaccion."'";

        if(isset($Ciudad) && strlen($Ciudad)>0)
            $sql.=" and P.Ciudad like '%".$Ciudad."%'";

        if(isset($Broker) && strlen($Broker)>0)
            $sql.=" and P.CodigoBroker like '%".$Broker."%'";

        $sql.=" and P.CodigoEdoPropiedad=2";

        $sql .= " ORDER BY ".$sort." ".$dir;
        $sql .= " LIMIT $start , $limit";
        $rs= Dgt_Data_Utils::doQuery($sql);
        return Dgt_Data_Utils::convertDataforGrid($rs);

    }

    public static function getProPropiedadCount($CodigoCliente=null,$Categoria=-1,$Pais=null,
            $Transaccion=null,$Ciudad=null,$Broker=null) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = "select count(P.CodigoPropiedad) as total from propropiedad P
                inner join cliente CL on P.Uid = CL.Uid 
                where";

        if(isset($CodigoCliente) && strlen($CodigoCliente)>0)
            $sql.=" CL.CodigoCliente = ".$CodigoCliente;
        else
            $sql.=" CL.CodigoCliente = -1";

        if($Categoria>=0)
            $sql.=" and P.CodigoCategoria=".$Categoria;

        if(isset($Pais) && strlen($Pais)>0)
            $sql.=" and P.ZipCode='".$Pais."'";

        if(isset($Transaccion) && strlen($Transaccion)>0)
            $sql.=" and P.Accion='".$Transaccion."'";

        if(isset($Ciudad) && strlen($Ciudad)>0)
            $sql.=" and P.Ciudad like '%".$Ciudad."%'";

        if(isset($Broker) && strlen($Broker)>0)
            $sql.=" and P.CodigoBroker like '%".$Broker."%'";

        $sql.=" and P.CodigoEdoPropiedad=2";
        
        return Dgt_Data_Utils::doQuery($sql);
    }


     public static function convertDataforGrid($arr) {
        for($i=0;$i<count($arr);$i++) {
            $arr[$i]->NombrePropiedad= iconv("latin1","utf-8", $arr[$i]->NombrePropiedad);
            $arr[$i]->Ciudad= iconv("latin1","utf-8", $arr[$i]->Ciudad);
        }
        return $arr;
    }

        public static function getDetalleCredito($sort,$dir,$start,$limit,$CodigoCredito=-1,$CodigoPropiedad=-1) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql ="select CodigoCreditoProp,CodigoCredito,CodigoPropiedad,Cantidad,EstadoAsigna
              from crecreditopropiedad
              where CodigoCredito=$CodigoCredito and CodigoPropiedad=$CodigoPropiedad";
        
        $sql .= " ORDER BY ".$sort." ".$dir;
        $sql .= " LIMIT $start , $limit";
        //$rs= Dgt_Data_Utils::doQuery($sql);
        //return Dgt_Data_Utils::convertDataforGrid($rs);
        return Dgt_Data_Utils::doQuery($sql);

    }

    public static function getDetalleCreditoCount($CodigoCredito=-1,$CodigoPropiedad=-1) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = "select count(CodigoCreditoProp) as total from crecreditopropiedad
                where CodigoCredito=$CodigoCredito and CodigoPropiedad=$CodigoPropiedad";
        
        return Dgt_Data_Utils::doQuery($sql);
    }

}