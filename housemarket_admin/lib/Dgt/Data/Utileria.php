<?php

/**
 * @todo  refactoriar esta clase. comun $db, $sql, $params, $rs.
 */
class Dgt_Data_Utileria {

    public static function getUsers() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        //$sql = 'SELECT id, username, active, nombre, apellido, email FROM users';
        $sql = 'SELECT idusuario, tipo, usuario, estatus as active,
                if( estatus = 1, \'activa\', \'inactiva\') estatus,
                nombre, apellido, email FROM usuarios';
        $rs = $db->query($sql);
        return $rs->fetchAll();
    }

    private static function doQuery($sql, $params=null) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        if (isset($params)) {
            $rs = $db->query($sql,$params);
        } else {
            $rs = $db->query($sql);
        }
        $data = $rs->fetchAll();
        return $data;
    }

    public static function getCreditos($sort,$dir,$start,$limit) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql ="SELECT
                      C.CodigoCredito,
                      R.NombreRepresenta,
                      CL.NombreCliente,
                      C.FechaPedido,
                      C.Cantidad,
                      C.MontoCompra,
                      (C.Cantidad-C.Saldo) as CreditoAsignado
                      FROM
                      crecredito C
                      inner join representante R on C.CodigoRepresentante=R.CodigoRepresenta 
                      inner join cliente CL on C.CodigoCliente=CL.CodigoCliente where C.EstadoCredito='P'";

        $sql .= " ORDER BY ".$sort." ".$dir;
        $sql .= " LIMIT $start , $limit";
        $rs=Dgt_Data_Utileria::doQuery($sql);
        return Dgt_Data_Utileria::convertDataforGrid($rs,$cliente=true,$ciudad=false,$propiedad=false,$representante=true);
        //return Dgt_Data_Utileria::doQuery($sql);
    }

    public static function getCountCreditos() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = "SELECT count(C.CodigoCredito) as total FROM crecredito C
                      inner join representante R on C.CodigoRepresentante=R.CodigoRepresenta
                      inner join cliente CL on C.CodigoCliente=CL.CodigoCliente where C.EstadoCredito='P'";

        return Dgt_Data_Utileria::doQuery($sql);
    }

    public static function getPropiedad($sort,$dir,$start,$limit,$CodigoCredito=null) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql ="select P.CodigoPropiedad,P.NombrePropiedad,CP.Pais,P.Ciudad,CCP.Cantidad
                from propropiedad P
                inner join crecreditopropiedad CCP on P.CodigoPropiedad=CCP.CodigoPropiedad
                left join catpais CP on P.ZipCode=CP.ZipCode where";

        if(isset($CodigoCredito) && strlen($CodigoCredito)>0)
            $sql.=" CCP.CodigoCredito = ".$CodigoCredito;
        else
            $sql.=" CCP.CodigoCredito = -1";

        $sql .= " ORDER BY ".$sort." ".$dir;
        $sql .= " LIMIT $start , $limit";
        $rs=Dgt_Data_Utileria::doQuery($sql);
        return Dgt_Data_Utileria::convertDataforGrid($rs,$cliente=false,$ciudad=true,$propiedad=true,$representante=false);
        //return Dgt_Data_Utileria::doQuery($sql);
    }

    public static function getCountPropiedad($CodigoCredito=null) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql ="select count(P.CodigoPropiedad) as total from propropiedad P
                inner join crecreditopropiedad CCP on P.CodigoPropiedad=CCP.CodigoPropiedad
                left join catpais CP on P.ZipCode=CP.ZipCode where";

        if(isset($CodigoCredito) && strlen($CodigoCredito)>0)
            $sql.=" CCP.CodigoCredito = ".$CodigoCredito;
        else
            $sql.=" CCP.CodigoCredito = -1";

        return Dgt_Data_Utileria::doQuery($sql);
    }

    public static function getCarga($sort,$dir,$start,$limit) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql ="select CodigoCliente,NombreCliente,TelefonoCliente,EMail,
                case when FechaOtorgaCarga is null and FechaRevocaCarga is null then
                     'Solicitud'
                else
                    case when FechaOtorgaCarga is not null then
                        'Autorizado'
                    else
                        'Revocado'
                    end
                end as Estado,
                FechaSolicitaCarga,FechaOtorgaCarga,FechaRevocaCarga
                from cliente where FechaSolicitaCarga is not null ";

        $sql .= " ORDER BY ".$sort." ".$dir;
        $sql .= " LIMIT $start , $limit";
        $rs=Dgt_Data_Utileria::doQuery($sql);
        return Dgt_Data_Utileria::convertDataforGrid($rs,$cliente=true,$ciudad=false,$propiedad=false,$representante=false);

    }

    public static function getCountCarga() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql ="select count(C.CodigoCliente) as total from cliente C where FechaSolicitaCarga is not null ";

        return Dgt_Data_Utileria::doQuery($sql);
    }
    
    public static function getTab($sort,$dir,$start,$limit) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = "SELECT ps.id, ps.pageId, ps.pageName AS name, ps.serviceStatus, ps.serviceStartDate, ps.serviceExpirationDate, ps.requestDate
                FROM page_service_status AS ps
                WHERE serviceType = 1";

        $sql .= " ORDER BY ".$sort." ".$dir;
        $sql .= " LIMIT $start , $limit";
        $rs=Dgt_Data_Utileria::doQuery($sql);
        return $rs;
    }
    
    public static function getCountTab() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql ="select count(id) as total from page_service_status where serviceType = 1";

        return Dgt_Data_Utileria::doQuery($sql);
    }

    public static function convertDataforGrid($arr,$cliente=false,$ciudad=false,$propiedad=false,$representante=false) {
        for($i=0;$i<count($arr);$i++) {
            if($cliente)
                $arr[$i]->NombreCliente= iconv("latin1","utf-8", $arr[$i]->NombreCliente);
            if($ciudad)
                $arr[$i]->Ciudad= iconv("latin1","utf-8", $arr[$i]->Ciudad);
            if($propiedad)
                $arr[$i]->NombrePropiedad= iconv("latin1","utf-8", $arr[$i]->NombrePropiedad);
            if($representante)
                $arr[$i]->NombreRepresenta= iconv("latin1","utf-8", $arr[$i]->NombreRepresenta);
        }
        return $arr;
    }

    public static function getRepresentantes($sort,$dir,$start,$limit) {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql ="SELECT
               CodigoRepresenta,NombreRepresenta as Representante,NombreEmpresa as Empresa,
               Direccion,Telefono,EMail FROM representante";

        $sql .= " ORDER BY ".$sort." ".$dir;
        $sql .= " LIMIT $start , $limit";
        $rs=Dgt_Data_Utileria::doQuery($sql);
        return Dgt_Data_Utileria::convertDataStandart($rs,true,true,true);
    }

    public static function getCountRepresentantes() {
        $db = Zend_Registry::get('dbAdapter');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = "SELECT count(CodigoRepresenta) as total FROM representante";

        return Dgt_Data_Utileria::doQuery($sql);
    }

     public static function convertDataStandart($arr,$representante=false,$empresa=false,$direccion=false) {
        for($i=0;$i<count($arr);$i++) {
            if($representante)
                $arr[$i]->Representante= iconv("latin1","utf-8", $arr[$i]->Representante);
            if($empresa)
                $arr[$i]->Empresa= iconv("latin1","utf-8", $arr[$i]->Empresa);
            if($direccion)
                $arr[$i]->Direccion= iconv("latin1","utf-8", $arr[$i]->Direccion);
            
        }
        return $arr;
    }


}