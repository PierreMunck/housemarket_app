CREATE TABLE IF NOT EXISTS `profavorito` (
  `CodigoFavorito` int(11) NOT NULL auto_increment,
  `CodigoPropiedad` text NOT NULL COMMENT 'Identificador de la propiedad',
  `CodigoCliente` text NOT NULL COMMENT 'Identificador del usiario',
  PRIMARY KEY  (`CodigoFavorito`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Propiedad favorita del usuario' AUTO_INCREMENT=0 ;

ALTER TABLE `cliente` 
ADD `ComparteFavorito` BIT( 1 ) NULL DEFAULT b'00000' 
COMMENT 'Indica si el cliente da el permiso para acceder a sus propiedades favoritas. Por default el valor es 0-> sin permiso. ' 
AFTER CargaMasiva ;