#! /bin/sh
DUMPDIR="/var/www/vhosts/ip-50-63-147-230.ip.secureserver.net/hmapp/backups"
mkdir -p ${DUMPDIR}
fecha=`date +%Y%m%d%H%M%s`
db="hmapp"
find ${DUMPDIR}/* -name "${db}_*.sql.gz" -mtime +7 -exec rm -f {} \; -print > ${DUMPDIR}/run.log
echo "------------------------"
echo "BACKUP"
file="${db}_${fecha}.sql.gz"
echo "backup de ${db} en el archivo ${file}"
mysqldump --opt -h localhost -uhmapp -pH1M2A3p4p_ hmapp | gzip > ${DUMPDIR}/${file}
echo "------------------------"
echo "finalizado"
echo "========================"
