#!/bin/bash

cd /home/hmadmin/procesos/archivos

fecha=`date +%Y%m%d%H%M%S`

echo "" > eliminapropiedades_${fecha}.log
echo "---------------------------" >> eliminapropiedades_${fecha}.log
echo "Inicia - `date`" >> eliminapropiedades_${fecha}.log

mysql --user=hmapp --password=xolotlan hmapp << EOF

\T eliminapropiedades_${fecha}.log

delete from propropiedad where CodigoEdoPropiedad=4;

delete from propropiedad where CodigoEdoPropiedad=5 and
CURRENT_DATE > DATE_ADD(FechaInactiva,INTERVAL 3 MONTH);

commit;


EOF

#consultamos el resultado de la ejecucion
if [ $? -eq "0" ]
then
  echo "Ejecuciòn Correcta" >> eliminapropiedades_${fecha}.log
else
  echo "Ejecuciòn fallida" >> eliminapropiedades_${fecha}.log 
fi

echo "Fin - `date`" >> eliminapropiedades_${fecha}.log
echo "" >> eliminapropiedades_${fecha}.log
