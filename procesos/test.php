<?php

/*
 * Actualizacion a la clase facebook 3.0.1
 * Mejoras en rendimiento del php y redireccion de las paginas
 * Ambiente de desarrollo bajo lighttpd
 * Para la instalacion: 
 * -app.log tiene como propietario a apache
 * -Se debe activar las directivas de reescritura en el .htaccess
 * -Se requiere de autenticacion oAuth en el panel de la aplicacion en facebook
 * -Para optimiazar se comprime con mod_deflate
 */

// Define path to application directory
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../housemarket/application'));
/* Si no esta definida esta variable de entorno en los archivos de configuracion del servidor web, no se cargara la configuracion de desarrollo
 * y se cargarÃ¡ por defecto la configuracion de produccion, generando un redireccionamiento a hmapp.com
 */
// Define application environment. 
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

//Mostramos los errores en desarrollo
error_reporting(E_ALL);

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
            realpath(APPLICATION_PATH . '/../housemarket/library/'),
            realpath(APPLICATION_PATH . '/models/'),
            realpath(APPLICATION_PATH . '/views/helpers/'),
			realpath(APPLICATION_PATH . '/controllers/'),
            get_include_path(),
        )));

/** Zend_Application * */
require_once 'Zend/Loader/Autoloader.php';
require_once 'Zend/Config/Ini.php';
require_once 'Zend/Session/Namespace.php';
require_once 'Zend/Session.php';

/*
 * Clases para el manejo de arreglos
 * Clase de facebook 3.0.1 para manejo de la informacion de usuarios facebook
 */

require_once 'Hm/Table.class.php';
require_once 'Hm/Cache.php';
require_once 'functions/function.php';
require_once 'functions/common.php';
//Cargando el archivo de configuracion

$cfg = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);

// Create application
require_once 'Zend/Application.php';
try{
	$application = new Zend_Application(
		APPLICATION_ENV,
		APPLICATION_PATH . '/configs/application.ini'
	);
}catch(Exception $e){
	print_r($e);
}

// application bootstrap
try{
	$application->bootstrap();
}catch(Exception $e){
	print_r($e);
}

$UserUid = '100000041940062';

$db = Zend_Registry::get('db');
// recuperación de información si el user tiene el derecho de broker (see all)

$sql = "SELECT count(*) as nbpropriedad FROM propropiedad p INNER JOIN cliente c on p.Uid = c.Uid WHERE c.Uid =" . $UserUid;
echo "<html>";
$NbPropriedad = $db->fetchAll($sql);
$user_info['nbPropriedad'] = $NbPropriedad[0]->nbpropriedad;
$user_info['type'] = 'comun';
if($user_info['nbPropriedad'] > 0){
	$user_info['type'] = 'broker';
}


echo "<pre>";
print_r($user_info);
echo "</pre>";

$proprietyId = '15428';

$property = new Hm_Pro_Propiedad();
$result = $property->GetPropiedad($proprietyId);

echo "<pre>";
print_r($result);
echo "</pre>";

$result = Hm_Pro_Propiedad::GetPropertyDataByUid($UserUid);
echo "<pre>";
print_r($result);
echo "</pre>";
echo '<img src="/pic36810_4.jpg"/>';
echo "</html>";