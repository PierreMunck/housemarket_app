#!/bin/bash

cd /home/hmadmin/procesos/archivos

fecha=`date +%Y%m%d%H%M%S`

echo "" > evaluapropiedades_${fecha}.log
echo "---------------------------" >> evaluapropiedades_${fecha}.log
echo "Inicia - `date`" >> evaluapropiedades_${fecha}.log

mysql --user=hmapp --password=xolotlan hmapp << EOF

\T evaluapropiedades.sh_${fecha}.log

update propropiedad set CodigoEdoPropiedad=5, FechaInactiva=CURRENT_DATE     where CURRENT_DATE > DATE_ADD(FechaPublica,INTERVAL 3 MONTH) and
CodigoEdoPropiedad=2;

commit;


EOF

#consultamos el resultado de la ejecucion
if [ $? -eq "0" ]
then
  echo "Ejecuciòn Correcta" >> evaluapropiedades_${fecha}.log
else
  echo "Ejecuciòn fallida" >> evaluapropiedades_${fecha}.log 
fi

echo "Fin - `date`" >> evaluapropiedades_${fecha}.log
echo "" >> evaluapropiedades_${fecha}.log
