#!/bin/bash 

while read line    
do
  echo -e "Processig file: $line\n"    
  php importxml.php "$line"
  echo -e "Processed file: $line\n"    
done < $1
