#!/bin/bash
cd /home/hmadmin/backup/
fecha=`date +%Y%m%d%H%M`
db="hmapp"
echo "----------------------------------"
echo "Accediendo a la base ${db} en hmapp.com"
file="${db}_${fecha}.sql"
echo "Procesando backup para ${db}, espere..."
mysqldump -h hmapp.com --opt --add-drop-database -u hmapp -pH1M2A3p4p_ --databases hmapp > ${file}
echo "Backup generado para ${db}, el archivo generado es ${file}"
echo "Proceso de backup finalizado."
echo "Eliminando versiones de 3 dias anteriores"
find /home/hmadmin/backup -mtime +1 -exec rm -f {} \;
echo "Eliminacion terminada"
#echo "Accediendo a la base ${db} local"
#echo "La base de datos se esta actualizando, espere..."
#mysql -u hmapp -pH1M2A3p4p_ hmapp < ${file}
#echo "Base de datos actualizada"
