#!/bin/bash

cd /home/hmadmin/procesos/archivos

fecha=`date +%Y%m%d%H%M%S`

echo "" > creditospropiedades_${fecha}.log
echo "---------------------------" >> creditospropiedades_${fecha}.log
echo "Inicia - `date`" >> creditospropiedades_${fecha}.log

mysql --user=hmapp --password=xolotlan hmapp << EOF

\T creditospropiedades_${fecha}.log

update crecreditopropiedad set EstadoAsigna='V' where CodigoCredito in(
select CodigoCredito from crecredito where FechaVence < CURRENT_DATE and
EstadoCredito='A');

update crecredito set EstadoCredito='V' where FechaVence < CURRENT_DATE and
EstadoCredito='A'; 

commit;


EOF

#consultamos el resultado de la ejecucion
if [ $? -eq "0" ]
then
  echo "Ejecuciòn Correcta" >> creditospropiedades_${fecha}.log
else
  echo "Ejecuciòn fallida" >> creditospropiedades_${fecha}.log 
fi

echo "Fin - `date`" >> creditospropiedades_${fecha}.log
echo "" >> creditospropiedades_${fecha}.log
