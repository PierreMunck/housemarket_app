#!/bin/bash

cd /home/hmadmin/procesos/archivos

fecha=`date +%Y%m%d%H%M%S`

echo "" > cantidadpropiedades_${fecha}.log
echo "---------------------------" >> cantidadpropiedades_${fecha}.log
echo "Inicia - `date`" >> cantidadpropiedades_${fecha}.log

mysql --user=hmapp --password=xolotlan hmapp << EOF

\T cantidadpropiedades_${fecha}.log

insert into RecuentoPropiedad (FechaHora,Cantidad)  values(CURRENT_TIMESTAMP,(select count(CodigoPropiedad) from propropiedad where CodigoEdoPropiedad=2));

commit;


EOF

#consultamos el resultado de la ejecucion
if [ $? -eq "0" ]
then
  echo "Ejecuciòn Correcta" >> cantidadpropiedades_${fecha}.log
else
  echo "Ejecuciòn fallida" >> cantidadpropiedades_${fecha}.log 
fi

echo "Fin - `date`" >> cantidadpropiedades_${fecha}.log
echo "" >> cantidadpropiedades_${fecha}.log
